<?php
namespace Base\Journal;

use Marmot\Core;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

use Base\Journal\Model\Journal;
use Base\Journal\Model\Banner;
use Base\Journal\Model\UnAuditedJournal;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

trait SetDataTrait
{
    protected function userGroup($id = 1) : array
    {
        return
        [
            [
                'user_group_id' => $id,
                'name' => '税务局',
                'short_name' => '税务局',
                'status' => UserGroup::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }

    protected function crew($id = 2) : array
    {
        return
        [
            [
                'crew_id' => $id,
                'user_name' => '18800000001',
                'cellphone' => '18800000001',
                'real_name' => '张正科',
                'card_id' => '610424198810202422',
                'status' => 0,
                'category' => Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'],
                'purview' => json_encode(array(1,2,3,4)),
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'user_group_id' => 0,
                'department_id' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function journal($status = IEnableAble::STATUS['ENABLED']) : array
    {
        return
        [
            [
                'journal_id' => 1,
                'title' => '信用刊物标题',
                'source' => '信用刊物来源',
                'description' => '信用刊物简介',
                'cover' => json_encode(array('name'=>'封面名称', 'identify'=>'封面地址')),
                'attachment' => json_encode(array('name'=>'附件名称', 'identify'=>'附件地址.pdf')),
                'auth_images' => json_encode(array(array('name'=>'授权图片名称', 'identify'=>'授权图片地址'))),
                'year' => '2021',
                'crew_id' => 2,
                'publish_usergroup_id' => 1,
                'status' => $status,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyForm(
        $applyStatus = IApproveAble::APPLY_STATUS['APPROVE'],
        $operationType = IApproveAble::OPERATION_TYPE['ADD']
    ) : array {
        return
        [
            [
                'apply_form_id' => 1,
                'title' => '信用刊物标题',
                'relation_id' => 2,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 2,
                'operation_type' => $operationType,
                'apply_info' => base64_encode(gzcompress(serialize(array('apply_info')))),
                'reject_reason' => '驳回原因',
                'apply_info_category' => UnAuditedJournal::APPLY_JOURNAL_CATEGORY,
                'apply_info_type' => UnAuditedJournal::APPLY_JOURNAL_TYPE,
                'apply_status' => $applyStatus,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
