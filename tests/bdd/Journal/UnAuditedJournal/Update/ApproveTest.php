<?php
namespace Base\Journal\UnAuditedJournal\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;

use Base\Journal\SetDataTrait;
use Base\Journal\Repository\UnAuditedJournalRepository;

use Base\Crew\Model\Crew;

/**
 * @Feature: 我是拥有信用刊物审核权限的平台管理员/委办局管理员/操作用户,当我需要审核一个信用刊物时,在审核表中,审核待审核的信用刊物数据
 *           通过信用刊物详情页面的审核通过与审核驳回操作,以便于我维护信用刊物列表
 * @Scenario: 审核通过
 */
class ApproveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条待审核的信用刊物审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyForm(IApproveAble::APPLY_STATUS['PENDING']),
            'pcore_crew' => $this->crew(1)
        ]);
    }

    /**
     * @When: 获取需要审核通过的信用刊物
     */
    public function fetchUnAuditedJournal($id)
    {
        $repository = new UnAuditedJournalRepository();

        $unAuditedJournal = $repository->fetchOne($id);

        return $unAuditedJournal;
    }

    /**
     * @And: 当我调用审核通过函数,期待返回true
     */
    public function approve()
    {
        $unAuditedJournal = $this->fetchUnAuditedJournal(1);
        $unAuditedJournal->setApplyCrew(new Crew(1));
        
        return $unAuditedJournal->approve();
    }

    /**
     * @Then: 数据已经被审核通过
     */
    public function testValidate()
    {
        $result = $this->approve();

        $this->assertTrue($result);

        $unAuditedJournal = $this->fetchUnAuditedJournal(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['APPROVE'], $unAuditedJournal->getApplyStatus());
        $this->assertEquals(Core::$container->get('time'), $unAuditedJournal->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $unAuditedJournal->getStatusTime());
        $this->assertEquals(1, $unAuditedJournal->getApplyCrew()->getId());
    }
}
