<?php
namespace Base\Journal\UnAuditedJournal\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Journal\SetDataTrait;
use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Repository\UnAuditedJournalRepository;
use Base\Journal\Translator\UnAuditedJournalDbTranslator;

use Base\Crew\Model\Crew;

use Base\Common\Model\IApproveAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

/**
 * @Feature: 我是拥有信用刊物管理权限的平台管理员/委办局管理员/操作用户,当我需要编辑信用刊物信息时,在审核表中,编辑已驳回信用刊物数据
 *           通过编辑信用刊物界面，并根据我所采集的信用刊物数据进行编辑,以便于我可以更好的管理信用刊物
 * @Scenario: 正常编辑信用刊物审核数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyForm(IApproveAble::APPLY_STATUS['REJECT']),
            'pcore_crew' => $this->crew(3),
        ]);
    }

    /**
     * @When: 获取需要编辑的信用刊物数据
     */
    public function fetchUnAuditedJournal($id)
    {
        $repository = new UnAuditedJournalRepository();

        $unAuditedJournal = $repository->fetchOne($id);

        return $unAuditedJournal;
    }
    /**
     * @When: 当我调用重新编辑函数,期待返回true
     */
    public function resubmit()
    {
        $unAuditedJournal = $this->fetchUnAuditedJournal(1);

        $unAuditedJournal->setApplyTitle('测试重新编辑');
        $unAuditedJournal->setRelation(new Crew(3));
        $unAuditedJournal->setApplyInfo(array('测试重新编辑'));

        return $unAuditedJournal->resubmit();
    }

    /**
     * @Then: 可以查到重新编辑的数据
     */
    public function testValidate()
    {
        $result = $this->resubmit();

        $this->assertTrue($result);
        
        $unAuditedJournal = $this->fetchUnAuditedJournal(1);

        $this->assertEquals('测试重新编辑', $unAuditedJournal->getApplyTitle());
        $this->assertEquals(3, $unAuditedJournal->getRelation()->getId());
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $unAuditedJournal->getApplyStatus());
    }
}
