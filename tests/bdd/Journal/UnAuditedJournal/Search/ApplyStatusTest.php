<?php
namespace Base\Journal\UnAuditedJournal\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Journal\SetDataTrait;
use Base\Journal\Repository\UnAuditedJournalRepository;
use Base\Journal\Translator\UnAuditedJournalDbTranslator;

use Base\Common\Model\IApproveAble;

 /**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,我拥有信用刊物管理权限、当我需要查看某个特定的信用刊物时,
 *           在审核表中,我可以搜索到我需要的信用刊物数据
 *           通过列表形式查看我搜索到的信用刊物数据 ,以便于我可以快速的搜索到我需要的信用刊物数据
 * @Scenario: 通过审核状态搜索
 */
class ApplyStatusTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
    }

    /**
     * @Given: 存在信用刊物审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyForm()
        ]);
    }
    /**
     * @When: 当我查看信用刊物审核数据列表时
     */
    public function fetchUnAuditedJournalList()
    {
        $repository = new UnAuditedJournalRepository();
 
        $filter['apply_status'] = IApproveAble::APPLY_STATUS['APPROVE'];

        list($unAuditedJournalList, $count) = $repository->filter($filter);
        
        unset($count);

        return $unAuditedJournalList;
    }

    /**
     * @Then: 我可以看到信用刊物审核状态为"已通过"的信用刊物审核数据
     */
    public function testViewUnAuditedJournalList()
    {
        $setUnAuditedJournalList = $this->applyForm();

        foreach ($setUnAuditedJournalList as $key => $setUnAuditedJournal) {
            unset($setUnAuditedJournal);
            $setUnAuditedJournalList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedJournalList = $this->fetchUnAuditedJournalList();
        $translator = new UnAuditedJournalDbTranslator();
        foreach ($unAuditedJournalList as $unAuditedJournal) {
            $unAuditedJournalArray[] = $translator->objectToArray($unAuditedJournal);
        }

        $this->assertEquals($unAuditedJournalArray, $setUnAuditedJournalList);

        foreach ($unAuditedJournalArray as $unAuditedJournal) {
            $this->assertEquals(IApproveAble::APPLY_STATUS['APPROVE'], $unAuditedJournal['apply_status']);
        }
    }
}
