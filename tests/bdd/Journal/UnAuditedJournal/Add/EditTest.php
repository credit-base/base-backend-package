<?php
namespace Base\Journal\UnAuditedJournal\Add;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

use Base\Journal\SetDataTrait;
use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Repository\UnAuditedJournalRepository;
use Base\Journal\Translator\UnAuditedJournalDbTranslator;

use Base\Common\Model\IApproveAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

/**
 * @Feature: 我是拥有信用刊物发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑信用刊物时,在发布表中,编辑对应信用刊物数据
 *           通过编辑信用刊物界面，并根据我所采集的信用刊物数据进行编辑,以便于我可以更好的维护信用刊物管理列表
 * @Scenario: 编辑信用刊物分类,需要审核
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在需要编辑的信用刊物
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(),
        ]);
    }

    /**
     * @When: 当我调用审核的编辑函数,期待返回true
     */
    public function edit()
    {
        $applyFormArray = $this->applyForm(
            IApproveAble::APPLY_STATUS['PENDING'],
            IApproveAble::OPERATION_TYPE['EDIT']
        )[0];

        $unAuditedJournal = new UnAuditedJournal();

        $unAuditedJournal->setApplyTitle($applyFormArray['title']);
        $unAuditedJournal->setRelation(new Crew($applyFormArray['relation_id']));
        $unAuditedJournal->setApplyCrew(new Crew($applyFormArray['apply_crew_id']));
        $unAuditedJournal->setApplyUserGroup(new UserGroup($applyFormArray['apply_usergroup_id']));
        $unAuditedJournal->setOperationType($applyFormArray['operation_type']);
        $unAuditedJournal->setApplyInfo(unserialize(gzuncompress(base64_decode($applyFormArray['apply_info'], true))));
        $unAuditedJournal->setRejectReason($applyFormArray['reject_reason']);


        return $unAuditedJournal->edit();
    }

    /**
     * @Then: 可以查到新增的审核数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);

        $setUnAuditedJournal = $this->applyForm(
            IApproveAble::APPLY_STATUS['PENDING'],
            IApproveAble::OPERATION_TYPE['EDIT']
        )[0];
        $setUnAuditedJournal['apply_form_id'] = 1;
        
        $repository = new UnAuditedJournalRepository();

        $unAuditedJournal = $repository->fetchOne($result);

        $translator = new UnAuditedJournalDbTranslator();
        $unAuditedJournalArray = $translator->objectToArray($unAuditedJournal);

        $this->assertEquals($unAuditedJournalArray, $setUnAuditedJournal);
    }
}
