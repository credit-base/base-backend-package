<?php
namespace Base\Journal\UnAuditedJournal\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Journal\SetDataTrait;
use Base\Journal\Repository\UnAuditedJournalRepository;
use Base\Journal\Translator\UnAuditedJournalDbTranslator;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,我拥有信用刊物管理权限、且当我需要查看未审核或已驳回的信用刊物列表时,
 *           在审核表中,查看审核表中的已驳回或是未审核的信息,通过列表与详情的形式查看到未审核或已驳回的信用刊物信息,以便于我维护信用刊物模块
 * @Scenario: 查看信用刊物审核数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在信用刊物审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_apply_form' => $this->applyForm(),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看信用刊物审核数据列表时
     */
    public function fetchUnAuditedJournalList()
    {
        $repository = new UnAuditedJournalRepository();

        list($unAuditedJournalList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedJournalList;
    }

    /**
     * @Then: 我可以看到信用刊物数据的全部信息
     */
    public function testViewJournalList()
    {
        $setUnAuditedJournalList = $this->applyForm();

        foreach ($setUnAuditedJournalList as $key => $setUnAuditedJournal) {
            unset($setUnAuditedJournal);
            $setUnAuditedJournalList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedJournalList = $this->fetchUnAuditedJournalList();

        $translator = new UnAuditedJournalDbTranslator();
        foreach ($unAuditedJournalList as $unAuditedJournal) {
            $unAuditedJournalArray[] = $translator->objectToArray($unAuditedJournal);
        }

        $this->assertEquals($unAuditedJournalArray, $setUnAuditedJournalList);
    }
}
