<?php
namespace Base\Journal\Journal\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Journal\SetDataTrait;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Translator\JournalDbTranslator;

 /**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有信用刊物管理权限、当我需要查看某个特定的已发布信用刊物时,
 *           在发布表中,我可以搜索到我需要的信用刊物数据
 *           通过列表形式查看我搜索到的信用刊物数据 ,以便于我可以快速的搜索到我需要的信用刊物数据
 * @Scenario: 通过发布年份搜索
 */
class YearTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_journal');
    }

    /**
     * @Given: 存在信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_journal' => $this->journal()
        ]);
    }
    /**
     * @When: 当我查看信用刊物数据列表时
     */
    public function fetchJournalList()
    {
        $repository = new JournalRepository();
 
        $filter['year'] = 2021;

        list($journalList, $count) = $repository->filter($filter);
        
        unset($count);

        return $journalList;
    }

    /**
     * @Then: 我可以看到信用刊物发布年份为"2021"的信用刊物数据
     */
    public function testViewJournalList()
    {
        $setJournalList = $this->journal();

        foreach ($setJournalList as $key => $setJournal) {
            $setJournalList[$key]['journal_id'] = $key +1;
            $setJournalList[$key]['attachment'] = json_decode($setJournal['attachment'], true);
            $setJournalList[$key]['cover'] = json_decode($setJournal['cover'], true);
            $setJournalList[$key]['auth_images'] = json_decode($setJournal['auth_images'], true);
        }

        $journalList = $this->fetchJournalList();
        $translator = new JournalDbTranslator();
        foreach ($journalList as $journal) {
            $journalArray[] = $translator->objectToArray($journal);
        }

        $this->assertEquals($journalArray, $setJournalList);

        foreach ($journalArray as $journal) {
            $this->assertEquals(2021, $journal['year']);
        }
    }
}
