<?php
namespace Base\Journal\Journal\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\UserGroup\Model\UserGroup;

use Base\Crew\Model\Crew;

use Base\Journal\SetDataTrait;
use Base\Journal\Model\Journal;
use Base\Journal\Model\Banner;
use Base\Journal\Model\JournalCategory;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Translator\JournalDbTranslator;

/**
 * @Feature: 我是拥有信用刊物管理权限的平台管理员/委办局管理员/操作用户,当我需要新增一个信用刊物时,在发布表中,新增对应的信用刊物数据
 *           通过新增信用刊物界面，并根据我所采集的信用刊物数据进行新增,以便于我维护信用刊物列表
 * @Scenario: 正常新增信用刊物数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_journal');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $journalArray = $this->journal()[0];

        $journal = new Journal();
        $journal->setTitle($journalArray['title']);
        $journal->setSource($journalArray['source']);
        $journal->setDescription($journalArray['description']);
        $journal->setCover(json_decode($journalArray['cover'], true));
        $journal->setAttachment(json_decode($journalArray['attachment'], true));
        $journal->setAuthImages(json_decode($journalArray['auth_images'], true));
        $journal->setYear($journalArray['year']);
        $journal->setCrew(new Crew($journalArray['crew_id']));
        $journal->setPublishUserGroup(new UserGroup($journalArray['publish_usergroup_id']));
        $journal->setStatus($journalArray['status']);
       
        return $journal->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setJournal = $this->journal()[0];
        $setJournal['journal_id'] = 1;
        $setJournal['cover'] = json_decode($setJournal['cover'], true);
        $setJournal['attachment'] = json_decode($setJournal['attachment'], true);
        $setJournal['auth_images'] = json_decode($setJournal['auth_images'], true);
        
        $repository = new JournalRepository();

        $journal = $repository->fetchOne($result);

        $translator = new JournalDbTranslator();
        $journalArray = $translator->objectToArray($journal);

        $this->assertEquals($journalArray, $setJournal);
    }
}
