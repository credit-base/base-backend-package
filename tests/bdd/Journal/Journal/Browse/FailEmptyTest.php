<?php
namespace Base\Journal\Journal\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Journal\Repository\JournalRepository;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,我拥有信用刊物管理权限、且当我需要查看通过审核的信用刊物的列表时,
 *           在发布表中,管理发布表中的信用刊物,通过列表与详情的形式查看到已发布的信用刊物信息, 以便于我维护信用刊物模块
 * @Scenario: 查看信用刊物数据列表-异常不存在数据
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看信用刊物数据列表时
     */
    public function fetchJournalList()
    {
        $repository = new JournalRepository();

        list($journalList, $count) = $repository->filter([]);
        
        unset($count);

        return $journalList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewJournal()
    {
        $journalList = $this->fetchJournalList();

        $this->assertEmpty($journalList);
    }
}
