<?php
namespace Base\Journal\Journal\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Journal\SetDataTrait;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Translator\JournalDbTranslator;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,我拥有信用刊物管理权限、且当我需要查看通过审核的信用刊物的列表时,
 *           在发布表中,管理发布表中的信用刊物,通过列表与详情的形式查看到已发布的信用刊物信息, 以便于我维护信用刊物模块
 * @Scenario: 查看信用刊物数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_journal');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_journal' => $this->journal(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 当我查看该条信用刊物数据详情时
     */
    public function fetchJournal($id)
    {
        $repository = new JournalRepository();

        $journal = $repository->fetchOne($id);

        return $journal;
    }

    /**
     * @Then: 我可以看见该条信用刊物数据的全部信息
     */
    public function testViewJournalList()
    {
        $id = 1;
        
        $setJournal = $this->journal()[0];
        $setJournal['journal_id'] = $id;
        $setJournal['cover'] = json_decode($setJournal['cover'], true);
        $setJournal['attachment'] = json_decode($setJournal['attachment'], true);
        $setJournal['auth_images'] = json_decode($setJournal['auth_images'], true);

        $journal = $this->fetchJournal($id);
        $translator = new JournalDbTranslator();
        $journalArray = $translator->objectToArray($journal);

        $this->assertEquals($journalArray, $setJournal);
    }
}
