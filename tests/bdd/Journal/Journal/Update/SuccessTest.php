<?php
namespace Base\Journal\Journal\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Journal\SetDataTrait;
use Base\Journal\Model\Journal;
use Base\Journal\Repository\JournalRepository;

use Base\UserGroup\Model\UserGroup;

use Base\Crew\Model\Crew;

use Base\Common\Model\IEnableAble;

/**
 * @Feature: 我是拥有信用刊物管理权限的平台管理员/委办局管理员/操作用户,当我需要编辑信用刊物信息时,在发布表中,编辑对应信用刊物数据
 *           通过编辑信用刊物界面，并根据我所采集的信用刊物数据进行编辑,以便于我可以更好的维护信用刊物管理列表
 * @Scenario: 正常编辑信用刊物数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_journal');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_journal' => $this->journal(),
            'pcore_user_group' => $this->userGroup(2),
            'pcore_crew' => $this->crew(1),
        ]);
    }

    /**
     * @When: 获取需要编辑的信用刊物数据
     */
    public function fetchJournal($id)
    {
        $repository = new JournalRepository();

        $journal = $repository->fetchOne($id);

        return $journal;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $journal = $this->fetchJournal(1);
        $journal->setTitle('测试编辑数据');
        $journal->setSource('测试编辑数据');
        $journal->setDescription('测试描述');
        $journal->setCover(array('name'=>'测试名称', 'identify'=>'测试地址.jpg'));
        $journal->setAttachment(array('name'=>'测试名称', 'identify'=>'测试地址.pdf'));
        $journal->setAuthImages(array(array('name'=>'测试名称', 'identify'=>'测试地址.jpg')));
        $journal->setYear(2021);
        $journal->setCrew(new Crew(1));
        $journal->setPublishUserGroup(new UserGroup(2));
        $journal->setStatus(IEnableAble::STATUS['DISABLED']);
        
        return $journal->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $journal = $this->fetchJournal(1);

        $this->assertEquals('测试编辑数据', $journal->getTitle());
        $this->assertEquals('测试编辑数据', $journal->getSource());
        $this->assertEquals('测试描述', $journal->getDescription());
        $this->assertEquals(2021, $journal->getYear());
        $this->assertEquals(array('name'=>'测试名称', 'identify'=>'测试地址.jpg'), $journal->getCover());
        $this->assertEquals(array('name'=>'测试名称', 'identify'=>'测试地址.pdf'), $journal->getAttachment());
        $this->assertEquals(array(array('name'=>'测试名称', 'identify'=>'测试地址.jpg')), $journal->getAuthImages());
        $this->assertEquals(1, $journal->getCrew()->getId());
        $this->assertEquals(2, $journal->getPublishUserGroup()->getId());
        $this->assertEquals(IEnableAble::STATUS['DISABLED'], $journal->getStatus());
    }
}
