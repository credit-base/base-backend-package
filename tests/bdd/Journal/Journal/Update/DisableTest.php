<?php
namespace Base\Journal\Journal\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IEnableAble;

use Base\Journal\SetDataTrait;
use Base\Journal\Repository\JournalRepository;

use Base\Crew\Model\Crew;

/**
 * @Feature: 我是拥有信用刊物发布权限的平台管理员/委办局管理员/操作用户,当我需要修改某个特定信用刊物禁用时,在发布表的操作栏中,将信用刊物修改为禁用状态
 *           通过发布表的操作栏中的禁用操作,以便于我可以更好的维护信用刊物发布管理列表
 * @Scenario: 禁用信用刊物
 */
class DisableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_journal');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在需要禁用的信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_journal' => $this->journal(),
            'pcore_crew' => $this->crew(1)
        ]);
    }

    /**
     * @When: 获取需要禁用的信用刊物数据
     */
    public function fetchJournal($id)
    {
        $repository = new JournalRepository();

        $journal = $repository->fetchOne($id);

        return $journal;
    }

    /**
     * @And: 当我调用禁用函数,期待返回true
     */
    public function disable()
    {
        $journal = $this->fetchJournal(1);
        $journal->setCrew(new Crew(1));
        
        return $journal->disable();
    }

    /**
     * @Then: 数据已经被禁用
     */
    public function testValidate()
    {
        $result = $this->disable();

        $this->assertTrue($result);

        $journal = $this->fetchJournal(1);

        $this->assertEquals(IEnableAble::STATUS['DISABLED'], $journal->getStatus());
        $this->assertEquals(Core::$container->get('time'), $journal->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $journal->getStatusTime());
        $this->assertEquals(1, $journal->getCrew()->getId());
    }
}
