<?php
namespace Base\Journal\Journal\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IEnableAble;

use Base\Journal\SetDataTrait;
use Base\Journal\Repository\JournalRepository;

use Base\Crew\Model\Crew;

/**
 * @Feature: 我是拥有信用刊物发布权限的平台管理员/委办局管理员/操作用户,当我需要修改某个特定信用刊物启用时,在发布表的操作栏中,将信用刊物修改为启用状态
 *           通过发布表的操作栏中的启用操作,以便于我可以更好的维护信用刊物发布管理列表
 * @Scenario: 启用信用刊物
 */
class EnableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_journal');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在需要启用的信用刊物
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_journal' => $this->journal(IEnableAble::STATUS['DISABLED']),
            'pcore_crew' => $this->crew(1)
        ]);
    }

    /**
     * @When: 获取需要启用的信用刊物
     */
    public function fetchJournal($id)
    {
        $repository = new JournalRepository();

        $journal = $repository->fetchOne($id);

        return $journal;
    }

    /**
     * @And: 当我调用启用函数,期待返回true
     */
    public function enable()
    {
        $journal = $this->fetchJournal(1);
        $journal->setCrew(new Crew(1));
        
        return $journal->enable();
    }

    /**
     * @Then: 数据已经被启用
     */
    public function testValidate()
    {
        $result = $this->enable();

        $this->assertTrue($result);

        $journal = $this->fetchJournal(1);

        $this->assertEquals(IEnableAble::STATUS['ENABLED'], $journal->getStatus());
        $this->assertEquals(Core::$container->get('time'), $journal->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $journal->getStatusTime());
        $this->assertEquals(1, $journal->getCrew()->getId());
    }
}
