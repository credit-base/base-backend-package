<?php
namespace Base\UserGroup\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @Feature: 我是一名分公司员工（平管/超管）,当我需要查看我的委办局数据时,在委办局管理中,可以查看到我的所有委办局数据,
 *           通过列表和详情的形式查看到我所有的委办局信息,以便于我可以了解委办局的情况
 * @Scenario: 查看委办局数据列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在委办局数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看委办局数据列表时
     */
    public function fetchUserGroupList()
    {
        $repository = new UserGroupRepository();

        list($userGroupList, $count) = $repository->filter([]);
        
        unset($count);

        return $userGroupList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewUserGroup()
    {
        $userGroupList = $this->fetchUserGroupList();

        $this->assertEmpty($userGroupList);
    }
}
