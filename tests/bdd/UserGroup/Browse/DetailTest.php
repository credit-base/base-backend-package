<?php
namespace Base\UserGroup\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;
use Base\UserGroup\Translator\UserGroupDbTranslator;

/**
 * @Feature: 我是一名分公司员工（平管/超管）,当我需要查看我的委办局数据时,在委办局管理中,可以查看到我的所有委办局数据,
 *           通过列表和详情的形式查看到我所有的委办局信息,以便于我可以了解委办局的情况
 * @Scenario: 查看委办局数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    private $userGroup;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->userGroup);
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在一条委办局数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => [
                    [
                        'user_group_id' => 1,
                        'name' => '发展和改革委员会',
                        'short_name' => '发改委',
                        'status' => UserGroup::STATUS_NORMAL,
                        'status_time' => Core::$container->get('time'),
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time')
                    ]
                ],
            ]
        );
    }

    /**
     * @When: 当我查看该条委办局数据详情时
     */
    public function fetchUserGroup($id)
    {
        $repository = new UserGroupRepository();

        $this->userGroup = $repository->fetchOne($id);

        return $this->userGroup;
    }

    /**
     * @Then 我可以看见委办局详情, 名字为发展和该和委员会, 简称为发改委, 创建时间, 更新时间.
     */
    public function testViewUserGroup()
    {
        $this->fetchUserGroup(1);

        $this->assertEquals('发展和改革委员会', $this->userGroup->getName());
        $this->assertEquals('发改委', $this->userGroup->getShortName());
        $this->assertEquals(UserGroup::STATUS_NORMAL, $this->userGroup->getStatus());
        $this->assertEquals(
            Core::$container->get('time'),
            $this->userGroup->getCreateTime()
        );
        $this->assertEquals(
            Core::$container->get('time'),
            $this->userGroup->getUpdateTime()
        );
    }
}
