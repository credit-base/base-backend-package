<?php
namespace Base\UserGroup\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;
use Base\UserGroup\Translator\UserGroupDbTranslator;

/**
 * @Feature: 我是一名分公司员工（平管/超管）,当我需要查看我的委办局数据时,在委办局管理中,可以查看到我的所有委办局数据,
 *           通过列表和详情的形式查看到我所有的委办局信息,以便于我可以了解委办局的情况
 * @Scenario: 查看委办局数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_user_group');
    }

    private function userGroup() : array
    {
        return
        [
            [
                'user_group_id' => 1,
                'name' => '发展和改革委员会',
                'short_name' => '发改委',
                'status' => UserGroup::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }

    /**
     * @Given: 存在委办局数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
            ]
        );
    }

    /**
     * @When: 当我存在委办局数据列表时
     */
    public function fetchUserGroupList()
    {
        $repository = new UserGroupRepository();

        list($userGroupList, $count) = $repository->filter([]);
        
        unset($count);

        return $userGroupList;
    }

    /**
     * @Then: 我可以查看委办局数据的全部信息
     */
    public function testViewUserGroupList()
    {
        $setUserGroupList = $this->userGroup();

        foreach ($setUserGroupList as $key => $setUserGroup) {
            unset($setUserGroup);
            $setUserGroupList[$key]['user_group_id'] = $key +1;
        }

        $userGroupList = $this->fetchUserGroupList();

        $translator = new UserGroupDbTranslator();
        foreach ($userGroupList as $userGroup) {
            $userGroupArray[] = $translator->objectToArray($userGroup);
        }

        $this->assertEquals($userGroupArray, $setUserGroupList);
    }
}
