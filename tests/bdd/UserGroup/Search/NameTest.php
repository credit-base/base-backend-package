<?php
namespace Base\UserGroup\Search;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @Feature: 我是超级管理员,当我需要查看特定条件委办局数据时,在委办局管理,可以搜索我需要的所有委办局数据
 *           通过列表的形式展现我搜索的所有委办局数据,以便于我可以快速查看到我需要的委办局数据
 * @Scenario: 通过委办局名称搜索
 */
class NameTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在一条委办局数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => [
                    [
                        'user_group_id' => 1,
                        'name' => '税务局',
                        'short_name' => '税务局',
                        'status' => UserGroup::STATUS_NORMAL,
                        'status_time' => Core::$container->get('time'),
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time')
                    ]
                ],
            ]
        );
    }

    /**
     * @When: 当我查看委办局数据列表时
     */
    public function fetchUserGroupList()
    {
        $repository = new UserGroupRepository();
 
        $filter['name'] = '税务局';

        list($userGroupList, $count) = $repository->filter($filter);
        
        unset($count);

        return $userGroupList;
    }

    /**
     * @Then: 我可以看到委办局名称中带"税务局"的所有委办局数据
     */
    public function testViewUserGroupList()
    {
        $userGroupList = $this->fetchUserGroupList();

        foreach ($userGroupList as $userGroup) {
            $this->assertEquals('税务局', $userGroup->getName());
            $this->assertEquals('税务局', $userGroup->getShortName());
            $this->assertEquals(UserGroup::STATUS_NORMAL, $userGroup->getStatus());
            $this->assertEquals(
                Core::$container->get('time'),
                $userGroup->getCreateTime()
            );
            $this->assertEquals(
                Core::$container->get('time'),
                $userGroup->getUpdateTime()
            );
        }
    }
}
