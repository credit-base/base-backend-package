<?php
namespace Base\WebsiteCustomize;

use Marmot\Core;

use Base\Crew\Model\Crew;

use Base\WebsiteCustomize\Model\WebsiteCustomize;

trait SetDataTrait
{
    protected function crew() : array
    {
        return
        [
            [
                'crew_id' => 3,
                'user_name' => '18800000002',
                'cellphone' => '18800000003',
                'real_name' => '张雯',
                'card_id' => '610424198810202425',
                'status' => 0,
                'category' => Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'],
                'purview' => json_encode(array(1,2,3,4,5)),
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'user_group_id' => 0,
                'department_id' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function websiteCustomize() : array
    {
        return
        [
            [
                'website_customize_id' => 1,
                'crew_id' => 3,
                'category' => 1,
                'content' => json_encode(array(array('content'))),
                'version' => '2108173901',
                'status' => WebsiteCustomize::STATUS['UNPUBLISHED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'website_customize_id' => 2,
                'crew_id' => 3,
                'category' => 1,
                'content' => json_encode(array(array('content'))),
                'version' => '2108173901',
                'status' => WebsiteCustomize::STATUS['PUBLISHED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
