<?php
namespace Base\WebsiteCustomize\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\WebsiteCustomize\SetDataTrait;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;

/**
 * @Feature: 我是平台管理员,我已经登录、并且已经成功设置首页的相关自定义项元素时,我进入到自定义项设置页面
 *           我可以查看其内容,通过页面的展现方式呈现, 以便于我能更好的维护网站定制页
 * @Scenario: 查看首页定制
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_website_customize');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条网站定制数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_website_customize' => $this->websiteCustomize(),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 当我查看该数据详情时
     */
    public function fetchWebsiteCustomize($id)
    {
        $repository = new WebsiteCustomizeRepository();

        $websiteCustomize = $repository->fetchOne($id);

        return $websiteCustomize;
    }

    /**
     * @Then: 我可以看见该条数据的全部信息
     */
    public function testViewWebsiteCustomizeList()
    {
        $id = 1;
        
        $setWebsiteCustomize = $this->websiteCustomize()[0];
        $setWebsiteCustomize['website_customize_id'] = $id;
        $setWebsiteCustomize['content'] = json_decode($setWebsiteCustomize['content'], true);

        $websiteCustomize = $this->fetchWebsiteCustomize($id);
        $translator = new WebsiteCustomizeDbTranslator();
        $websiteCustomizeArray = $translator->objectToArray($websiteCustomize);

        $this->assertEquals($websiteCustomizeArray, $setWebsiteCustomize);
    }
}
