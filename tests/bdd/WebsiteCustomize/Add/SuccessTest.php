<?php
namespace Base\WebsiteCustomize\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Crew\Model\Crew;

use Base\WebsiteCustomize\SetDataTrait;
use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;

/**
 * @Feature: 我是平台管理员,我拥有门户自定义项的操作权限、且当我需要修改网站内容时,在系统设置模块中,修改对应的网站内容
 *           通过网站定制界面，以便于我能更好的维护网站内容
 * @Scenario: 正常新增网站定制数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_website_customize');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $websiteCustomizeArray = $this->websiteCustomize()[0];

        $websiteCustomize = new WebsiteCustomize();
        $websiteCustomize->setCategory($websiteCustomizeArray['category']);
        $websiteCustomize->setVersion($websiteCustomizeArray['version']);
        $websiteCustomize->setStatus($websiteCustomizeArray['status']);
        $websiteCustomize->setContent(json_decode($websiteCustomizeArray['content'], true));
        $websiteCustomize->setCrew(new Crew($websiteCustomizeArray['crew_id']));
       
        return $websiteCustomize->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setWebsiteCustomize = $this->websiteCustomize()[0];
        $setWebsiteCustomize['website_customize_id'] = 1;
        $setWebsiteCustomize['content'] = json_decode($setWebsiteCustomize['content'], true);
        
        $repository = new WebsiteCustomizeRepository();

        $websiteCustomize = $repository->fetchOne($result);

        $translator = new WebsiteCustomizeDbTranslator();
        $websiteCustomizeArray = $translator->objectToArray($websiteCustomize);

        $this->assertEquals($websiteCustomizeArray, $setWebsiteCustomize);
    }
}
