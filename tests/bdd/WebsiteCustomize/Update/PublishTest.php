<?php
namespace Base\WebsiteCustomize\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IEnableAble;

use Base\WebsiteCustomize\SetDataTrait;
use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

/**
 * @Feature: 我是平台管理员,我拥有门户自定义项的操作权限、且当我需要设置门户首页主题时,在系统设置模块中
 *           我可以预览我设置成功的首页自定义元素,以便于我查看我设置的平台首页是否是符合我要求的页面
 * @Scenario: 以便于我查看我设置的平台首页是否是符合我要求的页面
 */
class PublishTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_website_customize');
    }

    /**
     * @Given: 存在需要发布的网站定制数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_website_customize' => $this->websiteCustomize(),
        ]);
    }

    /**
     * @When: 获取需要发布的网站定制数据
     */
    public function fetchWebsiteCustomize($id)
    {
        $repository = new WebsiteCustomizeRepository();

        $websiteCustomize = $repository->fetchOne($id);

        return $websiteCustomize;
    }

    /**
     * @And: 当我调用发布函数,期待返回true
     */
    public function publish()
    {
        $websiteCustomize = $this->fetchWebsiteCustomize(1);
            
        return $websiteCustomize->publish();
    }

    /**
     * @Then: 数据发布成功
     */
    public function testValidate()
    {
        $result = $this->publish();

        $this->assertTrue($result);

        $websiteCustomize = $this->fetchWebsiteCustomize(1);

        $this->assertEquals(WebsiteCustomize::STATUS['PUBLISHED'], $websiteCustomize->getStatus());
        $this->assertEquals(Core::$container->get('time'), $websiteCustomize->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $websiteCustomize->getStatusTime());
    }
}
