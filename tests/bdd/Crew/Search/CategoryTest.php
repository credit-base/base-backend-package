<?php
namespace Base\Crew\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Crew\Model\Crew;
use Base\Crew\SetDataTrait;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Translator\CrewDbTranslator;

 /**
 * @Feature: 我是超级管理员,当我需要查看某位特定员工的数据时,在员工管理列表中,我可以搜索到我需要的所有员工数据
 *           通过列表形式查看我搜索的所有员工数据 ,以便于我可以快速查找到我需要的员工数据
 * @Scenario: 通过员工类型搜索
 */
class CategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_department' => $this->department(),
        ]);
    }
    /**
     * @When: 当我查看员工数据列表时
     */
    public function fetchCrewList()
    {
        $repository = new CrewRepository();
 
        $filter['category'] = Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'];

        list($crewList, $count) = $repository->filter($filter);
        
        unset($count);

        return $crewList;
    }

    /**
     * @Then: 我可以看到员工用户类型为"委办局管理员"的员工数据
     */
    public function testViewCrewList()
    {
        $setCrewList = $this->crew();

        foreach ($setCrewList as $key => $setCrew) {
            $setCrewList[$key]['crew_id'] = $key +1;
            $setCrewList[$key]['purview'] = json_decode($setCrew['purview']);
        }

        $crewList = $this->fetchCrewList();
        $translator = new CrewDbTranslator();
        foreach ($crewList as $crew) {
            $crewArray[] = $translator->objectToArray($crew);
        }

        $this->assertEquals($crewArray, $setCrewList);

        foreach ($crewArray as $crew) {
            $this->assertEquals(Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'], $crew['category']);
        }
    }
}
