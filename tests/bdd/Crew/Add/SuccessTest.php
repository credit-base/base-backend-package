<?php
namespace Base\Crew\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\UserGroup\Model\UserGroup;

use Base\Department\Model\Department;

use Base\Crew\SetDataTrait;
use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Translator\CrewDbTranslator;

/**
 * @Feature: 我是平台管理员,当我需要新增科室时,在委办局管理下的科室管理中,新增对应的科室数据
 *           根据我归集到的科室数据新增,以便于我能更好的管理科室信息
 * @Scenario: 正常新增科室数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_department' => $this->department(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $crewArray = $this->crew()[0];

        $crew = new Crew();
        $crew->setUserGroup(new UserGroup($crewArray['user_group_id']));
        $crew->setDepartment(new Department($crewArray['department_id']));
        $crew->getDepartment()->getUserGroup()->setId($crew->getUserGroup()->getId());
        $crew->setRealName($crewArray['real_name']);
        $crew->setCellphone($crewArray['cellphone']);
        $crew->setUserName($crewArray['cellphone']);
        $crew->setPassword($crewArray['password']);
        $crew->setCategory($crewArray['category']);
        $crew->setCardId($crewArray['card_id']);
        $crew->setSalt($crewArray['salt']);
        $crew->setPurview(json_decode($crewArray['purview']));
        
        return $crew->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setCrew = $this->crew()[0];
        $setCrew['crew_id'] = 1;
        $setCrew['purview'] = json_decode($setCrew['purview']);
        
        $repository = new CrewRepository();

        $crew = $repository->fetchOne($result);

        $translator = new CrewDbTranslator();
        $crewArray = $translator->objectToArray($crew);

        $this->assertEquals($crewArray, $setCrew);
    }
}
