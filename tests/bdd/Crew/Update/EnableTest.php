<?php
namespace Base\Crew\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IEnableAble;

use Base\Crew\SetDataTrait;
use Base\Crew\Repository\CrewRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要将禁用掉的员工启用时,在员工子系统下的本级数据管理,启用对应的员工
 *           根据我核实需要启用的数据启用,以便于我实现平台员工的呈现
 * @Scenario: 启用员工
 */
class EnableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在需要启用的员工
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(IEnableAble::STATUS['DISABLED'])
        ]);
    }

    /**
     * @When: 获取需要启用的员工
     */
    public function fetchCrew($id)
    {
        $repository = new CrewRepository();

        $crew = $repository->fetchOne($id);

        return $crew;
    }

    /**
     * @And: 当我调用启用函数,期待返回true
     */
    public function enable()
    {
        $crew = $this->fetchCrew(1);
        
        return $crew->enable();
    }

    /**
     * @Then: 数据已经被启用
     */
    public function testValidate()
    {
        $result = $this->enable();

        $this->assertTrue($result);

        $crew = $this->fetchCrew(1);

        $this->assertEquals(IEnableAble::STATUS['ENABLED'], $crew->getStatus());
        $this->assertEquals(Core::$container->get('time'), $crew->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $crew->getStatusTime());
    }
}
