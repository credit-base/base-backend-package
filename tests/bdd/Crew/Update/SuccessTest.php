<?php
namespace Base\Crew\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Crew\SetDataTrait;
use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Translator\CrewDbTranslator;

use Base\UserGroup\Model\UserGroup;

use Base\Department\Model\Department;

/**
 * @Feature: 我是平台管理员,当我需要编辑员工时,在委办局管理下的员工管理中,编辑对应的员工数据
 *           根据我归集到的员工数据编辑,以便于我能更好的管理员工信息
 * @Scenario: 正常编辑员工数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_user_group' => $this->userGroup(2),
            'pcore_department' => $this->department(2),
        ]);
    }

    /**
     * @When: 获取需要编辑的员工数据
     */
    public function fetchCrew($id)
    {
        $repository = new CrewRepository();

        $crew = $repository->fetchOne($id);

        return $crew;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $crew = $this->fetchCrew(1);
        $crew->setRealName('张珂');
        $crew->setCardId('412825199009098765');
        $crew->setPurview(array(3,4,5));
        $crew->setCategory(Crew::CATEGORY['OPERATOR']);
        $crew->setUserGroup(new UserGroup(2));
        $crew->setDepartment(new Department(2));
        $crew->getDepartment()->getUserGroup()->setId($crew->getUserGroup()->getId());

        return $crew->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $crew = $this->fetchCrew(1);

        $this->assertEquals('张珂', $crew->getRealName());
        $this->assertEquals('412825199009098765', $crew->getCardId());
        $this->assertEquals(array(3,4,5), $crew->getPurview());
        $this->assertEquals(Crew::CATEGORY['OPERATOR'], $crew->getCategory());
        $this->assertEquals(2, $crew->getUserGroup()->getId());
        $this->assertEquals(2, $crew->getDepartment()->getId());
    }
}
