<?php
namespace Base\Crew\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IEnableAble;

use Base\Crew\SetDataTrait;
use Base\Crew\Repository\CrewRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要禁用员工数据时,在员工数据子系统下的本级数据管理,禁用对应的员工数据
 *           根据我核实需要禁用的数据禁用,以便于我可以更好的管理平台
 * @Scenario: 禁用员工数据
 */
class DisableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在需要禁用的员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
        ]);
    }

    /**
     * @When: 获取需要禁用的员工数据
     */
    public function fetchCrew($id)
    {
        $repository = new CrewRepository();

        $crew = $repository->fetchOne($id);

        return $crew;
    }

    /**
     * @And: 当我调用禁用函数,期待返回true
     */
    public function disable()
    {
        $crew = $this->fetchCrew(1);
        
        return $crew->disable();
    }

    /**
     * @Then: 数据已经被禁用
     */
    public function testValidate()
    {
        $result = $this->disable();

        $this->assertTrue($result);

        $crew = $this->fetchCrew(1);

        $this->assertEquals(IEnableAble::STATUS['DISABLED'], $crew->getStatus());
        $this->assertEquals(Core::$container->get('time'), $crew->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $crew->getStatusTime());
    }
}
