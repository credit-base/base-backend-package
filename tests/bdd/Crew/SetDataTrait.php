<?php
namespace Base\Crew;

use Marmot\Core;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

use Base\Department\Model\Department;

use Base\Common\Model\IEnableAble;

trait SetDataTrait
{
    protected function userGroup($id = 1) : array
    {
        return
        [
            [
                'user_group_id' => $id,
                'name' => '税务局',
                'short_name' => '税务局',
                'status' => UserGroup::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }

    protected function department($id = 1, $userGroupId = 1) : array
    {
        return
        [
            [
                'department_id' => $id,
                'name' => '财经科',
                'user_group_id' => $userGroupId,
                'status' => Department::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }

    protected function crew($status = IEnableAble::STATUS['ENABLED']) : array
    {
        return
        [
            [
                'crew_id' => 1,
                'user_name' => '18800000000',
                'cellphone' => '18800000000',
                'real_name' => '张科',
                'card_id' => '610424198810202422',
                'status' => $status,
                'category' => Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'],
                'purview' => json_encode(array(1,2,3)),
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'user_group_id' => 1,
                'department_id' => 1,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
