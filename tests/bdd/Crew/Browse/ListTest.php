<?php
namespace Base\Crew\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Crew\SetDataTrait;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Translator\CrewDbTranslator;

/**
 * @Feature: 我是超级管理员 ,当我需要查看所有员工时,进入员工管理列表,查看所有员工信息,
 *           通过列表与详情的形式查看到所有员工信息, 以便于我维护平台
 * @Scenario: 查看员工数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_department' => $this->department(),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看员工数据列表时
     */
    public function fetchCrewList()
    {
        $repository = new CrewRepository();

        list($crewList, $count) = $repository->filter([]);
        
        unset($count);

        return $crewList;
    }

    /**
     * @Then: 我可以看到员工数据的全部信息
     */
    public function testViewCrewList()
    {
        $setCrewList = $this->crew();

        foreach ($setCrewList as $key => $setCrew) {
            $setCrewList[$key]['crew_id'] = $key +1;
            $setCrewList[$key]['purview'] = json_decode($setCrew['purview']);
        }

        $crewList = $this->fetchCrewList();

        $translator = new CrewDbTranslator();
        foreach ($crewList as $crew) {
            $crewArray[] = $translator->objectToArray($crew);
        }

        $this->assertEquals($crewArray, $setCrewList);
    }
}
