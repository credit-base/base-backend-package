<?php
namespace Base\Crew\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Crew\SetDataTrait;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Translator\CrewDbTranslator;

/**
 * @Feature: 我是超级管理员 ,当我需要查看所有员工时,进入员工管理列表,查看所有员工信息,
 *           通过列表与详情的形式查看到所有员工信息, 以便于我维护平台
 * @Scenario: 查看员工数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_department' => $this->department(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 当我查看该条员工数据详情时
     */
    public function fetchCrew($id)
    {
        $repository = new CrewRepository();

        $crew = $repository->fetchOne($id);

        return $crew;
    }

    /**
     * @Then: 我可以看见该条员工数据的全部信息
     */
    public function testViewCrewList()
    {
        $id = 1;
        
        $setCrew = $this->crew()[0];
        $setCrew['crew_id'] = $id;
        $setCrew['purview'] = json_decode($setCrew['purview']);

        $crew = $this->fetchCrew($id);
        $translator = new CrewDbTranslator();
        $crewArray = $translator->objectToArray($crew);

        $this->assertEquals($crewArray, $setCrew);
    }
}
