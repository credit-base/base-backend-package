<?php
namespace Base\Crew\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Crew\Repository\CrewRepository;

/**
 * @Feature: 我是超级管理员 ,当我需要查看所有员工时,进入员工管理列表,查看所有员工信息,
 *           通过列表与详情的形式查看到所有员工信息, 以便于我维护平台
 * @Scenario: 查看员工数据列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在员工
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看员工列表时
     */
    public function fetchCrewList()
    {
        $repository = new CrewRepository();

        list($crewList, $count) = $repository->filter([]);
        
        unset($count);

        return $crewList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewCrew()
    {
        $crewList = $this->fetchCrewList();

        $this->assertEmpty($crewList);
    }
}
