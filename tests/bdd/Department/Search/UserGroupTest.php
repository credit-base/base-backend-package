<?php
namespace Base\Department\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Department\SetDataTrait;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Translator\DepartmentDbTranslator;

 /**
 * @Feature: 我是平台管理员,当我需要查看特定条件科室数据时,在政务网OA中,可以搜索我需要的所有科室数据
 *           通过列表的形式展现我搜索的所有科室数据,以便于我可以快速查看到我需要的科室数据
 * @Scenario: 通过所属委办局搜索
 */
class UserGroupTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
    }
    
    /**
     * @Given: 存在科室数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_department' => $this->department(),
            'pcore_user_group' => $this->userGroup(),
        ]);
    }

    /**
     * @When: 当我查看科室数据列表时
     */
    public function fetchDepartmentList()
    {
        $repository = new DepartmentRepository();
 
        $filter['userGroup'] = 1;

        list($departmentList, $count) = $repository->filter($filter);
        
        unset($count);

        return $departmentList;
    }

    /**
     * @Then: 我可以看到所属委办局Id为1的所有科室数据
     */
    public function testViewDepartmentList()
    {
        $setDepartmentList = $this->department();

        foreach ($setDepartmentList as $key => $setDepartment) {
            unset($setDepartment);
            $setDepartmentList[$key]['department_id'] = $key +1;
        }

        $departmentList = $this->fetchDepartmentList();
        $translator = new DepartmentDbTranslator();
        foreach ($departmentList as $department) {
            $departmentArray[] = $translator->objectToArray($department);
        }

        $this->assertEquals($departmentArray, $setDepartmentList);

        foreach ($departmentArray as $department) {
            $this->assertEquals(1, $department['user_group_id']);
        }
    }
}
