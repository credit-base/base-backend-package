<?php
namespace Base\Department\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\UserGroup\Model\UserGroup;

use Base\Department\SetDataTrait;
use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Translator\DepartmentDbTranslator;

/**
 * @Feature: 我是平台管理员,当我需要新增科室时,在委办局管理下的科室管理中,新增对应的科室数据
 *           根据我归集到的科室数据新增,以便于我能更好的管理科室信息
 * @Scenario: 正常新增科室数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $departmentArray = $this->department()[0];

        $department = new Department();
        $department->setUserGroup(new UserGroup($departmentArray['user_group_id']));
        $department->setName($departmentArray['name']);

        return $department->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setDepartment = $this->department()[0];
        $setDepartment['department_id'] = 1;
        
        $repository = new DepartmentRepository();

        $department = $repository->fetchOne($result);

        $translator = new DepartmentDbTranslator();
        $departmentArray = $translator->objectToArray($department);

        $this->assertEquals($departmentArray, $setDepartment);
    }
}
