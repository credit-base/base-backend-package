<?php
namespace Base\Department;

use Marmot\Core;

use Base\UserGroup\Model\UserGroup;

use Base\Department\Model\Department;

trait SetDataTrait
{
    protected function userGroup() : array
    {
        return
        [
            [
                'user_group_id' => 1,
                'name' => '发展和改革委员会',
                'short_name' => '发改委',
                'status' => UserGroup::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }

    protected function department() : array
    {
        return
        [
            [
                'department_id' => 1,
                'name' => '技术部',
                'user_group_id' => 1,
                'status' => Department::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }
}
