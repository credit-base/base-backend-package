<?php
namespace Base\Department\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Department\SetDataTrait;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Translator\DepartmentDbTranslator;

/**
 * @Feature: 我是平台管理员,当我需要查看所有的科室数据时,在委办局管理下的科室管理中,可以查看到所有委办局下的科室数据,
 *           通过列表和详情的形式查看到我所有的科室信息,以便于我可以管理所有的科室数据
 * @Scenario: 查看科室数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在一条科室数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_department' => $this->department(),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看该条科室数据详情时
     */
    public function fetchDepartment($id)
    {
        $repository = new DepartmentRepository();

        $department = $repository->fetchOne($id);

        return $department;
    }

    /**
     * @Then: 我可以看见该条科室数据的全部信息
     */
    public function testViewDepartmentList()
    {
        $id = 1;
        
        $setDepartment = $this->department()[0];
        $setDepartment['department_id'] = $id;

        $department = $this->fetchDepartment($id);
        $translator = new DepartmentDbTranslator();
        $departmentArray = $translator->objectToArray($department);

        $this->assertEquals($departmentArray, $setDepartment);
    }
}
