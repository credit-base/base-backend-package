<?php
namespace Base\Department\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Department\Repository\DepartmentRepository;

/**
 * @Feature: 我是平台管理员,当我需要查看所有的科室数据时,在委办局管理下的科室管理中,可以查看到所有委办局下的科室数据,
 *           通过列表和详情的形式查看到我所有的科室信息,以便于我可以管理所有的科室数据
 * @Scenario: 查看科室数据列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在科室
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看科室列表时
     */
    public function fetchDepartmentList()
    {
        $repository = new DepartmentRepository();

        list($departmentList, $count) = $repository->filter([]);
        
        unset($count);

        return $departmentList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewDepartment()
    {
        $departmentList = $this->fetchDepartmentList();

        $this->assertEmpty($departmentList);
    }
}
