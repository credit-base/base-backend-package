<?php
namespace Base\Department\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\UserGroup\Model\UserGroup;

use Base\Department\SetDataTrait;
use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;

/**
 * @Feature: 我是平台管理员,当我需要编辑科室时,在委办局管理下的科室管理中,编辑对应的科室数据
 *           根据我归集到的科室数据编辑,以便于我能更好的管理科室信息
 * @Scenario: 异常流程-数据重复,编辑失败
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我已经编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_department' => [
                [
                    'department_id' => 1,
                    'name' => '技术部',
                    'user_group_id' => 1,
                    'status' => Department::STATUS_NORMAL,
                    'status_time' => 0,
                    'create_time' => Core::$container->get('time'),
                    'update_time' => Core::$container->get('time')
                ],
                [
                    'department_id' => 2,
                    'name' => '财经科',
                    'user_group_id' => 1,
                    'status' => Department::STATUS_NORMAL,
                    'status_time' => 0,
                    'create_time' => Core::$container->get('time'),
                    'update_time' => Core::$container->get('time')
                ]
            ]
        ]);
    }

    /**
     * @When: 获取需要编辑的科室数据
     */
    public function fetchDepartment($id)
    {
        $repository = new DepartmentRepository();

        $department = $repository->fetchOne($id);

        return $department;
    }
    /**
     * @When: 当我调用编辑函数,期待返回false
     */
    public function edit()
    {
        $department = $this->fetchDepartment(1);
        $department->setName('财经科');

        return $department->edit();
    }

    /**
     * @Then: 数据已经存在
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertFalse($result);
    }
}
