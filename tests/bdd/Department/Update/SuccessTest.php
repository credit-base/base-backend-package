<?php
namespace Base\Department\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Department\SetDataTrait;
use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Translator\DepartmentDbTranslator;

/**
 * @Feature: 我是平台管理员,当我需要编辑科室时,在委办局管理下的科室管理中,编辑对应的科室数据
 *           根据我归集到的科室数据编辑,以便于我能更好的管理科室信息
 * @Scenario: 正常编辑科室数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_department' => $this->department(),
            'pcore_user_group' => $this->userGroup(),
        ]);
    }

    /**
     * @When: 获取需要编辑的科室数据
     */
    public function fetchDepartment($id)
    {
        $repository = new DepartmentRepository();

        $department = $repository->fetchOne($id);

        return $department;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $department = $this->fetchDepartment(1);
        $department->setName('市场部');

        return $department->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $department = $this->fetchDepartment(1);

        $this->assertEquals('市场部', $department->getName());
    }
}
