<?php
namespace Base\Member\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait;
use Base\Member\Repository\MemberRepository;
use Base\Member\Translator\MemberDbTranslator;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户 ,当我需要通过OA管理门户网注册的用户们时,我可以查看门户网注册的用户的具体信息,
 *           通过在门户网用户管理列表中进行查看、启用禁用操作, 以便于我可以通过查看与禁用的操作快速处理应急问题
 * @Scenario: 查看用户数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在一条用户数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member()
        ]);
    }

    /**
     * @When: 当我查看该条用户数据详情时
     */
    public function fetchMember($id)
    {
        $repository = new MemberRepository();

        $member = $repository->fetchOne($id);

        return $member;
    }

    /**
     * @Then: 我可以看见该条用户数据的全部信息
     */
    public function testViewMemberList()
    {
        $id = 1;
        
        $setMember = $this->member()[0];
        $setMember['member_id'] = $id;

        $member = $this->fetchMember($id);
        $translator = new MemberDbTranslator();
        $memberArray = $translator->objectToArray($member);

        $this->assertEquals($memberArray, $setMember);
    }
}
