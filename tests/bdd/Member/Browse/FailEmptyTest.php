<?php
namespace Base\Member\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\Repository\MemberRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户 ,当我需要通过OA管理门户网注册的用户们时,我可以查看门户网注册的用户的具体信息,
 *           通过在门户网用户管理列表中进行查看、启用禁用操作, 以便于我可以通过查看与禁用的操作快速处理应急问题
 * @Scenario: 查看用户数据列表-异常-数据不存在
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在用户
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看用户列表时
     */
    public function fetchMemberList()
    {
        $repository = new MemberRepository();

        list($memberList, $count) = $repository->filter([]);
        
        unset($count);

        return $memberList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewMember()
    {
        $memberList = $this->fetchMemberList();

        $this->assertEmpty($memberList);
    }
}
