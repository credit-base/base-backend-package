<?php
namespace Base\Member\SignIn;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait;
use Base\Member\Repository\MemberRepository;
use Base\Member\Translator\MemberDbTranslator;

 /**
 * @Feature: 我是信用网站门户网站的用户,当我需要了解信用网站门户中需要登录才可以查看的内容时,在门户网用户登录页面,登录自己在门户网的账号
 *           通过输入用户名、密码并拖动滑条 ,以便于信用网站区分不同类型的用户
 * @Scenario: 登录-异常流程,登录失败
 */
class FailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在用户数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
        ]);
    }
    /**
     * @When: 通过获取到的用户名查询用户数据
     */
    public function fetchMemberList()
    {
        $repository = new MemberRepository();
 
        $filter['userName'] = '18800000001';

        list($memberList, $count) = $repository->filter($filter);
        
        unset($count);

        return $memberList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewMemberList()
    {
        $memberList = $this->fetchMemberList();

        $this->assertEmpty($memberList);
    }
}
