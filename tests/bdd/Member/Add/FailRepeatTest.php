<?php
namespace Base\Member\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait;
use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;

/**
 * @Feature: 我是信用网站门户网站的用户,当我需要为自己创建一个账号时,在门户网用户注册页面,注册一个门户网用户账号
 *           通过输入网页展示的信息,以便于我可以在信用网站上查询到更多的相关信息
 * @Scenario: 异常流程-数据重复,新增失败
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 我已经新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member()
        ]);
    }

    /**
     * @When: 当我调用添加函数
     */
    public function add()
    {
        $memberArray = $this->member()[0];

        $member = new Member();
        
        $member->setUserName($memberArray['user_name']);
        $member->setRealName($memberArray['real_name']);
        $member->setCellphone($memberArray['cellphone']);
        $member->setEmail($memberArray['email']);
        $member->setCardId($memberArray['cardid']);
        $member->setGender($memberArray['gender']);
        $member->setPassword($memberArray['password']);
        $member->setSalt($memberArray['salt']);
        $member->setContactAddress($memberArray['contact_address']);
        $member->setSecurityQuestion($memberArray['security_question']);
        $member->setSecurityAnswer($memberArray['security_answer']);

        return $member->add();
    }

    /**
     * @Then: 期望数据返回失败
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertFalse($result);
    }
}
