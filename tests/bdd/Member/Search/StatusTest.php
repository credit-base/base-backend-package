<?php
namespace Base\Member\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait;
use Base\Member\Repository\MemberRepository;
use Base\Member\Translator\MemberDbTranslator;

use Base\Common\Model\IEnableAble;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当我需要通过OA管理门户网注册的用户们时,我可以通过条件搜索门户网注册的用户的信息,
 *           通过在门户网用户管理列表中进行搜索操作 ,以便于我可以通过快速定位某个门户网用户的账号
 * @Scenario: 通过状态搜索
 */
class StatusTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member()
        ]);
    }
    /**
     * @When: 当我查看员工数据列表时
     */
    public function fetchMemberList()
    {
        $repository = new MemberRepository();
 
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        list($memberList, $count) = $repository->filter($filter);
        
        unset($count);

        return $memberList;
    }

    /**
     * @Then: 我可以看到员工用户状态为"启用"的员工数据
     */
    public function testViewMemberList()
    {
        $setMemberList = $this->member();

        foreach ($setMemberList as $key => $setMember) {
            unset($setMember);
            $setMemberList[$key]['member_id'] = $key +1;
        }

        $memberList = $this->fetchMemberList();
        $translator = new MemberDbTranslator();
        foreach ($memberList as $member) {
            $memberArray[] = $translator->objectToArray($member);
        }

        $this->assertEquals($memberArray, $setMemberList);

        foreach ($memberArray as $member) {
            $this->assertEquals(IEnableAble::STATUS['ENABLED'], $member['status']);
        }
    }
}
