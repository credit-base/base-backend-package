<?php
namespace Base\Member;

use Marmot\Core;

use Base\Member\Model\Member;

use Base\Common\Model\IEnableAble;

trait SetDataTrait
{
    protected function member($status = IEnableAble::STATUS['ENABLED']) : array
    {
        return
        [
            [
                'member_id' => 1,
                'user_name' => '18800000000',
                'real_name' => '张科',
                'cellphone' => '18800000000',
                'email' => '997934308@qq.com',
                'cardid' => '610424198810202422',
                'gender' => Member::GENDER['MALE'],
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'contact_address' => '雁塔区长延堡街道',
                'security_question' => 5,
                'security_answer' => '黑色',
                'status' => $status,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
