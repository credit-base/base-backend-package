<?php
namespace Base\Member\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IEnableAble;

use Base\Member\SetDataTrait;
use Base\Member\Repository\MemberRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当我需要使OA管理门户网注册的用户账号失效时,在门户网用户管理列表,可以禁用某个用户的账号
 *           通过门户网用户管理列表中的禁用操作,以便于我管理门户网用户列表
 * @Scenario: 禁用用户数据
 */
class DisableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在需要禁用的用户数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 获取需要禁用的用户数据
     */
    public function fetchMember($id)
    {
        $repository = new MemberRepository();

        $member = $repository->fetchOne($id);

        return $member;
    }

    /**
     * @And: 当我调用禁用函数,期待返回true
     */
    public function disable()
    {
        $member = $this->fetchMember(1);
        
        return $member->disable();
    }

    /**
     * @Then: 数据已经被禁用
     */
    public function testValidate()
    {
        $result = $this->disable();

        $this->assertTrue($result);

        $member = $this->fetchMember(1);

        $this->assertEquals(IEnableAble::STATUS['DISABLED'], $member->getStatus());
        $this->assertEquals(Core::$container->get('time'), $member->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $member->getStatusTime());
    }
}
