<?php
namespace Base\Member\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IEnableAble;

use Base\Member\SetDataTrait;
use Base\Member\Repository\MemberRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当我需要使OA管理门户网注册的用户账号生效时,在门户网用户管理列表,可以启用某个用户的账号
 *           通过门户网用户管理列表中的启用操作,以便于我管理门户网用户列表
 * @Scenario: 启用用户数据
 */
class EnableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在需要启用的用户
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(IEnableAble::STATUS['DISABLED'])
        ]);
    }

    /**
     * @When: 获取需要启用的用户
     */
    public function fetchMember($id)
    {
        $repository = new MemberRepository();

        $member = $repository->fetchOne($id);

        return $member;
    }

    /**
     * @And: 当我调用启用函数,期待返回true
     */
    public function enable()
    {
        $member = $this->fetchMember(1);
        
        return $member->enable();
    }

    /**
     * @Then: 数据已经被启用
     */
    public function testValidate()
    {
        $result = $this->enable();

        $this->assertTrue($result);

        $member = $this->fetchMember(1);

        $this->assertEquals(IEnableAble::STATUS['ENABLED'], $member->getStatus());
        $this->assertEquals(Core::$container->get('time'), $member->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $member->getStatusTime());
    }
}
