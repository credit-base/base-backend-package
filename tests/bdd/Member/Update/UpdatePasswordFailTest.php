<?php
namespace Base\Member\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait;
use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;
use Base\Member\Translator\MemberDbTranslator;

/**
 * @Feature: 我是信用网站门户网站的用户,当我需要修改密码时,在用户中心页面,可以进行修改密码操作
 *           通过输入账号的旧密码、并输入修改后的密码及确认密码,以便于用户可以在个人中心就修改密码不用每次都对账号的密码进行重置
 * @Scenario: 异常流程-旧密码不正确,修改密码失败
 */
class UpdatePasswordFailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 获取需要修改密码的用户数据
     */
    public function fetchMember($id)
    {
        $repository = new MemberRepository();

        $member = $repository->fetchOne($id);

        return $member;
    }
    /**
     * @When: 当我调用修改密码函数,期待返回true
     */
    public function changePassword($oldPassword, $newPassword)
    {
        $member = $this->fetchMember(1);

        return $member->changePassword($oldPassword, $newPassword);
    }

    /**
     * @Then: 验证密码是否修改成功
     */
    public function testValidate()
    {
        $newPassword = 'Admin1234$';
        $oldPassword = 'Admin1234$';

        $result = $this->changePassword($oldPassword, $newPassword);

        $this->assertFalse($result);
    }
}
