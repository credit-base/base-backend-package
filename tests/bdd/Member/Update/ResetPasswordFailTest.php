<?php
namespace Base\Member\SignIn;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait;
use Base\Member\Repository\MemberRepository;
use Base\Member\Translator\MemberDbTranslator;

/**
 * @Feature: 我是信用网站门户网站的用户,当我忘记了我的密码时,在门户网用户忘记密码页面,找回自己的密码
 *           根据注册时所选择的密保问题与填写的密保答案,以便于我在忘记密码时，重置自己的密码
 * @Scenario: 异常流程-用户名不存在,重置密码失败
 */
class ResetPasswordFailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在用户数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
        ]);
    }
    /**
     * @When: 通过获取到的用户名查询用户数据
     */
    public function fetchMemberList()
    {
        $repository = new MemberRepository();
 
        $filter['userName'] = '18800000002';

        list($members, $count) = $repository->filter($filter);
        
        unset($count);

        return $members;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewMemberList()
    {
        $members = $this->fetchMemberList();

        $this->assertEmpty($members);
    }
}
