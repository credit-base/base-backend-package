<?php
namespace Base\Member\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait;
use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;
use Base\Member\Translator\MemberDbTranslator;

/**
 * @Feature: 我是信用网站门户网站的用户,当我需要对我的个人信息进行修改时,在用户中心页面,可以修改个人信息
 *           通过点击修改个人信息按钮，并进入修改个人信息页面,以便于我及时修改我的个人信息
 * @Scenario: 正常编辑用户数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 获取需要编辑的用户数据
     */
    public function fetchMember($id)
    {
        $repository = new MemberRepository();

        $member = $repository->fetchOne($id);

        return $member;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $member = $this->fetchMember(1);
        $member->setGender(Member::GENDER['FEMALE']);

        return $member->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $member = $this->fetchMember(1);

        $this->assertEquals(Member::GENDER['FEMALE'], $member->getGender());
    }
}
