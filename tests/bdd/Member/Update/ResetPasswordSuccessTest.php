<?php
namespace Base\Member\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait;
use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;
use Base\Member\Translator\MemberDbTranslator;

/**
 * @Feature: 我是信用网站门户网站的用户,当我忘记了我的密码时,在门户网用户忘记密码页面,找回自己的密码
 *           根据注册时所选择的密保问题与填写的密保答案,以便于我在忘记密码时，重置自己的密码
 * @Scenario: 重置密码成功
 */
class ResetPasswordSuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 通过用户填写的用户名获取需要重置密码的用户数据
     */
    public function fetchMember()
    {
        $repository = new MemberRepository();
 
        $filter['userName'] = '18800000000';

        list($memberList, $count) = $repository->filter($filter);
        
        unset($count);

        foreach ($memberList as $member) {
            $member = $member;
        }

        return $member;
    }
    /**
     * @When: 当我调用重置密码函数,期待返回true
     */
    public function resetPassword($newPassword)
    {
        $member = $this->fetchMember();

        return $member->resetPassword($newPassword);
    }

    /**
     * @Then: 验证密码是否修改成功
     */
    public function testValidate()
    {
        $newPassword = 'Admin1234$';

        $result = $this->resetPassword($newPassword);

        $this->assertTrue($result);
        
        $member = $this->fetchMember(1);

        $this->assertEquals(md5(md5($newPassword).$member->getSalt()), $member->getPassword());
    }
}
