<?php
namespace Base\CreditPhotography\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\CreditPhotography\Repository\CreditPhotographyRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当有审核状态为已通过的信用随手拍数据时,我可以在OA的信用随手拍审核的发布表查看该条数据
 *           我可以查看其内容,通过列表中已展现的数据内容, 以便于我了解详情
 * @Scenario: 查看信用随手拍列表-异常数据不存在
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在信用随手拍数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看信用随手拍数据列表时
     */
    public function fetchCreditPhotographyList()
    {
        $repository = new CreditPhotographyRepository();

        list($creditPhotographyList, $count) = $repository->filter([]);
        
        unset($count);

        return $creditPhotographyList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewCreditPhotography()
    {
        $creditPhotographyList = $this->fetchCreditPhotographyList();

        $this->assertEmpty($creditPhotographyList);
    }
}
