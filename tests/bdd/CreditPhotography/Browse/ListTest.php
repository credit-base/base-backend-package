<?php
namespace Base\CreditPhotography\CreditPhotography\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\CreditPhotography\SetDataTrait;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当有审核状态为已通过的信用随手拍数据时,我可以在OA的信用随手拍审核的发布表查看该条数据
 *           我可以查看其内容,通过列表中已展现的数据内容, 以便于我了解详情
 * @Scenario: 查看信用随手拍列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_credit_photography');
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在信用随手拍数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_credit_photography' => $this->creditPhotography(),
            'pcore_member' => $this->member()
        ]);
    }

    /**
     * @When: 当我查看信用随手拍数据列表时
     */
    public function fetchCreditPhotographyList()
    {
        $repository = new CreditPhotographyRepository();

        list($creditPhotographyList, $count) = $repository->filter([]);
        
        unset($count);

        return $creditPhotographyList;
    }

    /**
     * @Then: 我可以看到信用随手拍数据的全部信息
     */
    public function testViewCreditPhotographyList()
    {
        $setCreditPhotographyList = $this->creditPhotography();

        foreach ($setCreditPhotographyList as $key => $setCreditPhotography) {
            $setCreditPhotographyList[$key]['credit_photography_id'] = $key +1;
            $setCreditPhotographyList[$key]['attachments'] = json_decode($setCreditPhotography['attachments'], true);
        }

        $creditPhotographyList = $this->fetchCreditPhotographyList();

        $translator = new CreditPhotographyDbTranslator();
        foreach ($creditPhotographyList as $creditPhotography) {
            $creditPhotographyArray[] = $translator->objectToArray($creditPhotography);
        }

        $this->assertEquals($creditPhotographyArray, $setCreditPhotographyList);
    }
}
