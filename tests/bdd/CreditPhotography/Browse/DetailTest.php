<?php
namespace Base\CreditPhotography\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\CreditPhotography\SetDataTrait;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当有审核状态为已通过的信用随手拍数据时,我可以在OA的信用随手拍审核的发布表查看该条数据
 *           我可以查看其内容,通过列表中已展现的数据内容, 以便于我了解详情
 * @Scenario: 查看信用随手拍数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_credit_photography');
        $this->clear('pcore_member');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条信用随手拍数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_credit_photography' => $this->creditPhotography(),
            'pcore_member' => $this->member(),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 当我查看该条信用随手拍数据详情时
     */
    public function fetchCreditPhotography($id)
    {
        $repository = new CreditPhotographyRepository();

        $creditPhotography = $repository->fetchOne($id);

        return $creditPhotography;
    }

    /**
     * @Then: 我可以看见该条信用随手拍数据的全部信息
     */
    public function testViewCreditPhotographyList()
    {
        $id = 1;
        
        $setCreditPhotography = $this->creditPhotography()[0];
        $setCreditPhotography['credit_photography_id'] = $id;
        $setCreditPhotography['attachments'] = json_decode($setCreditPhotography['attachments'], true);

        $creditPhotography = $this->fetchCreditPhotography($id);
        $translator = new CreditPhotographyDbTranslator();
        $creditPhotographyArray = $translator->objectToArray($creditPhotography);

        $this->assertEquals($creditPhotographyArray, $setCreditPhotography);
    }
}
