<?php
namespace Base\CreditPhotography\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IEnableAble;

use Base\CreditPhotography\SetDataTrait;
use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;

/**
 * @Feature: 我是前台用户,当我需要删除信用随手拍数据时,在个人中心信用随手拍类别,删除对应的信用随手拍数据
 *           根据我核实需要删除的数据删除,以便于我可以更好的管理数据
 * @Scenario: 删除信用随手拍数据
 */
class DeleteTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_credit_photography');
    }

    /**
     * @Given: 存在需要删除的信用随手拍数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_credit_photography' => $this->creditPhotography(),
        ]);
    }

    /**
     * @When: 获取需要删除的信用随手拍数据
     */
    public function fetchCreditPhotography($id)
    {
        $repository = new CreditPhotographyRepository();

        $creditPhotography = $repository->fetchOne($id);

        return $creditPhotography;
    }

    /**
     * @And: 当我调用删除函数,期待返回true
     */
    public function delete()
    {
        $creditPhotography = $this->fetchCreditPhotography(1);
        
        return $creditPhotography->delete();
    }

    /**
     * @Then: 数据已经被删除
     */
    public function testValidate()
    {
        $result = $this->delete();

        $this->assertTrue($result);

        $creditPhotography = $this->fetchCreditPhotography(1);

        $this->assertEquals(CreditPhotography::STATUS['DELETED'], $creditPhotography->getStatus());
        $this->assertEquals(Core::$container->get('time'), $creditPhotography->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $creditPhotography->getStatusTime());
    }
}
