<?php
namespace Base\CreditPhotography\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;

use Base\CreditPhotography\SetDataTrait;
use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;

use Base\Crew\Model\Crew;

/**
 * @Feature: 我是拥有信用随手拍审核权限的平台管理员/委办局管理员/操作用户,当我需要审核一个信用随手拍时,在审核表中,审核待审核的信用随手拍数据
 *           通过信用随手拍详情页面的审核通过与审核驳回操作,以便于我维护信用随手拍列表
 * @Scenario: 审核通过
 */
class ApproveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_credit_photography');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条待审核的信用随手拍审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_credit_photography' => $this->creditPhotography(),
            'pcore_crew' => $this->crew(1)
        ]);
    }

    /**
     * @When: 获取需要审核通过的信用随手拍
     */
    public function fetchCreditPhotography($id)
    {
        $repository = new CreditPhotographyRepository();

        $creditPhotography = $repository->fetchOne($id);

        return $creditPhotography;
    }

    /**
     * @And: 当我调用审核通过函数,期待返回true
     */
    public function approve()
    {
        $creditPhotography = $this->fetchCreditPhotography(1);
        $creditPhotography->setApplyCrew(new Crew(1));
        
        return $creditPhotography->approve();
    }

    /**
     * @Then: 数据已经被审核通过
     */
    public function testValidate()
    {
        $result = $this->approve();

        $this->assertTrue($result);

        $creditPhotography = $this->fetchCreditPhotography(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['APPROVE'], $creditPhotography->getApplyStatus());
        $this->assertEquals(Core::$container->get('time'), $creditPhotography->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $creditPhotography->getStatusTime());
        $this->assertEquals(1, $creditPhotography->getApplyCrew()->getId());
    }
}
