<?php
namespace Base\CreditPhotography;

use Marmot\Core;

use Base\Crew\Model\Crew;

use Base\Member\Model\Member;

use Base\CreditPhotography\Model\CreditPhotography;

use Base\Common\Model\IApproveAble;

trait SetDataTrait
{
    protected function crew($id = 2) : array
    {
        return
        [
            [
                'crew_id' => $id,
                'user_name' => '18800000001',
                'cellphone' => '18800000001',
                'real_name' => '张正科',
                'card_id' => '610424198810202423',
                'status' => 0,
                'category' => Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'],
                'purview' => json_encode(array(1,2,3,4,5)),
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'user_group_id' => 0,
                'department_id' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function member() : array
    {
        return
        [
            [
                'member_id' => 2,
                'user_name' => '18800000001',
                'real_name' => '白玉',
                'cellphone' => '18800000001',
                'email' => '997934309@qq.com',
                'cardid' => '610424198810202422',
                'gender' => Member::GENDER['MALE'],
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'contact_address' => '雁塔区长延堡街道',
                'security_question' => 4,
                'security_answer' => '黑色',
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function creditPhotography() : array
    {
        return
        [
            [
                'credit_photography_id' => 1,
                'description' => '信用随手拍描述',
                'attachments' => json_encode(array(array('name'=>'附件名称', 'identify'=>'附件地址'))),
                'member_id' => 2,
                'real_name' => '白玉',
                'status' => CreditPhotography::STATUS['NORMAL'],
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'apply_crew_id' => 0,
                'reject_reason' => '',
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
