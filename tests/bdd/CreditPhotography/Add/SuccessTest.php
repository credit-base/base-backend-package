<?php
namespace Base\CreditPhotography\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\Model\Member;

use Base\CreditPhotography\SetDataTrait;
use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;

/**
 * @Feature: 我是前台用户,当我需要新增一个信用随手拍时,在门户信用随手拍栏目中,新增对应的信用随手拍数据
 *           通过新增信用随手拍界面，并根据我所采集的信用随手拍数据进行新增,以便于我维护信用随手拍列表
 * @Scenario: 正常新增信用随手拍数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_credit_photography');
        $this->clear('pcore_member');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $creditPhotographyArray = $this->creditPhotography()[0];

        $creditPhotography = new CreditPhotography();
        $creditPhotography->setDescription($creditPhotographyArray['description']);
        $creditPhotography->setAttachments(json_decode($creditPhotographyArray['attachments'], true));
        $creditPhotography->setMember(new Member($creditPhotographyArray['member_id']));
       
        return $creditPhotography->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setCreditPhotography = $this->creditPhotography()[0];
        $setCreditPhotography['credit_photography_id'] = 1;
        $setCreditPhotography['attachments'] = json_decode($setCreditPhotography['attachments'], true);
        
        $repository = new CreditPhotographyRepository();

        $creditPhotography = $repository->fetchOne($result);

        $translator = new CreditPhotographyDbTranslator();
        $creditPhotographyArray = $translator->objectToArray($creditPhotography);

        $this->assertEquals($creditPhotographyArray, $setCreditPhotography);
    }
}
