<?php
namespace Base\News;

use Marmot\Core;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\Model\UnAuditedNews;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

trait SetDataTrait
{
    protected function userGroup($id = 1) : array
    {
        return
        [
            [
                'user_group_id' => $id,
                'name' => '税务局',
                'short_name' => '税务局',
                'status' => UserGroup::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }

    protected function crew($id = 2) : array
    {
        return
        [
            [
                'crew_id' => $id,
                'user_name' => '18800000000',
                'cellphone' => '18800000000',
                'real_name' => '张正科',
                'card_id' => '610424198810202422',
                'status' => 0,
                'category' => Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'],
                'purview' => json_encode(array(1,2,3)),
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'user_group_id' => 0,
                'department_id' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function news(
        $status = IEnableAble::STATUS['ENABLED'],
        $stick = ITopAble::STICK['DISABLED']
    ) : array {
        return
        [
            [
                'news_id' => 1,
                'title' => '新闻标题',
                'source' => '新闻来源',
                'cover' => json_encode(array('name'=>'封面名称', 'identify'=>'封面地址')),
                'attachments' => json_encode(array(array('name'=>'附件名称', 'identify'=>'附件地址'))),
                'content' => '新闻内容',
                'description' => '新闻简介',
                'parent_category' => 0,
                'category' => 1,
                'type' => 1,
                'crew_id' => 2,
                'publish_usergroup_id' => 1,
                'stick' => $stick,
                'status' => $status,
                'dimension' => News::DIMENSIONALITY['SOCIOLOGY'],
                'banner_status' => Banner::BANNER_STATUS['DISABLED'],
                'banner_image' => json_encode(array('name'=>'轮播图图片名称', 'identify'=>'轮播图图片地址')),
                'home_page_show_status' => News::HOME_PAGE_SHOW_STATUS['DISABLED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyForm(
        $applyStatus = IApproveAble::APPLY_STATUS['APPROVE'],
        $operationType = IApproveAble::OPERATION_TYPE['ADD']
    ) : array {
        return
        [
            [
                'apply_form_id' => 1,
                'title' => '标题',
                'relation_id' => 2,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 2,
                'operation_type' => $operationType,
                'apply_info' => base64_encode(gzcompress(serialize(array('apply_info')))),
                'reject_reason' => '驳回原因',
                'apply_info_category' => UnAuditedNews::APPLY_NEWS_CATEGORY,
                'apply_info_type' => 1,
                'apply_status' => $applyStatus,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
