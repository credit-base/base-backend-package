<?php
namespace Base\News\UnAuditedNews\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\SetDataTrait;
use Base\News\Repository\UnAuditedNewsRepository;
use Base\News\Translator\UnAuditedNewsDbTranslator;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有新闻发布权限、且当我需要查看通过审核的新闻的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在新闻审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_apply_form' => $this->applyForm(),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看新闻审核数据列表时
     */
    public function fetchUnAuditedNewsList()
    {
        $repository = new UnAuditedNewsRepository();

        list($unAuditedNewsList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedNewsList;
    }

    /**
     * @Then: 我可以看到新闻数据的全部信息
     */
    public function testViewNewsList()
    {
        $setUnAuditedNewsList = $this->applyForm();

        foreach ($setUnAuditedNewsList as $key => $setUnAuditedNews) {
            unset($setUnAuditedNews);
            $setUnAuditedNewsList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedNewsList = $this->fetchUnAuditedNewsList();

        $translator = new UnAuditedNewsDbTranslator();
        foreach ($unAuditedNewsList as $unAuditedNews) {
            $unAuditedNewsArray[] = $translator->objectToArray($unAuditedNews);
        }

        $this->assertEquals($unAuditedNewsArray, $setUnAuditedNewsList);
    }
}
