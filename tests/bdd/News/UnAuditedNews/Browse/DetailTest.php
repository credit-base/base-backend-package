<?php
namespace Base\News\UnAuditedNews\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\SetDataTrait;
use Base\News\Repository\UnAuditedNewsRepository;
use Base\News\Translator\UnAuditedNewsDbTranslator;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户,我拥有新闻发布权限、且当我需要查看未审核或已驳回的新闻列表时,
 *           在审核表中,通过列表与详情的形式查看到未审核或已驳回的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻审核数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条新闻审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyForm(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 当我查看该条新闻审核数据详情时
     */
    public function fetchUnAuditedNews($id)
    {
        $repository = new UnAuditedNewsRepository();

        $unAuditedNews = $repository->fetchOne($id);

        return $unAuditedNews;
    }

    /**
     * @Then: 我可以看见该条新闻审核数据的全部信息
     */
    public function testViewUnAuditedNewsList()
    {
        $id = 1;
        
        $setUnAuditedNews = $this->applyForm()[0];
        $setUnAuditedNews['apply_form_id'] = $id;

        $unAuditedNews = $this->fetchUnAuditedNews($id);
        $translator = new UnAuditedNewsDbTranslator();
        $unAuditedNewsArray = $translator->objectToArray($unAuditedNews);

        $this->assertEquals($unAuditedNewsArray, $setUnAuditedNews);
    }
}
