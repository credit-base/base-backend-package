<?php
namespace Base\News\UnAuditedNews\Add;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

use Base\News\SetDataTrait;
use Base\News\Model\UnAuditedNews;
use Base\News\Repository\UnAuditedNewsRepository;
use Base\News\Translator\UnAuditedNewsDbTranslator;

use Base\Common\Model\IApproveAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要移动新闻时,在发布表中,移动对应新闻数据
 *           通过移动新闻界面，并根据我所采集的新闻数据进行移动,以便于我可以更好的维护新闻管理列表
 * @Scenario: 移动新闻分类,需要审核
 */
class MoveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在需要移动的新闻
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(),
        ]);
    }

    /**
     * @When: 当我调用审核的移动函数,期待返回true
     */
    public function move()
    {
        $applyFormArray = $this->applyForm(
            IApproveAble::APPLY_STATUS['PENDING'],
            IApproveAble::OPERATION_TYPE['MOVE']
        )[0];

        $unAuditedNews = new UnAuditedNews();

        $unAuditedNews->setApplyTitle($applyFormArray['title']);
        $unAuditedNews->setRelation(new Crew($applyFormArray['relation_id']));
        $unAuditedNews->setApplyCrew(new Crew($applyFormArray['apply_crew_id']));
        $unAuditedNews->setApplyUserGroup(new UserGroup($applyFormArray['apply_usergroup_id']));
        $unAuditedNews->setOperationType($applyFormArray['operation_type']);
        $unAuditedNews->setApplyInfo(unserialize(gzuncompress(base64_decode($applyFormArray['apply_info'], true))));
        $unAuditedNews->setRejectReason($applyFormArray['reject_reason']);

        $unAuditedNews->setApplyInfoCategory(
            new ApplyInfoCategory(UnAuditedNews::APPLY_NEWS_CATEGORY, $applyFormArray['apply_info_category'])
        );

        return $unAuditedNews->move();
    }

    /**
     * @Then: 可以查到新增的审核数据
     */
    public function testValidate()
    {
        $result = $this->move();

        $this->assertTrue($result);

        $setUnAuditedNews = $this->applyForm(
            IApproveAble::APPLY_STATUS['PENDING'],
            IApproveAble::OPERATION_TYPE['MOVE']
        )[0];
        $setUnAuditedNews['apply_form_id'] = 1;
        
        $repository = new UnAuditedNewsRepository();

        $unAuditedNews = $repository->fetchOne($result);

        $translator = new UnAuditedNewsDbTranslator();
        $unAuditedNewsArray = $translator->objectToArray($unAuditedNews);

        $this->assertEquals($unAuditedNewsArray, $setUnAuditedNews);
    }
}
