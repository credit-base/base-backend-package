<?php
namespace Base\News\UnAuditedNews\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\SetDataTrait;
use Base\News\Model\UnAuditedNews;
use Base\News\Repository\UnAuditedNewsRepository;
use Base\News\Translator\UnAuditedNewsDbTranslator;

use Base\Crew\Model\Crew;

use Base\Common\Model\IApproveAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑新闻信息时,在审核表中,编辑已驳回新闻数据
 *           通过编辑新闻界面，并根据我所采集的新闻数据进行编辑,以便于我可以更好的维护新闻管理列表
 * @Scenario: 正常编辑新闻审核数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyForm(IApproveAble::APPLY_STATUS['REJECT']),
            'pcore_crew' => $this->crew(3),
        ]);
    }

    /**
     * @When: 获取需要编辑的新闻数据
     */
    public function fetchUnAuditedNews($id)
    {
        $repository = new UnAuditedNewsRepository();

        $unAuditedNews = $repository->fetchOne($id);

        return $unAuditedNews;
    }
    /**
     * @When: 当我调用重新编辑函数,期待返回true
     */
    public function resubmit()
    {
        $unAuditedNews = $this->fetchUnAuditedNews(1);

        $unAuditedNews->setApplyTitle('测试重新编辑');
        $unAuditedNews->setRelation(new Crew(3));
        $unAuditedNews->setApplyInfo(array('测试重新编辑'));
        $unAuditedNews->setApplyInfoCategory(
            new ApplyInfoCategory(UnAuditedNews::APPLY_NEWS_CATEGORY, 2)
        );

        return $unAuditedNews->resubmit();
    }

    /**
     * @Then: 可以查到重新编辑的数据
     */
    public function testValidate()
    {
        $result = $this->resubmit();

        $this->assertTrue($result);
        
        $unAuditedNews = $this->fetchUnAuditedNews(1);

        $this->assertEquals('测试重新编辑', $unAuditedNews->getApplyTitle());
        $this->assertEquals(3, $unAuditedNews->getRelation()->getId());
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $unAuditedNews->getApplyStatus());
        $this->assertEquals(2, $unAuditedNews->getApplyInfoCategory()->getType());
    }
}
