<?php
namespace Base\News\UnAuditedNews\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;

use Base\News\SetDataTrait;
use Base\News\Repository\UnAuditedNewsRepository;

use Base\Crew\Model\Crew;

/**
 * @Feature: 我是拥有新闻审核权限的平台管理员/委办局管理员/操作用户,当我需要审核一个新闻时,在审核表中,审核待审核的新闻数据
 *           通过新闻详情页面的审核通过与审核驳回操作,以便于我维护新闻列表
 * @Scenario: 审核通过
 */
class ApproveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条待审核的新闻审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyForm(IApproveAble::APPLY_STATUS['PENDING']),
            'pcore_crew' => $this->crew(1)
        ]);
    }

    /**
     * @When: 获取需要审核通过的新闻
     */
    public function fetchUnAuditedNews($id)
    {
        $repository = new UnAuditedNewsRepository();

        $unAuditedNews = $repository->fetchOne($id);

        return $unAuditedNews;
    }

    /**
     * @And: 当我调用审核通过函数,期待返回true
     */
    public function approve()
    {
        $unAuditedNews = $this->fetchUnAuditedNews(1);
        $unAuditedNews->setApplyCrew(new Crew(1));
        
        return $unAuditedNews->approve();
    }

    /**
     * @Then: 数据已经被审核通过
     */
    public function testValidate()
    {
        $result = $this->approve();

        $this->assertTrue($result);

        $unAuditedNews = $this->fetchUnAuditedNews(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['APPROVE'], $unAuditedNews->getApplyStatus());
        $this->assertEquals(Core::$container->get('time'), $unAuditedNews->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $unAuditedNews->getStatusTime());
        $this->assertEquals(1, $unAuditedNews->getApplyCrew()->getId());
    }
}
