<?php
namespace Base\News\UnAuditedNews\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\SetDataTrait;
use Base\News\Repository\UnAuditedNewsRepository;
use Base\News\Translator\UnAuditedNewsDbTranslator;

 /**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,我拥有新闻发布权限、当我需要查看某个特定的已发布新闻时,
 *           在审核表中,我可以搜索到我需要的新闻数据
 *           通过列表形式查看我搜索到的新闻数据 ,以便于我可以快速的搜索到我需要的新闻数据
 * @Scenario: 通过新闻类型搜索
 */
class ApplyInfoTypeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
    }
    
    /**
     * @Given: 存在新闻审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyForm()
        ]);
    }

    /**
     * @When: 当我查看该条新闻审核数据详情时
     */
    public function fetchUnAuditedNews($id)
    {
        $repository = new UnAuditedNewsRepository();

        $unAuditedNews = $repository->fetchOne($id);

        return $unAuditedNews;
    }

    public function fetchUnAuditedNewsList()
    {
        $repository = new UnAuditedNewsRepository();
 
        $filter['applyInfoCategory'] = 1;
        $filter['applyInfoType'] = 1;

        list($unAuditedNewsList, $count) = $repository->filter($filter);
        
        unset($count);

        return $unAuditedNewsList;
    }

    /**
     * @Then: 我可以看到新闻类型为1的所有新闻审核数据
     */
    public function testViewUnAuditedNewsList()
    {
        $setUnAuditedNewsList = $this->applyForm();

        foreach ($setUnAuditedNewsList as $key => $setUnAuditedNews) {
            unset($setUnAuditedNews);
            $setUnAuditedNewsList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedNewsList = $this->fetchUnAuditedNewsList();
        $translator = new UnAuditedNewsDbTranslator();
        foreach ($unAuditedNewsList as $unAuditedNews) {
            $unAuditedNewsArray[] = $translator->objectToArray($unAuditedNews);
        }

        $this->assertEquals($unAuditedNewsArray, $setUnAuditedNewsList);

        foreach ($unAuditedNewsArray as $unAuditedNews) {
            $this->assertEquals(1, $unAuditedNews['apply_info_type']);
        }
    }
}
