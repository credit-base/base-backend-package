<?php
namespace Base\News\News\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\ITopAble;

use Base\News\SetDataTrait;
use Base\News\Repository\NewsRepository;

use Base\Crew\Model\Crew;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要修改某个特定新闻置顶时,在发布表的操作栏中,将新闻修改为置顶状态
 *           通过发布表的操作栏中的置顶操作,以便于我可以更好的维护新闻发布管理列表
 * @Scenario: 置顶新闻
 */
class TopTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_news');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在需要置顶的新闻
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_news' => $this->news(),
            'pcore_crew' => $this->crew(1)
        ]);
    }

    /**
     * @When: 获取需要置顶的新闻
     */
    public function fetchNews($id)
    {
        $repository = new NewsRepository();

        $news = $repository->fetchOne($id);

        return $news;
    }

    /**
     * @And: 当我调用置顶函数,期待返回true
     */
    public function top()
    {
        $news = $this->fetchNews(1);
        $news->setCrew(new Crew(1));
        
        return $news->top();
    }

    /**
     * @Then: 数据已经被置顶
     */
    public function testValidate()
    {
        $result = $this->top();

        $this->assertTrue($result);

        $news = $this->fetchNews(1);

        $this->assertEquals(ITopAble::STICK['ENABLED'], $news->getStick());
        $this->assertEquals(Core::$container->get('time'), $news->getUpdateTime());
        $this->assertEquals(1, $news->getCrew()->getId());
    }
}
