<?php
namespace Base\News\News\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;

use Base\News\SetDataTrait;
use Base\News\Repository\NewsRepository;

use Base\Crew\Model\Crew;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要修改某个特定新闻取消置顶时,在发布表的操作栏中,将新闻修改为取消置顶状态
 *           通过发布表的操作栏中的取消置顶操作,以便于我可以更好的维护新闻发布管理列表
 * @Scenario: 取消置顶新闻
 */
class CancelTopTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_news');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在需要取消置顶的新闻数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_news' => $this->news(IEnableAble::STATUS['ENABLED'], ITopAble::STICK['ENABLED']),
            'pcore_crew' => $this->crew(1)
        ]);
    }

    /**
     * @When: 获取需要取消置顶的新闻数据
     */
    public function fetchNews($id)
    {
        $repository = new NewsRepository();

        $news = $repository->fetchOne($id);

        return $news;
    }

    /**
     * @And: 当我调用取消置顶函数,期待返回true
     */
    public function cancelTop()
    {
        $news = $this->fetchNews(1);
        $news->setCrew(new Crew(1));
        
        return $news->cancelTop();
    }

    /**
     * @Then: 数据已经被取消置顶
     */
    public function testValidate()
    {
        $result = $this->cancelTop();

        $this->assertTrue($result);

        $news = $this->fetchNews(1);

        $this->assertEquals(ITopAble::STICK['DISABLED'], $news->getStick());
        $this->assertEquals(Core::$container->get('time'), $news->getUpdateTime());
        $this->assertEquals(1, $news->getCrew()->getId());
    }
}
