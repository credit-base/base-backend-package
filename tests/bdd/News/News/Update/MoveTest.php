<?php
namespace Base\News\News\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\SetDataTrait;
use Base\News\Model\NewsCategory;
use Base\News\Repository\NewsRepository;

use Base\Crew\Model\Crew;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要移动新闻时,在发布表中,移动对应新闻数据
 *           通过移动新闻界面，并根据我所采集的新闻数据进行移动,以便于我可以更好的维护新闻管理列表
 * @Scenario: 正常移动新闻数据
 */
class MoveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_news');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条新闻数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_news' => $this->news(),
            'pcore_crew' => $this->crew(1)
        ]);
    }

    /**
     * @When: 获取需要移动的新闻数据
     */
    public function fetchNews($id)
    {
        $repository = new NewsRepository();

        $news = $repository->fetchOne($id);

        return $news;
    }
    /**
     * @When: 当我调用移动函数,期待返回true
     */
    public function move()
    {
        $news = $this->fetchNews(1);
        
        $news->setNewsCategory(new NewsCategory(
            2,
            2,
            2
        ));
        $news->setCrew(new Crew(1));

        return $news->move();
    }

    /**
     * @Then: 可以查到移动的数据
     */
    public function testValidate()
    {
        $result = $this->move();

        $this->assertTrue($result);
        
        $news = $this->fetchNews(1);

        $this->assertEquals(2, $news->getNewsCategory()->getParentCategory());
        $this->assertEquals(2, $news->getNewsCategory()->getCategory());
        $this->assertEquals(2, $news->getNewsCategory()->getType());
        $this->assertEquals(1, $news->getCrew()->getId());
    }
}
