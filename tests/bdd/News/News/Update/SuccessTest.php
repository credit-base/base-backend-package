<?php
namespace Base\News\News\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\SetDataTrait;
use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\Model\NewsCategory;
use Base\News\Repository\NewsRepository;

use Base\UserGroup\Model\UserGroup;

use Base\Crew\Model\Crew;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑新闻信息时,在发布表中,编辑对应新闻数据
 *           通过编辑新闻界面，并根据我所采集的新闻数据进行编辑,以便于我可以更好的维护新闻管理列表
 * @Scenario: 正常编辑新闻数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_news');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_news' => $this->news(),
            'pcore_user_group' => $this->userGroup(2),
            'pcore_crew' => $this->crew(1),
        ]);
    }

    /**
     * @When: 获取需要编辑的新闻数据
     */
    public function fetchNews($id)
    {
        $repository = new NewsRepository();

        $news = $repository->fetchOne($id);

        return $news;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $news = $this->fetchNews(1);
        $news->setTitle('测试编辑数据');
        $news->setSource('测试编辑数据');
        $news->setCover(array('name'=>'测试名称', 'identify'=>'测试地址.jpg'));
        $news->setAttachments(array(array('name'=>'测试名称', 'identify'=>'测试地址.jpg')));
        $news->setContent('测试内容');
        $news->setDescription('测试描述');
        $news->setNewsCategory(new NewsCategory(
            2,
            2,
            2
        ));
        $news->setCrew(new Crew(1));
        $news->setPublishUserGroup(new UserGroup(2));
        $news->setStatus(IEnableAble::STATUS['DISABLED']);
        $news->setStick(ITopAble::STICK['ENABLED']);
        $news->setDimension(News::DIMENSIONALITY['GOVERNMENT_AFFAIRS']);
        $news->setBanner(new Banner(
            array('name'=>'测试名称', 'identify'=>'测试地址.jpg'),
            Banner::BANNER_STATUS['ENABLED']
        ));
        $news->setHomePageShowStatus(News::HOME_PAGE_SHOW_STATUS['DISABLED']);

        return $news->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $news = $this->fetchNews(1);

        $this->assertEquals('测试编辑数据', $news->getTitle());
        $this->assertEquals('测试编辑数据', $news->getSource());
        $this->assertEquals(array('name'=>'测试名称', 'identify'=>'测试地址.jpg'), $news->getCover());
        $this->assertEquals(array(array('name'=>'测试名称', 'identify'=>'测试地址.jpg')), $news->getAttachments());
        $this->assertEquals('测试内容', $news->getContent());
        $this->assertEquals('测试描述', $news->getDescription());
        $this->assertEquals(2, $news->getNewsCategory()->getParentCategory());
        $this->assertEquals(2, $news->getNewsCategory()->getCategory());
        $this->assertEquals(2, $news->getNewsCategory()->getType());
        $this->assertEquals(1, $news->getCrew()->getId());
        $this->assertEquals(2, $news->getPublishUserGroup()->getId());
        $this->assertEquals(IEnableAble::STATUS['DISABLED'], $news->getStatus());
        $this->assertEquals(ITopAble::STICK['ENABLED'], $news->getStick());
        $this->assertEquals(News::DIMENSIONALITY['GOVERNMENT_AFFAIRS'], $news->getDimension());
        $this->assertEquals(array('name'=>'测试名称', 'identify'=>'测试地址.jpg'), $news->getBanner()->getImage());
        $this->assertEquals(Banner::BANNER_STATUS['ENABLED'], $news->getBanner()->getStatus());
        $this->assertEquals(News::HOME_PAGE_SHOW_STATUS['DISABLED'], $news->getHomePageShowStatus());
    }
}
