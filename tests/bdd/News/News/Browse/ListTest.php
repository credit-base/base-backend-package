<?php
namespace Base\News\News\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\SetDataTrait;
use Base\News\Repository\NewsRepository;
use Base\News\Translator\NewsDbTranslator;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有新闻发布权限、且当我需要查看通过审核的新闻的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_news');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在新闻数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_news' => $this->news(),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看新闻数据列表时
     */
    public function fetchNewsList()
    {
        $repository = new NewsRepository();

        list($newsList, $count) = $repository->filter([]);
        
        unset($count);

        return $newsList;
    }

    /**
     * @Then: 我可以看到新闻数据的全部信息
     */
    public function testViewNewsList()
    {
        $setNewsList = $this->news();

        foreach ($setNewsList as $key => $setNews) {
            $setNewsList[$key]['news_id'] = $key +1;
            $setNewsList[$key]['cover'] = json_decode($setNews['cover'], true);
            $setNewsList[$key]['attachments'] = json_decode($setNews['attachments'], true);
            $setNewsList[$key]['banner_image'] = json_decode($setNews['banner_image'], true);
        }

        $newsList = $this->fetchNewsList();

        $translator = new NewsDbTranslator();
        foreach ($newsList as $news) {
            $newsArray[] = $translator->objectToArray($news);
        }

        $this->assertEquals($newsArray, $setNewsList);
    }
}
