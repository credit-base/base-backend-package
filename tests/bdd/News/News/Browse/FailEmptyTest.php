<?php
namespace Base\News\News\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\Repository\NewsRepository;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有新闻发布权限、且当我需要查看通过审核的新闻的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在新闻
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看新闻列表时
     */
    public function fetchNewsList()
    {
        $repository = new NewsRepository();

        list($newsList, $count) = $repository->filter([]);
        
        unset($count);

        return $newsList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewNews()
    {
        $newsList = $this->fetchNewsList();

        $this->assertEmpty($newsList);
    }
}
