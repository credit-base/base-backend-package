<?php
namespace Base\News\News\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\SetDataTrait;
use Base\News\Repository\NewsRepository;
use Base\News\Translator\NewsDbTranslator;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有新闻发布权限、且当我需要查看通过审核的新闻的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_news');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在一条新闻数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_news' => $this->news(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 当我查看该条新闻数据详情时
     */
    public function fetchNews($id)
    {
        $repository = new NewsRepository();

        $news = $repository->fetchOne($id);

        return $news;
    }

    /**
     * @Then: 我可以看见该条新闻数据的全部信息
     */
    public function testViewNewsList()
    {
        $id = 1;
        
        $setNews = $this->news()[0];
        $setNews['news_id'] = $id;
        $setNews['cover'] = json_decode($setNews['cover'], true);
        $setNews['attachments'] = json_decode($setNews['attachments'], true);
        $setNews['banner_image'] = json_decode($setNews['banner_image'], true);

        $news = $this->fetchNews($id);
        $translator = new NewsDbTranslator();
        $newsArray = $translator->objectToArray($news);

        $this->assertEquals($newsArray, $setNews);
    }
}
