<?php
namespace Base\News\News\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\UserGroup\Model\UserGroup;

use Base\Crew\Model\Crew;

use Base\News\SetDataTrait;
use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\Model\NewsCategory;
use Base\News\Repository\NewsRepository;
use Base\News\Translator\NewsDbTranslator;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要新增一个新闻时,在发布表中,新增对应的新闻数据
 *           通过新增新闻界面，并根据我所采集的新闻数据进行新增,以便于我维护新闻列表
 * @Scenario: 正常新增数据数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_news');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $newsArray = $this->news()[0];

        $news = new News();
        $news->setTitle($newsArray['title']);
        $news->setSource($newsArray['source']);
        $news->setCover(json_decode($newsArray['cover'], true));
        $news->setAttachments(json_decode($newsArray['attachments'], true));
        $news->setContent($newsArray['content']);
        $news->setDescription($newsArray['description']);
        $news->setNewsCategory(new NewsCategory(
            $newsArray['parent_category'],
            $newsArray['category'],
            $newsArray['type']
        ));
        $news->setCrew(new Crew($newsArray['crew_id']));
        $news->setPublishUserGroup(new UserGroup($newsArray['publish_usergroup_id']));
        $news->setStatus($newsArray['status']);
        $news->setStick($newsArray['stick']);
        $news->setDimension($newsArray['dimension']);
        $news->setBanner(new Banner(
            json_decode($newsArray['banner_image'], true),
            $newsArray['banner_status']
        ));
        $news->setHomePageShowStatus($newsArray['home_page_show_status']);
        
        return $news->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setNews = $this->news()[0];
        $setNews['news_id'] = 1;
        $setNews['cover'] = json_decode($setNews['cover'], true);
        $setNews['attachments'] = json_decode($setNews['attachments'], true);
        $setNews['banner_image'] = json_decode($setNews['banner_image'], true);
        
        $repository = new NewsRepository();

        $news = $repository->fetchOne($result);

        $translator = new NewsDbTranslator();
        $newsArray = $translator->objectToArray($news);

        $this->assertEquals($newsArray, $setNews);
    }
}
