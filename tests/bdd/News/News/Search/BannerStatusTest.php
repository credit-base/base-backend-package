<?php
namespace Base\News\News\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\News\SetDataTrait;
use Base\News\Model\Banner;
use Base\News\Repository\NewsRepository;
use Base\News\Translator\NewsDbTranslator;

 /**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,我拥有新闻发布权限、当我需要查看某个特定的已发布新闻时,
 *           在发布表中,我可以搜索到我需要的新闻数据
 *           通过列表形式查看我搜索到的新闻数据 ,以便于我可以快速的搜索到我需要的新闻数据
 * @Scenario: 通过轮播状态搜索
 */
class BannerStatusTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_news');
    }

    /**
     * @Given: 存在新闻数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_news' => $this->news()
        ]);
    }
    /**
     * @When: 当我查看新闻数据列表时
     */
    public function fetchNewsList()
    {
        $repository = new NewsRepository();
 
        $filter['bannerStatus'] = Banner::BANNER_STATUS['DISABLED'];

        list($newsList, $count) = $repository->filter($filter);
        
        unset($count);

        return $newsList;
    }

    /**
     * @Then: 我可以看到新闻轮播状态为"未轮播"的新闻数据
     */
    public function testViewNewsList()
    {
        $setNewsList = $this->news();

        foreach ($setNewsList as $key => $each) {
            $setNewsList[$key]['news_id'] = $key +1;
            $setNewsList[$key]['cover'] = json_decode($each['cover'], true);
            $setNewsList[$key]['attachments'] = json_decode($each['attachments'], true);
            $setNewsList[$key]['banner_image'] = json_decode($each['banner_image'], true);
        }

        $newsList = $this->fetchNewsList();
        $translator = new NewsDbTranslator();
        foreach ($newsList as $news) {
            $newsArray[] = $translator->objectToArray($news);
        }

        $this->assertEquals($newsArray, $setNewsList);

        foreach ($newsArray as $news) {
            $this->assertEquals(Banner::BANNER_STATUS['DISABLED'], $news['banner_status']);
        }
    }
}
