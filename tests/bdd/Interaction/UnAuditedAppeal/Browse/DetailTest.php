<?php
namespace Base\Interaction\UnAuditedAppeal\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedAppealDbTranslator;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉审核权限,在异议申诉模块中
 *           查看前台用户提交的异议申诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的异议申诉信息
 * @Scenario: 查看异议申诉审核数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在一条异议申诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormAppeal(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(),
            'pcore_member' => $this->member()
        ]);
    }

    /**
     * @When: 当我查看该条异议申诉审核数据详情时
     */
    public function fetchUnAuditedAppeal($id)
    {
        $repository = new UnAuditedAppealRepository();

        $unAuditedAppeal = $repository->fetchOne($id);

        return $unAuditedAppeal;
    }

    /**
     * @Then: 我可以看见该条异议申诉审核数据的全部信息
     */
    public function testViewUnAuditedAppealList()
    {
        $id = 1;
        
        $setUnAuditedAppeal = $this->applyFormAppeal()[0];
        $setUnAuditedAppeal['apply_form_id'] = $id;

        $unAuditedAppeal = $this->fetchUnAuditedAppeal($id);
        $translator = new UnAuditedAppealDbTranslator();
        $unAuditedAppealArray = $translator->objectToArray($unAuditedAppeal);

        $this->assertEquals($unAuditedAppealArray, $setUnAuditedAppeal);
    }
}
