<?php
namespace Base\Interaction\UnAuditedAppeal\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉审核权限,在异议申诉模块中
 *           查看前台用户提交的异议申诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的异议申诉信息
 * @Scenario: 异常数据不存在-查看异议申诉审核数据列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在异议申诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看异议申诉审核列表时
     */
    public function fetchUnAuditedAppealList()
    {
        $repository = new UnAuditedAppealRepository();

        list($unAuditedAppealList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedAppealList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewAppeal()
    {
        $unAuditedAppealList = $this->fetchUnAuditedAppealList();

        $this->assertEmpty($unAuditedAppealList);
    }
}
