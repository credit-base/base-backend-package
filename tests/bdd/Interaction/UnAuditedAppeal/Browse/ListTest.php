<?php
namespace Base\Interaction\UnAuditedAppeal\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedAppealDbTranslator;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉审核权限,在异议申诉模块中
 *           查看前台用户提交的异议申诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的异议申诉信息
 * @Scenario: 查看异议申诉审核数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在异议申诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_member' => $this->member(),
            'pcore_apply_form' => $this->applyFormAppeal(),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看异议申诉审核数据列表时
     */
    public function fetchUnAuditedAppealList()
    {
        $repository = new UnAuditedAppealRepository();

        list($unAuditedAppealList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedAppealList;
    }

    /**
     * @Then: 我可以看到异议申诉数据的全部信息
     */
    public function testViewAppealList()
    {
        $setUnAuditedAppealList = $this->applyFormAppeal();

        foreach ($setUnAuditedAppealList as $key => $setUnAuditedAppeal) {
            unset($setUnAuditedAppeal);
            $setUnAuditedAppealList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedAppealList = $this->fetchUnAuditedAppealList();

        $translator = new UnAuditedAppealDbTranslator();
        foreach ($unAuditedAppealList as $unAuditedAppeal) {
            $unAuditedAppealArray[] = $translator->objectToArray($unAuditedAppeal);
        }

        $this->assertEquals($unAuditedAppealArray, $setUnAuditedAppealList);
    }
}
