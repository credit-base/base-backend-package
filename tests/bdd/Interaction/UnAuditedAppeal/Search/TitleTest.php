<?php
namespace Base\Interaction\UnAuditedAppeal\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedAppealDbTranslator;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉审核权限，当我想要查看某个异议申诉时,
 *           在政务网OA中的异议申诉审核模块,我可以搜索待审核的异议申诉
 *           通过异议申诉的标题名称进行搜索 ,以便于我快速定位某条异议申诉并进行下一步操作
 * @Scenario: 通过异议申诉标题搜索
 */
class TitleTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
    }

    /**
     * @Given: 存在异议申诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormAppeal()
        ]);
    }
    /**
     * @When: 当我查看异议申诉审核数据列表时
     */
    public function fetchUnAuditedAppealList()
    {
        $repository = new UnAuditedAppealRepository();
 
        $filter['title'] = '申诉';

        list($unAuditedAppealList, $count) = $repository->filter($filter);
        
        unset($count);

        return $unAuditedAppealList;
    }

    /**
     * @Then: 我可以看到标题中带"申诉"的所有异议申诉审核数据
     */
    public function testViewUnAuditedAppealList()
    {
        $setUnAuditedAppealList = $this->applyFormAppeal();

        foreach ($setUnAuditedAppealList as $key => $setUnAuditedAppeal) {
            unset($setUnAuditedAppeal);
            $setUnAuditedAppealList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedAppealList = $this->fetchUnAuditedAppealList();
        $translator = new UnAuditedAppealDbTranslator();
        foreach ($unAuditedAppealList as $unAuditedAppeal) {
            $unAuditedAppealArray[] = $translator->objectToArray($unAuditedAppeal);
        }

        $this->assertEquals($unAuditedAppealArray, $setUnAuditedAppealList);

        foreach ($unAuditedAppealArray as $unAuditedAppeal) {
            $this->assertEquals('个人申诉', $unAuditedAppeal['title']);
        }
    }
}
