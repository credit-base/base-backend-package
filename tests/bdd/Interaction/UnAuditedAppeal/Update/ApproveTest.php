<?php
namespace Base\Interaction\UnAuditedAppeal\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Repository\Appeal\AppealRepository;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉审核权限,当我需要想要对某个待审核的异议申诉数据进行审核时,在业务审核-审核表中
 *           对前台用户提交上来的异议申诉进行审核通过或审核驳回操作,以便于我维护异议申诉审核信息
 * @Scenario: 审核通过
 */
class ApproveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_reply');
        $this->clear('pcore_member');
        $this->clear('pcore_appeal');
    }

    /**
     * @Given: 存在一条待审核的异议申诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_appeal' => $this->appeal(1),
            'pcore_apply_form' => $this->applyFormAppeal(IApproveAble::APPLY_STATUS['PENDING']),
            'pcore_crew' => $this->crew(2),
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
            'pcore_reply' => $this->reply()
        ]);
    }

    /**
     * @When: 获取需要审核通过的异议申诉
     */
    public function fetchUnAuditedAppeal($id)
    {
        $repository = new UnAuditedAppealRepository();

        $unAuditedAppeal = $repository->fetchOne($id);

        return $unAuditedAppeal;
    }

    /**
     * @And: 当我调用审核通过函数,期待返回true
     */
    public function approve()
    {
        $unAuditedAppeal = $this->fetchUnAuditedAppeal(1);
        $unAuditedAppeal->getApplyCrew()->setId(1);
        
        return $unAuditedAppeal->approve();
    }

    /**
     * @Then: 数据已经被审核通过
     */
    public function testValidate()
    {
        $result = $this->approve();

        $this->assertTrue($result);

        $unAuditedAppeal = $this->fetchUnAuditedAppeal(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['APPROVE'], $unAuditedAppeal->getApplyStatus());
        $this->assertEquals(Core::$container->get('time'), $unAuditedAppeal->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $unAuditedAppeal->getStatusTime());
        $this->assertEquals(1, $unAuditedAppeal->getApplyCrew()->getId());
    }
}
