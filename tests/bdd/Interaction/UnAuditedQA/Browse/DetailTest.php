<?php
namespace Base\Interaction\UnAuditedQA\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedQADbTranslator;
use Base\Interaction\Repository\QA\UnAuditedQARepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答审核权限,在信用问答模块中
 *           查看前台用户提交的信用问答信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用问答信息
 * @Scenario: 查看信用问答审核数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在一条信用问答审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormQA(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(),
            'pcore_member' => $this->member()
        ]);
    }

    /**
     * @When: 当我查看该条信用问答审核数据详情时
     */
    public function fetchUnAuditedQA($id)
    {
        $repository = new UnAuditedQARepository();

        $unAuditedQA = $repository->fetchOne($id);

        return $unAuditedQA;
    }

    /**
     * @Then: 我可以看见该条信用问答审核数据的全部信息
     */
    public function testViewUnAuditedQAList()
    {
        $id = 1;
        
        $setUnAuditedQA = $this->applyFormQA()[0];
        $setUnAuditedQA['apply_form_id'] = $id;

        $unAuditedQA = $this->fetchUnAuditedQA($id);
        $translator = new UnAuditedQADbTranslator();
        $unAuditedQAArray = $translator->objectToArray($unAuditedQA);

        $this->assertEquals($unAuditedQAArray, $setUnAuditedQA);
    }
}
