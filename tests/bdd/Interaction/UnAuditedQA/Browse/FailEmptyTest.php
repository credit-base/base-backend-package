<?php
namespace Base\Interaction\UnAuditedQA\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\Repository\QA\UnAuditedQARepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答审核权限,在信用问答模块中
 *           查看前台用户提交的信用问答信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用问答信息
 * @Scenario: 异常数据不存在-查看信用问答审核数据列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在信用问答审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看信用问答审核列表时
     */
    public function fetchUnAuditedQAList()
    {
        $repository = new UnAuditedQARepository();

        list($unAuditedQAList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedQAList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewQA()
    {
        $unAuditedQAList = $this->fetchUnAuditedQAList();

        $this->assertEmpty($unAuditedQAList);
    }
}
