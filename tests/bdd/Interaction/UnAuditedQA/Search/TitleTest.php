<?php
namespace Base\Interaction\UnAuditedQA\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedQADbTranslator;
use Base\Interaction\Repository\QA\UnAuditedQARepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答审核权限，当我想要查看某个信用问答时,
 *           在政务网OA中的信用问答审核模块,我可以搜索待审核的信用问答
 *           通过信用问答的标题名称进行搜索 ,以便于我快速定位某条信用问答并进行下一步操作
 * @Scenario: 通过信用问答标题搜索
 */
class TitleTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
    }

    /**
     * @Given: 存在信用问答审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormQA()
        ]);
    }
    /**
     * @When: 当我查看信用问答审核数据列表时
     */
    public function fetchUnAuditedQAList()
    {
        $repository = new UnAuditedQARepository();
 
        $filter['title'] = '信用问答';

        list($unAuditedQAList, $count) = $repository->filter($filter);
        
        unset($count);

        return $unAuditedQAList;
    }

    /**
     * @Then: 我可以看到标题中带"信用问答"的所有信用问答审核数据
     */
    public function testViewUnAuditedQAList()
    {
        $setUnAuditedQAList = $this->applyFormQA();

        foreach ($setUnAuditedQAList as $key => $setUnAuditedQA) {
            unset($setUnAuditedQA);
            $setUnAuditedQAList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedQAList = $this->fetchUnAuditedQAList();
        $translator = new UnAuditedQADbTranslator();
        foreach ($unAuditedQAList as $unAuditedQA) {
            $unAuditedQAArray[] = $translator->objectToArray($unAuditedQA);
        }

        $this->assertEquals($unAuditedQAArray, $setUnAuditedQAList);

        foreach ($unAuditedQAArray as $unAuditedQA) {
            $this->assertEquals('信用问答', $unAuditedQA['title']);
        }
    }
}
