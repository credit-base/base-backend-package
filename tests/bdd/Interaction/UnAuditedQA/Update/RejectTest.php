<?php
namespace Base\Interaction\UnAuditedQA\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Repository\QA\UnAuditedQARepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答审核权限,当我需要想要对某个待审核的信用问答数据进行审核时,在业务审核-审核表中
 *           对前台用户提交上来的信用问答进行审核通过或审核驳回操作,以便于我维护信用问答审核信息
 * @Scenario: 审核驳回
 */
class RejectTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在需要审核驳回的信用问答数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormQA(IApproveAble::APPLY_STATUS['PENDING']),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 获取需要审核驳回的信用问答数据
     */
    public function fetchUnAuditedQA($id)
    {
        $repository = new UnAuditedQARepository();

        $unAuditedQA = $repository->fetchOne($id);

        return $unAuditedQA;
    }

    /**
     * @And: 当我调用审核驳回函数,期待返回true
     */
    public function reject()
    {
        $unAuditedQA = $this->fetchUnAuditedQA(1);
        $unAuditedQA->setRejectReason('测试驳回原因');
        $unAuditedQA->getApplyCrew()->setId(1);
        
        return $unAuditedQA->reject();
    }

    /**
     * @Then: 数据已经被审核驳回
     */
    public function testValidate()
    {
        $result = $this->reject();

        $this->assertTrue($result);

        $unAuditedQA = $this->fetchUnAuditedQA(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['REJECT'], $unAuditedQA->getApplyStatus());
        $this->assertEquals('测试驳回原因', $unAuditedQA->getRejectReason());
        $this->assertEquals(Core::$container->get('time'), $unAuditedQA->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $unAuditedQA->getStatusTime());
        $this->assertEquals(1, $unAuditedQA->getApplyCrew()->getId());
    }
}
