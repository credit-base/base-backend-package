<?php
namespace Base\Interaction\Complaint\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\ComplaintDbTranslator;
use Base\Interaction\Repository\Complaint\ComplaintRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉管理权限,在信用投诉模块中
 *           查看前台用户提交的已审核通过的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用投诉信息
 * @Scenario: 查看信用投诉数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_complaint');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 存在一条信用投诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_complaint' => $this->complaint(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
            'pcore_reply' => $this->reply(1),
        ]);
    }

    /**
     * @When: 当我查看该条信用投诉数据详情时
     */
    public function fetchComplaint($id)
    {
        $repository = new ComplaintRepository();

        $complaint = $repository->fetchOne($id);

        return $complaint;
    }

    /**
     * @Then: 我可以看见该条信用投诉数据的全部信息
     */
    public function testViewComplaintList()
    {
        $id = 1;
        
        $setComplaint = $this->complaint()[0];
        $setComplaint['images'] = json_decode($setComplaint['images'], true);

        $complaint = $this->fetchComplaint($id);
        $translator = new ComplaintDbTranslator();
        $complaintArray = $translator->objectToArray($complaint);

        $this->assertEquals($complaintArray, $setComplaint);
    }
}
