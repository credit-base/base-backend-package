<?php
namespace Base\Interaction\Complaint\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\ComplaintDbTranslator;
use Base\Interaction\Repository\Complaint\ComplaintRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉管理权限,在信用投诉模块中
 *           查看前台用户提交的已审核通过的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用投诉信息
 * @Scenario: 查看信用投诉数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
        $this->clear('pcore_complaint');
        $this->clear('pcore_user_group');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 存在信用投诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
            'pcore_complaint' => $this->complaint(),
            'pcore_reply' => $this->reply(1),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看信用投诉数据列表时
     */
    public function fetchComplaintList()
    {
        $repository = new ComplaintRepository();

        list($complaintList, $count) = $repository->filter([]);
        
        unset($count);

        return $complaintList;
    }

    /**
     * @Then: 我可以看到信用投诉数据的全部信息
     */
    public function testViewComplaintList()
    {
        $setComplaintList = $this->complaint();

        foreach ($setComplaintList as $key => $setComplaint) {
            $setComplaintList[$key]['images'] = json_decode($setComplaint['images'], true);
        }

        $complaintList = $this->fetchComplaintList();

        $translator = new ComplaintDbTranslator();
        foreach ($complaintList as $complaint) {
            $complaintArray[] = $translator->objectToArray($complaint);
        }

        $this->assertEquals($complaintArray, $setComplaintList);
    }
}
