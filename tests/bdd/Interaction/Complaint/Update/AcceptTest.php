<?php
namespace Base\Interaction\Complaint\Update;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\Model\Reply;
use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\UnAuditedComplaint;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Translator\ReplyDbTranslator;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Repository\Reply\ReplyRepository;
use Base\Interaction\Repository\Complaint\ComplaintRepository;
use Base\Interaction\Translator\UnAuditedComplaintDbTranslator;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉管理权限,当我需要想要对某个信用投诉数据进行受理处理时,在业务管理-信用投诉中
 *           对前台用户提交上来的信用投诉进行受理处理，并提交我填写的受理依据,以便于我维护信用投诉信息
 * @Scenario: 受理信用投诉
 */
class AcceptTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_complaint');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 我并未受理过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_complaint' => $this->complaint(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(1),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 获取需要受理的信用投诉数据
     */
    public function fetchComplaint($id)
    {
        $repository = new ComplaintRepository();

        $complaint = $repository->fetchOne($id);

        return $complaint;
    }

    public function fetchUnAuditedComplaint($id)
    {
        $repository = new UnAuditedComplaintRepository();

        $complaint = $repository->fetchOne($id);

        return $complaint;
    }

    public function fetchReply($id)
    {
        $repository = new ReplyRepository();

        $complaint = $repository->fetchOne($id);

        return $complaint;
    }

    /**
     * @When: 当我调用受理函数,期待返回true
     */
    public function accept()
    {
        $replyArray = $this->reply()[0];
        $reply = new Reply();
        $reply->setContent($replyArray['content']);
        $reply->setImages(json_decode($replyArray['images'], true));
        $reply->setAdmissibility($replyArray['admissibility']);
        $reply->getAcceptUserGroup()->setId($replyArray['accept_usergroup_id']);
        $reply->getCrew()->setId($replyArray['crew_id']);

        $complaint = $this->fetchComplaint(1);
        $complaint->setReply($reply);

        $unAuditedComplaint = new UnAuditedComplaint();
        $unAuditedComplaint->setId($complaint->getId());
        $unAuditedComplaint->setTitle($complaint->getTitle());
        $unAuditedComplaint->setContent($complaint->getContent());
        $unAuditedComplaint->setMember($complaint->getMember());
        $unAuditedComplaint->setAcceptUserGroup($complaint->getAcceptUserGroup());
        $unAuditedComplaint->setName($complaint->getName());
        $unAuditedComplaint->setIdentify($complaint->getIdentify());
        $unAuditedComplaint->setType($complaint->getType());
        $unAuditedComplaint->setContact($complaint->getContact());
        $unAuditedComplaint->setImages($complaint->getImages());
        $unAuditedComplaint->setSubject($complaint->getSubject());
        $unAuditedComplaint->setApplyTitle($complaint->getTitle());
        $unAuditedComplaint->setRelation($complaint->getMember());
        $unAuditedComplaint->getApplyCrew()->setId(1);
        $unAuditedComplaint->setApplyUserGroup($complaint->getAcceptUserGroup());
        $unAuditedComplaint->setOperationType(IApproveAble::OPERATION_TYPE['ACCEPT']);
        $unAuditedComplaint->setApplyInfoCategory(
            new ApplyInfoCategory(
                UnAuditedComplaint::APPLY_COMPLAINT_CATEGORY,
                UnAuditedComplaint::APPLY_COMPLAINT_TYPE
            )
        );
        $unAuditedComplaint->setReply($reply);

        $service = new AcceptInteractionService($unAuditedComplaint, $complaint, $reply);

        return $service->accept();
    }

    /**
     * @Then: 可以查到受理的数据
     */
    public function testValidate()
    {
        $result = $this->accept();
        $this->assertTrue($result);

        //验证受理状态是否变为受理中
        $complaint = $this->fetchComplaint(1);
        $this->assertEquals(IInteractionAble::ACCEPT_STATUS['ACCEPTING'], $complaint->getAcceptStatus());
        $this->assertEquals(1, $complaint->getReply()->getId());

        //验证是否新增一条回复信息
        $setReply = $this->reply()[0];
        $setReply['images'] = json_decode($setReply['images'], true);

        $reply = $this->fetchReply(1);
        $translator = new ReplyDbTranslator();
        $replyArray = $translator->objectToArray($reply);

        $this->assertEquals($replyArray, $setReply);

        //验证是否新增一条审核信息
        $setUnAuditedComplaint = $this->applyFormComplaint(IApproveAble::APPLY_STATUS['PENDING'])[0];

        $unAuditedComplaint = $this->fetchUnAuditedComplaint(1);
        $translator = new UnAuditedComplaintDbTranslator();
        $unAuditedComplaintArray = $translator->objectToArray($unAuditedComplaint);
        
        $this->assertEquals($unAuditedComplaintArray, $setUnAuditedComplaint);
    }
}
