<?php
namespace Base\Interaction\Complaint\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\Complaint\ComplaintRepository;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的信用投诉时,在用户中心-信用投诉中,我可以管理我提交的信用投诉数据
 *           可以撤销受理情况为未受理状态的信用投诉,以便于我维护我的信用投诉信息
 * @Scenario: 撤销信用投诉
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_complaint');
    }

    /**
     * @Given: 存在需要撤销的信用投诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_complaint' => $this->complaint()
        ]);
    }

    /**
     * @When: 获取需要撤销的信用投诉数据
     */
    public function fetchComplaint($id)
    {
        $repository = new ComplaintRepository();

        $complaint = $repository->fetchOne($id);

        return $complaint;
    }

    /**
     * @And: 当我调用撤销函数,期待返回true
     */
    public function revoke()
    {
        $complaint = $this->fetchComplaint(1);
        
        return $complaint->revoke();
    }

    /**
     * @Then: 数据已经被撤销
     */
    public function testValidate()
    {
        $result = $this->revoke();

        $this->assertTrue($result);

        $complaint = $this->fetchComplaint(1);

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['REVOKE'], $complaint->getStatus());
        $this->assertEquals(Core::$container->get('time'), $complaint->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $complaint->getStatusTime());
    }
}
