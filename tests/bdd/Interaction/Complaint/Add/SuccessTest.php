<?php
namespace Base\Interaction\Complaint\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\Complaint;
use Base\Interaction\Translator\ComplaintDbTranslator;
use Base\Interaction\Repository\Complaint\ComplaintRepository;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行投诉时,在信用公示栏目-信用反馈中,新增一个信用投诉数据
 *           通过新增信用投诉界面，并根据我所提交的投诉信息数据进行新增,以便于我维护信用投诉信息
 * @Scenario: 新增信用投诉
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_complaint');
        $this->clear('pcore_member');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $complaintArray = $this->complaint()[0];

        $complaint = new Complaint();

        $complaint->getMember()->setId(1);
        $complaint->setType($complaintArray['type']);
        $complaint->setName($complaintArray['name']);
        $complaint->getAcceptUserGroup()->setId(1);
        $complaint->setTitle($complaintArray['title']);
        $complaint->setSubject($complaintArray['subject']);
        $complaint->setContact($complaintArray['contact']);
        $complaint->setContent($complaintArray['content']);
        $complaint->setIdentify($complaintArray['identify']);
        $complaint->setImages(json_decode($complaintArray['images'], true));

        return $complaint->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();
        $this->assertTrue($result);

        $setComplaint = $this->complaint()[0];
        $setComplaint['images'] = json_decode($setComplaint['images'], true);
        
        $repository = new ComplaintRepository();
        $complaint = $repository->fetchOne($result);
        $translator = new ComplaintDbTranslator();
        $complaintArray = $translator->objectToArray($complaint);

        $this->assertEquals($complaintArray, $setComplaint);
    }
}
