<?php
namespace Base\Interaction\Complaint\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\Complaint;

use Base\Member\Model\NullMember;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行投诉时,在信用公示栏目-信用反馈中,新增一个信用投诉数据
 *           通过新增信用投诉界面，并根据我所提交的投诉信息数据进行新增,以便于我维护信用投诉信息
 * @Scenario: 异常流程-新增信用投诉-数据格式错误
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_complaint');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我调用添加函数
     */
    public function add()
    {
        $complaint = new Complaint();
        $complaint->setMember(NullMember::getInstance());

        return $complaint->add();
    }

    /**
     * @Then: 期望数据返回失败
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertFalse($result);
    }
}
