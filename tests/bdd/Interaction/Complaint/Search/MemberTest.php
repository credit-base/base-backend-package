<?php
namespace Base\Interaction\Complaint\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\ComplaintDbTranslator;
use Base\Interaction\Repository\Complaint\ComplaintRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉管理权限，当我想要查看某个信用投诉时,
 *           在政务网OA中的信用投诉管理模块,我可以搜索信用投诉
 *           通过信用投诉的标题名称进行搜索 ,以便于我快速定位某条信用投诉并进行下一步操作
 * @Scenario: 通过信用投诉前台用户id搜索
 */
class MemberTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_complaint');
        $this->clear('pcore_member');
    }
    
    /**
     * @Given: 存在信用投诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_complaint' => $this->complaint(),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 当我查看信用投诉数据列表时
     */
    public function fetchComplaintList()
    {
        $repository = new ComplaintRepository();
 
        $filter['member'] = 1;

        list($complaintList, $count) = $repository->filter($filter);
        
        unset($count);

        return $complaintList;
    }

    /**
     * @Then: 我可以看到前台用户Id为1的所有信用投诉数据
     */
    public function testViewComplaintList()
    {
        $setComplaintList = $this->complaint();

        foreach ($setComplaintList as $key => $setComplaint) {
            $setComplaintList[$key]['images'] = json_decode($setComplaint['images'], true);
        }

        $complaintList = $this->fetchComplaintList();
        $translator = new ComplaintDbTranslator();
        foreach ($complaintList as $complaint) {
            $complaintArray[] = $translator->objectToArray($complaint);
        }

        $this->assertEquals($complaintArray, $setComplaintList);
        foreach ($complaintArray as $complaint) {
            $this->assertEquals(1, $complaint['member_id']);
        }
    }
}
