<?php
namespace Base\Interaction\QA\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\QA;
use Base\Interaction\Translator\QADbTranslator;
use Base\Interaction\Repository\QA\QARepository;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行表扬时,在信用公示栏目-信用反馈中,新增一个信用问答数据
 *           通过新增信用问答界面，并根据我所提交的表扬信息数据进行新增,以便于我维护信用问答信息
 * @Scenario: 新增信用问答
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qa');
        $this->clear('pcore_member');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $qaInteractionArray = $this->qaInteraction()[0];

        $qaInteraction = new QA();

        $qaInteraction->getMember()->setId(1);
        $qaInteraction->getAcceptUserGroup()->setId(1);
        $qaInteraction->setTitle($qaInteractionArray['title']);
        $qaInteraction->setContent($qaInteractionArray['content']);

        return $qaInteraction->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();
        $this->assertTrue($result);

        $setQA = $this->qaInteraction()[0];
        
        $repository = new QARepository();
        $qaInteraction = $repository->fetchOne($result);
        $translator = new QADbTranslator();
        $qaInteractionArray = $translator->objectToArray($qaInteraction);

        $this->assertEquals($qaInteractionArray, $setQA);
    }
}
