<?php
namespace Base\Interaction\QA\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\QA;

use Base\Member\Model\NullMember;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行表扬时,在信用公示栏目-信用反馈中,新增一个信用问答数据
 *           通过新增信用问答界面，并根据我所提交的表扬信息数据进行新增,以便于我维护信用问答信息
 * @Scenario: 异常流程-新增信用问答-数据格式错误
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qa');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我调用添加函数
     */
    public function add()
    {
        $qaInteraction = new QA();
        $qaInteraction->setMember(NullMember::getInstance());

        return $qaInteraction->add();
    }

    /**
     * @Then: 期望数据返回失败
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertFalse($result);
    }
}
