<?php
namespace Base\Interaction\QA\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\QADbTranslator;
use Base\Interaction\Repository\QA\QARepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答管理权限，当我想要查看某个信用问答时,
 *           在政务网OA中的信用问答管理模块,我可以搜索信用问答
 *           通过信用问答的标题名称进行搜索 ,以便于我快速定位某条信用问答并进行下一步操作
 * @Scenario: 通过信用问答前台用户id搜索
 */
class MemberTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qa');
        $this->clear('pcore_member');
    }
    
    /**
     * @Given: 存在信用问答数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_qa' => $this->qaInteraction(),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 当我查看信用问答数据列表时
     */
    public function fetchQAList()
    {
        $repository = new QARepository();
 
        $filter['member'] = 1;

        list($qaInteractionList, $count) = $repository->filter($filter);
        
        unset($count);

        return $qaInteractionList;
    }

    /**
     * @Then: 我可以看到前台用户Id为1的所有信用问答数据
     */
    public function testViewQAList()
    {
        $setQAList = $this->qaInteraction();

        $qaInteractionList = $this->fetchQAList();
        $translator = new QADbTranslator();
        foreach ($qaInteractionList as $qaInteraction) {
            $qaInteractionArray[] = $translator->objectToArray($qaInteraction);
        }

        $this->assertEquals($qaInteractionArray, $setQAList);
        foreach ($qaInteractionArray as $qaInteraction) {
            $this->assertEquals(1, $qaInteraction['member_id']);
        }
    }
}
