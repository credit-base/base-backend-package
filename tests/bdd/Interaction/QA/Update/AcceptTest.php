<?php
namespace Base\Interaction\QA\Update;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\Model\Reply;
use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\UnAuditedQA;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Translator\ReplyDbTranslator;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Repository\Reply\ReplyRepository;
use Base\Interaction\Repository\QA\QARepository;
use Base\Interaction\Translator\UnAuditedQADbTranslator;
use Base\Interaction\Repository\QA\UnAuditedQARepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答管理权限,当我需要想要对某个信用问答数据进行受理处理时,在业务管理-信用问答中
 *           对前台用户提交上来的信用问答进行受理处理，并提交我填写的受理依据,以便于我维护信用问答信息
 * @Scenario: 受理信用问答
 */
class AcceptTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qa');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 我并未受理过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_qa' => $this->qaInteraction(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(1),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 获取需要受理的信用问答数据
     */
    public function fetchQA($id)
    {
        $repository = new QARepository();

        $qaInteraction = $repository->fetchOne($id);

        return $qaInteraction;
    }

    public function fetchUnAuditedQA($id)
    {
        $repository = new UnAuditedQARepository();

        $qaInteraction = $repository->fetchOne($id);

        return $qaInteraction;
    }

    public function fetchReply($id)
    {
        $repository = new ReplyRepository();

        $qaInteraction = $repository->fetchOne($id);

        return $qaInteraction;
    }

    /**
     * @When: 当我调用受理函数,期待返回true
     */
    public function accept()
    {
        $replyArray = $this->reply()[0];
        $reply = new Reply();
        $reply->setContent($replyArray['content']);
        $reply->setImages(json_decode($replyArray['images'], true));
        $reply->setAdmissibility($replyArray['admissibility']);
        $reply->getAcceptUserGroup()->setId($replyArray['accept_usergroup_id']);
        $reply->getCrew()->setId($replyArray['crew_id']);

        $qaInteraction = $this->fetchQA(1);
        $qaInteraction->setReply($reply);

        $unAuditedQA = new UnAuditedQA();
        $unAuditedQA->setId($qaInteraction->getId());
        $unAuditedQA->setTitle($qaInteraction->getTitle());
        $unAuditedQA->setContent($qaInteraction->getContent());
        $unAuditedQA->setMember($qaInteraction->getMember());
        $unAuditedQA->setAcceptUserGroup($qaInteraction->getAcceptUserGroup());
        $unAuditedQA->setApplyTitle($qaInteraction->getTitle());
        $unAuditedQA->setRelation($qaInteraction->getMember());
        $unAuditedQA->getApplyCrew()->setId(1);
        $unAuditedQA->setApplyUserGroup($qaInteraction->getAcceptUserGroup());
        $unAuditedQA->setOperationType(IApproveAble::OPERATION_TYPE['ACCEPT']);
        $unAuditedQA->setApplyInfoCategory(
            new ApplyInfoCategory(
                UnAuditedQA::APPLY_QA_CATEGORY,
                UnAuditedQA::APPLY_QA_TYPE
            )
        );
        $unAuditedQA->setReply($reply);

        $service = new AcceptInteractionService($unAuditedQA, $qaInteraction, $reply);

        return $service->accept();
    }

    /**
     * @Then: 可以查到受理的数据
     */
    public function testValidate()
    {
        $result = $this->accept();
        $this->assertTrue($result);

        //验证受理状态是否变为受理中
        $qaInteraction = $this->fetchQA(1);
        $this->assertEquals(IInteractionAble::ACCEPT_STATUS['ACCEPTING'], $qaInteraction->getAcceptStatus());
        $this->assertEquals(1, $qaInteraction->getReply()->getId());

        //验证是否新增一条回复信息
        $setReply = $this->reply()[0];
        $setReply['images'] = json_decode($setReply['images'], true);

        $reply = $this->fetchReply(1);
        $translator = new ReplyDbTranslator();
        $replyArray = $translator->objectToArray($reply);

        $this->assertEquals($replyArray, $setReply);

        //验证是否新增一条审核信息
        $setUnAuditedQA = $this->applyFormQA(IApproveAble::APPLY_STATUS['PENDING'])[0];

        $unAuditedQA = $this->fetchUnAuditedQA(1);
        $translator = new UnAuditedQADbTranslator();
        $unAuditedQAArray = $translator->objectToArray($unAuditedQA);
        
        $this->assertEquals($unAuditedQAArray, $setUnAuditedQA);
    }
}
