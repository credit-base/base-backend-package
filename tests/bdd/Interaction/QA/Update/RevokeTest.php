<?php
namespace Base\Interaction\QA\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\QA\QARepository;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的信用问答时,在用户中心-信用问答中,我可以管理我提交的信用问答数据
 *           可以撤销受理情况为未受理状态的信用问答,以便于我维护我的信用问答信息
 * @Scenario: 撤销信用问答
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qa');
    }

    /**
     * @Given: 存在需要撤销的信用问答数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_qa' => $this->qaInteraction()
        ]);
    }

    /**
     * @When: 获取需要撤销的信用问答数据
     */
    public function fetchQA($id)
    {
        $repository = new QARepository();

        $qaInteraction = $repository->fetchOne($id);

        return $qaInteraction;
    }

    /**
     * @And: 当我调用撤销函数,期待返回true
     */
    public function revoke()
    {
        $qaInteraction = $this->fetchQA(1);
        
        return $qaInteraction->revoke();
    }

    /**
     * @Then: 数据已经被撤销
     */
    public function testValidate()
    {
        $result = $this->revoke();

        $this->assertTrue($result);

        $qaInteraction = $this->fetchQA(1);

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['REVOKE'], $qaInteraction->getStatus());
        $this->assertEquals(Core::$container->get('time'), $qaInteraction->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $qaInteraction->getStatusTime());
    }
}
