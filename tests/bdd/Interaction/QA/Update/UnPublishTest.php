<?php
namespace Base\Interaction\QA\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\QA\QARepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答管理权限,当我需要想要对某个待审核的信用问答数据进行公示/取消公示时,
 *           在业务管理-信用问答表中,对已经审核通过的信用问答进行公示或取消公示的操作,以便于我维护信用问答信息
 * @Scenario: 取消公示信用问答
 */
class UnPublishTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qa');
    }

    /**
     * @Given: 存在需要取消公示的信用问答数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_qa' => $this->qaInteraction(
                1,
                IInteractionAble::INTERACTION_STATUS['PUBLISH'],
                IInteractionAble::ACCEPT_STATUS['COMPLETE']
            )
        ]);
    }

    /**
     * @When: 获取需要取消公示的信用问答数据
     */
    public function fetchQA($id)
    {
        $repository = new QARepository();

        $qaInteraction = $repository->fetchOne($id);

        return $qaInteraction;
    }

    /**
     * @And: 当我调用公示函数,期待返回true
     */
    public function unPublish()
    {
        $qaInteraction = $this->fetchQA(1);
        
        return $qaInteraction->unPublish();
    }

    /**
     * @Then: 数据已经被公示
     */
    public function testValidate()
    {
        $result = $this->unPublish();

        $this->assertTrue($result);

        $qaInteraction = $this->fetchQA(1);

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['NORMAL'], $qaInteraction->getStatus());
        $this->assertEquals(Core::$container->get('time'), $qaInteraction->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $qaInteraction->getStatusTime());
    }
}
