<?php
namespace Base\Interaction\QA\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\QADbTranslator;
use Base\Interaction\Repository\QA\QARepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答管理权限,在信用问答模块中
 *           查看前台用户提交的已审核通过的信用问答信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用问答信息
 * @Scenario: 查看信用问答数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qa');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 存在一条信用问答数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_qa' => $this->qaInteraction(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
            'pcore_reply' => $this->reply(1),
        ]);
    }

    /**
     * @When: 当我查看该条信用问答数据详情时
     */
    public function fetchQA($id)
    {
        $repository = new QARepository();

        $qaInteraction = $repository->fetchOne($id);

        return $qaInteraction;
    }

    /**
     * @Then: 我可以看见该条信用问答数据的全部信息
     */
    public function testViewQAList()
    {
        $id = 1;
        
        $setQA = $this->qaInteraction()[0];

        $qaInteraction = $this->fetchQA($id);
        $translator = new QADbTranslator();
        $qaInteractionArray = $translator->objectToArray($qaInteraction);

        $this->assertEquals($qaInteractionArray, $setQA);
    }
}
