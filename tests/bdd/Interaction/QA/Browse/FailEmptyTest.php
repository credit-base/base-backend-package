<?php
namespace Base\Interaction\QA\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\Repository\QA\QARepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答管理权限,在信用问答模块中
 *           查看前台用户提交的已审核通过的信用问答信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用问答信息
 * @Scenario: 查看信用问答数据列表-异常数据不存在
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在信用问答
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看信用问答列表时
     */
    public function fetchQAList()
    {
        $repository = new QARepository();

        list($qaInteractionList, $count) = $repository->filter([]);
        
        unset($count);

        return $qaInteractionList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewQA()
    {
        $qaInteractionList = $this->fetchQAList();

        $this->assertEmpty($qaInteractionList);
    }
}
