<?php
namespace Base\Interaction\Feedback\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\FeedbackDbTranslator;
use Base\Interaction\Repository\Feedback\FeedbackRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限,在问题反馈模块中
 *           查看前台用户提交的已审核通过的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的问题反馈信息
 * @Scenario: 查看问题反馈数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
        $this->clear('pcore_feedback');
        $this->clear('pcore_user_group');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 存在问题反馈数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
            'pcore_feedback' => $this->feedback(),
            'pcore_reply' => $this->reply(1),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看问题反馈数据列表时
     */
    public function fetchFeedbackList()
    {
        $repository = new FeedbackRepository();

        list($feedbackList, $count) = $repository->filter([]);
        
        unset($count);

        return $feedbackList;
    }

    /**
     * @Then: 我可以看到问题反馈数据的全部信息
     */
    public function testViewFeedbackList()
    {
        $setFeedbackList = $this->feedback();

        $feedbackList = $this->fetchFeedbackList();

        $translator = new FeedbackDbTranslator();
        foreach ($feedbackList as $feedback) {
            $feedbackArray[] = $translator->objectToArray($feedback);
        }

        $this->assertEquals($feedbackArray, $setFeedbackList);
    }
}
