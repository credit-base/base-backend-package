<?php
namespace Base\Interaction\Feedback\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\Repository\Feedback\FeedbackRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限,在问题反馈模块中
 *           查看前台用户提交的已审核通过的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的问题反馈信息
 * @Scenario: 查看问题反馈数据列表-异常数据不存在
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在问题反馈
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看问题反馈列表时
     */
    public function fetchFeedbackList()
    {
        $repository = new FeedbackRepository();

        list($feedbackList, $count) = $repository->filter([]);
        
        unset($count);

        return $feedbackList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewFeedback()
    {
        $feedbackList = $this->fetchFeedbackList();

        $this->assertEmpty($feedbackList);
    }
}
