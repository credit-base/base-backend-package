<?php
namespace Base\Interaction\Feedback\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\Feedback\FeedbackRepository;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的问题反馈时,在用户中心-问题反馈中,我可以管理我提交的问题反馈数据
 *           可以撤销受理情况为未受理状态的问题反馈,以便于我维护我的问题反馈信息
 * @Scenario: 撤销问题反馈
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_feedback');
    }

    /**
     * @Given: 存在需要撤销的问题反馈数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_feedback' => $this->feedback()
        ]);
    }

    /**
     * @When: 获取需要撤销的问题反馈数据
     */
    public function fetchFeedback($id)
    {
        $repository = new FeedbackRepository();

        $feedback = $repository->fetchOne($id);

        return $feedback;
    }

    /**
     * @And: 当我调用撤销函数,期待返回true
     */
    public function revoke()
    {
        $feedback = $this->fetchFeedback(1);
        
        return $feedback->revoke();
    }

    /**
     * @Then: 数据已经被撤销
     */
    public function testValidate()
    {
        $result = $this->revoke();

        $this->assertTrue($result);

        $feedback = $this->fetchFeedback(1);

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['REVOKE'], $feedback->getStatus());
        $this->assertEquals(Core::$container->get('time'), $feedback->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $feedback->getStatusTime());
    }
}
