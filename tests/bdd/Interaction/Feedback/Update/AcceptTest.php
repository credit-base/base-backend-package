<?php
namespace Base\Interaction\Feedback\Update;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\Model\Reply;
use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\UnAuditedFeedback;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Translator\ReplyDbTranslator;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Repository\Reply\ReplyRepository;
use Base\Interaction\Repository\Feedback\FeedbackRepository;
use Base\Interaction\Translator\UnAuditedFeedbackDbTranslator;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限,当我需要想要对某个问题反馈数据进行受理处理时,在业务管理-问题反馈中
 *           对前台用户提交上来的问题反馈进行受理处理，并提交我填写的受理依据,以便于我维护问题反馈信息
 * @Scenario: 受理问题反馈
 */
class AcceptTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_feedback');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 我并未受理过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_feedback' => $this->feedback(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(1),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 获取需要受理的问题反馈数据
     */
    public function fetchFeedback($id)
    {
        $repository = new FeedbackRepository();

        $feedback = $repository->fetchOne($id);

        return $feedback;
    }

    public function fetchUnAuditedFeedback($id)
    {
        $repository = new UnAuditedFeedbackRepository();

        $feedback = $repository->fetchOne($id);

        return $feedback;
    }

    public function fetchReply($id)
    {
        $repository = new ReplyRepository();

        $feedback = $repository->fetchOne($id);

        return $feedback;
    }

    /**
     * @When: 当我调用受理函数,期待返回true
     */
    public function accept()
    {
        $replyArray = $this->reply()[0];
        $reply = new Reply();
        $reply->setContent($replyArray['content']);
        $reply->setImages(json_decode($replyArray['images'], true));
        $reply->setAdmissibility($replyArray['admissibility']);
        $reply->getAcceptUserGroup()->setId($replyArray['accept_usergroup_id']);
        $reply->getCrew()->setId($replyArray['crew_id']);

        $feedback = $this->fetchFeedback(1);
        $feedback->setReply($reply);

        $unAuditedFeedback = new UnAuditedFeedback();
        $unAuditedFeedback->setId($feedback->getId());
        $unAuditedFeedback->setTitle($feedback->getTitle());
        $unAuditedFeedback->setContent($feedback->getContent());
        $unAuditedFeedback->setMember($feedback->getMember());
        $unAuditedFeedback->setAcceptUserGroup($feedback->getAcceptUserGroup());
        $unAuditedFeedback->setApplyTitle($feedback->getTitle());
        $unAuditedFeedback->setRelation($feedback->getMember());
        $unAuditedFeedback->getApplyCrew()->setId(1);
        $unAuditedFeedback->setApplyUserGroup($feedback->getAcceptUserGroup());
        $unAuditedFeedback->setOperationType(IApproveAble::OPERATION_TYPE['ACCEPT']);
        $unAuditedFeedback->setApplyInfoCategory(
            new ApplyInfoCategory(
                UnAuditedFeedback::APPLY_FEEDBACK_CATEGORY,
                UnAuditedFeedback::APPLY_FEEDBACK_TYPE
            )
        );
        $unAuditedFeedback->setReply($reply);

        $service = new AcceptInteractionService($unAuditedFeedback, $feedback, $reply);

        return $service->accept();
    }

    /**
     * @Then: 可以查到受理的数据
     */
    public function testValidate()
    {
        $result = $this->accept();
        $this->assertTrue($result);

        //验证受理状态是否变为受理中
        $feedback = $this->fetchFeedback(1);
        $this->assertEquals(IInteractionAble::ACCEPT_STATUS['ACCEPTING'], $feedback->getAcceptStatus());
        $this->assertEquals(1, $feedback->getReply()->getId());

        //验证是否新增一条回复信息
        $setReply = $this->reply()[0];
        $setReply['images'] = json_decode($setReply['images'], true);

        $reply = $this->fetchReply(1);
        $translator = new ReplyDbTranslator();
        $replyArray = $translator->objectToArray($reply);

        $this->assertEquals($replyArray, $setReply);

        //验证是否新增一条审核信息
        $setUnAuditedFeedback = $this->applyFormFeedback(IApproveAble::APPLY_STATUS['PENDING'])[0];

        $unAuditedFeedback = $this->fetchUnAuditedFeedback(1);
        $translator = new UnAuditedFeedbackDbTranslator();
        $unAuditedFeedbackArray = $translator->objectToArray($unAuditedFeedback);
        
        $this->assertEquals($unAuditedFeedbackArray, $setUnAuditedFeedback);
    }
}
