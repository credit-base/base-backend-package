<?php
namespace Base\Interaction\Feedback\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\Feedback;
use Base\Interaction\Translator\FeedbackDbTranslator;
use Base\Interaction\Repository\Feedback\FeedbackRepository;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行表扬时,在信用公示栏目-信用反馈中,新增一个问题反馈数据
 *           通过新增问题反馈界面，并根据我所提交的表扬信息数据进行新增,以便于我维护问题反馈信息
 * @Scenario: 新增问题反馈
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_feedback');
        $this->clear('pcore_member');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $feedbackArray = $this->feedback()[0];

        $feedback = new Feedback();

        $feedback->getMember()->setId(1);
        $feedback->getAcceptUserGroup()->setId(1);
        $feedback->setTitle($feedbackArray['title']);
        $feedback->setContent($feedbackArray['content']);

        return $feedback->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();
        $this->assertTrue($result);

        $setFeedback = $this->feedback()[0];
        
        $repository = new FeedbackRepository();
        $feedback = $repository->fetchOne($result);
        $translator = new FeedbackDbTranslator();
        $feedbackArray = $translator->objectToArray($feedback);

        $this->assertEquals($feedbackArray, $setFeedback);
    }
}
