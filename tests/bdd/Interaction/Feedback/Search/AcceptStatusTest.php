<?php
namespace Base\Interaction\Feedback\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Translator\FeedbackDbTranslator;
use Base\Interaction\Repository\Feedback\FeedbackRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限，当我想要查看某个问题反馈时,
 *           在政务网OA中的问题反馈管理模块,我可以搜索问题反馈
 *           通过问题反馈的标题名称进行搜索 ,以便于我快速定位某条问题反馈并进行下一步操作
 * @Scenario: 通过问题反馈受理状态搜索
 */
class AcceptStatusTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_feedback');
    }

    /**
     * @Given: 存在问题反馈数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_feedback' => $this->feedback()
        ]);
    }
    /**
     * @When: 当我查看问题反馈数据列表时
     */
    public function fetchFeedbackList()
    {
        $repository = new FeedbackRepository();
 
        $filter['acceptStatus'] = IInteractionAble::ACCEPT_STATUS['PENDING'];

        list($feedbackList, $count) = $repository->filter($filter);
        
        unset($count);

        return $feedbackList;
    }

    /**
     * @Then: 我可以看到问题反馈受理状态为"待受理"的问题反馈数据
     */
    public function testViewFeedbackList()
    {
        $setFeedbackList = $this->feedback();

        $translator = new FeedbackDbTranslator();
        $feedbackList = $this->fetchFeedbackList();
        foreach ($feedbackList as $feedback) {
            $feedbackArray[] = $translator->objectToArray($feedback);
        }

        $this->assertEquals($feedbackArray, $setFeedbackList);
        foreach ($feedbackArray as $feedback) {
            $this->assertEquals(IInteractionAble::ACCEPT_STATUS['PENDING'], $feedback['accept_status']);
        }
    }
}
