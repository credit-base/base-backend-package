<?php
namespace Base\Interaction\UnAuditedPraise\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬审核权限,当我需要想要对某个待审核的信用表扬数据进行审核时,在业务审核-审核表中
 *           对前台用户提交上来的信用表扬进行审核通过或审核驳回操作,以便于我维护信用表扬审核信息
 * @Scenario: 审核驳回
 */
class RejectTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 存在需要审核驳回的信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormPraise(IApproveAble::APPLY_STATUS['PENDING']),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 获取需要审核驳回的信用表扬数据
     */
    public function fetchUnAuditedPraise($id)
    {
        $repository = new UnAuditedPraiseRepository();

        $unAuditedPraise = $repository->fetchOne($id);

        return $unAuditedPraise;
    }

    /**
     * @And: 当我调用审核驳回函数,期待返回true
     */
    public function reject()
    {
        $unAuditedPraise = $this->fetchUnAuditedPraise(1);
        $unAuditedPraise->setRejectReason('测试驳回原因');
        $unAuditedPraise->getApplyCrew()->setId(1);
        
        return $unAuditedPraise->reject();
    }

    /**
     * @Then: 数据已经被审核驳回
     */
    public function testValidate()
    {
        $result = $this->reject();

        $this->assertTrue($result);

        $unAuditedPraise = $this->fetchUnAuditedPraise(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['REJECT'], $unAuditedPraise->getApplyStatus());
        $this->assertEquals('测试驳回原因', $unAuditedPraise->getRejectReason());
        $this->assertEquals(Core::$container->get('time'), $unAuditedPraise->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $unAuditedPraise->getStatusTime());
        $this->assertEquals(1, $unAuditedPraise->getApplyCrew()->getId());
    }
}
