<?php
namespace Base\Interaction\UnAuditedPraise\Update;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Repository\Reply\ReplyRepository;
use Base\Interaction\Service\ResubmitAcceptInteractionService;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

use Base\Common\Model\IApproveAble;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,
 *           当我需要想要对某个受理情况为已受理且审核状态为已驳回的信用表扬数据进行重新受理时,在业务管理-信用表扬的受理表中
 *           对前台用户提交上来的信用表扬进行重新受理，并提交我填写的受理依据,以便于我维护信用表扬信息
 * @Scenario: 重新受理信用表扬审核数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_reply');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 我并未重新受理过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormPraise(IApproveAble::APPLY_STATUS['REJECT']),
            'pcore_reply' => $this->reply(),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 获取需要受理的信用表扬数据
     */
    public function fetchUnAuditedPraise($id)
    {
        $repository = new UnAuditedPraiseRepository();

        $unAuditedPraise = $repository->fetchOne($id);

        return $unAuditedPraise;
    }

    public function fetchReply($id)
    {
        $repository = new ReplyRepository();

        $reply = $repository->fetchOne($id);

        return $reply;
    }
    /**
     * @When: 当我调用重新受理函数,期待返回true
     */
    public function resubmit()
    {
        $reply = $this->fetchReply(1);
        $reply->setContent('重新受理内容');
        $reply->setImages(array('重新受理图片'));
        $reply->setAdmissibility(2);

        $unAuditedPraise = $this->fetchUnAuditedPraise(1);
        $unAuditedPraise->setReply($reply);

        $service = new ResubmitAcceptInteractionService($unAuditedPraise, $reply);

        return $service->resubmitAccept();
    }

    /**
     * @Then: 可以查到重新受理的数据
     */
    public function testValidate()
    {
        $result = $this->resubmit();
        $this->assertTrue($result);
        
        $unAuditedPraise = $this->fetchUnAuditedPraise(1);

        $this->assertEquals('重新受理内容', $unAuditedPraise->getReply()->getContent());
        $this->assertEquals(array('重新受理图片'), $unAuditedPraise->getReply()->getImages());
        $this->assertEquals(2, $unAuditedPraise->getReply()->getAdmissibility());
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $unAuditedPraise->getApplyStatus());

        $reply = $this->fetchReply(1);
        $this->assertEquals('重新受理内容', $reply->getContent());
        $this->assertEquals(array('重新受理图片'), $reply->getImages());
        $this->assertEquals(2, $reply->getAdmissibility());
    }
}
