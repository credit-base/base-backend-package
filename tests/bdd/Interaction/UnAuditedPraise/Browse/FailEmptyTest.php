<?php
namespace Base\Interaction\UnAuditedPraise\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬审核权限,在信用表扬模块中
 *           查看前台用户提交的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用表扬信息
 * @Scenario: 异常数据不存在-查看信用表扬审核数据列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在信用表扬审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看信用表扬审核列表时
     */
    public function fetchUnAuditedPraiseList()
    {
        $repository = new UnAuditedPraiseRepository();

        list($unAuditedPraiseList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedPraiseList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewPraise()
    {
        $unAuditedPraiseList = $this->fetchUnAuditedPraiseList();

        $this->assertEmpty($unAuditedPraiseList);
    }
}
