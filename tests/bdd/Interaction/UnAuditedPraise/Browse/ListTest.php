<?php
namespace Base\Interaction\UnAuditedPraise\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedPraiseDbTranslator;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬审核权限,在信用表扬模块中
 *           查看前台用户提交的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用表扬信息
 * @Scenario: 查看信用表扬审核数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在信用表扬审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_member' => $this->member(),
            'pcore_apply_form' => $this->applyFormPraise(),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看信用表扬审核数据列表时
     */
    public function fetchUnAuditedPraiseList()
    {
        $repository = new UnAuditedPraiseRepository();

        list($unAuditedPraiseList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedPraiseList;
    }

    /**
     * @Then: 我可以看到信用表扬数据的全部信息
     */
    public function testViewPraiseList()
    {
        $setUnAuditedPraiseList = $this->applyFormPraise();

        foreach ($setUnAuditedPraiseList as $key => $setUnAuditedPraise) {
            unset($setUnAuditedPraise);
            $setUnAuditedPraiseList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedPraiseList = $this->fetchUnAuditedPraiseList();

        $translator = new UnAuditedPraiseDbTranslator();
        foreach ($unAuditedPraiseList as $unAuditedPraise) {
            $unAuditedPraiseArray[] = $translator->objectToArray($unAuditedPraise);
        }

        $this->assertEquals($unAuditedPraiseArray, $setUnAuditedPraiseList);
    }
}
