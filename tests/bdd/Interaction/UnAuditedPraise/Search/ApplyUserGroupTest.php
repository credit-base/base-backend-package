<?php
namespace Base\Interaction\UnAuditedPraise\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedPraiseDbTranslator;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬审核权限，当我想要查看某个信用表扬时,
 *           在政务网OA中的信用表扬审核模块,我可以搜索待审核的信用表扬
 *           通过信用表扬的标题名称进行搜索 ,以便于我快速定位某条信用表扬并进行下一步操作
 * @Scenario: 通过受理委办局搜索
 */
class ApplyUserGroupTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }
    
    /**
     * @Given: 存在信用表扬审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormPraise(),
            'pcore_user_group' => $this->userGroup(),
        ]);
    }

    /**
     * @When: 当我查看信用表扬审核数据列表时
     */
    public function fetchUnAuditedPraiseList()
    {
        $repository = new UnAuditedPraiseRepository();
 
        $filter['applyUserGroup'] = 1;

        list($unAuditedPraiseList, $count) = $repository->filter($filter);
        
        unset($count);

        return $unAuditedPraiseList;
    }

    /**
     * @Then: 我可以看到受理委办局Id为1的所有信用表扬审核数据
     */
    public function testViewUnAuditedPraiseList()
    {
        $setUnAuditedPraiseList = $this->applyFormPraise();

        foreach ($setUnAuditedPraiseList as $key => $setUnAuditedPraise) {
            unset($setUnAuditedPraise);
            $setUnAuditedPraiseList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedPraiseList = $this->fetchUnAuditedPraiseList();
        $translator = new UnAuditedPraiseDbTranslator();
        foreach ($unAuditedPraiseList as $unAuditedPraise) {
            $unAuditedPraiseArray[] = $translator->objectToArray($unAuditedPraise);
        }

        $this->assertEquals($unAuditedPraiseArray, $setUnAuditedPraiseList);

        foreach ($unAuditedPraiseArray as $unAuditedPraise) {
            $this->assertEquals(1, $unAuditedPraise['apply_usergroup_id']);
        }
    }
}
