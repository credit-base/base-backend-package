<?php
namespace Base\Interaction\UnAuditedComplaint\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedComplaintDbTranslator;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉审核权限,在信用投诉模块中
 *           查看前台用户提交的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用投诉信息
 * @Scenario: 查看信用投诉审核数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在信用投诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_member' => $this->member(),
            'pcore_apply_form' => $this->applyFormComplaint(),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看信用投诉审核数据列表时
     */
    public function fetchUnAuditedComplaintList()
    {
        $repository = new UnAuditedComplaintRepository();

        list($unAuditedComplaintList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedComplaintList;
    }

    /**
     * @Then: 我可以看到信用投诉数据的全部信息
     */
    public function testViewComplaintList()
    {
        $setUnAuditedComplaintList = $this->applyFormComplaint();

        foreach ($setUnAuditedComplaintList as $key => $setUnAuditedComplaint) {
            unset($setUnAuditedComplaint);
            $setUnAuditedComplaintList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedComplaintList = $this->fetchUnAuditedComplaintList();

        $translator = new UnAuditedComplaintDbTranslator();
        foreach ($unAuditedComplaintList as $unAuditedComplaint) {
            $unAuditedComplaintArray[] = $translator->objectToArray($unAuditedComplaint);
        }

        $this->assertEquals($unAuditedComplaintArray, $setUnAuditedComplaintList);
    }
}
