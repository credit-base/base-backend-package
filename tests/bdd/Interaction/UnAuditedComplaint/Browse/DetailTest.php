<?php
namespace Base\Interaction\UnAuditedComplaint\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedComplaintDbTranslator;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉审核权限,在信用投诉模块中
 *           查看前台用户提交的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用投诉信息
 * @Scenario: 查看信用投诉审核数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在一条信用投诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormComplaint(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(),
            'pcore_member' => $this->member()
        ]);
    }

    /**
     * @When: 当我查看该条信用投诉审核数据详情时
     */
    public function fetchUnAuditedComplaint($id)
    {
        $repository = new UnAuditedComplaintRepository();

        $unAuditedComplaint = $repository->fetchOne($id);

        return $unAuditedComplaint;
    }

    /**
     * @Then: 我可以看见该条信用投诉审核数据的全部信息
     */
    public function testViewUnAuditedComplaintList()
    {
        $id = 1;
        
        $setUnAuditedComplaint = $this->applyFormComplaint()[0];
        $setUnAuditedComplaint['apply_form_id'] = $id;

        $unAuditedComplaint = $this->fetchUnAuditedComplaint($id);
        $translator = new UnAuditedComplaintDbTranslator();
        $unAuditedComplaintArray = $translator->objectToArray($unAuditedComplaint);

        $this->assertEquals($unAuditedComplaintArray, $setUnAuditedComplaint);
    }
}
