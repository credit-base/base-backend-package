<?php
namespace Base\Interaction\UnAuditedComplaint\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉审核权限,在信用投诉模块中
 *           查看前台用户提交的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用投诉信息
 * @Scenario: 异常数据不存在-查看信用投诉审核数据列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在信用投诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看信用投诉审核列表时
     */
    public function fetchUnAuditedComplaintList()
    {
        $repository = new UnAuditedComplaintRepository();

        list($unAuditedComplaintList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedComplaintList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewComplaint()
    {
        $unAuditedComplaintList = $this->fetchUnAuditedComplaintList();

        $this->assertEmpty($unAuditedComplaintList);
    }
}
