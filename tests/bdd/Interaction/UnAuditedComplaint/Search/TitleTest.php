<?php
namespace Base\Interaction\UnAuditedComplaint\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedComplaintDbTranslator;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉审核权限，当我想要查看某个信用投诉时,
 *           在政务网OA中的信用投诉审核模块,我可以搜索待审核的信用投诉
 *           通过信用投诉的标题名称进行搜索 ,以便于我快速定位某条信用投诉并进行下一步操作
 * @Scenario: 通过信用投诉标题搜索
 */
class TitleTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
    }

    /**
     * @Given: 存在信用投诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormComplaint()
        ]);
    }
    /**
     * @When: 当我查看信用投诉审核数据列表时
     */
    public function fetchUnAuditedComplaintList()
    {
        $repository = new UnAuditedComplaintRepository();
 
        $filter['title'] = '投诉';

        list($unAuditedComplaintList, $count) = $repository->filter($filter);
        
        unset($count);

        return $unAuditedComplaintList;
    }

    /**
     * @Then: 我可以看到标题中带"投诉"的所有信用投诉审核数据
     */
    public function testViewUnAuditedComplaintList()
    {
        $setUnAuditedComplaintList = $this->applyFormComplaint();

        foreach ($setUnAuditedComplaintList as $key => $setUnAuditedComplaint) {
            unset($setUnAuditedComplaint);
            $setUnAuditedComplaintList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedComplaintList = $this->fetchUnAuditedComplaintList();
        $translator = new UnAuditedComplaintDbTranslator();
        foreach ($unAuditedComplaintList as $unAuditedComplaint) {
            $unAuditedComplaintArray[] = $translator->objectToArray($unAuditedComplaint);
        }

        $this->assertEquals($unAuditedComplaintArray, $setUnAuditedComplaintList);

        foreach ($unAuditedComplaintArray as $unAuditedComplaint) {
            $this->assertEquals('企业投诉', $unAuditedComplaint['title']);
        }
    }
}
