<?php
namespace Base\Interaction\UnAuditedComplaint\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Repository\Complaint\ComplaintRepository;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉审核权限,当我需要想要对某个待审核的信用投诉数据进行审核时,在业务审核-审核表中
 *           对前台用户提交上来的信用投诉进行审核通过或审核驳回操作,以便于我维护信用投诉审核信息
 * @Scenario: 审核通过
 */
class ApproveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_reply');
        $this->clear('pcore_member');
        $this->clear('pcore_complaint');
    }

    /**
     * @Given: 存在一条待审核的信用投诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_complaint' => $this->complaint(1),
            'pcore_apply_form' => $this->applyFormComplaint(IApproveAble::APPLY_STATUS['PENDING']),
            'pcore_crew' => $this->crew(2),
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
            'pcore_reply' => $this->reply()
        ]);
    }

    /**
     * @When: 获取需要审核通过的信用投诉
     */
    public function fetchUnAuditedComplaint($id)
    {
        $repository = new UnAuditedComplaintRepository();

        $unAuditedComplaint = $repository->fetchOne($id);

        return $unAuditedComplaint;
    }

    /**
     * @And: 当我调用审核通过函数,期待返回true
     */
    public function approve()
    {
        $unAuditedComplaint = $this->fetchUnAuditedComplaint(1);
        $unAuditedComplaint->getApplyCrew()->setId(1);
        
        return $unAuditedComplaint->approve();
    }

    /**
     * @Then: 数据已经被审核通过
     */
    public function testValidate()
    {
        $result = $this->approve();

        $this->assertTrue($result);

        $unAuditedComplaint = $this->fetchUnAuditedComplaint(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['APPROVE'], $unAuditedComplaint->getApplyStatus());
        $this->assertEquals(Core::$container->get('time'), $unAuditedComplaint->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $unAuditedComplaint->getStatusTime());
        $this->assertEquals(1, $unAuditedComplaint->getApplyCrew()->getId());
    }
}
