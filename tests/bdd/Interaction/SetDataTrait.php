<?php
namespace Base\Interaction;

use Marmot\Core;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\UnAuditedQA;
use Base\Interaction\Model\UnAuditedPraise;
use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Model\UnAuditedFeedback;
use Base\Interaction\Model\UnAuditedComplaint;

use Base\Common\Model\IApproveAble;

trait SetDataTrait
{
    protected function userGroup() : array
    {
        return
        [
            [
                'user_group_id' => 1,
                'name' => '发展和改革委员会',
                'short_name' => '发改委',
                'status' => UserGroup::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }

    protected function crew($id = 1) : array
    {
        return
        [
            [
                'crew_id' => $id,
                'user_name' => '18800000000',
                'cellphone' => '18800000000',
                'real_name' => '张科',
                'card_id' => '610424198810202422',
                'status' => 0,
                'category' => Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'],
                'purview' => json_encode(array(1,2,3,8)),
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'user_group_id' => 1,
                'department_id' => 1,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function appeal(
        $replyId = 0,
        $status = IInteractionAble::INTERACTION_STATUS['NORMAL'],
        $acceptStatus = IInteractionAble::ACCEPT_STATUS['PENDING']
    ) : array {
        return
        [
            [
                'appeal_id' => '1',
                'certificates' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg'))),
                'title' => '个人申诉',
                'content' => '个人申诉',
                'accept_status' => $acceptStatus,
                'member_id' => 1,
                'reply_id' => $replyId,
                'accept_usergroup_id' => 1,
                'status' => $status,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '张文科',
                'identify' => '412825199009073428',
                'type' => IInteractionAble::TYPE['PERSONAL'],
                'contact' => '029-87654330',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ]
        ];
    }

    protected function complaint(
        $replyId = 0,
        $status = IInteractionAble::INTERACTION_STATUS['NORMAL'],
        $acceptStatus = IInteractionAble::ACCEPT_STATUS['PENDING']
    ) : array {
        return
        [
            [
                'complaint_id' => '1',
                'subject' => '陈锋',
                'title' => '企业投诉',
                'content' => '企业投诉',
                'accept_status' => $acceptStatus,
                'member_id' => 1,
                'reply_id' => $replyId,
                'accept_usergroup_id' => 1,
                'status' => $status,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '西安传媒科技有限公司',
                'identify' => '91282519820907342X',
                'type' => IInteractionAble::TYPE['ENTERPRISE'],
                'contact' => '029-87654320',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ]
        ];
    }

    protected function praise(
        $replyId = 0,
        $status = IInteractionAble::INTERACTION_STATUS['NORMAL'],
        $acceptStatus = IInteractionAble::ACCEPT_STATUS['PENDING']
    ) : array {
        return
        [
            [
                'praise_id' => '1',
                'subject' => '西安传媒科技有限公司',
                'title' => '企业表扬',
                'content' => '企业表扬',
                'accept_status' => $acceptStatus,
                'member_id' => 1,
                'reply_id' => $replyId,
                'accept_usergroup_id' => 1,
                'status' => $status,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '西安网络科技有限公司',
                'identify' => '91282529820907342X',
                'type' => IInteractionAble::TYPE['ENTERPRISE'],
                'contact' => '029-89664320',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ]
        ];
    }

    protected function feedback(
        $replyId = 0,
        $status = IInteractionAble::INTERACTION_STATUS['NORMAL'],
        $acceptStatus = IInteractionAble::ACCEPT_STATUS['PENDING']
    ) : array {
        return
        [
            [
                'feedback_id' => '1',
                'title' => '问题反馈',
                'content' => '问题反馈',
                'accept_status' => $acceptStatus,
                'member_id' => 1,
                'reply_id' => $replyId,
                'accept_usergroup_id' => 1,
                'status' => $status,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function qaInteraction(
        $replyId = 0,
        $status = IInteractionAble::INTERACTION_STATUS['NORMAL'],
        $acceptStatus = IInteractionAble::ACCEPT_STATUS['PENDING']
    ) : array {
        return
        [
            [
                'qa_id' => '1',
                'title' => '信用问答',
                'content' => '信用问答',
                'accept_status' => $acceptStatus,
                'member_id' => 1,
                'reply_id' => $replyId,
                'accept_usergroup_id' => 1,
                'status' => $status,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function reply() : array
    {
        return
        [
            [
                'reply_id' => 1,
                'content' => '回复内容',
                'images' => json_encode(array(array('name'=>'回复图片名称', 'identify'=>'回复图片地址.jpg'))),
                'accept_usergroup_id' => 1,
                'crew_id' => 1,
                'admissibility' => IInteractionAble::ADMISSIBILITY['INADMISSIBLE'],
                'status' => Reply::STATUS_NORMAL,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function applyFormAppeal(
        $applyStatus = IApproveAble::APPLY_STATUS['APPROVE']
    ) : array {
        $applyInfo = $this->appeal(1)[0];
        $applyInfo['certificates'] = json_decode($applyInfo['certificates'], true);
        $applyInfo['images'] = json_decode($applyInfo['images'], true);

        return
        [
            [
                'apply_form_id' => 1,
                'title' => '个人申诉',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfo))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedAppeal::APPLY_APPEAL_CATEGORY,
                'apply_info_type' => UnAuditedAppeal::APPLY_APPEAL_TYPE,
                'apply_status' => $applyStatus,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyFormComplaint(
        $applyStatus = IApproveAble::APPLY_STATUS['APPROVE']
    ) : array {
        $applyInfo = $this->complaint(1)[0];
        $applyInfo['images'] = json_decode($applyInfo['images'], true);
        return
        [
            [
                'apply_form_id' => 1,
                'title' => '企业投诉',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfo))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedComplaint::APPLY_COMPLAINT_CATEGORY,
                'apply_info_type' => UnAuditedComplaint::APPLY_COMPLAINT_TYPE,
                'apply_status' => $applyStatus,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyFormPraise(
        $applyStatus = IApproveAble::APPLY_STATUS['APPROVE']
    ) : array {
        $applyInfo = $this->praise(1)[0];
        $applyInfo['images'] = json_decode($applyInfo['images'], true);
        return
        [
            [
                'apply_form_id' => 1,
                'title' => '企业表扬',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfo))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedPraise::APPLY_PRAISE_CATEGORY,
                'apply_info_type' => UnAuditedPraise::APPLY_PRAISE_TYPE,
                'apply_status' => $applyStatus,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyFormFeedback(
        $applyStatus = IApproveAble::APPLY_STATUS['APPROVE']
    ) : array {
        $applyInfo = $this->feedback(1)[0];
        return
        [
            [
                'apply_form_id' => 1,
                'title' => '问题反馈',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfo))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedFeedback::APPLY_FEEDBACK_CATEGORY,
                'apply_info_type' => UnAuditedFeedback::APPLY_FEEDBACK_TYPE,
                'apply_status' => $applyStatus,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyFormQA(
        $applyStatus = IApproveAble::APPLY_STATUS['APPROVE']
    ) : array {
        $applyInfo = $this->qaInteraction(1)[0];
        return
        [
            [
                'apply_form_id' => 1,
                'title' => '信用问答',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfo))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedQA::APPLY_QA_CATEGORY,
                'apply_info_type' => UnAuditedQA::APPLY_QA_TYPE,
                'apply_status' => $applyStatus,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
