<?php
namespace Base\Interaction\Praise\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\Praise\PraiseRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,当我需要想要对某个待审核的信用表扬数据进行公示/取消公示时,
 *           在业务管理-信用表扬表中,对已经审核通过的信用表扬进行公示或取消公示的操作,以便于我维护信用表扬信息
 * @Scenario: 取消公示信用表扬
 */
class UnPublishTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_praise');
    }

    /**
     * @Given: 存在需要取消公示的信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_praise' => $this->praise(
                1,
                IInteractionAble::INTERACTION_STATUS['PUBLISH'],
                IInteractionAble::ACCEPT_STATUS['COMPLETE']
            )
        ]);
    }

    /**
     * @When: 获取需要取消公示的信用表扬数据
     */
    public function fetchPraise($id)
    {
        $repository = new PraiseRepository();

        $praise = $repository->fetchOne($id);

        return $praise;
    }

    /**
     * @And: 当我调用公示函数,期待返回true
     */
    public function unPublish()
    {
        $praise = $this->fetchPraise(1);
        
        return $praise->unPublish();
    }

    /**
     * @Then: 数据已经被公示
     */
    public function testValidate()
    {
        $result = $this->unPublish();

        $this->assertTrue($result);

        $praise = $this->fetchPraise(1);

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['NORMAL'], $praise->getStatus());
        $this->assertEquals(Core::$container->get('time'), $praise->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $praise->getStatusTime());
    }
}
