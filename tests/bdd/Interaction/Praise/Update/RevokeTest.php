<?php
namespace Base\Interaction\Praise\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\Praise\PraiseRepository;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的信用表扬时,在用户中心-信用表扬中,我可以管理我提交的信用表扬数据
 *           可以撤销受理情况为未受理状态的信用表扬,以便于我维护我的信用表扬信息
 * @Scenario: 撤销信用表扬
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_praise');
    }

    /**
     * @Given: 存在需要撤销的信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_praise' => $this->praise()
        ]);
    }

    /**
     * @When: 获取需要撤销的信用表扬数据
     */
    public function fetchPraise($id)
    {
        $repository = new PraiseRepository();

        $praise = $repository->fetchOne($id);

        return $praise;
    }

    /**
     * @And: 当我调用撤销函数,期待返回true
     */
    public function revoke()
    {
        $praise = $this->fetchPraise(1);
        
        return $praise->revoke();
    }

    /**
     * @Then: 数据已经被撤销
     */
    public function testValidate()
    {
        $result = $this->revoke();

        $this->assertTrue($result);

        $praise = $this->fetchPraise(1);

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['REVOKE'], $praise->getStatus());
        $this->assertEquals(Core::$container->get('time'), $praise->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $praise->getStatusTime());
    }
}
