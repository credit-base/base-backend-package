<?php
namespace Base\Interaction\Praise\Update;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\Model\Reply;
use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\UnAuditedPraise;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Translator\ReplyDbTranslator;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Repository\Reply\ReplyRepository;
use Base\Interaction\Repository\Praise\PraiseRepository;
use Base\Interaction\Translator\UnAuditedPraiseDbTranslator;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,当我需要想要对某个信用表扬数据进行受理处理时,在业务管理-信用表扬中
 *           对前台用户提交上来的信用表扬进行受理处理，并提交我填写的受理依据,以便于我维护信用表扬信息
 * @Scenario: 受理信用表扬
 */
class AcceptTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_praise');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 我并未受理过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_praise' => $this->praise(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(1),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 获取需要受理的信用表扬数据
     */
    public function fetchPraise($id)
    {
        $repository = new PraiseRepository();

        $praise = $repository->fetchOne($id);

        return $praise;
    }

    public function fetchUnAuditedPraise($id)
    {
        $repository = new UnAuditedPraiseRepository();

        $praise = $repository->fetchOne($id);

        return $praise;
    }

    public function fetchReply($id)
    {
        $repository = new ReplyRepository();

        $praise = $repository->fetchOne($id);

        return $praise;
    }

    /**
     * @When: 当我调用受理函数,期待返回true
     */
    public function accept()
    {
        $replyArray = $this->reply()[0];
        $reply = new Reply();
        $reply->setContent($replyArray['content']);
        $reply->setImages(json_decode($replyArray['images'], true));
        $reply->setAdmissibility($replyArray['admissibility']);
        $reply->getAcceptUserGroup()->setId($replyArray['accept_usergroup_id']);
        $reply->getCrew()->setId($replyArray['crew_id']);

        $praise = $this->fetchPraise(1);
        $praise->setReply($reply);

        $unAuditedPraise = new UnAuditedPraise();
        $unAuditedPraise->setId($praise->getId());
        $unAuditedPraise->setTitle($praise->getTitle());
        $unAuditedPraise->setContent($praise->getContent());
        $unAuditedPraise->setMember($praise->getMember());
        $unAuditedPraise->setAcceptUserGroup($praise->getAcceptUserGroup());
        $unAuditedPraise->setName($praise->getName());
        $unAuditedPraise->setIdentify($praise->getIdentify());
        $unAuditedPraise->setType($praise->getType());
        $unAuditedPraise->setContact($praise->getContact());
        $unAuditedPraise->setImages($praise->getImages());
        $unAuditedPraise->setSubject($praise->getSubject());
        $unAuditedPraise->setApplyTitle($praise->getTitle());
        $unAuditedPraise->setRelation($praise->getMember());
        $unAuditedPraise->getApplyCrew()->setId(1);
        $unAuditedPraise->setApplyUserGroup($praise->getAcceptUserGroup());
        $unAuditedPraise->setOperationType(IApproveAble::OPERATION_TYPE['ACCEPT']);
        $unAuditedPraise->setApplyInfoCategory(
            new ApplyInfoCategory(
                UnAuditedPraise::APPLY_PRAISE_CATEGORY,
                UnAuditedPraise::APPLY_PRAISE_TYPE
            )
        );
        $unAuditedPraise->setReply($reply);

        $service = new AcceptInteractionService($unAuditedPraise, $praise, $reply);

        return $service->accept();
    }

    /**
     * @Then: 可以查到受理的数据
     */
    public function testValidate()
    {
        $result = $this->accept();
        $this->assertTrue($result);

        //验证受理状态是否变为受理中
        $praise = $this->fetchPraise(1);
        $this->assertEquals(IInteractionAble::ACCEPT_STATUS['ACCEPTING'], $praise->getAcceptStatus());
        $this->assertEquals(1, $praise->getReply()->getId());

        //验证是否新增一条回复信息
        $setReply = $this->reply()[0];
        $setReply['images'] = json_decode($setReply['images'], true);

        $reply = $this->fetchReply(1);
        $translator = new ReplyDbTranslator();
        $replyArray = $translator->objectToArray($reply);

        $this->assertEquals($replyArray, $setReply);

        //验证是否新增一条审核信息
        $setUnAuditedPraise = $this->applyFormPraise(IApproveAble::APPLY_STATUS['PENDING'])[0];

        $unAuditedPraise = $this->fetchUnAuditedPraise(1);
        $translator = new UnAuditedPraiseDbTranslator();
        $unAuditedPraiseArray = $translator->objectToArray($unAuditedPraise);
        
        $this->assertEquals($unAuditedPraiseArray, $setUnAuditedPraise);
    }
}
