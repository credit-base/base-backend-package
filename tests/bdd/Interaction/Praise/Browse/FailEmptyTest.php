<?php
namespace Base\Interaction\Praise\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\Repository\Praise\PraiseRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,在信用表扬模块中
 *           查看前台用户提交的已审核通过的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用表扬信息
 * @Scenario: 查看信用表扬数据列表-异常数据不存在
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在信用表扬
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看信用表扬列表时
     */
    public function fetchPraiseList()
    {
        $repository = new PraiseRepository();

        list($praiseList, $count) = $repository->filter([]);
        
        unset($count);

        return $praiseList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewPraise()
    {
        $praiseList = $this->fetchPraiseList();

        $this->assertEmpty($praiseList);
    }
}
