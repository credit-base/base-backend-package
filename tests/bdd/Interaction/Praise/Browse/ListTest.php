<?php
namespace Base\Interaction\Praise\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\PraiseDbTranslator;
use Base\Interaction\Repository\Praise\PraiseRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,在信用表扬模块中
 *           查看前台用户提交的已审核通过的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用表扬信息
 * @Scenario: 查看信用表扬数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
        $this->clear('pcore_praise');
        $this->clear('pcore_user_group');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 存在信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_member' => $this->member(),
            'pcore_praise' => $this->praise(),
            'pcore_reply' => $this->reply(1),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看信用表扬数据列表时
     */
    public function fetchPraiseList()
    {
        $repository = new PraiseRepository();

        list($praiseList, $count) = $repository->filter([]);
        
        unset($count);

        return $praiseList;
    }

    /**
     * @Then: 我可以看到信用表扬数据的全部信息
     */
    public function testViewPraiseList()
    {
        $setPraiseList = $this->praise();

        foreach ($setPraiseList as $key => $setPraise) {
            $setPraiseList[$key]['images'] = json_decode($setPraise['images'], true);
        }

        $praiseList = $this->fetchPraiseList();

        $translator = new PraiseDbTranslator();
        foreach ($praiseList as $praise) {
            $praiseArray[] = $translator->objectToArray($praise);
        }

        $this->assertEquals($praiseArray, $setPraiseList);
    }
}
