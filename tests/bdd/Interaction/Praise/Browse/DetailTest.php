<?php
namespace Base\Interaction\Praise\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\PraiseDbTranslator;
use Base\Interaction\Repository\Praise\PraiseRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,在信用表扬模块中
 *           查看前台用户提交的已审核通过的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用表扬信息
 * @Scenario: 查看信用表扬数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_praise');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 存在一条信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_praise' => $this->praise(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
            'pcore_reply' => $this->reply(1),
        ]);
    }

    /**
     * @When: 当我查看该条信用表扬数据详情时
     */
    public function fetchPraise($id)
    {
        $repository = new PraiseRepository();

        $praise = $repository->fetchOne($id);

        return $praise;
    }

    /**
     * @Then: 我可以看见该条信用表扬数据的全部信息
     */
    public function testViewPraiseList()
    {
        $id = 1;
        
        $setPraise = $this->praise()[0];
        $setPraise['images'] = json_decode($setPraise['images'], true);

        $praise = $this->fetchPraise($id);
        $translator = new PraiseDbTranslator();
        $praiseArray = $translator->objectToArray($praise);

        $this->assertEquals($praiseArray, $setPraise);
    }
}
