<?php
namespace Base\Interaction\Praise\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\PraiseDbTranslator;
use Base\Interaction\Repository\Praise\PraiseRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限，当我想要查看某个信用表扬时,
 *           在政务网OA中的信用表扬管理模块,我可以搜索信用表扬
 *           通过信用表扬的标题名称进行搜索 ,以便于我快速定位某条信用表扬并进行下一步操作
 * @Scenario: 通过信用表扬前台用户id搜索
 */
class MemberTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_praise');
        $this->clear('pcore_member');
    }
    
    /**
     * @Given: 存在信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_praise' => $this->praise(),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 当我查看信用表扬数据列表时
     */
    public function fetchPraiseList()
    {
        $repository = new PraiseRepository();
 
        $filter['member'] = 1;

        list($praiseList, $count) = $repository->filter($filter);
        
        unset($count);

        return $praiseList;
    }

    /**
     * @Then: 我可以看到前台用户Id为1的所有信用表扬数据
     */
    public function testViewPraiseList()
    {
        $setPraiseList = $this->praise();

        foreach ($setPraiseList as $key => $setPraise) {
            $setPraiseList[$key]['images'] = json_decode($setPraise['images'], true);
        }

        $praiseList = $this->fetchPraiseList();
        $translator = new PraiseDbTranslator();
        foreach ($praiseList as $praise) {
            $praiseArray[] = $translator->objectToArray($praise);
        }

        $this->assertEquals($praiseArray, $setPraiseList);
        foreach ($praiseArray as $praise) {
            $this->assertEquals(1, $praise['member_id']);
        }
    }
}
