<?php
namespace Base\Interaction\Praise\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Translator\PraiseDbTranslator;
use Base\Interaction\Repository\Praise\PraiseRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限，当我想要查看某个信用表扬时,
 *           在政务网OA中的信用表扬管理模块,我可以搜索信用表扬
 *           通过信用表扬的标题名称进行搜索 ,以便于我快速定位某条信用表扬并进行下一步操作
 * @Scenario: 通过信用表扬受理状态搜索
 */
class AcceptStatusTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_praise');
    }

    /**
     * @Given: 存在信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_praise' => $this->praise()
        ]);
    }
    /**
     * @When: 当我查看信用表扬数据列表时
     */
    public function fetchPraiseList()
    {
        $repository = new PraiseRepository();
 
        $filter['acceptStatus'] = IInteractionAble::ACCEPT_STATUS['PENDING'];

        list($praiseList, $count) = $repository->filter($filter);
        
        unset($count);

        return $praiseList;
    }

    /**
     * @Then: 我可以看到信用表扬受理状态为"待受理"的信用表扬数据
     */
    public function testViewPraiseList()
    {
        $setPraiseList = $this->praise();

        foreach ($setPraiseList as $key => $praise) {
            $setPraiseList[$key]['images'] = json_decode($praise['images'], true);
        }

        $translator = new PraiseDbTranslator();
        $praiseList = $this->fetchPraiseList();
        foreach ($praiseList as $praise) {
            $praiseArray[] = $translator->objectToArray($praise);
        }

        $this->assertEquals($praiseArray, $setPraiseList);
        foreach ($praiseArray as $praise) {
            $this->assertEquals(IInteractionAble::ACCEPT_STATUS['PENDING'], $praise['accept_status']);
        }
    }
}
