<?php
namespace Base\Interaction\Appeal\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\AppealDbTranslator;
use Base\Interaction\Repository\Appeal\AppealRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉管理权限,在异议申诉模块中
 *           查看前台用户提交的已审核通过的异议申诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的异议申诉信息
 * @Scenario: 查看异议申诉数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_appeal');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
    }

    /**
     * @Given: 存在一条异议申诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_appeal' => $this->appeal(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
            'pcore_reply' => $this->reply(1),
        ]);
    }

    /**
     * @When: 当我查看该条异议申诉数据详情时
     */
    public function fetchAppeal($id)
    {
        $repository = new AppealRepository();

        $appeal = $repository->fetchOne($id);

        return $appeal;
    }

    /**
     * @Then: 我可以看见该条异议申诉数据的全部信息
     */
    public function testViewAppealList()
    {
        $id = 1;
        
        $setAppeal = $this->appeal()[0];
        $setAppeal['certificates'] = json_decode($setAppeal['certificates'], true);
        $setAppeal['images'] = json_decode($setAppeal['images'], true);

        $appeal = $this->fetchAppeal($id);
        $translator = new AppealDbTranslator();
        $appealArray = $translator->objectToArray($appeal);

        $this->assertEquals($appealArray, $setAppeal);
    }
}
