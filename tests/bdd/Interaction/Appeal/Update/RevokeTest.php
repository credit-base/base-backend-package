<?php
namespace Base\Interaction\Appeal\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\Appeal\AppealRepository;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的异议申诉时,在用户中心-异议申诉中,我可以管理我提交的异议申诉数据
 *           可以撤销受理情况为未受理状态的异议申诉,以便于我维护我的异议申诉信息
 * @Scenario: 撤销异议申诉
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_appeal');
    }

    /**
     * @Given: 存在需要撤销的异议申诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_appeal' => $this->appeal()
        ]);
    }

    /**
     * @When: 获取需要撤销的异议申诉数据
     */
    public function fetchAppeal($id)
    {
        $repository = new AppealRepository();

        $appeal = $repository->fetchOne($id);

        return $appeal;
    }

    /**
     * @And: 当我调用撤销函数,期待返回true
     */
    public function revoke()
    {
        $appeal = $this->fetchAppeal(1);
        
        return $appeal->revoke();
    }

    /**
     * @Then: 数据已经被撤销
     */
    public function testValidate()
    {
        $result = $this->revoke();

        $this->assertTrue($result);

        $appeal = $this->fetchAppeal(1);

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['REVOKE'], $appeal->getStatus());
        $this->assertEquals(Core::$container->get('time'), $appeal->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $appeal->getStatusTime());
    }
}
