<?php
namespace Base\Interaction\Appeal\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\Appeal;
use Base\Interaction\Translator\AppealDbTranslator;
use Base\Interaction\Repository\Appeal\AppealRepository;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行投诉时,在信用公示栏目-信用反馈中,新增一个异议申诉数据
 *           通过新增异议申诉界面，并根据我所提交的投诉信息数据进行新增,以便于我维护异议申诉信息
 * @Scenario: 新增异议申诉
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_appeal');
        $this->clear('pcore_member');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $appealArray = $this->appeal()[0];

        $appeal = new Appeal();

        $appeal->getMember()->setId(1);
        $appeal->setType($appealArray['type']);
        $appeal->setName($appealArray['name']);
        $appeal->getAcceptUserGroup()->setId(1);
        $appeal->setTitle($appealArray['title']);
        $appeal->setContact($appealArray['contact']);
        $appeal->setContent($appealArray['content']);
        $appeal->setIdentify($appealArray['identify']);
        $appeal->setImages(json_decode($appealArray['images'], true));
        $appeal->setCertificates(json_decode($appealArray['certificates'], true));

        return $appeal->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();
        $this->assertTrue($result);

        $setAppeal = $this->appeal()[0];
        $setAppeal['images'] = json_decode($setAppeal['images'], true);
        $setAppeal['certificates'] = json_decode($setAppeal['certificates'], true);
        
        $repository = new AppealRepository();
        $appeal = $repository->fetchOne($result);
        $translator = new AppealDbTranslator();
        $appealArray = $translator->objectToArray($appeal);

        $this->assertEquals($appealArray, $setAppeal);
    }
}
