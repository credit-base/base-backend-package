<?php
namespace Base\Interaction\Appeal\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\AppealDbTranslator;
use Base\Interaction\Repository\Appeal\AppealRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉管理权限，当我想要查看某个异议申诉时,
 *           在政务网OA中的异议申诉管理模块,我可以搜索异议申诉
 *           通过异议申诉的标题名称进行搜索 ,以便于我快速定位某条异议申诉并进行下一步操作
 * @Scenario: 通过异议申诉受理委办局id搜索
 */
class AcceptUserGroupTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_appeal');
        $this->clear('pcore_user_group');
    }
    
    /**
     * @Given: 存在异议申诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_appeal' => $this->appeal(),
            'pcore_user_group' => $this->userGroup(),
        ]);
    }

    /**
     * @When: 当我查看异议申诉数据列表时
     */
    public function fetchAppealList()
    {
        $repository = new AppealRepository();
 
        $filter['acceptUserGroup'] = 1;

        list($appealList, $count) = $repository->filter($filter);
        
        unset($count);

        return $appealList;
    }

    /**
     * @Then: 我可以看到受理委办局Id为1的所有异议申诉数据
     */
    public function testViewAppealList()
    {
        $setAppealList = $this->appeal();

        foreach ($setAppealList as $key => $setAppeal) {
            $setAppealList[$key]['images'] = json_decode($setAppeal['images'], true);
            $setAppealList[$key]['certificates'] = json_decode($setAppeal['certificates'], true);
        }

        $appealList = $this->fetchAppealList();
        $translator = new AppealDbTranslator();
        foreach ($appealList as $appeal) {
            $appealArray[] = $translator->objectToArray($appeal);
        }

        $this->assertEquals($appealArray, $setAppealList);
        foreach ($appealArray as $appeal) {
            $this->assertEquals(1, $appeal['accept_usergroup_id']);
        }
    }
}
