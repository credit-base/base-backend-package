<?php
namespace Base\Interaction\Appeal\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Translator\AppealDbTranslator;
use Base\Interaction\Repository\Appeal\AppealRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉管理权限，当我想要查看某个异议申诉时,
 *           在政务网OA中的异议申诉管理模块,我可以搜索异议申诉
 *           通过异议申诉的标题名称进行搜索 ,以便于我快速定位某条异议申诉并进行下一步操作
 * @Scenario: 通过异议申诉状态搜索
 */
class StatusTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_appeal');
    }

    /**
     * @Given: 存在异议申诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_appeal' => $this->appeal()
        ]);
    }
    /**
     * @When: 当我查看异议申诉数据列表时
     */
    public function fetchAppealList()
    {
        $repository = new AppealRepository();
 
        $filter['status'] = IInteractionAble::INTERACTION_STATUS['NORMAL'];

        list($appealList, $count) = $repository->filter($filter);
        
        unset($count);

        return $appealList;
    }

    /**
     * @Then: 我可以看到异议申诉状态为"正常"的异议申诉数据
     */
    public function testViewAppealList()
    {
        $setAppealList = $this->appeal();

        foreach ($setAppealList as $key => $appeal) {
            $setAppealList[$key]['images'] = json_decode($appeal['images'], true);
            $setAppealList[$key]['certificates'] = json_decode($appeal['certificates'], true);
        }

        $appealList = $this->fetchAppealList();
        $translator = new AppealDbTranslator();
        foreach ($appealList as $appeal) {
            $appealArray[] = $translator->objectToArray($appeal);
        }

        $this->assertEquals($appealArray, $setAppealList);
        foreach ($appealArray as $appeal) {
            $this->assertEquals(IInteractionAble::INTERACTION_STATUS['NORMAL'], $appeal['status']);
        }
    }
}
