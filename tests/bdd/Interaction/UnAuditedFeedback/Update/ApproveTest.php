<?php
namespace Base\Interaction\UnAuditedFeedback\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Common\Model\IApproveAble;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Repository\Feedback\FeedbackRepository;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈审核权限,当我需要想要对某个待审核的问题反馈数据进行审核时,在业务审核-审核表中
 *           对前台用户提交上来的问题反馈进行审核通过或审核驳回操作,以便于我维护问题反馈审核信息
 * @Scenario: 审核通过
 */
class ApproveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_reply');
        $this->clear('pcore_member');
        $this->clear('pcore_feedback');
    }

    /**
     * @Given: 存在一条待审核的问题反馈审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_feedback' => $this->feedback(1),
            'pcore_apply_form' => $this->applyFormFeedback(IApproveAble::APPLY_STATUS['PENDING']),
            'pcore_crew' => $this->crew(2),
            'pcore_user_group' => $this->userGroup(),
            'pcore_member' => $this->member(),
            'pcore_reply' => $this->reply()
        ]);
    }

    /**
     * @When: 获取需要审核通过的问题反馈
     */
    public function fetchUnAuditedFeedback($id)
    {
        $repository = new UnAuditedFeedbackRepository();

        $unAuditedFeedback = $repository->fetchOne($id);

        return $unAuditedFeedback;
    }

    /**
     * @And: 当我调用审核通过函数,期待返回true
     */
    public function approve()
    {
        $unAuditedFeedback = $this->fetchUnAuditedFeedback(1);
        $unAuditedFeedback->getApplyCrew()->setId(1);
        
        return $unAuditedFeedback->approve();
    }

    /**
     * @Then: 数据已经被审核通过
     */
    public function testValidate()
    {
        $result = $this->approve();

        $this->assertTrue($result);

        $unAuditedFeedback = $this->fetchUnAuditedFeedback(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['APPROVE'], $unAuditedFeedback->getApplyStatus());
        $this->assertEquals(Core::$container->get('time'), $unAuditedFeedback->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $unAuditedFeedback->getStatusTime());
        $this->assertEquals(1, $unAuditedFeedback->getApplyCrew()->getId());
    }
}
