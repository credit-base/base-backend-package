<?php
namespace Base\Interaction\UnAuditedFeedback\Update;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Repository\Reply\ReplyRepository;
use Base\Interaction\Service\ResubmitAcceptInteractionService;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

use Base\Common\Model\IApproveAble;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限,
 *           当我需要想要对某个受理情况为已受理且审核状态为已驳回的问题反馈数据进行重新受理时,在业务管理-问题反馈的受理表中
 *           对前台用户提交上来的问题反馈进行重新受理，并提交我填写的受理依据,以便于我维护问题反馈信息
 * @Scenario: 重新受理问题反馈审核数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_reply');
        $this->clear('pcore_crew');
    }

    /**
     * @Given: 我并未重新受理过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormFeedback(IApproveAble::APPLY_STATUS['REJECT']),
            'pcore_reply' => $this->reply(),
            'pcore_crew' => $this->crew()
        ]);
    }

    /**
     * @When: 获取需要受理的问题反馈数据
     */
    public function fetchUnAuditedFeedback($id)
    {
        $repository = new UnAuditedFeedbackRepository();

        $unAuditedFeedback = $repository->fetchOne($id);

        return $unAuditedFeedback;
    }

    public function fetchReply($id)
    {
        $repository = new ReplyRepository();

        $reply = $repository->fetchOne($id);

        return $reply;
    }
    /**
     * @When: 当我调用重新受理函数,期待返回true
     */
    public function resubmit()
    {
        $reply = $this->fetchReply(1);
        $reply->setContent('重新受理内容');
        $reply->setImages(array('重新受理图片'));
        $reply->setAdmissibility(2);

        $unAuditedFeedback = $this->fetchUnAuditedFeedback(1);
        $unAuditedFeedback->setReply($reply);

        $service = new ResubmitAcceptInteractionService($unAuditedFeedback, $reply);

        return $service->resubmitAccept();
    }

    /**
     * @Then: 可以查到重新受理的数据
     */
    public function testValidate()
    {
        $result = $this->resubmit();
        $this->assertTrue($result);
        
        $unAuditedFeedback = $this->fetchUnAuditedFeedback(1);

        $this->assertEquals('重新受理内容', $unAuditedFeedback->getReply()->getContent());
        $this->assertEquals(array('重新受理图片'), $unAuditedFeedback->getReply()->getImages());
        $this->assertEquals(2, $unAuditedFeedback->getReply()->getAdmissibility());
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $unAuditedFeedback->getApplyStatus());

        $reply = $this->fetchReply(1);
        $this->assertEquals('重新受理内容', $reply->getContent());
        $this->assertEquals(array('重新受理图片'), $reply->getImages());
        $this->assertEquals(2, $reply->getAdmissibility());
    }
}
