<?php
namespace Base\Interaction\UnAuditedFeedback\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedFeedbackDbTranslator;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈审核权限,在问题反馈模块中
 *           查看前台用户提交的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的问题反馈信息
 * @Scenario: 查看问题反馈审核数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * @Given: 存在问题反馈审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_crew' => $this->crew(),
            'pcore_member' => $this->member(),
            'pcore_apply_form' => $this->applyFormFeedback(),
            'pcore_user_group' => $this->userGroup()
        ]);
    }

    /**
     * @When: 当我查看问题反馈审核数据列表时
     */
    public function fetchUnAuditedFeedbackList()
    {
        $repository = new UnAuditedFeedbackRepository();

        list($unAuditedFeedbackList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedFeedbackList;
    }

    /**
     * @Then: 我可以看到问题反馈数据的全部信息
     */
    public function testViewFeedbackList()
    {
        $setUnAuditedFeedbackList = $this->applyFormFeedback();

        foreach ($setUnAuditedFeedbackList as $key => $setUnAuditedFeedback) {
            unset($setUnAuditedFeedback);
            $setUnAuditedFeedbackList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedFeedbackList = $this->fetchUnAuditedFeedbackList();

        $translator = new UnAuditedFeedbackDbTranslator();
        foreach ($unAuditedFeedbackList as $unAuditedFeedback) {
            $unAuditedFeedbackArray[] = $translator->objectToArray($unAuditedFeedback);
        }

        $this->assertEquals($unAuditedFeedbackArray, $setUnAuditedFeedbackList);
    }
}
