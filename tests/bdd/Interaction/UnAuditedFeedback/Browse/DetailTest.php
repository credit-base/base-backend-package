<?php
namespace Base\Interaction\UnAuditedFeedback\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Member\SetDataTrait as MemberSetDataTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedFeedbackDbTranslator;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈审核权限,在问题反馈模块中
 *           查看前台用户提交的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的问题反馈信息
 * @Scenario: 查看问题反馈审核数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, MemberSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
    }

    /**
     * @Given: 存在一条问题反馈审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormFeedback(),
            'pcore_user_group' => $this->userGroup(),
            'pcore_crew' => $this->crew(),
            'pcore_member' => $this->member()
        ]);
    }

    /**
     * @When: 当我查看该条问题反馈审核数据详情时
     */
    public function fetchUnAuditedFeedback($id)
    {
        $repository = new UnAuditedFeedbackRepository();

        $unAuditedFeedback = $repository->fetchOne($id);

        return $unAuditedFeedback;
    }

    /**
     * @Then: 我可以看见该条问题反馈审核数据的全部信息
     */
    public function testViewUnAuditedFeedbackList()
    {
        $id = 1;
        
        $setUnAuditedFeedback = $this->applyFormFeedback()[0];
        $setUnAuditedFeedback['apply_form_id'] = $id;

        $unAuditedFeedback = $this->fetchUnAuditedFeedback($id);
        $translator = new UnAuditedFeedbackDbTranslator();
        $unAuditedFeedbackArray = $translator->objectToArray($unAuditedFeedback);

        $this->assertEquals($unAuditedFeedbackArray, $setUnAuditedFeedback);
    }
}
