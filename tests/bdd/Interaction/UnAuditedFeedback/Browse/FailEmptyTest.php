<?php
namespace Base\Interaction\UnAuditedFeedback\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈审核权限,在问题反馈模块中
 *           查看前台用户提交的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的问题反馈信息
 * @Scenario: 异常数据不存在-查看问题反馈审核数据列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在问题反馈审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看问题反馈审核列表时
     */
    public function fetchUnAuditedFeedbackList()
    {
        $repository = new UnAuditedFeedbackRepository();

        list($unAuditedFeedbackList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedFeedbackList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewFeedback()
    {
        $unAuditedFeedbackList = $this->fetchUnAuditedFeedbackList();

        $this->assertEmpty($unAuditedFeedbackList);
    }
}
