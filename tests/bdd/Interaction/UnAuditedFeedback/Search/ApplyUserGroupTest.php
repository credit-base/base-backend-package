<?php
namespace Base\Interaction\UnAuditedFeedback\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use Base\Interaction\SetDataTrait;
use Base\Interaction\Translator\UnAuditedFeedbackDbTranslator;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈审核权限，当我想要查看某个问题反馈时,
 *           在政务网OA中的问题反馈审核模块,我可以搜索待审核的问题反馈
 *           通过问题反馈的标题名称进行搜索 ,以便于我快速定位某条问题反馈并进行下一步操作
 * @Scenario: 通过受理委办局搜索
 */
class ApplyUserGroupTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }
    
    /**
     * @Given: 存在问题反馈审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form' => $this->applyFormFeedback(),
            'pcore_user_group' => $this->userGroup(),
        ]);
    }

    /**
     * @When: 当我查看问题反馈审核数据列表时
     */
    public function fetchUnAuditedFeedbackList()
    {
        $repository = new UnAuditedFeedbackRepository();
 
        $filter['applyUserGroup'] = 1;

        list($unAuditedFeedbackList, $count) = $repository->filter($filter);
        
        unset($count);

        return $unAuditedFeedbackList;
    }

    /**
     * @Then: 我可以看到受理委办局Id为1的所有问题反馈审核数据
     */
    public function testViewUnAuditedFeedbackList()
    {
        $setUnAuditedFeedbackList = $this->applyFormFeedback();

        foreach ($setUnAuditedFeedbackList as $key => $setUnAuditedFeedback) {
            unset($setUnAuditedFeedback);
            $setUnAuditedFeedbackList[$key]['apply_form_id'] = $key +1;
        }

        $unAuditedFeedbackList = $this->fetchUnAuditedFeedbackList();
        $translator = new UnAuditedFeedbackDbTranslator();
        foreach ($unAuditedFeedbackList as $unAuditedFeedback) {
            $unAuditedFeedbackArray[] = $translator->objectToArray($unAuditedFeedback);
        }

        $this->assertEquals($unAuditedFeedbackArray, $setUnAuditedFeedbackList);

        foreach ($unAuditedFeedbackArray as $unAuditedFeedback) {
            $this->assertEquals(1, $unAuditedFeedback['apply_usergroup_id']);
        }
    }
}
