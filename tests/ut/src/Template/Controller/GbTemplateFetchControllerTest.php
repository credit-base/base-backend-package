<?php
namespace Base\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use Base\Template\Model\NullGbTemplate;
use Base\Template\View\GbTemplateView;
use Base\Template\Model\GbTemplate;

class GbTemplateFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(GbTemplateFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockGbTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\Template\Repository\GbTemplateSdkRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockGbTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\View\GbTemplateView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockGbTemplateFetchController();

        $this->assertEquals(
            'gbTemplates',
            $controller->getResourceName()
        );
    }
}
