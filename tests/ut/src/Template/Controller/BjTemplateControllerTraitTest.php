<?php
namespace Base\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class BjTemplateControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockBjTemplateControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetTemplateWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Template\WidgetRule\TemplateWidgetRule',
            $this->trait->getTemplateWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Template\Repository\BjTemplateSdkRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}
