<?php
namespace Base\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Template\Model\GbTemplate;
use Base\Template\WidgetRule\TemplateWidgetRule;
use Base\Template\Repository\GbTemplateSdkRepository;
use Base\Template\Command\GbTemplate\AddGbTemplateCommand;
use Base\Template\Command\GbTemplate\EditGbTemplateCommand;

class GbTemplateOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new GbTemplateOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    /**
     * 测试 add 成功
     * 1. 为 GbTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 GbTemplate 类建立预言
     * 4. 为 GbTemplateSdkRepository 类建立预言, GbTemplateSdkRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的GbTemplate, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 为 GbTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(GbTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getTemplateWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 true
        $this->initAdd($controller, true);

        // 为 GbTemplate 类建立预言
        $gbTemplate  = $this->prophesize(GbTemplate::class);

        // 为 GbTemplateSdkRepository 类建立预言, GbTemplateSdkRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的GbTemplate, getRepository 方法被调用一次
        $id = 0;
        $repository = $this->prophesize(GbTemplateSdkRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($gbTemplate);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->add 方法被调用一次, 且返回结果为 true
        $result = $controller->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 GbTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 为 GbTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(GbTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getTemplateWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd($controller, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->add 方法被调用一次, 且返回结果为 false
        $result = $controller->add();
        $this->assertFalse($result);
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 TemplateWidgetRule 类建立预言, 验证请求参数, getTemplateWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddGbTemplateCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(GbTemplateOperateController $controller, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestData();
        $attributes = $data['attributes'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->initCommonWidgetRule($attributes, $controller);
        $this->initTemplateWidgetRule($attributes, $controller);

        // 为 CommandBus 类建立预言, 传入 AddGbTemplateCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AddGbTemplateCommand(
                    $attributes['name'],
                    $attributes['identify'],
                    $attributes['subjectCategory'],
                    $attributes['dimension'],
                    $attributes['exchangeFrequency'],
                    $attributes['infoClassify'],
                    $attributes['infoCategory'],
                    $attributes['description'],
                    $attributes['items']
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    /**
     * 测试 edit 成功
     * 1. 为 GbTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initEdit(), 期望结果为 true
     * 3. 为 GbTemplate 类建立预言
     * 4. 为 GbTemplateSdkRepository 类建立预言, GbTemplateSdkRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的GbTemplate, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 GbTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(GbTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getTemplateWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initEdit($controller, $id, true);

        // 为 GbTemplate 类建立预言
        $gbTemplate = $this->prophesize(GbTemplate::class);

        // 为 GbTemplateSdkRepository 类建立预言, GbTemplateSdkRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的GbTemplate, getRepository 方法被调用一次
        $repository = $this->prophesize(GbTemplateSdkRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($gbTemplate);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->edit 方法被调用一次, 且返回结果为 true
        $result = $controller->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 GbTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initEdit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 GbTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(GbTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getTemplateWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 false
        $this->initEdit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->edit 方法被调用一次, 且返回结果为 false
        $result = $controller->edit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 edit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 TemplateWidgetRule 类建立预言, 验证请求参数, getTemplateWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 EditGbTemplateCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEdit(GbTemplateOperateController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestData();
        $attributes = $data['attributes'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->initCommonWidgetRule($attributes, $controller);
        $this->initTemplateWidgetRule($attributes, $controller);

        // 为 CommandBus 类建立预言, 传入 EditGbTemplateCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditGbTemplateCommand(
                    $attributes['name'],
                    $attributes['identify'],
                    $attributes['subjectCategory'],
                    $attributes['dimension'],
                    $attributes['exchangeFrequency'],
                    $attributes['infoClassify'],
                    $attributes['infoCategory'],
                    $attributes['description'],
                    $attributes['items'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    private function mockRequestData() : array
    {
        return array(
            "attributes" => array(
                "name" => '登记信息',    //目录名称
                "identify" => 'DJXX',    //目录标识
                "subjectCategory" => array(1, 3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "exchangeFrequency" => 1,    //更新频率
                "infoClassify" => 1,    //信息分类
                "infoCategory" => 1,    //信息类别
                "description" => "目录描述信息",    //目录描述
                "items" => array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '50',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '信用主体代码',    //备注
                    ),
                    array(
                        "name" => '信息类别',    //信息项名称
                        "identify" => 'XXLB',    //数据标识
                        "type" => 5,    //数据类型
                        "length" => '50',    //数据长度
                        "options" => array(
                            "基础信息",
                            "守信信息",
                            "失信信息",
                            "其他信息"
                        ),    //可选范围
                        "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(1, 2),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                        "remarks" => '信息性质类型，支持单选',    //备注
                    )
                )
            )
        );
    }

    private function initCommonWidgetRule(array $attributes, $controller)
    {
        $exchangeFrequency = $attributes['exchangeFrequency'];
        $infoClassify = $attributes['infoClassify'];

        // 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用2次
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($exchangeFrequency), Argument::exact('exchangeFrequency'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($infoClassify), Argument::exact('infoClassify'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(2))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());
    }

    private function initTemplateWidgetRule(array $attributes, $controller)
    {
        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $subjectCategory = $attributes['subjectCategory'];
        $dimension = $attributes['dimension'];
        $infoCategory = $attributes['infoCategory'];
        $description = $attributes['description'];
        $items = $attributes['items'];

        // 为 TemplateWidgetRule 类建立预言, 验证请求参数, getTemplateWidgetRule 方法被调用6次
        $templateWidgetRule = $this->prophesize(TemplateWidgetRule::class);
        $templateWidgetRule->name(Argument::exact($name))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->identify(Argument::exact($identify))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->subjectCategory(Argument::exact($subjectCategory))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->dimension(Argument::exact($dimension))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->infoCategory(Argument::exact($infoCategory))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->description(Argument::exact($description))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->items(Argument::exact($subjectCategory), Argument::exact($items))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(7))
            ->method('getTemplateWidgetRule')
            ->willReturn($templateWidgetRule->reveal());
    }
}
