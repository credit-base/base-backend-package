<?php
namespace Base\Template\Controller;

use PHPUnit\Framework\TestCase;

class BaseTemplateFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(BaseTemplateFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockBaseTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\Template\Repository\BaseTemplateSdkRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockBaseTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\View\BaseTemplateView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockBaseTemplateFetchController();

        $this->assertEquals(
            'baseTemplates',
            $controller->getResourceName()
        );
    }
}
