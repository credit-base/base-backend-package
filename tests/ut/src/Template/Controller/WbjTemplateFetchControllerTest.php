<?php
namespace Base\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use Base\Template\Model\NullWbjTemplate;
use Base\Template\View\WbjTemplateView;
use Base\Template\Model\WbjTemplate;

class WbjTemplateFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(WbjTemplateFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockWbjTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\Template\Repository\WbjTemplateSdkRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockWbjTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\View\WbjTemplateView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockWbjTemplateFetchController();

        $this->assertEquals(
            'templates',
            $controller->getResourceName()
        );
    }
}
