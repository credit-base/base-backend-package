<?php
namespace Base\Template\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\WidgetRule\CommonWidgetRule;
use Base\Template\WidgetRule\TemplateWidgetRule;

class BaseTemplateControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockBaseTemplateControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetTemplateWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Template\WidgetRule\TemplateWidgetRule',
            $this->trait->getTemplateWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Template\Repository\BaseTemplateSdkRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    public function testValidateOperateScenario()
    {
        $controller = $this->getMockBuilder(MockBaseTemplateControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule', 'getTemplateWidgetRule']) ->getMock();

        $subjectCategory = array(1, 3);
        $items = array('items');
        $dimension = 1;
        $exchangeFrequency = 1;
        $infoClassify = 1;
        $infoCategory = 1;

        $templateWidgetRule = $this->prophesize(TemplateWidgetRule::class);
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($exchangeFrequency), Argument::exact('exchangeFrequency'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($infoClassify), Argument::exact('infoClassify'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($infoCategory), Argument::exact('infoCategory'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
            
        $templateWidgetRule->dimension(Argument::exact($dimension))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->subjectCategory(Argument::exact($subjectCategory))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->items(Argument::exact($subjectCategory), Argument::exact($items))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(3))
            ->method('getTemplateWidgetRule')
            ->willReturn($templateWidgetRule->reveal());
        $controller->expects($this->exactly(3))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateOperateScenarioPublic(
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $items
        );
        
        $this->assertTrue($result);
    }
}
