<?php
namespace Base\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;
use Base\Template\Model\NullQzjTemplate;
use Base\Template\Model\QzjTemplate;
use Base\Template\View\QzjTemplateView;

class QzjTemplateFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(QzjTemplateFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockQzjTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\Template\Repository\QzjTemplateSdkRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockQzjTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\View\QzjTemplateView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockQzjTemplateFetchController();

        $this->assertEquals(
            'qzjTemplates',
            $controller->getResourceName()
        );
    }
}
