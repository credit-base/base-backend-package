<?php
namespace Base\Template\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Template\Model\QzjTemplate;
use Base\Template\Repository\QzjTemplateSdkRepository;
use Base\Template\Command\QzjTemplate\AddQzjTemplateCommand;
use Base\Template\Command\QzjTemplate\EditQzjTemplateCommand;

class QzjTemplateOperateControllerTest extends TestCase
{
    private $qzjController;

    public function setUp()
    {
        $this->qzjController = new QzjTemplateOperateController();
    }

    public function tearDown()
    {
        unset($this->qzjController);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->qzjController
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->qzjController
        );
    }

    /**
     * 测试 add 成功
     * 1. 为 QzjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 QzjTemplate 类建立预言
     * 4. 为 QzjTemplateSdkRepository 类建立预言, QzjTemplateSdkRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的QzjTemplate, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且qzjController返回结果为 true
     * 6. qzjController->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 为 QzjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、getRepository、render方法
        $qzjController = $this->getMockBuilder(QzjTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateOperateScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 true
        $this->initAdd($qzjController, true);

        // 为 QzjTemplate 类建立预言
        $qzjTemplate  = $this->prophesize(QzjTemplate::class);

        // 为 QzjTemplateSdkRepository 类建立预言, QzjTemplateSdkRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的QzjTemplate, getRepository 方法被调用一次
        $id = 0;
        $repository = $this->prophesize(QzjTemplateSdkRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($qzjTemplate);

        $qzjController->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        // render 方法被调用一次, 且qzjController返回结果为 true
        $qzjController->expects($this->exactly(1))->method('render')->willReturn(true);

        // qzjController->add 方法被调用一次, 且返回结果为 true
        $result = $qzjController->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 QzjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且qzjController返回结果为 false
     * 4. qzjController->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 为 QzjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、displayError 方法
        $qzjController = $this->getMockBuilder(QzjTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateOperateScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd($qzjController, false);

        // displayError 方法被调用一次, 且qzjController返回结果为 false
        $qzjController->expects($this->exactly(1))->method('displayError')->willReturn(false);

        // qzjController->add 方法被调用一次, 且返回结果为 false
        $result = $qzjController->add();
        $this->assertFalse($result);
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 TemplateWidgetRule 类建立预言, 验证请求参数, getTemplateWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddQzjTemplateCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(QzjTemplateOperateController $qzjController, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestData();
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);
        $qzjController->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());
        $qzjController->expects($this->exactly(1))->method('validateOperateScenario')->willReturn(true);

        // 为 CommandBus 类建立预言, 传入 AddQzjTemplateCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AddQzjTemplateCommand(
                    $attributes['name'],
                    $attributes['identify'],
                    $attributes['subjectCategory'],
                    $attributes['dimension'],
                    $attributes['exchangeFrequency'],
                    $attributes['infoClassify'],
                    $attributes['infoCategory'],
                    $attributes['description'],
                    $attributes['items'],
                    $relationships['sourceUnit']['data'][0]['id'],
                    $attributes['category']
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $qzjController->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    /**
     * 测试 edit 成功
     * 1. 为 QzjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initEdit(), 期望结果为 true
     * 3. 为 QzjTemplate 类建立预言
     * 4. 为 QzjTemplateSdkRepository 类建立预言, QzjTemplateSdkRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的QzjTemplate, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且qzjController返回结果为 true
     * 6. qzjController->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 QzjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、getRepository、render方法
        $qzjController = $this->getMockBuilder(QzjTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateOperateScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initEdit($qzjController, $id, true);

        // 为 QzjTemplate 类建立预言
        $qzjTemplate = $this->prophesize(QzjTemplate::class);

        // 为 QzjTemplateSdkRepository 类建立预言, QzjTemplateSdkRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的QzjTemplate, getRepository 方法被调用一次
        $repository = $this->prophesize(QzjTemplateSdkRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($qzjTemplate);
        $qzjController->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        // render 方法被调用一次, 且qzjController返回结果为 true
        $qzjController->expects($this->exactly(1))->method('render')->willReturn(true);

        // qzjController->edit 方法被调用一次, 且返回结果为 true
        $result = $qzjController->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 QzjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initEdit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且qzjController返回结果为 false
     * 4. qzjController->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 QzjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、displayError 方法
        $qzjController = $this->getMockBuilder(QzjTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateOperateScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 false
        $this->initEdit($qzjController, $id, false);

        // displayError 方法被调用一次, 且qzjController返回结果为 false
        $qzjController->expects($this->exactly(1))->method('displayError')->willReturn(false);

        // qzjController->edit 方法被调用一次, 且返回结果为 false
        $result = $qzjController->edit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 edit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 TemplateWidgetRule 类建立预言, 验证请求参数, getTemplateWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 EditQzjTemplateCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEdit(QzjTemplateOperateController $qzjController, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestData();
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);
        $qzjController->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $qzjController->expects($this->exactly(1))->method('validateOperateScenario')->willReturn(true);

        // 为 CommandBus 类建立预言, 传入 EditQzjTemplateCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditQzjTemplateCommand(
                    $attributes['name'],
                    $attributes['identify'],
                    $attributes['subjectCategory'],
                    $attributes['dimension'],
                    $attributes['exchangeFrequency'],
                    $attributes['infoClassify'],
                    $attributes['infoCategory'],
                    $attributes['description'],
                    $attributes['items'],
                    $relationships['sourceUnit']['data'][0]['id'],
                    $attributes['category'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $qzjController->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    private function mockRequestData() : array
    {
        $data = array(
            "attributes" => array(
                "name" => '登记信息',    //目录名称
                "identify" => 'DJXX',    //目录标识
                "subjectCategory" => array(1, 3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "exchangeFrequency" => 1,    //更新频率
                "infoClassify" => 1,    //信息分类
                "infoCategory" => 1,    //信息类别
                "description" => "目录描述信息",    //目录描述
                "category" => 21,    //目录类别
                "items" => array(
                    array(
                        "name" => '企业名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '企业名称'    //备注
                    )
                )
            ),
            "relationships" => array(
                "sourceUnit" => array( // 来源单位
                    "data" => array(
                        array("type" => "userGroups", "id" => 1)
                    )
                )
            )
        );

        return $data;
    }
}
