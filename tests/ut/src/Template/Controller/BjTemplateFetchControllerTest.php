<?php
namespace Base\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use Base\Template\Model\NullBjTemplate;
use Base\Template\Model\BjTemplate;
use Base\Template\View\BjTemplateView;

class BjTemplateFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(BjTemplateFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockBjTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\Template\Repository\BjTemplateSdkRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockBjTemplateFetchController();

        $this->assertInstanceOf(
            'Base\Template\View\BjTemplateView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockBjTemplateFetchController();

        $this->assertEquals(
            'bjTemplates',
            $controller->getResourceName()
        );
    }
}
