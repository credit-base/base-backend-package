<?php
namespace Base\Template\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;

class GbTemplateTest extends TestCase
{
    private $gbTemplate;

    public function setUp()
    {
        $this->gbTemplate = new GbTemplate();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->gbTemplate);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\Template',
            $this->gbTemplate
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IOperate',
            $this->gbTemplate
        );
    }

    public function testGetRepository()
    {
        $gbTemplate = new MockGbTemplate();
        $this->assertInstanceOf('Base\Template\Adapter\GbTemplate\IGbTemplateAdapter', $gbTemplate->getRepository());
        $this->assertInstanceOf('Base\Template\Repository\GbTemplateSdkRepository', $gbTemplate->getRepository());
    }

    public function testGetCategory()
    {
        $result = $this->gbTemplate->getCategory();
        $this->assertEquals($result, Template::CATEGORY['GB']);
    }
    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IGbTemplateAdapter 调用 add 并且传参 $gbTemplate 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $gbTemplate = $this->getMockBuilder(GbTemplate::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IGbTemplateAdapter
        $repository = $this->prophesize(IGbTemplateAdapter::class);
        $repository->add(Argument::exact($gbTemplate))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $gbTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $gbTemplate->add();
        $this->assertTrue($result);
    }

    //add()
    /**
     * 测试添加失败
     * 1. 期望返回false
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IGbTemplateAdapter 调用 add 并且传参 $gbTemplate 对象, 返回 false
     */
    public function testAddFail()
    {
        //初始化
        $gbTemplate = $this->getMockBuilder(GbTemplate::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IGbTemplateAdapter
        $repository = $this->prophesize(IGbTemplateAdapter::class);
        $repository->add(Argument::exact($gbTemplate))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        //绑定
        $gbTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $gbTemplate->add();
        $this->assertFalse($result);
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 updateTime
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IGbTemplateAdapter 调用 edit 并且传参 $gbTemplate 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $gbTemplate = $this->getMockBuilder(GbTemplate::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IGbTemplateAdapter
        $repository = $this->prophesize(IGbTemplateAdapter::class);
        $repository->edit(
            Argument::exact($gbTemplate),
            Argument::exact(
                [
                    'name',
                    'identify',
                    'subjectCategory',
                    'dimension',
                    'exchangeFrequency',
                    'infoClassify',
                    'infoCategory',
                    'description',
                    'items'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $gbTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $gbTemplate->edit();
        $this->assertTrue($result);
    }

    //edit
    /**
     * 期望编辑失败
     * 1. 期望更新 updateTime
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IGbTemplateAdapter 调用 edit 并且传参 $gbTemplate 对象, 和相应字段, 返回false
     */
    public function testEditFail()
    {
        //初始化
        $gbTemplate = $this->getMockBuilder(GbTemplate::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IGbTemplateAdapter
        $repository = $this->prophesize(IGbTemplateAdapter::class);
        $repository->edit(
            Argument::exact($gbTemplate),
            Argument::exact(
                [
                    'name',
                    'identify',
                    'subjectCategory',
                    'dimension',
                    'exchangeFrequency',
                    'infoClassify',
                    'infoCategory',
                    'description',
                    'items'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(false);

        //绑定
        $gbTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $gbTemplate->edit();
        $this->assertFalse($result);
    }
}
