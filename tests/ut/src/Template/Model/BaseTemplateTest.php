<?php
namespace Base\Template\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class BaseTemplateTest extends TestCase
{
    private $baseTemplate;

    public function setUp()
    {
        $this->baseTemplate = new BaseTemplate();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->baseTemplate);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\Template',
            $this->baseTemplate
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IOperate',
            $this->baseTemplate
        );
    }

    public function testGetRepository()
    {
        $baseTemplate = new MockBaseTemplate();
        $this->assertInstanceOf(
            'Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter',
            $baseTemplate->getRepository()
        );
        $this->assertInstanceOf(
            'Base\Template\Repository\BaseTemplateSdkRepository',
            $baseTemplate->getRepository()
        );
    }

    public function testGetCategory()
    {
        $result = $this->baseTemplate->getCategory();
        $this->assertEquals($result, Template::CATEGORY['BASE']);
    }

    public function testAddSuccess()
    {
        //验证
        $result = $this->baseTemplate->add();
        $this->assertFalse($result);
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 updateTime
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IBaseTemplateAdapter 调用 edit 并且传参 $baseTemplate 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $baseTemplate = $this->getMockBuilder(BaseTemplate::class)
                           ->setMethods(['setUpdateTime', 'getRepository'])
                           ->getMock();

        //预言修改updateTime
        $baseTemplate->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));

        //预言 IBaseTemplateAdapter
        $repository = $this->prophesize(IBaseTemplateAdapter::class);
        $repository->edit(
            Argument::exact($baseTemplate),
            Argument::exact(
                [
                    'subjectCategory',
                    'dimension',
                    'infoClassify',
                    'infoCategory',
                    'items',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $baseTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $baseTemplate->edit();
        $this->assertTrue($result);
    }
}
