<?php
namespace Base\Template\Model;

use PHPUnit\Framework\TestCase;

class NullGbTemplateTest extends TestCase
{
    private $gbTemplate;

    public function setUp()
    {
        $this->gbTemplate = NullGbTemplate::getInstance();
    }

    public function tearDown()
    {
        unset($this->gbTemplate);
    }

    public function testExtendsGbTemplate()
    {
        $this->assertInstanceof('Base\Template\Model\GbTemplate', $this->gbTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->gbTemplate);
    }
}
