<?php
namespace Base\Template\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class TranslatorFactoryTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new TranslatorFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->translator);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullSdkTranslator()
    {
        $translator = $this->translator->getTranslator(0);
            $this->assertInstanceOf(
                'Base\Common\Translator\NullSdkTranslator',
                $translator
            );
    }

    public function testGetTranslator()
    {
        foreach (TranslatorFactory::MAPS as $key => $translator) {
            $this->assertInstanceOf(
                $translator,
                $this->translator->getTranslator($key)
            );
        }
    }
}
