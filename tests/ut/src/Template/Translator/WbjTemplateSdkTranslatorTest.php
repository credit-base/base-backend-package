<?php
namespace Base\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Translator\ISdkTranslator;

use Base\Template\Model\NullWbjTemplate;
use Base\Template\Utils\WbjTemplateSdkUtils;

use BaseSdk\Template\Model\WbjTemplate as WbjTemplateSdk;
use BaseSdk\Template\Model\NullWbjTemplate as NullWbjTemplateSdk;

use Base\UserGroup\Translator\UserGroupSdkTranslator;
use BaseSdk\UserGroup\Model\UserGroup as UserGroupSdk;

class WbjTemplateSdkTranslatorTest extends TestCase
{
    use WbjTemplateSdkUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(WbjTemplateSdkTranslator::class)
                    ->setMethods([
                        'getUserGroupSdkTranslator'
                    ])
                    ->getMock();

        $this->childTranslator = new class extends WbjTemplateSdkTranslator
        {
            public function getUserGroupSdkTranslator() : ISdkTranslator
            {
                return parent::getUserGroupSdkTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupSdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\UserGroup\Translator\UserGroupSdkTranslator',
            $this->childTranslator->getUserGroupSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->childTranslator->getUserGroupSdkTranslator()
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $wbjTemplate = \Base\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);

        $userGroupSdk = new UserGroupSdk($wbjTemplate->getSourceUnit()->getId());

        $wbjTemplateSdk = new WbjTemplateSdk(
            $wbjTemplate->getId(),
            $wbjTemplate->getName(),
            $wbjTemplate->getIdentify(),
            $wbjTemplate->getCategory(),
            $wbjTemplate->getSubjectCategory(),
            $wbjTemplate->getDimension(),
            $wbjTemplate->getExchangeFrequency(),
            $wbjTemplate->getInfoClassify(),
            $wbjTemplate->getInfoCategory(),
            $wbjTemplate->getDescription(),
            $wbjTemplate->getItems(),
            $userGroupSdk,
            $wbjTemplate->getStatus(),
            $wbjTemplate->getStatusTime(),
            $wbjTemplate->getCreateTime(),
            $wbjTemplate->getUpdateTime()
        );

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->valueObjectToObject(Argument::exact($userGroupSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjTemplate->getSourceUnit());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $wbjTemplateObject = $this->translator->valueObjectToObject($wbjTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\WbjTemplate', $wbjTemplateObject);
        $this->compareValueObjectAndObject($wbjTemplateSdk, $wbjTemplateObject);
    }

    public function testValueObjectToObjectFail()
    {
        $wbjTemplateSdk = NullWbjTemplateSdk::getInstance();

        $wbjTemplate = $this->translator->valueObjectToObject($wbjTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\NullWbjTemplate', $wbjTemplate);
    }

    public function testObjectToValueObjectFail()
    {
        $wbjTemplate = array();

        $wbjTemplateSdk = $this->translator->objectToValueObject($wbjTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\WbjTemplate', $wbjTemplateSdk);
    }

    public function testObjectToValueObject()
    {
        $wbjTemplate = \Base\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $userGroupSdk = new UserGroupSdk($wbjTemplate->getSourceUnit()->getId());

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->objectToValueObject(Argument::exact($wbjTemplate->getSourceUnit()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroupSdk);
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $wbjTemplateSdk = $this->translator->objectToValueObject($wbjTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\WbjTemplate', $wbjTemplateSdk);
        $this->compareValueObjectAndObject($wbjTemplateSdk, $wbjTemplate);
    }
}
