<?php
namespace Base\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Translator\ISdkTranslator;

use Base\Template\Model\NullBjTemplate;
use Base\Template\Utils\BjTemplateSdkUtils;

use BaseSdk\Template\Model\BjTemplate as BjTemplateSdk;
use BaseSdk\Template\Model\NullBjTemplate as NullBjTemplateSdk;
use BaseSdk\Template\Model\GbTemplate as GbTemplateSdk;

use Base\UserGroup\Translator\UserGroupSdkTranslator;
use BaseSdk\UserGroup\Model\UserGroup as UserGroupSdk;

class BjTemplateSdkTranslatorTest extends TestCase
{
    use BjTemplateSdkUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(BjTemplateSdkTranslator::class)
                    ->setMethods([
                        'getUserGroupSdkTranslator',
                        'getGbTemplateSdkTranslator',
                    ])
                    ->getMock();

        $this->childTranslator = new class extends BjTemplateSdkTranslator
        {
            public function getUserGroupSdkTranslator() : ISdkTranslator
            {
                return parent::getUserGroupSdkTranslator();
            }

            public function getGbTemplateSdkTranslator() : ISdkTranslator
            {
                return parent::getGbTemplateSdkTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupSdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\UserGroup\Translator\UserGroupSdkTranslator',
            $this->childTranslator->getUserGroupSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->childTranslator->getUserGroupSdkTranslator()
        );
    }

    public function testGetGbTemplateSdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Template\Translator\GbTemplateSdkTranslator',
            $this->childTranslator->getGbTemplateSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->childTranslator->getGbTemplateSdkTranslator()
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $bjTemplate = \Base\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);

        $userGroupSdk = new UserGroupSdk($bjTemplate->getSourceUnit()->getId());
        $gbTemplateSdk = new GbTemplateSdk($bjTemplate->getGbTemplate()->getId());

        $bjTemplateSdk = new BjTemplateSdk(
            $bjTemplate->getId(),
            $bjTemplate->getName(),
            $bjTemplate->getIdentify(),
            $bjTemplate->getCategory(),
            $bjTemplate->getSubjectCategory(),
            $bjTemplate->getDimension(),
            $bjTemplate->getExchangeFrequency(),
            $bjTemplate->getInfoClassify(),
            $bjTemplate->getInfoCategory(),
            $bjTemplate->getDescription(),
            $bjTemplate->getItems(),
            $userGroupSdk,
            $gbTemplateSdk,
            $bjTemplate->getStatus(),
            $bjTemplate->getStatusTime(),
            $bjTemplate->getCreateTime(),
            $bjTemplate->getUpdateTime()
        );

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->valueObjectToObject(Argument::exact($userGroupSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjTemplate->getSourceUnit());

        $gbTemplateSdkTranslator = $this->prophesize(GbTemplateSdkTranslator::class);
        $gbTemplateSdkTranslator->valueObjectToObject(Argument::exact($gbTemplateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjTemplate->getGbTemplate());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getGbTemplateSdkTranslator')
            ->willReturn($gbTemplateSdkTranslator->reveal());

        $bjTemplateObject = $this->translator->valueObjectToObject($bjTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\BjTemplate', $bjTemplateObject);
        $this->compareValueObjectAndObject($bjTemplateSdk, $bjTemplateObject);
    }

    public function testValueObjectToObjectFail()
    {
        $bjTemplateSdk = array();

        $bjTemplate = $this->translator->valueObjectToObject($bjTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\NullBjTemplate', $bjTemplate);
    }

    public function testObjectToValueObjectFail()
    {
        $bjTemplate = array();

        $bjTemplateSdk = $this->translator->objectToValueObject($bjTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\BjTemplate', $bjTemplateSdk);
    }

    public function testObjectToValueObject()
    {
        $bjTemplate = \Base\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
        $userGroupSdk = new UserGroupSdk($bjTemplate->getSourceUnit()->getId());
        $gbTemplateSdk = new GbTemplateSdk($bjTemplate->getGbTemplate()->getId());

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->objectToValueObject(Argument::exact($bjTemplate->getSourceUnit()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroupSdk);

        $gbTemplateSdkTranslator = $this->prophesize(GbTemplateSdkTranslator::class);
        $gbTemplateSdkTranslator->objectToValueObject(Argument::exact($bjTemplate->getGbTemplate()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbTemplateSdk);
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getGbTemplateSdkTranslator')
            ->willReturn($gbTemplateSdkTranslator->reveal());

        $bjTemplateSdk = $this->translator->objectToValueObject($bjTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\BjTemplate', $bjTemplateSdk);
        $this->compareValueObjectAndObject($bjTemplateSdk, $bjTemplate);
    }
}
