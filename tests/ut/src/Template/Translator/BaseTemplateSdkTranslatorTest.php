<?php
namespace Base\Template\Translator;

use PHPUnit\Framework\TestCase;

use Base\Template\Model\NullBaseTemplate;
use Base\Template\Utils\BaseTemplateSdkUtils;

use BaseSdk\Template\Model\BaseTemplate as BaseTemplateSdk;
use BaseSdk\Template\Model\NullBaseTemplate as NullBaseTemplateSdk;

class BaseTemplateSdkTranslatorTest extends TestCase
{
    use BaseTemplateSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BaseTemplateSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $baseTemplate = \Base\Template\Utils\BaseTemplateMockFactory::generateBaseTemplate(1);

        $baseTemplateSdk = new BaseTemplateSdk(
            $baseTemplate->getId(),
            $baseTemplate->getName(),
            $baseTemplate->getIdentify(),
            $baseTemplate->getCategory(),
            $baseTemplate->getSubjectCategory(),
            $baseTemplate->getDimension(),
            $baseTemplate->getExchangeFrequency(),
            $baseTemplate->getInfoClassify(),
            $baseTemplate->getInfoCategory(),
            $baseTemplate->getDescription(),
            $baseTemplate->getItems(),
            $baseTemplate->getStatus(),
            $baseTemplate->getStatusTime(),
            $baseTemplate->getCreateTime(),
            $baseTemplate->getUpdateTime()
        );

        $baseTemplateObject = $this->translator->valueObjectToObject($baseTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\BaseTemplate', $baseTemplateObject);
        $this->compareValueObjectAndObject($baseTemplateSdk, $baseTemplateObject);
    }

    public function testValueObjectToObjectFail()
    {
        $baseTemplateSdk = NullBaseTemplateSdk::getInstance();

        $baseTemplate = $this->translator->valueObjectToObject($baseTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\NullBaseTemplate', $baseTemplate);
    }

    public function testObjectToValueObjectFail()
    {
        $baseTemplate = array();

        $baseTemplateSdk = $this->translator->objectToValueObject($baseTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\BaseTemplate', $baseTemplateSdk);
    }

    public function testObjectToValueObject()
    {
        $baseTemplate = \Base\Template\Utils\BaseTemplateMockFactory::generateBaseTemplate(1);

        $baseTemplateSdk = $this->translator->objectToValueObject($baseTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\BaseTemplate', $baseTemplateSdk);
        $this->compareValueObjectAndObject($baseTemplateSdk, $baseTemplate);
    }
}
