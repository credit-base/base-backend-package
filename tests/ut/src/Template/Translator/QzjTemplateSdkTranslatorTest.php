<?php
namespace Base\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Translator\ISdkTranslator;

use Base\Template\Model\NullQzjTemplate;
use Base\Template\Utils\QzjTemplateSdkUtils;

use BaseSdk\Template\Model\QzjTemplate as QzjTemplateSdk;
use BaseSdk\Template\Model\NullQzjTemplate as NullQzjTemplateSdk;

use Base\UserGroup\Translator\UserGroupSdkTranslator;
use BaseSdk\UserGroup\Model\UserGroup as UserGroupSdk;

class QzjTemplateSdkTranslatorTest extends TestCase
{
    use QzjTemplateSdkUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(QzjTemplateSdkTranslator::class)
                    ->setMethods([
                        'getUserGroupSdkTranslator'
                    ])
                    ->getMock();

        $this->childTranslator = new class extends QzjTemplateSdkTranslator
        {
            public function getUserGroupSdkTranslator() : ISdkTranslator
            {
                return parent::getUserGroupSdkTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupSdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\UserGroup\Translator\UserGroupSdkTranslator',
            $this->childTranslator->getUserGroupSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->childTranslator->getUserGroupSdkTranslator()
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $qzjTemplate = \Base\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate(1);

        $userGroupSdk = new UserGroupSdk($qzjTemplate->getSourceUnit()->getId());

        $qzjTemplateSdk = new QzjTemplateSdk(
            $qzjTemplate->getId(),
            $qzjTemplate->getName(),
            $qzjTemplate->getIdentify(),
            $qzjTemplate->getCategory(),
            $qzjTemplate->getSubjectCategory(),
            $qzjTemplate->getDimension(),
            $qzjTemplate->getExchangeFrequency(),
            $qzjTemplate->getInfoClassify(),
            $qzjTemplate->getInfoCategory(),
            $qzjTemplate->getDescription(),
            $qzjTemplate->getItems(),
            $userGroupSdk,
            $qzjTemplate->getStatus(),
            $qzjTemplate->getStatusTime(),
            $qzjTemplate->getCreateTime(),
            $qzjTemplate->getUpdateTime()
        );

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->valueObjectToObject(Argument::exact($userGroupSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($qzjTemplate->getSourceUnit());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $qzjTemplateObject = $this->translator->valueObjectToObject($qzjTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\QzjTemplate', $qzjTemplateObject);
        $this->compareValueObjectAndObject($qzjTemplateSdk, $qzjTemplateObject);
    }

    public function testValueObjectToObjectFail()
    {
        $qzjTemplateSdk = NullQzjTemplateSdk::getInstance();

        $qzjTemplate = $this->translator->valueObjectToObject($qzjTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\NullQzjTemplate', $qzjTemplate);
    }

    public function testObjectToValueObjectFail()
    {
        $qzjTemplate = array();

        $qzjTemplateSdk = $this->translator->objectToValueObject($qzjTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\QzjTemplate', $qzjTemplateSdk);
    }

    public function testObjectToValueObject()
    {
        $qzjTemplate = \Base\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate(1);
        $userGroupSdk = new UserGroupSdk($qzjTemplate->getSourceUnit()->getId());

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->objectToValueObject(Argument::exact($qzjTemplate->getSourceUnit()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroupSdk);
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $qzjTemplateSdk = $this->translator->objectToValueObject($qzjTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\QzjTemplate', $qzjTemplateSdk);
        $this->compareValueObjectAndObject($qzjTemplateSdk, $qzjTemplate);
    }
}
