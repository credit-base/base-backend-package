<?php
namespace Base\Template\Translator;

use PHPUnit\Framework\TestCase;

use Base\Template\Model\NullGbTemplate;
use Base\Template\Utils\GbTemplateSdkUtils;

use BaseSdk\Template\Model\GbTemplate as GbTemplateSdk;
use BaseSdk\Template\Model\NullGbTemplate as NullGbTemplateSdk;

class GbTemplateSdkTranslatorTest extends TestCase
{
    use GbTemplateSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbTemplateSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $gbTemplate = \Base\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);

        $gbTemplateSdk = new GbTemplateSdk(
            $gbTemplate->getId(),
            $gbTemplate->getName(),
            $gbTemplate->getIdentify(),
            $gbTemplate->getCategory(),
            $gbTemplate->getSubjectCategory(),
            $gbTemplate->getDimension(),
            $gbTemplate->getExchangeFrequency(),
            $gbTemplate->getInfoClassify(),
            $gbTemplate->getInfoCategory(),
            $gbTemplate->getDescription(),
            $gbTemplate->getItems(),
            $gbTemplate->getStatus(),
            $gbTemplate->getStatusTime(),
            $gbTemplate->getCreateTime(),
            $gbTemplate->getUpdateTime()
        );

        $gbTemplateObject = $this->translator->valueObjectToObject($gbTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\GbTemplate', $gbTemplateObject);
        $this->compareValueObjectAndObject($gbTemplateSdk, $gbTemplateObject);
    }

    public function testValueObjectToObjectFail()
    {
        $gbTemplateSdk = NullGbTemplateSdk::getInstance();

        $gbTemplate = $this->translator->valueObjectToObject($gbTemplateSdk);
        $this->assertInstanceof('Base\Template\Model\NullGbTemplate', $gbTemplate);
    }

    public function testObjectToValueObjectFail()
    {
        $gbTemplate = array();

        $gbTemplateSdk = $this->translator->objectToValueObject($gbTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\GbTemplate', $gbTemplateSdk);
    }

    public function testObjectToValueObject()
    {
        $gbTemplate = \Base\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);

        $gbTemplateSdk = $this->translator->objectToValueObject($gbTemplate);
        $this->assertInstanceof('BaseSdk\Template\Model\GbTemplate', $gbTemplateSdk);
        $this->compareValueObjectAndObject($gbTemplateSdk, $gbTemplate);
    }
}
