<?php
namespace Base\Template\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class BaseTemplateSchemaTest extends TestCase
{
    private $baseTemplateSchema;

    private $baseTemplate;

    public function setUp()
    {
        $this->baseTemplateSchema = new BaseTemplateSchema(new Factory());

        $this->baseTemplate = \Base\Template\Utils\BaseTemplateMockFactory::generateBaseTemplate(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->baseTemplateSchema);
        unset($this->baseTemplate);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->baseTemplateSchema);
    }

    public function testGetId()
    {
        $result = $this->baseTemplateSchema->getId($this->baseTemplate);

        $this->assertEquals($result, $this->baseTemplate->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->baseTemplateSchema->getAttributes($this->baseTemplate);

        $this->assertEquals($result['name'], $this->baseTemplate->getName());
        $this->assertEquals($result['identify'], $this->baseTemplate->getIdentify());
        $this->assertEquals($result['subjectCategory'], $this->baseTemplate->getSubjectCategory());
        $this->assertEquals($result['dimension'], $this->baseTemplate->getDimension());
        $this->assertEquals($result['exchangeFrequency'], $this->baseTemplate->getExchangeFrequency());
        $this->assertEquals($result['infoClassify'], $this->baseTemplate->getInfoClassify());
        $this->assertEquals($result['infoCategory'], $this->baseTemplate->getInfoCategory());
        $this->assertEquals($result['description'], $this->baseTemplate->getDescription());
        $this->assertEquals($result['category'], $this->baseTemplate->getCategory());
        $this->assertEquals($result['items'], $this->baseTemplate->getItems());
        $this->assertEquals($result['status'], $this->baseTemplate->getStatus());
        $this->assertEquals($result['createTime'], $this->baseTemplate->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->baseTemplate->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->baseTemplate->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->baseTemplateSchema->getRelationships($this->baseTemplate, 0, array());

        $this->assertEquals($result, array());
    }
}
