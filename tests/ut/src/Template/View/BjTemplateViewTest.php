<?php
namespace Base\Template\View;

use PHPUnit\Framework\TestCase;

use Base\Template\Model\BjTemplate;

class BjTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $bjTemplate = new BjTemplateView(new BjTemplate());
        $this->assertInstanceof('Base\Common\View\CommonView', $bjTemplate);
    }
}
