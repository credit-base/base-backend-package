<?php
namespace Base\Template\View;

use PHPUnit\Framework\TestCase;

use Base\Template\Model\BaseTemplate;

class BaseTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $baseTemplate = new BaseTemplateView(new BaseTemplate());
        $this->assertInstanceof('Base\Common\View\CommonView', $baseTemplate);
    }
}
