<?php
namespace Base\Template\View;

use PHPUnit\Framework\TestCase;

use Base\Template\Model\QzjTemplate;

class QzjTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $qzjTemplate = new QzjTemplateView(new QzjTemplate());
        $this->assertInstanceof('Base\Common\View\CommonView', $qzjTemplate);
    }
}
