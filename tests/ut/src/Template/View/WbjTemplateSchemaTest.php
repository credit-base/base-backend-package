<?php
namespace Base\Template\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class WbjTemplateSchemaTest extends TestCase
{
    private $wbjTemplateSchema;

    private $wbjTemplate;

    public function setUp()
    {
        $this->wbjTemplateSchema = new WbjTemplateSchema(new Factory());

        $this->wbjTemplate = \Base\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->wbjTemplateSchema);
        unset($this->wbjTemplate);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->wbjTemplateSchema);
    }

    public function testGetId()
    {
        $result = $this->wbjTemplateSchema->getId($this->wbjTemplate);

        $this->assertEquals($result, $this->wbjTemplate->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->wbjTemplateSchema->getAttributes($this->wbjTemplate);

        $this->assertEquals($result['name'], $this->wbjTemplate->getName());
        $this->assertEquals($result['identify'], $this->wbjTemplate->getIdentify());
        $this->assertEquals($result['subjectCategory'], $this->wbjTemplate->getSubjectCategory());
        $this->assertEquals($result['dimension'], $this->wbjTemplate->getDimension());
        $this->assertEquals($result['exchangeFrequency'], $this->wbjTemplate->getExchangeFrequency());
        $this->assertEquals($result['infoClassify'], $this->wbjTemplate->getInfoClassify());
        $this->assertEquals($result['infoCategory'], $this->wbjTemplate->getInfoCategory());
        $this->assertEquals($result['description'], $this->wbjTemplate->getDescription());
        $this->assertEquals($result['category'], $this->wbjTemplate->getCategory());
        $this->assertEquals($result['items'], $this->wbjTemplate->getItems());
        $this->assertEquals($result['status'], $this->wbjTemplate->getStatus());
        $this->assertEquals($result['createTime'], $this->wbjTemplate->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->wbjTemplate->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->wbjTemplate->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->wbjTemplateSchema->getRelationships($this->wbjTemplate, 0, array());

        $this->assertEquals($result['sourceUnit'], ['data' => $this->wbjTemplate->getSourceUnit()]);
    }
}
