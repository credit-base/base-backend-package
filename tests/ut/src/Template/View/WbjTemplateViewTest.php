<?php
namespace Base\Template\View;

use PHPUnit\Framework\TestCase;

use Base\Template\Model\WbjTemplate;

class WbjTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $wbjTemplate = new WbjTemplateView(new WbjTemplate());
        $this->assertInstanceof('Base\Common\View\CommonView', $wbjTemplate);
    }
}
