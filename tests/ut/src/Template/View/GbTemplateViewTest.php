<?php
namespace Base\Template\View;

use PHPUnit\Framework\TestCase;

use Base\Template\Model\GbTemplate;

class GbTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $gbTemplate = new GbTemplateView(new GbTemplate());
        $this->assertInstanceof('Base\Common\View\CommonView', $gbTemplate);
    }
}
