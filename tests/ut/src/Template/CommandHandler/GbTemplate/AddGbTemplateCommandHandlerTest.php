<?php
namespace Base\Template\CommandHandler\GbTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Template\Model\Template;
use Base\Template\Model\GbTemplate;
use Base\Template\Command\GbTemplate\AddGbTemplateCommand;

class AddGbTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddGbTemplateCommandHandler::class)
                                     ->setMethods(['getGbTemplate'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetGbTemplate()
    {
        $commandHandler = new MockAddGbTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Template\Model\GbTemplate',
            $commandHandler->getGbTemplate()
        );
    }

    public function testExecuteSuccess()
    {
        $command = $this->initCommand();
        $gbTemplate = $this->initGbTemplate($command, true);

        $expectId = 10;
        $gbTemplate->getId()->shouldBeCalledTimes(1)->willReturn($expectId);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
        $this->assertEquals($expectId, $command->id);
    }

    public function testExecuteFail()
    {
        $command = $this->initCommand();
        $gbTemplate = $this->initGbTemplate($command, false);

        $gbTemplate->getId()->shouldBeCalledTimes(0);//调用0次

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }

    private function initCommand() : ICommand
    {
        $command = new AddGbTemplateCommand(
            $this->faker->word,
            $this->faker->regexify('[A-Z_]{1,100}'),
            $this->faker->randomElements(Template::SUBJECT_CATEGORY, 2),
            $this->faker->randomElement(Template::DIMENSION),
            $this->faker->randomElement(Template::EXCHANGE_FREQUENCY),
            $this->faker->randomElement(Template::INFO_CLASSIFY),
            $this->faker->randomElement(Template::INFO_CATEGORY),
            $this->faker->sentence,
            $this->faker->randomElements(
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => Template::TYPE['ZFX'],    //数据类型
                        "length" => '50',    //数据长度
                    ),
                ),
                1
            )
        );

        return $command;
    }

    private function initGbTemplate(ICommand $command, bool $result)
    {
        $gbTemplate = $this->prophesize(GbTemplate::class);
        $gbTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $gbTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $gbTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $gbTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $gbTemplate->setItems(Argument::exact($command->items))->shouldBeCalledTimes(1);
        $gbTemplate->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->any())
            ->method('getGbTemplate')
            ->willReturn($gbTemplate->reveal());

        return $gbTemplate;
    }
}
