<?php
namespace Base\Template\CommandHandler\GbTemplate;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Base\Template\Command\GbTemplate\AddGbTemplateCommand;
use Base\Template\Command\GbTemplate\EditGbTemplateCommand;

class GbTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new GbTemplateCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddGbTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddGbTemplateCommand(
                $this->faker->word,
                $this->faker->regexify('[A-Z_]{1,100}'),
                $this->faker->randomElements(array(1,2,3), 2),
                $this->faker->randomElement(array(1,2,3)),
                $this->faker->randomDigit,
                $this->faker->randomDigit,
                $this->faker->randomDigit,
                $this->faker->sentence,
                $this->faker->randomElements(array('1','2','3'), 2)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Template\CommandHandler\GbTemplate\AddGbTemplateCommandHandler',
            $commandHandler
        );
    }

    public function testEditGbTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditGbTemplateCommand(
                $this->faker->word,
                $this->faker->regexify('[A-Z_]{1,100}'),
                $this->faker->randomElements(array(1,2,3), 2),
                $this->faker->randomElement(array(1,2,3)),
                $this->faker->randomDigit,
                $this->faker->randomDigit,
                $this->faker->randomDigit,
                $this->faker->sentence,
                $this->faker->randomElements(array('1','2','3'), 2),
                $this->faker->randomDigit
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Template\CommandHandler\GbTemplate\EditGbTemplateCommandHandler',
            $commandHandler
        );
    }
}
