<?php
namespace Base\Template\CommandHandler\QzjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Template\Model\QzjTemplate;
use Base\Template\Repository\QzjTemplateSdkRepository;

class QzjTemplateCommandHandlerTraitTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new MockQzjTemplateCommandHandlerTrait();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testGetUserGroupRepository()
    {
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $this->commandHandler->publicGetUserGroupRepository()
        );
    }

    public function testFetchUserGroup()
    {
        $id = 1;
        $userGroup = new UserGroup();

        $this->commandHandler = $this->getMockBuilder(MockQzjTemplateCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getUserGroupRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($userGroup);

        $this->commandHandler->expects($this->once())
                       ->method('getUserGroupRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->publicFetchUserGroup($id);
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $result
        );
    }

    public function testGetQzjTemplate()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\QzjTemplate',
            $this->commandHandler->publicGetQzjTemplate()
        );
    }

    public function testGetQzjTemplateSdkRepository()
    {
        $this->assertInstanceOf(
            'Base\Template\Repository\QzjTemplateSdkRepository',
            $this->commandHandler->publicGetQzjTemplateSdkRepository()
        );
    }

    public function testFetchQzjTemplate()
    {
        $id = 1;
        $qzjTemplate = new QzjTemplate();

        $this->commandHandler = $this->getMockBuilder(MockQzjTemplateCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getQzjTemplateSdkRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(QzjTemplateSdkRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($qzjTemplate);

        $this->commandHandler->expects($this->once())
                       ->method('getQzjTemplateSdkRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->publicFetchQzjTemplate($id);
        $this->assertInstanceOf(
            'Base\Template\Model\QzjTemplate',
            $result
        );
    }
}
