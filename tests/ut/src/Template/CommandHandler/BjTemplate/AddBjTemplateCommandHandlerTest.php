<?php
namespace Base\Template\CommandHandler\BjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Template\Model\Template;
use Base\Template\Model\GbTemplate;
use Base\Template\Model\BjTemplate;
use Base\Template\Command\BjTemplate\AddBjTemplateCommand;

use Base\UserGroup\Model\UserGroup;

class AddBjTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddBjTemplateCommandHandler::class)
                                     ->setMethods(['getBjTemplate'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetBjTemplate()
    {
        $commandHandler = new MockAddBjTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Template\Model\BjTemplate',
            $commandHandler->getBjTemplate()
        );
    }

    public function testExecuteSuccess()
    {
        $command = $this->initCommand();
        $bjTemplate = $this->initBjTemplate($command, true);

        $expectId = 10;
        $bjTemplate->getId()->shouldBeCalledTimes(1)->willReturn($expectId);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
        $this->assertEquals($expectId, $command->id);
    }

    public function testExecuteFail()
    {
        $command = $this->initCommand();
        $bjTemplate = $this->initBjTemplate($command, false);

        $bjTemplate->getId()->shouldBeCalledTimes(0);//调用0次

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }

    private function initCommand() : ICommand
    {
        $command = new AddBjTemplateCommand(
            $this->faker->word,
            $this->faker->regexify('[A-Z_]{1,100}'),
            $this->faker->randomElements(Template::SUBJECT_CATEGORY, 2),
            $this->faker->randomElement(Template::DIMENSION),
            $this->faker->randomElement(Template::EXCHANGE_FREQUENCY),
            $this->faker->randomElement(Template::INFO_CLASSIFY),
            $this->faker->randomElement(Template::INFO_CATEGORY),
            $this->faker->sentence,
            $this->faker->randomElements(
                array(
                    array(
                        "name" => '罚款金额',    //信息项名称
                        "identify" => 'FKJE',    //数据标识
                        "type" => Template::TYPE['FDX'],    //数据类型
                        "length" => '24',    //数据长度
                    )
                ),
                1
            ),
            $this->faker->randomDigitNotNull,
            $this->faker->randomDigitNotNull
        );

        return $command;
    }

    private function initBjTemplate(ICommand $command, bool $result)
    {
        $bjTemplate = $this->prophesize(BjTemplate::class);
        $gbTemplate = $this->prophesize(GbTemplate::class);
        $gbTemplate->setId(Argument::exact($command->gbTemplate))->shouldBeCalledTimes(1);
        $bjTemplate->setSourceUnit(new UserGroup($command->sourceUnit))->shouldBeCalledTimes(1);
        $bjTemplate->getGbTemplate()->shouldBeCalledTimes(1)->willReturn($gbTemplate->reveal());
        $bjTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $bjTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $bjTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $bjTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $bjTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $bjTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $bjTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $bjTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $bjTemplate->setItems(Argument::exact($command->items))->shouldBeCalledTimes(1);

        $bjTemplate->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->any())
            ->method('getBjTemplate')
            ->willReturn($bjTemplate->reveal());

        return $bjTemplate;
    }
}
