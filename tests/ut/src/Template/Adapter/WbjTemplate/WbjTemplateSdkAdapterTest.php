<?php
namespace Base\Template\Adapter\WbjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Template\Model\WbjTemplate;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjTemplateSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockWbjTemplateSdkAdapter::class)
                           ->setMethods([
                               'fetchSourceUnit',
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction',
                               'addAction',
                               'editAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIWbjTemplateAdapter()
    {
        $adapter = new WbjTemplateSdkAdapter();

        $this->assertInstanceOf(
            'Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Repository\WbjTemplateRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\NullWbjTemplate',
            $this->adapter->getNullObject()
        );
    }

    /**
     * 测试是否初始化 UserGroupRepository
     */
    public function testGetUserGroupRepository()
    {
        $adapter = new MockWbjTemplateSdkAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }
    
    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockWbjTemplateSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedWbjTemplate = new WbjTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedWbjTemplate);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnit')
                         ->with($expectedWbjTemplate);

        //验证
        $wbjTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedWbjTemplate, $wbjTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $wbjTemplateOne = new WbjTemplate(1);
        $wbjTemplateTwo = new WbjTemplate(2);

        $ids = [1, 2];

        $expectedWbjTemplateList = [];
        $expectedWbjTemplateList[$wbjTemplateOne->getId()] = $wbjTemplateOne;
        $expectedWbjTemplateList[$wbjTemplateTwo->getId()] = $wbjTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedWbjTemplateList);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnit')
                         ->with($expectedWbjTemplateList);

        //验证
        $wbjTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedWbjTemplateList, $wbjTemplateList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $wbjTemplateOne = new WbjTemplate(1);
        $wbjTemplateTwo = new WbjTemplate(2);

        $expectedWbjTemplateList = [];
        $expectedWbjTemplateList[$wbjTemplateOne->getId()] = $wbjTemplateOne;
        $expectedWbjTemplateList[$wbjTemplateTwo->getId()] = $wbjTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedWbjTemplateList, 2]);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnit')
                         ->with($expectedWbjTemplateList);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedWbjTemplateList, 2], $result);
    }

    //add
    public function testAdd()
    {
        //初始化
        $wbjTemplate = new WbjTemplate();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($wbjTemplate)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($wbjTemplate);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $wbjTemplate = new WbjTemplate();
        $keys = array();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($wbjTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($wbjTemplate, $keys);
        $this->assertTrue($result);
    }

    //fetchSourceUnit
    public function testFetchSourceUnitIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockWbjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchSourceUnitByObject'
                                ]
                           )
                           ->getMock();
        
        $wbjTemplate = new WbjTemplate();

        $adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnitByObject')
                         ->with($wbjTemplate);

        $adapter->fetchSourceUnit($wbjTemplate);
    }

    public function testFetchSourceUnitIsArray()
    {
        $adapter = $this->getMockBuilder(MockWbjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchSourceUnitByList'
                                ]
                           )
                           ->getMock();
        
        $wbjTemplateOne = new WbjTemplate(1);
        $wbjTemplateTwo = new WbjTemplate(2);
        
        $wbjTemplateList[$wbjTemplateOne->getId()] = $wbjTemplateOne;
        $wbjTemplateList[$wbjTemplateTwo->getId()] = $wbjTemplateTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnitByList')
                         ->with($wbjTemplateList);

        $adapter->fetchSourceUnit($wbjTemplateList);
    }

    //fetchSourceUnitByObject
    public function testFetchSourceUnitByObject()
    {
        $adapter = $this->getMockBuilder(MockWbjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $wbjTemplate = new WbjTemplate();
        $wbjTemplate->setSourceUnit(new UserGroup(1));
        
        $sourceUnitId = 1;

        // 为 UserGroup 类建立预言
        $sourceUnit  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($sourceUnitId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceUnit);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchSourceUnitByObject($wbjTemplate);
    }

    //fetchSourceUnitByList
    public function testFetchSourceUnitByList()
    {
        $adapter = $this->getMockBuilder(MockWbjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $wbjTemplateOne = new WbjTemplate(1);
        $sourceUnitOne = new UserGroup(1);
        $wbjTemplateOne->setSourceUnit($sourceUnitOne);

        $wbjTemplateTwo = new WbjTemplate(2);
        $sourceUnitTwo = new UserGroup(2);
        $wbjTemplateTwo->setSourceUnit($sourceUnitTwo);
        $sourceUnitIds = [1, 2];
        
        $sourceUnitList[$sourceUnitOne->getId()] = $sourceUnitOne;
        $sourceUnitList[$sourceUnitTwo->getId()] = $sourceUnitTwo;
        
        $wbjTemplateList[$wbjTemplateOne->getId()] = $wbjTemplateOne;
        $wbjTemplateList[$wbjTemplateTwo->getId()] = $wbjTemplateTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(Argument::exact($sourceUnitIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceUnitList);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchSourceUnitByList($wbjTemplateList);
    }
}
