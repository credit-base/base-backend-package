<?php
namespace Base\Template\Adapter\GbTemplate;

use PHPUnit\Framework\TestCase;

use Base\Template\Model\GbTemplate;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbTemplateSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockGbTemplateSdkAdapter::class)
                           ->setMethods([
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction',
                               'editAction',
                               'addAction'])->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIGbTemplateAdapter()
    {
        $adapter = new GbTemplateSdkAdapter();

        $this->assertInstanceOf(
            'Base\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Repository\GbTemplateRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\NullGbTemplate',
            $this->adapter->getNullObject()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockGbTemplateSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedGbTemplate = new GbTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedGbTemplate);

        //验证
        $gbTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedGbTemplate, $gbTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $gbTemplateOne = new GbTemplate(1);
        $gbTemplateTwo = new GbTemplate(2);

        $ids = [1, 2];

        $expectedGbTemplateList = [];
        $expectedGbTemplateList[$gbTemplateOne->getId()] = $gbTemplateOne;
        $expectedGbTemplateList[$gbTemplateTwo->getId()] = $gbTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedGbTemplateList);

        //验证
        $gbTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedGbTemplateList, $gbTemplateList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $gbTemplateOne = new GbTemplate(1);
        $gbTemplateTwo = new GbTemplate(2);

        $expectedGbTemplateList = [];
        $expectedGbTemplateList[$gbTemplateOne->getId()] = $gbTemplateOne;
        $expectedGbTemplateList[$gbTemplateTwo->getId()] = $gbTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedGbTemplateList, 2]);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedGbTemplateList, 2], $result);
    }

    //add
    public function testAdd()
    {
        //初始化
        $gbTemplate = new GbTemplate();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($gbTemplate)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($gbTemplate);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $gbTemplate = new GbTemplate();
        $keys = array();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($gbTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($gbTemplate, $keys);
        $this->assertTrue($result);
    }
}
