<?php
namespace Base\Template\Adapter\BaseTemplate;

use PHPUnit\Framework\TestCase;

use Base\Template\Model\BaseTemplate;

class BaseTemplateSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockBaseTemplateSdkAdapter::class)
                           ->setMethods(['fetchOneAction', 'fetchListAction', 'filterAction', 'editAction'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIBaseTemplateAdapter()
    {
        $adapter = new BaseTemplateSdkAdapter();

        $this->assertInstanceOf(
            'Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Repository\BaseTemplateRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\NullBaseTemplate',
            $this->adapter->getNullObject()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockBaseTemplateSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedBaseTemplate = new BaseTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedBaseTemplate);

        //验证
        $baseTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedBaseTemplate, $baseTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $baseTemplateOne = new BaseTemplate(1);
        $baseTemplateTwo = new BaseTemplate(2);

        $ids = [1, 2];

        $expectedBaseTemplateList = [];
        $expectedBaseTemplateList[$baseTemplateOne->getId()] = $baseTemplateOne;
        $expectedBaseTemplateList[$baseTemplateTwo->getId()] = $baseTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedBaseTemplateList);

        //验证
        $baseTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedBaseTemplateList, $baseTemplateList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $baseTemplateOne = new BaseTemplate(1);
        $baseTemplateTwo = new BaseTemplate(2);

        $expectedBaseTemplateList = [];
        $expectedBaseTemplateList[$baseTemplateOne->getId()] = $baseTemplateOne;
        $expectedBaseTemplateList[$baseTemplateTwo->getId()] = $baseTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedBaseTemplateList, 2]);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedBaseTemplateList, 2], $result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $baseTemplate = new BaseTemplate();
        $keys = array();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($baseTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($baseTemplate, $keys);
        $this->assertTrue($result);
    }
}
