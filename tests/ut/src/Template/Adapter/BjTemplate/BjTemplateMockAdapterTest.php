<?php
namespace Base\Template\Adapter\BjTemplate;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Template\Model\BjTemplate;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class BjTemplateMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new BjTemplateMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new BjTemplate()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new BjTemplate(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\BjTemplate',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Template\Model\BjTemplate',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Template\Model\BjTemplate',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
