<?php
namespace Base\Template\Adapter\BjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Template\Model\BjTemplate;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjTemplateSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockBjTemplateSdkAdapter::class)
                           ->setMethods([
                               'fetchSourceUnit',
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction',
                               'addAction',
                               'editAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIBjTemplateAdapter()
    {
        $adapter = new BjTemplateSdkAdapter();

        $this->assertInstanceOf(
            'Base\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Repository\BjTemplateRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\NullBjTemplate',
            $this->adapter->getNullObject()
        );
    }

    /**
     * 测试是否初始化 UserGroupRepository
     */
    public function testGetUserGroupRepository()
    {
        $adapter = new MockBjTemplateSdkAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }
    
    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockBjTemplateSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedBjTemplate = new BjTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedBjTemplate);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnit')
                         ->with($expectedBjTemplate);

        //验证
        $bjTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedBjTemplate, $bjTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $bjTemplateOne = new BjTemplate(1);
        $bjTemplateTwo = new BjTemplate(2);

        $ids = [1, 2];

        $expectedBjTemplateList = [];
        $expectedBjTemplateList[$bjTemplateOne->getId()] = $bjTemplateOne;
        $expectedBjTemplateList[$bjTemplateTwo->getId()] = $bjTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedBjTemplateList);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnit')
                         ->with($expectedBjTemplateList);

        //验证
        $bjTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedBjTemplateList, $bjTemplateList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $bjTemplateOne = new BjTemplate(1);
        $bjTemplateTwo = new BjTemplate(2);

        $expectedBjTemplateList = [];
        $expectedBjTemplateList[$bjTemplateOne->getId()] = $bjTemplateOne;
        $expectedBjTemplateList[$bjTemplateTwo->getId()] = $bjTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedBjTemplateList, 2]);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnit')
                         ->with($expectedBjTemplateList);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedBjTemplateList, 2], $result);
    }

    //add
    public function testAdd()
    {
        //初始化
        $bjTemplate = new BjTemplate();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($bjTemplate)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($bjTemplate);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $bjTemplate = new BjTemplate();
        $keys = array();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($bjTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($bjTemplate, $keys);
        $this->assertTrue($result);
    }

    //fetchSourceUnit
    public function testFetchSourceUnitIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockBjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchSourceUnitByObject'
                                ]
                           )
                           ->getMock();
        
        $bjTemplate = new BjTemplate();

        $adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnitByObject')
                         ->with($bjTemplate);

        $adapter->fetchSourceUnit($bjTemplate);
    }

    public function testFetchSourceUnitIsArray()
    {
        $adapter = $this->getMockBuilder(MockBjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchSourceUnitByList'
                                ]
                           )
                           ->getMock();
        
        $bjTemplateOne = new BjTemplate(1);
        $bjTemplateTwo = new BjTemplate(2);
        
        $bjTemplateList[$bjTemplateOne->getId()] = $bjTemplateOne;
        $bjTemplateList[$bjTemplateTwo->getId()] = $bjTemplateTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnitByList')
                         ->with($bjTemplateList);

        $adapter->fetchSourceUnit($bjTemplateList);
    }

    //fetchSourceUnitByObject
    public function testFetchSourceUnitByObject()
    {
        $adapter = $this->getMockBuilder(MockBjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $bjTemplate = new BjTemplate();
        $bjTemplate->setSourceUnit(new UserGroup(1));
        
        $sourceUnitId = 1;

        // 为 UserGroup 类建立预言
        $sourceUnit  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($sourceUnitId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceUnit);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchSourceUnitByObject($bjTemplate);
    }

    //fetchSourceUnitByList
    public function testFetchSourceUnitByList()
    {
        $adapter = $this->getMockBuilder(MockBjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $bjTemplateOne = new BjTemplate(1);
        $sourceUnitOne = new UserGroup(1);
        $bjTemplateOne->setSourceUnit($sourceUnitOne);

        $bjTemplateTwo = new BjTemplate(2);
        $sourceUnitTwo = new UserGroup(2);
        $bjTemplateTwo->setSourceUnit($sourceUnitTwo);
        $sourceUnitIds = [1, 2];
        
        $sourceUnitList[$sourceUnitOne->getId()] = $sourceUnitOne;
        $sourceUnitList[$sourceUnitTwo->getId()] = $sourceUnitTwo;
        
        $bjTemplateList[$bjTemplateOne->getId()] = $bjTemplateOne;
        $bjTemplateList[$bjTemplateTwo->getId()] = $bjTemplateTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(Argument::exact($sourceUnitIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceUnitList);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchSourceUnitByList($bjTemplateList);
    }
}
