<?php
namespace Base\Template\Adapter\QzjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Template\Model\QzjTemplate;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class QzjTemplateSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockQzjTemplateSdkAdapter::class)
                           ->setMethods([
                               'fetchSourceUnit',
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction',
                               'addAction',
                               'editAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIQzjTemplateAdapter()
    {
        $adapter = new QzjTemplateSdkAdapter();

        $this->assertInstanceOf(
            'Base\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\Template\Repository\QzjTemplateRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\NullQzjTemplate',
            $this->adapter->getNullObject()
        );
    }

    /**
     * 测试是否初始化 UserGroupRepository
     */
    public function testGetUserGroupRepository()
    {
        $adapter = new MockQzjTemplateSdkAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }
    
    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockQzjTemplateSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedQzjTemplate = new QzjTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedQzjTemplate);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnit')
                         ->with($expectedQzjTemplate);

        //验证
        $qzjTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedQzjTemplate, $qzjTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $qzjTemplateOne = new QzjTemplate(1);
        $qzjTemplateTwo = new QzjTemplate(2);

        $ids = [1, 2];

        $expectedQzjTemplateList = [];
        $expectedQzjTemplateList[$qzjTemplateOne->getId()] = $qzjTemplateOne;
        $expectedQzjTemplateList[$qzjTemplateTwo->getId()] = $qzjTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedQzjTemplateList);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnit')
                         ->with($expectedQzjTemplateList);

        //验证
        $qzjTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedQzjTemplateList, $qzjTemplateList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $qzjTemplateOne = new QzjTemplate(1);
        $qzjTemplateTwo = new QzjTemplate(2);

        $expectedQzjTemplateList = [];
        $expectedQzjTemplateList[$qzjTemplateOne->getId()] = $qzjTemplateOne;
        $expectedQzjTemplateList[$qzjTemplateTwo->getId()] = $qzjTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedQzjTemplateList, 2]);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnit')
                         ->with($expectedQzjTemplateList);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedQzjTemplateList, 2], $result);
    }

    //add
    public function testAdd()
    {
        //初始化
        $qzjTemplate = new QzjTemplate();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($qzjTemplate)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($qzjTemplate);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $qzjTemplate = new QzjTemplate();
        $keys = array();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($qzjTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($qzjTemplate, $keys);
        $this->assertTrue($result);
    }

    //fetchSourceUnit
    public function testFetchSourceUnitIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockQzjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchSourceUnitByObject'
                                ]
                           )
                           ->getMock();
        
        $qzjTemplate = new QzjTemplate();

        $adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnitByObject')
                         ->with($qzjTemplate);

        $adapter->fetchSourceUnit($qzjTemplate);
    }

    public function testFetchSourceUnitIsArray()
    {
        $adapter = $this->getMockBuilder(MockQzjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchSourceUnitByList'
                                ]
                           )
                           ->getMock();
        
        $qzjTemplateOne = new QzjTemplate(1);
        $qzjTemplateTwo = new QzjTemplate(2);
        
        $qzjTemplateList[$qzjTemplateOne->getId()] = $qzjTemplateOne;
        $qzjTemplateList[$qzjTemplateTwo->getId()] = $qzjTemplateTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchSourceUnitByList')
                         ->with($qzjTemplateList);

        $adapter->fetchSourceUnit($qzjTemplateList);
    }

    //fetchSourceUnitByObject
    public function testFetchSourceUnitByObject()
    {
        $adapter = $this->getMockBuilder(MockQzjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $qzjTemplate = new QzjTemplate();
        $qzjTemplate->setSourceUnit(new UserGroup(1));
        
        $sourceUnitId = 1;

        // 为 UserGroup 类建立预言
        $sourceUnit  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($sourceUnitId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceUnit);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchSourceUnitByObject($qzjTemplate);
    }

    //fetchSourceUnitByList
    public function testFetchSourceUnitByList()
    {
        $adapter = $this->getMockBuilder(MockQzjTemplateSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $qzjTemplateOne = new QzjTemplate(1);
        $sourceUnitOne = new UserGroup(1);
        $qzjTemplateOne->setSourceUnit($sourceUnitOne);

        $qzjTemplateTwo = new QzjTemplate(2);
        $sourceUnitTwo = new UserGroup(2);
        $qzjTemplateTwo->setSourceUnit($sourceUnitTwo);
        $sourceUnitIds = [1, 2];
        
        $sourceUnitList[$sourceUnitOne->getId()] = $sourceUnitOne;
        $sourceUnitList[$sourceUnitTwo->getId()] = $sourceUnitTwo;
        
        $qzjTemplateList[$qzjTemplateOne->getId()] = $qzjTemplateOne;
        $qzjTemplateList[$qzjTemplateTwo->getId()] = $qzjTemplateTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(Argument::exact($sourceUnitIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceUnitList);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchSourceUnitByList($qzjTemplateList);
    }
}
