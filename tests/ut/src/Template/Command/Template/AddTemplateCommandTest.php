<?php
namespace Base\Template\Command\Template;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class AddTemplateCommandTest extends TestCase
{
    use TemplateCommandTrait;
    
    private $fakerData;
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'name' => $faker->word,
            'identify' => $faker->regexify('[A-Z_]{1,100}'),
            'subjectCategory' => $faker->randomElements(array(1,2,3), 2),
            'dimension' => $faker->randomElement(array(1,2,3)),
            'exchangeFrequency' => $faker->randomDigit,
            'infoClassify' => $faker->randomDigit,
            'infoCategory' => $faker->randomDigit,
            'description' => $faker->sentence,
            'items' => array('items')
        );

        $this->command = new AddTemplateCommand(
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['subjectCategory'],
            $this->fakerData['dimension'],
            $this->fakerData['exchangeFrequency'],
            $this->fakerData['infoClassify'],
            $this->fakerData['infoCategory'],
            $this->fakerData['description'],
            $this->fakerData['items']
        );
    }

    public function testDefaultParameter()
    {
        $command = new AddTemplateCommand(
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['subjectCategory'],
            $this->fakerData['dimension'],
            $this->fakerData['exchangeFrequency'],
            $this->fakerData['infoClassify'],
            $this->fakerData['infoCategory'],
            $this->fakerData['description'],
            $this->fakerData['items']
        );
        $this->assertEquals(0, $command->id);
    }
}
