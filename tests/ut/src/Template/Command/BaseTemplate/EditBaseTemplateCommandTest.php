<?php
namespace Base\Template\Command\BaseTemplate;

use PHPUnit\Framework\TestCase;

class EditBaseTemplateCommandTest extends TestCase
{
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->command = new EditBaseTemplateCommand(
            $faker->randomElements(array('1','2','3'), 2),
            $faker->randomElements(array('1','2','3'), 2),
            $faker->randomDigit(),
            $faker->randomDigit(),
            $faker->randomDigit(),
            $faker->randomDigit(),
            $faker->randomDigit()
        );
    }
    
    public function testImplementsICommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }
}
