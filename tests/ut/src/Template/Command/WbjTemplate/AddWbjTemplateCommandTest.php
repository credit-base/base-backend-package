<?php
namespace Base\Template\Command\WbjTemplate;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class AddWbjTemplateCommandTest extends TestCase
{
    private $fakerData;

    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'sourceUnit' => $faker->randomDigit
        );

        $this->command = new AddWbjTemplateCommand(
            $faker->word,
            $faker->regexify('[A-Z_]{1,100}'),
            $faker->randomElements(array(1,2,3), 2),
            $faker->randomElement(array(1,2,3)),
            $faker->randomDigit,
            $faker->randomDigit,
            $faker->randomDigit,
            $faker->sentence,
            $faker->randomElements(array('1','2','3'), 2),
            $this->fakerData['sourceUnit']
        );
    }
    
    public function testExtendsCommand()
    {
        $this->assertInstanceOf('Base\Template\Command\Template\AddTemplateCommand', $this->command);
    }

    public function testSourceUnitParameter()
    {
        $this->assertEquals($this->fakerData['sourceUnit'], $this->command->sourceUnit);
    }
}
