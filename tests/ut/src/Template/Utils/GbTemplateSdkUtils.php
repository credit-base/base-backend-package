<?php
namespace Base\Template\Utils;

trait GbTemplateSdkUtils
{
    private function compareValueObjectAndObject(
        $gbTemplateSdk,
        $gbTemplate
    ) {
        $this->assertEquals($gbTemplateSdk->getId(), $gbTemplate->getId());
        $this->assertEquals($gbTemplateSdk->getName(), $gbTemplate->getName());
        $this->assertEquals($gbTemplateSdk->getIdentify(), $gbTemplate->getIdentify());
        $this->assertEquals($gbTemplateSdk->getSubjectCategory(), $gbTemplate->getSubjectCategory());
        $this->assertEquals($gbTemplateSdk->getDimension(), $gbTemplate->getDimension());
        $this->assertEquals($gbTemplateSdk->getExchangeFrequency(), $gbTemplate->getExchangeFrequency());
        $this->assertEquals($gbTemplateSdk->getInfoClassify(), $gbTemplate->getInfoClassify());
        $this->assertEquals($gbTemplateSdk->getInfoCategory(), $gbTemplate->getInfoCategory());
        $this->assertEquals($gbTemplateSdk->getDescription(), $gbTemplate->getDescription());
        $this->assertEquals($gbTemplateSdk->getItems(), $gbTemplate->getItems());
        $this->assertEquals($gbTemplateSdk->getStatus(), $gbTemplate->getStatus());
        $this->assertEquals($gbTemplateSdk->getCreateTime(), $gbTemplate->getCreateTime());
        $this->assertEquals($gbTemplateSdk->getStatusTime(), $gbTemplate->getStatusTime());
        $this->assertEquals($gbTemplateSdk->getUpdateTime(), $gbTemplate->getUpdateTime());
    }
}
