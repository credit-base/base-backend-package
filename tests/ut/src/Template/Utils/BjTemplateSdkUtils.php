<?php
namespace Base\Template\Utils;

trait BjTemplateSdkUtils
{
    private function compareValueObjectAndObject(
        $bjTemplateSdk,
        $bjTemplate
    ) {
        $this->assertEquals($bjTemplateSdk->getId(), $bjTemplate->getId());
        $this->assertEquals($bjTemplateSdk->getName(), $bjTemplate->getName());
        $this->assertEquals($bjTemplateSdk->getIdentify(), $bjTemplate->getIdentify());
        $this->assertEquals($bjTemplateSdk->getSubjectCategory(), $bjTemplate->getSubjectCategory());
        $this->assertEquals($bjTemplateSdk->getDimension(), $bjTemplate->getDimension());
        $this->assertEquals($bjTemplateSdk->getExchangeFrequency(), $bjTemplate->getExchangeFrequency());
        $this->assertEquals($bjTemplateSdk->getInfoClassify(), $bjTemplate->getInfoClassify());
        $this->assertEquals($bjTemplateSdk->getInfoCategory(), $bjTemplate->getInfoCategory());
        $this->assertEquals($bjTemplateSdk->getDescription(), $bjTemplate->getDescription());
        $this->assertEquals($bjTemplateSdk->getItems(), $bjTemplate->getItems());
        $this->assertEquals($bjTemplateSdk->getSourceUnit()->getId(), $bjTemplate->getSourceUnit()->getId());
        $this->assertEquals($bjTemplateSdk->getGbTemplate()->getId(), $bjTemplate->getGbTemplate()->getId());
        $this->assertEquals($bjTemplateSdk->getStatus(), $bjTemplate->getStatus());
        $this->assertEquals($bjTemplateSdk->getCreateTime(), $bjTemplate->getCreateTime());
        $this->assertEquals($bjTemplateSdk->getStatusTime(), $bjTemplate->getStatusTime());
        $this->assertEquals($bjTemplateSdk->getUpdateTime(), $bjTemplate->getUpdateTime());
    }
}
