<?php
namespace Base\Template\Utils;

use Base\Template\Model\BaseTemplate;

class BaseTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateBaseTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BaseTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $baseTemplate = new BaseTemplate($id);
        $baseTemplate->setId($id);

        self::generateName($baseTemplate, $faker, $value);
        self::generateIdentify($baseTemplate, $faker, $value);
        self::generateSubjectCategory($baseTemplate, $faker, $value);
        self::generateDimension($baseTemplate, $faker, $value);
        self::generateExchangeFrequency($baseTemplate, $faker, $value);
        self::generateInfoClassify($baseTemplate, $faker, $value);
        self::generateInfoCategory($baseTemplate, $faker, $value);
        self::generateDescription($baseTemplate, $faker, $value);
        self::generateItems($baseTemplate, $faker, $value);
        self::generateStatus($baseTemplate, $faker, $value);
        
        $baseTemplate->setCreateTime($faker->unixTime());
        $baseTemplate->setUpdateTime($faker->unixTime());
        $baseTemplate->setStatusTime($faker->unixTime());

        return $baseTemplate;
    }
}
