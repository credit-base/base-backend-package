<?php
namespace Base\Template\Utils;

use Base\Template\Model\BjTemplate;
use Base\Template\Model\GbTemplate;
use Base\UserGroup\Model\UserGroup;

class BjTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateBjTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BjTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $bjTemplate = new BjTemplate($id);
        $bjTemplate->setId($id);

        self::generateName($bjTemplate, $faker, $value);
        self::generateIdentify($bjTemplate, $faker, $value);
        self::generateSubjectCategory($bjTemplate, $faker, $value);
        self::generateDimension($bjTemplate, $faker, $value);
        self::generateExchangeFrequency($bjTemplate, $faker, $value);
        self::generateInfoClassify($bjTemplate, $faker, $value);
        self::generateInfoCategory($bjTemplate, $faker, $value);
        self::generateDescription($bjTemplate, $faker, $value);
        self::generateItems($bjTemplate, $faker, $value);
        self::generateStatus($bjTemplate, $faker, $value);
        self::generateSourceUnit($bjTemplate, $faker, $value);
        self::generateGbTemplate($bjTemplate, $faker, $value);
        
        $bjTemplate->setCreateTime($faker->unixTime());
        $bjTemplate->setUpdateTime($faker->unixTime());
        $bjTemplate->setStatusTime($faker->unixTime());

        return $bjTemplate;
    }

    private static function generateSourceUnit($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        
        $sourceUnit = isset($value['sourceUnit']) ?
            new UserGroup($value['sourceUnit']) :
            new UserGroup();
            
        $bjTemplate->setSourceUnit($sourceUnit);
    }

    private static function generateGbTemplate($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        
        $gbTemplate = isset($value['gbTemplate']) ?
            new GbTemplate($value['gbTemplate']) :
            new GbTemplate();
            
        $bjTemplate->setGbTemplate($gbTemplate);
    }
}
