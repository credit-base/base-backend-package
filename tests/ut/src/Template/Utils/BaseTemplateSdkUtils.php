<?php
namespace Base\Template\Utils;

trait BaseTemplateSdkUtils
{
    private function compareValueObjectAndObject(
        $baseTemplateSdk,
        $baseTemplate
    ) {
        $this->assertEquals($baseTemplateSdk->getId(), $baseTemplate->getId());
        $this->assertEquals($baseTemplateSdk->getName(), $baseTemplate->getName());
        $this->assertEquals($baseTemplateSdk->getIdentify(), $baseTemplate->getIdentify());
        $this->assertEquals($baseTemplateSdk->getSubjectCategory(), $baseTemplate->getSubjectCategory());
        $this->assertEquals($baseTemplateSdk->getDimension(), $baseTemplate->getDimension());
        $this->assertEquals($baseTemplateSdk->getExchangeFrequency(), $baseTemplate->getExchangeFrequency());
        $this->assertEquals($baseTemplateSdk->getInfoClassify(), $baseTemplate->getInfoClassify());
        $this->assertEquals($baseTemplateSdk->getInfoCategory(), $baseTemplate->getInfoCategory());
        $this->assertEquals($baseTemplateSdk->getDescription(), $baseTemplate->getDescription());
        $this->assertEquals($baseTemplateSdk->getItems(), $baseTemplate->getItems());
        $this->assertEquals($baseTemplateSdk->getStatus(), $baseTemplate->getStatus());
        $this->assertEquals($baseTemplateSdk->getCreateTime(), $baseTemplate->getCreateTime());
        $this->assertEquals($baseTemplateSdk->getStatusTime(), $baseTemplate->getStatusTime());
        $this->assertEquals($baseTemplateSdk->getUpdateTime(), $baseTemplate->getUpdateTime());
    }
}
