<?php
namespace Base\Template\Utils;

trait WbjTemplateSdkUtils
{
    private function compareValueObjectAndObject(
        $wbjTemplateSdk,
        $wbjTemplate
    ) {
        $this->assertEquals($wbjTemplateSdk->getId(), $wbjTemplate->getId());
        $this->assertEquals($wbjTemplateSdk->getName(), $wbjTemplate->getName());
        $this->assertEquals($wbjTemplateSdk->getIdentify(), $wbjTemplate->getIdentify());
        $this->assertEquals($wbjTemplateSdk->getSubjectCategory(), $wbjTemplate->getSubjectCategory());
        $this->assertEquals($wbjTemplateSdk->getDimension(), $wbjTemplate->getDimension());
        $this->assertEquals($wbjTemplateSdk->getExchangeFrequency(), $wbjTemplate->getExchangeFrequency());
        $this->assertEquals($wbjTemplateSdk->getInfoClassify(), $wbjTemplate->getInfoClassify());
        $this->assertEquals($wbjTemplateSdk->getInfoCategory(), $wbjTemplate->getInfoCategory());
        $this->assertEquals($wbjTemplateSdk->getDescription(), $wbjTemplate->getDescription());
        $this->assertEquals($wbjTemplateSdk->getItems(), $wbjTemplate->getItems());
        $this->assertEquals($wbjTemplateSdk->getSourceUnit()->getId(), $wbjTemplate->getSourceUnit()->getId());
        $this->assertEquals($wbjTemplateSdk->getStatus(), $wbjTemplate->getStatus());
        $this->assertEquals($wbjTemplateSdk->getCreateTime(), $wbjTemplate->getCreateTime());
        $this->assertEquals($wbjTemplateSdk->getStatusTime(), $wbjTemplate->getStatusTime());
        $this->assertEquals($wbjTemplateSdk->getUpdateTime(), $wbjTemplate->getUpdateTime());
    }
}
