<?php
namespace Base\Template\Utils;

trait QzjTemplateSdkUtils
{
    private function compareValueObjectAndObject(
        $qzjTemplateSdk,
        $qzjTemplate
    ) {
        $this->assertEquals($qzjTemplateSdk->getId(), $qzjTemplate->getId());
        $this->assertEquals($qzjTemplateSdk->getName(), $qzjTemplate->getName());
        $this->assertEquals($qzjTemplateSdk->getIdentify(), $qzjTemplate->getIdentify());
        $this->assertEquals($qzjTemplateSdk->getSubjectCategory(), $qzjTemplate->getSubjectCategory());
        $this->assertEquals($qzjTemplateSdk->getDimension(), $qzjTemplate->getDimension());
        $this->assertEquals($qzjTemplateSdk->getExchangeFrequency(), $qzjTemplate->getExchangeFrequency());
        $this->assertEquals($qzjTemplateSdk->getInfoClassify(), $qzjTemplate->getInfoClassify());
        $this->assertEquals($qzjTemplateSdk->getInfoCategory(), $qzjTemplate->getInfoCategory());
        $this->assertEquals($qzjTemplateSdk->getDescription(), $qzjTemplate->getDescription());
        $this->assertEquals($qzjTemplateSdk->getItems(), $qzjTemplate->getItems());
        $this->assertEquals($qzjTemplateSdk->getSourceUnit()->getId(), $qzjTemplate->getSourceUnit()->getId());
        $this->assertEquals($qzjTemplateSdk->getCategory(), $qzjTemplate->getCategory());
        $this->assertEquals($qzjTemplateSdk->getStatus(), $qzjTemplate->getStatus());
        $this->assertEquals($qzjTemplateSdk->getCreateTime(), $qzjTemplate->getCreateTime());
        $this->assertEquals($qzjTemplateSdk->getStatusTime(), $qzjTemplate->getStatusTime());
        $this->assertEquals($qzjTemplateSdk->getUpdateTime(), $qzjTemplate->getUpdateTime());
    }
}
