<?php
namespace Base\Template\Utils;

use Base\Template\Model\WbjTemplate;
use Base\UserGroup\Model\UserGroup;

class WbjTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateWbjTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WbjTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $wbjTemplate = new WbjTemplate($id);
        $wbjTemplate->setId($id);

        self::generateName($wbjTemplate, $faker, $value);
        self::generateIdentify($wbjTemplate, $faker, $value);
        self::generateSubjectCategory($wbjTemplate, $faker, $value);
        self::generateDimension($wbjTemplate, $faker, $value);
        self::generateExchangeFrequency($wbjTemplate, $faker, $value);
        self::generateInfoClassify($wbjTemplate, $faker, $value);
        self::generateInfoCategory($wbjTemplate, $faker, $value);
        self::generateDescription($wbjTemplate, $faker, $value);
        self::generateItems($wbjTemplate, $faker, $value);
        self::generateStatus($wbjTemplate, $faker, $value);
        self::generateSourceUnit($wbjTemplate, $faker, $value);
        
        $wbjTemplate->setCreateTime($faker->unixTime());
        $wbjTemplate->setUpdateTime($faker->unixTime());
        $wbjTemplate->setStatusTime($faker->unixTime());

        return $wbjTemplate;
    }

    private static function generateSourceUnit($wbjTemplate, $faker, $value) : void
    {
        unset($faker);
        
        $sourceUnit = isset($value['sourceUnit']) ?
            new UserGroup($value['sourceUnit']) :
            new UserGroup();
            
        $wbjTemplate->setSourceUnit($sourceUnit);
    }
}
