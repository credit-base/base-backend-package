<?php
namespace Base\Member\WidgetRule;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Common\Utils\StringGenerate;

use Base\Member\Model\Member;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class MemberWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new MemberWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //userName -- start
    /**
     * @dataProvider invalidUserNameProvider
     */
    public function testUserNameInvalid($actual, $expected)
    {
        $result = $this->widgetRule->userName($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidUserNameProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(MemberWidgetRule::USER_NAME_MIN_LENGTH-1), false),
            array(StringGenerate::generate(MemberWidgetRule::USER_NAME_MAX_LENGTH+1), false),
            array(StringGenerate::generate(MemberWidgetRule::USER_NAME_MIN_LENGTH), true),
            array(StringGenerate::generate(MemberWidgetRule::USER_NAME_MIN_LENGTH+1), true),
            array(StringGenerate::generate(MemberWidgetRule::USER_NAME_MAX_LENGTH), true),
            array(StringGenerate::generate(MemberWidgetRule::USER_NAME_MAX_LENGTH-1), true)
        );
    }
    //userName -- end

    // email -- start
    /**
     * @dataProvider invalidEmailProvider
     */
    public function testEmailInvalid($actual, $expected)
    {
        $result = $this->widgetRule->email($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidEmailProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(StringGenerate::generate(2), false),
            array($faker->email(), true),
        );
    }
    // email -- end

    //contactAddress -- start
    /**
     * @dataProvider invalidContactAddressProvider
     */
    public function testContactAddressInvalid($actual, $expected)
    {
        $result = $this->widgetRule->contactAddress($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidContactAddressProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(MemberWidgetRule::CONTACT_ADDRESS_MIN_LENGTH-1), false),
            array(StringGenerate::generate(MemberWidgetRule::CONTACT_ADDRESS_MAX_LENGTH+1), false),
            array(StringGenerate::generate(MemberWidgetRule::CONTACT_ADDRESS_MIN_LENGTH), true),
            array(StringGenerate::generate(MemberWidgetRule::CONTACT_ADDRESS_MIN_LENGTH+1), true),
            array(StringGenerate::generate(MemberWidgetRule::CONTACT_ADDRESS_MAX_LENGTH), true),
            array(StringGenerate::generate(MemberWidgetRule::CONTACT_ADDRESS_MAX_LENGTH-1), true)
        );
    }
    //contactAddress -- end
    
    //securityAnswer -- start
    /**
     * @dataProvider invalidSecurityAnswerProvider
     */
    public function testSecurityAnswerInvalid($actual, $expected)
    {
        $result = $this->widgetRule->securityAnswer($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidSecurityAnswerProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(MemberWidgetRule::SECURITY_ANSWER_MIN_LENGTH-1), false),
            array(StringGenerate::generate(MemberWidgetRule::SECURITY_ANSWER_MAX_LENGTH+1), false),
            array(StringGenerate::generate(MemberWidgetRule::SECURITY_ANSWER_MIN_LENGTH), true),
            array(StringGenerate::generate(MemberWidgetRule::SECURITY_ANSWER_MIN_LENGTH+1), true),
            array(StringGenerate::generate(MemberWidgetRule::SECURITY_ANSWER_MAX_LENGTH), true),
            array(StringGenerate::generate(MemberWidgetRule::SECURITY_ANSWER_MAX_LENGTH-1), true)
        );
    }
    //securityAnswer -- end

    //securityQuestion -- start
    /**
     * @dataProvider invalidSecurityQuestionProvider
     */
    public function testSecurityQuestionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->securityQuestion($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidSecurityQuestionProvider()
    {
        return array(
            array('', false),
            array(Member::SECURITY_QUESTION['QUESTION_ONE'], true),
            array(Member::SECURITY_QUESTION['QUESTION_TWO'], true),
            array(999, false),
        );
    }
    //securityQuestion -- end
    
    //gender -- start
    /**
     * @dataProvider invalidGenderProvider
     */
    public function testGenderInvalid($actual, $expected)
    {
        $result = $this->widgetRule->gender($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidGenderProvider()
    {
        return array(
            array('', false),
            array(Member::GENDER['MALE'], true),
            array(Member::GENDER['FEMALE'], true),
            array(999, false),
        );
    }
    //gender -- end
}
