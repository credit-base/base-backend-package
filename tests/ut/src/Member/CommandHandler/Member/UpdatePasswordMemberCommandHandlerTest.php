<?php
namespace Base\Member\CommandHandler\Member;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\Member\Model\Member;
use Base\Member\Command\Member\UpdatePasswordMemberCommand;

class UpdatePasswordMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = new UpdatePasswordMemberCommandHandler();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->stub = $this->getMockBuilder(UpdatePasswordMemberCommandHandler::class)
                       ->setMethods(['fetchMember'])
                       ->getMock();

        $id = $this->faker->randomNumber();
        $oldPassword = $this->faker->randomNumber(3);
        $password = $this->faker->randomNumber(3);
    
        $command = new UpdatePasswordMemberCommand(
            $oldPassword,
            $password,
            $id
        );

        $member = $this->prophesize(Member::class);

        $member->changePassword(
            Argument::exact($command->oldPassword),
            Argument::exact($command->password)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->any())
            ->method('fetchMember')
            ->with($command->id)
            ->willReturn($member->reveal());

        $result = $this->stub->execute($command);
        
        $this->assertTrue($result);
    }
}
