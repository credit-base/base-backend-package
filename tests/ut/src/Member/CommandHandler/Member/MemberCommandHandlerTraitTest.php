<?php
namespace Base\Member\CommandHandler\Member;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;

class MemberCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockMemberCommandHandlerTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetMemberRepository()
    {
        $trait = new MockMemberCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Member\Repository\MemberRepository',
            $trait->publicGetMemberRepository()
        );
    }

    public function testFetchMember()
    {
        $trait = $this->getMockBuilder(MockMemberCommandHandlerTrait::class)
                 ->setMethods(['getMemberRepository']) ->getMock();

        $id = 1;

        $member = \Base\Member\Utils\MockFactory::generateMember($id);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);

        $trait->expects($this->exactly(1))
                         ->method('getMemberRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchMember($id);

        $this->assertEquals($result, $member);
    }
}
