<?php
namespace Base\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Base\Member\Command\Member\AddMemberCommand;
use Base\Member\Command\Member\EditMemberCommand;
use Base\Member\Command\Member\EnableMemberCommand;
use Base\Member\Command\Member\DisableMemberCommand;
use Base\Member\Command\Member\SignInMemberCommand;
use Base\Member\Command\Member\ResetPasswordMemberCommand;
use Base\Member\Command\Member\UpdatePasswordMemberCommand;

class MemberCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new MemberCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddMemberCommand(
                $this->faker->name(),
                $this->faker->name(),
                $this->faker->creditCardNumber(),
                $this->faker->phoneNumber(),
                $this->faker->email(),
                $this->faker->address(),
                $this->faker->word(),
                md5($this->faker->password()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Member\CommandHandler\Member\AddMemberCommandHandler',
            $commandHandler
        );
    }

    public function testEditMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditMemberCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Member\CommandHandler\Member\EditMemberCommandHandler',
            $commandHandler
        );
    }

    public function testEnableMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EnableMemberCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Member\CommandHandler\Member\EnableMemberCommandHandler',
            $commandHandler
        );
    }

    public function testDisableMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableMemberCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Member\CommandHandler\Member\DisableMemberCommandHandler',
            $commandHandler
        );
    }

    public function testResetPasswordMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResetPasswordMemberCommand(
                $this->faker->name(),
                $this->faker->word(),
                md5($this->faker->password()),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Member\CommandHandler\Member\ResetPasswordMemberCommandHandler',
            $commandHandler
        );
    }

    public function testSignInMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new SignInMemberCommand(
                $this->faker->name(),
                $this->faker->password(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Member\CommandHandler\Member\SignInMemberCommandHandler',
            $commandHandler
        );
    }

    public function testUpdatePasswordMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new UpdatePasswordMemberCommand(
                $this->faker->password(),
                $this->faker->password(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Member\CommandHandler\Member\UpdatePasswordMemberCommandHandler',
            $commandHandler
        );
    }
}
