<?php
namespace Base\Member\CommandHandler\Member;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;
use Base\Member\Command\Member\ResetPasswordMemberCommand;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ResetPasswordMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MockResetPasswordMemberCommandHandler::class)
                                     ->setMethods(['getMemberRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecuteFailMemberNotExist()
    {
        $commandHandler = $this->getMockBuilder(MockResetPasswordMemberCommandHandler::class)
                                     ->setMethods([
                                         'searchMembers',
                                         'isMemberExist'
                                        ])
                                     ->getMock();

        $member = \Base\Member\Utils\MockFactory::generateMember(1);

        $command = new ResetPasswordMemberCommand(
            $member->getUserName(),
            $member->getSecurityAnswer(),
            $member->getPassword()
        );

        $members = array($member);
        $count = 1;

        $commandHandler->expects($this->once())
                   ->method('searchMembers')
                   ->willReturn([$members, $count]);

        $commandHandler->expects($this->once())
                   ->method('isMemberExist')
                   ->with($count)
                   ->willReturn(false);
        //验证
        $result = $commandHandler->execute($command);
        
        $this->assertFalse($result);
    }

    public function testExecuteFailSecurityAnswerError()
    {
        $commandHandler = $this->getMockBuilder(MockResetPasswordMemberCommandHandler::class)
                                     ->setMethods([
                                         'searchMembers',
                                         'isMemberExist'
                                        ])
                                     ->getMock();

        $member = \Base\Member\Utils\MockFactory::generateMember(1);

        $command = new ResetPasswordMemberCommand(
            $member->getUserName(),
            'securityAnswer',
            $member->getPassword()
        );

        $count = 1;

        $commandHandler->expects($this->once())
                   ->method('isMemberExist')
                   ->with($count)
                   ->willReturn(true);

        $member = $this->prophesize(Member::class);
        $member->validateSecurity($command->securityAnswer)->shouldBeCalledTimes(1)->willReturn(false);

        $commandHandler->expects($this->once())
             ->method('searchMembers')
             ->willReturn([[$member->reveal()], $count]);
        //验证
        $result = $commandHandler->execute($command);
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_ERROR, Core::getLastError()->getId());
    }

    public function testExecuteSuccess()
    {
        $commandHandler = $this->getMockBuilder(MockResetPasswordMemberCommandHandler::class)
                                     ->setMethods([
                                         'searchMembers',
                                         'isMemberExist'
                                        ])
                                     ->getMock();

        $id = 1;
        $command = new ResetPasswordMemberCommand(
            'userName',
            'securityAnswer',
            'password',
            $id
        );

        $count = 1;

        $commandHandler->expects($this->once())
                   ->method('isMemberExist')
                   ->willReturn(true);

        $member = $this->prophesize(Member::class);
        $member->getId()->shouldBeCalledTimes(1)->willReturn($id);
        $member->validateSecurity($command->securityAnswer)->shouldBeCalledTimes(1)->willReturn(true);
        $member->resetPassword($command->password)->shouldBeCalledTimes(1)->willReturn(true);

        $commandHandler->expects($this->once())
             ->method('searchMembers')
             ->willReturn([[$member->reveal()], $count]);

        //验证
        $result = $commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testIsMemberExistFail()
    {
        $count = 3;

        $result = $this->commandHandler->isMemberExist($count);

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testIsMemberExistSuccess()
    {
        $count = ResetPasswordMemberCommandHandler::REST_PASSWORD_SEARCH_COUNT;

        $result = $this->commandHandler->isMemberExist($count);

        $this->assertTrue($result);
    }

    public function testSearchMembers()
    {
        $userName = $this->faker->name();
        $list = array(1);
        $count = 2;

        $filter['userName'] = $userName;

        $repository = $this->prophesize(MemberRepository::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->commandHandler->expects($this->any())
                   ->method('getMemberRepository')
                   ->willReturn($repository->reveal());

        $result = $this->commandHandler->searchMembers($userName);
        $this->assertEquals($result, [$list, $count]);
    }
}
