<?php
namespace Base\Member\CommandHandler\Member;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\Member\Model\Member;
use Base\Member\Command\Member\ValidateSecurityMemberCommand;

class ValidateSecurityMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = new ValidateSecurityMemberCommandHandler();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->stub = $this->getMockBuilder(ValidateSecurityMemberCommandHandler::class)
                       ->setMethods(['fetchMember'])
                       ->getMock();

        $id = $this->faker->randomNumber();
        $securityAnswer = $this->faker->word();
    
        $command = new ValidateSecurityMemberCommand(
            $securityAnswer,
            $id
        );

        $member = $this->prophesize(Member::class);
        $member->validateSecurity(Argument::exact($command->securityAnswer))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->any())
            ->method('fetchMember')
            ->with($command->id)
            ->willReturn($member->reveal());

        $result = $this->stub->execute($command);
        
        $this->assertTrue($result);
    }
}
