<?php
namespace Base\Member\CommandHandler\Member;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;
use Base\Member\Command\Member\SignInMemberCommand;

use Base\Common\Model\IEnableAble;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class SignInMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MockSignInMemberCommandHandler::class)
                                     ->setMethods(['getMemberRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecuteFail()
    {
        $commandHandler = $this->getMockBuilder(MockSignInMemberCommandHandler::class)
                                     ->setMethods([
                                         'searchMembers',
                                         'isMemberExist'
                                        ])
                                     ->getMock();

        $member = \Base\Member\Utils\MockFactory::generateMember(1);

        $command = new SignInMemberCommand(
            $member->getUserName(),
            $member->getPassword()
        );

        $members = array($member);
        $count = 1;

        $commandHandler->expects($this->once())
                   ->method('searchMembers')
                   ->with($command->userName)
                   ->willReturn([$members, $count]);

        $commandHandler->expects($this->once())
                   ->method('isMemberExist')
                   ->with($count)
                   ->willReturn(false);
        //验证
        $result = $commandHandler->execute($command);
        
        $this->assertFalse($result);
    }

    public function testExecuteSuccess()
    {
        $commandHandler = $this->getMockBuilder(MockSignInMemberCommandHandler::class)
                                     ->setMethods([
                                         'searchMembers',
                                         'isMemberExist',
                                         'isMemberDisabled',
                                         'memberPasswordCorrect'
                                        ])
                                     ->getMock();

        $id = 1;
        $member = \Base\Member\Utils\MockFactory::generateMember($id);

        $command = new SignInMemberCommand(
            $member->getUserName(),
            $member->getPassword()
        );

        $members = array($member);
        $count = 1;

        $commandHandler->expects($this->once())
                   ->method('searchMembers')
                   ->willReturn([$members, $count]);

        $commandHandler->expects($this->once())
                   ->method('isMemberExist')
                   ->willReturn(true);

        $commandHandler->expects($this->once())
                   ->method('isMemberDisabled')
                   ->willReturn(true);

        $commandHandler->expects($this->once())
                   ->method('memberPasswordCorrect')
                   ->willReturn(true);

        //验证
        $result = $commandHandler->execute($command);
        
        $this->assertTrue($result);
        $this->assertEquals($id, $command->id);
    }

    public function testSearchMembers()
    {
        $userName = $this->faker->phoneNumber();
        $list = array(1);
        $count = 1;

        $filter['userName'] = $userName;

        $repository = $this->prophesize(MemberRepository::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->commandHandler->expects($this->any())
                   ->method('getMemberRepository')
                   ->willReturn($repository->reveal());

        $result = $this->commandHandler->searchMembers($userName);
        $this->assertEquals($result, [$list, $count]);
    }

    public function testIsMemberExistFail()
    {
        $count = 2;

        $result = $this->commandHandler->isMemberExist($count);

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testIsMemberExistSuccess()
    {
        $count = SignInMemberCommandHandler::SIGN_IN_SEARCH_COUNT;

        $result = $this->commandHandler->isMemberExist($count);

        $this->assertTrue($result);
    }

    public function testIsMemberDisabledFail()
    {
        $member = \Base\Member\Utils\MockFactory::generateMember(1);
        $member->setStatus(IEnableAble::STATUS['DISABLED']);

        $result = $this->commandHandler->isMemberDisabled($member);

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testIsMemberDisabledSuccess()
    {
        $member = \Base\Member\Utils\MockFactory::generateMember(1);
        $member->setStatus(IEnableAble::STATUS['ENABLED']);

        $result = $this->commandHandler->isMemberDisabled($member);

        $this->assertTrue($result);
    }

    public function testMemberPasswordCorrectFail()
    {
        $member = \Base\Member\Utils\MockFactory::generateMember(1);

        $passwordCommand = 'Admin123$';
        $salt = 'W8i4';
        $password = md5(md5($passwordCommand).'W8i3');
        $member->setPassword($password);
        $member->setSalt($salt);

        $command = new SignInMemberCommand(
            '18800000000',
            $passwordCommand
        );

        $result = $this->commandHandler->memberPasswordCorrect($member, $command);

        $this->assertFalse($result);
        $this->assertEquals(PASSWORD_INCORRECT, Core::getLastError()->getId());
    }

    public function testMemberPasswordCorrectSuccess()
    {
        $member = \Base\Member\Utils\MockFactory::generateMember(1);

        $passwordCommand = 'Admin123$';
        $salt = 'W8i3';
        $password = md5(md5($passwordCommand).$salt);
        $member->setPassword($password);
        $member->setSalt($salt);

        $command = new SignInMemberCommand(
            '18800000000',
            $passwordCommand
        );

        $result = $this->commandHandler->memberPasswordCorrect($member, $command);

        $this->assertTrue($result);
    }
}
