<?php
namespace Base\Member\Adapter\Member;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Common\Model\IEnableAble;

use Base\Member\Model\Member;
use Base\Member\Model\NullMember;
use Base\Member\Translator\MemberDbTranslator;
use Base\Member\Adapter\Member\Query\MemberRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class MemberDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MemberDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchUserGroup'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IMemberAdapter
     */
    public function testImplementsIMemberAdapter()
    {
        $this->assertInstanceOf(
            'Base\Member\Adapter\Member\IMemberAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 MemberDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockMemberDbAdapter();
        $this->assertInstanceOf(
            'Base\Member\Translator\MemberDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 MemberRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockMemberDbAdapter();
        $this->assertInstanceOf(
            'Base\Member\Adapter\Member\Query\MemberRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockMemberDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedMember = new Member();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedMember)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedMember);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedMember = new Member();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedMember, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedMember, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedMember = new Member();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedMember);

        //验证
        $member = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedMember, $member);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $memberOne = new Member(1);
        $memberTwo = new Member(2);

        $ids = [1, 2];

        $expectedMemberList = [];
        $expectedMemberList[$memberOne->getId()] = $memberOne;
        $expectedMemberList[$memberTwo->getId()] = $memberTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedMemberList);

        //验证
        $memberList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedMemberList, $memberList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-cellphone
    public function testFormatFilterCellphone()
    {
        $filter = array(
            'cellphone' => '18800000000',
        );
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = 'cellphone = \''.$filter['cellphone'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatFilter-email
    public function testFormatFilterEmail()
    {
        $filter = array(
            'email' => 'email',
        );
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = 'email = \''.$filter['email'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatFilter-userName
    public function testFormatFilterUserName()
    {
        $filter = array(
            'userName' => 'userName',
        );
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = 'user_name = \''.$filter['userName'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    //filter-realName
    public function testFormatFilterRealName()
    {
        $filter = array(
            'realName' => 'realName'
        );

        $adapter = new MockMemberDbAdapter();

        $expectedCondition = 'real_name LIKE \'%'.$filter['realName'].'%\'';
        
        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    //filter-Status
    public function testFormatFilterStatus()
    {
        $filter = array(
            'status' => IEnableAble::STATUS['ENABLED']
        );
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = 'status = '.$filter['status'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortStatusDesc()
    {
        $sort = array('status' => -1);
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = ' ORDER BY status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortStatusAsc()
    {
        $sort = array('status' => 1);
        $adapter = new MockMemberDbAdapter();

        $expectedCondition = ' ORDER BY status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
