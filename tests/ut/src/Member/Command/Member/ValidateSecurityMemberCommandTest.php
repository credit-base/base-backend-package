<?php
namespace Base\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class ValidateSecurityMemberCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'securityAnswer' => md5($faker->word()),
            'id' => $faker->randomNumber()
        );

        $this->command = new ValidateSecurityMemberCommand(
            $this->fakerData['securityAnswer'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testSecurityAnswerParameter()
    {
        $this->assertEquals($this->fakerData['securityAnswer'], $this->command->securityAnswer);
    }
}
