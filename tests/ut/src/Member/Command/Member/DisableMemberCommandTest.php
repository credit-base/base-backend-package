<?php
namespace Base\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class DisableMemberCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new DisableMemberCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsDisableCommand()
    {
        $this->assertInstanceof('Base\Common\Command\DisableCommand', $this->stub);
    }
}
