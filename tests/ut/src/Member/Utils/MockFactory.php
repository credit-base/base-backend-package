<?php
namespace Base\Member\Utils;

use Base\Common\Model\IEnableAble;

use Base\Member\Model\Member;

class MockFactory
{
    public static function generateMember(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Member {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $member = new Member($id);
        $member->setId($id);

        //userName
        $userName = isset($value['userName']) ? $value['userName'] : $faker->phoneNumber();
        $member->setUserName($userName);

        //cellphone
        $cellphone = isset($value['cellphone']) ? $value['cellphone'] : $faker->phoneNumber();
        $member->setCellphone($cellphone);

        //realName
        $realName = isset($value['realName']) ? $value['realName'] : $faker->name();
        $member->setRealName($realName);

        //email
        $email = isset($value['email']) ? $value['email'] : $faker->email();
        $member->setEmail($email);

        $cardId = isset($value['cardId']) ? $value['cardId'] : $faker->creditCardNumber();
        $member->setCardId($cardId);

        $contactAddress = isset($value['contactAddress']) ? $value['contactAddress'] : $faker->address();
        $member->setContactAddress($contactAddress);

        $securityAnswer = isset($value['securityAnswer']) ? $value['securityAnswer'] : $faker->word();
        $member->setSecurityAnswer($securityAnswer);

        self::generateSecurityQuestion($member, $faker, $value);
        self::generateGender($member, $faker, $value);
        self::generatePassword($member, $faker, $value);
        self::generateSalt($member, $faker, $value);
        self::generateStatus($member, $faker, $value);
        
        $member->setCreateTime($faker->unixTime());
        $member->setUpdateTime($faker->unixTime());
        $member->setStatusTime($faker->unixTime());

        return $member;
    }

    private static function generateSecurityQuestion($member, $faker, $value) : void
    {
        $securityQuestion = isset($value['securityQuestion'])
        ? $value['securityQuestion']
        : $faker->randomElement(Member::GENDER);
        $member->setSecurityQuestion($securityQuestion);
    }

    private static function generateGender($member, $faker, $value) : void
    {
        $gender = isset($value['gender'])
        ? $value['gender']
        : $faker->randomElement(Member::SECURITY_QUESTION);
        $member->setGender($gender);
    }

    private static function generatePassword($member, $faker, $value) : void
    {
        $password = isset($value['password']) ?
            $value['password'] :
            md5($faker->randomNumber());
        $member->setPassword($password);
    }

    private static function generateSalt($member, $faker, $value) : void
    {
        unset($faker);

        $salt = isset($value['salt']) ? $value['salt'] : 'salt';
        $member->setSalt($salt);
    }

    private static function generateStatus($member, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(IEnableAble::STATUS);
        
        $member->setStatus($status);
    }
}
