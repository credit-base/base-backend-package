<?php
namespace Base\Member\Utils;

trait MemberUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $member
    ) {
        $this->assertEquals($expectedArray['member_id'], $member->getId());
        $this->assertEquals($expectedArray['user_name'], $member->getUserName());
        $this->assertEquals($expectedArray['real_name'], $member->getRealName());
        $this->assertEquals($expectedArray['cellphone'], $member->getCellphone());
        $this->assertEquals($expectedArray['email'], $member->getEmail());
        $this->assertEquals($expectedArray['cardid'], $member->getCardId());
        $this->assertEquals($expectedArray['contact_address'], $member->getContactAddress());
        $this->assertEquals($expectedArray['security_question'], $member->getSecurityQuestion());
        $this->assertEquals($expectedArray['security_answer'], $member->getSecurityAnswer());
        $this->assertEquals($expectedArray['gender'], $member->getGender());

        $this->assertEquals($expectedArray['status'], $member->getStatus());
        $this->assertEquals($expectedArray['create_time'], $member->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $member->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $member->getStatusTime());
    }
}
