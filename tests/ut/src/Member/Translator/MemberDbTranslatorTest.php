<?php
namespace Base\Member\Translator;

use PHPUnit\Framework\TestCase;

use Base\Member\Utils\MemberUtils;
use Base\Member\Utils\MockFactory;

class MemberDbTranslatorTest extends TestCase
{
    use MemberUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new MemberDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Member\Model\NullMember', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $member = MockFactory::generateMember(1);

        $expression['member_id'] = $member->getId();
        $expression['user_name'] = $member->getUserName();
        $expression['real_name'] = $member->getRealName();
        $expression['cellphone'] = $member->getCellphone();
        $expression['email'] = $member->getEmail();
        $expression['cardid'] = $member->getCardId();
        $expression['contact_address'] = $member->getContactAddress();
        $expression['security_question'] = $member->getSecurityQuestion();
        $expression['security_answer'] = $member->getSecurityAnswer();
        $expression['gender'] = $member->getGender();
        $expression['password'] = $member->getPassword();
        $expression['salt'] = $member->getSalt();

        $expression['status'] = $member->getStatus();
        $expression['status_time'] = $member->getStatusTime();
        $expression['create_time'] = $member->getCreateTime();
        $expression['update_time'] = $member->getUpdateTime();

        $member = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Member\Model\Member', $member);
        $this->compareArrayAndObject($expression, $member);
    }

    public function testObjectToArray()
    {
        $member = MockFactory::generateMember(1);

        $expression = $this->translator->objectToArray($member);

        $this->compareArrayAndObject($expression, $member);
    }
}
