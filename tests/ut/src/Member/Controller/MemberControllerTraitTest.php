<?php
namespace Base\Member\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\User\WidgetRule\UserWidgetRule;

use Base\Member\WidgetRule\MemberWidgetRule;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class MemberControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockMemberControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetMemberWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Member\WidgetRule\MemberWidgetRule',
            $this->trait->getMemberWidgetRulePublic()
        );
    }

    public function testGetUserWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\User\WidgetRule\UserWidgetRule',
            $this->trait->getUserWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Member\Repository\MemberRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    public function testValidateAddScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getUserWidgetRule', 'getMemberWidgetRule']) ->getMock();

        $userName = 'userName';
        $realName = 'realName';
        $cardId = 'cardId';
        $cellphone = 'cellphone';
        $email = 'email';
        $contactAddress = 'contactAddress';
        $securityAnswer = 'securityAnswer';
        $password = 'password';
        $securityQuestion = 'securityQuestion';

        $userWidgetRule = $this->prophesize(UserWidgetRule::class);
        $userWidgetRule->realName(Argument::exact($realName))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->cardId(Argument::exact($cardId))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->cellphone(Argument::exact($cellphone))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->password(
            Argument::exact($password),
            Argument::exact('password')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getUserWidgetRule')
            ->willReturn($userWidgetRule->reveal());

        $memberWidgetRule = $this->prophesize(MemberWidgetRule::class);
        $memberWidgetRule->userName(Argument::exact($userName))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->email(Argument::exact($email))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->contactAddress(Argument::exact($contactAddress))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->securityAnswer(Argument::exact($securityAnswer))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->securityQuestion(
            Argument::exact($securityQuestion)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRule')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateAddScenarioPublic(
            $userName,
            $realName,
            $cardId,
            $cellphone,
            $email,
            $contactAddress,
            $securityAnswer,
            $password,
            $securityQuestion
        );
        
        $this->assertTrue($result);
    }

    public function testValidateEditScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getMemberWidgetRule']) ->getMock();

        $gender = 'gender';

        $memberWidgetRule = $this->prophesize(MemberWidgetRule::class);
        $memberWidgetRule->gender(Argument::exact($gender))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRule')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateEditScenarioPublic($gender);
        
        $this->assertTrue($result);
    }

    public function testValidateSignInScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getUserWidgetRule', 'getMemberWidgetRule']) ->getMock();

        $userName = 'userName';
        $password = 'password';

        $userWidgetRule = $this->prophesize(UserWidgetRule::class);
        $userWidgetRule->password(
            Argument::exact($password),
            Argument::exact('password')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getUserWidgetRule')
            ->willReturn($userWidgetRule->reveal());

        $memberWidgetRule = $this->prophesize(MemberWidgetRule::class);
        $memberWidgetRule->userName(Argument::exact($userName))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRule')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateSignInScenarioPublic(
            $userName,
            $password
        );
        
        $this->assertTrue($result);
    }

    public function testValidateResetPasswordScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getUserWidgetRule', 'getMemberWidgetRule']) ->getMock();

        $userName = 'userName';
        $securityAnswer = 'securityAnswer';
        $password = 'password';

        $userWidgetRule = $this->prophesize(UserWidgetRule::class);
        $userWidgetRule->password(
            Argument::exact($password),
            Argument::exact('password')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getUserWidgetRule')
            ->willReturn($userWidgetRule->reveal());

        $memberWidgetRule = $this->prophesize(MemberWidgetRule::class);
        $memberWidgetRule->userName(Argument::exact($userName))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->securityAnswer(Argument::exact($securityAnswer))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRule')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateResetPasswordScenarioPublic(
            $userName,
            $securityAnswer,
            $password
        );
        
        $this->assertTrue($result);
    }

    public function testValidateUpdatePasswordScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getUserWidgetRule']) ->getMock();

        $oldPassword = 'oldPassword';
        $password = 'password';

        $userWidgetRule = $this->prophesize(UserWidgetRule::class);
        $userWidgetRule->password(
            Argument::exact($oldPassword),
            Argument::exact('oldPassword')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->password(
            Argument::exact($password),
            Argument::exact('password')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getUserWidgetRule')
            ->willReturn($userWidgetRule->reveal());

        $result = $controller->validateUpdatePasswordScenarioPublic(
            $oldPassword,
            $password
        );
        
        $this->assertTrue($result);
    }

    public function testValidateValidateSecurityScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getMemberWidgetRule']) ->getMock();

        $securityAnswer = 'securityAnswer';

        $memberWidgetRule = $this->prophesize(MemberWidgetRule::class);
        $memberWidgetRule->securityAnswer(Argument::exact($securityAnswer))->shouldBeCalledTimes(1)->willReturn(true);
        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRule')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateValidateSecurityScenarioPublic($securityAnswer);
        
        $this->assertTrue($result);
    }
}
