<?php
namespace Base\Member\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\User\WidgetRule\UserWidgetRule;

use Base\Member\Model\Member;
use Base\Member\WidgetRule\MemberWidgetRule;
use Base\Member\Repository\MemberRepository;
use Base\Member\Command\Member\AddMemberCommand;
use Base\Member\Command\Member\EditMemberCommand;
use Base\Member\Command\Member\SignInMemberCommand;

class MemberOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MemberOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    /**
     * 测试 add 成功
     * 1. 为 MemberOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
     *    getMemberWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 Member 类建立预言
     * 4. 为 MemberRepository 类建立预言, MemberRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Member, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 为 MemberOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
        // getMemberWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(MemberOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateAddScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 true
        $this->initAdd($controller, true);

        // 为 Member 类建立预言
        $member  = $this->prophesize(Member::class);

        // 为 MemberRepository 类建立预言, MemberRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Member, getRepository 方法被调用一次
        $id = 0;
        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->add 方法被调用一次, 且返回结果为 true
        $result = $controller->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 MemberOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
     *    getMemberWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 为 MemberOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
        // getMemberWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(MemberOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateAddScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd($controller, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->add 方法被调用一次, 且返回结果为 false
        $result = $controller->add();
        $this->assertFalse($result);
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 UserWidgetRule 类建立预言, 验证请求参数,  getUserWidgetRule 方法被调用一次
     * 4. 为 MemberWidgetRule 类建立预言, 验证请求参数, getMemberWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddMemberCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(MemberOperateController $controller, bool $result)
    {
        // mock 请求参数
        $data = array(
            'attributes' => array(
                "userName"=>"用户名",
                "realName"=>"姓名",
                "cardId"=>"身份证号",
                "cellphone"=>"手机号",
                "email"=>"邮箱",
                "contactAddress"=>'联系地址',
                "securityQuestion"=>1,
                "securityAnswer"=>'密保答案',
                "password"=>'密码'
            ),
        );
        $attributes = $data['attributes'];
        $userName = $attributes['userName'];
        $realName = $attributes['realName'];
        $cardId = $attributes['cardId'];
        $cellphone = $attributes['cellphone'];
        $email = $attributes['email'];
        $contactAddress = $attributes['contactAddress'];
        $securityAnswer = $attributes['securityAnswer'];
        $securityQuestion = $attributes['securityQuestion'];
        $password = $attributes['password'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateAddScenario')
            ->willReturn(true);

        // 为 CommandBus 类建立预言, 传入 AddMemberCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AddMemberCommand(
                    $userName,
                    $realName,
                    $cardId,
                    $cellphone,
                    $email,
                    $contactAddress,
                    $securityAnswer,
                    $password,
                    $securityQuestion
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    /**
     * 测试 edit 成功
     * 1. 为 MemberOperateController 类建立桩件, 并模仿 getRequest、
     *    getMemberWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initEdit(), 期望结果为 true
     * 3. 为 Member 类建立预言
     * 4. 为 MemberRepository 类建立预言, MemberRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Member, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 MemberOperateController 类建立桩件, 并模仿 getRequest、
        // getMemberWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(MemberOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateEditScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initEdit($controller, $id, true);

        // 为 Member 类建立预言
        $member = $this->prophesize(Member::class);

        // 为 MemberRepository 类建立预言, MemberRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Member, getRepository 方法被调用一次
        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->edit 方法被调用一次, 且返回结果为 true
        $result = $controller->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 MemberOperateController 类建立桩件, 并模仿 getRequest、
     *    getMemberWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initEdit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 MemberOperateController 类建立桩件, 并模仿 getRequest、
        // getMemberWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(MemberOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateEditScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 false
        $this->initEdit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->edit 方法被调用一次, 且返回结果为 false
        $result = $controller->edit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 edit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 MemberWidgetRule 类建立预言, 验证请求参数, getMemberWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 EditMemberCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEdit(MemberOperateController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = array(
            'attributes' => array(
                "gender"=>1
            ),
        );

        $attributes = $data['attributes'];
        $gender = $attributes['gender'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateEditScenario')
            ->willReturn(true);

        // 为 CommandBus 类建立预言, 传入 EditMemberCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditMemberCommand(
                    $gender,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    /**
     * 测试 signIn 成功
     * 1. 为 MemberOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
     *    getMemberWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initSignIn(), 期望结果为 true
     * 3. 为 Member 类建立预言
     * 4. 为 MemberRepository 类建立预言, MemberRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Member, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->signIn 方法被调用一次, 且返回结果为 true
     */
    public function testSignIn()
    {
        // 为 MemberOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
        // getMemberWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(MemberOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateSignInScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $this->initSignIn($controller, true);

        // 为 Member 类建立预言
        $member = $this->prophesize(Member::class);
        $id = 0;

        // 为 MemberRepository 类建立预言, MemberRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Member, getRepository 方法被调用一次
        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->signIn 方法被调用一次, 且返回结果为 true
        $result = $controller->signIn();
        $this->assertTrue($result);
    }

    /**
     * 测试 signIn 失败
     * 1. 为 MemberOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
     *    getMemberWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initSignIn(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->signIn 方法被调用一次, 且返回结果为 false
     */
    public function testSignInFail()
    {
        // 为 MemberOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
        // getMemberWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(MemberOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateSignInScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initSignIn(), 期望结果为 false
        $this->initSignIn($controller, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->signIn 方法被调用一次, 且返回结果为 false
        $result = $controller->signIn();
        $this->assertFalse($result);
    }

    /**
     * 初始化 signIn 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 MemberWidgetRule 类建立预言, 验证请求参数, getMemberWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 SignInMemberCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initSignIn(MemberOperateController $controller, bool $result)
    {
        // mock 请求参数
        $data = array(
            'attributes' => array(
                "userName"=>'用户名',
                "password"=>'Admin123$'
            ),
        );

        $attributes = $data['attributes'];
        $userName = $attributes['userName'];
        $password = $attributes['password'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

       
        $controller->expects($this->exactly(1))
            ->method('validateSignInScenario')
            ->willReturn(true);
        // 为 CommandBus 类建立预言, 传入 SignInMemberCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new SignInMemberCommand(
                    $userName,
                    $password
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }
}
