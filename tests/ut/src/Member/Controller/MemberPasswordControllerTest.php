<?php
namespace Base\Member\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;
use Base\Member\Command\Member\ResetPasswordMemberCommand;
use Base\Member\Command\Member\UpdatePasswordMemberCommand;
use Base\Member\Command\Member\ValidateSecurityMemberCommand;

class MemberPasswordControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MemberPasswordController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testResetPassword()
    {
        $controller = $this->getMockBuilder(MemberPasswordController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateResetPasswordScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $this->initResetPassword($controller, true);

        $member  = $this->prophesize(Member::class);

        $id = 0;
        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        $result = $controller->resetPassword();
        $this->assertTrue($result);
    }

    public function testResetPasswordFail()
    {
        $controller = $this->getMockBuilder(MemberPasswordController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateResetPasswordScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        $this->initResetPassword($controller, false);

        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $controller->resetPassword();
        $this->assertFalse($result);
    }

    protected function initResetPassword(MemberPasswordController $controller, bool $result)
    {
        // mock 请求参数
        $data = array(
            'attributes' => array(
                "userName"=>"用户名",
                "securityAnswer"=>'密保答案',
                "password"=>'密码'
            ),
        );
        $attributes = $data['attributes'];
        $userName = $attributes['userName'];
        $securityAnswer = $attributes['securityAnswer'];
        $password = $attributes['password'];

        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateResetPasswordScenario')
            ->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ResetPasswordMemberCommand(
                    $userName,
                    $securityAnswer,
                    $password
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testUpdatePassword()
    {
        $controller = $this->getMockBuilder(MemberPasswordController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateUpdatePasswordScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initUpdatePassword($controller, $id, true);

        $member = $this->prophesize(Member::class);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        $result = $controller->updatePassword($id);
        $this->assertTrue($result);
    }

    public function testUpdatePasswordFail()
    {
        $controller = $this->getMockBuilder(MemberPasswordController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateUpdatePasswordScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        $this->initUpdatePassword($controller, $id, false);

        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $controller->updatePassword($id);
        $this->assertFalse($result);
    }

    protected function initUpdatePassword(MemberPasswordController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = array(
            'attributes' => array(
                "oldPassword" => "oldPassword",
                "password" => "password"
            ),
        );

        $attributes = $data['attributes'];
        $oldPassword = $attributes['oldPassword'];
        $password = $attributes['password'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateUpdatePasswordScenario')
            ->willReturn(true);

        // 为 CommandBus 类建立预言, 传入 UpdatePasswordMemberCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new UpdatePasswordMemberCommand(
                    $password,
                    $oldPassword,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testValidateSecurity()
    {
        $controller = $this->getMockBuilder(MemberPasswordController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateValidateSecurityScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initValidateSecurity($controller, $id, true);

        $member = $this->prophesize(Member::class);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        $result = $controller->validateSecurity($id);
        $this->assertTrue($result);
    }

    public function testValidateSecurityFail()
    {
        $controller = $this->getMockBuilder(MemberPasswordController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateValidateSecurityScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        $this->initValidateSecurity($controller, $id, false);

        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $controller->validateSecurity($id);
        $this->assertFalse($result);
    }

    protected function initValidateSecurity(MemberPasswordController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = array(
            'attributes' => array(
                "securityAnswer" => "securityAnswer"
            ),
        );

        $attributes = $data['attributes'];
        $securityAnswer = $attributes['securityAnswer'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateValidateSecurityScenario')
            ->willReturn(true);

        // 为 CommandBus 类建立预言, 传入 ValidateSecurityMemberCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ValidateSecurityMemberCommand(
                    $securityAnswer,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }
}
