<?php
namespace Base\Member\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Member\Model\Member;
use Base\Member\View\MemberView;
use Base\Member\Model\NullMember;
use Base\Member\Adapter\Member\IMemberAdapter;

class MemberFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(MemberFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockMemberFetchController();

        $this->assertInstanceOf(
            'Base\Member\Adapter\Member\IMemberAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockMemberFetchController();

        $this->assertInstanceOf(
            'Base\Member\View\MemberView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockMemberFetchController();

        $this->assertEquals(
            'members',
            $controller->getResourceName()
        );
    }
}
