<?php
namespace Base\Member\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\CommandBus;

use Base\Member\Utils\MockFactory;
use Base\Member\Repository\MemberRepository;
use Base\Member\Command\Member\EnableMemberCommand;
use Base\Member\Command\Member\DisableMemberCommand;

class MemberEnableControllerTest extends TestCase
{
    private $memberStub;

    public function setUp()
    {
        $this->memberStub = $this->getMockBuilder(MockMemberEnableController::class)
        ->setMethods([
            'displayError',
            ])
        ->getMock();
    }

    public function teatDown()
    {
        unset($this->memberStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Member\Repository\MemberRepository',
            $this->memberStub->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->memberStub->getCommandBus()
        );
    }

    private function initialDisable($result)
    {
        $this->memberStub = $this->getMockBuilder(MockMemberEnableController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new DisableMemberCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->memberStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDisableFailure()
    {
        $command = $this->initialDisable(false);

        $this->memberStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->memberStub->disable($command->id);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $command = $this->initialDisable(true);

        $member = MockFactory::generateMember($command->id);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);
        $this->memberStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->memberStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->memberStub->disable($command->id);
        $this->assertTrue($result);
    }

    private function initialEnable($result)
    {
        $this->memberStub = $this->getMockBuilder(MockMemberEnableController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new EnableMemberCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->memberStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testEnableFailure()
    {
        $command = $this->initialEnable(false);

        $this->memberStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->memberStub->enable($command->id);
        $this->assertFalse($result);
    }

    public function testEnableSuccess()
    {
        $command = $this->initialEnable(true);

        $member = MockFactory::generateMember($command->id);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);

        $this->memberStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->memberStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->memberStub->enable($command->id);
        $this->assertTrue($result);
    }
}
