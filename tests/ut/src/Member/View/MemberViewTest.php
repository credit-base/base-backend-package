<?php
namespace Base\Member\View;

use PHPUnit\Framework\TestCase;

use Base\Member\Model\Member;

class MemberViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $member = new MemberView(new Member());
        $this->assertInstanceof('Base\Common\View\CommonView', $member);
    }
}
