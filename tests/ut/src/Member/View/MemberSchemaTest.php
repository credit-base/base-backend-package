<?php
namespace Base\Member\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class MemberSchemaTest extends TestCase
{
    private $memberSchema;

    private $member;

    public function setUp()
    {
        $this->memberSchema = new MemberSchema(new Factory());

        $this->member = \Base\Member\Utils\MockFactory::generateMember(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->memberSchema);
        unset($this->member);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->memberSchema);
    }

    public function testGetId()
    {
        $result = $this->memberSchema->getId($this->member);

        $this->assertEquals($result, $this->member->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->memberSchema->getAttributes($this->member);

        $this->assertEquals($result['userName'], $this->member->getUserName());
        $this->assertEquals($result['realName'], $this->member->getRealName());
        $this->assertEquals($result['cellphone'], $this->member->getCellphone());
        $this->assertEquals($result['email'], $this->member->getEmail());
        $this->assertEquals($result['cardId'], $this->member->getCardId());
        $this->assertEquals($result['contactAddress'], $this->member->getContactAddress());
        $this->assertEquals($result['securityQuestion'], $this->member->getSecurityQuestion());
        $this->assertEquals($result['gender'], $this->member->getGender());
        $this->assertEquals($result['status'], $this->member->getStatus());
        $this->assertEquals($result['createTime'], $this->member->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->member->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->member->getStatusTime());
    }
}
