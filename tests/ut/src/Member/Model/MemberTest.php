<?php
namespace Base\Member\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Model\IEnableAble;

use Base\Member\Adapter\Member\IMemberAdapter;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class MemberTest extends TestCase
{
    private $member;

    public function setUp()
    {
        $this->member = new MockMember();
    }

    public function tearDown()
    {
        unset($this->member);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IOperate',
            $this->member
        );
    }

    public function testConstructor()
    {
        $this->assertEmpty($this->member->getRealName());
        $this->assertEmpty($this->member->getEmail());
        $this->assertEmpty($this->member->getCardId());
        $this->assertEmpty($this->member->getContactAddress());
        $this->assertEquals(MEMBER::SECURITY_QUESTION['NULL'], $this->member->getSecurityQuestion());
        $this->assertEmpty($this->member->getSecurityAnswer());
        $this->assertEquals(MEMBER::GENDER['NULL'], $this->member->getGender());
        //测试初始化状态
        $this->assertEquals(IEnableAble::STATUS['ENABLED'], $this->member->getStatus());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Member setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->member->setId(1);
        $this->assertEquals(1, $this->member->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //realName 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setRealName() 正确的传参类型,期望传值正确
     */
    public function testSetRealNameCorrectType()
    {
        $this->member->setRealName('string');
        $this->assertEquals('string', $this->member->getRealName());
    }

    /**
     * 设置 Member setRealName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRealNameWrongType()
    {
        $this->member->setRealName(array(1,2,3));
    }
    //realName 测试 --------------------------------------------------------   end

    //email 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setEmail() 正确的传参类型,期望传值正确
     */
    public function testSetEmailCorrectType()
    {
        $this->member->setEmail('string');
        $this->assertEquals('string', $this->member->getEmail());
    }

    /**
     * 设置 Member setEmail() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEmailWrongType()
    {
        $this->member->setEmail(array(1,2,3));
    }
    //email 测试 --------------------------------------------------------   end

    //cardId 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setCardId() 正确的传参类型,期望传值正确
     */
    public function testSetCardIdCorrectType()
    {
        $this->member->setCardId('string');
        $this->assertEquals('string', $this->member->getCardId());
    }

    /**
     * 设置 Member setCardId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCardIdWrongType()
    {
        $this->member->setCardId(array(1,2,3));
    }
    //cardId 测试 --------------------------------------------------------   end

    //contactAddress 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setContactAddress() 正确的传参类型,期望传值正确
     */
    public function testSetContactAddressCorrectType()
    {
        $this->member->setContactAddress('string');
        $this->assertEquals('string', $this->member->getContactAddress());
    }

    /**
     * 设置 Member setContactAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContactAddressWrongType()
    {
        $this->member->setContactAddress(array(1,2,3));
    }
    //contactAddress 测试 --------------------------------------------------------   end

    //securityQuestion 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Member setSecurityQuestion() 是否符合预定范围
     *
     * @dataProvider securityQuestionProvider
     */
    public function testSetSecurityQuestion($actual, $expected)
    {
        $this->member->setSecurityQuestion($actual);
        $this->assertEquals($expected, $this->member->getSecurityQuestion());
    }

    /**
     * 循环测试 Member setSecurityQuestion() 数据构建器
     */
    public function securityQuestionProvider()
    {
        return array(
            array(Member::SECURITY_QUESTION['QUESTION_ONE'], Member::SECURITY_QUESTION['QUESTION_ONE']),
            array(Member::SECURITY_QUESTION['QUESTION_TWO'], Member::SECURITY_QUESTION['QUESTION_TWO']),
            array(Member::SECURITY_QUESTION['QUESTION_THREE'], Member::SECURITY_QUESTION['QUESTION_THREE']),
            array(Member::SECURITY_QUESTION['QUESTION_FOUR'], Member::SECURITY_QUESTION['QUESTION_FOUR']),
            array(Member::SECURITY_QUESTION['QUESTION_FIVE'], Member::SECURITY_QUESTION['QUESTION_FIVE']),
            array(999, Member::SECURITY_QUESTION['NULL']),
        );
    }

    /**
     * 设置 Member setSecurityQuestion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSecurityQuestionWrongType()
    {
        $this->member->setSecurityQuestion('string');
    }
    //securityQuestion 测试 ------------------------------------------------------   end

    //encryptSecurityAnswer 测试 ---------------------------------------------  start
    /**
     * 设置Member encryptSecurityAnswer() ,期望产生加密过的密保答案
     */
    public function testEncryptSecurityAnswerWithoutSalt()
    {
        //初始化密保答案
        $securityAnswer = '密保答案';
        $this->member->encryptSecurityAnswer($securityAnswer);

        //确认密保答案是一个32位长度加密过的md5值
        $this->assertEquals(32, strlen($this->member->getSecurityAnswer()));
    }

    /**
     * 设置Member encryptSecurityAnswer()
     *
     * 1. 先生成密保答案
     * 2. 传入原始密保答案,确认再次加密后的值和第一次生成的密保答案一致
     */
    public function testEncryptSecurityAnswerWithSalt()
    {
        //初始化密保答案
        $securityAnswer = '密保答案';
        $this->member->encryptSecurityAnswer($securityAnswer);

        //初始化一个新的用户,再次加密
        $member = new Member();
        $member->encryptSecurityAnswer($securityAnswer);

        //校验第一次生成的密保答案和盐,再次加密期望一致
        $this->assertEquals($this->member->getSecurityAnswer(), $member->getSecurityAnswer());
    }
    //encryptSecurityAnswer 测试 ----------------------------------------------  end

    //securityAnswer 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setSecurityAnswer() 正确的传参类型,期望传值正确
     */
    public function testSetSecurityAnswerCorrectType()
    {
        $this->member->setSecurityAnswer('string');
        $this->assertEquals('string', $this->member->getSecurityAnswer());
    }

    /**
     * 设置 Member setSecurityAnswer() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSecurityAnswerWrongType()
    {
        $this->member->setSecurityAnswer(array(1,2,3));
    }
    //securityAnswer 测试 --------------------------------------------------------   end

    //gender 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Member setGender() 是否符合预定范围
     *
     * @dataProvider genderProvider
     */
    public function testSetGender($actual, $expected)
    {
        $this->member->setGender($actual);
        $this->assertEquals($expected, $this->member->getGender());
    }

    /**
     * 循环测试 Member setGender() 数据构建器
     */
    public function genderProvider()
    {
        return array(
            array(Member::GENDER['MALE'], Member::GENDER['MALE']),
            array(Member::GENDER['FEMALE'], Member::GENDER['FEMALE']),
            array(999, Member::GENDER['NULL']),
        );
    }

    /**
     * 设置 Member setGender() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGenderWrongType()
    {
        $this->member->setGender('string');
    }
    //gender 测试 ------------------------------------------------------   end


    public function testGetRepository()
    {
        $this->assertInstanceOf('Base\Member\Adapter\Member\IMemberAdapter', $this->member->getRepository());
    }

    private function initialIsCellphoneExist($result)
    {
        $this->member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $id = 1;
        
        $member = \Base\Member\Utils\MockFactory::generateMember($id);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($member);
        }

        $this->member->setCellphone($member->getCellphone());

        $filter['cellphone'] = $member->getCellphone();

        $repository = $this->prophesize(IMemberAdapter::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->member->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testIsCellphoneExistFailure()
    {
        $this->initialIsCellphoneExist(false);

        $result = $this->member->isCellphoneExist();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
        $this->assertEquals('cellphone', Core::getLastError()->getSource()['pointer']);
    }

    public function testIsCellphoneExistSuccess()
    {
        $this->initialIsCellphoneExist(true);
        
        $result = $this->member->isCellphoneExist();

        $this->assertTrue($result);
    }

    private function initialIsEmailExist($result)
    {
        $this->member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $id = 1;
        
        $member = \Base\Member\Utils\MockFactory::generateMember($id);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($member);
        }

        $this->member->setEmail($member->getEmail());

        $filter['email'] = $member->getEmail();

        $repository = $this->prophesize(IMemberAdapter::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->member->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testIsEmailExistFailure()
    {
        $this->initialIsEmailExist(false);

        $result = $this->member->isEmailExist();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
        $this->assertEquals('email', Core::getLastError()->getSource()['pointer']);
    }

    public function testIsEmailExistSuccess()
    {
        $this->initialIsEmailExist(true);
        
        $result = $this->member->isEmailExist();

        $this->assertTrue($result);
    }

    private function initialIsUserNameExist($result)
    {
        $this->member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $id = 1;
        
        $member = \Base\Member\Utils\MockFactory::generateMember($id);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($member);
        }

        $this->member->setUserName($member->getUserName());

        $filter['userName'] = $member->getUserName();

        $repository = $this->prophesize(IMemberAdapter::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->member->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testIsUserNameExistFailure()
    {
        $this->initialIsUserNameExist(false);

        $result = $this->member->isUserNameExist();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
        $this->assertEquals('userName', Core::getLastError()->getSource()['pointer']);
    }

    public function testIsUserNameExistSuccess()
    {
        $this->initialIsUserNameExist(true);
        
        $result = $this->member->isUserNameExist();

        $this->assertTrue($result);
    }

    public function testAddSuccess()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository', 'isCellphoneExist', 'isEmailExist', 'isUserNameExist'])
                           ->getMock();

        $member->expects($this->once())->method('isCellphoneExist')->willReturn(true);
        $member->expects($this->once())->method('isEmailExist')->willReturn(true);
        $member->expects($this->once())->method('isUserNameExist')->willReturn(true);

        //预言 IMemberAdapter
        $repository = $this->prophesize(IMemberAdapter::class);
        $repository->add(Argument::exact($member))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $member->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $member->add();
        $this->assertTrue($result);
    }

    public function testEditSuccess()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository', 'setUpdateTime'])
                           ->getMock();

        $member->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));
        //预言 IMemberAdapter
        $repository = $this->prophesize(IMemberAdapter::class);
        $repository->edit(Argument::exact($member), Argument::exact(array(
            'gender',
            'updateTime'
        )))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $member->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $member->edit();
        $this->assertTrue($result);
    }

    public function testUpdatePassword()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository', 'setUpdateTime', 'encryptPassword'])
                           ->getMock();

        $newPassword = 'newPassword';

        $member->expects($this->exactly(1))
             ->method('encryptPassword')
             ->with($newPassword);

        $member->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IMemberAdapter
        $repository = $this->prophesize(IMemberAdapter::class);
        $repository->edit(Argument::exact($member), Argument::exact(array(
            'updateTime',
            'password',
            'salt',
        )))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $member->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $member->updatePassword($newPassword);
        $this->assertTrue($result);
    }

    public function testVerifyPasswordSuccess()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['encryptPassword', 'getPassword'])
                           ->getMock();

        $oldPassword = 'oldPassword';
        $salt = 'salt';
        $password = md5(md5($oldPassword).$salt);
        $member->setPassword($password);
        $member->setSalt($salt);

        $member->expects($this->exactly(1))
             ->method('encryptPassword')
             ->with($oldPassword, $salt);

        $member->expects($this->exactly(2))->method('getPassword')->willReturn($password);

        //验证
        $result = $member->verifyPassword($oldPassword);
        $this->assertTrue($result);
    }

    public function testVerifyPasswordFail()
    {
        $oldPassword = 'oldPassword';

        //验证
        $result = $this->member->verifyPassword($oldPassword);
        $this->assertFalse($result);
        $this->assertEquals(PASSWORD_INCORRECT, Core::getLastError()->getId());
        $this->assertEquals('oldPassword', Core::getLastError()->getSource()['pointer']);
    }

    public function testValidateSecuritySuccess()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['encryptSecurityAnswer', 'getSecurityAnswer'])
                           ->getMock();

        $securityAnswer = 'securityAnswer';
        $securityAnswerMd5 = md5(md5($securityAnswer));
        $member->setSecurityAnswer($securityAnswerMd5);

        $member->expects($this->exactly(1))
             ->method('encryptSecurityAnswer')
             ->with($securityAnswer);

        $member->expects($this->exactly(2))->method('getSecurityAnswer')->willReturn($securityAnswerMd5);

        //验证
        $result = $member->validateSecurity($securityAnswer);
        $this->assertTrue($result);
    }

    public function testValidateSecurityFail()
    {
        $securityAnswer = 'securityAnswer';

        //验证
        $result = $this->member->validateSecurity($securityAnswer);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_ERROR, Core::getLastError()->getId());
        $this->assertEquals('securityAnswer', Core::getLastError()->getSource()['pointer']);
    }
}
