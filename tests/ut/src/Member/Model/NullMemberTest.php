<?php
namespace Base\Member\Model;

use PHPUnit\Framework\TestCase;

class NullMemberTest extends TestCase
{
    private $member;

    public function setUp()
    {
        $this->member = NullMember::getInstance();
    }

    public function tearDown()
    {
        unset($this->member);
    }

    public function testExtendsMember()
    {
        $this->assertInstanceof('Base\Member\Model\Member', $this->member);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->member);
    }
}
