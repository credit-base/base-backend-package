<?php
namespace Base\Crew\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Adapter\Crew\ICrewAdapter;
use Base\Crew\Adapter\Crew\CrewDbAdapter;

class CrewRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(CrewRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->childRepository = new class extends CrewRepository
        {
            public function getAdapter() : ICrewAdapter
            {
                return parent::getAdapter();
            }

            public function getActualAdapter() : ICrewAdapter
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter() : ICrewAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new CrewDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->childRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Crew\Adapter\Crew\ICrewAdapter',
            $this->childRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Crew\Adapter\Crew\CrewDbAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Crew\Adapter\Crew\ICrewAdapter',
            $this->childRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Crew\Adapter\Crew\CrewMockAdapter',
            $this->childRepository->getMockAdapter()
        );
    }

    public function testAdd()
    {
        $id = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);
        
        $adapter = $this->prophesize(ICrewAdapter::class);
        $adapter->add(Argument::exact($crew))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($crew);
    }

    public function testEdit()
    {
        $id = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);
        $keys = array();
        
        $adapter = $this->prophesize(ICrewAdapter::class);
        $adapter->edit(Argument::exact($crew), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($crew, $keys);
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(ICrewAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(ICrewAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(ICrewAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }
}
