<?php
namespace Base\Crew\CommandHandler\Crew;

use PHPUnit\Framework\TestCase;

class DisableCrewCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDisableCrewCommandHandler::class)
        ->setMethods(['fetchCrew'])
        ->getMock();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);

        $this->stub->expects($this->once())->method('fetchCrew')->with($id)->willReturn($crew);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $crew);
    }
}
