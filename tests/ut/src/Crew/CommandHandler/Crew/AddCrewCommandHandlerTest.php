<?php
namespace Base\Crew\CommandHandler\Crew;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Crew\Model\Crew;
use Base\UserGroup\Model\UserGroup;
use Base\Department\Model\Department;

use Base\Crew\Command\Crew\AddCrewCommand;

class AddCrewCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddCrewCommandHandler::class)
                                     ->setMethods(['getCrew', 'fetchUserGroup', 'fetchDepartment'])
                                     ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    //getCrew
    /**
     * 测试根据不同分类返回 crew
     */
    public function testGetCrew()
    {
        $commandHandler = new MockAddCrewCommandHandler();
        foreach (Crew::CATEGORY_MAPS as $category => $crew) {
            $this->assertInstanceOf(
                $crew,
                $commandHandler->getCrew($category)
            );
        }
    }

    //execute
    /**
     * 测试执行命令成功
     * 1. getCrew 被调用一次
     * 2. 测试 crew 赋值 set
     * 3. $crew->add() 调用一次, 返回 true
     * 4. command->id 赋值为 $crew->getId()
     * 5. 返回true
     */
    public function testExecuteSuccess()
    {
        //初始化
        $faker = \Faker\Factory::create('zh_CN');
        $command = new AddCrewCommand(
            $faker->randomNumber(),
            $faker->name(),
            $faker->phoneNumber(),
            '610103190000000000',
            $faker->password(),
            array(),
            $faker->randomNumber(),
            $faker->randomNumber()
        );
        $expectId = 10;

        //预言
        $crew = $this->prophesizeCrew($command);
        $crew->getId()->shouldBeCalledTimes(1)->willReturn($expectId);
        $crew->add()->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $this->commandHandler->expects($this->any())->method('getCrew')->willReturn($crew->reveal());

        //验证
        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
        $this->assertEquals($expectId, $command->id);
    }

    /**
     * 测试执行命令失败
     * 1. getCrew 被调用一次
     * 2. 测试 crew 赋值 set
     * 3. $crew->add() 调用一次, 返回 false
     * 4. command->id 为 0, $crew->getId() 没有被调用
     * 5. 返回false
     */
    public function testExecuteFail()
    {
        //初始化
        $faker = \Faker\Factory::create('zh_CN');
        $command = new AddCrewCommand(
            $faker->randomNumber(),
            $faker->name(),
            $faker->phoneNumber(),
            '610103190000000000',
            $faker->password(),
            array(),
            $faker->randomNumber(),
            $faker->randomNumber()
        );
        $expectId = 0;

        //预言
        $crew = $this->prophesizeCrew($command);
        $crew->add()->shouldBeCalledTimes(1)->willReturn(false);

        //绑定
        $this->commandHandler->expects($this->any())->method('getCrew')->willReturn($crew->reveal());

        //验证
        $result = $this->commandHandler->execute($command);
        
        $this->assertFalse($result);
        $this->assertEquals($expectId, $command->id);
    }

    /**
     * 为 Crew 类建立预言
     */
    protected function prophesizeCrew(ICommand $command)
    {
        $crew = $this->prophesize(Crew::class);
        $crew->setCategory(Argument::exact($command->category))->shouldBeCalledTimes(1);
        $crew->setRealName(Argument::exact($command->realName))->shouldBeCalledTimes(1);
        $crew->setCellphone(Argument::exact($command->cellphone))->shouldBeCalledTimes(1);
        $crew->setUserName(Argument::exact($command->cellphone))->shouldBeCalledTimes(1);
        $crew->setCardId(Argument::exact($command->cardId))->shouldBeCalledTimes(1);
        $crew->encryptPassword(Argument::exact($command->password))->shouldBeCalledTimes(1);
        $crew->setPurview(Argument::exact($command->purview))->shouldBeCalledTimes(1);
        
        $userGroup = new UserGroup($command->userGroupId);
        $department = new Department($command->departmentId);
        $this->commandHandler->expects($this->exactly(1))
                         ->method('fetchUserGroup')
                         ->with($command->userGroupId)
                         ->willReturn($userGroup);
        $this->commandHandler->expects($this->exactly(1))
                         ->method('fetchDepartment')
                         ->with($command->departmentId)
                         ->willReturn($department);

        $crew->setUserGroup(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $crew->setDepartment(Argument::exact($department))->shouldBeCalledTimes(1);

        return $crew;
    }
}
