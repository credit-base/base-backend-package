<?php
namespace Base\Crew\CommandHandler\Crew;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\Crew\Repository\CrewRepository;
use Base\Crew\Command\Crew\SignInCrewCommand;

use Base\Common\Model\IEnableAble;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class SignInCrewCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MockSignInCrewCommandHandler::class)
                                     ->setMethods(['getRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecuteFail()
    {
        $commandHandler = $this->getMockBuilder(MockSignInCrewCommandHandler::class)
                                     ->setMethods([
                                         'searchCrews',
                                         'isCrewExist'
                                        ])
                                     ->getMock();

        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $command = new SignInCrewCommand(
            $crew->getUserName(),
            $crew->getPassword()
        );

        $crews = array($crew);
        $count = 1;

        $commandHandler->expects($this->once())
                   ->method('searchCrews')
                   ->with($command->userName)
                   ->willReturn([$crews, $count]);

        $commandHandler->expects($this->once())
                   ->method('isCrewExist')
                   ->with($count)
                   ->willReturn(false);
        //验证
        $result = $commandHandler->execute($command);
        
        $this->assertFalse($result);
    }

    public function testExecuteSuccess()
    {
        $commandHandler = $this->getMockBuilder(MockSignInCrewCommandHandler::class)
                                     ->setMethods([
                                         'searchCrews',
                                         'isCrewExist',
                                         'isCrewDisabled',
                                         'crewPasswordCorrect'
                                        ])
                                     ->getMock();

        $id = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);

        $command = new SignInCrewCommand(
            $crew->getUserName(),
            $crew->getPassword()
        );

        $crews = array($crew);
        $count = 1;

        $commandHandler->expects($this->once())
                   ->method('searchCrews')
                   ->willReturn([$crews, $count]);

        $commandHandler->expects($this->once())
                   ->method('isCrewExist')
                   ->willReturn(true);

        $commandHandler->expects($this->once())
                   ->method('isCrewDisabled')
                   ->willReturn(true);

        $commandHandler->expects($this->once())
                   ->method('crewPasswordCorrect')
                   ->willReturn(true);

        //验证
        $result = $commandHandler->execute($command);
        
        $this->assertTrue($result);
        $this->assertEquals($id, $command->id);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockSignInCrewCommandHandler();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $commandHandler->getRepository()
        );
    }

    public function testSearchCrews()
    {
        $userName = $this->faker->phoneNumber();
        $list = array(1);
        $count = 1;

        $filter['userName'] = $userName;

        $repository = $this->prophesize(CrewRepository::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->commandHandler->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->commandHandler->searchCrews($userName);
        $this->assertEquals($result, [$list, $count]);
    }

    public function testIsCrewExistFail()
    {
        $count = 2;

        $result = $this->commandHandler->isCrewExist($count);

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testIsCrewExistSuccess()
    {
        $count = SignInCrewCommandHandler::SIGN_IN_SEARCH_COUNT;

        $result = $this->commandHandler->isCrewExist($count);

        $this->assertTrue($result);
    }

    public function testIsCrewDisabledFail()
    {
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $crew->setStatus(IEnableAble::STATUS['DISABLED']);

        $result = $this->commandHandler->isCrewDisabled($crew);

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testIsCrewDisabledSuccess()
    {
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $crew->setStatus(IEnableAble::STATUS['ENABLED']);

        $result = $this->commandHandler->isCrewDisabled($crew);

        $this->assertTrue($result);
    }

    public function testCrewPasswordCorrectFail()
    {
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $passwordCommand = 'Admin123$';
        $salt = 'W8i4';
        $password = md5(md5($passwordCommand).'W8i3');
        $crew->setPassword($password);
        $crew->setSalt($salt);

        $command = new SignInCrewCommand(
            '18800000000',
            $passwordCommand
        );

        $result = $this->commandHandler->crewPasswordCorrect($crew, $command);

        $this->assertFalse($result);
        $this->assertEquals(PASSWORD_INCORRECT, Core::getLastError()->getId());
    }

    public function testCrewPasswordCorrectSuccess()
    {
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $passwordCommand = 'Admin123$';
        $salt = 'W8i3';
        $password = md5(md5($passwordCommand).$salt);
        $crew->setPassword($password);
        $crew->setSalt($salt);

        $command = new SignInCrewCommand(
            '18800000000',
            $passwordCommand
        );

        $result = $this->commandHandler->crewPasswordCorrect($crew, $command);

        $this->assertTrue($result);
    }
}
