<?php
namespace Base\Crew\CommandHandler\Crew;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;

use Base\Crew\Command\Crew\EditCrewCommand;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class EditCrewCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditCrewCommandHandler::class)
                                     ->setMethods(['getRepository', 'fetchUserGroup', 'fetchDepartment'])
                                     ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    //getRepository
    public function testGetRepository()
    {
        $editCrewCommandHandler = new MockEditCrewCommandHandler();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $editCrewCommandHandler->getRepository()
        );
    }

    public function testGetUserGroupRepository()
    {
        $editCrewCommandHandler = new MockEditCrewCommandHandler();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $editCrewCommandHandler->getUserGroupRepository()
        );
    }

    public function testFetchUserGroup()
    {
        $commandHandler = $this->getMockBuilder(MockEditCrewCommandHandler::class)
                           ->setMethods(['getUserGroupRepository'])
                           ->getMock();

        $id = 1;

        // 为 UserGroup 类建立预言
        $userGroup  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($userGroup);

        $commandHandler->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $commandHandler->fetchUserGroup($id);
    }

    public function testFetchDepartment()
    {
        $commandHandler = $this->getMockBuilder(MockEditCrewCommandHandler::class)
                           ->setMethods(['getDepartmentRepository'])
                           ->getMock();

        $id = 1;

        // 为 Department 类建立预言
        $deparment  = $this->prophesize(Department::class);
        $deparmentRepository = $this->prophesize(DepartmentRepository::class);
        $deparmentRepository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($deparment);

        $commandHandler->expects($this->exactly(1))->method(
            'getDepartmentRepository'
        )->willReturn($deparmentRepository->reveal());

        $commandHandler->fetchDepartment($id);
    }

    public function testGetDepartmentRepository()
    {
        $editCrewCommandHandler = new MockEditCrewCommandHandler();
        $this->assertInstanceOf(
            'Base\Department\Repository\DepartmentRepository',
            $editCrewCommandHandler->getDepartmentRepository()
        );
    }
    //execute
    /**
     * 测试不修改 userGroup 和 department 执行命令成功
     * 1. getRepository 被调用一次
     * 2. 测试 crew 赋值 set
     * 3. $crew->edit() 调用一次, 返回 true
     * 4. 返回true
     */
    public function testExecuteNotModifyUserGroupAndDepartmentSuccess()
    {
        //初始化
        $faker = \Faker\Factory::create('zh_CN');
        $command = new EditCrewCommand(
            $faker->randomNumber(),
            $faker->name(),
            '610103190000000000',
            array(),
            0, //userGroupId
            0, //departmentId
            $faker->randomNumber()
        );

        //预言
        $crew = $this->prophesizeCrew($command);
        $crew->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne($command->id)->shouldBeCalledTimes(1)->willReturn($crew->reveal());

        //绑定
        $this->commandHandler->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        //验证
        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteModifyUserGroupAndDepartmentSuccess()
    {
        //初始化
        $faker = \Faker\Factory::create('zh_CN');
        $command = new EditCrewCommand(
            $faker->randomNumber(),
            $faker->name(),
            '610103190000000000',
            array(),
            4, //userGroupId
            2, //departmentId
            3
        );
        //预言
        $crew = $this->prophesizeCrew($command);

        $crew->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne($command->id)->shouldBeCalledTimes(1)->willReturn($crew->reveal());

        //绑定
        $this->commandHandler->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        //验证
        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    /**
     * 测试执行命令失败
     * 1. getRepository 被调用一次
     * 2. 测试 crew 赋值 set
     * 3. $crew->edit() 调用一次, 返回 false
     * 4. 返回false
     */
    public function testExecuteFail()
    {
        //初始化
        $faker = \Faker\Factory::create('zh_CN');
        $command = new EditCrewCommand(
            $faker->randomNumber(),
            $faker->name(),
            '610103190000000000',
            array(),
            0, //userGroupId
            0, //departmentId
            $faker->randomNumber()
        );

        //预言
        $crew = $this->prophesizeCrew($command);
        $crew->edit()->shouldBeCalledTimes(1)->willReturn(false);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne($command->id)->shouldBeCalledTimes(1)->willReturn($crew->reveal());
        //绑定
        $this->commandHandler->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        //验证
        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }

    /**
     * 为 Crew 类建立预言
     */
    protected function prophesizeCrew(ICommand $command)
    {
        $userGroup = new UserGroup($command->userGroupId);
        $department = new Department($command->departmentId);

        $crew = $this->prophesize(Crew::class);
        $crew->setCategory(Argument::exact($command->category))->shouldBeCalledTimes(1);
        $crew->setRealName(Argument::exact($command->realName))->shouldBeCalledTimes(1);
        $crew->setCardId(Argument::exact($command->cardId))->shouldBeCalledTimes(1);
        $crew->setPurview(Argument::exact($command->purview))->shouldBeCalledTimes(1);

        $this->commandHandler->expects($this->exactly(1))
                         ->method('fetchUserGroup')
                         ->with($command->userGroupId)
                         ->willReturn($userGroup);
        $this->commandHandler->expects($this->exactly(1))
                         ->method('fetchDepartment')
                         ->with($command->departmentId)
                         ->willReturn($department);

        $crew->setUserGroup(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $crew->setDepartment(Argument::exact($department))->shouldBeCalledTimes(1);

        return $crew;
    }
}
