<?php
namespace Base\Crew\CommandHandler\Crew;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Crew\Command\Crew\AddCrewCommand;
use Base\Crew\Command\Crew\EditCrewCommand;
use Base\Crew\Command\Crew\SignInCrewCommand;
use Base\Crew\Command\Crew\EnableCrewCommand;
use Base\Crew\Command\Crew\DisableCrewCommand;

class CrewCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new CrewCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddCrewCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddCrewCommand(
                $this->faker->randomNumber(),
                $this->faker->name(),
                $this->faker->phoneNumber(),
                '610103190000000000',
                $this->faker->password(),
                array(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Crew\CommandHandler\Crew\AddCrewCommandHandler',
            $commandHandler
        );
    }

    public function testEditCrewCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditCrewCommand(
                $this->faker->randomNumber(),
                $this->faker->name(),
                '610103190000000000',
                array(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Crew\CommandHandler\Crew\EditCrewCommandHandler',
            $commandHandler
        );
    }

    public function testDisableCrewCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableCrewCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Crew\CommandHandler\Crew\DisableCrewCommandHandler',
            $commandHandler
        );
    }

    public function testEnableCrewCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EnableCrewCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Crew\CommandHandler\Crew\EnableCrewCommandHandler',
            $commandHandler
        );
    }

    public function testSignCrewCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new SignInCrewCommand(
                $this->faker->phoneNumber(),
                $this->faker->password()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Crew\CommandHandler\Crew\SignInCrewCommandHandler',
            $commandHandler
        );
    }
}
