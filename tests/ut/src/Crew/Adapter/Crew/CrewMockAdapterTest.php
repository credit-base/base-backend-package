<?php
namespace Base\Crew\Adapter\Crew;

use PHPUnit\Framework\TestCase;

use Base\Crew\Model\Crew;

class CrewMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new CrewMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new Crew()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new Crew(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Crew\Model\Crew',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Crew\Model\Crew',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
