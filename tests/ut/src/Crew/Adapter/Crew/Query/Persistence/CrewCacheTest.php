<?php
namespace Base\Crew\Adapter\Crew\Query\Persistence;

use PHPUnit\Framework\TestCase;

class CrewCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockCrewCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(CrewCache::KEY, $this->cache->getKey());
    }
}
