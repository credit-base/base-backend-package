<?php
namespace Base\Crew\Adapter\Crew\Query\Persistence;

use PHPUnit\Framework\TestCase;

class CrewDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockCrewDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(CrewDb::TABLE, $this->database->getTable());
    }
}
