<?php
namespace Base\Crew\Adapter\Crew;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Model\Crew;
use Base\Crew\Translator\CrewDbTranslator;
use Base\Crew\Adapter\Crew\Query\CrewRowCacheQuery;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

use Base\Department\Model\Department;
use Base\Department\Adapter\Department\IDepartmentAdapter;

use Base\Common\Model\IEnableAble;

/**
 * @SuppressWarnings(PHPMD)
 */
class CrewDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(CrewDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction',
                                    'fetchUserGroup',
                                    'fetchDepartment'
                               ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 ICrewAdapter
     */
    public function testImplementsICrewAdapter()
    {
        $this->assertInstanceOf(
            'Base\Crew\Adapter\Crew\ICrewAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 CrewDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockCrewDbAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Translator\CrewDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 CrewRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockCrewDbAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Adapter\Crew\Query\CrewRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    /**
     * 测试是否初始化 userGroupRepository
     */
    public function testGetUserGroupRepository()
    {
        $adapter = new MockCrewDbAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter',
            $adapter->getUserGroupRepository()
        );
    }

    /**
     * 测试是否初始化 departmentRepository
     */
    public function testGetDepartmentRepository()
    {
        $adapter = new MockCrewDbAdapter();
        $this->assertInstanceOf(
            'Base\Department\Adapter\Department\IDepartmentAdapter',
            $adapter->getDepartmentRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockCrewDbAdapter();
        $this->assertInstanceOf('Marmot\Interfaces\INull', $adapter->getNullObject());
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedCrew = new Crew();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($expectedCrew)->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedCrew);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedCrew = new Crew();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))->method('editAction')->with($expectedCrew, $keys)->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedCrew, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedCrew = new Crew();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))->method('fetchOneAction')->with($id)->willReturn($expectedCrew);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedCrew);
        $this->adapter->expects($this->exactly(1))->method('fetchDepartment')->with($expectedCrew);
        //验证
        $crew = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedCrew, $crew);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $crewOne = new Crew(1);
        $crewTwo = new Crew(2);

        $ids = [1, 2];

        $expectedCrewList = [];
        $expectedCrewList[$crewOne->getId()] = $crewOne;
        $expectedCrewList[$crewTwo->getId()] = $crewTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'fetchListAction'
        )->with($ids)->willReturn($expectedCrewList);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedCrewList);
        $this->adapter->expects($this->exactly(1))->method('fetchDepartment')->with($expectedCrewList);

        //验证
        $crewList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedCrewList, $crewList);
    }

    //fetchUserGroup
    public function testFetchUserGroupIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockCrewDbAdapter::class)
                           ->setMethods(['fetchUserGroupByObject'])
                           ->getMock();
        
        $crew = new Crew();

        $adapter->expects($this->exactly(1))->method('fetchUserGroupByObject')->with($crew);

        $adapter->fetchUserGroup($crew);
    }

    public function testFetchUserGroupIsArray()
    {
        $adapter = $this->getMockBuilder(MockCrewDbAdapter::class)
                           ->setMethods(['fetchUserGroupByList'])
                           ->getMock();
        
        $crewOne = new Crew(1);
        $crewTwo = new Crew(2);
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;

        $adapter->expects($this->exactly(1))->method('fetchUserGroupByList')->with($crewList);

        $adapter->fetchUserGroup($crewList);
    }

    //fetchUserGroupByObject
    public function testFetchUserGroupByObject()
    {
        $adapter = $this->getMockBuilder(MockCrewDbAdapter::class)
                           ->setMethods(['getUserGroupRepository'])
                           ->getMock();

        $crew = new Crew();
        $crew->setUserGroup(new UserGroup(1));
        
        $userGroupId = 1;

        // 为 UserGroup 类建立预言
        $userGroup  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(IUserGroupAdapter::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))->shouldBeCalledTimes(1)->willReturn($userGroup);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByObject($crew);
    }

    //fetchUserGroupByList
    public function testFetchUserGroupByList()
    {
        $adapter = $this->getMockBuilder(MockCrewDbAdapter::class)
                           ->setMethods(['getUserGroupRepository'])
                           ->getMock();

        $crewOne = new Crew(1);
        $userGroupOne = new UserGroup(1);
        $crewOne->setUserGroup($userGroupOne);

        $crewTwo = new Crew(2);
        $userGroupTwo = new UserGroup(2);
        $crewTwo->setUserGroup($userGroupTwo);
        $userGroupIds = [1, 2];
        
        $userGroupList[$userGroupOne->getId()] = $userGroupOne;
        $userGroupList[$userGroupTwo->getId()] = $userGroupTwo;
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;

        $userGroupRepository = $this->prophesize(IUserGroupAdapter::class);
        $userGroupRepository->fetchList(
            Argument::exact($userGroupIds)
        )->shouldBeCalledTimes(1)->willReturn($userGroupList);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByList($crewList);
    }

    //fetchDepartment
    public function testFetchDepartmentIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockCrewDbAdapter::class)
                           ->setMethods(['fetchDepartmentByObject'])
                           ->getMock();
        
        $crew = new Crew();

        $adapter->expects($this->exactly(1))->method('fetchDepartmentByObject')->with($crew);

        $adapter->fetchDepartment($crew);
    }

    public function testFetchDepartmentIsArray()
    {
        $adapter = $this->getMockBuilder(MockCrewDbAdapter::class)
                           ->setMethods(['fetchDepartmentByList'])
                           ->getMock();
        
        $crewOne = new Crew(1);
        $crewTwo = new Crew(2);
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;

        $adapter->expects($this->exactly(1))->method('fetchDepartmentByList')->with($crewList);

        $adapter->fetchDepartment($crewList);
    }

    //fetchDepartmentByObject
    public function testFetchDepartmentByObject()
    {
        $adapter = $this->getMockBuilder(MockCrewDbAdapter::class)
                           ->setMethods(['getDepartmentRepository'])
                           ->getMock();

        $crew = new Crew();
        $crew->setDepartment(new Department(1));
        
        $departmentId = 1;

        // 为 Department 类建立预言
        $department  = $this->prophesize(Department::class);
        $departmentRepository = $this->prophesize(IDepartmentAdapter::class);
        $departmentRepository->fetchOne(
            Argument::exact($departmentId)
        )->shouldBeCalledTimes(1)->willReturn($department);

        $adapter->expects($this->exactly(1))->method(
            'getDepartmentRepository'
        )->willReturn($departmentRepository->reveal());

        $adapter->fetchDepartmentByObject($crew);
    }

    //fetchDepartmentByList
    public function testFetchDepartmentByList()
    {
        $adapter = $this->getMockBuilder(MockCrewDbAdapter::class)
                           ->setMethods(['getDepartmentRepository'])
                           ->getMock();

        $crewOne = new Crew(1);
        $departmentOne = new Department(1);
        $crewOne->setDepartment($departmentOne);

        $crewTwo = new Crew(2);
        $departmentTwo = new Department(2);
        $crewTwo->setDepartment($departmentTwo);
        $departmentIds = [1, 2];
        
        $departmentList[$departmentOne->getId()] = $departmentOne;
        $departmentList[$departmentTwo->getId()] = $departmentTwo;

        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;

        $departmentRepository = $this->prophesize(IDepartmentAdapter::class);
        $departmentRepository->fetchList(
            Argument::exact($departmentIds)
        )->shouldBeCalledTimes(1)->willReturn($departmentList);

        $adapter->expects($this->exactly(1))->method(
            'getDepartmentRepository'
        )->willReturn($departmentRepository->reveal());

        $adapter->fetchDepartmentByList($crewList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterUserGroup()
    {
        $filter = array(
            'userGroup' => 1
        );
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = 'user_group_id = '.$filter['userGroup'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterDepartment()
    {
        $filter = array(
            'department' => 1
        );
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = 'department_id = '.$filter['department'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterCellphone()
    {
        $filter = array(
            'cellphone' => '18840287788',
        );
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = 'cellphone = \''.$filter['cellphone'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterUserName()
    {
        $filter = array(
            'userName' => 'userName',
        );
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = 'user_name = \''.$filter['userName'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterCategory()
    {
        $filter = array(
            'category' => 1
        );
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = 'category IN ('.$filter['category'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/Status
    public function testFormatFilterStatus()
    {
        $filter = array(
            'status' => IEnableAble::STATUS['ENABLED']
        );
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = 'status = '.$filter['status'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/realName
    public function testSearchDataFormatFilterName()
    {
        $filter = array(
            'realName' => 'realName'
        );

        $adapter = new MockCrewDbAdapter();

        $expectedCondition = 'real_name LIKE \'%'.$filter['realName'].'%\'';
        
        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortStatusDesc()
    {
        $sort = array('status' => -1);
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = ' ORDER BY status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortStatusAsc()
    {
        $sort = array('status' => 1);
        $adapter = new MockCrewDbAdapter();

        $expectedCondition = ' ORDER BY status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
