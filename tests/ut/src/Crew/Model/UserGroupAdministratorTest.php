<?php
namespace Base\Crew\Model;

use PHPUnit\Framework\TestCase;

class UserGroupAdministratorTest extends TestCase
{
    private $userGroupAdministrator;

    public function setUp()
    {
        $this->userGroupAdministrator = new UserGroupAdministrator();
    }

    public function tearDown()
    {
        unset($this->userGroupAdministrator);
    }

    public function testExtendsCrew()
    {
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->userGroupAdministrator
        );
    }
}
