<?php
namespace Base\Crew\Model;

use PHPUnit\Framework\TestCase;

class OperatorTest extends TestCase
{
    private $operator;

    public function setUp()
    {
        $this->operator = new Operator();
    }

    public function tearDown()
    {
        unset($this->operator);
    }

    public function testExtendsCrew()
    {
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->operator
        );
    }
}
