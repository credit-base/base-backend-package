<?php
namespace Base\Crew\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Base\UserGroup\Model\UserGroup;
use Base\Department\Model\Department;
use Base\Crew\Adapter\Crew\ICrewAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class CrewTest extends TestCase
{
    private $crew;

    public function setUp()
    {
        $this->crew = new MockCrew();
    }

    public function tearDown()
    {
        unset($this->crew);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        //测试初始化姓名
        $this->assertEmpty($this->crew->getRealName());

        //测试初始化用户组
        $this->assertInstanceOf('Base\UserGroup\Model\UserGroup', $this->crew->getUserGroup());

        //测试初始化部门
        $this->assertInstanceOf('Base\Department\Model\Department', $this->crew->getDepartment());

        //测试初始化类型
        $this->assertEquals(Crew::CATEGORY['OPERATOR'], $this->crew->getCategory());

        //测试初始化权限
        $this->assertEquals(array(), $this->crew->getPurview());

        //测试初始化状态
        $this->assertEquals(Crew::STATUS_NORMAL, $this->crew->getStatus());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Crew setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->crew->setId(1);
        $this->assertEquals(1, $this->crew->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //realName 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setRealName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->crew->setRealName('string');
        $this->assertEquals('string', $this->crew->getRealName());
    }

    /**
     * 设置 Crew setRealName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->crew->setRealName(array(1,2,3));
    }
    //realName 测试 --------------------------------------------------------   end

    //UserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->crew->setUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->crew->getUserGroup());
    }

    /**
     * 设置 Crew setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->crew->setUserGroup('userGroup');
    }
    //UserGroup 测试 --------------------------------------------------------   end

    //Department 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setDepartment() 正确的传参类型,期望传值正确
     */
    public function testSetDepartmentCorrectType()
    {
        $expectedDepartment = new Department();

        $this->crew->setDepartment($expectedDepartment);
        $this->assertEquals($expectedDepartment, $this->crew->getDepartment());
    }

    /**
     * 设置 Crew setDepartment() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDepartmentWrongType()
    {
        $this->crew->setDepartment('department');
    }
    //Department 测试 --------------------------------------------------------   end

    //category 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Crew setCategory() 是否符合预定范围
     *
     * @dataProvider categoryProvider
     */
    public function testSetCategory($actual, $expected)
    {
        $this->crew->setCategory($actual);
        $this->assertEquals($expected, $this->crew->getCategory());
    }

    /**
     * 循环测试 Crew setCategory() 数据构建器
     */
    public function categoryProvider()
    {
        return array(
            array(Crew::CATEGORY['SUPER_ADMINISTRATOR'], Crew::CATEGORY['SUPER_ADMINISTRATOR']),
            array(Crew::CATEGORY['PLATFORM_ADMINISTRATOR'], Crew::CATEGORY['PLATFORM_ADMINISTRATOR']),
            array(Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'], Crew::CATEGORY['USER_GROUP_ADMINISTRATOR']),
            array(Crew::CATEGORY['OPERATOR'], Crew::CATEGORY['OPERATOR']),
            array(999, Crew::CATEGORY['OPERATOR']),
        );
    }

    /**
     * 设置 Crew setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->crew->setCategory('string');
    }
    //category 测试 ------------------------------------------------------   end

    //cartId 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setCardId() 正确的传参类型,期望传值正确
     */
    public function testSetCardIdCorrectType()
    {
        $this->crew->setCardId('string');
        $this->assertEquals('string', $this->crew->getCardId());
    }

    /**
     * 设置 Crew setCardId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCardIdWrongType()
    {
        $this->crew->setCardId(array(1,2,3));
    }
    //cartId 测试 --------------------------------------------------------   end

    //purview 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setPurview() 正确的传参类型,期望传值正确
     */
    public function testSetPurviewCorrectType()
    {
        $this->crew->setPurview(['purview']);
        $this->assertEquals(['purview'], $this->crew->getPurview());
    }

    /**
     * 设置 Crew setPurview() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPurviewWrongType()
    {
        $this->crew->setPurview('purview');
    }
    //purview 测试 --------------------------------------------------------   end

    //getRepository
    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Crew\Adapter\Crew\ICrewAdapter',
            $this->crew->getRepository()
        );
    }

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 ICrewAdapter 调用 add 并且传参 $crew 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $crew = $this->getMockBuilder(Crew::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $userGroup = new UserGroup(1);
        $department = new Department(1);
        $department->setUserGroup($userGroup);
        $crew->setUserGroup($userGroup);
        $crew->setDepartment($department);

        //预言 ICrewAdapter
        $repository = $this->prophesize(ICrewAdapter::class);
        $repository->add(Argument::exact($crew))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $crew->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $crew->add();
        $this->assertTrue($result);
    }

    public function testAddFailNotBelong()
    {
        //初始化
        $crew = $this->getMockBuilder(Crew::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
        
        $userGroup = new UserGroup(1);
        $department = new Department(1);
        $department->setUserGroup(new UserGroup(2));
        $crew->setUserGroup($userGroup);
        $crew->setDepartment($department);

        //验证
        $result = $crew->add();
        $this->assertFalse($result);
        $this->assertEquals(DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP, Core::getLastError()->getId());
    }

    /**
     * 测试添加失败
     * 1. 期望返回false
     * 2. 期望 getRepository 被调用 1 次
     * 3. 期望 ICrewAdapter 调用 add 并且传参 $crew 对象, 返回 false(即添加失败)
     */
    public function testAddFail()
    {
        //初始化
        $crew = $this->getMockBuilder(Crew::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 ICrewAdapter
        $repository = $this->prophesize(ICrewAdapter::class);
        $repository->add(Argument::exact($crew))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        $userGroup = new UserGroup(1);
        $department = new Department(1);
        $department->setUserGroup($userGroup);
        $crew->setUserGroup($userGroup);
        $crew->setDepartment($department);
        //绑定
        $crew->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $crew->add();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
        $this->assertEquals(
            array('pointer' => 'cellphone'),
            Core::getLastError()->getSource()
        );
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 updateTime
     * 2. 期望调用 getRepository 被调用 1 次
     * 3. 期望 ICrewAdapter 调用 edit 并且传参 $crew 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $crew = $this->getMockBuilder(Crew::class)
                           ->setMethods(['getRepository', 'setUpdateTime'])
                           ->getMock();

        $userGroup = new UserGroup(1);
        $department = new Department(1);
        $department->setUserGroup($userGroup);
        $crew->setUserGroup($userGroup);
        $crew->setDepartment($department);
        //预言修改updateTime
        $crew->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(
                 Core::$container->get('time')
             );

        //预言 ICrewAdapter
        $repository = $this->prophesize(ICrewAdapter::class);
        $repository->edit(
            Argument::exact($crew),
            Argument::exact(
                [
                        'realName',
                        'cardId',
                        'userGroup',
                        'department',
                        'purview',
                        'category',
                        'updateTime'
                        ]
            )
        )->shouldBeCalledTimes(1)
                   ->willReturn(true);
        //绑定
        $crew->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $crew->edit();
        $this->assertTrue($result);
    }

    public function testEditFailNotBelong()
    {
        //初始化
        $crew = $this->getMockBuilder(Crew::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
        
        $userGroup = new UserGroup(1);
        $department = new Department(1);
        $department->setUserGroup(new UserGroup(2));
        $crew->setUserGroup($userGroup);
        $crew->setDepartment($department);

        //验证
        $result = $crew->edit();
        $this->assertFalse($result);
        $this->assertEquals(DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP, Core::getLastError()->getId());
    }

    public function testEditFail()
    {
        //初始化
        $crew = $this->getMockBuilder(Crew::class)
                           ->setMethods(['getRepository', 'setUpdateTime'])
                           ->getMock();

        //预言修改updateTime
        $crew->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(
                 Core::$container->get('time')
             );

        $userGroup = new UserGroup(0);
        $department = new Department(0);
        $department->setUserGroup($userGroup);
        $crew->setUserGroup($userGroup);
        $crew->setDepartment($department);
        $crew->setCategory(Crew::CATEGORY['PLATFORM_ADMINISTRATOR']);
        //预言 ICrewAdapter
        $repository = $this->prophesize(ICrewAdapter::class);
        $repository->edit(
            Argument::exact($crew),
            Argument::exact([
                        'realName',
                        'cardId',
                        'userGroup',
                        'department',
                        'purview',
                        'category',
                        'updateTime'])
        )->shouldBeCalledTimes(1)
                   ->willReturn(false);
        //绑定
        $crew->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $crew->edit();
        $this->assertFalse($result);
        $this->assertEquals(ERROR_NOT_DEFINED, Core::getLastError()->getId());
    }

    public function testUpdatePassword()
    {
        $newPassword = 'newPassword';
        
        $result = $this->crew->updatePassword($newPassword);
        $this->assertFalse($result);
    }

    public function testVerifyPassword()
    {
        $oldPassword = 'oldPassword';
        
        $result = $this->crew->verifyPassword($oldPassword);
        $this->assertFalse($result);
    }
}
