<?php
namespace Base\Crew\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Base\Crew\Adapter\Crew\ICrewAdapter;

use Base\UserGroup\Model\UserGroup;
use Base\Department\Model\Department;

class PlatformAdministratorTest extends TestCase
{
    private $platformAdministrator;

    public function setUp()
    {
        $this->platformAdministrator = new PlatformAdministrator();
    }

    public function tearDown()
    {
        unset($this->platformAdministrator);
    }

    public function testExtendsCrew()
    {
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->platformAdministrator
        );
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 updateTime
     * 2. 期望调用 getRepository 被调用 1 次
     * 3. 期望 ICrewAdapter 调用 edit 并且传参 $platformAdministrator 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $crew = $this->getMockBuilder(PlatformAdministrator::class)
                           ->setMethods(['getRepository', 'setUpdateTime', 'setUserGroup', 'setDepartment'])
                           ->getMock();

        //预言修改updateTime
        $crew->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(
                 Core::$container->get('time')
             );
        $crew->expects($this->exactly(1))
             ->method('setDepartment')
             ->with(
                 new Department()
             );

        $crew->expects($this->exactly(1))
             ->method('setUserGroup')
             ->with(
                 new UserGroup()
             );
        //预言 ICrewAdapter
        $repository = $this->prophesize(ICrewAdapter::class);
        $repository->edit(
            Argument::exact($crew),
            Argument::exact(
                [
                        'realName',
                        'cardId',
                        'updateTime',
                        'purview',
                        'userGroup',
                        'department',
                        ]
            )
        )->shouldBeCalledTimes(1)
                   ->willReturn(true);
        //绑定
        $crew->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $crew->edit();
        $this->assertTrue($result);
    }

    public function testEditFail()
    {
        //初始化
        $platformAdministrator = $this->getMockBuilder(PlatformAdministrator::class)
                           ->setMethods(['getRepository', 'setUpdateTime', 'setDepartment', 'setUserGroup'])
                           ->getMock();

        //预言修改updateTime
        $platformAdministrator->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(
                 Core::$container->get('time')
             );

        $platformAdministrator->expects($this->exactly(1))
             ->method('setDepartment')
             ->with(
                 new Department()
             );

        $platformAdministrator->expects($this->exactly(1))
             ->method('setUserGroup')
             ->with(
                 new UserGroup()
             );
        //预言 ICrewAdapter
        $repository = $this->prophesize(ICrewAdapter::class);
        $repository->edit(
            Argument::exact($platformAdministrator),
            Argument::exact(
                [
                        'realName',
                        'cardId',
                        'updateTime',
                        'purview',
                        'userGroup',
                        'department',
                        ]
            )
        )->shouldBeCalledTimes(1)
                   ->willReturn(false);
        //绑定
        $platformAdministrator->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $platformAdministrator->edit();
        $this->assertFalse($result);
        $this->assertEquals(ERROR_NOT_DEFINED, Core::getLastError()->getId());
    }
}
