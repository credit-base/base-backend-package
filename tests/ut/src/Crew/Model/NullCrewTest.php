<?php
namespace Base\Crew\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullCrewTest extends TestCase
{
    private $crew;

    public function setUp()
    {
        $this->crew = $this->getMockBuilder(NullCrew::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->crew);
    }

    public function testExtendsCrew()
    {
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->crew
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->crew
        );
    }
}
