<?php
namespace Base\Crew\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class SuperAdministratorTest extends TestCase
{
    private $superAdministrator;

    public function setUp()
    {
        $this->superAdministrator = new SuperAdministrator();
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->superAdministrator);
    }

    public function testExtendsCrew()
    {
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->superAdministrator
        );
    }

    public function testAdd()
    {
        $result = $this->superAdministrator->add();
        $this->assertFalse($result);
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
    }

    public function testEdit()
    {
        $result = $this->superAdministrator->edit();
        $this->assertFalse($result);
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
    }
}
