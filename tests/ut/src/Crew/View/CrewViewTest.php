<?php
namespace Base\Crew\View;

use PHPUnit\Framework\TestCase;

use Base\Crew\Model\Crew;

class CrewViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $crew = new CrewView(new Crew());
        $this->assertInstanceof('Base\Common\View\CommonView', $crew);
    }
}
