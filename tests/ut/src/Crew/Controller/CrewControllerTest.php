<?php
namespace Base\Crew\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\User\WidgetRule\UserWidgetRule;

use Base\Crew\Model\Crew;
use Base\Crew\WidgetRule\CrewWidgetRule;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Command\Crew\SignInCrewCommand;

class CrewControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new CrewController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }
    public function testSignIn()
    {
        // 为 CrewController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(CrewController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getUserWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initSignIn(), 期望结果为 true
        $this->initSignIn($controller, true);

        // 为 Crew 类建立预言
        $department = $this->prophesize(Crew::class);

        // 为 CrewRepository 类建立预言, CrewRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Crew, getRepository 方法被调用一次
        $id = 0;
        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($department);
        $controller->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->signIn 方法被调用一次, 且返回结果为 true
        $result = $controller->signIn();
        $this->assertTrue($result);
    }

    public function testSignInFail()
    {
        // 为 CrewController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(CrewController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getUserWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initSignIn(), 期望结果为 false
        $this->initSignIn($controller, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->signIn 方法被调用一次, 且返回结果为 false
        $result = $controller->signIn();
        $this->assertFalse($result);
    }

    protected function initSignIn(CrewController $controller, bool $result)
    {
        // mock 请求参数
        $data = array(
            "attributes" => array(
                "userName" => '18800000000',
                "password" => 'Admin123$'
            )
        );
        $userName = '18800000000';
        $password = 'Admin123$';

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        // 为 UserWidgetRule 类建立预言, 验证请求参数,  getUserWidgetRule 方法被调用2次
        $userWidgetRule = $this->prophesize(UserWidgetRule::class);
        $userWidgetRule->cellphone(Argument::exact($userName))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $userWidgetRule->password(Argument::exact($password), Argument::exact('password'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(2))
            ->method('getUserWidgetRule')
            ->willReturn($userWidgetRule->reveal());

        // 为 CommandBus 类建立预言, 传入 SignInCrewCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new SignInCrewCommand(
                    $userName,
                    $password
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }
}
