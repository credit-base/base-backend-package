<?php
namespace Base\Crew\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Crew\Adapter\Crew\ICrewAdapter;
use Base\Crew\Model\NullCrew;
use Base\Crew\Model\Crew;
use Base\Crew\View\CrewView;

class CrewFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(CrewFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockCrewFetchController();

        $this->assertInstanceOf(
            'Base\Crew\Adapter\Crew\ICrewAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockCrewFetchController();

        $this->assertInstanceOf(
            'Base\Crew\View\CrewView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockCrewFetchController();

        $this->assertEquals(
            'crews',
            $controller->getResourceName()
        );
    }
}
