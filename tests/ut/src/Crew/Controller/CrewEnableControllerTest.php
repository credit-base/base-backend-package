<?php
namespace Base\Crew\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\CommandBus;

use Base\Crew\Utils\MockFactory;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Command\Crew\EnableCrewCommand;
use Base\Crew\Command\Crew\DisableCrewCommand;

class CrewEnableControllerTest extends TestCase
{
    private $crewStub;

    public function setUp()
    {
        $this->crewStub = $this->getMockBuilder(MockCrewEnableController::class)
                        ->setMethods([
                            'displayError',
                            ])
                        ->getMock();
    }

    public function teatDown()
    {
        unset($this->crewStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $this->crewStub->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->crewStub->getCommandBus()
        );
    }

    private function initialDisable($result)
    {
        $this->crewStub = $this->getMockBuilder(MockCrewEnableController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new DisableCrewCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->crewStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDisableFailure()
    {
        $command = $this->initialDisable(false);

        $this->crewStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->crewStub->disable($command->id);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $command = $this->initialDisable(true);

        $crew = MockFactory::generateCrew($command->id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);
        $this->crewStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->crewStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->crewStub->disable($command->id);
        $this->assertTrue($result);
    }

    private function initialEnable($result)
    {
        $this->crewStub = $this->getMockBuilder(MockCrewEnableController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new EnableCrewCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->crewStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testEnableFailure()
    {
        $command = $this->initialEnable(false);

        $this->crewStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->crewStub->enable($command->id);
        $this->assertFalse($result);
    }

    public function testEnableSuccess()
    {
        $command = $this->initialEnable(true);

        $crew = MockFactory::generateCrew($command->id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $this->crewStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->crewStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->crewStub->enable($command->id);
        $this->assertTrue($result);
    }
}
