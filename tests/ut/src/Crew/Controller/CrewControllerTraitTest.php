<?php
namespace Base\Crew\Controller;

use PHPUnit\Framework\TestCase;

class CrewControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockCrewControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetCrewWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Crew\WidgetRule\CrewWidgetRule',
            $this->trait->getCrewWidgetRulePublic()
        );
    }

    public function testGetUserWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\User\WidgetRule\UserWidgetRule',
            $this->trait->getUserWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}
