<?php
namespace Base\Crew\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;
use Base\User\WidgetRule\UserWidgetRule;

use Base\Crew\Model\Crew;
use Base\Crew\WidgetRule\CrewWidgetRule;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Command\Crew\AddCrewCommand;
use Base\Crew\Command\Crew\EditCrewCommand;

class CrewOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new CrewOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    /**
     * 测试 add 成功
     * 1. 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getUserWidgetRule、getCrewWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initAddPlatformAdmin(), 期望结果为 true
     * 3. 为 Crew 类建立预言
     * 4. 为 CrewRepository 类建立预言, CrewRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Crew, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getUserWidgetRule、getCrewWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(CrewOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getUserWidgetRule',
                    'getCrewWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initAddPlatformAdmin(), 期望结果为 true
        $this->initAddPlatformAdmin($controller, true);

        // 为 Crew 类建立预言
        $department = $this->prophesize(Crew::class);

        // 为 CrewRepository 类建立预言, CrewRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Crew, getRepository 方法被调用一次
        $id = 0;
        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($department);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->add 方法被调用一次, 且返回结果为 true
        $result = $controller->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getUserWidgetRule、getCrewWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAddNotPlatformAdmin(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getUserWidgetRule、getCrewWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(CrewOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getUserWidgetRule',
                    'getCrewWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAddNotPlatformAdmin(), 期望结果为 false
        $this->initAddNotPlatformAdmin($controller, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->add 方法被调用一次, 且返回结果为 false
        $result = $controller->add();
        $this->assertFalse($result);
    }

    public function testAddValidateFail()
    {
        // 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getUserWidgetRule、getCrewWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(CrewOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getUserWidgetRule',
                    'getCrewWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAddNotInCategory()
        $this->initAddNotInCategory($controller);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->add 方法被调用一次, 且返回结果为 false
        $result = $controller->add();
        $this->assertFalse($result);
    }

    protected function initAddNotInCategory(CrewOperateController $controller)
    {
        // mock 请求参数
        $category = 10;
        $data = $this->mockAddRequestData($category);

        $this->initAddRequest($data, $controller);
    }

    protected function initAddPlatformAdmin(CrewOperateController $controller, bool $result)
    {
        // mock 请求参数
        $category = Crew::CATEGORY['PLATFORM_ADMINISTRATOR'];
        $data = $this->mockAddRequestData($category);
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $this->initAddRequest($data, $controller);
        $this->initAddUserWidgetRule($attributes, $controller);
        $this->initCrewWidgetRule($attributes, $controller);
        $this->initAddCrewCommand($attributes, $relationships, $controller, $result);
    }

    protected function initAddNotPlatformAdmin(CrewOperateController $controller, bool $result)
    {
        // mock 请求参数
        $category = Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'];
        $data = $this->mockAddRequestData($category);
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $this->initAddRequest($data, $controller);
        $this->initAddUserWidgetRule($attributes, $controller);
        $this->initCrewWidgetRule($attributes, $controller);
        $this->initCommonWidgetRule($relationships, $controller);
        $this->initAddCrewCommand($attributes, $relationships, $controller, $result);
    }

    private function mockAddRequestData(int $category) : array
    {
        return array(
            "attributes" => array(
                "realName" => '张科',
                "cardId" => '610424198810272544',
                "cellphone" => '18800000000',
                "password" => 'Admin123$',
                "category" => $category,
                "purview" => array(1,2,3),
            ),
            "relationships" => array(
                "userGroup" => array("data" => array(array("type" => "userGroups","id" => 1))),
                "department" => array("data" => array(array("type" => "departments","id" => 1)))
            )
        );
    }

    private function initAddRequest(array $data, CrewOperateController $controller)
    {
        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用1次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());
    }

    private function initAddUserWidgetRule(array $attributes, CrewOperateController $controller)
    {
        $realName = $attributes['realName'];
        $cellphone = $attributes['cellphone'];
        $cardId = $attributes['cardId'];
        $password = $attributes['password'];

        // 为 UserWidgetRule 类建立预言, 验证请求参数,  getUserWidgetRule 方法被调用4次
        $userWidgetRule = $this->prophesize(UserWidgetRule::class);
        $userWidgetRule->realName(Argument::exact($realName))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $userWidgetRule->cellphone(Argument::exact($cellphone))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $userWidgetRule->cardId(Argument::exact($cardId))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $userWidgetRule->password(Argument::exact($password), Argument::exact('password'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(4))
            ->method('getUserWidgetRule')
            ->willReturn($userWidgetRule->reveal());
    }

    private function initAddCrewCommand(
        array $attributes,
        array $relationships,
        CrewOperateController $controller,
        bool $result
    ) {
        // 为 CommandBus 类建立预言, 传入 AddCrewCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AddCrewCommand(
                    $attributes['category'],
                    $attributes['realName'],
                    $attributes['cellphone'],
                    $attributes['cardId'],
                    $attributes['password'],
                    $attributes['purview'],
                    $relationships['userGroup']['data'][0]['id'],
                    $relationships['department']['data'][0]['id']
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    /**
     * 测试 edit 成功
     * 1. 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
     *    getCrewWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initEditPlatformAdmin(), 期望结果为 true
     * 3. 为 Crew 类建立预言
     * 4. 为 CrewRepository 类建立预言, CrewRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Crew, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
        // getCrewWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(CrewOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getUserWidgetRule',
                    'getCrewWidgetRule',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEditPlatformAdmin(), 期望结果为 true
        $this->initEditPlatformAdmin($controller, $id, true);

        // 为 Crew 类建立预言
        $department = $this->prophesize(Crew::class);

        // 为 CrewRepository 类建立预言, CrewRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Crew, getRepository 方法被调用一次
        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($department);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->edit 方法被调用一次, 且返回结果为 true
        $result = $controller->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
     *    getCrewWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initEditNotPlatformAdmin(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
        // getCrewWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(CrewOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getUserWidgetRule',
                    'getCrewWidgetRule',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEditNotPlatformAdmin(), 期望结果为 false
        $this->initEditNotPlatformAdmin($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->edit 方法被调用一次, 且返回结果为 false
        $result = $controller->edit($id);
        $this->assertFalse($result);
    }

    public function testEditValidateFail()
    {
        // 为 CrewOperateController 类建立桩件, 并模仿 getRequest、getUserWidgetRule、
        // getCrewWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(CrewOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getUserWidgetRule',
                    'getCrewWidgetRule',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEditNotInCategory()
        $this->initEditNotInCategory($controller);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->edit 方法被调用一次, 且返回结果为 false
        $result = $controller->edit($id);
        $this->assertFalse($result);
    }

    protected function initEditNotInCategory(CrewOperateController $controller)
    {
        // mock 请求参数
        $category = 10;
        $data = $this->mockEditRequestData($category);

        $this->initEditRequest($data, $controller);
    }

    protected function initEditPlatformAdmin(CrewOperateController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $category = Crew::CATEGORY['PLATFORM_ADMINISTRATOR'];
        $data = $this->mockEditRequestData($category);
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $this->initEditRequest($data, $controller);
        $this->initEditUserWidgetRule($attributes, $controller);
        $this->initCrewWidgetRule($attributes, $controller);
        $this->initEditCrewCommand($id, $attributes, $relationships, $controller, $result);
    }

    protected function initEditNotPlatformAdmin(CrewOperateController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $category = Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'];
        $data = $this->mockEditRequestData($category);
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $this->initEditRequest($data, $controller);
        $this->initEditUserWidgetRule($attributes, $controller);
        $this->initCrewWidgetRule($attributes, $controller);
        $this->initCommonWidgetRule($relationships, $controller);
        $this->initEditCrewCommand($id, $attributes, $relationships, $controller, $result);
    }

    private function mockEditRequestData(int $category) : array
    {
        return array(
            "attributes" => array(
                "realName" => '张科',
                "cardId" => '610424198810272544',
                "category" => $category,
                "purview" => array(1,2,3),
            ),
            "relationships" => array(
                "userGroup" => array(
                    "data" => array(
                        array("type" => "userGroups","id" => 1)
                    )
                ),
                "department" => array(
                    "data" => array(
                        array("type" => "departments","id" => 1)
                    )
                )
            )
        );
    }

    private function initEditRequest(array $data, CrewOperateController $controller)
    {
        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());
    }

    private function initEditUserWidgetRule(array $attributes, CrewOperateController $controller)
    {
        $realName = $attributes['realName'];
        $cardId = $attributes['cardId'];

        // 为 UserWidgetRule 类建立预言, 验证请求参数,  getUserWidgetRule 方法被调用2次
        $userWidgetRule = $this->prophesize(UserWidgetRule::class);
        $userWidgetRule->realName(Argument::exact($realName))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $userWidgetRule->cardId(Argument::exact($cardId))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(2))
            ->method('getUserWidgetRule')
            ->willReturn($userWidgetRule->reveal());
    }

    private function initCrewWidgetRule(array $attributes, CrewOperateController $controller)
    {
        $category = $attributes['category'];
        $purview = $attributes['purview'];

        // 为 CrewWidgetRule 类建立预言, 验证请求参数, getCrewWidgetRule 方法被调用1次
        $crewWidgetRule = $this->prophesize(CrewWidgetRule::class);
        $crewWidgetRule->category(Argument::exact($category))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $crewWidgetRule->purview(Argument::exact($purview))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(2))
            ->method('getCrewWidgetRule')
            ->willReturn($crewWidgetRule->reveal());
    }

    private function initCommonWidgetRule(array $relationships, CrewOperateController $controller)
    {
        $userGroupId = $relationships['userGroup']['data'][0]['id'];
        $departmentId = $relationships['department']['data'][0]['id'];

        // 为 CommonWidgetRule 类建立预言, 验证请求参数, getCommonWidgetRule 方法被调用3次
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($userGroupId), Argument::exact('userGroupId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($departmentId), Argument::exact('departmentId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(2))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());
    }

    private function initEditCrewCommand(
        int $id,
        array $attributes,
        array $relationships,
        CrewOperateController $controller,
        bool $result
    ) {
        // 为 CommandBus 类建立预言, 传入 EditCrewCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditCrewCommand(
                    $attributes['category'],
                    $attributes['realName'],
                    $attributes['cardId'],
                    $attributes['purview'],
                    $relationships['userGroup']['data'][0]['id'],
                    $relationships['department']['data'][0]['id'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }
}
