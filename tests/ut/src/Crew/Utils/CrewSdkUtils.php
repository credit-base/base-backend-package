<?php
namespace Base\Crew\Utils;

trait CrewSdkUtils
{
    private function compareValueObjectAndObject(
        $crewSdk,
        $crew
    ) {
        $this->assertEquals($crewSdk->getId(), $crew->getId());
    }
}
