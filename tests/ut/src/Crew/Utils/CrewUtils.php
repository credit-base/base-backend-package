<?php
namespace Base\Crew\Utils;

trait CrewUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $crew
    ) {
        $this->assertEquals($expectedArray['crew_id'], $crew->getId());
        $this->assertEquals($expectedArray['user_name'], $crew->getUserName());
        $this->assertEquals($expectedArray['cellphone'], $crew->getCellphone());
        $this->assertEquals($expectedArray['real_name'], $crew->getRealName());
        $this->assertEquals($expectedArray['card_id'], $crew->getCardId());
        $this->assertEquals($expectedArray['category'], $crew->getCategory());

        $purview = is_string($expectedArray['purview']) ?
        json_decode($expectedArray['purview'], true) :
        $expectedArray['purview'];
        $this->assertEquals($purview, $crew->getPurview());
        
        $this->assertEquals(
            $expectedArray['user_group_id'],
            $crew->getUserGroup()->getId()
        );
        $this->assertEquals(
            $expectedArray['department_id'],
            $crew->getDepartment()->getId()
        );
        $this->assertEquals($expectedArray['status'], $crew->getStatus());
        $this->assertEquals($expectedArray['create_time'], $crew->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $crew->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $crew->getStatusTime());
    }
}
