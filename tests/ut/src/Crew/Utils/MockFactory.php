<?php
namespace Base\Crew\Utils;

use Base\Crew\Model\Crew;

class MockFactory
{
    public static function generateCrew(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Crew {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $crew = new Crew($id);
        $crew->setId($id);

        //cellphone
        self::generateCellphone($crew, $faker, $value);
        //realName
        self::generateRealName($crew, $faker, $value);
        //cardId
        self::generateCardId($crew, $faker, $value);
        //category
        self::generateCategory($crew, $faker, $value);
        //userGroup
        self::generateUserGroup($crew, $faker, $value);
        //department
        self::generateDepartment($crew, $faker, $value);
        //password
        self::generatePassword($crew, $faker, $value);
        //salt
        self::generateSalt($crew, $faker, $value);

        //status
        $crew->setCreateTime($faker->unixTime());
        $crew->setUpdateTime($faker->unixTime());
        $crew->setStatusTime($faker->unixTime());

        return $crew;
    }

    private static function generateCellphone($crew, $faker, $value) : void
    {
        $cellphone = isset($value['cellphone']) ?
            $value['cellphone'] :
            $faker->phoneNumber();
        
        $crew->setUserName($cellphone);
        $crew->setCellphone($cellphone);
    }

    private static function generateRealName($crew, $faker, $value) : void
    {
        $realName = isset($value['realName']) ?
            $value['realName'] :
            $faker->name();
        
        $crew->setRealName($realName);
    }

    private static function generateCardId($crew, $faker, $value) : void
    {
        $cardId = isset($value['cardId']) ? $value['cardId'] : $faker->creditCardNumber();
        $crew->setCardId($cardId);
    }

    private static function generateCategory($crew, $faker, $value) : void
    {
        $category = isset($value['category']) ? $value['category'] : $faker->randomElement(Crew::CATEGORY);
        $crew->setCategory($category);
    }

    private static function generateUserGroup($crew, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
            $value['userGroup'] :
            \Base\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
        $crew->setUserGroup($userGroup);
    }

    private static function generateDepartment($crew, $faker, $value) : void
    {
        $department = isset($value['department']) ?
            $value['department'] :
            \Base\Department\Utils\MockFactory::generateDepartment($faker->randomDigit());
        $crew->setDepartment($department);
    }

    private static function generatePassword($crew, $faker, $value) : void
    {
        $password = isset($value['password']) ?
            $value['password'] :
            md5($faker->randomNumber());
        $crew->setPassword($password);
    }

    private static function generateSalt($crew, $faker, $value) : void
    {
        unset($faker);

        $salt = isset($value['salt']) ? $value['salt'] : 'salt';
        $crew->setSalt($salt);
    }
}
