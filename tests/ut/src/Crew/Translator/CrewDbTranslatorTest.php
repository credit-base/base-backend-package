<?php
namespace Base\Crew\Translator;

use PHPUnit\Framework\TestCase;

use Base\Crew\Utils\CrewUtils;

class CrewDbTranslatorTest extends TestCase
{
    use CrewUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new CrewDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Crew\Model\NullCrew', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $expression['crew_id'] = $crew->getId();
        $expression['user_name'] = $crew->getUserName();
        $expression['cellphone'] = $crew->getCellphone();
        $expression['real_name'] = $crew->getRealName();
        $expression['card_id'] = $crew->getCardId();
        $expression['category'] = $crew->getCategory();
        $expression['password'] = $crew->getPassword();
        $expression['salt'] = $crew->getSalt();
        $expression['purview'] = json_encode($crew->getPurview());
        $expression['user_group_id'] = $crew->getUserGroup()->getId();
        $expression['department_id'] = $crew->getDepartment()->getId();
        $expression['status'] = $crew->getStatus();
        $expression['status_time'] = $crew->getStatusTime();
        $expression['create_time'] = $crew->getCreateTime();
        $expression['update_time'] = $crew->getUpdateTime();

        $crew = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Crew\Model\Crew', $crew);
        $this->compareArrayAndObject($expression, $crew);
    }

    public function testObjectToArray()
    {
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $expression = $this->translator->objectToArray($crew);

        $this->compareArrayAndObject($expression, $crew);
    }
}
