<?php
namespace Base\Crew\Translator;

use PHPUnit\Framework\TestCase;

use Base\Crew\Model\NullCrew;
use Base\Crew\Utils\CrewSdkUtils;

use BaseSdk\Crew\Model\Crew as CrewSdk;
use BaseSdk\Crew\Model\NullCrew as NullCrewSdk;

class CrewSdkTranslatorTest extends TestCase
{
    use CrewSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new CrewSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $crewSdk = new CrewSdk($crew->getId());

        $crewObject = $this->translator->valueObjectToObject($crewSdk);
        $this->assertInstanceof('Base\Crew\Model\Crew', $crewObject);
        $this->compareValueObjectAndObject($crewSdk, $crewObject);
    }

    public function testValueObjectToObjectFail()
    {
        $crewSdk = NullCrewSdk::getInstance();

        $crew = $this->translator->valueObjectToObject($crewSdk);
        $this->assertInstanceof('Base\Crew\Model\NullCrew', $crew);
    }

    public function testObjectToValueObjectFail()
    {
        $crewSdk = NullCrewSdk::getInstance();

        $result = $this->translator->objectToValueObject($crewSdk);

        $this->assertEquals(
            $crewSdk,
            $result
        );
    }

    public function testObjectToValueObject()
    {
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $crewSdk = $this->translator->objectToValueObject($crew);
        $this->assertInstanceof('BaseSdk\Crew\Model\Crew', $crewSdk);
        $this->compareValueObjectAndObject($crewSdk, $crew);
    }
}
