<?php
namespace Base\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class EnableCrewCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new EnableCrewCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsEnableCommand()
    {
        $this->assertInstanceof('Base\Common\Command\EnableCommand', $this->stub);
    }
}
