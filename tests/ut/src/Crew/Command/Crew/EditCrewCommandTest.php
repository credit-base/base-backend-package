<?php
namespace Base\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class EditCrewCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'category' => $faker->randomNumber(),
            'realName' => $faker->name(),
            'cardId' => '610103190000000000',
            'purview' => array(),
            'userGroupId' => $faker->randomNumber(),
            'departmentId' => $faker->randomNumber(),
            'id' => $faker->randomNumber(),
        );

        $this->command = new EditCrewCommand(
            $this->fakerData['category'],
            $this->fakerData['realName'],
            $this->fakerData['cardId'],
            $this->fakerData['purview'],
            $this->fakerData['userGroupId'],
            $this->fakerData['departmentId'],
            $this->fakerData['id']
        );
    }

    public function testExtendsEditCommand()
    {
        $this->assertInstanceOf(
            'Base\Common\Command\EditCommand',
            $this->command
        );
    }

    public function testCategoryParameter()
    {
        $this->assertEquals($this->fakerData['category'], $this->command->category);
    }

    public function testRealNameParameter()
    {
        $this->assertEquals($this->fakerData['realName'], $this->command->realName);
    }

    public function testCardIdParameter()
    {
        $this->assertEquals($this->fakerData['cardId'], $this->command->cardId);
    }

    public function testUserGroupIdParameter()
    {
        $this->assertEquals($this->fakerData['userGroupId'], $this->command->userGroupId);
    }

    public function testDepartmentIdParameter()
    {
        $this->assertEquals($this->fakerData['departmentId'], $this->command->departmentId);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}
