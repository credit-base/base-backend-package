<?php
namespace Base\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class DisableCrewCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new DisableCrewCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsDisableCommand()
    {
        $this->assertInstanceof('Base\Common\Command\DisableCommand', $this->stub);
    }
}
