<?php
namespace Base\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class AddCrewCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'category' => $faker->randomNumber(),
            'realName' => $faker->name(),
            'cellphone' => $faker->phoneNumber(),
            'cardId' => '610103190000000000',
            'password' => $faker->password(),
            'purview' => array(),
            'userGroupId' => $faker->randomNumber(),
            'departmentId' => $faker->randomNumber(),
            'id' => $faker->randomNumber(),
        );

        $this->command = new AddCrewCommand(
            $this->fakerData['category'],
            $this->fakerData['realName'],
            $this->fakerData['cellphone'],
            $this->fakerData['cardId'],
            $this->fakerData['password'],
            $this->fakerData['purview'],
            $this->fakerData['userGroupId'],
            $this->fakerData['departmentId'],
            $this->fakerData['id']
        );
    }

    public function testExtendsAddCommand()
    {
        $this->assertInstanceOf(
            'Base\Common\Command\AddCommand',
            $this->command
        );
    }

    public function testCategoryParameter()
    {
        $this->assertEquals($this->fakerData['category'], $this->command->category);
    }

    public function testRealNameParameter()
    {
        $this->assertEquals($this->fakerData['realName'], $this->command->realName);
    }

    public function testCellphoneParameter()
    {
        $this->assertEquals($this->fakerData['cellphone'], $this->command->cellphone);
    }

    public function testCardIdParameter()
    {
        $this->assertEquals($this->fakerData['cardId'], $this->command->cardId);
    }

    public function testPasswordParameter()
    {
        $this->assertEquals($this->fakerData['password'], $this->command->password);
    }

    public function testPurviewParameter()
    {
        $this->assertEquals($this->fakerData['purview'], $this->command->purview);
    }

    public function testUserGroupIdParameter()
    {
        $this->assertEquals($this->fakerData['userGroupId'], $this->command->userGroupId);
    }

    public function testDepartmentIdParameter()
    {
        $this->assertEquals($this->fakerData['departmentId'], $this->command->departmentId);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testDefaultParameter()
    {
        $this->command = new AddCrewCommand(
            $this->fakerData['category'],
            $this->fakerData['realName'],
            $this->fakerData['cellphone'],
            $this->fakerData['cardId'],
            $this->fakerData['password'],
            $this->fakerData['purview']
        );

        $this->assertEquals(0, $this->command->userGroupId);
        $this->assertEquals(0, $this->command->departmentId);
        $this->assertEquals(0, $this->command->id);
    }
}
