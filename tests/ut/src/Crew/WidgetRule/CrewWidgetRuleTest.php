<?php
namespace Base\Crew\WidgetRule;

use PHPUnit\Framework\TestCase;
use Base\Crew\Model\Crew;
use Marmot\Core;

class CrewWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new CrewWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //category -- start
    /**
     * @dataProvider invalidCategoryProvider
     */
    public function testRealCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->category($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidCategoryProvider()
    {
        return array(
            array('', false),
            array(Crew::CATEGORY['SUPER_ADMINISTRATOR'], true),
            array(Crew::CATEGORY['PLATFORM_ADMINISTRATOR'], true),
            array(Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'], true),
            array(Crew::CATEGORY['OPERATOR'], true),
            array(0, false),
            array(999, false),
        );
    }
    //category -- end

    //purview -- start
    /**
     * @dataProvider invalidPurviewProvider
     */
    public function testPurviewInvalid($actual, $expected)
    {
        $result = $this->widgetRule->purview($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidPurviewProvider()
    {
        return array(
            array('', false),
            array(array('name'), false),
            array(999, false),
            array(array(1), true),
            array(array(), false),
        );
    }
    //purview -- end
}
