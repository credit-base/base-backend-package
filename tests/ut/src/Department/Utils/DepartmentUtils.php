<?php
namespace Base\Department\Utils;

trait DepartmentUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $department
    ) {
        $this->assertEquals($expectedArray['department_id'], $department->getId());
        $this->assertEquals($expectedArray['name'], $department->getName());
        $this->assertEquals($expectedArray['user_group_id'], $department->getUserGroup()->getId());
        $this->assertEquals($expectedArray['status'], $department->getStatus());
        $this->assertEquals($expectedArray['create_time'], $department->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $department->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $department->getStatusTime());
    }
}
