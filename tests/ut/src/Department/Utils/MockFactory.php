<?php
namespace Base\Department\Utils;

use Base\Department\Model\Department;
use Base\UserGroup\Model\UserGroup;

class MockFactory
{
    public static function generateDepartment(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Department {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $department = new Department($id);
        $department->setId($id);

        //name
        self::generateName($department, $faker, $value);
        //userGroup
        self::generateUserGroup($department, $faker, $value);

        //status
        
        $department->setCreateTime($faker->unixTime());
        $department->setUpdateTime($faker->unixTime());
        $department->setStatusTime($faker->unixTime());

        return $department;
    }

    private static function generateName($department, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->title;
        
        $department->setName($name);
    }

    private static function generateUserGroup($department, $faker, $value) : void
    {
        unset($faker);
        
        $userGroup = isset($value['userGroup']) ?
            new UserGroup($value['userGroup']) :
            \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);
            
        $department->setUserGroup($userGroup);
    }
}
