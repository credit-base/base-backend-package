<?php
namespace Base\Department\CommandHandler\Department;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Command\Department\AddDepartmentCommand;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

class AddDepartmentCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddDepartmentCommandHandler::class)
                                     ->setMethods(['getDepartment', 'getUserGroupRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetDepartment()
    {
        $commandHandler = new MockAddDepartmentCommandHandler();
        $this->assertInstanceOf(
            'Base\Department\Model\Department',
            $commandHandler->getDepartment()
        );
    }

    public function testGetUserGroupRepository()
    {
        $commandHandler = new MockAddDepartmentCommandHandler();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getUserGroupRepository()
        );
    }

    public function testExecuteSuccess()
    {
        $command = new AddDepartmentCommand(
            $this->faker->name,
            $this->faker->randomNumber(), //userGroupId
            $this->faker->randomNumber() //id
        );
        $expectId = 10;

        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup($command->userGroupId);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($command->userGroupId))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($userGroup);
        $this->commandHandler->expects($this->once())
            ->method('getUserGroupRepository')
            ->willReturn($userGroupRepository->reveal());

        $department = $this->prophesize(Department::class);
        $department->getId()->shouldBeCalledTimes(1)->willReturn($expectId);
        $department->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $department->setUserGroup(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $department->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getDepartment')
            ->willReturn($department->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
        $this->assertEquals($expectId, $command->id);
    }

    public function testExecuteFail()
    {
        $command = new AddDepartmentCommand(
            $this->faker->name,
            $this->faker->randomNumber(), //userGroupId
            $this->faker->randomNumber() //id
        );

        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup($command->userGroupId);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($command->userGroupId))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($userGroup);
        $this->commandHandler->expects($this->once())
            ->method('getUserGroupRepository')
            ->willReturn($userGroupRepository->reveal());

        $department = $this->prophesize(Department::class);
        $department->getId()->shouldBeCalledTimes(0);//调用0次
        $department->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $department->setUserGroup(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $department->add()->shouldBeCalledTimes(1)->willReturn(false);

        $this->commandHandler->expects($this->any())
            ->method('getDepartment')
            ->willReturn($department->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertFalse($result);
    }
}
