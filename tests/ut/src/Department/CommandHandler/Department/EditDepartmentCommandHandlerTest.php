<?php
namespace Base\Department\CommandHandler\Department;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Command\Department\EditDepartmentCommand;

class EditDepartmentCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditDepartmentCommandHandler::class)
                                     ->setMethods(['getDepartmentRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetDepartmentRepository()
    {
        $commandHandler = new MockEditDepartmentCommandHandler();
        $this->assertInstanceOf(
            'Base\Department\Repository\DepartmentRepository',
            $commandHandler->getDepartmentRepository()
        );
    }

    public function testExecute()
    {
        $command = new EditDepartmentCommand(
            $this->faker->name,
            $this->faker->randomNumber()
        );

        $department = $this->prophesize(Department::class);
        $department->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $department->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $departmentRepository = $this->prophesize(DepartmentRepository::class);
        $departmentRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($department->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getDepartmentRepository')
            ->willReturn($departmentRepository->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}
