<?php
namespace Base\Department\CommandHandler\Department;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Base\Department\Command\Department\AddDepartmentCommand;
use Base\Department\Command\Department\EditDepartmentCommand;

class DepartmentCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new DepartmentCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddDepartmentCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddDepartmentCommand(
                $this->faker->name(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Department\CommandHandler\Department\AddDepartmentCommandHandler',
            $commandHandler
        );
    }

    public function testEditDepartmentCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditDepartmentCommand(
                $this->faker->name(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Department\CommandHandler\Department\EditDepartmentCommandHandler',
            $commandHandler
        );
    }
}
