<?php
namespace Base\Department\Adapter\Department;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Department\Model\Department;
use Base\Department\Model\NullDepartment;
use Base\Department\Translator\DepartmentDbTranslator;
use Base\Department\Adapter\Department\Query\DepartmentRowCacheQuery;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class DepartmentDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(DepartmentDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction',
                                    'fetchUserGroup'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IDepartmentAdapter
     */
    public function testImplementsIDepartmentAdapter()
    {
        $this->assertInstanceOf(
            'Base\Department\Adapter\Department\IDepartmentAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 DepartmentDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockDepartmentDbAdapter();
        $this->assertInstanceOf(
            'Base\Department\Translator\DepartmentDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 DepartmentRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockDepartmentDbAdapter();
        $this->assertInstanceOf(
            'Base\Department\Adapter\Department\Query\DepartmentRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    /**
     * 测试是否初始化 UserGroupRepository
     */
    public function testGetUserGroupRepository()
    {
        $adapter = new MockDepartmentDbAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockDepartmentDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedDepartment = new Department();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedDepartment)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedDepartment);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedDepartment = new Department();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedDepartment, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedDepartment, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedDepartment = new Department();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedDepartment);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchUserGroup')
                         ->with($expectedDepartment);

        //验证
        $department = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedDepartment, $department);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $departmentOne = new Department(1);
        $departmentTwo = new Department(2);

        $ids = [1, 2];

        $expectedDepartmentList = [];
        $expectedDepartmentList[$departmentOne->getId()] = $departmentOne;
        $expectedDepartmentList[$departmentTwo->getId()] = $departmentTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedDepartmentList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchUserGroup')
                         ->with($expectedDepartmentList);

        //验证
        $departmentList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedDepartmentList, $departmentList);
    }

    //fetchUserGroup
    public function testFetchUserGroupIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockDepartmentDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchUserGroupByObject'
                                ]
                           )
                           ->getMock();
        
        $department = new Department();

        $adapter->expects($this->exactly(1))
                         ->method('fetchUserGroupByObject')
                         ->with($department);

        $adapter->fetchUserGroup($department);
    }

    public function testFetchUserGroupIsArray()
    {
        $adapter = $this->getMockBuilder(MockDepartmentDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchUserGroupByList'
                                ]
                           )
                           ->getMock();
        
        $departmentOne = new Department(1);
        $departmentTwo = new Department(2);
        
        $departmentList[$departmentOne->getId()] = $departmentOne;
        $departmentList[$departmentTwo->getId()] = $departmentTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchUserGroupByList')
                         ->with($departmentList);

        $adapter->fetchUserGroup($departmentList);
    }

    //fetchUserGroupByObject
    public function testFetchUserGroupByObject()
    {
        $adapter = $this->getMockBuilder(MockDepartmentDbAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $department = new Department();
        $department->setUserGroup(new UserGroup(1));
        
        $userGroupId = 1;

        // 为 UserGroup 类建立预言
        $userGroup  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(IUserGroupAdapter::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($userGroup);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByObject($department);
    }

    //fetchUserGroupByList
    public function testFetchUserGroupByList()
    {
        $adapter = $this->getMockBuilder(MockDepartmentDbAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $departmentOne = new Department(1);
        $userGroupOne = new UserGroup(1);
        $departmentOne->setUserGroup($userGroupOne);

        $departmentTwo = new Department(2);
        $userGroupTwo = new UserGroup(2);
        $departmentTwo->setUserGroup($userGroupTwo);
        $userGroupIds = [1, 2];
        
        $userGroupList[$userGroupOne->getId()] = $userGroupOne;
        $userGroupList[$userGroupTwo->getId()] = $userGroupTwo;
        
        $departmentList[$departmentOne->getId()] = $departmentOne;
        $departmentList[$departmentTwo->getId()] = $departmentTwo;

        $userGroupRepository = $this->prophesize(IUserGroupAdapter::class);
        $userGroupRepository->fetchList(Argument::exact($userGroupIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($userGroupList);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByList($departmentList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockDepartmentDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterName()
    {
        $filter = array(
            'name' => 1
        );
        $adapter = new MockDepartmentDbAdapter();

        $expectedCondition = 'name LIKE \'%'.$filter['name'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterUserGroup()
    {
        $filter = array(
            'userGroup' => 1
        );
        $adapter = new MockDepartmentDbAdapter();

        $expectedCondition = 'user_group_id = '.$filter['userGroup'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterId()
    {
        $filter = array(
            'id' => 1
        );
        $adapter = new MockDepartmentDbAdapter();

        $expectedCondition = 'department_id <> '.$filter['id'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterUnique()
    {
        $filter = array(
            'unique' => 'unique'
        );
        $adapter = new MockDepartmentDbAdapter();

        $expectedCondition = 'name = \''.$filter['unique'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockDepartmentDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockDepartmentDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
