<?php
namespace Base\Department\Adapter\Department\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class DepartmentCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockDepartmentCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(DepartmentCache::KEY, $this->cache->getKey());
    }
}
