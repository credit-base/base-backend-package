<?php
namespace Base\Department\Translator;

use PHPUnit\Framework\TestCase;

use Base\Department\Utils\DepartmentUtils;

class DepartmentDbTranslatorTest extends TestCase
{
    use DepartmentUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new DepartmentDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Department\Model\NullDepartment', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $department = \Base\Department\Utils\MockFactory::generateDepartment(1);

        $expression['department_id'] = $department->getId();
        $expression['name'] = $department->getName();
        $expression['user_group_id'] = $department->getUserGroup()->getId();
        $expression['status'] = $department->getStatus();
        $expression['status_time'] = $department->getStatusTime();
        $expression['create_time'] = $department->getCreateTime();
        $expression['update_time'] = $department->getUpdateTime();

        $department = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Department\Model\Department', $department);
        $this->compareArrayAndObject($expression, $department);
    }

    public function testObjectToArray()
    {
        $department = \Base\Department\Utils\MockFactory::generateDepartment(1);

        $expression = $this->translator->objectToArray($department);

        $this->compareArrayAndObject($expression, $department);
    }
}
