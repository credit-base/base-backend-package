<?php
namespace Base\Department\Model;

use PHPUnit\Framework\TestCase;

class NullDepartmentTest extends TestCase
{
    private $department;

    public function setUp()
    {
        $this->department = NullDepartment::getInstance();
    }

    public function tearDown()
    {
        unset($this->department);
    }

    public function testExtendsNews()
    {
        $this->assertInstanceof('Base\Department\Model\Department', $this->department);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->department);
    }
}
