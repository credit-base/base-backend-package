<?php
namespace Base\Department\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\UserGroup\Model\NullUserGroup;
use Base\UserGroup\Model\UserGroup;
use Base\Department\Adapter\Department\IDepartmentAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class DepartmentTest extends TestCase
{
    private $department;

    public function setUp()
    {
        $this->department = new Department();
    }

    public function tearDown()
    {
        unset($this->department);
    }

    /**
     * Department 单位领域对象,测试构造函数
     */
    public function testConstructor()
    {
        //测试初始化单位id
        $this->assertEquals(0, $this->department->getId());

        //测试初始化单位名称
        $this->assertEmpty($this->department->getName());

        //测试初始化委办局
        $this->assertInstanceof('Base\UserGroup\Model\UserGroup', $this->department->getUserGroup());

        //测试初始化创建时间
        $this->assertEquals(Core::$container->get('time'), $this->department->getCreateTime());

        //测试初始化状态
        $this->assertEquals(Department::STATUS_NORMAL, $this->department->getStatus());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Department setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->department->setId(1);
        $this->assertEquals(1, $this->department->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 Department setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->department->setName('string');
        $this->assertEquals('string', $this->department->getName());
    }

    /**
     * 设置 Department setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->department->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //userGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Department setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->department->setUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->department->getUserGroup());
    }

    /**
     * 设置 Department setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->department->setUserGroup('userGroup');
    }
    //userGroup 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $department = new MockDepartment();
        $this->assertInstanceOf('Base\Department\Adapter\Department\IDepartmentAdapter', $department->getRepository());
    }

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IDepartmentAdapter 调用 add 并且传参 $department 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['getRepository', 'isNameExist'])
                           ->getMock();

        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);
        $department->setUserGroup($userGroup);

        $department->expects($this->once())
                   ->method('isNameExist')
                   ->willReturn(true);

        //预言 IDepartmentAdapter
        $repository = $this->prophesize(IDepartmentAdapter::class);
        $repository->add(Argument::exact($department))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $department->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $department->add();
        $this->assertTrue($result);
    }

    /**
     * 测试添加失败-所属委办局为空
     * 1. 期望返回false
     * 2. 设置委办局为NullUserGroup
     * 3. 调用add,期望返回false
     */
    public function testAddFailUserGroupNull()
    {
        $this->department->setUserGroup(new NullUserGroup());

        $result = $this->department->add();

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('userGroupId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    /**
     * 测试添加失败-名称重复
     * 1. 期望返回false
     * 2. 设置委办局为UserGroup
     * 3. 期望isNameExist被调用一次,并返回false
     * 4. 调用add,期望返回false
     */
    public function testAddFailNameExist()
    {
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['isNameExist'])
                           ->getMock();

        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);
        $department->setUserGroup($userGroup);

        $department->expects($this->once())
                   ->method('isNameExist')
                   ->willReturn(false);

        $result = $department->add();

        $this->assertFalse($result);
    }

    //add()
    /**
     * 测试添加失败
     * 1. 期望返回false
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IDepartmentAdapter 调用 add 并且传参 $department 对象, 返回 false
     */
    public function testAddFail()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['isNameExist', 'getRepository'])
                           ->getMock();

        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);
        $department->setUserGroup($userGroup);

        $department->expects($this->once())
                   ->method('isNameExist')
                   ->willReturn(true);

        //预言 IDepartmentAdapter
        $repository = $this->prophesize(IDepartmentAdapter::class);
        $repository->add(Argument::exact($department))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        //绑定
        $department->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $department->add();
        $this->assertFalse($result);
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 name, updateTime
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IDepartmentAdapter 调用 edit 并且传参 $department 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['isNameExist', 'setUpdateTime', 'getRepository'])
                           ->getMock();

        $department->expects($this->once())
                   ->method('isNameExist')
                   ->willReturn(true);
                   
        //预言修改updateTime
        $department->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IDepartmentAdapter
        $repository = $this->prophesize(IDepartmentAdapter::class);
        $repository->edit(
            Argument::exact($department),
            Argument::exact(
                [
                    'name',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $department->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $department->edit();
        $this->assertTrue($result);
    }

    /**
     * 测试编辑失败-名称重复
     * 1. 期望返回false
     * 2. 期望isNameExist被调用一次,并返回false
     * 3. 调用edit,期望返回false
     */
    public function testEditFailNameExist()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['isNameExist'])
                           ->getMock();

        $department->expects($this->once())
                   ->method('isNameExist')
                   ->willReturn(false);

        //验证
        $result = $department->edit();

        $this->assertFalse($result);
    }
    //edit
    /**
     * 期望编辑失败
     * 1. 期望更新 name, updateTime
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IDepartmentAdapter 调用 edit 并且传参 $department 对象, 和相应字段, 返回false
     */
    public function testEditFail()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['isNameExist', 'setUpdateTime', 'getRepository'])
                           ->getMock();

        $department->expects($this->once())
                   ->method('isNameExist')
                   ->willReturn(true);
                   
        //预言修改updateTime
        $department->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IDepartmentAdapter
        $repository = $this->prophesize(IDepartmentAdapter::class);
        $repository->edit(
            Argument::exact($department),
            Argument::exact(
                [
                    'name',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(false);

        //绑定
        $department->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $department->edit();
        $this->assertFalse($result);
    }

    private function initialIsNameExist($result)
    {
        $this->department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $id = 1;
        
        $department = \Base\Department\Utils\MockFactory::generateDepartment($id);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($department);
        }

        $this->department->setId($department->getId());
        $this->department->setName($department->getName());
        $this->department->setUserGroup($department->getUserGroup());

        $filter['id'] = $department->getId();
        $filter['unique'] = $department->getName();
        $filter['userGroup'] = $department->getUserGroup()->getId();

        $repository = $this->prophesize(IDepartmentAdapter::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->department->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testIsNameExistFailure()
    {
        $this->initialIsNameExist(false);

        $result = $this->department->isNameExist();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
        $this->assertEquals('departmentName', Core::getLastError()->getSource()['pointer']);
    }

    public function testIsNameExistSuccess()
    {
        $this->initialIsNameExist(true);
        
        $result = $this->department->isNameExist();

        $this->assertTrue($result);
    }
}
