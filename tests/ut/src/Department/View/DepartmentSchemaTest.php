<?php
namespace Base\Department\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class DepartmentSchemaTest extends TestCase
{
    private $departmentSchema;

    private $department;

    public function setUp()
    {
        $this->departmentSchema = new DepartmentSchema(new Factory());

        $this->department = \Base\Department\Utils\MockFactory::generateDepartment(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->departmentSchema);
        unset($this->department);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->departmentSchema);
    }

    public function testGetId()
    {
        $result = $this->departmentSchema->getId($this->department);

        $this->assertEquals($result, $this->department->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->departmentSchema->getAttributes($this->department);

        $this->assertEquals($result['name'], $this->department->getName());
        $this->assertEquals($result['status'], $this->department->getStatus());
        $this->assertEquals($result['createTime'], $this->department->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->department->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->department->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->departmentSchema->getRelationships($this->department, 0, array());

        $this->assertEquals($result['userGroup'], ['data' => $this->department->getUserGroup()]);
    }
}
