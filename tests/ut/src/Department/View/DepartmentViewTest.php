<?php
namespace Base\Department\View;

use PHPUnit\Framework\TestCase;

use Base\Department\Model\Department;

class DepartmentViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $department = new DepartmentView(new Department());
        $this->assertInstanceof('Base\Common\View\CommonView', $department);
    }
}
