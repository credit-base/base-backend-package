<?php
namespace Base\Department\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Command\Department\AddDepartmentCommand;
use Base\Department\Command\Department\EditDepartmentCommand;

class DepartmentOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new DepartmentOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    /**
     * 测试 add 成功
     * 1. 为 DepartmentOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getDepartmentWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 Department 类建立预言
     * 4. 为 DepartmentRepository 类建立预言, DepartmentRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Department, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 为 DepartmentOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getDepartmentWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(DepartmentOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getDepartmentWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 true
        $this->initAdd($controller, true);

        // 为 Department 类建立预言
        $department = $this->prophesize(Department::class);

        // 为 DepartmentRepository 类建立预言, DepartmentRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Department, getRepository 方法被调用一次
        $id = 0;
        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($department);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->add 方法被调用一次, 且返回结果为 true
        $result = $controller->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 DepartmentOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 为 DepartmentOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(DepartmentOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd($controller, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->add 方法被调用一次, 且返回结果为 false
        $result = $controller->add();
        $this->assertFalse($result);
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用两次
     * 5. 为 CommandBus 类建立预言, 传入 AddDepartmentCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(DepartmentOperateController $controller, bool $result)
    {
        // mock 请求参数
        $data = array(
            'attributes' => array(
                "name" => "信用办",
            ),
            'relationships' => array(
                'userGroup' => array(
                    'data' => array(
                        array('type' => 'userGroups', 'id' => 1)
                    )
                )
            )
        );

        $name = "信用办";
        $userGroupId = 1;

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        // 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用两次
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->name(Argument::exact($name))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($userGroupId), Argument::exact('userGroupId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(2))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        // 为 CommandBus 类建立预言, 传入 AddDepartmentCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AddDepartmentCommand(
                    $name,
                    $userGroupId
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    /**
     * 测试 edit 成功
     * 1. 为 DepartmentOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getCommandBus、getRepository、render方法
     * 2. 调用 $this->initEdit(), 期望结果为 true
     * 3. 为 Department 类建立预言
     * 4. 为 DepartmentRepository 类建立预言, DepartmentRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Department, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 DepartmentOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(DepartmentOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommandBus',
                    'getCommonWidgetRule',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 true
        $this->initEdit($controller, $id, true);

        // 为 Department 类建立预言
        $department = $this->prophesize(Department::class);

        // 为 DepartmentRepository 类建立预言, DepartmentRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Department, getRepository 方法被调用一次
        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($department);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->edit 方法被调用一次, 且返回结果为 true
        $result = $controller->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 DepartmentOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getCommandBus、displayError 方法
     * 2. 调用 $this->initEdit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 DepartmentOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(DepartmentOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommandBus',
                    'getCommonWidgetRule',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 false
        $this->initEdit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->edit 方法被调用一次, 且返回结果为 false
        $result = $controller->edit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 edit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数, getCommonWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 EditDepartmentCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEdit(DepartmentOperateController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = array(
            'attributes' => array(
                "name" => "信用办",
            )
        );

        $name = "信用办";

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        // 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用两次
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->name(Argument::exact($name))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        // 为 CommandBus 类建立预言, 传入 EditDepartmentCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditDepartmentCommand(
                    $name,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }
}
