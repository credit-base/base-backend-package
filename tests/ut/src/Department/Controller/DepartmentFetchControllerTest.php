<?php
namespace Base\Department\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Department\Adapter\Department\IDepartmentAdapter;
use Base\Department\Model\NullDepartment;
use Base\Department\Model\Department;
use Base\Department\View\DepartmentView;

class DepartmentFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(DepartmentFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockDepartmentFetchController();

        $this->assertInstanceOf(
            'Base\Department\Adapter\Department\IDepartmentAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockDepartmentFetchController();

        $this->assertInstanceOf(
            'Base\Department\View\DepartmentView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockDepartmentFetchController();

        $this->assertEquals(
            'departments',
            $controller->getResourceName()
        );
    }
}
