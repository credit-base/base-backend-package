<?php
namespace Base\Department\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Department\Adapter\Department\IDepartmentAdapter;
use Base\Department\Adapter\Department\DepartmentDbAdapter;

class DepartmentRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(DepartmentRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->childRepository = new class extends DepartmentRepository
        {
            public function getAdapter() : IDepartmentAdapter
            {
                return parent::getAdapter();
            }

            public function getActualAdapter() : IDepartmentAdapter
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter() : IDepartmentAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new DepartmentDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->childRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Department\Adapter\Department\IDepartmentAdapter',
            $this->childRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Department\Adapter\Department\DepartmentDbAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Department\Adapter\Department\IDepartmentAdapter',
            $this->childRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Department\Adapter\Department\DepartmentMockAdapter',
            $this->childRepository->getMockAdapter()
        );
    }

    public function testAdd()
    {
        $id = 1;
        $department = \Base\Department\Utils\MockFactory::generateDepartment($id);
        $keys = array();
        
        $adapter = $this->prophesize(IDepartmentAdapter::class);
        $adapter->add(Argument::exact($department))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($department, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $department = \Base\Department\Utils\MockFactory::generateDepartment($id);
        $keys = array();
        
        $adapter = $this->prophesize(IDepartmentAdapter::class);
        $adapter->edit(Argument::exact($department), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($department, $keys);
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IDepartmentAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IDepartmentAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IDepartmentAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }
}
