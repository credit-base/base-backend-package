<?php
namespace Base\Department\Command\Department;

use PHPUnit\Framework\TestCase;

class AddDepartmentCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'name' => $faker->name(),
            'userGroupId' => $faker->randomNumber(),
            'id' => $faker->randomNumber(),
        );

        $this->command = new AddDepartmentCommand(
            $this->fakerData['name'],
            $this->fakerData['userGroupId'],
            $this->fakerData['id']
        );
    }

    public function testExtendsAddCommand()
    {
        $this->assertInstanceOf(
            'Base\Common\Command\AddCommand',
            $this->command
        );
    }

    public function testNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }

    public function testUserGroupIdParameter()
    {
        $this->assertEquals($this->fakerData['userGroupId'], $this->command->userGroupId);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testDefaultIdParameter()
    {
        $this->command = new AddDepartmentCommand(
            $this->fakerData['name'],
            $this->fakerData['userGroupId']
        );

        $this->assertEquals(0, $this->command->id);
    }
}
