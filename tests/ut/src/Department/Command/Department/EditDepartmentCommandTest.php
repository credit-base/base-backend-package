<?php
namespace Base\Department\Command\Department;

use PHPUnit\Framework\TestCase;

class EditDepartmentCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'name' => $faker->name(),
            'id' => $faker->randomNumber(),
        );

        $this->command = new EditDepartmentCommand(
            $this->fakerData['name'],
            $this->fakerData['id']
        );
    }

    public function testExtendsEditCommand()
    {
        $this->assertInstanceOf(
            'Base\Common\Command\EditCommand',
            $this->command
        );
    }

    public function testNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}
