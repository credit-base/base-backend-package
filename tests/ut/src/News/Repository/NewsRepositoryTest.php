<?php
namespace Base\News\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\News\Adapter\News\INewsAdapter;
use Base\News\Adapter\News\NewsDbAdapter;

use Base\News\Utils\MockFactory;

class NewsRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(NewsRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockNewsRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new NewsDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\News\Adapter\News\INewsAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\News\Adapter\News\NewsDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\News\Adapter\News\INewsAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\News\Adapter\News\NewsMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(INewsAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(INewsAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(INewsAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $news = MockFactory::generateNews($id);
        $keys = array();
        
        $adapter = $this->prophesize(INewsAdapter::class);
        $adapter->add(Argument::exact($news))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($news, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $news = MockFactory::generateNews($id);
        $keys = array();
        
        $adapter = $this->prophesize(INewsAdapter::class);
        $adapter->edit(Argument::exact($news), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($news, $keys);
    }
}
