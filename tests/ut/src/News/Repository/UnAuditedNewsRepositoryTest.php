<?php
namespace Base\News\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;
use Base\News\Adapter\News\UnAuditedNewsDbAdapter;
use Base\News\Adapter\News\UnAuditedNewsMockAdapter;

use Base\News\Utils\MockFactory;

class UnAuditedNewsRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditedNewsRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockUnAuditedNewsRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\News\Adapter\News\UnAuditedNewsDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\News\Adapter\News\UnAuditedNewsMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }
}
