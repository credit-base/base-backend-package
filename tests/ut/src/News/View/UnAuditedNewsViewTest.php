<?php
namespace Base\News\View;

use PHPUnit\Framework\TestCase;

use Base\News\Model\UnAuditedNews;

class UnAuditedNewsViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $news = new UnAuditedNewsView(new UnAuditedNews());
        $this->assertInstanceof('Base\Common\View\CommonView', $news);
    }
}
