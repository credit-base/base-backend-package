<?php
namespace Base\News\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class NewsSchemaTest extends TestCase
{
    private $newsSchema;

    private $news;

    public function setUp()
    {
        $this->newsSchema = new NewsSchema(new Factory());

        $this->news = \Base\News\Utils\MockFactory::generateNews(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->newsSchema);
        unset($this->news);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->newsSchema);
    }

    public function testGetId()
    {
        $result = $this->newsSchema->getId($this->news);

        $this->assertEquals($result, $this->news->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->newsSchema->getAttributes($this->news);

        $this->assertEquals($result['title'], $this->news->getTitle());
        $this->assertEquals($result['source'], $this->news->getSource());
        $this->assertEquals($result['cover'], $this->news->getCover());
        $this->assertEquals($result['attachments'], $this->news->getAttachments());
        $this->assertEquals($result['content'], $this->news->getContent());
        $this->assertEquals($result['description'], $this->news->getDescription());
        $this->assertEquals($result['parentCategory'], $this->news->getNewsCategory()->getParentCategory());
        $this->assertEquals($result['category'], $this->news->getNewsCategory()->getCategory());
        $this->assertEquals($result['newsType'], $this->news->getNewsCategory()->getType());
        $this->assertEquals($result['dimension'], $this->news->getDimension());
        $this->assertEquals($result['bannerImage'], $this->news->getBanner()->getImage());
        $this->assertEquals($result['bannerStatus'], $this->news->getBanner()->getStatus());
        $this->assertEquals($result['homePageShowStatus'], $this->news->getHomePageShowStatus());
        $this->assertEquals($result['stick'], $this->news->getStick());
        $this->assertEquals($result['status'], $this->news->getStatus());
        $this->assertEquals($result['createTime'], $this->news->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->news->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->news->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->newsSchema->getRelationships($this->news, 0, array());

        $this->assertEquals($result['publishUserGroup'], ['data' => $this->news->getPublishUserGroup()]);
        $this->assertEquals($result['crew'], ['data' => $this->news->getCrew()]);
    }
}
