<?php
namespace Base\News\View;

use PHPUnit\Framework\TestCase;

use Base\News\Model\News;

class NewsViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $news = new NewsView(new News());
        $this->assertInstanceof('Base\Common\View\CommonView', $news);
    }
}
