<?php
namespace Base\News\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class UnAuditedNewsSchemaTest extends TestCase
{
    private $unAuditedNewsSchema;

    private $unAuditedNews;

    public function setUp()
    {
        $this->unAuditedNewsSchema = new UnAuditedNewsSchema(new Factory());

        $this->unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->unAuditedNewsSchema);
        unset($this->unAuditedNews);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->unAuditedNewsSchema);
    }

    public function testGetId()
    {
        $result = $this->unAuditedNewsSchema->getId($this->unAuditedNews);

        $this->assertEquals($result, $this->unAuditedNews->getApplyId());
    }

    public function testGetAttributes()
    {
        $result = $this->unAuditedNewsSchema->getAttributes($this->unAuditedNews);


        $this->assertEquals($result['title'], $this->unAuditedNews->getTitle());
        $this->assertEquals($result['source'], $this->unAuditedNews->getSource());
        $this->assertEquals($result['cover'], $this->unAuditedNews->getCover());
        $this->assertEquals($result['attachments'], $this->unAuditedNews->getAttachments());
        $this->assertEquals($result['content'], $this->unAuditedNews->getContent());
        $this->assertEquals($result['description'], $this->unAuditedNews->getDescription());
        $this->assertEquals($result['parentCategory'], $this->unAuditedNews->getNewsCategory()->getParentCategory());
        $this->assertEquals($result['category'], $this->unAuditedNews->getNewsCategory()->getCategory());
        $this->assertEquals($result['newsType'], $this->unAuditedNews->getNewsCategory()->getType());
        $this->assertEquals($result['dimension'], $this->unAuditedNews->getDimension());
        $this->assertEquals($result['bannerImage'], $this->unAuditedNews->getBanner()->getImage());
        $this->assertEquals($result['bannerStatus'], $this->unAuditedNews->getBanner()->getStatus());
        $this->assertEquals($result['homePageShowStatus'], $this->unAuditedNews->getHomePageShowStatus());
        $this->assertEquals($result['stick'], $this->unAuditedNews->getStick());
        $this->assertEquals($result['status'], $this->unAuditedNews->getStatus());
        $this->assertEquals($result['createTime'], $this->unAuditedNews->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->unAuditedNews->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->unAuditedNews->getStatusTime());
        $this->assertEquals(
            $result['applyInfoCategory'],
            $this->unAuditedNews->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals(
            $result['applyInfoType'],
            $this->unAuditedNews->getApplyInfoCategory()->getType()
        );
        $this->assertEquals($result['applyStatus'], $this->unAuditedNews->getApplyStatus());
        $this->assertEquals($result['rejectReason'], $this->unAuditedNews->getRejectReason());
        $this->assertEquals($result['operationType'], $this->unAuditedNews->getOperationType());
        $this->assertEquals($result['newsId'], $this->unAuditedNews->getId());
    }

    public function testGetRelationships()
    {
        $result = $this->unAuditedNewsSchema->getRelationships($this->unAuditedNews, 0, array());

        $this->assertEquals($result['publishUserGroup'], ['data' => $this->unAuditedNews->getPublishUserGroup()]);
        $this->assertEquals($result['crew'], ['data' => $this->unAuditedNews->getCrew()]);
        $this->assertEquals($result['relation'], ['data' => $this->unAuditedNews->getRelation()]);
        $this->assertEquals($result['applyCrew'], ['data' => $this->unAuditedNews->getApplyCrew()]);
        $this->assertEquals($result['applyUserGroup'], ['data' => $this->unAuditedNews->getApplyUserGroup()]);
    }
}
