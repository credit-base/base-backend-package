<?php
namespace Base\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\News\Model\UnAuditedNews;
use Base\News\Command\News\AddNewsCommand;

class AddNewsCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddNewsCommandHandler::class)
                                     ->setMethods(['getUnAuditedNews', 'executeAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $applyInfo = array('applyInfo');

        $command = new AddNewsCommand(
            $unAuditedNews->getTitle(),
            $unAuditedNews->getSource(),
            $unAuditedNews->getCover(),
            $unAuditedNews->getAttachments(),
            $unAuditedNews->getContent(),
            $unAuditedNews->getNewsCategory()->getType(),
            $unAuditedNews->getDimension(),
            $unAuditedNews->getStatus(),
            $unAuditedNews->getStick(),
            $unAuditedNews->getBanner()->getStatus(),
            $unAuditedNews->getBanner()->getImage(),
            $unAuditedNews->getHomePageShowStatus(),
            $unAuditedNews->getCrew()->getId()
        );

        $unAuditedNews = $this->prophesize(UnAuditedNews::class);
    
        $this->commandHandler->expects($this->exactly(1))
             ->method('executeAction')
             ->willReturn($unAuditedNews->reveal());

        $this->commandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $unAuditedNews->setOperationType(Argument::exact($command->operationType))
                       ->shouldBeCalledTimes(1);
        $unAuditedNews->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);

        $unAuditedNews->add()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $unAuditedNews->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
            ->method('getUnAuditedNews')
            ->willReturn($unAuditedNews->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
