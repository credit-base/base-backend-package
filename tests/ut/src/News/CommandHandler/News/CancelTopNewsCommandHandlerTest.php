<?php
namespace Base\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\News\Command\News\CancelTopNewsCommand;

use Base\News\Model\News;

class CancelTopNewsCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockCancelTopNewsCommandHandler::class)
        ->setMethods(['fetchNews'])
        ->getMock();
    }

    public function testExtendsCancelTopCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Common\CommandHandler\CancelTopCommandHandler',
            $this->stub
        );
    }

    public function testFetchITopObject()
    {
        $id = 1;
        $news = \Base\News\Utils\MockFactory::generateNews($id);

        $this->stub->expects($this->once())
             ->method('fetchNews')
             ->with($id)
             ->willReturn($news);

        $result = $this->stub->fetchITopObject($id);

        $this->assertEquals($result, $news);
    }

    public function testExecuteAction()
    {
        $stub = $this->getMockBuilder(MockCancelTopNewsCommandHandler::class)
                ->setMethods(['fetchITopObject', 'fetchCrew'])
                ->getMock();

        $id = 1;
        $news = \Base\News\Utils\MockFactory::generateNews($id);
        $crew = $news->getCrew();

        $command = new CancelTopNewsCommand($crew->getId(), $id);

        $stub->expects($this->once())
             ->method('fetchCrew')
             ->with($crew->getId())
             ->willReturn($crew);

        $news = $this->prophesize(News::class);

        $news->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $news->setPublishUserGroup(Argument::exact($crew->getUserGroup()))->shouldBeCalledTimes(1);

        $news->cancelTop()->shouldBeCalledTimes(1)->willReturn(true);

        $stub->expects($this->exactly(1))->method('fetchITopObject')->willReturn($news->reveal());

        $result = $stub->executeAction($command);

        $this->assertTrue($result);
    }
}
