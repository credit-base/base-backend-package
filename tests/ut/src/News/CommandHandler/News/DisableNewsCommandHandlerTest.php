<?php
namespace Base\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\News\Command\News\DisableNewsCommand;

use Base\News\Model\News;

class DisableNewsCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDisableNewsCommandHandler::class)
        ->setMethods(['fetchNews'])
        ->getMock();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $news = \Base\News\Utils\MockFactory::generateNews($id);

        $this->stub->expects($this->once())
             ->method('fetchNews')
             ->with($id)
             ->willReturn($news);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $news);
    }

    public function testExecuteAction()
    {
        $stub = $this->getMockBuilder(MockDisableNewsCommandHandler::class)
                ->setMethods(['fetchIEnableObject', 'fetchCrew'])
                ->getMock();

        $id = 1;
        $news = \Base\News\Utils\MockFactory::generateNews($id);
        $crew = $news->getCrew();

        $command = new DisableNewsCommand($crew->getId(), $id);

        $stub->expects($this->once())
             ->method('fetchCrew')
             ->with($crew->getId())
             ->willReturn($crew);

        $news = $this->prophesize(News::class);

        $news->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $news->setPublishUserGroup(Argument::exact($crew->getUserGroup()))->shouldBeCalledTimes(1);

        $news->disable()->shouldBeCalledTimes(1)->willReturn(true);

        $stub->expects($this->exactly(1))->method('fetchIEnableObject')->willReturn($news->reveal());

        $result = $stub->executeAction($command);

        $this->assertTrue($result);
    }
}
