<?php
namespace Base\News\CommandHandler\News;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\Model\UnAuditedNews;
use Base\News\Repository\NewsRepository;
use Base\News\Translator\NewsDbTranslator;
use Base\News\Command\News\AddNewsCommand;
use Base\News\Command\News\EnableNewsCommand;
use Base\News\Repository\UnAuditedNewsRepository;

use Base\ApplyForm\Model\ApplyInfoCategory;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class NewsCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetCrewRepository()
    {
        $trait = new MockNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $trait->publicGetCrewRepository()
        );
    }

    public function testGetNewsRepository()
    {
        $trait = new MockNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\News\Repository\NewsRepository',
            $trait->publicGetNewsRepository()
        );
    }

    public function testGetNewsDbTranslator()
    {
        $trait = new MockNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\News\Translator\NewsDbTranslator',
            $trait->publicGetNewsDbTranslator()
        );
    }

    public function testGetUnAuditedNewsRepository()
    {
        $trait = new MockNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\News\Repository\UnAuditedNewsRepository',
            $trait->publicGetUnAuditedNewsRepository()
        );
    }

    public function testGetNews()
    {
        $trait = new MockNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\News\Model\News',
            $trait->publicGetNews()
        );
    }

    public function testGetUnAuditedNews()
    {
        $trait = new MockNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\News\Model\UnAuditedNews',
            $trait->publicGetUnAuditedNews()
        );
    }

    public function testFetchCrew()
    {
        $trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                 ->setMethods(['getCrewRepository']) ->getMock();

        $id = 1;

        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $trait->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchCrew($id);

        $this->assertEquals($result, $crew);
    }

    public function testFetchNews()
    {
        $trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                 ->setMethods(['getNewsRepository']) ->getMock();

        $id = 1;

        $news = \Base\News\Utils\MockFactory::generateNews($id);

        $repository = $this->prophesize(NewsRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($news);

        $trait->expects($this->exactly(1))
                         ->method('getNewsRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchNews($id);

        $this->assertEquals($result, $news);
    }

    public function testFetchUnAuditedNews()
    {
        $trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                 ->setMethods(['getUnAuditedNewsRepository']) ->getMock();
                 
        $id = 1;

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);

        $repository = $this->prophesize(UnAuditedNewsRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedNews);

        $trait->expects($this->exactly(1))
                         ->method('getUnAuditedNewsRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditedNews($id);

        $this->assertEquals($result, $unAuditedNews);
    }

    public function testExecuteAction()
    {
        $trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                 ->setMethods(['fetchCrew']) ->getMock();
        $id = 1;

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);

        $command = new AddNewsCommand(
            $unAuditedNews->getTitle(),
            $unAuditedNews->getSource(),
            $unAuditedNews->getCover(),
            $unAuditedNews->getAttachments(),
            $unAuditedNews->getContent(),
            $unAuditedNews->getNewsCategory()->getType(),
            $unAuditedNews->getDimension(),
            $unAuditedNews->getStatus(),
            $unAuditedNews->getStick(),
            $unAuditedNews->getBanner()->getStatus(),
            $unAuditedNews->getBanner()->getImage(),
            $unAuditedNews->getHomePageShowStatus(),
            $unAuditedNews->getCrew()->getId()
        );

        $trait->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($unAuditedNews->getCrew()->getId())
                         ->willReturn($unAuditedNews->getCrew());
                         
        $result = $trait->publicExecuteAction($command, $unAuditedNews);

        $this->assertEquals($result, $unAuditedNews);
    }

    public function testNewsExecuteAction()
    {
        $trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                 ->setMethods(['fetchCrew', 'fetchNews']) ->getMock();
        $id = 1;

        $news = \Base\News\Utils\MockFactory::generateNews($id);
        $unAuditedNews = new UnAuditedNews();

        $command = new EnableNewsCommand(
            $id,
            $news->getCrew()->getId()
        );

        $trait->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($news->getCrew()->getId())
                         ->willReturn($news->getCrew());

        $trait->expects($this->exactly(1))
                         ->method('fetchNews')
                         ->with($id)
                         ->willReturn($news);
                         
        $result = $trait->publicNewsExecuteAction($command, $unAuditedNews);

        $this->assertEquals($result, $unAuditedNews);
    }

    public function testApplyInfo()
    {
        $trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                 ->setMethods(['getNewsDbTranslator']) ->getMock();

        $applyInfo = array('applyInfo');
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews(1);

        $translator = $this->prophesize(NewsDbTranslator::class);
        $translator->objectToArray(Argument::exact($unAuditedNews))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($applyInfo);

        $trait->expects($this->exactly(1))
                         ->method('getNewsDbTranslator')
                         ->willReturn($translator->reveal());

                         
        $result = $trait->publicApplyInfo($unAuditedNews);

        $this->assertEquals($result, $applyInfo);
    }
}
