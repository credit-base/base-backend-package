<?php
namespace Base\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\News\Model\UnAuditedNews;
use Base\News\Command\News\TopNewsCommand;

class TopNewsCommandHandlerTest extends TestCase
{
    private $topCommandHandler;

    private $faker;

    public function setUp()
    {
        $this->topCommandHandler = $this->getMockBuilder(TopNewsCommandHandler::class)
                                     ->setMethods(['getUnAuditedNews', 'newsExecuteAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->topCommandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->topCommandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->topCommandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 2;

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $applyInfo = array('applyInfo');

        $command = new TopNewsCommand(
            $id,
            $unAuditedNews->getCrew()->getId()
        );

        $unAuditedNews = $this->prophesize(UnAuditedNews::class);
    
        $this->topCommandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $this->topCommandHandler->expects($this->exactly(1))
             ->method('newsExecuteAction')
             ->willReturn($unAuditedNews->reveal());

        $unAuditedNews->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);

        $unAuditedNews->top()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $unAuditedNews->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->topCommandHandler->expects($this->exactly(1))
            ->method('getUnAuditedNews')
            ->willReturn($unAuditedNews->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->topCommandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->topCommandHandler->execute($command);

        $this->assertFalse($result);
    }
}
