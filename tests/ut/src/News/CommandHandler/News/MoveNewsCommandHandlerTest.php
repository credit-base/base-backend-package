<?php
namespace Base\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\News\Model\UnAuditedNews;
use Base\News\Command\News\MoveNewsCommand;

class MoveNewsCommandHandlerTest extends TestCase
{
    private $moveCommandHandler;

    private $faker;

    public function setUp()
    {
        $this->moveCommandHandler = $this->getMockBuilder(MoveNewsCommandHandler::class)
                                     ->setMethods(['getUnAuditedNews', 'newsExecuteAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->moveCommandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->moveCommandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->moveCommandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 2;

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $applyInfo = array('applyInfo');

        $command = new MoveNewsCommand(
            $unAuditedNews->getNewsCategory()->getType(),
            $id,
            $unAuditedNews->getCrew()->getId()
        );

        $unAuditedNews = $this->prophesize(UnAuditedNews::class);
    
        $this->moveCommandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $this->moveCommandHandler->expects($this->exactly(1))
             ->method('newsExecuteAction')
             ->willReturn($unAuditedNews->reveal());

        $unAuditedNews->setCategory(Argument::exact($command->newsType))->shouldBeCalledTimes(1);
        $unAuditedNews->setApplyInfoCategory(
            Argument::exact(new ApplyInfoCategory(UnAuditedNews::APPLY_NEWS_CATEGORY, $command->newsType))
        )->shouldBeCalledTimes(1);

        $unAuditedNews->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);

        $unAuditedNews->move()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $unAuditedNews->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->moveCommandHandler->expects($this->exactly(1))
            ->method('getUnAuditedNews')
            ->willReturn($unAuditedNews->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->moveCommandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->moveCommandHandler->execute($command);

        $this->assertFalse($result);
    }
}
