<?php
namespace Base\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\News\Model\UnAuditedNews;
use Base\News\Command\News\EnableNewsCommand;

class EnableNewsCommandHandlerTest extends TestCase
{
    private $enableCommandHandler;

    private $faker;

    public function setUp()
    {
        $this->enableCommandHandler = $this->getMockBuilder(EnableNewsCommandHandler::class)
                                     ->setMethods(['getUnAuditedNews', 'newsExecuteAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->enableCommandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->enableCommandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->enableCommandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 2;

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $applyInfo = array('applyInfo');

        $command = new EnableNewsCommand(
            $id,
            $unAuditedNews->getCrew()->getId()
        );

        $unAuditedNews = $this->prophesize(UnAuditedNews::class);
    
        $this->enableCommandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $this->enableCommandHandler->expects($this->exactly(1))
             ->method('newsExecuteAction')
             ->willReturn($unAuditedNews->reveal());

        $unAuditedNews->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);

        $unAuditedNews->enable()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $unAuditedNews->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->enableCommandHandler->expects($this->exactly(1))
            ->method('getUnAuditedNews')
            ->willReturn($unAuditedNews->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->enableCommandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->enableCommandHandler->execute($command);

        $this->assertFalse($result);
    }
}
