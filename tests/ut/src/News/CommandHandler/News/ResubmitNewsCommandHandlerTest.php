<?php
namespace Base\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\News\Model\UnAuditedNews;
use Base\News\Command\News\ResubmitNewsCommand;

class ResubmitNewsCommandHandlerTest extends TestCase
{
    private $resubmitCommandHandler;

    private $faker;

    public function setUp()
    {
        $this->resubmitCommandHandler = $this->getMockBuilder(ResubmitNewsCommandHandler::class)
                                     ->setMethods(['fetchUnAuditedNews', 'executeAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->resubmitCommandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->resubmitCommandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->resubmitCommandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 2;

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $applyInfo = array('applyInfo');

        $command = new ResubmitNewsCommand(
            $unAuditedNews->getTitle(),
            $unAuditedNews->getSource(),
            $unAuditedNews->getCover(),
            $unAuditedNews->getAttachments(),
            $unAuditedNews->getContent(),
            $unAuditedNews->getNewsCategory()->getType(),
            $unAuditedNews->getDimension(),
            $unAuditedNews->getStatus(),
            $unAuditedNews->getStick(),
            $unAuditedNews->getBanner()->getStatus(),
            $unAuditedNews->getBanner()->getImage(),
            $unAuditedNews->getHomePageShowStatus(),
            $unAuditedNews->getCrew()->getId()
        );

        $unAuditedNews = $this->prophesize(UnAuditedNews::class);
    
        $this->resubmitCommandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $this->resubmitCommandHandler->expects($this->exactly(1))
             ->method('executeAction')
             ->willReturn($unAuditedNews->reveal());

        $unAuditedNews->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);

        $unAuditedNews->resubmit()->shouldBeCalledTimes(1)->willReturn($result);

        $this->resubmitCommandHandler->expects($this->exactly(1))
            ->method('fetchUnAuditedNews')
            ->willReturn($unAuditedNews->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->resubmitCommandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->resubmitCommandHandler->execute($command);

        $this->assertFalse($result);
    }
}
