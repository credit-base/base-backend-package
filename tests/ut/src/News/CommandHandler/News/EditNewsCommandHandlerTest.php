<?php
namespace Base\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\News\Model\UnAuditedNews;
use Base\News\Command\News\EditNewsCommand;

class EditNewsCommandHandlerTest extends TestCase
{
    private $editCommandHandler;

    private $faker;

    public function setUp()
    {
        $this->editCommandHandler = $this->getMockBuilder(EditNewsCommandHandler::class)
                                     ->setMethods(['getUnAuditedNews', 'executeAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->editCommandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->editCommandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->editCommandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 2;

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $applyInfo = array('applyInfo');

        $command = new EditNewsCommand(
            $unAuditedNews->getTitle(),
            $unAuditedNews->getSource(),
            $unAuditedNews->getCover(),
            $unAuditedNews->getAttachments(),
            $unAuditedNews->getContent(),
            $unAuditedNews->getNewsCategory()->getType(),
            $unAuditedNews->getDimension(),
            $unAuditedNews->getStatus(),
            $unAuditedNews->getStick(),
            $unAuditedNews->getBanner()->getStatus(),
            $unAuditedNews->getBanner()->getImage(),
            $unAuditedNews->getHomePageShowStatus(),
            $id,
            $unAuditedNews->getCrew()->getId()
        );

        $unAuditedNews = $this->prophesize(UnAuditedNews::class);
    
        $this->editCommandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $this->editCommandHandler->expects($this->exactly(1))
             ->method('executeAction')
             ->willReturn($unAuditedNews->reveal());

        $unAuditedNews->setOperationType(Argument::exact($command->operationType))
                       ->shouldBeCalledTimes(1);
        $unAuditedNews->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);
        $unAuditedNews->setId(Argument::exact($command->newsId))->shouldBeCalledTimes(1);

        $unAuditedNews->edit()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $unAuditedNews->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->editCommandHandler->expects($this->exactly(1))
            ->method('getUnAuditedNews')
            ->willReturn($unAuditedNews->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->editCommandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->editCommandHandler->execute($command);

        $this->assertFalse($result);
    }
}
