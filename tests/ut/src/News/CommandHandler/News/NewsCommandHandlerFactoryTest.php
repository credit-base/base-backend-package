<?php
namespace Base\News\CommandHandler\News;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Base\News\Command\News\AddNewsCommand;
use Base\News\Command\News\CancelTopNewsCommand;
use Base\News\Command\News\DisableNewsCommand;
use Base\News\Command\News\EditNewsCommand;
use Base\News\Command\News\EnableNewsCommand;
use Base\News\Command\News\MoveNewsCommand;
use Base\News\Command\News\ResubmitNewsCommand;
use Base\News\Command\News\TopNewsCommand;

class NewsCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new NewsCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddNewsCommand(
                $this->faker->title(),
                $this->faker->title(),
                array($this->faker->title()),
                array($this->faker->title()),
                $this->faker->text(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                array($this->faker->title()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\News\CommandHandler\News\AddNewsCommandHandler',
            $commandHandler
        );
    }

    public function testEditNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditNewsCommand(
                $this->faker->title(),
                $this->faker->title(),
                array($this->faker->title()),
                array($this->faker->md5()),
                $this->faker->text(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                array($this->faker->md5()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\News\CommandHandler\News\EditNewsCommandHandler',
            $commandHandler
        );
    }

    public function testCancelTopNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new CancelTopNewsCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\News\CommandHandler\News\CancelTopNewsCommandHandler',
            $commandHandler
        );
    }

    public function testDisableNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableNewsCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\News\CommandHandler\News\DisableNewsCommandHandler',
            $commandHandler
        );
    }

    public function testEnableNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EnableNewsCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\News\CommandHandler\News\EnableNewsCommandHandler',
            $commandHandler
        );
    }

    public function testMoveNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new MoveNewsCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\News\CommandHandler\News\MoveNewsCommandHandler',
            $commandHandler
        );
    }

    public function testResubmitNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResubmitNewsCommand(
                $this->faker->title(),
                $this->faker->word(),
                array($this->faker->md5()),
                array($this->faker->md5()),
                $this->faker->text(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                array($this->faker->title()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\News\CommandHandler\News\ResubmitNewsCommandHandler',
            $commandHandler
        );
    }

    public function testTopNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new TopNewsCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\News\CommandHandler\News\TopNewsCommandHandler',
            $commandHandler
        );
    }
}
