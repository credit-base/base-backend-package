<?php
namespace Base\News\Command\News;

use PHPUnit\Framework\TestCase;

use Base\News\Model\News;
use Base\News\Model\Banner;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class EditNewsCommandTest extends TestCase
{
    use NewsTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'title' => $faker->title(),
            'source' => $faker->title(),
            'cover' => array($faker->name()),
            'attachments' => array($faker->name()),
            'content' => $faker->text(),
            'newsType' => $faker->randomElement(NEWS_TYPE),
            'dimension' => $faker->randomElement(News::DIMENSIONALITY),
            'status' => $faker->randomElement(IEnableAble::STATUS),
            'stick' => $faker->randomElement(ITopAble::STICK),
            'bannerStatus' => $faker->randomElement(Banner::BANNER_STATUS),
            'bannerImage' => array($faker->word()),
            'homePageShowStatus' => $faker->randomElement(News::HOME_PAGE_SHOW_STATUS),
            'newsId' => $faker->randomDigit(),
            'crew' => $faker->randomDigit(),
            'id' => $faker->randomDigit()
        );

        $this->command = new EditNewsCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['cover'],
            $this->fakerData['attachments'],
            $this->fakerData['content'],
            $this->fakerData['newsType'],
            $this->fakerData['dimension'],
            $this->fakerData['status'],
            $this->fakerData['stick'],
            $this->fakerData['bannerStatus'],
            $this->fakerData['bannerImage'],
            $this->fakerData['homePageShowStatus'],
            $this->fakerData['newsId'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }

    public function testNewsIdParameter()
    {
        $this->assertEquals($this->fakerData['newsId'], $this->command->newsId);
    }
}
