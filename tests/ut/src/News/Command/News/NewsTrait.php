<?php
namespace Base\News\Command\News;

use PHPUnit\Framework\TestCase;

trait NewsTrait
{
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testTitleParameter()
    {
        $this->assertEquals($this->fakerData['title'], $this->command->title);
    }

    public function testSourceParameter()
    {
        $this->assertEquals($this->fakerData['source'], $this->command->source);
    }

    public function testCoverParameter()
    {
        $this->assertEquals($this->fakerData['cover'], $this->command->cover);
    }

    public function testAttachmentsParameter()
    {
        $this->assertEquals($this->fakerData['attachments'], $this->command->attachments);
    }

    public function testContentParameter()
    {
        $this->assertEquals($this->fakerData['content'], $this->command->content);
    }

    public function testNewsTypeParameter()
    {
        $this->assertEquals($this->fakerData['newsType'], $this->command->newsType);
    }

    public function testDimensionParameter()
    {
        $this->assertEquals($this->fakerData['dimension'], $this->command->dimension);
    }

    public function testStatusParameter()
    {
        $this->assertEquals($this->fakerData['status'], $this->command->status);
    }

    public function testStickParameter()
    {
        $this->assertEquals($this->fakerData['stick'], $this->command->stick);
    }

    public function testBannerStatusParameter()
    {
        $this->assertEquals($this->fakerData['bannerStatus'], $this->command->bannerStatus);
    }

    public function testBannerImageParameter()
    {
        $this->assertEquals($this->fakerData['bannerImage'], $this->command->bannerImage);
    }

    public function testHomePageShowStatusParameter()
    {
        $this->assertEquals($this->fakerData['homePageShowStatus'], $this->command->homePageShowStatus);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }
}
