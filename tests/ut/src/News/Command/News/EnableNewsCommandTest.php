<?php
namespace Base\News\Command\News;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class EnableNewsCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        
        $this->fakerData = array(
            'newsId' => $faker->randomDigit(),
            'crew' => $faker->randomDigit(),
            'id' => $faker->randomDigit()
        );

        $this->command = new EnableNewsCommand(
            $this->fakerData['newsId'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }

    public function testNewsIdParameter()
    {
        $this->assertEquals($this->fakerData['newsId'], $this->command->newsId);
    }
}
