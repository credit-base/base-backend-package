<?php
namespace Base\News\Command\News;

use PHPUnit\Framework\TestCase;

use Base\News\Model\News;
use Base\News\Model\Banner;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class AddNewsCommandTest extends TestCase
{
    use NewsTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'title' => $faker->title(),
            'source' => $faker->word(),
            'cover' => array($faker->md5()),
            'attachments' => array($faker->word()),
            'content' => $faker->word(),
            'newsType' => $faker->randomElement(NEWS_TYPE),
            'dimension' => $faker->randomElement(News::DIMENSIONALITY),
            'status' => $faker->randomElement(IEnableAble::STATUS),
            'stick' => $faker->randomElement(ITopAble::STICK),
            'bannerStatus' => $faker->randomElement(Banner::BANNER_STATUS),
            'bannerImage' => array($faker->word()),
            'homePageShowStatus' => $faker->randomElement(News::HOME_PAGE_SHOW_STATUS),
            'crew' => $faker->randomDigit(1),
            'id' => $faker->randomDigit()
        );

        $this->command = new AddNewsCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['cover'],
            $this->fakerData['attachments'],
            $this->fakerData['content'],
            $this->fakerData['newsType'],
            $this->fakerData['dimension'],
            $this->fakerData['status'],
            $this->fakerData['stick'],
            $this->fakerData['bannerStatus'],
            $this->fakerData['bannerImage'],
            $this->fakerData['homePageShowStatus'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }
}
