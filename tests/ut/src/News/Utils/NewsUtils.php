<?php
namespace Base\News\Utils;

trait NewsUtils
{

    private function compareArrayAndObjectNews(
        array $expectedArray,
        $news
    ) {
        $this->assertEquals($expectedArray['news_id'], $news->getId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $news);
    }

    private function compareArrayAndObjectsUnAuditedNews(
        array $expectedArray,
        $unAuditedNews
    ) {
        $this->assertEquals($expectedArray['news_id'], $unAuditedNews->getApplyId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedNews);
    }

    private function compareArrayAndObjectUnAuditedNews(
        array $expectedArray,
        $unAuditedNews
    ) {
        $applyInfo = array();
        if (is_string($expectedArray['apply_info'])) {
            $applyInfo = unserialize(gzuncompress(base64_decode($expectedArray['apply_info'], true)));
        }
        if (is_array($expectedArray['apply_info'])) {
            $applyInfo = $expectedArray['apply_info'];
        }
        
        $this->assertEquals($applyInfo, $unAuditedNews->getApplyInfo());
        $this->assertEquals($expectedArray['apply_form_id'], $unAuditedNews->getApplyId());
        $this->assertEquals($expectedArray['title'], $unAuditedNews->getApplyTitle());
        $this->assertEquals($expectedArray['relation_id'], $unAuditedNews->getRelation()->getId());
        $this->assertEquals($expectedArray['operation_type'], $unAuditedNews->getOperationType());
        $this->assertEquals(
            $expectedArray['apply_info_category'],
            $unAuditedNews->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals($expectedArray['apply_info_type'], $unAuditedNews->getApplyInfoCategory()->getType());
        $this->assertEquals($expectedArray['reject_reason'], $unAuditedNews->getRejectReason());
        $this->assertEquals($expectedArray['apply_status'], $unAuditedNews->getApplyStatus());
        $this->assertEquals($expectedArray['apply_crew_id'], $unAuditedNews->getApplyCrew()->getId());
        $this->assertEquals($expectedArray['apply_usergroup_id'], $unAuditedNews->getApplyUserGroup()->getId());
    }

    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $news
    ) {
        $this->assertEquals($expectedArray['title'], $news->getTitle());
        $this->assertEquals($expectedArray['source'], $news->getSource());
        $this->coverEquals($expectedArray, $news);
        $this->attachmentsEquals($expectedArray, $news);
        $this->assertEquals($expectedArray['content'], $news->getContent());
        $this->assertEquals($expectedArray['description'], $news->getDescription());
        $this->assertEquals($expectedArray['parent_category'], $news->getNewsCategory()->getParentCategory());
        $this->assertEquals($expectedArray['category'], $news->getNewsCategory()->getCategory());
        $this->assertEquals($expectedArray['type'], $news->getNewsCategory()->getType());
        $this->assertEquals($expectedArray['crew_id'], $news->getCrew()->getId());
        $this->assertEquals($expectedArray['publish_usergroup_id'], $news->getPublishUserGroup()->getId());
        $this->bannerImageEquals($expectedArray, $news);
        $this->assertEquals($expectedArray['banner_status'], $news->getBanner()->getStatus());
        $this->assertEquals($expectedArray['home_page_show_status'], $news->getHomePageShowStatus());
        $this->assertEquals($expectedArray['dimension'], $news->getDimension());

        $this->assertEquals($expectedArray['stick'], $news->getStick());
        $this->assertEquals($expectedArray['status'], $news->getStatus());
        $this->assertEquals($expectedArray['create_time'], $news->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $news->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $news->getStatusTime());
    }

    private function coverEquals($expectedArray, $news)
    {
        $cover = array();

        if (is_string($expectedArray['cover'])) {
            $cover = json_decode($expectedArray['cover'], true);
        }
        if (is_array($expectedArray['cover'])) {
            $cover = $expectedArray['cover'];
        }

        $this->assertEquals($cover, $news->getCover());
    }

    private function attachmentsEquals($expectedArray, $news)
    {
        $attachments = array();

        if (is_string($expectedArray['attachments'])) {
            $attachments = json_decode($expectedArray['attachments'], true);
        }
        if (is_array($expectedArray['attachments'])) {
            $attachments = $expectedArray['attachments'];
        }

        $this->assertEquals($attachments, $news->getAttachments());
    }

    private function bannerImageEquals($expectedArray, $news)
    {
        $bannerImage = array();

        if (is_string($expectedArray['banner_image'])) {
            $bannerImage = json_decode($expectedArray['banner_image'], true);
        }
        if (is_array($expectedArray['banner_image'])) {
            $bannerImage = $expectedArray['banner_image'];
        }

        $this->assertEquals($bannerImage, $news->getBanner()->getImage());
    }
}
