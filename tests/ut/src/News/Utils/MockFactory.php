<?php
namespace Base\News\Utils;

use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\Model\UnAuditedNews;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

use Base\ApplyForm\Model\ApplyInfoCategory;

class MockFactory
{
    public static function generateCommon(
        News $news,
        int $seed = 0,
        array $value = array()
    ) {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);
        //title
        $title = isset($value['title']) ? $value['title'] : $faker->word();
        $news->setTitle($title);

        //source
        $source = isset($value['source']) ? $value['source'] : $faker->word();
        $news->setSource($source);

        //cover
        $cover = isset($value['cover']) ? $value['cover'] : array(
            'name'=>$faker->name(),
            'identify'=>$faker->word().'.jpg'
        );
        $news->setCover($cover);

        //attachments
        $attachments = isset($value['attachments']) ? $value['attachments'] : array(
            array('name'=>$faker->name(), 'identify'=>$faker->word().'.doc')
        );
        $news->setAttachments($attachments);

        //content
        $content = isset($value['content']) ? $value['content'] : $faker->text();
        $news->setContent($content);
        $news->subDescription($content);

        //newsCategory
        self::generateNewsCategory($news, $faker, $value);
        //crew
        self::generateCrew($news, $faker, $value);
        //publishUserGroup
        $news->setPublishUserGroup($news->getCrew()->getUserGroup());
        //banner
        self::generateBanner($news, $faker, $value);
        //dimension
        self::generateDimension($news, $faker, $value);
        //homePageShowStatus
        self::generateHomePageShowStatus($news, $faker, $value);

        self::generateStick($news, $faker, $value);
        
        self::generateStatus($news, $faker, $value);

        $news->setCreateTime($faker->unixTime());
        $news->setUpdateTime($faker->unixTime());
        $news->setStatusTime($faker->unixTime());

        return $news;
    }

    protected static function generateNewsCategory($news, $faker, $value) : void
    {
        $newsType = isset($value['newsType']) ?
            $value['newsType'] :
            $faker->randomElement(
                NEWS_TYPE
            );
        
        $news->setCategory($newsType);
    }

    protected static function generateCrew($news, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $news->setCrew($crew);
    }

    protected static function generateBanner($news, $faker, $value) : void
    {
        //image
        $image = isset($value['image']) ? $value['image'] : array(
            'name'=>$faker->name(),
            'identify'=>$faker->word().'.jpg'
        );
        
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                Banner::BANNER_STATUS
            );

        $banner = new Banner($image, $status);
        $news->setBanner($banner);
    }
    
    protected static function generateDimension($news, $faker, $value) : void
    {
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            $faker->randomElement(
                News::DIMENSIONALITY
            );
        
        $news->setDimension($dimension);
    }

    protected static function generateHomePageShowStatus($news, $faker, $value) : void
    {
        $homePageShowStatus = isset($value['homePageShowStatus']) ?
            $value['homePageShowStatus'] :
            $faker->randomElement(
                News::HOME_PAGE_SHOW_STATUS
            );
        
        $news->setHomePageShowStatus($homePageShowStatus);
    }

    protected static function generateStatus($news, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                IEnableAble::STATUS
            );
        
        $news->setStatus($status);
    }

    protected static function generateStick($news, $faker, $value) : void
    {
        $stick = isset($value['stick']) ?
            $value['stick'] :
            $faker->randomElement(
                ITopAble::STICK
            );
        
        $news->setStick($stick);
    }

    public static function generateNews(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : News {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $news = new News($id);
        $news->setId($id);

        $news = self::generateCommon($news, $seed, $value);

        return $news;
    }

    public static function generateUnAuditedNews(int $id = 0, int $seed = 0, array $value = array()) : UnAuditedNews
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditNews = new UnAuditedNews($id);
        
        $unAuditNews->setApplyId($id);

        $unAuditNews = self::generateCommon($unAuditNews, $seed, $value);

        $unAuditNews->setApplyTitle($unAuditNews->getTitle());
        $unAuditNews->setRelation($unAuditNews->getCrew());
        $unAuditNews->setApplyUserGroup($unAuditNews->getPublishUserGroup());
        $unAuditNews->setApplyCrew($unAuditNews->getCrew());
        $unAuditNews->setRejectReason($unAuditNews->getTitle());
        $unAuditNews->setApplyInfo(array('apply_info'));

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            IApproveAble::APPLY_STATUS
        );
        $unAuditNews->setApplyStatus($applyStatus);

        //operationType
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomElement(
            IApproveAble::OPERATION_TYPE
        );
        $unAuditNews->setOperationType($operationType);

        //type
        $type = isset($value['type']) ? $value['type'] : $faker->randomElement(
            NEWS_TYPE
        );
        $unAuditNews->setApplyInfoCategory(new ApplyInfoCategory(UnAuditedNews::APPLY_NEWS_CATEGORY, $type));

        return $unAuditNews;
    }
}
