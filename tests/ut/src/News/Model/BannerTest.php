<?php
namespace Base\News\Model;

use PHPUnit\Framework\TestCase;

class BannerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new Banner(
            array('image'),
            Banner::BANNER_STATUS['DISABLED']
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetImage()
    {
        $this->assertEquals(array('image'), $this->stub->getImage());
    }

    public function testGetStatus()
    {
        $this->assertEquals(Banner::BANNER_STATUS['DISABLED'], $this->stub->getStatus());
    }
}
