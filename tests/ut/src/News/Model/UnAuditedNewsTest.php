<?php
namespace Base\News\Model;

use PHPUnit\Framework\TestCase;

use Base\ApplyForm\Model\IApplyFormAble;

class UnAuditedNewsTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new UnAuditedNews();
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Base\ApplyForm\Model\IApplyFormAble', $this->stub);
    }

    public function testCorrectExtendsNews()
    {
        $this->assertInstanceof('Base\News\Model\News', $this->stub);
    }

    public function testUnAuditedNewsConstructor()
    {
        $this->assertEquals(0, $this->stub->getApplyId());
        $this->assertEquals('', $this->stub->getApplyTitle());
        $this->assertEquals(UnAuditedNews::OPERATION_TYPE['NULL'], $this->stub->getOperationType());
        $this->assertEquals(array(), $this->stub->getApplyInfo());
        $this->assertEquals(UnAuditedNews::APPLY_STATUS['PENDING'], $this->stub->getApplyStatus());
        $this->assertEquals('', $this->stub->getRejectReason());
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->stub->getRelation()
        );
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->stub->getApplyCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->stub->getApplyUserGroup()
        );
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\ApplyInfoCategory',
            $this->stub->getApplyInfoCategory()
        );
    }

    public function testMove()
    {
        $unAuditedNews = $this->getMockBuilder(UnAuditedNews::class)
                           ->setMethods(['apply'])
                           ->getMock();

        $unAuditedNews->expects($this->exactly(1))
             ->method('apply')
             ->willReturn(true);

        $result = $unAuditedNews->move();
        $this->assertTrue($result);
    }
}
