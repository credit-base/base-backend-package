<?php
namespace Base\News\Model;

use PHPUnit\Framework\TestCase;

class NullNewsTest extends TestCase
{
    private $news;

    public function setUp()
    {
        $this->news = NullNews::getInstance();
    }

    public function tearDown()
    {
        unset($this->news);
    }

    public function testExtendsNews()
    {
        $this->assertInstanceof('Base\News\Model\News', $this->news);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->news);
    }
}
