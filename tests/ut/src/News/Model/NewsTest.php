<?php
namespace Base\News\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;

use Base\Crew\Model\Crew;
use Base\Crew\Model\NullCrew;

use Base\UserGroup\Model\UserGroup;

use Base\News\Adapter\News\INewsAdapter;

use Common\Utils\StringGenerate;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class NewsTest extends TestCase
{
    private $news;

    public function setUp()
    {
        $this->news = new MockNews();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->news);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->news
        );
    }

    public function testImplementsIEnableAble()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IEnableAble',
            $this->news
        );
    }

    public function testImplementsITopAble()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\ITopAble',
            $this->news
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IOperate',
            $this->news
        );
    }

    public function testGetRepository()
    {
        $news = new MockNews();
        $this->assertInstanceOf('Base\News\Adapter\News\INewsAdapter', $news->getRepository());
    }

    public function testNewsConstructor()
    {
        $this->assertEquals(0, $this->news->getId());
        $this->assertEmpty($this->news->getTitle());
        $this->assertEmpty($this->news->getSource());
        $this->assertEquals(array(), $this->news->getCover());
        $this->assertEquals(array(), $this->news->getAttachments());
        $this->assertEquals('', $this->news->getContent());
        $this->assertEquals('', $this->news->getDescription());
        $this->assertInstanceOf(
            'Base\News\Model\NewsCategory',
            $this->news->getNewsCategory()
        );
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->news->getCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->news->getPublishUserGroup()
        );
        $this->assertEquals(News::DIMENSIONALITY['SOCIOLOGY'], $this->news->getDimension());
        $this->assertInstanceOf(
            'Base\News\Model\Banner',
            $this->news->getBanner()
        );
        $this->assertEquals(News::HOME_PAGE_SHOW_STATUS['DISABLED'], $this->news->getHomePageShowStatus());
        $this->assertEquals(ITopAble::STICK['DISABLED'], $this->news->getStick());
        $this->assertEquals(IEnableAble::STATUS['ENABLED'], $this->news->getStatus());
        $this->assertEquals(Core::$container->get('time'), $this->news->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $this->news->getCreateTime());
        $this->assertEquals(0, $this->news->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 News setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->news->setId(1);
        $this->assertEquals(1, $this->news->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //title 测试 ------------------------------------------------------- start
    /**
     * 设置 News setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->news->setTitle('string');
        $this->assertEquals('string', $this->news->getTitle());
    }

    /**
     * 设置 News setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->news->setTitle(array(1, 2, 3));
    }
    //title 测试 -------------------------------------------------------   end

    //source 测试 ------------------------------------------------------ start
    /**
     * 设置 Journal setSource() 正确的传参类型,期望传值正确
     */
    public function testSetSourceCorrectType()
    {
        $this->news->setSource('string');
        $this->assertEquals('string', $this->news->getSource());
    }

    /**
     * 设置 Journal setSource() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceWrongType()
    {
        $this->news->setSource(array(1, 2, 3));
    }
    //source 测试 ------------------------------------------------------   end

    //cover 测试 ------------------------------------------------------ start
    public function testSetCoverCorrectType()
    {
        $this->news->setCover(array('cover'));
        $this->assertEquals(array('cover'), $this->news->getCover());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetCoverWrongType()
    {
        $this->news->setCover('cover');
    }
    //cover 测试 ------------------------------------------------------   end

    //attachments 测试 ------------------------------------------------- start
    public function testSetAttachmentsCorrectType()
    {
        $this->news->setAttachments(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->news->getAttachments());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetAttachmentsWrongType()
    {
        $this->news->setAttachments(1);
    }
    //attachments 测试 -------------------------------------------------   end

    //content 测试 ----------------------------------------------------- start
    /**
     * 设置 News setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->news->setContent('string');
        $this->assertEquals('string', $this->news->getContent());
    }

    /**
     * 设置 News setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->news->setContent(array(1, 2, 3));
    }
    //content 测试 -----------------------------------------------------   end

    public function testSubDescription()
    {
        $content = StringGenerate::generate(1000);
        $news = new News();
        $news->subDescription($content);
    
        $this->news->subDescription($content);

        $this->assertEquals($news->getDescription(), $this->news->getDescription());
    }
    
    //newsCategory 测试 --------------------------------------------- start
    /**
     * 设置 News setNewsCategory() 正确的传参类型,期望传值正确
     */
    public function testSetNewsCategoryCorrectType()
    {
        $object = new NewsCategory();
        $this->news->setNewsCategory($object);
        $this->assertSame($object, $this->news->getNewsCategory());
    }

    /**
     * 设置 News setNewsCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNewsCategoryWrongType()
    {
        $this->news->setNewsCategory('string');
    }
    //newsCategory 测试 ---------------------------------------------   end

    public function testSetCategory()
    {
        $newsType = 0;
        $parentCategory = 0;
        $category = 0;

        $newsCategory = new NewsCategory(
            $parentCategory,
            $category,
            $newsType
        );

        $this->news->setCategory($newsType);
        $this->assertEquals($newsCategory, $this->news->getNewsCategory());
    }

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 News setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->news->setCrew($object);
        $this->assertSame($object, $this->news->getCrew());
    }

    /**
     * 设置 News setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->news->setCrew('string');
    }
    //crew 测试 ---------------------------------------------   end

    //publishUserGroup 测试 --------------------------------------------- start
    /**
     * 设置 News setPublishUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetPublishUserGroupCorrectType()
    {
        $object = new UserGroup();
        $this->news->setPublishUserGroup($object);
        $this->assertSame($object, $this->news->getPublishUserGroup());
    }

    /**
     * 设置 News setPublishUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPublishUserGroupWrongType()
    {
        $this->news->setPublishUserGroup('string');
    }
    //publishUserGroup 测试 ---------------------------------------------   end

    //banner 测试 --------------------------------------------- start
    /**
     * 设置 News setBanner() 正确的传参类型,期望传值正确
     */
    public function testSetBannerCorrectType()
    {
        $object = new Banner();
        $this->news->setBanner($object);
        $this->assertSame($object, $this->news->getBanner());
    }

    /**
     * 设置 News setBanner() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBannerWrongType()
    {
        $this->news->setBanner('string');
    }
    //banner 测试 ---------------------------------------------   end

    //homePageShowStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 News setHomePageShowStatus() 是否符合预定范围
     *
     * @dataProvider homePageShowStatusProvider
     */
    public function testSetHomePageShowStatus($actual, $expected)
    {
        $this->news->setHomePageShowStatus($actual);
        $this->assertEquals($expected, $this->news->getHomePageShowStatus());
    }

    /**
     * 循环测试 News setHomePageShowStatus() 数据构建器
     */
    public function homePageShowStatusProvider()
    {
        return array(
            array(News::HOME_PAGE_SHOW_STATUS['ENABLED'],News::HOME_PAGE_SHOW_STATUS['ENABLED']),
            array(News::HOME_PAGE_SHOW_STATUS['DISABLED'],News::HOME_PAGE_SHOW_STATUS['DISABLED']),
            array(999,News::HOME_PAGE_SHOW_STATUS['DISABLED']),
        );
    }

    /**
     * 设置 News setHomePageShowStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHomePageShowStatusWrongType()
    {
        $this->news->setHomePageShowStatus('string');
    }
    //homePageShowStatus 测试 ------------------------------------------------------   end

    //dimension 测试 ------------------------------------------------------ start
    /**
     * 循环测试 News setDimension() 是否符合预定范围
     *
     * @dataProvider dimensionProvider
     */
    public function testSetDimension($actual, $expected)
    {
        $this->news->setDimension($actual);
        $this->assertEquals($expected, $this->news->getDimension());
    }

    /**
     * 循环测试 News setDimension() 数据构建器
     */
    public function dimensionProvider()
    {
        return array(
            array(News::DIMENSIONALITY['SOCIOLOGY'],News::DIMENSIONALITY['SOCIOLOGY']),
            array(News::DIMENSIONALITY['GOVERNMENT_AFFAIRS'],News::DIMENSIONALITY['GOVERNMENT_AFFAIRS']),
            array(999,News::DIMENSIONALITY['SOCIOLOGY']),
        );
    }

    /**
     * 设置 News setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->news->setDimension('string');
    }
    //dimension 测试 ------------------------------------------------------   end

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 INewsAdapter 调用 add 并且传参 $news 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $news = $this->getMockBuilder(MockNews::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $news->setCrew($crew);

        //预言 INewsAdapter
        $repository = $this->prophesize(INewsAdapter::class);
        $repository->add(Argument::exact($news))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $news->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $news->add();
        $this->assertTrue($result);
    }

    /**
     * 测试添加失败-发布人
     * 1. 期望返回false
     * 2. 设置委办局为NullCrew
     * 3. 调用add,期望返回false
     */
    public function testAddFailCrewNull()
    {
        $this->news->setCrew(new NullCrew());

        $result = $this->news->add();

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('crew', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望编辑
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 INewsAdapter 调用 edit 并且传参 $news 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $news = $this->getMockBuilder(MockNews::class)
                           ->setMethods(['setUpdateTime', 'getRepository'])
                           ->getMock();

        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $news->setCrew($crew);
                   
        //预言修改updateTime
        $news->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 INewsAdapter
        $repository = $this->prophesize(INewsAdapter::class);
        $repository->edit(
            Argument::exact($news),
            Argument::exact(
                [
                    'title',
                    'content',
                    'source',
                    'description',
                    'attachments',
                    'cover',
                    'dimension',
                    'parentCategory',
                    'bannerStatus',
                    'bannerImage',
                    'homePageShowStatus',
                    'status',
                    'stick',
                    'category',
                    'type',
                    'crew',
                    'publishUserGroup',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $news->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $news->edit();
        $this->assertTrue($result);
    }

    public function testEditFailCrewNull()
    {
        $this->news->setCrew(new NullCrew());

        $result = $this->news->edit();

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('crew', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testMove()
    {
        //初始化
        $news = $this->getMockBuilder(MockNews::class)
                           ->setMethods(['setUpdateTime', 'getRepository'])
                           ->getMock();

        //预言修改updateTime
        $news->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 INewsAdapter
        $repository = $this->prophesize(INewsAdapter::class);
        $repository->edit(
            Argument::exact($news),
            Argument::exact(
                [
                    'parentCategory',
                    'category',
                    'type',
                    'crew',
                    'publishUserGroup',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $news->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $news->move();
        $this->assertTrue($result);
    }

    public function testUpdateStatus()
    {
        $news = $this->getMockBuilder(MockNews::class)
            ->setMethods(['getRepository'])
            ->getMock();
            
        $status = IEnableAble::STATUS['ENABLED'];
        $news->setStatus($status);
        $news->setUpdateTime(Core::$container->get('time'));
        $news->setStatusTime(Core::$container->get('time'));

        $repository = $this->prophesize(INewsAdapter::class);

        $repository->edit(
            Argument::exact($news),
            Argument::exact(array(
                'statusTime',
                'status',
                'updateTime',
                'publishUserGroup',
                'crew'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $news->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $news->updateStatus($status);
        
        $this->assertTrue($result);
    }

    public function testUpdateStick()
    {
        $this->new = $this->getMockBuilder(MockNews::class)
            ->setMethods(['getRepository'])
            ->getMock();
            
        $stick = ITopAble::STICK['ENABLED'];
        $this->new->setStick($stick);
        $this->new->setUpdateTime(Core::$container->get('time'));

        $repository = $this->prophesize(INewsAdapter::class);

        $repository->edit(
            Argument::exact($this->new),
            Argument::exact(array(
                'stick',
                'updateTime',
                'publishUserGroup',
                'crew'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->new->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->new->updateStick($stick);
        
        $this->assertTrue($result);
    }

    public function testEnable()
    {
        $newsEnable = $this->getMockBuilder(MockNews::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
        
        // $newsEnable->expects($this->exactly(1))
        //     ->method('updateStatus')
        //     ->with(IEnableAble::STATUS['ENABLED'])
        //     ->willReturn(true);

        $status = IEnableAble::STATUS['ENABLED'];
        $newsEnable->setStatus($status);
        $newsEnable->setUpdateTime(Core::$container->get('time'));
        $newsEnable->setStatusTime(Core::$container->get('time'));

        $repository = $this->prophesize(INewsAdapter::class);

        $repository->edit(
            Argument::exact($newsEnable),
            Argument::exact(array(
                'statusTime',
                'status',
                'updateTime',
                'publishUserGroup',
                'crew'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $newsEnable->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $newsEnable->enable();
        $this->assertTrue($result);
    }

    public function testTop()
    {
        $news = $this->getMockBuilder(MockNews::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $stick = ITopAble::STICK['ENABLED'];
        $news->setStick($stick);
        $news->setUpdateTime(Core::$container->get('time'));

        $repository = $this->prophesize(INewsAdapter::class);

        $repository->edit(
            Argument::exact($news),
            Argument::exact(array(
                'stick',
                'updateTime',
                'publishUserGroup',
                'crew'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $news->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());


        // $news->expects($this->exactly(1))
        //      ->method('updateStick')
        //      ->with($stick)
        //      ->willReturn(true);

        $result = $news->top();
        $this->assertTrue($result);
    }
}
