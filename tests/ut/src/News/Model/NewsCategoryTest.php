<?php
namespace Base\News\Model;

use PHPUnit\Framework\TestCase;

class NewsCategoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new NewsCategory(
            1,
            2,
            3
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetParentCategory()
    {
        $this->assertEquals(1, $this->stub->getParentCategory());
    }

    public function testGetCategory()
    {
        $this->assertEquals(2, $this->stub->getCategory());
    }

    public function testGetType()
    {
        $this->assertEquals(3, $this->stub->getType());
    }
}
