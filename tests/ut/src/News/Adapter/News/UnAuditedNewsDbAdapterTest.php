<?php
namespace Base\News\Adapter\News;

use PHPUnit\Framework\TestCase;

class UnAuditedNewsDbAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new UnAuditedNewsDbAdapter();
        $this->assertInstanceof('Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter', $adapter);
    }
}
