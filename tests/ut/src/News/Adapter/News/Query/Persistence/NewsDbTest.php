<?php
namespace Base\News\Adapter\News\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NewsDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockNewsDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(NewsDb::TABLE, $this->database->getTable());
    }
}
