<?php
namespace Base\News\Adapter\News;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\News\Model\News;
use Base\News\Model\NullNews;
use Base\News\Translator\NewsDbTranslator;
use Base\News\Adapter\News\Query\NewsRowCacheQuery;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class NewsDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(NewsDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchCrew',
                                'fetchPublishUserGroup'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 INewsAdapter
     */
    public function testImplementsINewsAdapter()
    {
        $this->assertInstanceOf(
            'Base\News\Adapter\News\INewsAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 NewsDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockNewsDbAdapter();
        $this->assertInstanceOf(
            'Base\News\Translator\NewsDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 NewsRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockNewsDbAdapter();
        $this->assertInstanceOf(
            'Base\News\Adapter\News\Query\NewsRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetCrewRepository()
    {
        $adapter = new MockNewsDbAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }

    public function testGetUserGroupRepository()
    {
        $adapter = new MockNewsDbAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockNewsDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedNews = new News();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedNews)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedNews);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedNews = new News();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedNews, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedNews, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedNews = new News();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedNews);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchPublishUserGroup')
                         ->with($expectedNews);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedNews);
        //验证
        $news = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedNews, $news);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $newsOne = new News(1);
        $newsTwo = new News(2);

        $ids = [1, 2];

        $expectedNewsList = [];
        $expectedNewsList[$newsOne->getId()] = $newsOne;
        $expectedNewsList[$newsTwo->getId()] = $newsTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedNewsList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedNewsList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchPublishUserGroup')
                         ->with($expectedNewsList);
        //验证
        $newsList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedNewsList, $newsList);
    }

    //fetchPublishUserGroup
    public function testFetchPublishUserGroupIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockNewsDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchPublishUserGroupByObject'
                                ]
                           )
                           ->getMock();
        
        $news = new News();

        $adapter->expects($this->exactly(1))
                         ->method('fetchPublishUserGroupByObject')
                         ->with($news);

        $adapter->fetchPublishUserGroup($news);
    }

    public function testFetchPublishUserGroupIsArray()
    {
        $adapter = $this->getMockBuilder(MockNewsDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchPublishUserGroupByList'
                                ]
                           )
                           ->getMock();
        
        $newsOne = new News(1);
        $newsTwo = new News(2);
        
        $newsList[$newsOne->getId()] = $newsOne;
        $newsList[$newsTwo->getId()] = $newsTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchPublishUserGroupByList')
                         ->with($newsList);

        $adapter->fetchPublishUserGroup($newsList);
    }

    //fetchPublishUserGroupByObject
    public function testFetchPublishUserGroupByObject()
    {
        $adapter = $this->getMockBuilder(MockNewsDbAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $news = new News();
        $news->setPublishUserGroup(new UserGroup(1));
        
        $userGroupId = 1;

        // 为 UserGroup 类建立预言
        $userGroup  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($userGroup);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchPublishUserGroupByObject($news);
    }

    //fetchPublishUserGroupByList
    public function testFetchUserGroupByList()
    {
        $adapter = $this->getMockBuilder(MockNewsDbAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $newsOne = new News(1);
        $userGroupOne = new UserGroup(1);
        $newsOne->setPublishUserGroup($userGroupOne);

        $newsTwo = new News(2);
        $userGroupTwo = new UserGroup(2);
        $newsTwo->setPublishUserGroup($userGroupTwo);
        $userGroupIds = [1, 2];
        
        $userGroupList[$userGroupOne->getId()] = $userGroupOne;
        $userGroupList[$userGroupTwo->getId()] = $userGroupTwo;
        
        $newsList[$newsOne->getId()] = $newsOne;
        $newsList[$newsTwo->getId()] = $newsTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(Argument::exact($userGroupIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($userGroupList);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchPublishUserGroupByList($newsList);
    }

    //fetchCrew
    public function testFetchCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockNewsDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByObject'
                                ]
                           )
                           ->getMock();
        
        $news = new News();

        $adapter->expects($this->exactly(1))
                         ->method('fetchCrewByObject')
                         ->with($news);

        $adapter->fetchCrew($news);
    }

    public function testFetchCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockNewsDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByList'
                                ]
                           )
                           ->getMock();
        
        $newsOne = new News(1);
        $newsTwo = new News(2);
        
        $newsList[$newsOne->getId()] = $newsOne;
        $newsList[$newsTwo->getId()] = $newsTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchCrewByList')
                         ->with($newsList);

        $adapter->fetchCrew($newsList);
    }

    //fetchCrewByObject
    public function testFetchCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockNewsDbAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $news = new News();
        $news->setCrew(new Crew(1));
        
        $crewId = 1;

        // 为 Crew 类建立预言
        $crew  = $this->prophesize(Crew::class);
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $adapter->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByObject($news);
    }

    //fetchCrewByList
    public function testFetchCrewByList()
    {
        $adapter = $this->getMockBuilder(MockNewsDbAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $newsOne = new News(1);
        $crewOne = new Crew(1);
        $newsOne->setCrew($crewOne);

        $newsTwo = new News(2);
        $crewTwo = new Crew(2);
        $newsTwo->setCrew($crewTwo);
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $newsList[$newsOne->getId()] = $newsOne;
        $newsList[$newsTwo->getId()] = $newsTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crewList);

        $adapter->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByList($newsList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-publishUserGroup
    public function testFormatFilterPublishUserGroup()
    {
        $filter = array(
            'publishUserGroup' => 1
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'publish_usergroup_id = '.$filter['publishUserGroup'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-parentCategory
    public function testFormatFilterParentCategory()
    {
        $filter = array(
            'parentCategory' => 1
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'parent_category IN ('.$filter['parentCategory'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-category
    public function testFormatFilterCategory()
    {
        $filter = array(
            'category' => 1
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'category IN ('.$filter['category'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-newsType
    public function testFormatFilterNewsType()
    {
        $filter = array(
            'newsType' => 1
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'type IN ('.$filter['newsType'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-dimension
    public function testFormatFilterDimension()
    {
        $filter = array(
            'dimension' => 1
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'dimension IN ('.$filter['dimension'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-title
    public function testFormatFilterTitle()
    {
        $filter = array(
            'title' => 1
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'title LIKE \'%'.$filter['title'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-status
    public function testFormatFilterStatus()
    {
        $filter = array(
            'status' => -2
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'status = '.$filter['status'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-stick
    public function testFormatFilterStick()
    {
        $filter = array(
            'stick' => 2
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'stick = '.$filter['stick'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-bannerStatus
    public function testFormatFilterBannerStatus()
    {
        $filter = array(
            'bannerStatus' => 2
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'banner_status = '.$filter['bannerStatus'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-homePageShowStatus
    public function testFormatFilterHomePageShowStatus()
    {
        $filter = array(
            'homePageShowStatus' => 2
        );
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = 'home_page_show_status = '.$filter['homePageShowStatus'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--status
    public function testFormatSortStatusDesc()
    {
        $sort = array('status' => -1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-status
    public function testFormatSortStatusAsc()
    {
        $sort = array('status' => 1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--stick
    public function testFormatSortStickDesc()
    {
        $sort = array('stick' => -1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY stick DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-stick
    public function testFormatSortStickAsc()
    {
        $sort = array('stick' => 1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY stick ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--bannerStatus
    public function testFormatSortBannerStatusDesc()
    {
        $sort = array('bannerStatus' => -1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY banner_status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-bannerStatus
    public function testFormatSortBannerStatusAsc()
    {
        $sort = array('bannerStatus' => 1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY banner_status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--homePageShowStatus
    public function testFormatSortHomePageShowStatusDesc()
    {
        $sort = array('homePageShowStatus' => -1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY home_page_show_status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-homePageShowStatus
    public function testFormatSortHomePageShowStatusAsc()
    {
        $sort = array('homePageShowStatus' => 1);
        $adapter = new MockNewsDbAdapter();

        $expectedCondition = ' ORDER BY home_page_show_status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
