<?php
namespace Base\News\WidgetRule;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\News\Model\News;
use Base\News\Model\Banner;

class NewsWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new NewsWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //newsType -- start
    /**
     * @dataProvider invalidNewsTypeProvider
     */
    public function testNewsTypeInvalid($actual, $expected)
    {
        $result = $this->widgetRule->newsType($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidNewsTypeProvider()
    {
        return array(
            array('', false),
            array(NEWS_TYPE['LEADING_GROUP'], true),
            array(999, false),
        );
    }
    //newsType -- end

    //dimension -- start
    /**
     * @dataProvider invalidDimensionProvider
     */
    public function testDimensionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->dimension($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidDimensionProvider()
    {
        return array(
            array('', false),
            array(News::DIMENSIONALITY['SOCIOLOGY'], true),
            array(News::DIMENSIONALITY['GOVERNMENT_AFFAIRS'], true),
            array(999, false),
        );
    }
    //dimension -- end
    
    //homePageShowStatus -- start
    /**
     * @dataProvider invalidHomePageShowStatusProvider
     */
    public function testHomePageShowStatusInvalid($actual, $expected)
    {
        $result = $this->widgetRule->homePageShowStatus($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidHomePageShowStatusProvider()
    {
        return array(
            array('', false),
            array(News::HOME_PAGE_SHOW_STATUS['DISABLED'], true),
            array(News::HOME_PAGE_SHOW_STATUS['ENABLED'], true),
            array(999, false),
        );
    }
    //homePageShowStatus -- end
    
    //bannerStatus -- start
    /**
     * @dataProvider invalidBannerStatusProvider
     */
    public function testBannerStatusInvalid($actual, $expected)
    {
        $result = $this->widgetRule->bannerStatus($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidBannerStatusProvider()
    {
        return array(
            array('', false),
            array(Banner::BANNER_STATUS['DISABLED'], true),
            array(Banner::BANNER_STATUS['ENABLED'], true),
            array(999, false),
        );
    }
    //bannerStatus -- end
}
