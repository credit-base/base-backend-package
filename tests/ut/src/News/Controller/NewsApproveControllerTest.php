<?php
namespace Base\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class NewsApproveControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockNewsApproveController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIApproveAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IApproveAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockNewsApproveController::class)
            ->setMethods(['render'])->getMock();

        $id = 1;
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        $controller->displaySuccess($unAuditedNews);
    }
}
