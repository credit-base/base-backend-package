<?php
namespace Base\News\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\News\Model\Banner;
use Base\News\WidgetRule\NewsWidgetRule;

class NewsControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockNewsControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetNewsWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\News\WidgetRule\NewsWidgetRule',
            $this->trait->getNewsWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\News\Repository\NewsRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetUnAuditedNewsRepository()
    {
        $this->assertInstanceOf(
            'Base\News\Repository\UnAuditedNewsRepository',
            $this->trait->getUnAuditedNewsRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    public function testValidateCommonScenario()
    {
        $controller = $this->getMockBuilder(MockNewsControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule']) ->getMock();

        $crew = 1;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($crew), Argument::exact('crewId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateCommonScenarioPublic($crew);
        
        $this->assertTrue($result);
    }

    public function testValidateMoveScenario()
    {
        $controller = $this->getMockBuilder(MockNewsControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule', 'getNewsWidgetRule']) ->getMock();

        $crew = 1;
        $newsType = 1;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($crew), Argument::exact('crewId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $newsWidgetRule = $this->prophesize(NewsWidgetRule::class);
        $newsWidgetRule->newsType(Argument::exact($newsType))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getNewsWidgetRule')
            ->willReturn($newsWidgetRule->reveal());

        $result = $controller->validateMoveScenarioPublic($newsType, $crew);
        
        $this->assertTrue($result);
    }

    public function testValidateNewsScenario()
    {
        $controller = $this->getMockBuilder(MockNewsControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule', 'getNewsWidgetRule']) ->getMock();

        $id = 1;
        $news = \Base\News\Utils\MockFactory::generateNews($id);
        $title = $news->getTitle();
        $source = $news->getSource();
        $cover = $news->getCover();
        $attachments = $news->getAttachments();
        $content = $news->getContent();
        $newsType = $news->getNewsCategory()->getType();
        $dimension = $news->getDimension();
        $status = $news->getStatus();
        $stick = $news->getStick();
        $bannerStatus = $news->getBanner()->getStatus();
        $bannerImage = Banner::BANNER_STATUS['ENABLED'];
        $homePageShowStatus = $news->getHomePageShowStatus();
        $crew = $news->getCrew()->getId();

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->title(Argument::exact($title))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->source(Argument::exact($source))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->image(
            Argument::exact($cover),
            Argument::exact('cover')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->attachments(Argument::exact($attachments))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->formatString(
            Argument::exact($content),
            Argument::exact('content')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->status(Argument::exact($status))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->stick(Argument::exact($stick))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->image(
            Argument::exact($bannerImage),
            Argument::exact('bannerImage')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->formatNumeric(
            Argument::exact($crew),
            Argument::exact('crewId')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->any())
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $newsWidgetRule = $this->prophesize(NewsWidgetRule::class);
        $newsWidgetRule->newsType(Argument::exact($newsType))->shouldBeCalledTimes(1)->willReturn(true);
        $newsWidgetRule->dimension(Argument::exact($dimension))->shouldBeCalledTimes(1)->willReturn(true);
        $newsWidgetRule->bannerStatus(Argument::exact($bannerStatus))->shouldBeCalledTimes(1)->willReturn(true);
        $newsWidgetRule->homePageShowStatus(
            Argument::exact($homePageShowStatus)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->any())
            ->method('getNewsWidgetRule')
            ->willReturn($newsWidgetRule->reveal());

        $result = $controller->validateNewsScenarioPublic(
            $title,
            $source,
            $cover,
            $attachments,
            $content,
            $newsType,
            $dimension,
            $status,
            $stick,
            $bannerStatus,
            $bannerImage,
            $homePageShowStatus,
            $crew
        );
        
        $this->assertTrue($result);
    }
}
