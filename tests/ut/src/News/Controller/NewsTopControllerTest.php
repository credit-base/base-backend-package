<?php
namespace Base\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\News\Utils\MockFactory;
use Base\News\Repository\NewsRepository;
use Base\News\Repository\UnAuditedNewsRepository;
use Base\News\Command\News\TopNewsCommand;
use Base\News\Command\News\CancelTopNewsCommand;

class NewsTopControllerTest extends TestCase
{
    private $topNewsStub;

    public function setUp()
    {
        $this->topNewsStub = $this->getMockBuilder(NewsTopController::class)
                        ->setMethods([
                            'displayError',
                            ])
                        ->getMock();
    }

    public function teatDown()
    {
        unset($this->topNewsStub);
    }

    private function initialCancelTop($result)
    {
        $this->topNewsStub = $this->getMockBuilder(NewsTopController::class)
                    ->setMethods([
                        'getRequest',
                        'validateCommonScenario',
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = $crewId = 1;

        $data = array(
            'relationships' => array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$crewId)
                    )
                )
            ),
        );

        $this->topNewsStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->willReturn(true);

        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->topNewsStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $command = new CancelTopNewsCommand($crewId, $id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->topNewsStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testCancelTopFailure()
    {
        $command = $this->initialCancelTop(false);

        $this->topNewsStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->topNewsStub->cancelTop($command->id);
        $this->assertFalse($result);
    }

    public function testCancelTopSuccess()
    {
        $command = $this->initialCancelTop(true);

        $news = MockFactory::generateNews($command->id);

        $repository = $this->prophesize(NewsRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($news);
        $this->topNewsStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->topNewsStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->topNewsStub->cancelTop($command->id);
        $this->assertTrue($result);
    }

    private function initialTop($result)
    {
        $this->topNewsStub = $this->getMockBuilder(NewsTopController::class)
                    ->setMethods([
                        'getRequest',
                        'validateCommonScenario',
                        'getCommandBus',
                        'getUnAuditedNewsRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $crewId = 2;

        $data = array(
            'relationships' => array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$crewId)
                    )
                )
            ),
        );

        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->topNewsStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->topNewsStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->willReturn(true);
            
        $command = new TopNewsCommand($id, $crewId);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->topNewsStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testTopFailure()
    {
        $command = $this->initialTop(false);

        $this->topNewsStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->topNewsStub->top($command->newsId);
        $this->assertFalse($result);
    }

    public function testTopSuccess()
    {
        $command = $this->initialTop(true);

        $news = MockFactory::generateUnAuditedNews($command->newsId);

        $repository = $this->prophesize(UnAuditedNewsRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($news);

        $this->topNewsStub->expects($this->exactly(1))
             ->method('getUnAuditedNewsRepository')
             ->willReturn($repository->reveal());
             
        $this->topNewsStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->topNewsStub->top($command->newsId);
        $this->assertTrue($result);
    }
}
