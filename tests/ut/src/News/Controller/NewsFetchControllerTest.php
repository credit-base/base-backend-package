<?php
namespace Base\News\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\News\Adapter\News\INewsAdapter;
use Base\News\Model\NullNews;
use Base\News\Model\News;
use Base\News\View\NewsView;

class NewsFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(NewsFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockNewsFetchController();

        $this->assertInstanceOf(
            'Base\News\Adapter\News\INewsAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockNewsFetchController();

        $this->assertInstanceOf(
            'Base\News\View\NewsView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockNewsFetchController();

        $this->assertEquals(
            'news',
            $controller->getResourceName()
        );
    }
}
