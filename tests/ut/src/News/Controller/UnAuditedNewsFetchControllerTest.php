<?php
namespace Base\News\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\News\Model\UnAuditedNews;
use Base\News\Repository\UnAuditedNewsRepository;

class UnAuditedNewsFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(UnAuditedNewsFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockUnAuditedNewsFetchController();

        $this->assertInstanceOf(
            'Base\News\Repository\UnAuditedNewsRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockUnAuditedNewsFetchController();

        $this->assertInstanceOf(
            'Base\News\View\UnAuditedNewsView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockUnAuditedNewsFetchController();

        $this->assertEquals(
            'unAuditedNews',
            $controller->getResourceName()
        );
    }

    private function initialFilter($unAuditedNewsArray)
    {
        $this->stub = $this->getMockBuilder(MockUnAuditedNewsFetchController::class)
                    ->setMethods(['getRepository', 'renderView', 'displayError', 'formatParameters'])
                    ->getMock();

        $sort = array();
        $curpage = 1;
        $perpage = 20;
        $filter['applyInfoCategory'] = UnAuditedNews::APPLY_NEWS_CATEGORY;

        $this->stub->expects($this->any())
        ->method('formatParameters')
        ->willReturn([$filter, $sort, $curpage, $perpage]);

        $repository = $this->prophesize(UnAuditedNewsRepository::class);

        $repository->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(($curpage-1)*$perpage),
            Argument::exact($perpage)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedNewsArray);

        $this->stub->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());
    }
 
    public function testFilterSuccess()
    {
        $unAuditedNewsArray = array(
            array(\Base\News\Utils\MockFactory::generateUnAuditedNews(1)),
            1
        );

        $this->initialFilter($unAuditedNewsArray);
        
        $this->stub->expects($this->exactly(1))
        ->method('renderView')
        ->willReturn(true);
        
        $result = $this->stub->filter();

        $this->assertTrue($result);
    }

    public function testFilterFailure()
    {
        $unAuditedNewsArray = array(
            array(),
            0
        );

        $this->initialFilter($unAuditedNewsArray);
        
        $this->stub->expects($this->exactly(1))
        ->method('displayError')
        ->willReturn(false);
        
        $result = $this->stub->filter();

        $this->assertFalse($result);
    }
}
