<?php
namespace Base\News\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\News\Model\UnAuditedNews;
use Base\News\Command\News\ResubmitNewsCommand;
use Base\News\Repository\UnAuditedNewsRepository;

class NewsResubmitControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new NewsResubmitController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIResubmitAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IResubmitAbleController',
            $this->controller
        );
    }

    private function newsData() : array
    {
        return array(
            "type"=>"news",
            "attributes"=>array(
                "title"=>"新闻标题",
                "source"=>"新闻来源",
                "cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
                "attachments"=>array(
                    array('name' => '附件名称', 'identify' => '附件地址.doc'),
                ),
                "content"=>"新闻内容",
                "newsType"=>1,
                "dimension"=>1,
                "status"=>0,
                "stick"=>2,
                "bannerStatus"=>2,
                "bannerImage"=>array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
                "homePageShowStatus"=>2,
            ),
            "relationships"=>array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>2)
                    )
                )
            )
        );
    }
    
    /**
     * 测试 resubmit 成功
     * 1. 为 NewsResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getNewsWidgetRule、getCommandBus、getUnAuditedNewsRepository、render方法
     * 2. 调用 $this->initResubmit(), 期望结果为 true
     * 3. 为 News 类建立预言
     * 4. 为 UnAuditedNewsRepository 类建立预言, UnAuditedNewsRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的News, getUnAuditedNewsRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->resubmit 方法被调用一次, 且返回结果为 true
     */
    public function testResubmit()
    {
        // 为 NewsResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getNewsWidgetRule、getCommandBus、getUnAuditedNewsRepository、render方法
        $controller = $this->getMockBuilder(NewsResubmitController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateNewsScenario',
                    'getCommandBus',
                    'getUnAuditedNewsRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;

        $this->initResubmit($controller, $id, true);

        // 为 News 类建立预言
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);

        // 为 UnAuditedNewsRepository 类建立预言, UnAuditedNewsRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的News, getUnAuditedNewsRepository 方法被调用一次
        $repository = $this->prophesize(UnAuditedNewsRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedNews);
        $controller->expects($this->once())
                             ->method('getUnAuditedNewsRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->resubmit 方法被调用一次, 且返回结果为 true
        $result = $controller->resubmit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 resubmit 失败
     * 1. 为 NewsResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getNewsWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initResubmit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->resubmit 方法被调用一次, 且返回结果为 false
     */
    public function testResubmitFail()
    {
        // 为 NewsResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getNewsWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(NewsResubmitController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateNewsScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        $id = 1;

        // 调用 $this->initResubmit(), 期望结果为 false
        $this->initResubmit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->resubmit 方法被调用一次, 且返回结果为 false
        $result = $controller->resubmit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 resubmit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 NewsWidgetRule 类建立预言, 验证请求参数, getNewsWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 ResubmitNewsCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initResubmit(NewsResubmitController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->newsData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $cover = $attributes['cover'];
        $dimension = $attributes['dimension'];
        $attachments = $attributes['attachments'];
        $content = $attributes['content'];
        $newsType = $attributes['newsType'];
        $stick = $attributes['stick'];
        $status = $attributes['status'];
        $bannerStatus = $attributes['bannerStatus'];
        $bannerImage = $attributes['bannerImage'];
        $homePageShowStatus = $attributes['homePageShowStatus'];

        $crew = $relationships['crew']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateNewsScenario')
            ->willReturn(true);

        $command = new ResubmitNewsCommand(
            $title,
            $source,
            $cover,
            $attachments,
            $content,
            $newsType,
            $dimension,
            $status,
            $stick,
            $bannerStatus,
            $bannerImage,
            $homePageShowStatus,
            $crew,
            $id
        );
        // 为 CommandBus 类建立预言, 传入 ResubmitNewsCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }
}
