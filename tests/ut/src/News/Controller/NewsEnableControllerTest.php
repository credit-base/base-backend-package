<?php
namespace Base\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\News\Utils\MockFactory;
use Base\News\Repository\NewsRepository;
use Base\News\Repository\UnAuditedNewsRepository;
use Base\News\Command\News\EnableNewsCommand;
use Base\News\Command\News\DisableNewsCommand;

class NewsEnableControllerTest extends TestCase
{
    private $newsStub;

    public function setUp()
    {
        $this->newsStub = $this->getMockBuilder(NewsEnableController::class)
                        ->setMethods([
                            'displayError',
                            ])
                        ->getMock();
    }

    public function teatDown()
    {
        unset($this->newsStub);
    }

    private function initialDisable($result)
    {
        $this->newsStub = $this->getMockBuilder(NewsEnableController::class)
                    ->setMethods([
                        'getRequest',
                        'validateCommonScenario',
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = $crewId = 1;

        $data = array(
            'relationships' => array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$crewId)
                    )
                )
            ),
        );

        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->newsStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->newsStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->willReturn(true);

        $command = new DisableNewsCommand($crewId, $id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->newsStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDisableFailure()
    {
        $command = $this->initialDisable(false);

        $this->newsStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->newsStub->disable($command->id);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $command = $this->initialDisable(true);

        $news = MockFactory::generateNews($command->id);

        $repository = $this->prophesize(NewsRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($news);
        $this->newsStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->newsStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->newsStub->disable($command->id);
        $this->assertTrue($result);
    }

    private function initialEnable($result)
    {
        $this->newsStub = $this->getMockBuilder(NewsEnableController::class)
                    ->setMethods([
                        'getRequest',
                        'validateCommonScenario',
                        'getCommandBus',
                        'getUnAuditedNewsRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $crewId = 2;

        $data = array(
            'relationships' => array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$crewId)
                    )
                )
            ),
        );

        $this->newsStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->willReturn(true);
            
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->newsStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $command = new EnableNewsCommand($id, $crewId);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->newsStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testEnableFailure()
    {
        $command = $this->initialEnable(false);

        $this->newsStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->newsStub->enable($command->newsId);
        $this->assertFalse($result);
    }

    public function testEnableSuccess()
    {
        $command = $this->initialEnable(true);

        $news = MockFactory::generateUnAuditedNews($command->newsId);

        $repository = $this->prophesize(UnAuditedNewsRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($news);

        $this->newsStub->expects($this->exactly(1))
             ->method('getUnAuditedNewsRepository')
             ->willReturn($repository->reveal());
             
        $this->newsStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->newsStub->enable($command->newsId);
        $this->assertTrue($result);
    }
}
