<?php
namespace Base\News\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\News\Model\UnAuditedNews;
use Base\News\Command\News\AddNewsCommand;
use Base\News\Command\News\EditNewsCommand;
use Base\News\Command\News\MoveNewsCommand;
use Base\News\Repository\UnAuditedNewsRepository;

class NewsOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new NewsOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    private function newsData() : array
    {
        return array(
            "type"=>"news",
            "attributes"=>array(
                "title"=>"新闻标题",
                "source"=>"新闻来源",
                "cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
                "attachments"=>array(
                    array('name' => '附件名称', 'identify' => '附件地址.doc'),
                ),
                "content"=>"新闻内容",
                "newsType"=>2,
                "dimension"=>2,
                "status"=>-2,
                "stick"=>2,
                "bannerStatus"=>2,
                "bannerImage"=>array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
                "homePageShowStatus"=>2,
            ),
            "relationships"=>array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>1)
                    )
                )
            )
        );
    }
    /**
     * 测试 add 成功
     * 1. 为 NewsOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getNewsWidgetRule、getCommandBus、getUnAuditedNewsRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 News 类建立预言
     * 4. 为 UnAuditedNewsRepository 类建立预言, UnAuditedNewsRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的News, getUnAuditedNewsRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 为 NewsOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getNewsWidgetRule、getCommandBus、getUnAuditedNewsRepository、render方法
        $controller = $this->getMockBuilder(NewsOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateNewsScenario',
                    'getCommandBus',
                    'getUnAuditedNewsRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 true
        $command = $this->initAdd($controller, true);

        // 为 News 类建立预言
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($command->id);

        // 为 UnAuditedNewsRepository 类建立预言, UnAuditedNewsRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的News, getUnAuditedNewsRepository 方法被调用一次
        $repository = $this->prophesize(UnAuditedNewsRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedNews);
        $controller->expects($this->once())
                             ->method('getUnAuditedNewsRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->add 方法被调用一次, 且返回结果为 true
        $result = $controller->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 NewsOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getNewsWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 为 NewsOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getNewsWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(NewsOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateNewsScenario',
                    'getCommandBus',
                    'getUnAuditedNewsRepository',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd($controller, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->add 方法被调用一次, 且返回结果为 false
        $result = $controller->add();
        $this->assertFalse($result);
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 NewsWidgetRule 类建立预言, 验证请求参数, getNewsWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddNewsCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(NewsOperateController $controller, bool $result)
    {
        // mock 请求参数
        $data = $this->newsData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $cover = $attributes['cover'];
        $attachments = $attributes['attachments'];
        $content = $attributes['content'];
        $newsType = $attributes['newsType'];
        $dimension = $attributes['dimension'];
        $status = $attributes['status'];
        $stick = $attributes['stick'];
        $bannerStatus = $attributes['bannerStatus'];
        $bannerImage = $attributes['bannerImage'];
        $homePageShowStatus = $attributes['homePageShowStatus'];

        $crew = $relationships['crew']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateNewsScenario')
            ->willReturn(true);

        $command = new AddNewsCommand(
            $title,
            $source,
            $cover,
            $attachments,
            $content,
            $newsType,
            $dimension,
            $status,
            $stick,
            $bannerStatus,
            $bannerImage,
            $homePageShowStatus,
            $crew
        );
        // 为 CommandBus 类建立预言, 传入 AddNewsCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
            
        return $command;
    }

    /**
     * 测试 edit 成功
     * 1. 为 NewsOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getNewsWidgetRule、getCommandBus、getUnAuditedNewsRepository、render方法
     * 2. 调用 $this->initEdit(), 期望结果为 true
     * 3. 为 News 类建立预言
     * 4. 为 UnAuditedNewsRepository 类建立预言, UnAuditedNewsRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的News, getUnAuditedNewsRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 NewsOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getNewsWidgetRule、getCommandBus、getUnAuditedNewsRepository、render方法
        $controller = $this->getMockBuilder(NewsOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateNewsScenario',
                    'getCommandBus',
                    'getUnAuditedNewsRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;

        $command = $this->initEdit($controller, $id, true);

        // 为 News 类建立预言
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($command->id);

        // 为 UnAuditedNewsRepository 类建立预言, UnAuditedNewsRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的News, getUnAuditedNewsRepository 方法被调用一次
        $repository = $this->prophesize(UnAuditedNewsRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedNews);
        $controller->expects($this->once())
                             ->method('getUnAuditedNewsRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->edit 方法被调用一次, 且返回结果为 true
        $result = $controller->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 NewsOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getNewsWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initEdit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 NewsOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getNewsWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(NewsOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateNewsScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 false
        $this->initEdit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->edit 方法被调用一次, 且返回结果为 false
        $result = $controller->edit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 edit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 NewsWidgetRule 类建立预言, 验证请求参数, getNewsWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 EditNewsCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEdit(NewsOperateController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->newsData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $cover = $attributes['cover'];
        $attachments = $attributes['attachments'];
        $dimension = $attributes['dimension'];
        $content = $attributes['content'];
        $newsType = $attributes['newsType'];
        $status = $attributes['status'];
        $stick = $attributes['stick'];
        $bannerStatus = $attributes['bannerStatus'];
        $bannerImage = $attributes['bannerImage'];
        $homePageShowStatus = $attributes['homePageShowStatus'];

        $crew = $relationships['crew']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateNewsScenario')
            ->willReturn(true);

        $command = new EditNewsCommand(
            $title,
            $source,
            $cover,
            $attachments,
            $content,
            $newsType,
            $dimension,
            $status,
            $stick,
            $bannerStatus,
            $bannerImage,
            $homePageShowStatus,
            $id,
            $crew
        );
        // 为 CommandBus 类建立预言, 传入 EditNewsCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        return $command;
    }

    private function initialMove($result)
    {
        $this->newsStub = $this->getMockBuilder(NewsOperateController::class)
                    ->setMethods([
                        'getRequest',
                        'validateMoveScenario',
                        'getCommandBus',
                        'getUnAuditedNewsRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $crewId = $newsType = 2;

        $data = array(
            'relationships' => array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$crewId)
                    )
                )
            ),
        );

        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->newsStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->newsStub->expects($this->exactly(1))
            ->method('validateMoveScenario')
            ->willReturn(true);
            
        $command = new MoveNewsCommand($newsType, $id, $crewId);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->newsStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testMoveFailure()
    {
        $command = $this->initialMove(false);

        $this->newsStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->newsStub->move($command->newsId, $command->newsType);
        $this->assertFalse($result);
    }

    public function testMoveSuccess()
    {
        $command = $this->initialMove(true);

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($command->newsId);

        $repository = $this->prophesize(UnAuditedNewsRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedNews);

        $this->newsStub->expects($this->exactly(1))
             ->method('getUnAuditedNewsRepository')
             ->willReturn($repository->reveal());
             
        $this->newsStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->newsStub->move($command->newsId, $command->newsType);
        $this->assertTrue($result);
    }
}
