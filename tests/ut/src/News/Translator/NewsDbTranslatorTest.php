<?php
namespace Base\News\Translator;

use PHPUnit\Framework\TestCase;

use Base\News\Utils\NewsUtils;
use Base\News\Utils\MockFactory;

class NewsDbTranslatorTest extends TestCase
{
    use NewsUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new NewsDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\News\Model\NullNews', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $news = MockFactory::generateNews(1);

        $expression['news_id'] = $news->getId();
        $expression['title'] = $news->getTitle();
        $expression['source'] = $news->getSource();
        $expression['cover'] = $news->getCover();
        $expression['attachments'] = $news->getAttachments();
        $expression['content'] = $news->getContent();
        $expression['description'] = $news->getDescription();
        $expression['parent_category'] = $news->getNewsCategory()->getParentCategory();
        $expression['category'] = $news->getNewsCategory()->getCategory();
        $expression['type'] = $news->getNewsCategory()->getType();
        $expression['crew_id'] = $news->getCrew()->getId();
        $expression['dimension'] = $news->getDimension();
        $expression['publish_usergroup_id'] = $news->getPublishUserGroup()->getId();
        $expression['banner_image'] = $news->getBanner()->getImage();
        $expression['banner_status'] = $news->getBanner()->getStatus();
        $expression['home_page_show_status'] = $news->getHomePageShowStatus();
        $expression['stick'] = $news->getStick();

        $expression['status'] = $news->getStatus();
        $expression['status_time'] = $news->getStatusTime();
        $expression['create_time'] = $news->getCreateTime();
        $expression['update_time'] = $news->getUpdateTime();

        $news = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\News\Model\News', $news);
        $this->compareArrayAndObjectNews($expression, $news);
    }

    public function testObjectToArray()
    {
        $news = MockFactory::generateNews(1);

        $expression = $this->translator->objectToArray($news);

        $this->compareArrayAndObjectNews($expression, $news);
    }
}
