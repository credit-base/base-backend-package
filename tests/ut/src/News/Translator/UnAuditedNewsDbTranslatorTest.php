<?php
namespace Base\News\Translator;

use PHPUnit\Framework\TestCase;

use Base\News\Utils\NewsUtils;

class UnAuditedNewsDbTranslatorTest extends TestCase
{
    use NewsUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditedNewsDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testExtendsApplyFormDbTranslator()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Translator\ApplyFormDbTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews(1);

        $expression['apply_form_id'] = $unAuditedNews->getId();
        $expression['news_id'] = $unAuditedNews->getId();
        $expression['title'] = $unAuditedNews->getApplyTitle();
        $expression['relation_id'] = $unAuditedNews->getRelation()->getId();
        $expression['operation_type'] = $unAuditedNews->getOperationType();
        $expression['apply_info_category'] = $unAuditedNews->getApplyInfoCategory()->getCategory();
        $expression['apply_info_type'] = $unAuditedNews->getApplyInfoCategory()->getType();
        $expression['reject_reason'] = $unAuditedNews->getRejectReason();
        $expression['apply_info'] = base64_encode(gzcompress(serialize($unAuditedNews->getApplyInfo())));
        $expression['apply_crew_id'] = $unAuditedNews->getApplyCrew()->getId();
        $expression['apply_usergroup_id'] = $unAuditedNews->getApplyUserGroup()->getId();
        $expression['apply_status'] = $unAuditedNews->getApplyStatus();
        $expression['status_time'] = $unAuditedNews->getStatusTime();
        $expression['create_time'] = $unAuditedNews->getCreateTime();
        $expression['update_time'] = $unAuditedNews->getUpdateTime();

        $unAuditedNews = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\News\Model\UnAuditedNews', $unAuditedNews);
        $this->compareArrayAndObjectUnAuditedNews($expression, $unAuditedNews);
    }

    public function testArrayToObjects()
    {
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews(1);

        $expression['news_id'] = $unAuditedNews->getApplyId();
        $expression['title'] = $unAuditedNews->getTitle();
        $expression['source'] = $unAuditedNews->getSource();
        $expression['cover'] = json_encode($unAuditedNews->getCover());
        $expression['attachments'] = json_encode($unAuditedNews->getAttachments());
        $expression['content'] = $unAuditedNews->getContent();
        $expression['description'] = $unAuditedNews->getDescription();
        $expression['parent_category'] = $unAuditedNews->getNewsCategory()->getParentCategory();
        $expression['category'] = $unAuditedNews->getNewsCategory()->getCategory();
        $expression['type'] = $unAuditedNews->getNewsCategory()->getType();
        $expression['crew_id'] = $unAuditedNews->getCrew()->getId();
        $expression['dimension'] = $unAuditedNews->getDimension();
        $expression['publish_usergroup_id'] = $unAuditedNews->getPublishUserGroup()->getId();
        $expression['banner_image'] = json_encode($unAuditedNews->getBanner()->getImage());
        $expression['banner_status'] = $unAuditedNews->getBanner()->getStatus();
        $expression['home_page_show_status'] = $unAuditedNews->getHomePageShowStatus();
        $expression['stick'] = $unAuditedNews->getStick();
        $expression['status'] = $unAuditedNews->getStatus();
        $expression['status_time'] = $unAuditedNews->getStatusTime();
        $expression['create_time'] = $unAuditedNews->getCreateTime();
        $expression['update_time'] = $unAuditedNews->getUpdateTime();

        $unAuditedNews = $this->translator->arrayToObjects($expression, $unAuditedNews);

        $this->assertInstanceof('Base\News\Model\UnAuditedNews', $unAuditedNews);
        $this->compareArrayAndObjectsUnAuditedNews($expression, $unAuditedNews);
    }

    public function testObjectToArray()
    {
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews(1);

        $expression = $this->translator->objectToArray($unAuditedNews);

        $this->compareArrayAndObjectUnAuditedNews($expression, $unAuditedNews);
    }
}
