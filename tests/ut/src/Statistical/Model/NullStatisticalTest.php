<?php
namespace Base\Statistical\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullStatisticalTest extends TestCase
{
    private $nullStatistical;

    public function setUp()
    {
        $this->nullStatistical = NullStatistical::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullStatistical);
    }

    public function testExtendsStatistical()
    {
        $this->assertInstanceof('Base\Statistical\Model\Statistical', $this->nullStatistical);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->nullStatistical);
    }
}
