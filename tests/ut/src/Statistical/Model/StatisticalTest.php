<?php
namespace Base\Statistical\Model;

use PHPUnit\Framework\TestCase;

class StatisticalTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new Statistical();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    /**
     * Statistical 领域对象,测试构造函数
     */
    public function testStatisticalConstructor()
    {
        //测试初始化id
        $this->assertEquals(0, $this->stub->getId());

        //测试初始化统计结果
        $this->assertArraySubset(array(), $this->stub->getResult());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Statistical setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Statistical setId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdWrongType()
    {
        $this->stub->setId('string');
    }
    //id 测试 ----------------------------------------------------------   end

    //result 测试 ---------------------------------------------------------- start
    /**
     * 设置 Statistical setResult() 正确的传参类型,期望传值正确
     */
    public function testSetResultCorrectType()
    {
        $this->stub->setResult(array());
        $this->assertEquals(array(), $this->stub->getResult());
    }

    /**
     * 设置 Statistical setResult() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetResultWrongType()
    {
        $this->stub->setResult('string');
    }
    //result 测试 ----------------------------------------------------------   end
}
