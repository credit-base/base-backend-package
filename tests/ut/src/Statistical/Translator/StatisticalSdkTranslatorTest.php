<?php
namespace Base\Statistical\Translator;

use PHPUnit\Framework\TestCase;

use Base\Statistical\Model\NullStatistical;
use Base\Statistical\Utils\StatisticalSdkUtils;

use BaseSdk\Statistical\Model\Statistical as StatisticalSdk;
use BaseSdk\Statistical\Model\NullStatistical as NullStatisticalSdk;

class StatisticalSdkTranslatorTest extends TestCase
{
    use StatisticalSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new StatisticalSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $statistical = \Base\Statistical\Utils\MockFactory::generateStatistical(1);

        $statisticalSdk = new StatisticalSdk(
            $statistical->getId(),
            $statistical->getResult()
        );

        $statisticalObject = $this->translator->valueObjectToObject($statisticalSdk);
        $this->assertInstanceof('Base\Statistical\Model\Statistical', $statisticalObject);
        $this->compareValueObjectAndObject($statisticalSdk, $statisticalObject);
    }

    public function testValueObjectToObjectFail()
    {
        $statisticalSdk = NullStatisticalSdk::getInstance();

        $statistical = $this->translator->valueObjectToObject($statisticalSdk);
        $this->assertInstanceof('Base\Statistical\Model\NullStatistical', $statistical);
    }

    public function testObjectToValueObject()
    {
        $statisticalSdk = NullStatisticalSdk::getInstance();

        $result = $this->translator->objectToValueObject($statisticalSdk);

        $this->assertEquals(
            $statisticalSdk,
            $result
        );
    }
}
