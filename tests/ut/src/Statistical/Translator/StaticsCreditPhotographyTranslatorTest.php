<?php
namespace Base\Statistical\Translator;

use PHPUnit\Framework\TestCase;

use Base\Common\Model\IApproveAble;

use Base\Statistical\Model\Statistical;

class StaticsCreditPhotographyTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new StaticsCreditPhotographyTranslator();
        parent::setUp();
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    /**
     * 测试翻译数组转换对象
     */
    public function testArrayToObjectCorrectArray()
    {
        $faker = \Faker\Factory::create('zh_CN');
        //保证每次生成数据一致
        $faker->seed(1000);

        $expression = array();

        $expression = array(
            array('apply_status' => IApproveAble::APPLY_STATUS['PENDING'], 'count(*)' => $faker->randomDigit()),
            array('apply_status' => IApproveAble::APPLY_STATUS['APPROVE'], 'count(*)' => $faker->randomDigit()),
            array('apply_status' => IApproveAble::APPLY_STATUS['REJECT'], 'count(*)' => $faker->randomDigit())
        );

        $statistical = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Statistical\Model\Statistical', $statistical);

        $result = $statistical->getResult();

        $this->assertEquals($expression[0]['count(*)'], $result['pendingTotal']);
        $this->assertEquals($expression[1]['count(*)'], $result['approveTotal']);
        $this->assertEquals($expression[2]['count(*)'], $result['rejectTotal']);
    }

    public function testObjectToArray()
    {
        $statistical = array();
        $keys = array();

        $result = $this->translator->objectToArray($statistical, $keys);
        $this->assertEquals(array(), $result);
    }
}
