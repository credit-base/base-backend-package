<?php
namespace Base\Statistical\Controller;

use PHPUnit\Framework\TestCase;

class IAdapterFactoryTest extends TestCase
{
    public function testStaticsCreditPhotography()
    {
        $adapter = new IAdapterFactory();
        $this->assertInstanceOf(
            'Base\Statistical\Adapter\Statistical\StaticsCreditPhotographyAdapter',
            $adapter->getAdapter('staticsCreditPhotography')
        );
    }

    public function testStaticsServiceAuthenticationCount()
    {
        $adapter = new IAdapterFactory();
        $this->assertInstanceOf(
            'Base\Statistical\Adapter\Statistical\NullStatisticalAdapter',
            $adapter->getAdapter('test')
        );
    }
}
