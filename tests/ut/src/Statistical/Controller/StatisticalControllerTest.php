<?php
namespace Base\Statistical\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Statistical\Model\Statistical;
use Base\Statistical\Model\NullStatistical;
use Base\Statistical\Repository\StatisticalRepository;

class StatisticalControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockStatisticalController();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetStatisticalRepository()
    {
        $type = '';

        $this->assertInstanceOf(
            'Base\Statistical\Repository\StatisticalRepository',
            $this->stub->getStatisticalRepository($type)
        );
    }

    public function testAnalyseFail()
    {
        $controller = $this->getMockBuilder(MockStatisticalController::class)
                           ->setMethods(
                               ['getStatisticalRepository', 'displayError', 'formatParameters']
                           )
                           ->getMock();

        //初始化
        $filter = ['filter'];
        $sort = ['sort'];
        $curpage = 1;
        $perpage = 20;
        $statistical = new NullStatistical();
        $type = 'type';

        //预言
        $repository = $this->prophesize(StatisticalRepository::class);
        $repository->analyse($filter)->shouldBeCalledTimes(1)->willReturn($statistical);

        //绑定
        $controller->expects($this->exactly(1))
                         ->method('getStatisticalRepository')
                         ->willReturn($repository->reveal());

        $controller->expects($this->exactly(1))
                         ->method('formatParameters')
                         ->willReturn([$filter, $sort, $curpage, $perpage]);
                         
        $controller->expects($this->exactly(1))
                         ->method('displayError');

        //验证
        $result = $controller->analyse($type);
        $this->assertFalse($result);
    }

    public function testAnalyse()
    {
        $controller = $this->getMockBuilder(MockStatisticalController::class)
                           ->setMethods(
                               ['getStatisticalRepository', 'renderView', 'formatParameters']
                           )
                           ->getMock();

        //初始化
        $filter = ['filter'];
        $sort = ['sort'];
        $curpage = 1;
        $perpage = 20;
        $statistical = new Statistical();
        $type = 'type';

        //预言
        $repository = $this->prophesize(StatisticalRepository::class);
        $repository->analyse($filter)->shouldBeCalledTimes(1)->willReturn($statistical);

        //绑定
        $controller->expects($this->exactly(1))
                         ->method('getStatisticalRepository')
                         ->willReturn($repository->reveal());

        $controller->expects($this->exactly(1))
                         ->method('formatParameters')
                         ->willReturn([$filter, $sort, $curpage, $perpage]);
                         
        $controller->expects($this->exactly(1))
                         ->method('renderView');

        //验证
        $result = $controller->analyse($type);
        $this->assertTrue($result);
    }
}
