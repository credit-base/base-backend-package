<?php
namespace Base\Statistical\Adapter\Statistical;

use PHPUnit\Framework\TestCase;

use Base\Common\Translator\ISdkTranslator;

use Base\Statistical\Model\NullStatistical;

use BaseSdk\Statistical\Model\Statistical as StatisticalSdk;
use BaseSdk\Statistical\Adapter\Statistical\IStatisticalAdapter;
use BaseSdk\Statistical\Model\NullStatistical as NullStatisticalSdk;

class EnterpriseRelationInformationCountSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new EnterpriseRelationInformationCountSdkAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsStatisticalSdkAdapter()
    {
        $this->assertInstanceOf(
            'Base\Statistical\Adapter\Statistical\StatisticalSdkAdapter',
            $this->adapter
        );
    }

    public function testGetRestfulAdapter()
    {
        $adapter = new MockEnterpriseRelationInformationCountSdkAdapter();

        $this->assertInstanceOf(
            'BaseSdk\Statistical\Adapter\Statistical\IStatisticalAdapter',
            $adapter->getRestfulAdapter()
        );
        
        $this->assertInstanceOf(
            'BaseSdk\Statistical\Adapter\Statistical\EnterpriseRelationInformationCountRestfulAdapter',
            $adapter->getRestfulAdapter()
        );
    }

    public function testAnalysePublic()
    {
        $filter = array('filter');
        $statistical = \Base\Statistical\Utils\MockFactory::generateStatistical(1);
        $statisticalSdk = new StatisticalSdk();

        $adapter = $this->getMockBuilder(EnterpriseRelationInformationCountSdkAdapter::class)
                      ->setMethods(['getRestfulAdapter', 'getTranslator'])
                      ->getMock();

        $restfulAdapter = $this->prophesize(IStatisticalAdapter::class);
        $restfulAdapter->analyse($filter)->shouldBeCalledTimes(1)->willReturn($statisticalSdk);
        $adapter->expects($this->exactly(1))->method('getRestfulAdapter')->willReturn($restfulAdapter->reveal());

        $translator = $this->prophesize(ISdkTranslator::class);
        $translator->valueObjectToObject($statisticalSdk)->shouldBeCalledTimes(1)->willReturn($statistical);
        $adapter->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $result = $adapter->analyse($filter);
        $this->assertEquals($statistical, $result);
    }

    public function testAnalysePublicFail()
    {
        $filter = array('filter');
        $nullStatistical = NullStatistical::getInstance();
        $nullStatisticalSdk = NullStatisticalSdk::getInstance();

        $adapter = $this->getMockBuilder(EnterpriseRelationInformationCountSdkAdapter::class)
                      ->setMethods(['getRestfulAdapter'])
                      ->getMock();

        $restfulAdapter = $this->prophesize(IStatisticalAdapter::class);
        $restfulAdapter->analyse($filter)->shouldBeCalledTimes(1)->willReturn($nullStatisticalSdk);
        $adapter->expects($this->exactly(1))->method('getRestfulAdapter')->willReturn($restfulAdapter->reveal());

        $result = $adapter->analyse($filter);
        $this->assertEquals($nullStatistical, $result);
    }
}
