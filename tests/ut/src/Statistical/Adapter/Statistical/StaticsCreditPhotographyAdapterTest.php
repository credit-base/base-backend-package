<?php
namespace Base\Statistical\Adapter\Statistical;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Statistical\Model\Statistical;
use Base\Statistical\Model\NullStatistical;
use Base\Statistical\Translator\StaticsCreditPhotographyTranslator;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;
use Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence\CreditPhotographyDb;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class StaticsCreditPhotographyAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockStaticsCreditPhotographyAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIStatisticalAdapter()
    {
        $this->assertInstanceOf(
            'Base\Statistical\Adapter\Statistical\IStatisticalAdapter',
            $this->adapter
        );
    }

    public function testGetStaticsCreditPhotographyTranslator()
    {
        $this->assertInstanceOf(
            'Base\Statistical\Translator\StaticsCreditPhotographyTranslator',
            $this->adapter->getStaticsCreditPhotographyTranslator()
        );
    }

    public function testGetCreditPhotographyDb()
    {
        $this->assertInstanceOf(
            'Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence\CreditPhotographyDb',
            $this->adapter->getCreditPhotographyDb()
        );
    }

    public function testGetCreditPhotographyDbTranslator()
    {
        $this->assertInstanceOf(
            'Base\CreditPhotography\Translator\CreditPhotographyDbTranslator',
            $this->adapter->getCreditPhotographyDbTranslator()
        );
    }

    private function initialAnalyse($info, $filter)
    {
        $this->adapter = $this->getMockBuilder(MockStaticsCreditPhotographyAdapter::class)
                           ->setMethods([
                                'getStaticsCreditPhotographyTranslator',
                                'formatFilter',
                                'formatGroupBy',
                                'getCreditPhotographyDb'
                            ])->getMock();

        $condition = 'member_id IN ('.$filter['memberId'].')';
        $conditionGroupBy = ' GROUP BY apply_status';
        $groupByKey = 'apply_status';

        $this->adapter->expects($this->once())->method('formatFilter')->willReturn($condition);
        $this->adapter->expects($this->once())->method('formatGroupBy')->willReturn([$conditionGroupBy, $groupByKey]);

        $condition .= $conditionGroupBy;

        $creditPhotographyDb = $this->prophesize(CreditPhotographyDb::class);
        $creditPhotographyDb->select(
            Argument::exact($condition),
            Argument::exact('count(*),'.$groupByKey)
        )->shouldBeCalledTimes(1)->willReturn($info);
        $this->adapter->expects($this->any())
                   ->method('getCreditPhotographyDb')
                   ->willReturn($creditPhotographyDb->reveal());
    }

    public function testAnalyseFailure()
    {
        $info = array();
        $filter['memberId'] = 1;
        $this->initialAnalyse($info, $filter);

        $result = $this->adapter->analyse($filter);

        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertInstanceOf('Base\Statistical\Model\NullStatistical', $result);
    }

    public function testAnalyseSuccess()
    {
        $info = array('info');
        $filter['memberId'] = 1;
        $statistical = new Statistical();
        $this->initialAnalyse($info, $filter);
        
        $translator = $this->prophesize(StaticsCreditPhotographyTranslator::class);
        $translator->arrayToObject(Argument::exact($info))->shouldBeCalledTimes(1)->willReturn($statistical);
        $this->adapter->expects($this->any())
                   ->method('getStaticsCreditPhotographyTranslator')
                   ->willReturn($translator->reveal());

        $result = $this->adapter->analyse($filter);
        $this->assertEquals($statistical, $result);
    }

    public function testFormatFilter()
    {
        $expectedCondition = ' 1 ';
        $condition = $this->adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-memberId
    public function testFormatFilterMemberId()
    {
        $filter = array(
            'memberId' => 1
        );

        $expectedCondition = 'member_id IN ('.$filter['memberId'].')';

        //验证
        $condition = $this->adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-status
    public function testFormatFilterStatus()
    {
        
        $filter = array(
            'status' => CreditPhotography::STATUS['NORMAL']
        );

        $expectedCondition = 'status = '.$filter['status'];

        //验证
        $condition = $this->adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatGroupByNull()
    {
        $expectedCondition = $groupByKey = '';

        $condition =  $this->adapter->formatGroupBy([]);
        $this->assertEquals([$expectedCondition, $groupByKey], $condition);
    }

    //formatGroupBy
    public function testFormatGroupBy()
    {
        $groupBy['applyStatus'] = '';

        $expectedCondition = ' GROUP BY apply_status';
        $groupByKey = 'apply_status';

        //验证
        $condition = $this->adapter->formatGroupBy($groupBy);
        $this->assertEquals([$expectedCondition, $groupByKey], $condition);
    }
}
