<?php
namespace Base\Statistical\Adapter\Statistical;

use PHPUnit\Framework\TestCase;

class StatisticalSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockStatisticalSdkAdapter::class)
                                ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIStatisticalAdapter()
    {
        $adapter = $this->getMockBuilder(StatisticalSdkAdapter::class)
                                ->getMockForAbstractClass();

        $this->assertInstanceOf(
            'Base\Statistical\Adapter\Statistical\IStatisticalAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'Base\Statistical\Translator\StatisticalSdkTranslator',
            $this->adapter->getTranslator()
        );
    }
}
