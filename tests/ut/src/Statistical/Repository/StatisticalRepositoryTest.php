<?php
namespace Base\Statistical\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Statistical\Model\Statistical;

use Base\Statistical\Adapter\Statistical\IStatisticalAdapter;
use Base\Statistical\Adapter\Statistical\StaticsCreditPhotographyAdapter;

class StatisticalRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = new StatisticalRepository(
            new StaticsCreditPhotographyAdapter()
        );
        parent::setUp();
    }

    public function testImplementsIStatisticalAdapter()
    {
        $this->assertInstanceOf(
            'Base\Statistical\Adapter\Statistical\IStatisticalAdapter',
            $this->repository
        );
    }

    public function testAnalyse()
    {
        $filter = array();

        $adapter = $this->prophesize(IStatisticalAdapter::class);
        $adapter->analyse(Argument::exact($filter))->shouldBeCalledTimes(1);

        $this->repository->setAdapter($adapter->reveal());

        $this->repository->analyse($filter);
    }
}
