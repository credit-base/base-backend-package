<?php
namespace Base\Statistical\View;

use PHPUnit\Framework\TestCase;

use Base\Statistical\Model\Statistical;

class StatisticalViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $statistical = new StatisticalView(new Statistical());
        $this->assertInstanceof('Base\Common\View\CommonView', $statistical);
    }
}
