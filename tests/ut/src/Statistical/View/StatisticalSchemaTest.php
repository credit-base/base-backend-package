<?php
namespace Base\Statistical\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

use Base\Statistical\Model\Statistical;

class StatisticalSchemaTest extends TestCase
{
    private $statisticalSchema;

    private $statistical;

    public function setUp()
    {
        $this->statisticalSchema = new StatisticalSchema(new Factory());

        $this->statistical = new Statistical();
        $this->statistical->setResult(array('result'));
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->statisticalSchema);
        unset($this->statistical);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->statisticalSchema);
    }

    public function testGetId()
    {
        $result = $this->statisticalSchema->getId($this->statistical);

        $this->assertEquals($result, $this->statistical->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->statisticalSchema->getAttributes($this->statistical);

        $this->assertEquals($result['result'], $this->statistical->getResult());
    }
}
