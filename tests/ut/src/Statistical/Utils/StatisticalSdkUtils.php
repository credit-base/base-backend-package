<?php
namespace Base\Statistical\Utils;

trait StatisticalSdkUtils
{
    private function compareValueObjectAndObject(
        $statisticalSdk,
        $statistical
    ) {
        $this->assertEquals($statisticalSdk->getId(), $statistical->getId());
        $this->assertEquals($statisticalSdk->getResult(), $statistical->getResult());
    }
}
