<?php
namespace Base\Statistical\Utils;

use Base\Statistical\Model\Statistical;

class MockFactory
{
    public static function generateStatistical(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Statistical {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $statistical = new Statistical($id);
        $statistical->setId($id);

        self::generateResult($statistical, $faker, $value);

        return $statistical;
    }

    private static function generateResult($statistical, $faker, $value) : void
    {
        $result = isset($value['result'])
        ? $value['result']
        : $faker->words(3, false);
        $statistical->setResult($result);
    }
}
