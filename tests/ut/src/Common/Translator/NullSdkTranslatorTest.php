<?php
namespace Base\Common\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullSdkTranslatorTest extends TestCase
{
    private $nullTranslator;

    public function setUp()
    {
        $this->nullTranslator = NullSdkTranslator::getInstance();
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf('Base\Common\Translator\ISdkTranslator', $this->nullTranslator);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceOf('Marmot\Interfaces\INull', $this->nullTranslator);
    }

    public function testValueObjectToObject()
    {
        $result = $this->nullTranslator->valueObjectToObject(null);

        $this->assertFalse($result);
        $this->assertEquals(TRANSLATOR_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testValueObjectToObjects()
    {
        $result = $this->nullTranslator->valueObjectToObjects(array());

        $this->assertFalse($result);
        $this->assertEquals(TRANSLATOR_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testObjectToValueObject()
    {
        $result = $this->nullTranslator->objectToValueObject('test');

        $this->assertFalse($result);
        $this->assertEquals(TRANSLATOR_NOT_EXIST, Core::getLastError()->getId());
    }
}
