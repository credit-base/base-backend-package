<?php
namespace Base\Common\Translator;

use PHPUnit\Framework\TestCase;

use Base\Common\Model\MockObject;

class SdkTranslatorTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockSdkTranslatorTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    //valueObjectToObjects-emptyFail
    public function testValueObjectToObjectsEmptyFail()
    {
        $expression = array();

        $execResult = $this->trait->valueObjectToObjects($expression);

        $this->assertEquals($execResult, array([], 0));
    }

    //valueObjectToObjects
    public function testValueObjectToObjectsSuccess()
    {
        $trait = $this->getMockBuilder(
            MockSdkTranslatorTrait::class
        )->setMethods(['translateToObject'])->getMock();

        $count = 1;
        $object = new MockObject();
        $valueObjectList = array($object);
        $object->setId(1);
        $objectList = array(1=>$object);
        $expression = array($valueObjectList, $count);

        $trait->expects($this->once())->method('translateToObject')->willReturn($object);

        $result = $trait->valueObjectToObjects($expression);

        $this->assertEquals(array($objectList, $count), $result);
    }
}
