<?php
namespace Base\Common\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Common\Utils\StringGenerate;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;

/**
 * @SuppressWarnings(PHPMD)
 */
class CommonWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new CommonWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //formatNumeric -- start
    /**
     * @dataProvider invalidFormatNumericProvider
     */
    public function testFormatNumericInvalid($actual, $expected, $pointer)
    {
        $result = $this->widgetRule->formatNumeric($actual, $pointer);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>$pointer), Core::getLastError()->getSource());
    }

    public function invalidFormatNumericProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->word, false, 'string'),
            array($faker->randomNumber(2), true, 'int'),
            array(array($faker->md5), false, 'array')
        );
    }
    //formatNumeric -- end
    
    //formatString -- start
    /**
     * @dataProvider invalidFormatStringProvider
     */
    public function testFormatStringInvalid($actual, $expected, $pointer)
    {
        $result = $this->widgetRule->formatString($actual, $pointer);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>$pointer), Core::getLastError()->getSource());
    }

    public function invalidFormatStringProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->word, true, 'string'),
            array($faker->randomNumber(2), false, 'int'),
            array(array($faker->md5), false, 'array')
        );
    }
    //formatString -- end
    
    //formatArray -- start
    /**
     * @dataProvider invalidFormatArrayProvider
     */
    public function testFormatArrayInvalid($actual, $expected, $pointer)
    {
        $result = $this->widgetRule->formatArray($actual, $pointer);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>$pointer), Core::getLastError()->getSource());
    }

    public function invalidFormatArrayProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->word, false, 'string'),
            array($faker->randomNumber(2), false, 'int'),
            array(array($faker->md5), true, 'array')
        );
    }
    //formatArray -- end

    //reason -- start
    /**
     * @dataProvider invalidReasonProvider
     */
    public function testReasonInvalid($actual, $expected)
    {
        $result = $this->widgetRule->reason($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidReasonProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(CommonWidgetRule::REASON_MIN_LENGTH-1), false),
            array(StringGenerate::generate(CommonWidgetRule::REASON_MAX_LENGTH+1), false),
            array(StringGenerate::generate(CommonWidgetRule::REASON_MIN_LENGTH), true),
            array(StringGenerate::generate(CommonWidgetRule::REASON_MIN_LENGTH+1), true),
            array(StringGenerate::generate(CommonWidgetRule::REASON_MAX_LENGTH), true),
            array(StringGenerate::generate(CommonWidgetRule::REASON_MAX_LENGTH-1), true)
        );
    }
    //reason -- end

    //pdf -- start
    /**
     * @dataProvider invalidPdfProvider
     */
    public function testPdfInvalid($actual, $expected)
    {
        $result = $this->widgetRule->pdf($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidPdfProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(array(), false),
            array($faker->word, false),
            array(array($faker->word), false),
            array(array('identify' => $faker->word.'.doc'), false),
            array(array('name' => 'name', 'identify' => 'identify.pdf'), true),
        );
    }
    //pdf -- end
     /**
     * @dataProvider imageProvider
     */
    public function testImage($actual, $expected)
    {
        $result = $this->widgetRule->image($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function imageProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(['name'=>$faker->word, 'identify'=>'1.jpg'], true),
            array($faker->word, false),
            array(['name'=>$faker->word], false),
            array(['name'=>$faker->word, 'identify'=>$faker->word], false),
        );
    }

    /**
     * @dataProvider imagesProvider
     */
    public function testImages($actual, $expected)
    {
        $result = $this->widgetRule->images($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function imagesProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(
                [
                    ['name'=>$faker->word, 'identify'=>'1.jpg'],
                    ['name'=>$faker->word, 'identify'=>'1.jpg']
                ], true),
            array($faker->word, false),
            array(
                [
                    ['name'=>$faker->word, 'identify'=>$faker->word],
                    ['name'=>$faker->word, 'identify'=>$faker->word]
                ], false),
        );
    }

    /**
     * @dataProvider attachmentsProvider
     */
    public function testAttachments($actual, $expected)
    {
        $result = $this->widgetRule->attachments($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function attachmentsProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        
        return array(
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.zip'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.doc'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.docx'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.xlsx'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.xls'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.pdf'],
                ], false),
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.zip'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.doc'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.docx'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.xlsx'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.xls'],
                ], true),
            array($faker->word, false),
            array(
                [
                    ['name'=>$faker->word, 'identify'=>$faker->word],
                    ['name'=>$faker->word, 'identify'=>$faker->word]
                ], false),
        );
    }
     //name -- start
    /**
     * @dataProvider invalidNameProvider
     */
    public function testNameInvalid($actual, $expected)
    {
        $result = $this->widgetRule->name($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidNameProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(CommonWidgetRule::NAME_MIN_LENGTH-1), false),
            array(StringGenerate::generate(CommonWidgetRule::NAME_MAX_LENGTH+1), false),
            array(StringGenerate::generate(CommonWidgetRule::NAME_MIN_LENGTH), true),
            array(StringGenerate::generate(CommonWidgetRule::NAME_MIN_LENGTH+1), true),
            array(StringGenerate::generate(CommonWidgetRule::NAME_MAX_LENGTH), true),
            array(StringGenerate::generate(CommonWidgetRule::NAME_MAX_LENGTH-1), true)
        );
    }
    //name -- end
    //title -- start
    /**
     * @dataProvider invalidTitleProvider
     */
    public function testTitle($actual, $expected)
    {
        $result = $this->widgetRule->title($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidTitleProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.CommonWidgetRule::TITLE_MIN_LENGTH.','.CommonWidgetRule::TITLE_MAX_LENGTH.'}'
            ), true),
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.
                    (CommonWidgetRule::TITLE_MAX_LENGTH + 1 ).','.
                    (CommonWidgetRule::TITLE_MAX_LENGTH + 5 ).
                '}'
            ), false),
            array('', false),
            array($faker->randomDigit, false)
        );
    }
    //title -- end
    
    //source -- start
    /**
     * @dataProvider invalidSourceProvider
     */
    public function testSourceInvalid($actual, $expected)
    {
        $result = $this->widgetRule->source($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        } else {
            $this->assertTrue($result);
        }
    }

    public function invalidSourceProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(StringGenerate::generate(CommonWidgetRule::SOURCE_MIN_LENGTH+1), true),
            array(StringGenerate::generate(CommonWidgetRule::SOURCE_MIN_LENGTH), true),
            array(StringGenerate::generate(CommonWidgetRule::SOURCE_MIN_LENGTH-1), false),
            array(StringGenerate::generate(CommonWidgetRule::SOURCE_MAX_LENGTH+1), false),
            array(StringGenerate::generate(CommonWidgetRule::SOURCE_MAX_LENGTH), true),
            array(StringGenerate::generate(CommonWidgetRule::SOURCE_MAX_LENGTH-1), true)
        );
    }

    //status -- start
    /**
     * @dataProvider invalidStatusProvider
     */
    public function testStatusInvalid($actual, $expected)
    {
        $result = $this->widgetRule->status($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidStatusProvider()
    {
        return array(
            array('', false),
            array(IEnableAble::STATUS['DISABLED'], true),
            array(IEnableAble::STATUS['ENABLED'], true),
            array('status', false),
            array(999, false),
        );
    }
    //status -- end

    //stick -- start
    /**
     * @dataProvider invalidStickProvider
     */
    public function testStickInvalid($actual, $expected)
    {
        $result = $this->widgetRule->stick($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidStickProvider()
    {
        return array(
            array('', false),
            array(ITopAble::STICK['DISABLED'], true),
            array(ITopAble::STICK['ENABLED'], true),
            array('stick', false),
            array(999, false),
        );
    }
    //stick -- end
    
    //description -- start
    /**
     * @dataProvider invalidDescriptionProvider
     */
    public function testDescription($actual, $expected)
    {
        $result = $this->widgetRule->description($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidDescriptionProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(StringGenerate::generate(CommonWidgetRule::DESCRIPTION_MIN_LENGTH+1), true),
            array(StringGenerate::generate(CommonWidgetRule::DESCRIPTION_MIN_LENGTH), true),
            array(StringGenerate::generate(CommonWidgetRule::DESCRIPTION_MIN_LENGTH-1), false),
            array(StringGenerate::generate(CommonWidgetRule::DESCRIPTION_MAX_LENGTH+1), false),
            array(StringGenerate::generate(CommonWidgetRule::DESCRIPTION_MAX_LENGTH), true),
            array(StringGenerate::generate(CommonWidgetRule::DESCRIPTION_MAX_LENGTH-1), true)
        );
    }
    //description -- end
}
