<?php
namespace Base\Common\Adapter;

use Marmot\Interfaces\INull;
use PHPUnit\Framework\TestCase;
use Marmot\Common\Model\IObject;

use Base\Common\Model\MockObject;
use Base\Common\Repository\MockRepository;
use Base\Common\Translator\ISdkTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class SdkAdapterTraitTest extends TestCase
{
    private $adapter;
    
    public function setUp()
    {
        $this->adapter = new MockSdkAdapterTrait();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    private function initAddAction(bool $result)
    {
        $this->trait = $this->getMockBuilder(MockSdkAdapterTrait::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $object = $this->getMockBuilder(IObject::class)->getMock();
        $valueObject = $this->getMockBuilder(IObject::class)->getMock();

        $translator = $this->prophesize(ISdkTranslator::class);
        $translator->objectToValueObject($object)->shouldBeCalledTimes(1)->willReturn($valueObject);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(MockRepository::class);
        $repository->add($valueObject)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return $object;
    }

    public function testAddActionPublic()
    {
        $object = $this->initAddAction(true);

        $result = $this->trait->addActionPublic($object);
        $this->assertTrue($result);
    }

    public function testAddActionPublicFail()
    {
        $object = $this->initAddAction(false);

        $result = $this->trait->addActionPublic($object);
        $this->assertFalse($result);
    }
    
    private function initEditAction(bool $result)
    {
        $this->trait = $this->getMockBuilder(MockSdkAdapterTrait::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $object = $this->getMockBuilder(IObject::class)->getMock();
        $valueObject = $this->getMockBuilder(IObject::class)->getMock();
        $keys = array();

        $translator = $this->prophesize(ISdkTranslator::class);
        $translator->objectToValueObject($object)->shouldBeCalledTimes(1)->willReturn($valueObject);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(MockRepository::class);
        $repository->edit($valueObject, $keys)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return [$object, $keys];
    }

    public function testEditActionPublic()
    {
        list($object, $keys) = $this->initEditAction(true);

        $result = $this->trait->editActionPublic($object, $keys);
        $this->assertTrue($result);
    }

    public function testEditActionPublicFail()
    {
        list($object, $keys) = $this->initEditAction(false);

        $result = $this->trait->editActionPublic($object, $keys);
        $this->assertFalse($result);
    }

    public function testFetchOnePublic()
    {
        $trait = $this->getMockBuilder(MockSdkAdapterTrait::class)
                      ->setMethods(['getRestfulRepository', 'getTranslator'])
                      ->getMock();

        $id = 1;
        $object = $this->getMockBuilder(IObject::class)->getMock();
        $valueObject = $this->getMockBuilder(IObject::class)->getMock();

        $repository = $this->prophesize(MockRepository::class);
        $repository->scenario()->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($valueObject);
        $trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        $translator = $this->prophesize(ISdkTranslator::class);
        $translator->valueObjectToObject($valueObject)->shouldBeCalledTimes(1)->willReturn($object);
        $trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $result = $trait->fetchOneActionPublic($id);

        $this->assertEquals($object, $result);
    }

    public function testFetchOnePublicFail()
    {
        $trait = $this->getMockBuilder(MockSdkAdapterTrait::class)
                      ->setMethods(['getRestfulRepository', 'getNullObject'])
                      ->getMock();

        $id = 1;
        $object = new class extends MockObject implements INull{
        };
        $valueObject = $this->getMockBuilder(INull::class)->getMock();

        $repository = $this->prophesize(MockRepository::class);
        $repository->scenario()->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($valueObject);
        $trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        $trait->expects($this->exactly(1))->method('getNullObject')->willReturn($object);
        
        $result = $trait->fetchOneActionPublic($id);
        $this->assertEquals($object, $result);
    }

    public function testFetchListActionPublic()
    {
        $ids = array(1);
        $valueObject = $this->getMockBuilder(INull::class)->getMock();
        $valueObjectList = array($valueObject);

        $object = new class extends MockObject implements INull{
        };
        $object->setId(1);
        $objectList = array(1=>$object);

        $trait = $this->getMockBuilder(MockSdkAdapterTrait::class)
                      ->setMethods(['getRestfulRepository', 'getTranslator'])
                      ->getMock();

        $repository = $this->prophesize(MockRepository::class);
        $repository->scenario()->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchList($ids)->shouldBeCalledTimes(1)->willReturn($valueObjectList);
        $trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());
             
        $translator = $this->prophesize(ISdkTranslator::class);
        $translator->valueObjectToObject($valueObject)->shouldBeCalledTimes(1)->willReturn($object);
        $trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $result = $trait->fetchListActionPublic($ids);
        $this->assertEquals($objectList, $result);
    }

    public function testFetchListActionPublicFail()
    {
        $ids = array(1,2,3);
        $valueObjectList = array();

        $trait = $this->getMockBuilder(MockSdkAdapterTrait::class)
                      ->setMethods(['getRestfulRepository'])
                      ->getMock();

        $repository = $this->prophesize(MockRepository::class);
        $repository->scenario()->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchList($ids)->shouldBeCalledTimes(1)->willReturn($valueObjectList);
        $trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());
             
        $result = $trait->fetchListActionPublic($ids);
        $this->assertEquals($valueObjectList, $result);
    }

    public function testFilterActionPublic()
    {
        $filter = array();
        $sort = ['-updateTime'];
        $number = 0;
        $size = 20;
        $count = 1;

        $valueObject = $this->getMockBuilder(INull::class)->getMock();
        $valueObjectList = array($valueObject);

        $object = new class extends MockObject implements INull{
        };
        $object->setId(1);
        $objectList = array(1=>$object);

        $trait = $this->getMockBuilder(MockSdkAdapterTrait::class)
                      ->setMethods(['getRestfulRepository', 'getTranslator'])
                      ->getMock();

        $repository = $this->prophesize(MockRepository::class);
        $repository->scenario()->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->filter($filter, $sort, $number, $size)
                    ->shouldBeCalledTimes(1)
                    ->willReturn(array($valueObjectList, $count));
        $trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        $translator = $this->prophesize(ISdkTranslator::class);
        $translator->valueObjectToObject($valueObject)->shouldBeCalledTimes(1)->willReturn($object);
        $trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $result = $trait->filterActionPublic($filter, $sort, $number, $size);

        $this->assertEquals(array($objectList, $count), $result);
    }

    public function testFilterActionPublicFail()
    {
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 20;
        $list = array(array(), 0);

        $trait = $this->getMockBuilder(MockSdkAdapterTrait::class)
                      ->setMethods(['getRestfulRepository'])
                      ->getMock();

        $repository = $this->prophesize(MockRepository::class);
        $repository->scenario()->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->filter($filter, $sort, $number, $size)->shouldBeCalledTimes(1)->willReturn($list);
        $trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        $result = $trait->filterActionPublic($filter, $sort, $number, $size);
        $this->assertEquals($list, $result);
    }
}
