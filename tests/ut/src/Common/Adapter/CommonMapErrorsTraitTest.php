<?php
namespace Base\Common\Adapter;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class CommonMapErrorsTraitTest extends TestCase
{
    private $adapter;
    
    public function setUp()
    {
        $this->adapter = new MockCommonMapErrorsTraitObject();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testCommonMapErrors()
    {
        $data = [
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR,
            102 => RESOURCE_CAN_NOT_MODIFY,
            103 => RESOURCE_ALREADY_EXIST,
            104 => PARAMETER_ERROR
        ];

        $this->assertEquals($data, $this->adapter->commonMapErrors());
    }

    /**
     * 测试 mapErrors, 初始化 Core::setLastError(ERROR_NOT_DEFINED);
     * 1. getMapErrors 返回数组, 后端错误ID xx => 映射本地错误ID RESOURCE_NOT_EXIST
     * 2. mock lasterErrorId 返回后端错误ID xx
     * 3. 调用 mapErrors, 并调用Core::getLastError()->getId(), 期望返回 RESOURCE_NOT_EXIST
     */
    private function initMapErrorsPublic(array $mapErrors)
    {
        $adapter = $this->getMockBuilder(MockCommonMapErrorsTraitObject::class)
            ->setMethods(
                [
                    'getMapErrors'
                ]
            )->getMock();
        
        $adapter->expects($this->once())->method('getMapErrors')->willReturn($mapErrors);

        $adapter->mapErrorsPublic();
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testMapErrors()
    {
        $apiErrorId = ERROR_NOT_DEFINED;
        $mapErrors = [$apiErrorId => RESOURCE_NOT_EXIST];

        Core::setLastError($apiErrorId);
        
        $this->initMapErrorsPublic($mapErrors);
    }

    public function testMapErrorsPointer()
    {
        $apiErrorId = ERROR_NOT_DEFINED;
        $pointer = 'test';
        $source = array('pointer' => $pointer);
        $mapErrors = array(
            $apiErrorId => array($pointer=>RESOURCE_NOT_EXIST)
        );

        Core::setLastError($apiErrorId, $source);

        $this->initMapErrorsPublic($mapErrors);
    }
}
