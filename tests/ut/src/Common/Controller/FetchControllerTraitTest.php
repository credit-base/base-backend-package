<?php
namespace Base\Common\Controller;

use Marmot\Core;
use Marmot\Basecode\Classes\Request;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\View\MockView;
use Base\Common\Controller\MockFetchControllerTrait;

class FetchControllerTraitTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(MockFetchControllerTrait::class)
                           ->setMethods(
                               [
                                'getRequest',
                                'getRepository',
                                'renderView',
                                'displayError',
                                'generateView',
                                'getResourceName',
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testGenerateView()
    {
        $controller = new MockFetchControllerTrait();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockFetchControllerTrait();
        $this->assertEquals(
            'resource',
            $controller->getResourceName()
        );
    }

    public function testGetRequest()
    {
        $controller = new MockFetchControllerTrait();

        $this->assertInstanceOf(
            'Marmot\Basecode\Classes\Request',
            $controller->getRequest()
        );
    }

    /**
     * 测试 filter
     */
    public function testFilterFail()
    {
        //初始化
        $filter = ['filter'];
        $sort = 'sort';
        $page = array('size'=> 2, 'number' => 1);

        $objectList = ['objectList'];
        $count = 0;

        //预言
        $repository = $this->prophesize(MockFetchControllerTrait::class);
        $repository->filter(
            $filter,
            [$sort],
            $page['number'],
            $page['size']
        )->shouldBeCalledTimes(1)->willReturn([$objectList, $count]);

        $request = $this->prophesize(Request::class);
        $request->get(
            Argument::exact('filter'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($filter);
        $request->get(
            Argument::exact('sort'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(
            Argument::exact('page'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($page);

        $this->controller->expects($this->exactly(3))->method('getRequest')->willReturn($request->reveal());
        //绑定
        $this->controller->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());
        $this->controller->expects($this->exactly(0))
                         ->method('renderView');
        $this->controller->expects($this->exactly(1))
                         ->method('displayError');

        //验证
        $result = $this->controller->filter();
        $this->assertFalse($result);
    }

    public function testFilter()
    {
        //初始化
        $filter = ['filter'];
        $sort = 'sort';
        $page = array('size'=> 2, 'number' => 1);

        $objectList = ['objectList'];
        $count = 20;

        $get = ['key'=>1];

        $resources = 'objetcs';

        $this->controller->getRequest()->setQueryParams($get);

        $request = $this->prophesize(Request::class);
        $request->get(
            Argument::exact('filter'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($filter);
        $request->get(
            Argument::exact('sort'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($sort);
        $request->get(
            Argument::exact('page'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($page);
        $request->get()->shouldBeCalledTimes(1)->willReturn($get);

        $this->controller->expects($this->exactly(4))->method('getRequest')->willReturn($request->reveal());

        //预言
        $view = new MockView($objectList);
        $view->pagination(
            $resources,
            $get,
            $count,
            $page['size'],
            $page['number']
        );

        $repository = $this->prophesize(MockFetchControllerTrait::class);
        $repository->filter(
            $filter,
            [$sort],
            $page['number'],
            $page['size']
        )->shouldBeCalledTimes(1)->willReturn([$objectList, $count]);

        //绑定
        $this->controller->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());
        $this->controller->expects($this->exactly(1))
                        ->method('generateView')
                        ->with($objectList)
                        ->willReturn(new MockView($objectList));
        $this->controller->expects($this->exactly(1))
                         ->method('renderView')
                         ->with($view);
        $this->controller->expects($this->exactly(0))
                         ->method('displayError');
        $this->controller->expects($this->exactly(1))
                         ->method('getResourceName')
                         ->willReturn($resources);

        //验证
        $result = $this->controller->filter();
        $this->assertTrue($result);
    }
}
