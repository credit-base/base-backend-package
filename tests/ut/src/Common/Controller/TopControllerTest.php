<?php
namespace Base\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Controller\Interfaces\ITopAbleController;

class TopControllerTest extends TestCase
{
    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(TopController::class)
                                 ->setMethods(['getController'])
                                 ->getMock();
                      
        $this->childController = new class extends TopController
        {
            public function getController(string $resource) : ITopAbleController
            {
                return parent::getController($resource);
            }
        };

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
    }

    public function testGetTopController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\ITopAbleController',
            $this->childController->getController($this->resource)
        );
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testTop()
    {
        $id = 1;

        $topController = $this->prophesize(ITopAbleController::class);
        $topController->top(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn(true);
        $this->controller->expects($this->exactly(1))
                         ->method('getController')
                         ->with($this->resource)
                         ->willReturn($topController->reveal());

        $result = $this->controller->top($this->resource, $id);
        $this->assertTrue($result);
    }

    public function testCancelTop()
    {
        $id = 1;

        $cancelTopController = $this->prophesize(ITopAbleController::class);
        $cancelTopController->cancelTop(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn(true);
        $this->controller->expects($this->exactly(1))
                         ->method('getController')
                         ->with($this->resource)
                         ->willReturn($cancelTopController->reveal());

        $result = $this->controller->cancelTop($this->resource, $id);
        $this->assertTrue($result);
    }
}
