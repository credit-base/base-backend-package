<?php
namespace Base\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ApproveControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new ApproveControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getController('');
            $this->assertInstanceOf(
                'Base\Common\Controller\NullApproveController',
                $controller
            );
    }

    public function testGetController()
    {
        foreach (ApproveControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getController($key)
            );
        }
    }
}
