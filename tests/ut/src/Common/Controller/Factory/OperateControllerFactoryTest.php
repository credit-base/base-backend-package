<?php
namespace Base\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class OperateControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new OperateControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getController('');
            $this->assertInstanceOf(
                'Base\Common\Controller\NullOperateController',
                $controller
            );
    }

    public function testGetController()
    {
        foreach (OperateControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getController($key)
            );
        }
    }
}
