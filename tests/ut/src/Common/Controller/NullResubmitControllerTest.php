<?php
namespace Base\Common\Controller;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullResubmitControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new NullResubmitController();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->controller);
    }

    public function testImplementIResubmitController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IResubmitAbleController',
            $this->controller
        );
    }

    public function testImplementINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->controller
        );
    }

    public function testResubmit()
    {
        $this->controller->resubmit(0);
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }
}
