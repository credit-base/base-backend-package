<?php
namespace Base\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Controller\Interfaces\IResubmitAbleController;

class ResubmitControllerTest extends TestCase
{
    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(ResubmitController::class)
                                 ->setMethods(['getController'])
                                 ->getMock();
                      
        $this->childController = new class extends ResubmitController
        {
            public function getController(string $resource) : IResubmitAbleController
            {
                return parent::getController($resource);
            }
        };

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
    }

    public function testGetResubmitController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IResubmitAbleController',
            $this->childController->getController($this->resource)
        );
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }
    
    public function testResubmit()
    {
        $id = 1;

        $resubmitController = $this->prophesize(IResubmitAbleController::class);
        $resubmitController->resubmit(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn(true);
        $this->controller->expects($this->exactly(1))
                         ->method('getController')
                         ->with($this->resource)
                         ->willReturn($resubmitController->reveal());

        $result = $this->controller->resubmit($this->resource, $id);
        $this->assertTrue($result);
    }
}
