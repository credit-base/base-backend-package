<?php
namespace Base\Common\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\News\Adapter\News\INewsAdapter;

class TopAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockTopAbleTrait::class)
                            ->setMethods(['updateStick'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    /**
     * @dataProvider stickDataProvider
     */
    public function testSetStick($actual, $expected)
    {
        $this->trait->setStick($actual);
        
        $result = $this->trait->getStick();
        $this->assertEquals($expected, $result);
    }

    public function stickDataProvider()
    {
        return [
            [ITopAble::STICK['ENABLED'], ITopAble::STICK['ENABLED']],
            [ITopAble::STICK['DISABLED'], ITopAble::STICK['DISABLED']],
            [999, ITopAble::STICK['DISABLED']]
        ];
    }
    /**
     * 设置 setStick() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStickWrongType()
    {
        $this->trait->setStick('stick');
    }

    public function testTopSuccess()
    {
        $this->trait->setStick(ITopAble::STICK['DISABLED']);

        $this->trait->expects($this->exactly(1))
                    ->method('updateStick')
                    ->with(ITopAble::STICK['ENABLED'])
                    ->willReturn(true);

        $result = $this->trait->top();
        $this->assertTrue($result);
    }

    public function testTopFail()
    {
        $this->trait->setStick(ITopAble::STICK['ENABLED']);

        $result = $this->trait->top();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }

    public function testCancelTopSuccess()
    {
        $this->trait->setStick(ITopAble::STICK['ENABLED']);

        $this->trait->expects($this->exactly(1))
                    ->method('updateStick')
                    ->with(ITopAble::STICK['DISABLED'])
                    ->willReturn(true);

        $result = $this->trait->cancelTop();
        $this->assertTrue($result);
    }

    public function testCancelTopFail()
    {
        $this->trait->setStick(ITopAble::STICK['DISABLED']);

        $result = $this->trait->cancelTop();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }
    
    public function testUpdateStick()
    {
        $this->trait = $this->getMockBuilder(MockTopAbleTrait::class)
                    ->setMethods(['getRepository'])
                    ->getMock();
            
        $stick = ITopAble::STICK['ENABLED'];
        $this->trait->setStick($stick);
        $this->trait->setUpdateTime(Core::$container->get('time'));

        $repository = $this->prophesize(INewsAdapter::class);
        $repository->edit(
            Argument::exact($this->trait),
            Argument::exact(array(
                'stick',
                'updateTime',
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->trait->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        $result = $this->trait->publicUpdateStick($stick);
        $this->assertTrue($result);
    }
}
