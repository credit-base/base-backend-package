<?php
namespace Base\Common\Model;

use PHPUnit\Framework\TestCase;

class NullTopAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullTopAbleTrait::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testTop()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->top();
        $this->assertFalse($result);
    }

    public function testCancelTop()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->cancelTop();
        $this->assertFalse($result);
    }

    public function testUpdateStick()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->publicUpdateStick(0);
        $this->assertFalse($result);
    }

    public function testIsTop()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isTop();
        $this->assertFalse($result);
    }

    public function testIsCancelTop()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->isCancelTop();
        $this->assertFalse($result);
    }

    private function mockResourceNotExist()
    {
        $this->trait->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);
    }
}
