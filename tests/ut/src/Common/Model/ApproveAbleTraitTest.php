<?php
namespace Base\Common\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ApproveAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockApproveAbleTrait::class)
                            ->setMethods(['approveAction', 'rejectAction'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    /**
     * @dataProvider applyStatusDataProvider
     */
    public function testSetApplyStatus($actual, $expected)
    {
        $this->trait->setApplyStatus($actual);
        
        $result = $this->trait->getApplyStatus();
        $this->assertEquals($expected, $result);
    }

    public function applyStatusDataProvider()
    {
        return [
            [IApproveAble::APPLY_STATUS['PENDING'], IApproveAble::APPLY_STATUS['PENDING']],
            [IApproveAble::APPLY_STATUS['APPROVE'], IApproveAble::APPLY_STATUS['APPROVE']],
            [IApproveAble::APPLY_STATUS['REJECT'], IApproveAble::APPLY_STATUS['REJECT']],
            [999, IApproveAble::APPLY_STATUS['PENDING']]
        ];
    }

    //rejectReason 测试 -------------------------------------------------------- start
    /**
     * 设置 setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $this->trait->setRejectReason('rejectReason');
        $this->assertEquals('rejectReason', $this->trait->getRejectReason());
    }

    /**
     * 设置 setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->trait->setRejectReason(array('rejectReason'));
    }
    //rejectReason 测试 --------------------------------------------------------   end

    public function testApproveSuccess()
    {
        $this->trait->setApplyStatus(IApproveAble::APPLY_STATUS['PENDING']);

        $this->trait->expects($this->exactly(1))->method('approveAction')->willReturn(true);

        $result = $this->trait->approve();
        $this->assertTrue($result);
    }

    public function testApproveFail()
    {
        $this->trait->setApplyStatus(IApproveAble::APPLY_STATUS['APPROVE']);

        $result = $this->trait->approve();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }

    public function testRejectSuccess()
    {
        $this->trait->setApplyStatus(IApproveAble::APPLY_STATUS['PENDING']);

        $this->trait->expects($this->exactly(1))->method('rejectAction')->willReturn(true);

        $result = $this->trait->reject();
        $this->assertTrue($result);
    }

    public function testRejectFail()
    {
        $this->trait->setApplyStatus(IApproveAble::APPLY_STATUS['APPROVE']);

        $result = $this->trait->reject();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }

    public function testIsPending()
    {
        $this->trait->setApplyStatus(IApproveAble::APPLY_STATUS['PENDING']);

        $result = $this->trait->isPending();
        $this->assertTrue($result);
    }

    public function testIsApprove()
    {
        $this->trait->setApplyStatus(IApproveAble::APPLY_STATUS['APPROVE']);

        $result = $this->trait->isApprove();
        $this->assertTrue($result);
    }

    public function testIsReject()
    {
        $this->trait->setApplyStatus(IApproveAble::APPLY_STATUS['REJECT']);

        $result = $this->trait->isReject();
        $this->assertTrue($result);
    }
}
