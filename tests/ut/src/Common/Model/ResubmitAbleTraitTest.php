<?php
namespace Base\Common\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ResubmitAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockResubmitAbleTrait::class)
                            ->setMethods(['isReject', 'resubmitAction'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testResubmitSuccess()
    {
        $this->trait->expects($this->exactly(1))->method('isReject')->willReturn(true);
        $this->trait->expects($this->exactly(1))->method('resubmitAction')->willReturn(true);

        $result = $this->trait->resubmit();
        $this->assertTrue($result);
    }

    public function testResubmitFail()
    {
        $this->trait->expects($this->exactly(1))->method('isReject')->willReturn(false);

        $result = $this->trait->resubmit();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }
}
