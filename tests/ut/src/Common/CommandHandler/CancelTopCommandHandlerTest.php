<?php
namespace Base\Common\CommandHandler;

use PHPUnit\Framework\TestCase;

use Base\Common\Model\ITopAble;
use Base\Common\Command\CancelTopCommand;

class CancelTopCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CancelTopCommandHandler::class)
                    ->setMethods(['fetchITopObject'])
                    ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $id =1;

        $command = new class($id) extends CancelTopCommand
        {
        };

        //预言
        $cancelTopAble = $this->prophesize(ITopAble::class);
        $cancelTopAble->cancelTop()->shouldBeCalledTimes(1)->willReturn(true);
        //揭示
        $this->stub->expects($this->exactly(1))
            ->method('fetchITopObject')
            ->with($id)
            ->willReturn($cancelTopAble->reveal());

        //验证
        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
