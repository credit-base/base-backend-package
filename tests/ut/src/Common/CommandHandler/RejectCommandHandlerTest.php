<?php
namespace Base\Common\CommandHandler;

use PHPUnit\Framework\TestCase;

use Base\Common\Command\RejectCommand;

use Base\News\Model\UnAuditedNews;

class RejectCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(RejectCommandHandler::class)
                    ->setMethods(['fetchIApplyObject'])
                    ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $rejectReason = 'rejectReason';
        $id = 1;

        $command = new class($rejectReason, $id) extends RejectCommand
        {
        };

        //预言
        $rejectAble = $this->prophesize(UnAuditedNews::class);
        $rejectAble->setRejectReason($rejectReason)->shouldBeCalledTimes(1);
        $rejectAble->reject()->shouldBeCalledTimes(1)->willReturn(true);

        //揭示
        $this->stub->expects($this->exactly(1))
            ->method('fetchIApplyObject')
            ->with($id)
            ->willReturn($rejectAble->reveal());

        //验证
        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
