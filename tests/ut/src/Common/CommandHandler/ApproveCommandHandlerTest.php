<?php
namespace Base\Common\CommandHandler;

use PHPUnit\Framework\TestCase;

use Base\Common\Model\IApproveAble;
use Base\Common\Command\ApproveCommand;

class ApproveCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(ApproveCommandHandler::class)
                    ->setMethods(['fetchIApplyObject'])
                    ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $id =1;

        $command = new class($id) extends ApproveCommand
        {
        };

        //预言
        $approveAble = $this->prophesize(IApproveAble::class);
        $approveAble->approve()->shouldBeCalledTimes(1)->willReturn(true);

        //揭示
        $this->stub->expects($this->exactly(1))
            ->method('fetchIApplyObject')
            ->with($id)
            ->willReturn($approveAble->reveal());

        //验证
        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
