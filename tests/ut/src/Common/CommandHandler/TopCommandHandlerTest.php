<?php
namespace Base\Common\CommandHandler;

use PHPUnit\Framework\TestCase;

use Base\Common\Model\ITopAble;
use Base\Common\Command\TopCommand;

class TopCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(TopCommandHandler::class)
                    ->setMethods(['fetchITopObject'])
                    ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $id =1;

        $command = new class($id) extends TopCommand
        {

        };

        //预言
        $topAble = $this->prophesize(ITopAble::class);
        $topAble->top()->shouldBeCalledTimes(1)->willReturn(true);

        //揭示
        $this->stub->expects($this->exactly(1))
            ->method('fetchITopObject')
            ->with($id)
            ->willReturn($topAble->reveal());

        //验证
        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
