<?php
namespace Base\Enterprise\Model;

use PHPUnit\Framework\TestCase;

class NullEnterpriseTest extends TestCase
{
    private $enterprise;

    public function setUp()
    {
        $this->enterprise = $this->getMockBuilder(NullEnterprise::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->enterprise);
    }

    public function testExtendsEnterprise()
    {
        $this->assertInstanceof('Base\Enterprise\Model\Enterprise', $this->enterprise);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->enterprise);
    }
}
