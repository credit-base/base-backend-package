<?php
namespace Base\Enterprise\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class EnterpriseTest extends TestCase
{
    private $enterprise;

    public function setUp()
    {
        $this->enterprise = new Enterprise();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->enterprise);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->enterprise
        );
    }

    public function testEnterpriseConstructor()
    {
        $this->assertEquals(0, $this->enterprise->getId());
        $this->assertEquals('', $this->enterprise->getName());
        $this->assertEquals('', $this->enterprise->getUnifiedSocialCreditCode());
        $this->assertEquals(0, $this->enterprise->getEstablishmentDate());
        $this->assertEquals(0, $this->enterprise->getApprovalDate());
        $this->assertEquals('', $this->enterprise->getAddress());
        $this->assertEquals('', $this->enterprise->getRegistrationCapital());
        $this->assertEquals(0, $this->enterprise->getBusinessTermStart());
        $this->assertEquals(0, $this->enterprise->getBusinessTermTo());
        $this->assertEquals('', $this->enterprise->getBusinessScope());
        $this->assertEquals('', $this->enterprise->getRegistrationAuthority());
        $this->assertEquals('', $this->enterprise->getPrincipal());
        $this->assertEquals('', $this->enterprise->getPrincipalCardId());
        $this->assertEquals('', $this->enterprise->getRegistrationStatus());
        $this->assertEquals('', $this->enterprise->getEnterpriseTypeCode());
        $this->assertEquals('', $this->enterprise->getEnterpriseType());
        $this->assertEquals(array(), $this->enterprise->getData());
        $this->assertEquals('', $this->enterprise->getIndustryCategory());
        $this->assertEquals('', $this->enterprise->getIndustryCode());
        $this->assertEquals(0, $this->enterprise->getAdministrativeArea());
        $this->assertEquals(Enterprise::STATUS['NORMAL'], $this->enterprise->getStatus());
        $this->assertEquals(Core::$container->get('time'), $this->enterprise->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $this->enterprise->getCreateTime());
        $this->assertEquals(0, $this->enterprise->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Enterprise setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->enterprise->setId(1);
        $this->assertEquals(1, $this->enterprise->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //name 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->enterprise->setName('name');
        $this->assertEquals('name', $this->enterprise->getName());
    }

    /**
     * 设置 Enterprise setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->enterprise->setName(array(1, 2, 3));
    }
    //name 测试 -------------------------------------------------------   end
    
    //unifiedSocialCreditCode 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setUnifiedSocialCreditCode() 正确的传参类型,期望传值正确
     */
    public function testSetUnifiedSocialCreditCodeCorrectType()
    {
        $this->enterprise->setUnifiedSocialCreditCode('unifiedSocialCreditCode');
        $this->assertEquals('unifiedSocialCreditCode', $this->enterprise->getUnifiedSocialCreditCode());
    }

    /**
     * 设置 Enterprise setUnifiedSocialCreditCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUnifiedSocialCreditCodeWrongType()
    {
        $this->enterprise->setUnifiedSocialCreditCode(array(1, 2, 3));
    }
    //unifiedSocialCreditCode 测试 -------------------------------------------------------   end
    
    //establishmentDate 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setEstablishmentDate() 正确的传参类型,期望传值正确
     */
    public function testSetEstablishmentDateCorrectType()
    {
        $this->enterprise->setEstablishmentDate('20210827');
        $this->assertEquals('20210827', $this->enterprise->getEstablishmentDate());
    }

    /**
     * 设置 Enterprise setEstablishmentDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEstablishmentDateWrongType()
    {
        $this->enterprise->setEstablishmentDate(array(1, 2, 3));
    }
    //establishmentDate 测试 -------------------------------------------------------   end
    
    //approvalDate 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setApprovalDate() 正确的传参类型,期望传值正确
     */
    public function testSetApprovalDateCorrectType()
    {
        $this->enterprise->setApprovalDate('20210827');
        $this->assertEquals('20210827', $this->enterprise->getApprovalDate());
    }

    /**
     * 设置 Enterprise setApprovalDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApprovalDateWrongType()
    {
        $this->enterprise->setApprovalDate(array(1, 2, 3));
    }
    //approvalDate 测试 -------------------------------------------------------   end
    
    //address 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setAddress() 正确的传参类型,期望传值正确
     */
    public function testSetAddressCorrectType()
    {
        $this->enterprise->setAddress('address');
        $this->assertEquals('address', $this->enterprise->getAddress());
    }

    /**
     * 设置 Enterprise setAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAddressWrongType()
    {
        $this->enterprise->setAddress(array(1, 2, 3));
    }
    //address 测试 -------------------------------------------------------   end
    
    //registrationCapital 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setRegistrationCapital() 正确的传参类型,期望传值正确
     */
    public function testSetRegistrationCapitalCorrectType()
    {
        $this->enterprise->setRegistrationCapital('registrationCapital');
        $this->assertEquals('registrationCapital', $this->enterprise->getRegistrationCapital());
    }

    /**
     * 设置 Enterprise setRegistrationCapital() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRegistrationCapitalWrongType()
    {
        $this->enterprise->setRegistrationCapital(array(1, 2, 3));
    }
    //registrationCapital 测试 -------------------------------------------------------   end
    
    //businessTermStart 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setBusinessTermStart() 正确的传参类型,期望传值正确
     */
    public function testSetBusinessTermStartCorrectType()
    {
        $this->enterprise->setBusinessTermStart('20210827');
        $this->assertEquals('20210827', $this->enterprise->getBusinessTermStart());
    }

    /**
     * 设置 Enterprise setBusinessTermStart() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBusinessTermStartWrongType()
    {
        $this->enterprise->setBusinessTermStart(array(1, 2, 3));
    }
    //businessTermStart 测试 -------------------------------------------------------   end
    
    //businessTermTo 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setBusinessTermTo() 正确的传参类型,期望传值正确
     */
    public function testSetBusinessTermToCorrectType()
    {
        $this->enterprise->setBusinessTermTo('20210827');
        $this->assertEquals('20210827', $this->enterprise->getBusinessTermTo());
    }

    /**
     * 设置 Enterprise setBusinessTermTo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBusinessTermToWrongType()
    {
        $this->enterprise->setBusinessTermTo(array(1, 2, 3));
    }
    //businessTermTo 测试 -------------------------------------------------------   end
    
    //businessScope 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setBusinessScope() 正确的传参类型,期望传值正确
     */
    public function testSetBusinessScopeCorrectType()
    {
        $this->enterprise->setBusinessScope('businessScope');
        $this->assertEquals('businessScope', $this->enterprise->getBusinessScope());
    }

    /**
     * 设置 Enterprise setBusinessScope() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBusinessScopeWrongType()
    {
        $this->enterprise->setBusinessScope(array(1, 2, 3));
    }
    //businessScope 测试 -------------------------------------------------------   end
    
    //registrationAuthority 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setRegistrationAuthority() 正确的传参类型,期望传值正确
     */
    public function testSetRegistrationAuthorityCorrectType()
    {
        $this->enterprise->setRegistrationAuthority('registrationAuthority');
        $this->assertEquals('registrationAuthority', $this->enterprise->getRegistrationAuthority());
    }

    /**
     * 设置 Enterprise setRegistrationAuthority() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRegistrationAuthorityWrongType()
    {
        $this->enterprise->setRegistrationAuthority(array(1, 2, 3));
    }
    //registrationAuthority 测试 -------------------------------------------------------   end
    
    //principal 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setPrincipal() 正确的传参类型,期望传值正确
     */
    public function testSetPrincipalCorrectType()
    {
        $this->enterprise->setPrincipal('principal');
        $this->assertEquals('principal', $this->enterprise->getPrincipal());
    }

    /**
     * 设置 Enterprise setPrincipal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPrincipalWrongType()
    {
        $this->enterprise->setPrincipal(array(1, 2, 3));
    }
    //principal 测试 -------------------------------------------------------   end
    
    //principalCardId 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setPrincipalCardId() 正确的传参类型,期望传值正确
     */
    public function testSetPrincipalCardIdCorrectType()
    {
        $this->enterprise->setPrincipalCardId('principalCardId');
        $this->assertEquals('principalCardId', $this->enterprise->getPrincipalCardId());
    }

    /**
     * 设置 Enterprise setPrincipalCardId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPrincipalCardIdWrongType()
    {
        $this->enterprise->setPrincipalCardId(array(1, 2, 3));
    }
    //principalCardId 测试 -------------------------------------------------------   end
    
    //registrationStatus 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setRegistrationStatus() 正确的传参类型,期望传值正确
     */
    public function testSetRegistrationStatusCorrectType()
    {
        $this->enterprise->setRegistrationStatus('registrationStatus');
        $this->assertEquals('registrationStatus', $this->enterprise->getRegistrationStatus());
    }

    /**
     * 设置 Enterprise setRegistrationStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRegistrationStatusWrongType()
    {
        $this->enterprise->setRegistrationStatus(array(1, 2, 3));
    }
    //registrationStatus 测试 -------------------------------------------------------   end
    
    //enterpriseTypeCode 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setEnterpriseTypeCode() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseTypeCodeCorrectType()
    {
        $this->enterprise->setEnterpriseTypeCode('enterpriseTypeCode');
        $this->assertEquals('enterpriseTypeCode', $this->enterprise->getEnterpriseTypeCode());
    }

    /**
     * 设置 Enterprise setEnterpriseTypeCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseTypeCodeWrongType()
    {
        $this->enterprise->setEnterpriseTypeCode(array(1, 2, 3));
    }
    //enterpriseTypeCode 测试 -------------------------------------------------------   end
    
    //enterpriseType 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setEnterpriseType() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseTypeCorrectType()
    {
        $this->enterprise->setEnterpriseType('enterpriseType');
        $this->assertEquals('enterpriseType', $this->enterprise->getEnterpriseType());
    }

    /**
     * 设置 Enterprise setEnterpriseType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseTypeWrongType()
    {
        $this->enterprise->setEnterpriseType(array(1, 2, 3));
    }
    //enterpriseType 测试 -------------------------------------------------------   end
    
    //data 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setData() 正确的传参类型,期望传值正确
     */
    public function testSetDataCorrectType()
    {
        $this->enterprise->setData(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->enterprise->getData());
    }

    /**
     * 设置 Enterprise setData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataWrongType()
    {
        $this->enterprise->setData('enterprise');
    }
    //data 测试 -------------------------------------------------------   end

    //industryCategory 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setIndustryCategory() 正确的传参类型,期望传值正确
     */
    public function testSetIndustryCategoryCorrectType()
    {
        $this->enterprise->setIndustryCategory('industryCategory');
        $this->assertEquals('industryCategory', $this->enterprise->getIndustryCategory());
    }

    /**
     * 设置 Enterprise setIndustryCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIndustryCategoryWrongType()
    {
        $this->enterprise->setIndustryCategory(array(1, 2, 3));
    }
    //industryCategory 测试 -------------------------------------------------------   end
    
    //industryCode 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setIndustryCode() 正确的传参类型,期望传值正确
     */
    public function testSetIndustryCodeCorrectType()
    {
        $this->enterprise->setIndustryCode('industryCode');
        $this->assertEquals('industryCode', $this->enterprise->getIndustryCode());
    }

    /**
     * 设置 Enterprise setIndustryCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIndustryCodeWrongType()
    {
        $this->enterprise->setIndustryCode(array(1, 2, 3));
    }
    //industryCode 测试 -------------------------------------------------------   end
    
    //administrativeArea 测试 ------------------------------------------------------- start
    /**
     * 设置 Enterprise setAdministrativeArea() 正确的传参类型,期望传值正确
     */
    public function testSetAdministrativeAreaCorrectType()
    {
        $this->enterprise->setAdministrativeArea('20210827');
        $this->assertEquals('20210827', $this->enterprise->getAdministrativeArea());
    }

    /**
     * 设置 Enterprise setAdministrativeArea() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAdministrativeAreaWrongType()
    {
        $this->enterprise->setAdministrativeArea(array(1, 2, 3));
    }
    //administrativeArea 测试 -------------------------------------------------------   end
    
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Enterprise setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->enterprise->setStatus($actual);
        $this->assertEquals($expected, $this->enterprise->getStatus());
    }

    /**
     * 循环测试 Enterprise setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Enterprise::STATUS['NORMAL'],Enterprise::STATUS['NORMAL']),
            array(Enterprise::STATUS['DELETED'],Enterprise::STATUS['DELETED']),
            array(999,Enterprise::STATUS['NORMAL']),
        );
    }

    /**
     * 设置 Enterprise setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->enterprise->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end
}
