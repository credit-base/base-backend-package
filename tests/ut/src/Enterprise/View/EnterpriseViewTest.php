<?php
namespace Base\Enterprise\View;

use PHPUnit\Framework\TestCase;

use Base\Enterprise\Model\Enterprise;

class EnterpriseViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $enterprise = new EnterpriseView(new Enterprise());
        $this->assertInstanceof('Base\Common\View\CommonView', $enterprise);
    }
}
