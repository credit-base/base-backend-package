<?php
namespace Base\Enterprise\Adapter\Enterprise;

use PHPUnit\Framework\TestCase;

use Base\Enterprise\Model\Enterprise;

class EnterpriseSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockEnterpriseSdkAdapter::class)
                           ->setMethods(['fetchOneAction', 'fetchListAction', 'filterAction'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIEnterpriseAdapter()
    {
        $adapter = new EnterpriseSdkAdapter();

        $this->assertInstanceOf(
            'Base\Enterprise\Adapter\Enterprise\IEnterpriseAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\Enterprise\Repository\EnterpriseRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\Enterprise\Model\NullEnterprise',
            $this->adapter->getNullObject()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockEnterpriseSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedEnterprise = new Enterprise();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedEnterprise);

        //验证
        $enterprise = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedEnterprise, $enterprise);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $enterpriseOne = new Enterprise(1);
        $enterpriseTwo = new Enterprise(2);

        $ids = [1, 2];

        $expectedEnterpriseList = [];
        $expectedEnterpriseList[$enterpriseOne->getId()] = $enterpriseOne;
        $expectedEnterpriseList[$enterpriseTwo->getId()] = $enterpriseTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedEnterpriseList);

        //验证
        $enterpriseList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedEnterpriseList, $enterpriseList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $enterpriseOne = new Enterprise(1);
        $enterpriseTwo = new Enterprise(2);

        $expectedEnterpriseList = [];
        $expectedEnterpriseList[$enterpriseOne->getId()] = $enterpriseOne;
        $expectedEnterpriseList[$enterpriseTwo->getId()] = $enterpriseTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedEnterpriseList, 2]);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedEnterpriseList, 2], $result);
    }
}
