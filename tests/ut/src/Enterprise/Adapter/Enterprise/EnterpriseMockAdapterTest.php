<?php
namespace Base\Enterprise\Adapter\Enterprise;

use PHPUnit\Framework\TestCase;

class EnterpriseMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new EnterpriseMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Enterprise\Model\Enterprise',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Enterprise\Model\Enterprise',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Enterprise\Model\Enterprise',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
