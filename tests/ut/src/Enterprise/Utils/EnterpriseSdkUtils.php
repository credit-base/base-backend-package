<?php
namespace Base\Enterprise\Utils;

trait EnterpriseSdkUtils
{
    private function compareValueObjectAndObject(
        $enterpriseSdk,
        $enterprise
    ) {

        $this->assertEquals($enterpriseSdk->getId(), $enterprise->getId());
        $this->assertEquals($enterpriseSdk->getName(), $enterprise->getName());
        $this->assertEquals($enterpriseSdk->getUnifiedSocialCreditCode(), $enterprise->getUnifiedSocialCreditCode());
        $this->assertEquals($enterpriseSdk->getEstablishmentDate(), $enterprise->getEstablishmentDate());
        $this->assertEquals($enterpriseSdk->getApprovalDate(), $enterprise->getApprovalDate());
        $this->assertEquals($enterpriseSdk->getRegistrationCapital(), $enterprise->getRegistrationCapital());
        $this->assertEquals($enterpriseSdk->getBusinessTermStart(), $enterprise->getBusinessTermStart());
        $this->assertEquals($enterpriseSdk->getBusinessTermTo(), $enterprise->getBusinessTermTo());
        $this->assertEquals($enterpriseSdk->getBusinessScope(), $enterprise->getBusinessScope());
        $this->assertEquals($enterpriseSdk->getRegistrationAuthority(), $enterprise->getRegistrationAuthority());
        $this->assertEquals($enterpriseSdk->getPrincipal(), $enterprise->getPrincipal());
        $this->assertEquals($enterpriseSdk->getPrincipalCardId(), $enterprise->getPrincipalCardId());
        $this->assertEquals($enterpriseSdk->getRegistrationStatus(), $enterprise->getRegistrationStatus());
        $this->assertEquals($enterpriseSdk->getEnterpriseTypeCode(), $enterprise->getEnterpriseTypeCode());
        $this->assertEquals($enterpriseSdk->getEnterpriseType(), $enterprise->getEnterpriseType());
        $this->assertEquals($enterpriseSdk->getIndustryCategory(), $enterprise->getIndustryCategory());
        $this->assertEquals($enterpriseSdk->getIndustryCode(), $enterprise->getIndustryCode());
        $this->assertEquals($enterpriseSdk->getAdministrativeArea(), $enterprise->getAdministrativeArea());
        $this->assertEquals($enterpriseSdk->getStatus(), $enterprise->getStatus());
        $this->assertEquals($enterpriseSdk->getCreateTime(), $enterprise->getCreateTime());
        $this->assertEquals($enterpriseSdk->getStatusTime(), $enterprise->getStatusTime());
        $this->assertEquals($enterpriseSdk->getUpdateTime(), $enterprise->getUpdateTime());
    }
}
