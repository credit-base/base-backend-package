<?php
namespace Base\Enterprise\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;
use Base\Enterprise\Adapter\Enterprise\EnterpriseSdkAdapter;

class EnterpriseSdkRepositoryTest extends TestCase
{
    private $repository;

    private $mockRepository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(EnterpriseSdkRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();

        $this->mockRepository = new MockEnterpriseRepository();
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->mockRepository);
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIEnterpriseAdapter()
    {
        $this->assertInstanceOf(
            'Base\Enterprise\Adapter\Enterprise\IEnterpriseAdapter',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new EnterpriseSdkAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Enterprise\Adapter\Enterprise\IEnterpriseAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'Base\Enterprise\Adapter\Enterprise\EnterpriseSdkAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Enterprise\Adapter\Enterprise\IEnterpriseAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'Base\Enterprise\Adapter\Enterprise\EnterpriseMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IEnterpriseAdapter::class);
        $adapter->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IEnterpriseAdapter::class);
        $adapter->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 20;

        $adapter = $this->prophesize(IEnterpriseAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($number),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $number, $size);
    }
}
