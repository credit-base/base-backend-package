<?php
namespace Base\Enterprise\Translator;

use PHPUnit\Framework\TestCase;

use Base\Enterprise\Model\NullEnterprise;
use Base\Enterprise\Utils\EnterpriseSdkUtils;

use BaseSdk\Enterprise\Model\Enterprise as EnterpriseSdk;
use BaseSdk\Enterprise\Model\NullEnterprise as NullEnterpriseSdk;

class EnterpriseSdkTranslatorTest extends TestCase
{
    use EnterpriseSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new EnterpriseSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $enterprise = \Base\Enterprise\Utils\MockFactory::generateEnterprise(1);

        $enterpriseSdk = new EnterpriseSdk(
            $enterprise->getId(),
            $enterprise->getName(),
            $enterprise->getUnifiedSocialCreditCode(),
            $enterprise->getEstablishmentDate(),
            $enterprise->getApprovalDate(),
            $enterprise->getAddress(),
            $enterprise->getRegistrationCapital(),
            $enterprise->getBusinessTermStart(),
            $enterprise->getBusinessTermTo(),
            $enterprise->getBusinessScope(),
            $enterprise->getRegistrationAuthority(),
            $enterprise->getPrincipal(),
            $enterprise->getPrincipalCardId(),
            $enterprise->getRegistrationStatus(),
            $enterprise->getEnterpriseTypeCode(),
            $enterprise->getEnterpriseType(),
            $enterprise->getData(),
            $enterprise->getIndustryCategory(),
            $enterprise->getIndustryCode(),
            $enterprise->getAdministrativeArea(),
            $enterprise->getStatus(),
            $enterprise->getStatusTime(),
            $enterprise->getCreateTime(),
            $enterprise->getUpdateTime()
        );

        $enterpriseObject = $this->translator->valueObjectToObject($enterpriseSdk);
        $this->assertInstanceof('Base\Enterprise\Model\Enterprise', $enterpriseObject);
        $this->compareValueObjectAndObject($enterpriseSdk, $enterpriseObject);
    }

    public function testValueObjectToObjectFail()
    {
        $enterpriseSdk = NullEnterpriseSdk::getInstance();

        $enterprise = $this->translator->valueObjectToObject($enterpriseSdk);
        $this->assertInstanceof('Base\Enterprise\Model\NullEnterprise', $enterprise);
    }

    public function testObjectToValueObject()
    {
        $enterpriseSdk = NullEnterpriseSdk::getInstance();

        $result = $this->translator->objectToValueObject($enterpriseSdk);

        $this->assertEquals(
            $enterpriseSdk,
            $result
        );
    }
}
