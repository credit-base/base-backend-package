<?php
namespace Base\CreditPhotography\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\CreditPhotography\WidgetRule\CreditPhotographyWidgetRule;

class CreditPhotographyControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockCreditPhotographyControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetCreditPhotographyWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\CreditPhotography\WidgetRule\CreditPhotographyWidgetRule',
            $this->trait->getCreditPhotographyWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\CreditPhotography\Repository\CreditPhotographyRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    public function testValidateApproveScenario()
    {
        $controller = $this->getMockBuilder(MockCreditPhotographyControllerTrait::class)
                    ->setMethods(['getCommonWidgetRule']) ->getMock();

        $applyCrew = 1;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(
            Argument::exact($applyCrew),
            Argument::exact('applyCrewId')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))->method(
            'getCommonWidgetRule'
        )->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateApproveScenarioPublic($applyCrew);
        $this->assertTrue($result);
    }

    public function testValidateRejectScenario()
    {
        $controller = $this->getMockBuilder(MockCreditPhotographyControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule']) ->getMock();

        $rejectReason = 'rejectReason';
        $applyCrew = 1;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->description(
            Argument::exact($rejectReason),
            Argument::exact('rejectReason')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->formatNumeric(
            Argument::exact($applyCrew),
            Argument::exact('applyCrewId')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(2))->method(
            'getCommonWidgetRule'
        )->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateRejectScenarioPublic($rejectReason, $applyCrew);
        $this->assertTrue($result);
    }

    public function testValidateAddScenario()
    {
        $controller = $this->getMockBuilder(MockCreditPhotographyControllerTrait::class)
                    ->setMethods(['getCommonWidgetRule', 'getCreditPhotographyWidgetRule']) ->getMock();

        $description = 'description';
        $attachments = array('attachments');
        $member = 1;

        $creditPhotographyWidgetRule = $this->prophesize(CreditPhotographyWidgetRule::class);
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->description(
            Argument::exact($description)
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->formatNumeric(
            Argument::exact($member),
            Argument::exact('memberId')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $creditPhotographyWidgetRule->attachments(
            Argument::exact($attachments)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))->method(
            'getCreditPhotographyWidgetRule'
        )->willReturn($creditPhotographyWidgetRule->reveal());
        $controller->expects($this->exactly(2))->method(
            'getCommonWidgetRule'
        )->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateAddScenarioPublic($description, $attachments, $member);
        $this->assertTrue($result);
    }
}
