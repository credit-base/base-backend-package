<?php
namespace Base\CreditPhotography\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\Response;
use Marmot\Framework\Classes\CommandBus;

use Base\CreditPhotography\Utils\MockFactory;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Command\CreditPhotography\ApproveCreditPhotographyCommand;
use Base\CreditPhotography\Command\CreditPhotography\RejectCreditPhotographyCommand;

class CreditPhotographyApproveControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new CreditPhotographyApproveController();
    }

    public function teatDown()
    {
        unset($this->controller);
    }

    public function testImplementsIApproveAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IApproveAbleController',
            $this->controller
        );
    }

    private function initialApprove($result)
    {
        $this->controller = $this->getMockBuilder(CreditPhotographyApproveController::class)
                    ->setMethods([
                        'getRequest',
                        'validateApproveScenario',
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $applyCrewId = 2;
        $data = array(
            'relationships' => array(
                "applyCrew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$applyCrewId)
                    )
                )
            ),
        );
        $command = new ApproveCreditPhotographyCommand($applyCrewId, $id);

        $this->controller->expects($this->exactly(1))->method('validateApproveScenario')->willReturn(true);
            
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);
        $this->controller->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testApproveFailure()
    {
        $command = $this->initialApprove(false);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->approve($command->id);
        $this->assertFalse($result);
    }

    public function testApproveSuccess()
    {
        $command = $this->initialApprove(true);

        $creditPhotography = \Base\CreditPhotography\Utils\MockFactory::generateCreditPhotography($command->id);

        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($creditPhotography);

        $this->controller->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $result = $this->controller->approve($command->id);
        $this->assertTrue($result);
    }

    private function initialReject($result)
    {
        $this->stub = $this->getMockBuilder(CreditPhotographyApproveController::class)
                    ->setMethods([
                        'getRequest',
                        'validateRejectScenario',
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $applyCrew = 2;
        $rejectReason = 'rejectReason';

        $data = array(
            'attributes' => array(
                'rejectReason' => $rejectReason
            ),
            'relationships' => array(
                "applyCrew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$applyCrew)
                    )
                )
            ),
        );
        $command = new RejectCreditPhotographyCommand($applyCrew, $rejectReason, $id);

        $this->stub->expects($this->exactly(1))->method('validateRejectScenario')->willReturn(true);

        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);

        $this->stub->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testRejectFailure()
    {
        $command = $this->initialReject(false);

        $this->stub->expects($this->exactly(1))->method('displayError');

        $result = $this->stub->reject($command->id);
        $this->assertFalse($result);
    }

    public function testRejectSuccess()
    {
        $command = $this->initialReject(true);

        $creditPhotography = \Base\CreditPhotography\Utils\MockFactory::generateCreditPhotography($command->id);

        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($creditPhotography);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))->method('render')->willReturn(true);

        $result = $this->stub->reject($command->id);
        $this->assertTrue($result);
    }
}
