<?php
namespace Base\CreditPhotography\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;
use Base\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand;

class CreditPhotographyOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new CreditPhotographyOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    private function creditPhotographyData() : array
    {
        return array(
            "type"=>"creditPhotography",
            "attributes"=>array(
                "description"=>"描述",
                "attachments"=>array(
                    array('name' => 'name', 'identify' => 'identify.jpg'),
                    array('name' => 'name', 'identify' => 'identify.jpg'),
                    array('name' => 'name', 'identify' => 'identify.jpg')
                )
            ),
            "relationships"=>array(
                "member"=>array(
                    "data"=>array(
                        array("type"=>"members","id"=>1)
                    )
                )
            )
        );
    }

    public function testAdd()
    {
        $controller = $this->getMockBuilder(CreditPhotographyOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateAddScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $command = $this->initAdd($controller, true);
        $creditPhotography = \Base\CreditPhotography\Utils\MockFactory::generateCreditPhotography($command->id);

        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($creditPhotography);

        $controller->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $result = $controller->add();
        $this->assertTrue($result);
    }

    public function testAddFail()
    {
        $controller = $this->getMockBuilder(CreditPhotographyOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateAddScenario',
                    'getCommandBus',
                    'getRepository',
                    'displayError'
                ]
            )->getMock();

        $this->initAdd($controller, false);

        $controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        $result = $controller->add();
        $this->assertFalse($result);
    }

    protected function initAdd(CreditPhotographyOperateController $controller, bool $result)
    {
        // mock 请求参数
        $data = $this->creditPhotographyData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $description = $attributes['description'];
        $attachments = $attributes['attachments'];

        $member = $relationships['member']['data'][0]['id'];

        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);

        $controller->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('validateAddScenario')->willReturn(true);

        $command = new AddCreditPhotographyCommand(
            $description,
            $attachments,
            $member
        );

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
            
        return $command;
    }

    public function testEdit()
    {
        $id = 1;
        $result = $this->controller ->edit($id);
        $this->assertFalse($result);
    }

    private function initialDelete($result)
    {
        $this->stub = $this->getMockBuilder(CreditPhotographyOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new DeleteCreditPhotographyCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDeleteSuccess()
    {
        $command = $this->initialDelete(true);

        $creditPhotography = \Base\CreditPhotography\Utils\MockFactory::generateCreditPhotography($command->id);

        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($creditPhotography);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->stub->expects($this->exactly(1))->method('render');

        $result = $this->stub->delete($command->id);
        $this->assertTrue($result);
    }

    public function testDeleteFailure()
    {
        $command = $this->initialDelete(false);

        $this->stub->expects($this->exactly(1))->method('displayError');

        $result = $this->stub->delete($command->id);
        $this->assertFalse($result);
    }
}
