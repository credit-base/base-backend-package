<?php
namespace Base\CreditPhotography\Controller;

use PHPUnit\Framework\TestCase;

class CreditPhotographyFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(CreditPhotographyFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockCreditPhotographyFetchController();

        $this->assertInstanceOf(
            'Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockCreditPhotographyFetchController();

        $this->assertInstanceOf(
            'Base\CreditPhotography\View\CreditPhotographyView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockCreditPhotographyFetchController();

        $this->assertEquals(
            'creditPhotography',
            $controller->getResourceName()
        );
    }
}
