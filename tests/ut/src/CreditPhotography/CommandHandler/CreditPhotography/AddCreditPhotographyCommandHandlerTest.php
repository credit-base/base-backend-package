<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;

class AddCreditPhotographyCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddCreditPhotographyCommandHandler::class)
                                     ->setMethods(['fetchMember', 'getCreditPhotography'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $command = new AddCreditPhotographyCommand(
            'description',
            array('attachments'),
            1
        );
        
        $expectId = 10;
        $member = \Base\Member\Utils\MockFactory::generateMember($command->member);

        $this->commandHandler->expects($this->exactly(1))
             ->method('fetchMember')
             ->with($command->member)
             ->willReturn($member);

        $creditPhotography = $this->prophesize(CreditPhotography::class);
        $creditPhotography->setDescription($command->description);
        $creditPhotography->setAttachments($command->attachments);
        $creditPhotography->setMember($member);
        $creditPhotography->add()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $creditPhotography->getId()->shouldBeCalledTimes(1)->willReturn($expectId);
        }

        $this->commandHandler->expects($this->exactly(1))
            ->method('getCreditPhotography')
            ->willReturn($creditPhotography->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
