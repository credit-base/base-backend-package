<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Command\CreditPhotography\ApproveCreditPhotographyCommand;

class ApproveCreditPhotographyCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveCreditPhotographyCommandHandler::class)
                    ->setMethods(['fetchCreditPhotography'])
                    ->getMock();
    }

    public function testExtendsApproveCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }

    public function testFetchIApplyObject()
    {
        $id = 1;
        $creditPhotography = \Base\CreditPhotography\Utils\MockFactory::generateCreditPhotography($id);

        $this->stub->expects($this->once())
             ->method('fetchCreditPhotography')
             ->with($id)
             ->willReturn($creditPhotography);

        $result = $this->stub->fetchIApplyObject($id);
        $this->assertEquals($result, $creditPhotography);
    }

    public function testExecuteAction()
    {
        $stub = $this->getMockBuilder(MockApproveCreditPhotographyCommandHandler::class)
                ->setMethods(['fetchIApplyObject', 'fetchApplyCrew'])
                ->getMock();

        $id = 1;
        $creditPhotography = \Base\CreditPhotography\Utils\MockFactory::generateCreditPhotography($id);
        $crew = $creditPhotography->getApplyCrew();

        $command = new ApproveCreditPhotographyCommand($crew->getId(), $id);

        $stub->expects($this->once())->method('fetchApplyCrew')->with($crew->getId())->willReturn($crew);

        $creditPhotography = $this->prophesize(CreditPhotography::class);
        $creditPhotography->setApplyCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $creditPhotography->approve()->shouldBeCalledTimes(1)->willReturn(true);

        $stub->expects($this->exactly(1))->method('fetchIApplyObject')->willReturn($creditPhotography->reveal());

        $result = $stub->executeAction($command);
        $this->assertTrue($result);
    }
}
