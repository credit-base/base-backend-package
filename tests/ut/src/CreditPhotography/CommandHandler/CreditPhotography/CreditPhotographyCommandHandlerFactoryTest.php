<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;
use Base\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand;
use Base\CreditPhotography\Command\CreditPhotography\ApproveCreditPhotographyCommand;
use Base\CreditPhotography\Command\CreditPhotography\RejectCreditPhotographyCommand;

class CreditPhotographyCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new CreditPhotographyCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddCreditPhotographyCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddCreditPhotographyCommand(
                $this->faker->word(),
                array($this->faker->md5()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\CreditPhotography\CommandHandler\CreditPhotography\AddCreditPhotographyCommandHandler',
            $commandHandler
        );
    }

    public function testDeleteCreditPhotographyCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DeleteCreditPhotographyCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\CreditPhotography\CommandHandler\CreditPhotography\DeleteCreditPhotographyCommandHandler',
            $commandHandler
        );
    }

    public function testApproveCreditPhotographyCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveCreditPhotographyCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\CreditPhotography\CommandHandler\CreditPhotography\ApproveCreditPhotographyCommandHandler',
            $commandHandler
        );
    }

    public function testRejectCreditPhotographyCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectCreditPhotographyCommand(
                $this->faker->randomNumber(),
                $this->faker->word(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\CreditPhotography\CommandHandler\CreditPhotography\RejectCreditPhotographyCommandHandler',
            $commandHandler
        );
    }
}
