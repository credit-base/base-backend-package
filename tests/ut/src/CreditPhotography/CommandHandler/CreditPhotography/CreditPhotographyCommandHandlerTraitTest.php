<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Repository\CrewRepository;

use Base\Member\Repository\MemberRepository;

use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CreditPhotographyCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockCreditPhotographyCommandHandlerTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetCrewRepository()
    {
        $trait = new MockCreditPhotographyCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $trait->publicGetCrewRepository()
        );
    }

    public function testGetMemberRepository()
    {
        $trait = new MockCreditPhotographyCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Member\Repository\MemberRepository',
            $trait->publicGetMemberRepository()
        );
    }

    public function testGetCreditPhotographyRepository()
    {
        $trait = new MockCreditPhotographyCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\CreditPhotography\Repository\CreditPhotographyRepository',
            $trait->publicGetCreditPhotographyRepository()
        );
    }

    public function testGetCreditPhotographyDbTranslator()
    {
        $trait = new MockCreditPhotographyCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\CreditPhotography\Translator\CreditPhotographyDbTranslator',
            $trait->publicGetCreditPhotographyDbTranslator()
        );
    }

    public function testGetCreditPhotography()
    {
        $trait = new MockCreditPhotographyCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\CreditPhotography\Model\CreditPhotography',
            $trait->publicGetCreditPhotography()
        );
    }

    public function testFetchApplyCrew()
    {
        $trait = $this->getMockBuilder(MockCreditPhotographyCommandHandlerTrait::class)
                 ->setMethods(['getCrewRepository']) ->getMock();

        $id = 1;

        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($crew);

        $trait->expects($this->exactly(1))->method('getCrewRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchApplyCrew($id);
        $this->assertEquals($result, $crew);
    }

    public function testFetchMember()
    {
        $trait = $this->getMockBuilder(MockCreditPhotographyCommandHandlerTrait::class)
                 ->setMethods(['getMemberRepository']) ->getMock();

        $id = 1;
        $member = \Base\Member\Utils\MockFactory::generateMember($id);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($member);

        $trait->expects($this->exactly(1))->method('getMemberRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchMember($id);
        $this->assertEquals($result, $member);
    }

    public function testFetchCreditPhotography()
    {
        $trait = $this->getMockBuilder(MockCreditPhotographyCommandHandlerTrait::class)
                 ->setMethods(['getCreditPhotographyRepository']) ->getMock();

        $id = 1;
        $creditPhotography = \Base\CreditPhotography\Utils\MockFactory::generateCreditPhotography($id);

        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($creditPhotography);

        $trait->expects($this->exactly(1))->method(
            'getCreditPhotographyRepository'
        )->willReturn($repository->reveal());

        $result = $trait->publicFetchCreditPhotography($id);
        $this->assertEquals($result, $creditPhotography);
    }
}
