<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand;

class DeleteCreditPhotographyCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(DeleteCreditPhotographyCommandHandler::class)
                                     ->setMethods(['fetchCreditPhotography'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $command = new DeleteCreditPhotographyCommand(
            $this->faker->randomNumber()
        );

        $creditPhotography = $this->prophesize(CreditPhotography::class);
        $creditPhotography->delete()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
             ->method('fetchCreditPhotography')
             ->with($command->id)
             ->willReturn($creditPhotography->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
