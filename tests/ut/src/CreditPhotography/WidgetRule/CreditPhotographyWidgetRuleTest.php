<?php
namespace Base\CreditPhotography\WidgetRule;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class CreditPhotographyWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new CreditPhotographyWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }
    
    //attachments -- start
    /**
     * @dataProvider invalidAttachmentsProvider
     */
    public function testAttachmentsInvalid($actual, $expected)
    {
        $result = $this->widgetRule->attachments($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidAttachmentsProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(array(), false),
            array($faker->name, false),
            array(array(
                array('name'=>$faker->name, 'identify'=>$faker->name)
            ), false),
            array(array(
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'2.jpg'),
                array('name'=>$faker->name,'identify'=>'3.jpg'),
                array('name'=>$faker->name,'identify'=>'4.jpg'),
                array('name'=>$faker->name,'identify'=>'5.jpg'),
                array('name'=>$faker->name,'identify'=>'6.jpg'),
                array('name'=>$faker->name,'identify'=>'7.jpg'),
                array('name'=>$faker->name,'identify'=>'8.jpg'),
                array('name'=>$faker->name,'identify'=>'9.jpg'),
            ), false),
            array(array(
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
            ), true),
            array(array(
                array('name'=>$faker->name,'identify'=>'1.mp4'),
                array('name'=>$faker->name,'identify'=>'1.mp4'),
            ), false),
            array(array(
                array('name'=>$faker->name,'identify'=>'1.mp4'),
            ), true),
        );
    }
    //attachments -- end
}
