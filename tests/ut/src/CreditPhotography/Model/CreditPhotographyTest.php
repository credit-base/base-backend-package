<?php
namespace Base\CreditPhotography\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Model\IApproveAble;

use Base\Crew\Model\Crew;

use Base\Member\Model\Member;
use Base\Member\Model\NullMember;

use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class CreditPhotographyTest extends TestCase
{
    private $creditPhotography;

    public function setUp()
    {
        $this->creditPhotography = new MockCreditPhotography();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->creditPhotography);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->creditPhotography
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IOperate',
            $this->creditPhotography
        );
    }

    public function testImplementsIApproveAble()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IApproveAble',
            $this->creditPhotography
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter',
            $this->creditPhotography->getRepository()
        );
    }

    public function testCreditPhotographyConstructor()
    {
        $this->assertEquals(0, $this->creditPhotography->getId());
        $this->assertEmpty($this->creditPhotography->getDescription());
        $this->assertEquals(array(), $this->creditPhotography->getAttachments());
        $this->assertInstanceOf('Base\Crew\Model\Crew', $this->creditPhotography->getApplyCrew());
        $this->assertInstanceOf('Base\Member\Model\Member', $this->creditPhotography->getMember());
        $this->assertEmpty($this->creditPhotography->getRejectReason());
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $this->creditPhotography->getApplyStatus());
        $this->assertEquals(CreditPhotography::STATUS['NORMAL'], $this->creditPhotography->getStatus());
        $this->assertEquals(Core::$container->get('time'), $this->creditPhotography->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $this->creditPhotography->getCreateTime());
        $this->assertEquals(0, $this->creditPhotography->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 CreditPhotography setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->creditPhotography->setId(1);
        $this->assertEquals(1, $this->creditPhotography->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //description 测试 ------------------------------------------------------- start
    /**
     * 设置 CreditPhotography setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->creditPhotography->setDescription('string');
        $this->assertEquals('string', $this->creditPhotography->getDescription());
    }

    /**
     * 设置 CreditPhotography setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->creditPhotography->setDescription(array(1, 2, 3));
    }
    //description 测试 -------------------------------------------------------   end
    
    //attachments 测试 ------------------------------------------------------- start
    /**
     * 设置 CreditPhotography setAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAttachmentsCorrectType()
    {
        $this->creditPhotography->setAttachments(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->creditPhotography->getAttachments());
    }

    /**
     * 设置 CreditPhotography setAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAttachmentsWrongType()
    {
        $this->creditPhotography->setAttachments('creditPhotography');
    }
    //attachments 测试 -------------------------------------------------------   end

    //member 测试 --------------------------------------------- start
    /**
     * 设置 setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $object = new Member();
        $this->creditPhotography->setMember($object);
        $this->assertSame($object, $this->creditPhotography->getMember());
    }

    /**
     * 设置 setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $this->creditPhotography->setMember('string');
    }
    //member 测试 ---------------------------------------------   end

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $object = new Crew();
        $this->creditPhotography->setApplyCrew($object);
        $this->assertSame($object, $this->creditPhotography->getApplyCrew());
    }

    /**
     * 设置 setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->creditPhotography->setApplyCrew('string');
    }
    //crew 测试 ---------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 CreditPhotography setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->creditPhotography->setStatus($actual);
        $this->assertEquals($expected, $this->creditPhotography->getStatus());
    }

    /**
     * 循环测试 CreditPhotography setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(CreditPhotography::STATUS['NORMAL'],CreditPhotography::STATUS['NORMAL']),
            array(CreditPhotography::STATUS['DELETED'],CreditPhotography::STATUS['DELETED']),
            array(999,CreditPhotography::STATUS['NORMAL']),
        );
    }

    /**
     * 设置 CreditPhotography setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->creditPhotography->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 ICreditPhotographyAdapter 调用 add 并且传参 $creditPhotography 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $creditPhotography = $this->getMockBuilder(MockCreditPhotography::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $member = \Base\Member\Utils\MockFactory::generateMember(1);
        $creditPhotography->setMember($member);

        //预言 ICreditPhotographyAdapter
        $repository = $this->prophesize(ICreditPhotographyAdapter::class);
        $repository->add(Argument::exact($creditPhotography))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $creditPhotography->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        //验证
        $result = $creditPhotography->add();
        $this->assertTrue($result);
    }

    /**
     * 测试添加失败-发布人
     * 1. 期望返回false
     * 2. 设置发布人为NullMember
     * 3. 调用add,期望返回false
     */
    public function testAddFailMemberNull()
    {
        $this->creditPhotography->setMember(new NullMember());

        $result = $this->creditPhotography->add();

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('memberId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testEdit()
    {
        $result = $this->creditPhotography->edit();
        $this->assertFalse($result);
    }

    public function testApproveAction()
    {
        //初始化
        $creditPhotography = $this->getMockBuilder(MockCreditPhotography::class)
                           ->setMethods(['updateApplyStatus'])
                           ->getMock();

        $creditPhotography->expects($this->exactly(1))
             ->method('updateApplyStatus')
             ->with(IApproveAble::APPLY_STATUS['APPROVE'])
             ->willReturn(true);
        
        //验证
        $result = $creditPhotography->approveAction();
        $this->assertTrue($result);
    }

    public function testRejectAction()
    {
        //初始化
        $creditPhotography = $this->getMockBuilder(MockCreditPhotography::class)
                           ->setMethods(['updateApplyStatus'])
                           ->getMock();

        $creditPhotography->expects($this->exactly(1))
             ->method('updateApplyStatus')
             ->with(IApproveAble::APPLY_STATUS['REJECT'])
             ->willReturn(true);
        
        //验证
        $result = $creditPhotography->rejectAction();
        $this->assertTrue($result);
    }

    public function testUpdateApplyStatus()
    {
        $creditPhotography = $this->getMockBuilder(MockCreditPhotography::class)
            ->setMethods(['getRepository', 'setUpdateTime', 'setStatusTime'])
            ->getMock();
            
        $applyStatus = IApproveAble::APPLY_STATUS['PENDING'];

        $creditPhotography->setApplyStatus($applyStatus);
        $creditPhotography->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));
        $creditPhotography->expects($this->exactly(1))->method('setStatusTime')->with(Core::$container->get('time'));

        $repository = $this->prophesize(ICreditPhotographyAdapter::class);
        $repository->edit(
            Argument::exact($creditPhotography),
            Argument::exact(array(
                'updateTime','applyStatus','statusTime', 'applyCrew', 'rejectReason'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $creditPhotography->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        $result = $creditPhotography->updateApplyStatus($applyStatus);
        $this->assertTrue($result);
    }

    public function testUpdateApplyStatusFail()
    {
        $applyStatus = IApproveAble::APPLY_STATUS['REJECT'];
        $this->creditPhotography->setApplyStatus($applyStatus);

        $result = $this->creditPhotography->updateApplyStatus($applyStatus);
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('applyStatus', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testDelete()
    {
        $creditPhotography = $this->getMockBuilder(MockCreditPhotography::class)
            ->setMethods(['getRepository', 'setUpdateTime', 'setStatusTime'])
            ->getMock();
            
        $status = CreditPhotography::STATUS['NORMAL'];

        $creditPhotography->setStatus($status);
        $creditPhotography->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));
        $creditPhotography->expects($this->exactly(1))->method('setStatusTime')->with(Core::$container->get('time'));

        $repository = $this->prophesize(ICreditPhotographyAdapter::class);
        $repository->edit(
            Argument::exact($creditPhotography),
            Argument::exact(array('updateTime','status','statusTime'))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $creditPhotography->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        $result = $creditPhotography->delete();
        $this->assertTrue($result);
    }

    public function testDeleteFail()
    {
        $status = CreditPhotography::STATUS['DELETED'];
        $this->creditPhotography->setStatus($status);

        $result = $this->creditPhotography->delete();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsNormal()
    {
        $this->creditPhotography->setStatus(CreditPhotography::STATUS['NORMAL']);

        $result = $this->creditPhotography->isNormal();
        $this->assertTrue($result);
    }
}
