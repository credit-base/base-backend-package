<?php
namespace Base\CreditPhotography\Model;

use PHPUnit\Framework\TestCase;

class NullCreditPhotographyTest extends TestCase
{
    private $creditPhotography;

    public function setUp()
    {
        $this->creditPhotography = $this->getMockBuilder(NullCreditPhotography::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->creditPhotography);
    }

    public function testExtendsCreditPhotography()
    {
        $this->assertInstanceof('Base\CreditPhotography\Model\CreditPhotography', $this->creditPhotography);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->creditPhotography);
    }

    public function testDelete()
    {
        $this->creditPhotography->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $this->creditPhotography->delete();
        $this->assertFalse($result);
    }
}
