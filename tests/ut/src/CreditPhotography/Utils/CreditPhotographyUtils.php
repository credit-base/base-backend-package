<?php
namespace Base\CreditPhotography\Utils;

trait CreditPhotographyUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $creditPhotography
    ) {
        $this->assertEquals($expectedArray['credit_photography_id'], $creditPhotography->getId());

        $this->assertEquals($expectedArray['description'], $creditPhotography->getDescription());
        $this->attachmentsEquals($expectedArray, $creditPhotography);
        $this->assertEquals($expectedArray['member_id'], $creditPhotography->getMember()->getId());
        if (isset($expectedArray['real_name'])) {
            $this->assertEquals($expectedArray['real_name'], $creditPhotography->getMember()->getRealName());
        }
        $this->assertEquals($expectedArray['apply_crew_id'], $creditPhotography->getApplyCrew()->getId());
        $this->assertEquals($expectedArray['apply_status'], $creditPhotography->getApplyStatus());
        $this->assertEquals($expectedArray['reject_reason'], $creditPhotography->getRejectReason());
        $this->assertEquals($expectedArray['status'], $creditPhotography->getStatus());
        $this->assertEquals($expectedArray['status'], $creditPhotography->getStatus());
        $this->assertEquals($expectedArray['create_time'], $creditPhotography->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $creditPhotography->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $creditPhotography->getStatusTime());
    }

    private function attachmentsEquals($expectedArray, $creditPhotography)
    {
        $attachments = array();

        if (is_string($expectedArray['attachments'])) {
            $attachments = json_decode($expectedArray['attachments'], true);
        }
        if (is_array($expectedArray['attachments'])) {
            $attachments = $expectedArray['attachments'];
        }

        $this->assertEquals($attachments, $creditPhotography->getAttachments());
    }
}
