<?php
namespace Base\CreditPhotography\Utils;

use Base\Common\Model\IApproveAble;

use Base\CreditPhotography\Model\CreditPhotography;

class MockFactory
{
    public static function generateCreditPhotography(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : CreditPhotography {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $creditPhotography = new CreditPhotography($id);
        $creditPhotography->setId($id);

        //description
        $description = isset($value['description']) ? $value['description'] : $faker->word();
        $creditPhotography->setDescription($description);

        //attachments
        $attachments = isset($value['attachments']) ? $value['attachments'] : array(
            array('name'=>$faker->name(), 'identify'=>$faker->word().'.png')
        );
        $creditPhotography->setAttachments($attachments);
        
        //rejectReason
        $rejectReason = isset($value['rejectReason']) ? $value['rejectReason'] : $faker->word();
        $creditPhotography->setRejectReason($rejectReason);

        self::generateMember($creditPhotography, $faker, $value);
        self::generateApplyCrew($creditPhotography, $faker, $value);
        self::generateStatus($creditPhotography, $faker, $value);
        self::generateApplyStatus($creditPhotography, $faker, $value);
        
        $creditPhotography->setCreateTime($faker->unixTime());
        $creditPhotography->setUpdateTime($faker->unixTime());
        $creditPhotography->setStatusTime($faker->unixTime());

        return $creditPhotography;
    }

    protected static function generateMember($creditPhotography, $faker, $value) : void
    {
        $member = isset($value['member']) ?
        $value['member'] :
        \Base\Member\Utils\MockFactory::generateMember($faker->randomDigit());
        
        $creditPhotography->setMember($member);
    }

    protected static function generateApplyCrew($creditPhotography, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $creditPhotography->setApplyCrew($crew);
    }

    protected static function generateStatus($creditPhotography, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                CreditPhotography::STATUS
            );
        
        $creditPhotography->setStatus($status);
    }

    protected static function generateApplyStatus($creditPhotography, $faker, $value) : void
    {
        //applyStatus
        $applyStatus = isset($value['applyStatus']) ?
        $value['applyStatus'] :
        $faker->randomElement(
            IApproveAble::APPLY_STATUS
        );

        $creditPhotography->setApplyStatus($applyStatus);
    }
}
