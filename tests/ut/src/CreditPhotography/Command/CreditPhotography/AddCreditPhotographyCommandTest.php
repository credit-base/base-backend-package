<?php
namespace Base\CreditPhotography\Command\CreditPhotography;

use PHPUnit\Framework\TestCase;

class AddCreditPhotographyCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'description' => $faker->word(),
            'attachments' => array($faker->word()),
            'member' => $faker->randomDigit(),
            'id' => $faker->randomDigit()
        );

        $this->command = new AddCreditPhotographyCommand(
            $this->fakerData['description'],
            $this->fakerData['attachments'],
            $this->fakerData['member'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testDescriptionParameter()
    {
        $this->assertEquals($this->fakerData['description'], $this->command->description);
    }

    public function testAttachmentsParameter()
    {
        $this->assertEquals($this->fakerData['attachments'], $this->command->attachments);
    }

    public function testMemberParameter()
    {
        $this->assertEquals($this->fakerData['member'], $this->command->member);
    }
}
