<?php
namespace Base\CreditPhotography\Command\CreditPhotography;

use PHPUnit\Framework\TestCase;

class RejectCreditPhotographyCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'applyCrew' => $faker->randomDigit(1),
            'rejectReason' => $faker->word(),
            'id' => $faker->randomDigit()
        );

        $this->command = new RejectCreditPhotographyCommand(
            $this->fakerData['applyCrew'],
            $this->fakerData['rejectReason'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testRejectReasonParameter()
    {
        $this->assertEquals($this->fakerData['rejectReason'], $this->command->rejectReason);
    }

    public function testApplyCrewParameter()
    {
        $this->assertEquals($this->fakerData['applyCrew'], $this->command->applyCrew);
    }
}
