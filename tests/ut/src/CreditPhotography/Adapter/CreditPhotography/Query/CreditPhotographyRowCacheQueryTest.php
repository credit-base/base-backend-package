<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography\Query;

use PHPUnit\Framework\TestCase;

class CreditPhotographyRowCacheQueryTest extends TestCase
{
    private $rowCacheQuery;

    public function setUp()
    {
        $this->rowCacheQuery = new MockCreditPhotographyRowCacheQuery();
    }

    public function tearDown()
    {
        unset($this->rowCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowCacheQuery', $this->rowCacheQuery);
    }

    /**
     * 测试是否cache层赋值正确
     */
    public function testCorrectCacheLayer()
    {
        $this->assertInstanceof(
            'Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence\CreditPhotographyCache',
            $this->rowCacheQuery->getCacheLayer()
        );
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence\CreditPhotographyDb',
            $this->rowCacheQuery->getDbLayer()
        );
    }
}
