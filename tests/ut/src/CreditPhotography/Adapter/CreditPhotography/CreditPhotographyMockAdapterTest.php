<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography;

use PHPUnit\Framework\TestCase;

use Base\CreditPhotography\Model\CreditPhotography;

class CreditPhotographyMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new CreditPhotographyMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new CreditPhotography()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new CreditPhotography(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\CreditPhotography\Model\CreditPhotography',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\CreditPhotography\Model\CreditPhotography',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\CreditPhotography\Model\CreditPhotography',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
