<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\CreditPhotography\Model\CreditPhotography;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class CreditPhotographyDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(CreditPhotographyDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchApplyCrew',
                                'fetchMember'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 ICreditPhotographyAdapter
     */
    public function testImplementsICreditPhotographyAdapter()
    {
        $this->assertInstanceOf(
            'Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 CreditPhotographyDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockCreditPhotographyDbAdapter();
        $this->assertInstanceOf(
            'Base\CreditPhotography\Translator\CreditPhotographyDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 CreditPhotographyRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockCreditPhotographyDbAdapter();
        $this->assertInstanceOf(
            'Base\CreditPhotography\Adapter\CreditPhotography\Query\CreditPhotographyRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetCrewRepository()
    {
        $adapter = new MockCreditPhotographyDbAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }

    public function testGetMemberRepository()
    {
        $adapter = new MockCreditPhotographyDbAdapter();
        $this->assertInstanceOf(
            'Base\Member\Repository\MemberRepository',
            $adapter->getMemberRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockCreditPhotographyDbAdapter();
        $this->assertInstanceOf('Marmot\Interfaces\INull', $adapter->getNullObject());
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedCreditPhotography = new CreditPhotography();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedCreditPhotography)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedCreditPhotography);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedCreditPhotography = new CreditPhotography();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedCreditPhotography, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedCreditPhotography, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedCreditPhotography = new CreditPhotography();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedCreditPhotography);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchMember')
                         ->with($expectedCreditPhotography);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchApplyCrew')
                         ->with($expectedCreditPhotography);
        //验证
        $creditPhotography = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedCreditPhotography, $creditPhotography);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $creditPhotographyOne = new CreditPhotography(1);
        $creditPhotographyTwo = new CreditPhotography(2);

        $ids = [1, 2];

        $expectedCreditPhotographyList = [];
        $expectedCreditPhotographyList[$creditPhotographyOne->getId()] = $creditPhotographyOne;
        $expectedCreditPhotographyList[$creditPhotographyTwo->getId()] = $creditPhotographyTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedCreditPhotographyList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchApplyCrew')
                         ->with($expectedCreditPhotographyList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchMember')
                         ->with($expectedCreditPhotographyList);
        //验证
        $creditPhotographyList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedCreditPhotographyList, $creditPhotographyList);
    }

    //fetchMember
    public function testFetchMemberIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockCreditPhotographyDbAdapter::class)
                           ->setMethods(['fetchMemberByObject'])
                           ->getMock();
        
        $creditPhotography = new CreditPhotography();

        $adapter->expects($this->exactly(1))
                         ->method('fetchMemberByObject')
                         ->with($creditPhotography);

        $adapter->fetchMember($creditPhotography);
    }

    public function testFetchMemberIsArray()
    {
        $adapter = $this->getMockBuilder(MockCreditPhotographyDbAdapter::class)
                           ->setMethods(['fetchMemberByList'])
                           ->getMock();
        
        $creditPhotographyOne = new CreditPhotography(1);
        $creditPhotographyTwo = new CreditPhotography(2);
        
        $creditPhotographyList[$creditPhotographyOne->getId()] = $creditPhotographyOne;
        $creditPhotographyList[$creditPhotographyTwo->getId()] = $creditPhotographyTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchMemberByList')
                         ->with($creditPhotographyList);

        $adapter->fetchMember($creditPhotographyList);
    }

    //fetchMemberByObject
    public function testFetchMemberByObject()
    {
        $adapter = $this->getMockBuilder(MockCreditPhotographyDbAdapter::class)
                           ->setMethods(['getMemberRepository'])
                           ->getMock();

        $creditPhotography = new CreditPhotography();
        $creditPhotography->setMember(new Member(1));
        
        $memberId = 1;

        // 为 Member 类建立预言
        $member  = $this->prophesize(Member::class);
        $memberRepository = $this->prophesize(MemberRepository::class);
        $memberRepository->fetchOne(Argument::exact($memberId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);

        $adapter->expects($this->exactly(1))
                         ->method('getMemberRepository')
                         ->willReturn($memberRepository->reveal());

        $adapter->fetchMemberByObject($creditPhotography);
    }

    //fetchMemberByList
    public function testFetchMemberByList()
    {
        $adapter = $this->getMockBuilder(MockCreditPhotographyDbAdapter::class)
                           ->setMethods(['getMemberRepository'])
                           ->getMock();

        $creditPhotographyOne = new CreditPhotography(1);
        $memberOne = new Member(1);
        $creditPhotographyOne->setMember($memberOne);

        $creditPhotographyTwo = new CreditPhotography(2);
        $memberTwo = new Member(2);
        $creditPhotographyTwo->setMember($memberTwo);
        $memberIds = [1, 2];
        
        $memberList[$memberOne->getId()] = $memberOne;
        $memberList[$memberTwo->getId()] = $memberTwo;
        
        $creditPhotographyList[$creditPhotographyOne->getId()] = $creditPhotographyOne;
        $creditPhotographyList[$creditPhotographyTwo->getId()] = $creditPhotographyTwo;

        $memberRepository = $this->prophesize(MemberRepository::class);
        $memberRepository->fetchList(Argument::exact($memberIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($memberList);

        $adapter->expects($this->exactly(1))
                         ->method('getMemberRepository')
                         ->willReturn($memberRepository->reveal());

        $adapter->fetchMemberByList($creditPhotographyList);
    }

    //fetchApplyCrew
    public function testFetchApplyCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockCreditPhotographyDbAdapter::class)
                           ->setMethods(['fetchApplyCrewByObject'])
                           ->getMock();
        
        $creditPhotography = new CreditPhotography();

        $adapter->expects($this->exactly(1))
                         ->method('fetchApplyCrewByObject')
                         ->with($creditPhotography);

        $adapter->fetchApplyCrew($creditPhotography);
    }

    public function testFetchApplyCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockCreditPhotographyDbAdapter::class)
                           ->setMethods(['fetchApplyCrewByList'])
                           ->getMock();
        
        $creditPhotographyOne = new CreditPhotography(1);
        $creditPhotographyTwo = new CreditPhotography(2);
        
        $creditPhotographyList[$creditPhotographyOne->getId()] = $creditPhotographyOne;
        $creditPhotographyList[$creditPhotographyTwo->getId()] = $creditPhotographyTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchApplyCrewByList')
                         ->with($creditPhotographyList);

        $adapter->fetchApplyCrew($creditPhotographyList);
    }

    //fetchApplyCrewByObject
    public function testFetchApplyCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockCreditPhotographyDbAdapter::class)
                           ->setMethods(['getCrewRepository'])
                           ->getMock();

        $creditPhotography = new CreditPhotography();
        $creditPhotography->setApplyCrew(new Crew(1));
        
        $crewId = 1;

        // 为 Crew 类建立预言
        $crew  = $this->prophesize(Crew::class);
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $adapter->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($crewRepository->reveal());

        $adapter->fetchApplyCrewByObject($creditPhotography);
    }

    //fetchApplyCrewByList
    public function testFetchApplyCrewByList()
    {
        $adapter = $this->getMockBuilder(MockCreditPhotographyDbAdapter::class)
                           ->setMethods(['getCrewRepository'])
                           ->getMock();

        $creditPhotographyOne = new CreditPhotography(1);
        $crewOne = new Crew(1);
        $creditPhotographyOne->setApplyCrew($crewOne);

        $creditPhotographyTwo = new CreditPhotography(2);
        $crewTwo = new Crew(2);
        $creditPhotographyTwo->setApplyCrew($crewTwo);
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $creditPhotographyList[$creditPhotographyOne->getId()] = $creditPhotographyOne;
        $creditPhotographyList[$creditPhotographyTwo->getId()] = $creditPhotographyTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crewList);

        $adapter->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($crewRepository->reveal());

        $adapter->fetchApplyCrewByList($creditPhotographyList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-member
    public function testFormatFilterMember()
    {
        $filter = array(
            'member' => 1
        );
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = 'member_id = '.$filter['member'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-realName
    public function testFormatFilterTitle()
    {
        $filter = array(
            'realName' => 1
        );
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = 'real_name LIKE \'%'.$filter['realName'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-status
    public function testFormatFilterStatus()
    {
        $filter = array(
            'status' => -2
        );
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = 'status = '.$filter['status'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-applyStatus
    public function testFormatFilterApplyStatus()
    {
        $filter = array(
            'applyStatus' => 0,-2
        );
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = 'apply_status IN ('.$filter['applyStatus'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--status
    public function testFormatSortStatusDesc()
    {
        $sort = array('status' => -1);
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = ' ORDER BY status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-status
    public function testFormatSortStatusAsc()
    {
        $sort = array('status' => 1);
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = ' ORDER BY status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--applyStatus
    public function testFormatSortApplyStatusDesc()
    {
        $sort = array('applyStatus' => -1);
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = ' ORDER BY apply_status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-applyStatus
    public function testFormatSortApplyStatusAsc()
    {
        $sort = array('applyStatus' => 1);
        $adapter = new MockCreditPhotographyDbAdapter();

        $expectedCondition = ' ORDER BY apply_status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
