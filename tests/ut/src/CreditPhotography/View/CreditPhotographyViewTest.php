<?php
namespace Base\CreditPhotography\View;

use PHPUnit\Framework\TestCase;

use Base\CreditPhotography\Model\CreditPhotography;

class CreditPhotographyViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $creditPhotography = new CreditPhotographyView(new CreditPhotography());
        $this->assertInstanceof('Base\Common\View\CommonView', $creditPhotography);
    }
}
