<?php
namespace Base\CreditPhotography\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class CreditPhotographySchemaTest extends TestCase
{
    private $creditPhotographySchema;

    private $creditPhotography;

    public function setUp()
    {
        $this->creditPhotographySchema = new CreditPhotographySchema(new Factory());

        $this->creditPhotography = \Base\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->creditPhotographySchema);
        unset($this->creditPhotography);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->creditPhotographySchema);
    }

    public function testGetId()
    {
        $result = $this->creditPhotographySchema->getId($this->creditPhotography);

        $this->assertEquals($result, $this->creditPhotography->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->creditPhotographySchema->getAttributes($this->creditPhotography);

        $this->assertEquals($result['description'], $this->creditPhotography->getDescription());
        $this->assertEquals($result['attachments'], $this->creditPhotography->getAttachments());
        $this->assertEquals($result['applyStatus'], $this->creditPhotography->getApplyStatus());
        $this->assertEquals($result['rejectReason'], $this->creditPhotography->getRejectReason());
        $this->assertEquals($result['status'], $this->creditPhotography->getStatus());
        $this->assertEquals($result['createTime'], $this->creditPhotography->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->creditPhotography->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->creditPhotography->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->creditPhotographySchema->getRelationships($this->creditPhotography, 0, array());

        $this->assertEquals($result['member'], ['data' => $this->creditPhotography->getMember()]);
        $this->assertEquals($result['applyCrew'], ['data' => $this->creditPhotography->getApplyCrew()]);
    }
}
