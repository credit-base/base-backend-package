<?php
namespace Base\CreditPhotography\Translator;

use PHPUnit\Framework\TestCase;

use Base\CreditPhotography\Utils\MockFactory;
use Base\CreditPhotography\Utils\CreditPhotographyUtils;

class CreditPhotographyDbTranslatorTest extends TestCase
{
    use CreditPhotographyUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new CreditPhotographyDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\CreditPhotography\Model\NullCreditPhotography', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $creditPhotography = MockFactory::generateCreditPhotography(1);

        $expression['credit_photography_id'] = $creditPhotography->getId();

        $expression['description'] = $creditPhotography->getDescription();
        $expression['attachments'] = $creditPhotography->getAttachments();
        $expression['member_id'] = $creditPhotography->getMember()->getId();
        $expression['apply_crew_id'] = $creditPhotography->getApplyCrew()->getId();
        $expression['reject_reason'] = $creditPhotography->getRejectReason();
        $expression['apply_status'] = $creditPhotography->getApplyStatus();
        $expression['status'] = $creditPhotography->getStatus();
        $expression['status_time'] = $creditPhotography->getStatusTime();
        $expression['create_time'] = $creditPhotography->getCreateTime();
        $expression['update_time'] = $creditPhotography->getUpdateTime();

        $creditPhotography = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\CreditPhotography\Model\CreditPhotography', $creditPhotography);
        $this->compareArrayAndObject($expression, $creditPhotography);
    }

    public function testArrayToObjectAttachments()
    {
        $creditPhotography = MockFactory::generateCreditPhotography(1);

        $expression['credit_photography_id'] = $creditPhotography->getId();

        $expression['description'] = $creditPhotography->getDescription();
        $expression['attachments'] =  json_encode($creditPhotography->getAttachments());
        $expression['member_id'] = $creditPhotography->getMember()->getId();
        $expression['apply_crew_id'] = $creditPhotography->getApplyCrew()->getId();
        $expression['apply_status'] = $creditPhotography->getApplyStatus();
        $expression['reject_reason'] = $creditPhotography->getRejectReason();
        $expression['status'] = $creditPhotography->getStatus();
        $expression['status_time'] = $creditPhotography->getStatusTime();
        $expression['create_time'] = $creditPhotography->getCreateTime();
        $expression['update_time'] = $creditPhotography->getUpdateTime();

        $creditPhotography = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\CreditPhotography\Model\CreditPhotography', $creditPhotography);
        $this->compareArrayAndObject($expression, $creditPhotography);
    }

    public function testObjectToArray()
    {
        $creditPhotography = MockFactory::generateCreditPhotography(1);

        $expression = $this->translator->objectToArray($creditPhotography);

        $this->compareArrayAndObject($expression, $creditPhotography);
    }
}
