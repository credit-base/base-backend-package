<?php
namespace Base\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class FailureDataTest extends TestCase
{
    public function testCorrectExtendsErrorData()
    {
        $model = new FailureData();
        $this->assertInstanceof('Base\ResourceCatalogData\Model\ErrorData', $model);
    }
}
