<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullSearchDataTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockNullSearchDataTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testResourceNotExist()
    {
        $result = $this->trait->publicResourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
