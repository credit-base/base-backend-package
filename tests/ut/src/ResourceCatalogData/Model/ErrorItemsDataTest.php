<?php
namespace Base\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class ErrorItemsDataTest extends TestCase
{
    private $errorItemsData;

    public function setUp()
    {
        $this->errorItemsData = new ErrorItemsData();
    }

    public function tearDown()
    {
        unset($this->errorItemsData);
    }

    public function testExtendsItemsData()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\ItemsData',
            $this->errorItemsData
        );
    }
}
