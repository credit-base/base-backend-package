<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Template\Model\BjTemplate;
use Base\Template\Model\NullBjTemplate;

use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class BjSearchDataTest extends TestCase
{
    private $bjSearchData;

    private $faker;

    public function setUp()
    {
        $this->bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
                        ->setMethods(['getRepository'])
                        ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->bjSearchData);
        unset($this->faker);
    }

    public function testSearchDataConstructor()
    {
        $this->assertEquals('', $this->bjSearchData->getDescription());
        $this->assertEquals(
            BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'],
            $this->bjSearchData->getFrontEndProcessorStatus()
        );
        $this->assertInstanceOf(
            'Base\Template\Model\BjTemplate',
            $this->bjSearchData->getTemplate()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\BjItemsData',
            $this->bjSearchData->getItemsData()
        );
    }

    //itemsData 测试 ---------------------------------------------------- start
    /**
     * 设置 setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $itemsData = new BjItemsData();

        $this->bjSearchData->setItemsData($itemsData);
        $this->assertEquals($itemsData, $this->bjSearchData->getItemsData());
    }

    /**
     * 设置 setItemsData() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $itemsData = array($this->faker->randomNumber());

        $this->bjSearchData->setItemsData($itemsData);
    }
    //itemsData 测试 ----------------------------------------------------   end
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->bjSearchData->setStatus($actual);
        $this->assertEquals($expected, $this->bjSearchData->getStatus());
    }
    /**
     * 循环测试 DispatchDepartment setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(BjSearchData::STATUS['CONFIRM'], BjSearchData::STATUS['CONFIRM']),
            array(BjSearchData::STATUS['ENABLED'], BjSearchData::STATUS['ENABLED']),
            array(BjSearchData::STATUS['DISABLED'], BjSearchData::STATUS['DISABLED']),
            array(BjSearchData::STATUS['DELETED'], BjSearchData::STATUS['DELETED']),
            array(999, BjSearchData::STATUS['CONFIRM'])
        );
    }
    /**
     * 设置 setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->bjSearchData->setStatus($this->faker->word());
    }
    //status 测试 ------------------------------------------------------   end
    //frontEndProcessorStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setFrontEndProcessorStatus() 是否符合预定范围
     *
     * @dataProvider frontEndProcessorStatusProvider
     */
    public function testSetFrontEndProcessorStatus($actual, $expected)
    {
        $this->bjSearchData->setFrontEndProcessorStatus($actual);
        $this->assertEquals($expected, $this->bjSearchData->getFrontEndProcessorStatus());
    }
    /**
     * 循环测试 DispatchDepartment setFrontEndProcessorStatus() 数据构建器
     */
    public function frontEndProcessorStatusProvider()
    {
        return array(
            array(
                BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'],
                BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT']
            ),
            array(
                BjSearchData::FRONT_END_PROCESSOR_STATUS['IMPORT'],
                BjSearchData::FRONT_END_PROCESSOR_STATUS['IMPORT']
            ),
            array(999, BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'])
        );
    }
    /**
     * 设置 setFrontEndProcessorStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFrontEndProcessorStatusWrongType()
    {
        $this->bjSearchData->setFrontEndProcessorStatus($this->faker->word());
    }
    //frontEndProcessorStatus 测试 ------------------------------------------------------   end
    
    //description 测试 ------------------------------------------------------- start
    /**
     * 设置 setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $description = $this->faker->word();

        $this->bjSearchData->setDescription($description);
        $this->assertEquals($description, $this->bjSearchData->getDescription());
    }

    /**
     * 设置 setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->bjSearchData->setDescription($this->faker->words(3, false));
    }
    //description 测试 -------------------------------------------------------   end

    public function testGetRepository()
    {
        $bjSearchData = new MockBjSearchData();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Repository\BjSearchDataSdkRepository',
            $bjSearchData->getRepository()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $bjSearchData->getRepository()
        );
    }

    public function testDelete()
    {
        $status = BjSearchData::STATUS['CONFIRM'];
        $this->bjSearchData->setStatus($status);

        $repository = $this->prophesize(IBjSearchDataAdapter::class);
        $repository->deletes(Argument::exact($this->bjSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->bjSearchData->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $this->bjSearchData->delete();
        
        $this->assertTrue($result);
    }

    public function testDeleteFail()
    {
        $status = BjSearchData::STATUS['DELETED'];
        $this->bjSearchData->setStatus($status);

        $result = $this->bjSearchData->delete();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsConfirm()
    {
        $this->bjSearchData->setStatus(BjSearchData::STATUS['CONFIRM']);

        $result = $this->bjSearchData->isConfirm();
        $this->assertTrue($result);
    }

    public function testIsEnabled()
    {
        $this->bjSearchData->setStatus(BjSearchData::STATUS['ENABLED']);

        $result = $this->bjSearchData->isEnabled();
        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        $status = BjSearchData::STATUS['CONFIRM'];
        $this->bjSearchData->setStatus($status);

        $repository = $this->prophesize(IBjSearchDataAdapter::class);
        $repository->confirm(Argument::exact($this->bjSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->bjSearchData->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $this->bjSearchData->confirm();
        
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $status = BjSearchData::STATUS['DELETED'];
        $this->bjSearchData->setStatus($status);

        $result = $this->bjSearchData->confirm();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $status = BjSearchData::STATUS['ENABLED'];
        $this->bjSearchData->setStatus($status);

        $repository = $this->prophesize(IBjSearchDataAdapter::class);
        $repository->disable(Argument::exact($this->bjSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->bjSearchData->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $this->bjSearchData->disable();
        
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $status = BjSearchData::STATUS['DELETED'];
        $this->bjSearchData->setStatus($status);

        $result = $this->bjSearchData->disable();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }
}
