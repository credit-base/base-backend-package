<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Template\Model\GbTemplate;
use Base\Template\Model\NullGbTemplate;

use Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class GbSearchDataTest extends TestCase
{
    private $gbSearchData;

    private $faker;

    public function setUp()
    {
        $this->gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
                            ->setMethods(['getRepository'])
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->gbSearchData);
        unset($this->faker);
    }

    public function testSearchDataConstructor()
    {
        $this->assertEquals('', $this->gbSearchData->getDescription());
        $this->assertEquals(
            GbSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'],
            $this->gbSearchData->getFrontEndProcessorStatus()
        );
        $this->assertInstanceOf(
            'Base\Template\Model\GbTemplate',
            $this->gbSearchData->getTemplate()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\GbItemsData',
            $this->gbSearchData->getItemsData()
        );
    }

    //itemsData 测试 ---------------------------------------------------- start
    /**
     * 设置 setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $itemsData = new GbItemsData();

        $this->gbSearchData->setItemsData($itemsData);
        $this->assertEquals($itemsData, $this->gbSearchData->getItemsData());
    }

    /**
     * 设置 setItemsData() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $itemsData = array($this->faker->randomNumber());

        $this->gbSearchData->setItemsData($itemsData);
    }
    //itemsData 测试 ----------------------------------------------------   end

    public function testGetRepository()
    {
        $gbSearchData = new MockGbSearchData();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $gbSearchData->getRepository()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Repository\GbSearchDataSdkRepository',
            $gbSearchData->getRepository()
        );
    }

    public function testDelete()
    {
        $status = GbSearchData::STATUS['CONFIRM'];
        $this->gbSearchData->setStatus($status);

        $repository = $this->prophesize(IGbSearchDataAdapter::class);
        $repository->deletes(Argument::exact($this->gbSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->gbSearchData->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $this->gbSearchData->delete();
        
        $this->assertTrue($result);
    }

    public function testDeleteFail()
    {
        $status = GbSearchData::STATUS['DELETED'];
        $this->gbSearchData->setStatus($status);

        $result = $this->gbSearchData->delete();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsConfirm()
    {
        $this->gbSearchData->setStatus(GbSearchData::STATUS['CONFIRM']);

        $result = $this->gbSearchData->isConfirm();
        $this->assertTrue($result);
    }

    public function testIsEnabled()
    {
        $this->gbSearchData->setStatus(GbSearchData::STATUS['ENABLED']);

        $result = $this->gbSearchData->isEnabled();
        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        $status = GbSearchData::STATUS['CONFIRM'];
        $this->gbSearchData->setStatus($status);

        $repository = $this->prophesize(IGbSearchDataAdapter::class);
        $repository->confirm(Argument::exact($this->gbSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->gbSearchData->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $this->gbSearchData->confirm();
        
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $status = GbSearchData::STATUS['DELETED'];
        $this->gbSearchData->setStatus($status);

        $result = $this->gbSearchData->confirm();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $status = GbSearchData::STATUS['ENABLED'];
        $this->gbSearchData->setStatus($status);

        $repository = $this->prophesize(IGbSearchDataAdapter::class);
        $repository->disable(Argument::exact($this->gbSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->gbSearchData->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $this->gbSearchData->disable();
        
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $status = GbSearchData::STATUS['DELETED'];
        $this->gbSearchData->setStatus($status);

        $result = $this->gbSearchData->disable();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }
}
