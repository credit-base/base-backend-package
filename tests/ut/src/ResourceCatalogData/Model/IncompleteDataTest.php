<?php
namespace Base\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class IncompleteDataTest extends TestCase
{
    public function testCorrectExtendsErrorData()
    {
        $model = new IncompleteData();
        $this->assertInstanceof('Base\ResourceCatalogData\Model\ErrorData', $model);
    }
}
