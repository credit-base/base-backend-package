<?php
namespace Base\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class GbItemsDataTest extends TestCase
{
    private $gbItemsData;

    public function setUp()
    {
        $this->gbItemsData = new GbItemsData();
    }

    public function tearDown()
    {
        unset($this->gbItemsData);
    }

    public function testExtendsItemsData()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\ItemsData',
            $this->gbItemsData
        );
    }
}
