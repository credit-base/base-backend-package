<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Model\Crew;
use Base\UserGroup\Model\UserGroup;
use Base\Template\Model\Template;
use Base\ResourceCatalogData\Adapter\Task\ITaskAdapter;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class TaskTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = new Task();
    }

    public function tearDown()
    {
        unset($this->task);
    }

    /**
     * Task 测试构造函数
     */
    public function testConstructor()
    {
        $this->assertEquals(0, $this->task->getId());

        $this->assertInstanceof('Base\Crew\Model\Crew', $this->task->getCrew());
        $this->assertInstanceof('Base\UserGroup\Model\UserGroup', $this->task->getUserGroup());
        $this->assertEquals(0, $this->task->getPid());
        $this->assertEquals(0, $this->task->getTotal());
        $this->assertEquals(0, $this->task->getSuccessNumber());
        $this->assertEquals(0, $this->task->getFailureNumber());
        $this->assertEquals(0, $this->task->getSourceCategory());
        $this->assertEquals(0, $this->task->getSourceTemplate());
        $this->assertEquals(0, $this->task->getTargetCategory());
        $this->assertEquals(0, $this->task->getTargetTemplate());
        $this->assertEquals(0, $this->task->getTargetRule());
        $this->assertEquals(0, $this->task->getScheduleTask());
        $this->assertEquals(0, $this->task->getErrorNumber());
        $this->assertEquals('', $this->task->getFileName());

        $this->assertEquals(Core::$container->get('time'), $this->task->getCreateTime());

        $this->assertEquals(Task::STATUS['DEFAULT'], $this->task->getStatus());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Task setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->task->setId(1);
        $this->assertEquals(1, $this->task->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //crew 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->task->setCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->task->getCrew());
    }

    /**
     * 设置 Task setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->task->setCrew('crew');
    }
    //crew 测试 --------------------------------------------------------   end

    //userGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->task->setUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->task->getUserGroup());
    }

    /**
     * 设置 Task setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->task->setUserGroup('userGroup');
    }
    //userGroup 测试 --------------------------------------------------------   end

    //pid 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setPid() 正确的传参类型,期望传值正确
     */
    public function testSetPidCorrectType()
    {
        $this->task->setPid(1);
        $this->assertEquals(1, $this->task->getPid());
    }

    /**
     * 设置 Task setPid() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPidWrongType()
    {
        $this->task->setPid(array(1,2,3));
    }
    //pid 测试 --------------------------------------------------------   end

    //total 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setTotal() 正确的传参类型,期望传值正确
     */
    public function testSetTotalCorrectType()
    {
        $this->task->setTotal(1);
        $this->assertEquals(1, $this->task->getTotal());
    }

    /**
     * 设置 Task setTotal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalWrongType()
    {
        $this->task->setTotal(array(1,2,3));
    }
    //total 测试 --------------------------------------------------------   end

    //successNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setSuccessNumber() 正确的传参类型,期望传值正确
     */
    public function testSetSuccessNumberCorrectType()
    {
        $this->task->setSuccessNumber(1);
        $this->assertEquals(1, $this->task->getSuccessNumber());
    }

    /**
     * 设置 Task setSuccessNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSuccessNumberWrongType()
    {
        $this->task->setSuccessNumber(array(1,2,3));
    }
    //successNumber 测试 --------------------------------------------------------   end

    //failureNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setFailureNumber() 正确的传参类型,期望传值正确
     */
    public function testSetFailureNumberCorrectType()
    {
        $this->task->setFailureNumber(1);
        $this->assertEquals(1, $this->task->getFailureNumber());
    }

    /**
     * 设置 Task setFailureNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFailureNumberWrongType()
    {
        $this->task->setFailureNumber(array(1,2,3));
    }
    //failureNumber 测试 --------------------------------------------------------   end

    //sourceCategory 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Task setSourceCategory() 是否符合预定范围
     *
     * @dataProvider sourceCategoryProvider
     */
    public function testSetSourceCategory($actual, $expected)
    {
        $this->task->setSourceCategory($actual);
        $this->assertEquals($expected, $this->task->getSourceCategory());
    }

    /**
     * 循环测试 Task setSourceCategory() 数据构建器
     */
    public function sourceCategoryProvider()
    {
        return array(
            array(Template::CATEGORY['WBJ'], Template::CATEGORY['WBJ']),
            array(Template::CATEGORY['BJ'], Template::CATEGORY['BJ']),
            array(Template::CATEGORY['GB'], Template::CATEGORY['GB']),
            array(Template::CATEGORY['QZJ_WBJ'], Template::CATEGORY['QZJ_WBJ']),
            array(Template::CATEGORY['QZJ_BJ'], Template::CATEGORY['QZJ_BJ']),
            array(Template::CATEGORY['QZJ_GB'], Template::CATEGORY['QZJ_GB']),
            array(999, 0),
        );
    }

    /**
     * 设置 Task setSourceCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceCategoryWrongType()
    {
        $this->task->setSourceCategory('string');
    }
    //sourceCategory 测试 ------------------------------------------------------   end

    //sourceTemplate 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setSourceTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetSourceTemplateCorrectType()
    {
        $this->task->setSourceTemplate(1);
        $this->assertEquals(1, $this->task->getSourceTemplate());
    }

    /**
     * 设置 Task setSourceTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceTemplateWrongType()
    {
        $this->task->setSourceTemplate(array(1,2,3));
    }
    //sourceTemplate 测试 --------------------------------------------------------   end

    //targetCategory 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Task setTargetCategory() 是否符合预定范围
     *
     * @dataProvider targetCategoryProvider
     */
    public function testSetTargetCategory($actual, $expected)
    {
        $this->task->setTargetCategory($actual);
        $this->assertEquals($expected, $this->task->getTargetCategory());
    }

    /**
     * 循环测试 Task setTargetCategory() 数据构建器
     */
    public function targetCategoryProvider()
    {
        return array(
            array(Template::CATEGORY['WBJ'], Template::CATEGORY['WBJ']),
            array(Template::CATEGORY['BJ'], Template::CATEGORY['BJ']),
            array(Template::CATEGORY['GB'], Template::CATEGORY['GB']),
            array(Template::CATEGORY['QZJ_WBJ'], Template::CATEGORY['QZJ_WBJ']),
            array(Template::CATEGORY['QZJ_BJ'], Template::CATEGORY['QZJ_BJ']),
            array(Template::CATEGORY['QZJ_GB'], Template::CATEGORY['QZJ_GB']),
            array(999, 0),
        );
    }

    /**
     * 设置 Task setTargetCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetCategoryWrongType()
    {
        $this->task->setTargetCategory('string');
    }
    //targetCategory 测试 ------------------------------------------------------   end

    //targetTemplate 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setTargetTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTargetTemplateCorrectType()
    {
        $this->task->setTargetTemplate(1);
        $this->assertEquals(1, $this->task->getTargetTemplate());
    }

    /**
     * 设置 Task setTargetTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetTemplateWrongType()
    {
        $this->task->setTargetTemplate(array(1,2,3));
    }
    //targetTemplate 测试 --------------------------------------------------------   end

    //targetRule 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setTargetRule() 正确的传参类型,期望传值正确
     */
    public function testSetTargetRuleCorrectType()
    {
        $this->task->setTargetRule(1);
        $this->assertEquals(1, $this->task->getTargetRule());
    }

    /**
     * 设置 Task setTargetRule() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetRuleWrongType()
    {
        $this->task->setTargetRule(array(1,2,3));
    }
    //targetRule 测试 --------------------------------------------------------   end

    //scheduleTask 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setScheduleTask() 正确的传参类型,期望传值正确
     */
    public function testSetScheduleTaskCorrectType()
    {
        $this->task->setScheduleTask(1);
        $this->assertEquals(1, $this->task->getScheduleTask());
    }

    /**
     * 设置 Task setScheduleTask() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetScheduleTaskWrongType()
    {
        $this->task->setScheduleTask(array(1,2,3));
    }
    //scheduleTask 测试 --------------------------------------------------------   end

    //errorNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setErrorNumber() 正确的传参类型,期望传值正确
     */
    public function testSetErrorNumberCorrectType()
    {
        $this->task->setErrorNumber(1);
        $this->assertEquals(1, $this->task->getErrorNumber());
    }

    /**
     * 设置 Task setErrorNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorNumberWrongType()
    {
        $this->task->setErrorNumber(array(1,2,3));
    }
    //errorNumber 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Task setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->task->setStatus($actual);
        $this->assertEquals($expected, $this->task->getStatus());
    }

    /**
     * 循环测试 Task setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Task::STATUS['SUCCESS'], Task::STATUS['SUCCESS']),
            array(Task::STATUS['FAILURE'], Task::STATUS['FAILURE']),
            array(Task::STATUS['FAILURE_FILE_DOWNLOAD'], Task::STATUS['FAILURE_FILE_DOWNLOAD']),
            array(999, Task::STATUS['DEFAULT']),
        );
    }

    /**
     * 设置 Task setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->task->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    //fileName 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setFileName() 正确的传参类型,期望传值正确
     */
    public function testSetFileNameCorrectType()
    {
        $this->task->setFileName('fileName');
        $this->assertEquals('fileName', $this->task->getFileName());
    }

    /**
     * 设置 Task setFileName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFileNameWrongType()
    {
        $this->task->setFileName(array(1,2,3));
    }
    //fileName 测试 --------------------------------------------------------   end

    public function testIsFailureStatus()
    {
        $this->task->setStatus(Task::STATUS['FAILURE']);

        $result = $this->task->isFailureStatus();
        $this->assertTrue($result);
    }

    public function testIsFailureFileDownloadStatus()
    {
        $this->task->setStatus(Task::STATUS['FAILURE_FILE_DOWNLOAD']);

        $result = $this->task->isFailureFileDownloadStatus();
        $this->assertTrue($result);
    }
}
