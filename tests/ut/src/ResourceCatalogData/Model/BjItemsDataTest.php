<?php
namespace Base\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class BjItemsDataTest extends TestCase
{
    private $bjItemsData;

    public function setUp()
    {
        $this->bjItemsData = new BjItemsData();
    }

    public function tearDown()
    {
        unset($this->bjItemsData);
    }

    public function testExtendsItemsData()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\ItemsData',
            $this->bjItemsData
        );
    }
}
