<?php
namespace Base\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullWbjSearchDataTest extends TestCase
{
    private $gbStub;

    public function setUp()
    {
        $this->gbStub = $this->getMockBuilder(NullWbjSearchData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->gbStub);
    }

    public function testExtendsWbjSearchData()
    {
        $this->assertInstanceof('Base\ResourceCatalogData\Model\WbjSearchData', $this->gbStub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->gbStub);
    }

    public function testDisable()
    {
        $this->mockResourceNotExist();
        
        $result = $this->gbStub->disable();
        $this->assertFalse($result);
    }

    public function testIsEnabled()
    {
        $this->mockResourceNotExist();
        
        $result = $this->gbStub->isEnabled();
        $this->assertFalse($result);
    }

    private function mockResourceNotExist()
    {
        $this->gbStub->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }
}
