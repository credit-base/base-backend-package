<?php
namespace Base\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullBjSearchDataTest extends TestCase
{
    private $nullBjSearchData;

    public function setUp()
    {
        $this->nullBjSearchData = $this->getMockBuilder(NullBjSearchData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->nullBjSearchData);
    }

    public function testExtendsBjSearchData()
    {
        $this->assertInstanceof('Base\ResourceCatalogData\Model\BjSearchData', $this->nullBjSearchData);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->nullBjSearchData);
    }

    public function testConfirm()
    {
        $this->mockResourceNotExist();
        
        $result = $this->nullBjSearchData->confirm();
        $this->assertFalse($result);
    }

    public function testIsConfirm()
    {
        $this->mockResourceNotExist();
        
        $result = $this->nullBjSearchData->isConfirm();
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $this->mockResourceNotExist();
        
        $result = $this->nullBjSearchData->disable();
        $this->assertFalse($result);
    }

    public function testIsEnabled()
    {
        $this->mockResourceNotExist();
        
        $result = $this->nullBjSearchData->isEnabled();
        $this->assertFalse($result);
    }

    public function testDelete()
    {
        $this->mockResourceNotExist();
        
        $result = $this->nullBjSearchData->delete();
        $this->assertFalse($result);
    }
    
    private function mockResourceNotExist()
    {
        $this->nullBjSearchData->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }
}
