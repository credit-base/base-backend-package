<?php
namespace Base\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullErrorItemsDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullErrorItemsData::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsErrorItemsData()
    {
        $this->assertInstanceof('Base\ResourceCatalogData\Model\ErrorItemsData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
