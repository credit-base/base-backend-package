<?php
namespace Base\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class WbjItemsDataTest extends TestCase
{
    private $wbjItemsData;

    public function setUp()
    {
        $this->wbjItemsData = new WbjItemsData();
    }

    public function tearDown()
    {
        unset($this->wbjItemsData);
    }

    public function testExtendsItemsData()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\ItemsData',
            $this->wbjItemsData
        );
    }
}
