<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Template\Model\WbjTemplate;
use Base\Template\Model\NullWbjTemplate;

use Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class WbjSearchDataTest extends TestCase
{
    private $wbjSearchData;

    private $faker;

    public function setUp()
    {
        $this->wbjSearchData = $this->getMockBuilder(MockWbjSearchData::class)
                            ->setMethods(['getRepository'])
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->wbjSearchData);
        unset($this->faker);
    }

    public function testExtendsSearchData()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\SearchData',
            $this->wbjSearchData
        );
    }
    
    public function testSearchDataConstructor()
    {
        $this->assertInstanceOf(
            'Base\Template\Model\WbjTemplate',
            $this->wbjSearchData->getTemplate()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\WbjItemsData',
            $this->wbjSearchData->getItemsData()
        );
    }

    //itemsData 测试 ---------------------------------------------------- start
    /**
     * 设置 setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $itemsData = new WbjItemsData();

        $this->wbjSearchData->setItemsData($itemsData);
        $this->assertEquals($itemsData, $this->wbjSearchData->getItemsData());
    }

    /**
     * 设置 setItemsData() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $itemsData = array($this->faker->randomNumber());

        $this->wbjSearchData->setItemsData($itemsData);
    }
    //itemsData 测试 ----------------------------------------------------   end

    public function testGetRepository()
    {
        $wbjSearchData = new MockWbjSearchData();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $wbjSearchData->getRepository()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Repository\WbjSearchDataSdkRepository',
            $wbjSearchData->getRepository()
        );
    }

    public function testIsEnabled()
    {
        $this->wbjSearchData->setStatus(WbjSearchData::STATUS['ENABLED']);

        $result = $this->wbjSearchData->isEnabled();
        $this->assertTrue($result);
    }

    public function testDisable()
    {
        $status = WbjSearchData::STATUS['ENABLED'];
        $this->wbjSearchData->setStatus($status);

        $repository = $this->prophesize(IWbjSearchDataAdapter::class);
        $repository->disable(Argument::exact($this->wbjSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->wbjSearchData->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $this->wbjSearchData->disable();
        
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $status = WbjSearchData::STATUS['DISABLED'];
        $this->wbjSearchData->setStatus($status);

        $result = $this->wbjSearchData->disable();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }
}
