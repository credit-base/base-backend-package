<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Model\Crew;
use Base\Crew\Model\NullCrew;
use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Model\NullUserGroup;

use Base\Template\Model\Template;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class SearchDataTest extends TestCase
{
    private $searchData;

    private $faker;

    public function setUp()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
                                ->getMockForAbstractClass();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->searchData);
        unset($this->faker);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->searchData
        );
    }

    public function testImplementsISearchDataAble()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\ISearchDataAble',
            $this->searchData
        );
    }

    public function testSearchDataConstructor()
    {
        $this->assertEquals(0, $this->searchData->getId());
        $this->assertEquals('', $this->searchData->getName());
        $this->assertEquals('', $this->searchData->getIdentify());
        $this->assertEquals(0, $this->searchData->getInfoClassify());
        $this->assertEquals(0, $this->searchData->getInfoCategory());
        $this->assertEquals(0, $this->searchData->getSubjectCategory());
        $this->assertEquals(0, $this->searchData->getDimension());
        $this->assertEquals(SearchData::EXPIRATION_DATE_DEFAULT, $this->searchData->getExpirationDate());
        $this->assertEquals('', $this->searchData->getHash());
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->searchData->getCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->searchData->getSourceUnit()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\Task',
            $this->searchData->getTask()
        );
    }

    public function testSetId()
    {
        $id = $this->faker->randomNumber();
        $this->searchData->setId($id);
        $this->assertEquals($id, $this->searchData->getId());
    }
    
    //name 测试 ------------------------------------------------------- start
    /**
     * 设置 setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $name = $this->faker->title();

        $this->searchData->setName($name);
        $this->assertEquals($name, $this->searchData->getName());
    }

    /**
     * 设置 setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->searchData->setName($this->faker->words(3, false));
    }
    //name 测试 -------------------------------------------------------   end

    //identify 测试 ------------------------------------------------------- start
    /**
     * 设置 setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $identify = $this->faker->title();

        $this->searchData->setIdentify($identify);
        $this->assertEquals($identify, $this->searchData->getIdentify());
    }

    /**
     * 设置 setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->searchData->setIdentify($this->faker->words(3, false));
    }
    //identify 测试 -------------------------------------------------------   end

    //infoClassify 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setInfoClassify() 是否符合预定范围
     *
     * @dataProvider infoClassifyProvider
     */
    public function testSetInfoClassify($actual, $expected)
    {
        $this->searchData->setInfoClassify($actual);
        $this->assertEquals($expected, $this->searchData->getInfoClassify());
    }
    /**
     * 循环测试 DispatchDepartment setInfoClassify() 数据构建器
     */
    public function infoClassifyProvider()
    {
        return array(
            array(ISearchDataAble::INFO_CLASSIFY['XZXK'], ISearchDataAble::INFO_CLASSIFY['XZXK']),
            array(ISearchDataAble::INFO_CLASSIFY['XZCF'], ISearchDataAble::INFO_CLASSIFY['XZCF']),
            array(ISearchDataAble::INFO_CLASSIFY['HONGMD'], ISearchDataAble::INFO_CLASSIFY['HONGMD']),
            array(ISearchDataAble::INFO_CLASSIFY['HEIMD'], ISearchDataAble::INFO_CLASSIFY['HEIMD']),
            array(ISearchDataAble::INFO_CLASSIFY['QT'], ISearchDataAble::INFO_CLASSIFY['QT'])
        );
    }

    //template 测试 ---------------------------------------------------- start
    /**
     * 设置 setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
            ->setMethods(['getItemsData'])
            ->getMock();

        $template = \Base\Template\Utils\BjTemplateMockFactory::generateBjTemplate($this->faker->randomNumber());
        $items = array(
            array('identify'=>'ZTMC', 'type' => 1),
            array('identify'=>'TYSHXYDM', 'type' => 1),
        );
        $template->setItems($items);

        $itemsData = array();
        foreach ($items as $item) {
            $itemsData[$item['identify']] = isset(
                Template::TEMPLATE_TYPE_DEFAULT[$item['type']]
            ) ? Template::TEMPLATE_TYPE_DEFAULT[$item['type']] : '';
        }

        $bjItemsData = new BjItemsData();
        $bjItemsData->setData($itemsData);

        $this->searchData->expects($this->any())
                    ->method('getItemsData')
                    ->willReturn($bjItemsData);

        $this->searchData->setTemplate($template);

        $this->assertEquals($template, $this->searchData->getTemplate());
        $this->assertEquals($itemsData, $this->searchData->getItemsData()->getData());
    }

    /**
     * 设置 setTemplate() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $template = array($this->faker->randomNumber());

        $this->searchData->setTemplate($template);
    }
    //template 测试 ----------------------------------------------------   end
    
    /**
     * 设置 setInfoClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoClassifyWrongType()
    {
        $this->searchData->setInfoClassify($this->faker->word());
    }
    //infoClassify 测试 ------------------------------------------------------   end
    
    //infoCategory 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setInfoCategory() 是否符合预定范围
     *
     * @dataProvider infoCategoryProvider
     */
    public function testSetInfoCategory($actual, $expected)
    {
        $this->searchData->setInfoCategory($actual);
        $this->assertEquals($expected, $this->searchData->getInfoCategory());
    }
    /**
     * 循环测试 DispatchDepartment setInfoCategory() 数据构建器
     */
    public function infoCategoryProvider()
    {
        return array(
            array(ISearchDataAble::INFO_CATEGORY['JCXX'], ISearchDataAble::INFO_CATEGORY['JCXX']),
            array(ISearchDataAble::INFO_CATEGORY['SHOUXXX'], ISearchDataAble::INFO_CATEGORY['SHOUXXX']),
            array(ISearchDataAble::INFO_CATEGORY['SHIXXX'], ISearchDataAble::INFO_CATEGORY['SHIXXX']),
            array(ISearchDataAble::INFO_CATEGORY['QTXX'], ISearchDataAble::INFO_CATEGORY['QTXX'])
        );
    }
    /**
     * 设置 setInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoCategoryWrongType()
    {
        $this->searchData->setInfoCategory($this->faker->word());
    }
    //infoCategory 测试 ------------------------------------------------------   end
    
    //crew 测试 ---------------------------------------------------- start
    /**
     * 设置 setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $crew = new Crew();

        $this->searchData->setCrew($crew);
        $this->assertEquals($crew, $this->searchData->getCrew());
    }

    /**
     * 设置 setCrew() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $crew = array($this->faker->randomNumber());

        $this->searchData->setCrew($crew);
    }
    //crew 测试 ----------------------------------------------------   end
    
    //sourceUnit 测试 ---------------------------------------------------- start
    /**
     * 设置 setSourceUnit() 正确的传参类型,期望传值正确
     */
    public function testSetSourceUnitCorrectType()
    {
        $sourceUnit = new UserGroup();

        $this->searchData->setSourceUnit($sourceUnit);
        $this->assertEquals($sourceUnit, $this->searchData->getSourceUnit());
    }

    /**
     * 设置 setSourceUnit() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceUnitWrongType()
    {
        $sourceUnit = array($this->faker->randomNumber());

        $this->searchData->setSourceUnit($sourceUnit);
    }
    //sourceUnit 测试 ----------------------------------------------------   end
        
    //subjectCategory 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setSubjectCategory() 是否符合预定范围
     *
     * @dataProvider subjectCategoryProvider
     */
    public function testSetSubjectCategory($actual, $expected)
    {
        $this->searchData->setSubjectCategory($actual);
        $this->assertEquals($expected, $this->searchData->getSubjectCategory());
    }
    /**
     * 循环测试 DispatchDepartment setSubjectCategory() 数据构建器
     */
    public function subjectCategoryProvider()
    {
        return array(
            array(ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'], ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ']),
            array(ISearchDataAble::SUBJECT_CATEGORY['ZRR'], ISearchDataAble::SUBJECT_CATEGORY['ZRR']),
            array(ISearchDataAble::SUBJECT_CATEGORY['GTGSH'], ISearchDataAble::SUBJECT_CATEGORY['GTGSH'])
        );
    }
    /**
     * 设置 setSubjectCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectCategoryWrongType()
    {
        $this->searchData->setSubjectCategory($this->faker->word());
    }
    //subjectCategory 测试 ------------------------------------------------------   end
    
    //dimension 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setDimension() 是否符合预定范围
     *
     * @dataProvider dimensionProvider
     */
    public function testSetDimension($actual, $expected)
    {
        $this->searchData->setDimension($actual);
        $this->assertEquals($expected, $this->searchData->getDimension());
    }
    /**
     * 循环测试 DispatchDepartment setDimension() 数据构建器
     */
    public function dimensionProvider()
    {
        return array(
            array(ISearchDataAble::DIMENSION['SHGK'], ISearchDataAble::DIMENSION['SHGK']),
            array(ISearchDataAble::DIMENSION['ZWGX'], ISearchDataAble::DIMENSION['ZWGX']),
            array(ISearchDataAble::DIMENSION['SQCX'], ISearchDataAble::DIMENSION['SQCX'])
        );
    }
    /**
     * 设置 setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->searchData->setDimension($this->faker->word());
    }
    //dimension 测试 ------------------------------------------------------   end
    
    //expirationDate 测试 ------------------------------------------------------- start
    /**
     * 设置 setExpirationDate() 正确的传参类型,期望传值正确
     */
    public function testSetExpirationDateCorrectType()
    {
        $expirationDate = $this->faker->unixTime();

        $this->searchData->setExpirationDate($expirationDate);
        $this->assertEquals($expirationDate, $this->searchData->getExpirationDate());
    }

    /**
     * 设置 setExpirationDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExpirationDateWrongType()
    {
        $this->searchData->setExpirationDate($this->faker->words(3, false));
    }
    //expirationDate 测试 -------------------------------------------------------   end
    
    //hash 测试 ------------------------------------------------------- start
    /**
     * 设置 setHash() 正确的传参类型,期望传值正确
     */
    public function testSetHashCorrectType()
    {
        $hash = $this->faker->md5();

        $this->searchData->setHash($hash);
        $this->assertEquals($hash, $this->searchData->getHash());
    }

    /**
     * 设置 setHash() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHashWrongType()
    {
        $this->searchData->setHash($this->faker->words(3, false));
    }
    //hash 测试 -------------------------------------------------------   end

    //task 测试 ---------------------------------------------------- start
    /**
     * 设置 setTask() 正确的传参类型,期望传值正确
     */
    public function testSetTaskCorrectType()
    {
        $task = new Task();

        $this->searchData->setTask($task);
        $this->assertEquals($task, $this->searchData->getTask());
    }

    /**
     * 设置 setTask() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTaskWrongType()
    {
        $task = array($this->faker->randomNumber());

        $this->searchData->setTask($task);
    }
    //task 测试 ----------------------------------------------------   end
}
