<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\BjSearchData;

class BjSearchDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $bjSearchData = new BjSearchDataView(new BjSearchData());
        $this->assertInstanceof('Base\Common\View\CommonView', $bjSearchData);
    }
}
