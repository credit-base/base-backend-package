<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\WbjSearchData;

class WbjSearchDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $wbjSearchData = new WbjSearchDataView(new WbjSearchData());
        $this->assertInstanceof('Base\Common\View\CommonView', $wbjSearchData);
    }
}
