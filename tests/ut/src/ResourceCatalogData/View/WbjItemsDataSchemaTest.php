<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class WbjItemsDataSchemaTest extends TestCase
{
    private $wbjItemsDataSchema;

    private $wbjItemsData;

    public function setUp()
    {
        $this->wbjItemsDataSchema = new WbjItemsDataSchema(new Factory());

        $this->wbjItemsData = \Base\ResourceCatalogData\Utils\WbjItemsDataMockFactory::generateWbjItemsData(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->wbjItemsDataSchema);
        unset($this->wbjItemsData);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->wbjItemsDataSchema);
    }

    public function testGetId()
    {
        $result = $this->wbjItemsDataSchema->getId($this->wbjItemsData);

        $this->assertEquals($result, $this->wbjItemsData->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->wbjItemsDataSchema->getAttributes($this->wbjItemsData);

        $this->assertEquals($result['data'], $this->wbjItemsData->getData());
    }
}
