<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\WbjItemsData;

class WbjItemsDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $wbjItemsData = new WbjItemsDataView(new WbjItemsData());
        $this->assertInstanceof('Base\Common\View\CommonView', $wbjItemsData);
    }
}
