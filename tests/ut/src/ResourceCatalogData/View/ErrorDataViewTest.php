<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\ErrorData;

class ErrorDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $errorData = new ErrorDataView(new ErrorData());
        $this->assertInstanceof('Base\Common\View\CommonView', $errorData);
    }
}
