<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\GbSearchData;

class GbSearchDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $gbSearchData = new GbSearchDataView(new GbSearchData());
        $this->assertInstanceof('Base\Common\View\CommonView', $gbSearchData);
    }
}
