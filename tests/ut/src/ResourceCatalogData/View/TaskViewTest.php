<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\Task;

class TaskViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $task = new TaskView(new Task());
        $this->assertInstanceof('Base\Common\View\CommonView', $task);
    }
}
