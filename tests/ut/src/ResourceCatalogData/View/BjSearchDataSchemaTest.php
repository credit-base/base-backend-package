<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class BjSearchDataSchemaTest extends TestCase
{
    private $bjSearchDataSchema;

    private $bjSearchData;

    public function setUp()
    {
        $this->bjSearchDataSchema = new BjSearchDataSchema(new Factory());

        $this->bjSearchData = \Base\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->bjSearchDataSchema);
        unset($this->bjSearchData);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->bjSearchDataSchema);
    }

    public function testGetId()
    {
        $result = $this->bjSearchDataSchema->getId($this->bjSearchData);

        $this->assertEquals($result, $this->bjSearchData->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->bjSearchDataSchema->getAttributes($this->bjSearchData);

        $this->assertEquals($result['infoClassify'], $this->bjSearchData->getInfoClassify());
        $this->assertEquals($result['infoCategory'], $this->bjSearchData->getInfoCategory());
        $this->assertEquals($result['subjectCategory'], $this->bjSearchData->getSubjectCategory());
        $this->assertEquals($result['dimension'], $this->bjSearchData->getDimension());
        $this->assertEquals($result['name'], $this->bjSearchData->getName());
        $this->assertEquals($result['identify'], $this->bjSearchData->getIdentify());
        $this->assertEquals($result['expirationDate'], $this->bjSearchData->getExpirationDate());
        $this->assertEquals($result['description'], $this->bjSearchData->getDescription());
        $this->assertEquals($result['frontEndProcessorStatus'], $this->bjSearchData->getFrontEndProcessorStatus());
        $this->assertEquals($result['status'], $this->bjSearchData->getStatus());
        $this->assertEquals($result['createTime'], $this->bjSearchData->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->bjSearchData->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->bjSearchData->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->bjSearchDataSchema->getRelationships($this->bjSearchData, 0, array());

        $this->assertEquals($result['itemsData'], ['data' => $this->bjSearchData->getItemsData()]);
        $this->assertEquals($result['template'], ['data' => $this->bjSearchData->getTemplate()]);
        $this->assertEquals($result['sourceUnit'], ['data' => $this->bjSearchData->getSourceUnit()]);
        $this->assertEquals($result['crew'], ['data' => $this->bjSearchData->getCrew()]);
    }
}
