<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class WbjSearchDataSchemaTest extends TestCase
{
    private $wbjSearchDataSchema;

    private $wbjSearchData;

    public function setUp()
    {
        $this->wbjSearchDataSchema = new WbjSearchDataSchema(new Factory());

        $this->wbjSearchData = \Base\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->wbjSearchDataSchema);
        unset($this->wbjSearchData);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->wbjSearchDataSchema);
    }

    public function testGetId()
    {
        $result = $this->wbjSearchDataSchema->getId($this->wbjSearchData);

        $this->assertEquals($result, $this->wbjSearchData->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->wbjSearchDataSchema->getAttributes($this->wbjSearchData);

        $this->assertEquals($result['infoClassify'], $this->wbjSearchData->getInfoClassify());
        $this->assertEquals($result['infoCategory'], $this->wbjSearchData->getInfoCategory());
        $this->assertEquals($result['subjectCategory'], $this->wbjSearchData->getSubjectCategory());
        $this->assertEquals($result['dimension'], $this->wbjSearchData->getDimension());
        $this->assertEquals($result['name'], $this->wbjSearchData->getName());
        $this->assertEquals($result['identify'], $this->wbjSearchData->getIdentify());
        $this->assertEquals($result['expirationDate'], $this->wbjSearchData->getExpirationDate());
        $this->assertEquals($result['status'], $this->wbjSearchData->getStatus());
        $this->assertEquals($result['createTime'], $this->wbjSearchData->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->wbjSearchData->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->wbjSearchData->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->wbjSearchDataSchema->getRelationships($this->wbjSearchData, 0, array());

        $this->assertEquals($result['itemsData'], ['data' => $this->wbjSearchData->getItemsData()]);
        $this->assertEquals($result['template'], ['data' => $this->wbjSearchData->getTemplate()]);
        $this->assertEquals($result['sourceUnit'], ['data' => $this->wbjSearchData->getSourceUnit()]);
        $this->assertEquals($result['crew'], ['data' => $this->wbjSearchData->getCrew()]);
    }
}
