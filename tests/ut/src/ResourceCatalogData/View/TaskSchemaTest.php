<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

use Base\ResourceCatalogData\Model\Task;

class TaskSchemaTest extends TestCase
{
    private $taskSchema;

    private $task;

    public function setUp()
    {
        $this->taskSchema = new TaskSchema(new Factory());

        $this->task = \Base\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->taskSchema);
        unset($this->task);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->taskSchema);
    }

    public function testGetId()
    {
        $result = $this->taskSchema->getId($this->task);

        $this->assertEquals($result, $this->task->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->taskSchema->getAttributes($this->task);

        $fileName = $this->task->getFileName();
        $errorFileName = Task::ERROR_FILE_NAME_PREFIX.'_'.$this->task->getSourceCategory().
        '_'.$this->task->getTargetTemplate().'_'.$this->task->getTargetCategory().'_'.$fileName;
        $fileName = $this->task->isFailureFileDownloadStatus() ? $errorFileName : $fileName;

        $this->assertEquals($result['pid'], $this->task->getPid());
        $this->assertEquals($result['total'], $this->task->getTotal());
        $this->assertEquals($result['successNumber'], $this->task->getSuccessNumber());
        $this->assertEquals($result['failureNumber'], $this->task->getFailureNumber());
        $this->assertEquals($result['sourceCategory'], $this->task->getSourceCategory());
        $this->assertEquals($result['sourceTemplate'], $this->task->getSourceTemplate());
        $this->assertEquals($result['targetCategory'], $this->task->getTargetCategory());
        $this->assertEquals($result['targetTemplate'], $this->task->getTargetTemplate());
        $this->assertEquals($result['targetRule'], $this->task->getTargetRule());
        $this->assertEquals($result['scheduleTask'], $this->task->getScheduleTask());
        $this->assertEquals($result['errorNumber'], $this->task->getErrorNumber());
        $this->assertEquals($result['fileName'], $fileName);

        $this->assertEquals($result['status'], $this->task->getStatus());
        $this->assertEquals($result['createTime'], $this->task->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->task->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->task->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->taskSchema->getRelationships($this->task, 0, array());

        $this->assertEquals($result['userGroup'], ['data' => $this->task->getUserGroup()]);
        $this->assertEquals($result['crew'], ['data' => $this->task->getCrew()]);
    }
}
