<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class BjItemsDataSchemaTest extends TestCase
{
    private $bjItemsDataSchema;

    private $bjItemsData;

    public function setUp()
    {
        $this->bjItemsDataSchema = new BjItemsDataSchema(new Factory());

        $this->bjItemsData = \Base\ResourceCatalogData\Utils\BjItemsDataMockFactory::generateBjItemsData(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->bjItemsDataSchema);
        unset($this->bjItemsData);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->bjItemsDataSchema);
    }

    public function testGetId()
    {
        $result = $this->bjItemsDataSchema->getId($this->bjItemsData);

        $this->assertEquals($result, $this->bjItemsData->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->bjItemsDataSchema->getAttributes($this->bjItemsData);

        $this->assertEquals($result['data'], $this->bjItemsData->getData());
    }
}
