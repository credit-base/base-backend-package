<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\BjItemsData;

class BjItemsDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $bjItemsData = new BjItemsDataView(new BjItemsData());
        $this->assertInstanceof('Base\Common\View\CommonView', $bjItemsData);
    }
}
