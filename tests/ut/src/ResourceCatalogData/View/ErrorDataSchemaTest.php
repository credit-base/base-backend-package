<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class ErrorDataSchemaTest extends TestCase
{
    private $errorDataSchema;

    private $errorData;

    public function setUp()
    {
        $this->errorDataSchema = new ErrorDataSchema(new Factory());

        $this->errorData = \Base\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->errorDataSchema);
        unset($this->errorData);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->errorDataSchema);
    }

    public function testGetId()
    {
        $result = $this->errorDataSchema->getId($this->errorData);

        $this->assertEquals($result, $this->errorData->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->errorDataSchema->getAttributes($this->errorData);

        $this->assertEquals($result['category'], $this->errorData->getCategory());
        $this->assertEquals($result['itemsData'], $this->errorData->getItemsData()->getData());
        $this->assertEquals($result['errorType'], $this->errorData->getErrorType());
        $this->assertEquals($result['errorReason'], $this->errorData->getErrorReason());
        $this->assertEquals($result['status'], $this->errorData->getStatus());
        $this->assertEquals($result['createTime'], $this->errorData->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->errorData->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->errorData->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->errorDataSchema->getRelationships($this->errorData, 0, array());

        $this->assertEquals($result['template'], ['data' => $this->errorData->getTemplate()]);
        $this->assertEquals($result['task'], ['data' => $this->errorData->getTask()]);
    }
}
