<?php
namespace Base\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\GbItemsData;

class GbItemsDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $gbItemsData = new GbItemsDataView(new GbItemsData());
        $this->assertInstanceof('Base\Common\View\CommonView', $gbItemsData);
    }
}
