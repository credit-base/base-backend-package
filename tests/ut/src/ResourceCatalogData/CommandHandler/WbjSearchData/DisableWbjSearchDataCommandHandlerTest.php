<?php
namespace Base\ResourceCatalogData\CommandHandler\WbjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\Command\WbjSearchData\DisableWbjSearchDataCommand;

class DisableWbjSearchDataCommandHandlerTest extends TestCase
{
    private $disableStub;

    private $faker;

    public function setUp()
    {
        $this->disableStub = $this->getMockBuilder(DisableWbjSearchDataCommandHandler::class)
                                     ->setMethods(['fetchWbjSearchData'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->disableStub);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->disableStub
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->disableStub->execute($command);
    }

    public function testExecute()
    {
        $command = new DisableWbjSearchDataCommand(
            $this->faker->randomNumber(1)
        );

        $wbjSearchData = $this->prophesize(WbjSearchData::class);
        $wbjSearchData->disable()->shouldBeCalledTimes(1)->willReturn(true);

        $this->disableStub->expects($this->once())
             ->method('fetchWbjSearchData')
             ->with($command->id)
             ->willReturn($wbjSearchData->reveal());

        $result = $this->disableStub->execute($command);
        
        $this->assertTrue($result);
    }
}
