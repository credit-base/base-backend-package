<?php
namespace Base\ResourceCatalogData\CommandHandler\GbSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Command\GbSearchData\DeleteGbSearchDataCommand;

class DeleteGbSearchDataCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(DeleteGbSearchDataCommandHandler::class)
                                     ->setMethods(['fetchGbSearchData'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $command = new DeleteGbSearchDataCommand(
            $this->faker->randomNumber()
        );

        $gbSearchData = $this->prophesize(GbSearchData::class);
        $gbSearchData->delete()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
             ->method('fetchGbSearchData')
             ->with($command->id)
             ->willReturn($gbSearchData->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}
