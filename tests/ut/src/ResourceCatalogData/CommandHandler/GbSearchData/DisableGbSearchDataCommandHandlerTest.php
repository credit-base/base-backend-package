<?php
namespace Base\ResourceCatalogData\CommandHandler\GbSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Command\GbSearchData\DisableGbSearchDataCommand;

class DisableGbSearchDataCommandHandlerTest extends TestCase
{
    private $disableStub;

    private $faker;

    public function setUp()
    {
        $this->disableStub = $this->getMockBuilder(DisableGbSearchDataCommandHandler::class)
                                     ->setMethods(['fetchGbSearchData'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->disableStub);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->disableStub
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->disableStub->execute($command);
    }

    public function testExecute()
    {
        $command = new DisableGbSearchDataCommand(
            $this->faker->randomNumber(1)
        );

        $gbSearchData = $this->prophesize(GbSearchData::class);
        $gbSearchData->disable()->shouldBeCalledTimes(1)->willReturn(true);

        $this->disableStub->expects($this->once())
             ->method('fetchGbSearchData')
             ->with($command->id)
             ->willReturn($gbSearchData->reveal());

        $result = $this->disableStub->execute($command);
        
        $this->assertTrue($result);
    }
}
