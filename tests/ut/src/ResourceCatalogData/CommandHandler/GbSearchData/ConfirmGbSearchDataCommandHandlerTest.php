<?php
namespace Base\ResourceCatalogData\CommandHandler\GbSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Command\GbSearchData\ConfirmGbSearchDataCommand;

class ConfirmGbSearchDataCommandHandlerTest extends TestCase
{
    private $confirmStub;

    private $faker;

    public function setUp()
    {
        $this->confirmStub = $this->getMockBuilder(ConfirmGbSearchDataCommandHandler::class)
                                     ->setMethods(['fetchGbSearchData'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->confirmStub);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->confirmStub
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->confirmStub->execute($command);
    }

    public function testExecute()
    {
        $command = new ConfirmGbSearchDataCommand(
            $this->faker->randomNumber(1)
        );

        $gbSearchData = $this->prophesize(GbSearchData::class);
        $gbSearchData->confirm()->shouldBeCalledTimes(1)->willReturn(true);

        $this->confirmStub->expects($this->once())
             ->method('fetchGbSearchData')
             ->with($command->id)
             ->willReturn($gbSearchData->reveal());

        $result = $this->confirmStub->execute($command);
        
        $this->assertTrue($result);
    }
}
