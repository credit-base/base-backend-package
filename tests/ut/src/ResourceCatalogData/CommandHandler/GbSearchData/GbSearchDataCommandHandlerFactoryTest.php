<?php
namespace Base\ResourceCatalogData\CommandHandler\GbSearchData;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Base\ResourceCatalogData\Command\GbSearchData\ConfirmGbSearchDataCommand;
use Base\ResourceCatalogData\Command\GbSearchData\DisableGbSearchDataCommand;
use Base\ResourceCatalogData\Command\GbSearchData\DeleteGbSearchDataCommand;

class GbSearchDataCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new GbSearchDataCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testDisableGbSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableGbSearchDataCommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\CommandHandler\GbSearchData\DisableGbSearchDataCommandHandler',
            $commandHandler
        );
    }

    public function testConfirmGbSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ConfirmGbSearchDataCommand(
                $this->faker->randomNumber(3)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\CommandHandler\GbSearchData\ConfirmGbSearchDataCommandHandler',
            $commandHandler
        );
    }

    public function testDeleteGbSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DeleteGbSearchDataCommand(
                $this->faker->randomNumber(3)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\CommandHandler\GbSearchData\DeleteGbSearchDataCommandHandler',
            $commandHandler
        );
    }
}
