<?php
namespace Base\ResourceCatalogData\CommandHandler\BjSearchData;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Repository\BjSearchDataSdkRepository;

class BjSearchDataCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockBjSearchDataCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Repository\BjSearchDataSdkRepository',
            $this->trait->publicGetRepository()
        );
    }

    public function testFetchBjSearchData()
    {
        $trait = $this->getMockBuilder(
            MockBjSearchDataCommandHandlerTrait::class
        )->setMethods(['getRepository'])->getMock();

        $id = 1;
        $bjSearchData = \Base\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData($id);

        $repository = $this->prophesize(BjSearchDataSdkRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($bjSearchData);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchBjSearchData($id);

        $this->assertEquals($result, $bjSearchData);
    }
}
