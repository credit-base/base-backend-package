<?php
namespace Base\ResourceCatalogData\CommandHandler\BjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Command\BjSearchData\DeleteBjSearchDataCommand;

class DeleteBjSearchDataCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(DeleteBjSearchDataCommandHandler::class)
                                     ->setMethods(['fetchBjSearchData'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $command = new DeleteBjSearchDataCommand(
            $this->faker->randomNumber()
        );

        $bjSearchData = $this->prophesize(BjSearchData::class);
        $bjSearchData->delete()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
             ->method('fetchBjSearchData')
             ->with($command->id)
             ->willReturn($bjSearchData->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}
