<?php
namespace Base\ResourceCatalogData\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;
use Base\ResourceCatalogData\Model\NullWbjSearchData;
use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\View\WbjSearchDataView;

class WbjSearchDataFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(WbjSearchDataFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockWbjSearchDataFetchController();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Repository\WbjSearchDataSdkRepository',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockWbjSearchDataFetchController();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\View\WbjSearchDataView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockWbjSearchDataFetchController();

        $this->assertEquals(
            'wbjSearchData',
            $controller->getResourceName()
        );
    }
}
