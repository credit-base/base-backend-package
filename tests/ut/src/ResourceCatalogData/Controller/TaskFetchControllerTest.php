<?php
namespace Base\ResourceCatalogData\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\ResourceCatalogData\Adapter\Task\ITaskAdapter;
use Base\ResourceCatalogData\Model\NullTask;
use Base\ResourceCatalogData\Model\Task;
use Base\ResourceCatalogData\View\TaskView;

class TaskFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(TaskFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockTaskFetchController();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Repository\TaskSdkRepository',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\Task\ITaskAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockTaskFetchController();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\View\TaskView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockTaskFetchController();

        $this->assertEquals(
            'tasks',
            $controller->getResourceName()
        );
    }
}
