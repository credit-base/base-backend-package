<?php
namespace Base\ResourceCatalogData\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use Base\ResourceCatalogData\Model\NullBjSearchData;
use Base\ResourceCatalogData\View\BjSearchDataView;
use Base\ResourceCatalogData\Model\BjSearchData;

class BjSearchDataFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(BjSearchDataFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockBjSearchDataFetchController();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Repository\BjSearchDataSdkRepository',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockBjSearchDataFetchController();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\View\BjSearchDataView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockBjSearchDataFetchController();

        $this->assertEquals(
            'bjSearchData',
            $controller->getResourceName()
        );
    }
}
