<?php
namespace Base\ResourceCatalogData\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\CommandBus;

use Base\ResourceCatalogData\Utils\WbjSearchDataMockFactory;
use Base\ResourceCatalogData\Repository\WbjSearchDataSdkRepository;
use Base\ResourceCatalogData\Command\WbjSearchData\DisableWbjSearchDataCommand;

class WbjSearchDataStatusControllerTest extends TestCase
{
    private $wbjStub;

    public function setUp()
    {
        $this->wbjStub = $this->getMockBuilder(MockWbjSearchDataStatusController::class)
        ->setMethods([
            'displayError',
            ])
        ->getMock();
    }

    public function teatDown()
    {
        unset($this->wbjStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $this->wbjStub->getRepository()
        );

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Repository\WbjSearchDataSdkRepository',
            $this->wbjStub->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->wbjStub->getCommandBus()
        );
    }

    private function initialDisable($result)
    {
        $this->wbjStub = $this->getMockBuilder(MockWbjSearchDataStatusController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new DisableWbjSearchDataCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->wbjStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDisableFailure()
    {
        $command = $this->initialDisable(false);

        $this->wbjStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->wbjStub->disable($command->id);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $command = $this->initialDisable(true);

        $crew = WbjSearchDataMockFactory::generateWbjSearchData($command->id);

        $repository = $this->prophesize(WbjSearchDataSdkRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);
        $this->wbjStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->wbjStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->wbjStub->disable($command->id);
        $this->assertTrue($result);
    }
}
