<?php
namespace Base\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\NullWbjItemsData;
use Base\ResourceCatalogData\Utils\WbjItemsDataSdkUtils;

use BaseSdk\ResourceCatalogData\Model\WbjItemsData as WbjItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullWbjItemsData as NullWbjItemsDataSdk;

class WbjItemsDataSdkTranslatorTest extends TestCase
{
    use WbjItemsDataSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WbjItemsDataSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $wbjItemsData = \Base\ResourceCatalogData\Utils\WbjItemsDataMockFactory::generateWbjItemsData(1);

        $wbjItemsDataSdk = new WbjItemsDataSdk(
            $wbjItemsData->getId(),
            $wbjItemsData->getData(),
            $wbjItemsData->getStatus(),
            $wbjItemsData->getStatusTime(),
            $wbjItemsData->getCreateTime(),
            $wbjItemsData->getUpdateTime()
        );

        $wbjItemsDataObject = $this->translator->valueObjectToObject($wbjItemsDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\WbjItemsData', $wbjItemsDataObject);
        $this->compareValueObjectAndObject($wbjItemsDataSdk, $wbjItemsDataObject);
    }

    public function testValueObjectToObjectFail()
    {
        $wbjItemsDataSdk = NullWbjItemsDataSdk::getInstance();

        $wbjItemsData = $this->translator->valueObjectToObject($wbjItemsDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\NullWbjItemsData', $wbjItemsData);
    }

    public function testObjectToValueObjectFail()
    {
        $wbjItemsData = array();

        $wbjItemsDataSdk = $this->translator->objectToValueObject($wbjItemsData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\WbjItemsData', $wbjItemsDataSdk);
    }
}
