<?php
namespace Base\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\NullErrorItemsData;
use Base\ResourceCatalogData\Utils\ErrorItemsDataSdkUtils;

use BaseSdk\ResourceCatalogData\Model\ErrorItemsData as ErrorItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullErrorItemsData as NullErrorItemsDataSdk;

class ErrorItemsDataSdkTranslatorTest extends TestCase
{
    use ErrorItemsDataSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ErrorItemsDataSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $errorItemsData = \Base\ResourceCatalogData\Utils\ErrorItemsDataMockFactory::generateErrorItemsData(1);

        $errorItemsDataSdk = new ErrorItemsDataSdk(
            $errorItemsData->getId(),
            $errorItemsData->getData(),
            $errorItemsData->getStatus(),
            $errorItemsData->getStatusTime(),
            $errorItemsData->getCreateTime(),
            $errorItemsData->getUpdateTime()
        );

        $errorItemsDataObject = $this->translator->valueObjectToObject($errorItemsDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\ErrorItemsData', $errorItemsDataObject);
        $this->compareValueObjectAndObject($errorItemsDataSdk, $errorItemsDataObject);
    }

    public function testValueObjectToObjectFail()
    {
        $errorItemsDataSdk = NullErrorItemsDataSdk::getInstance();

        $errorItemsData = $this->translator->valueObjectToObject($errorItemsDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\NullErrorItemsData', $errorItemsData);
    }

    public function testObjectToValueObjectFail()
    {
        $errorItemsData = array();

        $errorItemsDataSdk = $this->translator->objectToValueObject($errorItemsData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ErrorItemsData', $errorItemsDataSdk);
    }
}
