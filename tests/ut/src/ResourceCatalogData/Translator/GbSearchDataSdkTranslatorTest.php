<?php
namespace Base\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Translator\ISdkTranslator;

use Base\ResourceCatalogData\Model\NullGbSearchData;
use Base\ResourceCatalogData\Utils\GbSearchDataSdkUtils;

use BaseSdk\ResourceCatalogData\Model\Task as TaskSdk;
use BaseSdk\ResourceCatalogData\Model\GbItemsData as GbItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\GbSearchData as GbSearchDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullGbSearchData as NullGbSearchDataSdk;

use Base\Template\Translator\GbTemplateSdkTranslator;
use BaseSdk\Template\Model\GbTemplate as GbTemplateSdk;

use BaseSdk\Crew\Model\Crew as CrewSdk;
use Base\Crew\Translator\CrewSdkTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbSearchDataSdkTranslatorTest extends TestCase
{
    use GbSearchDataSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(GbSearchDataSdkTranslator::class)
                    ->setMethods([
                        'getCrewSdkTranslator',
                        'getTaskSdkTranslator',
                        'getGbItemsDataSdkTranslator',
                        'getGbTemplateSdkTranslator',
                    ])
                    ->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetCrewSdkTranslator()
    {
        $translator = new MockGbSearchDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\Crew\Translator\CrewSdkTranslator',
            $translator->getCrewSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getCrewSdkTranslator()
        );
    }

    public function testGetTaskSdkTranslator()
    {
        $translator = new MockGbSearchDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Translator\TaskSdkTranslator',
            $translator->getTaskSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getTaskSdkTranslator()
        );
    }

    public function testGetGbItemsDataSdkTranslator()
    {
        $translator = new MockGbSearchDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Translator\GbItemsDataSdkTranslator',
            $translator->getGbItemsDataSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getGbItemsDataSdkTranslator()
        );
    }

    public function testGetGbTemplateSdkTranslator()
    {
        $translator = new MockGbSearchDataSdkTranslator();

        $this->assertInstanceOf(
            'Base\Template\Translator\GbTemplateSdkTranslator',
            $translator->getGbTemplateSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getGbTemplateSdkTranslator()
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $gbSearchData = \Base\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData(1);

        $crewSdk = new CrewSdk($gbSearchData->getCrew()->getId());
        $taskSdk = new TaskSdk($gbSearchData->getTask()->getId());
        $gbTemplateSdk = new GbTemplateSdk($gbSearchData->getTemplate()->getId());
        $gbItemsDataSdk = new GbItemsDataSdk($gbSearchData->getItemsData()->getId());

        $gbSearchDataSdk = new GbSearchDataSdk(
            $gbSearchData->getId(),
            $gbSearchData->getName(),
            $gbSearchData->getIdentify(),
            $gbSearchData->getInfoClassify(),
            $gbSearchData->getInfoCategory(),
            $crewSdk,
            $gbSearchData->getSubjectCategory(),
            $gbSearchData->getDimension(),
            $gbSearchData->getExpirationDate(),
            $gbSearchData->getHash(),
            $gbTemplateSdk,
            $gbItemsDataSdk,
            $taskSdk,
            $gbSearchData->getDescription(),
            $gbSearchData->getFrontEndProcessorStatus(),
            $gbSearchData->getStatus(),
            $gbSearchData->getStatusTime(),
            $gbSearchData->getCreateTime(),
            $gbSearchData->getUpdateTime()
        );

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->valueObjectToObject(Argument::exact($crewSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbSearchData->getCrew());

        $gbTemplateSdkTranslator = $this->prophesize(GbTemplateSdkTranslator::class);
        $gbTemplateSdkTranslator->valueObjectToObject(Argument::exact($gbTemplateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbSearchData->getTemplate());

        $gbItemsDataSdkTranslator = $this->prophesize(GbItemsDataSdkTranslator::class);
        $gbItemsDataSdkTranslator->valueObjectToObject(Argument::exact($gbItemsDataSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbSearchData->getItemsData());

        $taskSdkTranslator = $this->prophesize(TaskSdkTranslator::class);
        $taskSdkTranslator->valueObjectToObject(Argument::exact($taskSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbSearchData->getTask());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getGbTemplateSdkTranslator')
            ->willReturn($gbTemplateSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getGbItemsDataSdkTranslator')
            ->willReturn($gbItemsDataSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTaskSdkTranslator')
            ->willReturn($taskSdkTranslator->reveal());

        $gbSearchDataObject = $this->translator->valueObjectToObject($gbSearchDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\GbSearchData', $gbSearchDataObject);
        $this->compareValueObjectAndObject($gbSearchDataSdk, $gbSearchDataObject);
    }

    public function testValueObjectToObjectFail()
    {
        $gbSearchDataSdk = array();

        $gbSearchData = $this->translator->valueObjectToObject($gbSearchDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\NullGbSearchData', $gbSearchData);
    }

    public function testObjectToValueObjectFail()
    {
        $gbSearchData = array();

        $gbSearchDataSdk = $this->translator->objectToValueObject($gbSearchData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\GbSearchData', $gbSearchDataSdk);
    }

    public function testObjectToValueObject()
    {
        $gbSearchData = \Base\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData(1);
        $crewSdk = new CrewSdk($gbSearchData->getCrew()->getId());
        $gbTemplateSdk = new GbTemplateSdk($gbSearchData->getTemplate()->getId());
        $taskSdk = new TaskSdk($gbSearchData->getTask()->getId());
        $gbItemsDataSdk = new GbItemsDataSdk($gbSearchData->getItemsData()->getId());

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->objectToValueObject(Argument::exact($gbSearchData->getCrew()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crewSdk);

        $gbTemplateSdkTranslator = $this->prophesize(GbTemplateSdkTranslator::class);
        $gbTemplateSdkTranslator->objectToValueObject(Argument::exact($gbSearchData->getTemplate()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbTemplateSdk);

        $taskSdkTranslator = $this->prophesize(TaskSdkTranslator::class);
        $taskSdkTranslator->objectToValueObject(Argument::exact($gbSearchData->getTask()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($taskSdk);

        $gbItemsDataSdkTranslator = $this->prophesize(GbItemsDataSdkTranslator::class);
        $gbItemsDataSdkTranslator->objectToValueObject(Argument::exact($gbSearchData->getItemsData()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbItemsDataSdk);
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getGbItemsDataSdkTranslator')
            ->willReturn($gbItemsDataSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getGbTemplateSdkTranslator')
            ->willReturn($gbTemplateSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTaskSdkTranslator')
            ->willReturn($taskSdkTranslator->reveal());

        $gbSearchDataSdk = $this->translator->objectToValueObject($gbSearchData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\GbSearchData', $gbSearchDataSdk);
        $this->compareValueObjectAndObject($gbSearchDataSdk, $gbSearchData);
    }
}
