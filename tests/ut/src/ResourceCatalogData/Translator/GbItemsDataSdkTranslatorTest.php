<?php
namespace Base\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\NullGbItemsData;
use Base\ResourceCatalogData\Utils\GbItemsDataSdkUtils;

use BaseSdk\ResourceCatalogData\Model\GbItemsData as GbItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullGbItemsData as NullGbItemsDataSdk;

class GbItemsDataSdkTranslatorTest extends TestCase
{
    use GbItemsDataSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbItemsDataSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $gbItemsData = \Base\ResourceCatalogData\Utils\GbItemsDataMockFactory::generateGbItemsData(1);

        $gbItemsDataSdk = new GbItemsDataSdk(
            $gbItemsData->getId(),
            $gbItemsData->getData(),
            $gbItemsData->getStatus(),
            $gbItemsData->getStatusTime(),
            $gbItemsData->getCreateTime(),
            $gbItemsData->getUpdateTime()
        );

        $gbItemsDataObject = $this->translator->valueObjectToObject($gbItemsDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\GbItemsData', $gbItemsDataObject);
        $this->compareValueObjectAndObject($gbItemsDataSdk, $gbItemsDataObject);
    }

    public function testValueObjectToObjectFail()
    {
        $gbItemsDataSdk = NullGbItemsDataSdk::getInstance();

        $gbItemsData = $this->translator->valueObjectToObject($gbItemsDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\NullGbItemsData', $gbItemsData);
    }

    public function testObjectToValueObjectFail()
    {
        $gbItemsData = array();

        $gbItemsDataSdk = $this->translator->objectToValueObject($gbItemsData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\GbItemsData', $gbItemsDataSdk);
    }
}
