<?php
namespace Base\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Translator\ISdkTranslator;

use Base\ResourceCatalogData\Model\NullBjSearchData;
use Base\ResourceCatalogData\Utils\BjSearchDataSdkUtils;

use BaseSdk\ResourceCatalogData\Model\Task as TaskSdk;
use BaseSdk\ResourceCatalogData\Model\BjItemsData as BjItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\BjSearchData as BjSearchDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullBjSearchData as NullBjSearchDataSdk;

use Base\Template\Translator\BjTemplateSdkTranslator;
use BaseSdk\Template\Model\BjTemplate as BjTemplateSdk;

use BaseSdk\Crew\Model\Crew as CrewSdk;
use Base\Crew\Translator\CrewSdkTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjSearchDataSdkTranslatorTest extends TestCase
{
    use BjSearchDataSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(BjSearchDataSdkTranslator::class)
                    ->setMethods([
                        'getCrewSdkTranslator',
                        'getTaskSdkTranslator',
                        'getBjItemsDataSdkTranslator',
                        'getBjTemplateSdkTranslator',
                    ])
                    ->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetCrewSdkTranslator()
    {
        $translator = new MockBjSearchDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\Crew\Translator\CrewSdkTranslator',
            $translator->getCrewSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getCrewSdkTranslator()
        );
    }

    public function testGetTaskSdkTranslator()
    {
        $translator = new MockBjSearchDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Translator\TaskSdkTranslator',
            $translator->getTaskSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getTaskSdkTranslator()
        );
    }

    public function testGetBjItemsDataSdkTranslator()
    {
        $translator = new MockBjSearchDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Translator\BjItemsDataSdkTranslator',
            $translator->getBjItemsDataSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getBjItemsDataSdkTranslator()
        );
    }

    public function testGetBjTemplateSdkTranslator()
    {
        $translator = new MockBjSearchDataSdkTranslator();

        $this->assertInstanceOf(
            'Base\Template\Translator\BjTemplateSdkTranslator',
            $translator->getBjTemplateSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getBjTemplateSdkTranslator()
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $bjSearchData = \Base\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);

        $crewSdk = new CrewSdk($bjSearchData->getCrew()->getId());
        $taskSdk = new TaskSdk($bjSearchData->getTask()->getId());
        $bjTemplateSdk = new BjTemplateSdk($bjSearchData->getTemplate()->getId());
        $bjItemsDataSdk = new BjItemsDataSdk($bjSearchData->getItemsData()->getId());

        $bjSearchDataSdk = new BjSearchDataSdk(
            $bjSearchData->getId(),
            $bjSearchData->getName(),
            $bjSearchData->getIdentify(),
            $bjSearchData->getInfoClassify(),
            $bjSearchData->getInfoCategory(),
            $crewSdk,
            $bjSearchData->getSubjectCategory(),
            $bjSearchData->getDimension(),
            $bjSearchData->getExpirationDate(),
            $bjSearchData->getHash(),
            $bjTemplateSdk,
            $bjItemsDataSdk,
            $taskSdk,
            $bjSearchData->getDescription(),
            $bjSearchData->getFrontEndProcessorStatus(),
            $bjSearchData->getStatus(),
            $bjSearchData->getStatusTime(),
            $bjSearchData->getCreateTime(),
            $bjSearchData->getUpdateTime()
        );

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->valueObjectToObject(Argument::exact($crewSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjSearchData->getCrew());

        $bjTemplateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $bjTemplateSdkTranslator->valueObjectToObject(Argument::exact($bjTemplateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjSearchData->getTemplate());

        $bjItemsDataSdkTranslator = $this->prophesize(BjItemsDataSdkTranslator::class);
        $bjItemsDataSdkTranslator->valueObjectToObject(Argument::exact($bjItemsDataSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjSearchData->getItemsData());

        $taskSdkTranslator = $this->prophesize(TaskSdkTranslator::class);
        $taskSdkTranslator->valueObjectToObject(Argument::exact($taskSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjSearchData->getTask());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getBjTemplateSdkTranslator')
            ->willReturn($bjTemplateSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getBjItemsDataSdkTranslator')
            ->willReturn($bjItemsDataSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTaskSdkTranslator')
            ->willReturn($taskSdkTranslator->reveal());

        $bjSearchDataObject = $this->translator->valueObjectToObject($bjSearchDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\BjSearchData', $bjSearchDataObject);
        $this->compareValueObjectAndObject($bjSearchDataSdk, $bjSearchDataObject);
    }

    public function testValueObjectToObjectFail()
    {
        $bjSearchDataSdk = array();

        $bjSearchData = $this->translator->valueObjectToObject($bjSearchDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\NullBjSearchData', $bjSearchData);
    }

    public function testObjectToValueObjectFail()
    {
        $bjSearchData = array();

        $bjSearchDataSdk = $this->translator->objectToValueObject($bjSearchData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\BjSearchData', $bjSearchDataSdk);
    }

    public function testObjectToValueObject()
    {
        $bjSearchData = \Base\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        $crewSdk = new CrewSdk($bjSearchData->getCrew()->getId());
        $bjTemplateSdk = new BjTemplateSdk($bjSearchData->getTemplate()->getId());
        $taskSdk = new TaskSdk($bjSearchData->getTask()->getId());
        $bjItemsDataSdk = new BjItemsDataSdk($bjSearchData->getItemsData()->getId());

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->objectToValueObject(Argument::exact($bjSearchData->getCrew()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crewSdk);

        $bjTemplateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $bjTemplateSdkTranslator->objectToValueObject(Argument::exact($bjSearchData->getTemplate()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjTemplateSdk);

        $taskSdkTranslator = $this->prophesize(TaskSdkTranslator::class);
        $taskSdkTranslator->objectToValueObject(Argument::exact($bjSearchData->getTask()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($taskSdk);

        $bjItemsDataSdkTranslator = $this->prophesize(BjItemsDataSdkTranslator::class);
        $bjItemsDataSdkTranslator->objectToValueObject(Argument::exact($bjSearchData->getItemsData()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjItemsDataSdk);
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getBjItemsDataSdkTranslator')
            ->willReturn($bjItemsDataSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getBjTemplateSdkTranslator')
            ->willReturn($bjTemplateSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTaskSdkTranslator')
            ->willReturn($taskSdkTranslator->reveal());

        $bjSearchDataSdk = $this->translator->objectToValueObject($bjSearchData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\BjSearchData', $bjSearchDataSdk);
        $this->compareValueObjectAndObject($bjSearchDataSdk, $bjSearchData);
    }
}
