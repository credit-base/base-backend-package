<?php
namespace Base\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Utils\TaskSdkUtils;

use BaseSdk\ResourceCatalogData\Model\Task as TaskSdk;

use BaseSdk\Crew\Model\Crew as CrewSdk;
use Base\Crew\Translator\CrewSdkTranslator;

class TaskSdkTranslatorTest extends TestCase
{
    use TaskSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(TaskSdkTranslator::class)
                    ->setMethods([
                        'getCrewSdkTranslator'
                    ])
                    ->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetCrewSdkTranslator()
    {
        $translator = new MockTaskSdkTranslator();
        $this->assertInstanceOf(
            'Base\Crew\Translator\CrewSdkTranslator',
            $translator->getCrewSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getCrewSdkTranslator()
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $task = \Base\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);

        $crewSdk = new CrewSdk($task->getCrew()->getId());

        $taskSdk = new TaskSdk(
            $task->getId(),
            $crewSdk,
            $task->getPid(),
            $task->getTotal(),
            $task->getSuccessNumber(),
            $task->getFailureNumber(),
            $task->getSourceCategory(),
            $task->getSourceTemplate(),
            $task->getTargetCategory(),
            $task->getTargetTemplate(),
            $task->getTargetRule(),
            $task->getScheduleTask(),
            $task->getErrorNumber(),
            $task->getFileName(),
            $task->getStatus(),
            $task->getStatusTime(),
            $task->getCreateTime(),
            $task->getUpdateTime()
        );

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->valueObjectToObject(Argument::exact($crewSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($task->getCrew());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());

        $taskObject = $this->translator->valueObjectToObject($taskSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\Task', $taskObject);
        $this->compareValueObjectAndObject($taskSdk, $taskObject);
    }

    public function testValueObjectToObjectFail()
    {
        $taskSdk = array();

        $task = $this->translator->valueObjectToObject($taskSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\NullTask', $task);
    }

    public function testObjectToValueObjectFail()
    {
        $task = array();

        $taskSdk = $this->translator->objectToValueObject($task);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\Task', $taskSdk);
    }

    public function testObjectToValueObject()
    {
        $task = \Base\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
        $crewSdk = new CrewSdk($task->getCrew()->getId());

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->objectToValueObject(Argument::exact($task->getCrew()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crewSdk);

        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());

        $taskSdk = $this->translator->objectToValueObject($task);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\Task', $taskSdk);
        $this->compareValueObjectAndObject($taskSdk, $task);
    }
}
