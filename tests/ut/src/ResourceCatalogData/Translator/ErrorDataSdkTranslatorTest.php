<?php
namespace Base\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Translator\ISdkTranslator;

use Base\ResourceCatalogData\Model\NullErrorData;
use Base\ResourceCatalogData\Utils\ErrorDataSdkUtils;

use BaseSdk\ResourceCatalogData\Model\Task as TaskSdk;
use BaseSdk\ResourceCatalogData\Model\ErrorItemsData as ErrorItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\ErrorData as ErrorDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullErrorData as NullErrorDataSdk;

use Base\Template\Model\Template;
use Base\Template\Translator\TranslatorFactory;
use Base\Template\Translator\BjTemplateSdkTranslator;
use BaseSdk\Template\Model\BjTemplate as BjTemplateSdk;

use BaseSdk\Crew\Model\Crew as CrewSdk;
use Base\Crew\Translator\CrewSdkTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ErrorDataSdkTranslatorTest extends TestCase
{
    use ErrorDataSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(ErrorDataSdkTranslator::class)
                    ->setMethods([
                        'getCrewSdkTranslator',
                        'getTaskSdkTranslator',
                        'getErrorItemsDataSdkTranslator',
                        'getTranslatorFactory',
                        'getTemplateSdkTranslator'
                    ])
                    ->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetCrewSdkTranslator()
    {
        $translator = new MockErrorDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\Crew\Translator\CrewSdkTranslator',
            $translator->getCrewSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getCrewSdkTranslator()
        );
    }

    public function testGetTaskSdkTranslator()
    {
        $translator = new MockErrorDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Translator\TaskSdkTranslator',
            $translator->getTaskSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getTaskSdkTranslator()
        );
    }

    public function testGetErrorItemsDataSdkTranslator()
    {
        $translator = new MockErrorDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Translator\ErrorItemsDataSdkTranslator',
            $translator->getErrorItemsDataSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getErrorItemsDataSdkTranslator()
        );
    }

    public function testGetTranslatorFactory()
    {
        $translator = new MockErrorDataSdkTranslator();

        $this->assertInstanceOf(
            'Base\Template\Translator\TranslatorFactory',
            $translator->getTranslatorFactory()
        );
    }

    public function testGetTemplateSdkTranslator()
    {
        $translator = $this->getMockBuilder(MockErrorDataSdkTranslator::class)
                                 ->setMethods(['getTranslatorFactory'])
                                 ->getMock();

        $sdkTranslator = new BjTemplateSdkTranslator();
        $category = Template::CATEGORY['BJ'];

        $translatorFactory = $this->prophesize(TranslatorFactory::class);
        $translatorFactory->getTranslator(Argument::exact($category))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sdkTranslator);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getTranslatorFactory')
            ->willReturn($translatorFactory->reveal());

        $result = $translator->getTemplateSdkTranslator($category);
        $this->assertInstanceof('Base\Common\Translator\ISdkTranslator', $result);
        $this->assertEquals($sdkTranslator, $result);
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $errorData = \Base\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);

        $crewSdk = new CrewSdk($errorData->getCrew()->getId());
        $taskSdk = new TaskSdk($errorData->getTask()->getId());
        $bjTemplateSdk = new BjTemplateSdk($errorData->getTemplate()->getId());
        $errorItemsDataSdk = new ErrorItemsDataSdk($errorData->getItemsData()->getId());

        $errorDataSdk = new ErrorDataSdk(
            $errorData->getId(),
            $errorData->getName(),
            $errorData->getIdentify(),
            $errorData->getInfoClassify(),
            $errorData->getInfoCategory(),
            $crewSdk,
            $errorData->getSubjectCategory(),
            $errorData->getDimension(),
            $errorData->getExpirationDate(),
            $errorData->getHash(),
            $bjTemplateSdk,
            $errorItemsDataSdk,
            $taskSdk,
            $errorData->getCategory(),
            $errorData->getErrorType(),
            $errorData->getErrorReason(),
            $errorData->getStatus(),
            $errorData->getStatusTime(),
            $errorData->getCreateTime(),
            $errorData->getUpdateTime()
        );

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->valueObjectToObject(Argument::exact($crewSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($errorData->getCrew());

        $bjTemplateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $bjTemplateSdkTranslator->valueObjectToObject(Argument::exact($bjTemplateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($errorData->getTemplate());

        $errorItemsDataSdkTranslator = $this->prophesize(ErrorItemsDataSdkTranslator::class);
        $errorItemsDataSdkTranslator->valueObjectToObject(Argument::exact($errorItemsDataSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($errorData->getItemsData());

        $taskSdkTranslator = $this->prophesize(TaskSdkTranslator::class);
        $taskSdkTranslator->valueObjectToObject(Argument::exact($taskSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($errorData->getTask());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTemplateSdkTranslator')
            ->willReturn($bjTemplateSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getErrorItemsDataSdkTranslator')
            ->willReturn($errorItemsDataSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTaskSdkTranslator')
            ->willReturn($taskSdkTranslator->reveal());

        $errorDataObject = $this->translator->valueObjectToObject($errorDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\ErrorData', $errorDataObject);
        $this->compareValueObjectAndObject($errorDataSdk, $errorDataObject);
    }

    public function testValueObjectToObjectFail()
    {
        $errorDataSdk = array();

        $errorData = $this->translator->valueObjectToObject($errorDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\NullErrorData', $errorData);
    }

    public function testObjectToValueObjectFail()
    {
        $errorData = array();

        $errorDataSdk = $this->translator->objectToValueObject($errorData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ErrorData', $errorDataSdk);
    }

    public function testObjectToValueObject()
    {
        $errorData = \Base\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);
        $crewSdk = new CrewSdk($errorData->getCrew()->getId());
        $bjTemplateSdk = new BjTemplateSdk($errorData->getTemplate()->getId());
        $taskSdk = new TaskSdk($errorData->getTask()->getId());
        $errorItemsDataSdk = new ErrorItemsDataSdk($errorData->getItemsData()->getId());

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->objectToValueObject(Argument::exact($errorData->getCrew()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crewSdk);

        $bjTemplateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $bjTemplateSdkTranslator->objectToValueObject(Argument::exact($errorData->getTemplate()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjTemplateSdk);

        $taskSdkTranslator = $this->prophesize(TaskSdkTranslator::class);
        $taskSdkTranslator->objectToValueObject(Argument::exact($errorData->getTask()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($taskSdk);

        $errorItemsDataSdkTranslator = $this->prophesize(ErrorItemsDataSdkTranslator::class);
        $errorItemsDataSdkTranslator->objectToValueObject(Argument::exact($errorData->getItemsData()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($errorItemsDataSdk);
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getErrorItemsDataSdkTranslator')
            ->willReturn($errorItemsDataSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTemplateSdkTranslator')
            ->willReturn($bjTemplateSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTaskSdkTranslator')
            ->willReturn($taskSdkTranslator->reveal());

        $errorDataSdk = $this->translator->objectToValueObject($errorData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\ErrorData', $errorDataSdk);
        $this->compareValueObjectAndObject($errorDataSdk, $errorData);
    }
}
