<?php
namespace Base\ResourceCatalogData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Translator\ISdkTranslator;

use Base\ResourceCatalogData\Model\NullWbjSearchData;
use Base\ResourceCatalogData\Utils\WbjSearchDataSdkUtils;

use BaseSdk\ResourceCatalogData\Model\Task as TaskSdk;
use BaseSdk\ResourceCatalogData\Model\WbjItemsData as WbjItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\WbjSearchData as WbjSearchDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullWbjSearchData as NullWbjSearchDataSdk;

use Base\Template\Translator\WbjTemplateSdkTranslator;
use BaseSdk\Template\Model\WbjTemplate as WbjTemplateSdk;

use BaseSdk\Crew\Model\Crew as CrewSdk;
use Base\Crew\Translator\CrewSdkTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjSearchDataSdkTranslatorTest extends TestCase
{
    use WbjSearchDataSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(WbjSearchDataSdkTranslator::class)
                    ->setMethods([
                        'getCrewSdkTranslator',
                        'getTaskSdkTranslator',
                        'getWbjItemsDataSdkTranslator',
                        'getWbjTemplateSdkTranslator',
                    ])
                    ->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetCrewSdkTranslator()
    {
        $translator = new MockWbjSearchDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\Crew\Translator\CrewSdkTranslator',
            $translator->getCrewSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getCrewSdkTranslator()
        );
    }

    public function testGetTaskSdkTranslator()
    {
        $translator = new MockWbjSearchDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Translator\TaskSdkTranslator',
            $translator->getTaskSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getTaskSdkTranslator()
        );
    }

    public function testGetWbjItemsDataSdkTranslator()
    {
        $translator = new MockWbjSearchDataSdkTranslator();
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Translator\WbjItemsDataSdkTranslator',
            $translator->getWbjItemsDataSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getWbjItemsDataSdkTranslator()
        );
    }

    public function testGetWbjTemplateSdkTranslator()
    {
        $translator = new MockWbjSearchDataSdkTranslator();

        $this->assertInstanceOf(
            'Base\Template\Translator\WbjTemplateSdkTranslator',
            $translator->getWbjTemplateSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getWbjTemplateSdkTranslator()
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $wbjSearchData = \Base\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);

        $crewSdk = new CrewSdk($wbjSearchData->getCrew()->getId());
        $taskSdk = new TaskSdk($wbjSearchData->getTask()->getId());
        $wbjTemplateSdk = new WbjTemplateSdk($wbjSearchData->getTemplate()->getId());
        $wbjItemsDataSdk = new WbjItemsDataSdk($wbjSearchData->getItemsData()->getId());

        $wbjSearchDataSdk = new WbjSearchDataSdk(
            $wbjSearchData->getId(),
            $wbjSearchData->getName(),
            $wbjSearchData->getIdentify(),
            $wbjSearchData->getInfoClassify(),
            $wbjSearchData->getInfoCategory(),
            $crewSdk,
            $wbjSearchData->getSubjectCategory(),
            $wbjSearchData->getDimension(),
            $wbjSearchData->getExpirationDate(),
            $wbjSearchData->getHash(),
            $wbjTemplateSdk,
            $wbjItemsDataSdk,
            $taskSdk,
            $wbjSearchData->getStatus(),
            $wbjSearchData->getStatusTime(),
            $wbjSearchData->getCreateTime(),
            $wbjSearchData->getUpdateTime()
        );

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->valueObjectToObject(Argument::exact($crewSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjSearchData->getCrew());

        $wbjTemplateSdkTranslator = $this->prophesize(WbjTemplateSdkTranslator::class);
        $wbjTemplateSdkTranslator->valueObjectToObject(Argument::exact($wbjTemplateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjSearchData->getTemplate());

        $wbjItemsDataSdkTranslator = $this->prophesize(WbjItemsDataSdkTranslator::class);
        $wbjItemsDataSdkTranslator->valueObjectToObject(Argument::exact($wbjItemsDataSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjSearchData->getItemsData());

        $taskSdkTranslator = $this->prophesize(TaskSdkTranslator::class);
        $taskSdkTranslator->valueObjectToObject(Argument::exact($taskSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjSearchData->getTask());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getWbjTemplateSdkTranslator')
            ->willReturn($wbjTemplateSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getWbjItemsDataSdkTranslator')
            ->willReturn($wbjItemsDataSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTaskSdkTranslator')
            ->willReturn($taskSdkTranslator->reveal());

        $wbjSearchDataObject = $this->translator->valueObjectToObject($wbjSearchDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\WbjSearchData', $wbjSearchDataObject);
        $this->compareValueObjectAndObject($wbjSearchDataSdk, $wbjSearchDataObject);
    }

    public function testValueObjectToObjectFail()
    {
        $wbjSearchDataSdk = array();

        $wbjSearchData = $this->translator->valueObjectToObject($wbjSearchDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\NullWbjSearchData', $wbjSearchData);
    }

    public function testObjectToValueObjectFail()
    {
        $wbjSearchData = array();

        $wbjSearchDataSdk = $this->translator->objectToValueObject($wbjSearchData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\WbjSearchData', $wbjSearchDataSdk);
    }

    public function testObjectToValueObject()
    {
        $wbjSearchData = \Base\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);
        $crewSdk = new CrewSdk($wbjSearchData->getCrew()->getId());
        $wbjTemplateSdk = new WbjTemplateSdk($wbjSearchData->getTemplate()->getId());
        $taskSdk = new TaskSdk($wbjSearchData->getTask()->getId());
        $wbjItemsDataSdk = new WbjItemsDataSdk($wbjSearchData->getItemsData()->getId());

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->objectToValueObject(Argument::exact($wbjSearchData->getCrew()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crewSdk);

        $wbjTemplateSdkTranslator = $this->prophesize(WbjTemplateSdkTranslator::class);
        $wbjTemplateSdkTranslator->objectToValueObject(Argument::exact($wbjSearchData->getTemplate()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjTemplateSdk);

        $taskSdkTranslator = $this->prophesize(TaskSdkTranslator::class);
        $taskSdkTranslator->objectToValueObject(Argument::exact($wbjSearchData->getTask()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($taskSdk);

        $wbjItemsDataSdkTranslator = $this->prophesize(WbjItemsDataSdkTranslator::class);
        $wbjItemsDataSdkTranslator->objectToValueObject(Argument::exact($wbjSearchData->getItemsData()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjItemsDataSdk);
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getWbjItemsDataSdkTranslator')
            ->willReturn($wbjItemsDataSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getWbjTemplateSdkTranslator')
            ->willReturn($wbjTemplateSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getTaskSdkTranslator')
            ->willReturn($taskSdkTranslator->reveal());

        $wbjSearchDataSdk = $this->translator->objectToValueObject($wbjSearchData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\WbjSearchData', $wbjSearchDataSdk);
        $this->compareValueObjectAndObject($wbjSearchDataSdk, $wbjSearchData);
    }
}
