<?php
namespace Base\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\NullBjItemsData;
use Base\ResourceCatalogData\Utils\BjItemsDataSdkUtils;

use BaseSdk\ResourceCatalogData\Model\BjItemsData as BjItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullBjItemsData as NullBjItemsDataSdk;

class BjItemsDataSdkTranslatorTest extends TestCase
{
    use BjItemsDataSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BjItemsDataSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $bjItemsData = \Base\ResourceCatalogData\Utils\BjItemsDataMockFactory::generateBjItemsData(1);

        $bjItemsDataSdk = new BjItemsDataSdk(
            $bjItemsData->getId(),
            $bjItemsData->getData(),
            $bjItemsData->getStatus(),
            $bjItemsData->getStatusTime(),
            $bjItemsData->getCreateTime(),
            $bjItemsData->getUpdateTime()
        );

        $bjItemsDataObject = $this->translator->valueObjectToObject($bjItemsDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\BjItemsData', $bjItemsDataObject);
        $this->compareValueObjectAndObject($bjItemsDataSdk, $bjItemsDataObject);
    }

    public function testValueObjectToObjectFail()
    {
        $bjItemsDataSdk = NullBjItemsDataSdk::getInstance();

        $bjItemsData = $this->translator->valueObjectToObject($bjItemsDataSdk);
        $this->assertInstanceof('Base\ResourceCatalogData\Model\NullBjItemsData', $bjItemsData);
    }

    public function testObjectToValueObjectFail()
    {
        $bjItemsData = array();

        $bjItemsDataSdk = $this->translator->objectToValueObject($bjItemsData);
        $this->assertInstanceof('BaseSdk\ResourceCatalogData\Model\BjItemsData', $bjItemsDataSdk);
    }
}
