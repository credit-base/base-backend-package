<?php
namespace Base\ResourceCatalogData\Adapter\WbjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Common\Model\IObject;

use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\Translator\WbjSearchDataSdkTranslator;

use BaseSdk\ResourceCatalogData\Model\WbjSearchData as WbjSearchDataSdk;
use BaseSdk\ResourceCatalogData\Repository\WbjSearchDataRestfulRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WbjSearchDataSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockWbjSearchDataSdkAdapter::class)
                           ->setMethods([
                               'fetchCrew',
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIWbjSearchDataAdapter()
    {
        $adapter = new WbjSearchDataSdkAdapter();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Repository\WbjSearchDataRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\NullWbjSearchData',
            $this->adapter->getNullObject()
        );
    }

    /**
     * 测试是否初始化 CrewRepository
     */
    public function testGetCrewRepository()
    {
        $adapter = new MockWbjSearchDataSdkAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }
    
    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockWbjSearchDataSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedWbjSearchData = new WbjSearchData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedWbjSearchData);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedWbjSearchData);

        //验证
        $wbjSearchData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedWbjSearchData, $wbjSearchData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $wbjSearchDataOne = new WbjSearchData(1);
        $wbjSearchDataTwo = new WbjSearchData(2);

        $ids = [1, 2];

        $expectedWbjSearchDataList = [];
        $expectedWbjSearchDataList[$wbjSearchDataOne->getId()] = $wbjSearchDataOne;
        $expectedWbjSearchDataList[$wbjSearchDataTwo->getId()] = $wbjSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedWbjSearchDataList);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedWbjSearchDataList);

        //验证
        $wbjSearchDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedWbjSearchDataList, $wbjSearchDataList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $wbjSearchDataOne = new WbjSearchData(1);
        $wbjSearchDataTwo = new WbjSearchData(2);

        $expectedWbjSearchDataList = [];
        $expectedWbjSearchDataList[$wbjSearchDataOne->getId()] = $wbjSearchDataOne;
        $expectedWbjSearchDataList[$wbjSearchDataTwo->getId()] = $wbjSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedWbjSearchDataList, 2]);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedWbjSearchDataList);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedWbjSearchDataList, 2], $result);
    }

    private function initDisable(bool $result)
    {
        $this->trait = $this->getMockBuilder(MockWbjSearchDataSdkAdapter::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $wbjSearchData = new WbjSearchData(1);
        $wbjSearchDataSdk = new WbjSearchDataSdk(1);

        $translator = $this->prophesize(WbjSearchDataSdkTranslator::class);
        $translator->objectToValueObject($wbjSearchData)->shouldBeCalledTimes(1)->willReturn($wbjSearchDataSdk);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(WbjSearchDataRestfulRepository::class);
        $repository->disable($wbjSearchDataSdk)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return $wbjSearchData;
    }

    public function testDisable()
    {
        $wbjSearchData = $this->initDisable(true);

        $result = $this->trait->disable($wbjSearchData);
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $wbjSearchData = $this->initDisable(false);

        $result = $this->trait->disable($wbjSearchData);
        $this->assertFalse($result);
    }

    //fetchCrew
    public function testFetchCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockWbjSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByObject'
                                ]
                           )
                           ->getMock();
        
        $wbjSearchData = new WbjSearchData();

        $adapter->expects($this->exactly(1))->method('fetchCrewByObject')->with($wbjSearchData);

        $adapter->fetchCrew($wbjSearchData);
    }

    public function testFetchCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockWbjSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByList'
                                ]
                           )
                           ->getMock();
        
        $wbjSearchDataOne = new WbjSearchData(1);
        $wbjSearchDataTwo = new WbjSearchData(2);
        
        $wbjSearchDataList[$wbjSearchDataOne->getId()] = $wbjSearchDataOne;
        $wbjSearchDataList[$wbjSearchDataTwo->getId()] = $wbjSearchDataTwo;

        $adapter->expects($this->exactly(1))->method('fetchCrewByList')->with($wbjSearchDataList);

        $adapter->fetchCrew($wbjSearchDataList);
    }

    //fetchCrewByObject
    public function testFetchCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockWbjSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $crewId = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($crewId);
        $wbjSearchData = new WbjSearchData();
        $wbjSearchData->setCrew($crew);
        $wbjSearchData->setSourceUnit($crew->getUserGroup());
        
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))->shouldBeCalledTimes(1)->willReturn($crew);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByObject($wbjSearchData);
    }

    //fetchCrewByList
    public function testFetchCrewByList()
    {
        $adapter = $this->getMockBuilder(MockWbjSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $wbjSearchDataOne = \Base\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);
        $crewOne = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $wbjSearchDataOne->setCrew($crewOne);
        $wbjSearchDataOne->setSourceUnit($crewOne->getUserGroup());

        $wbjSearchDataTwo = \Base\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(2);
        $crewTwo = \Base\Crew\Utils\MockFactory::generateCrew(2);
        $wbjSearchDataTwo->setCrew($crewTwo);
        $wbjSearchDataTwo->setSourceUnit($crewTwo->getUserGroup());
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $wbjSearchDataList[$wbjSearchDataOne->getId()] = $wbjSearchDataOne;
        $wbjSearchDataList[$wbjSearchDataTwo->getId()] = $wbjSearchDataTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))->shouldBeCalledTimes(1)->willReturn($crewList);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByList($wbjSearchDataList);
    }
}
