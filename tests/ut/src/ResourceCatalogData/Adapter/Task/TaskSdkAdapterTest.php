<?php
namespace Base\ResourceCatalogData\Adapter\Task;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\Task;

class TaskSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockTaskSdkAdapter::class)
                           ->setMethods([
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsITaskAdapter()
    {
        $adapter = new TaskSdkAdapter();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\Task\ITaskAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Repository\TaskRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\NullTask',
            $this->adapter->getNullObject()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockTaskSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedTask = new Task();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedTask);

        //验证
        $task = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedTask, $task);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $taskOne = new Task(1);
        $taskTwo = new Task(2);

        $ids = [1, 2];

        $expectedTaskList = [];
        $expectedTaskList[$taskOne->getId()] = $taskOne;
        $expectedTaskList[$taskTwo->getId()] = $taskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedTaskList);

        //验证
        $taskList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedTaskList, $taskList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $taskOne = new Task(1);
        $taskTwo = new Task(2);

        $expectedTaskList = [];
        $expectedTaskList[$taskOne->getId()] = $taskOne;
        $expectedTaskList[$taskTwo->getId()] = $taskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedTaskList, 2]);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedTaskList, 2], $result);
    }
}
