<?php
namespace Base\ResourceCatalogData\Adapter\Task;

use PHPUnit\Framework\TestCase;

class TaskMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new TaskMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\Task',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\ResourceCatalogData\Model\Task',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\ResourceCatalogData\Model\Task',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
