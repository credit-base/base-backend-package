<?php
namespace Base\ResourceCatalogData\Adapter\BjSearchData;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\BjSearchData;

class BjSearchDataMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new BjSearchDataMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testConfirm()
    {
        $this->assertTrue($this->adapter->confirm(new BjSearchData()));
    }

    public function testDeletes()
    {
        $this->assertTrue($this->adapter->deletes(new BjSearchData()));
    }

    public function testDisable()
    {
        $this->assertTrue($this->adapter->disable(new BjSearchData()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\BjSearchData',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\ResourceCatalogData\Model\BjSearchData',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\ResourceCatalogData\Model\BjSearchData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
