<?php
namespace Base\ResourceCatalogData\Adapter\BjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Common\Model\IObject;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Translator\BjSearchDataSdkTranslator;

use BaseSdk\ResourceCatalogData\Model\BjSearchData as BjSearchDataSdk;
use BaseSdk\ResourceCatalogData\Repository\BjSearchDataRestfulRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class BjSearchDataSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockBjSearchDataSdkAdapter::class)
                           ->setMethods([
                               'fetchCrew',
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIBjSearchDataAdapter()
    {
        $adapter = new BjSearchDataSdkAdapter();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Repository\BjSearchDataRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\NullBjSearchData',
            $this->adapter->getNullObject()
        );
    }

    /**
     * 测试是否初始化 CrewRepository
     */
    public function testGetCrewRepository()
    {
        $adapter = new MockBjSearchDataSdkAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }
    
    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockBjSearchDataSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedBjSearchData = new BjSearchData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedBjSearchData);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedBjSearchData);

        //验证
        $bjSearchData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedBjSearchData, $bjSearchData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $bjSearchDataOne = new BjSearchData(1);
        $bjSearchDataTwo = new BjSearchData(2);

        $ids = [1, 2];

        $expectedBjSearchDataList = [];
        $expectedBjSearchDataList[$bjSearchDataOne->getId()] = $bjSearchDataOne;
        $expectedBjSearchDataList[$bjSearchDataTwo->getId()] = $bjSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedBjSearchDataList);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedBjSearchDataList);

        //验证
        $bjSearchDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedBjSearchDataList, $bjSearchDataList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $bjSearchDataOne = new BjSearchData(1);
        $bjSearchDataTwo = new BjSearchData(2);

        $expectedBjSearchDataList = [];
        $expectedBjSearchDataList[$bjSearchDataOne->getId()] = $bjSearchDataOne;
        $expectedBjSearchDataList[$bjSearchDataTwo->getId()] = $bjSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedBjSearchDataList, 2]);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedBjSearchDataList);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedBjSearchDataList, 2], $result);
    }

    private function initBjSearchData(bool $result, string $method)
    {
        $this->trait = $this->getMockBuilder(MockBjSearchDataSdkAdapter::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $bjSearchData = new BjSearchData(1);
        $bjSearchDataSdk = new BjSearchDataSdk(1);

        $translator = $this->prophesize(BjSearchDataSdkTranslator::class);
        $translator->objectToValueObject($bjSearchData)->shouldBeCalledTimes(1)->willReturn($bjSearchDataSdk);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(BjSearchDataRestfulRepository::class);
        $repository->$method($bjSearchDataSdk)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return $bjSearchData;
    }

    public function testConfirm()
    {
        $bjSearchData = $this->initBjSearchData(true, 'confirm');

        $result = $this->trait->confirm($bjSearchData);
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $bjSearchData = $this->initBjSearchData(false, 'confirm');

        $result = $this->trait->confirm($bjSearchData);
        $this->assertFalse($result);
    }

    public function testDeletes()
    {
        $bjSearchData = $this->initBjSearchData(true, 'deletes');

        $result = $this->trait->deletes($bjSearchData);
        $this->assertTrue($result);
    }

    public function testDeletesFail()
    {
        $bjSearchData = $this->initBjSearchData(false, 'deletes');

        $result = $this->trait->deletes($bjSearchData);
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $bjSearchData = $this->initBjSearchData(true, 'disable');

        $result = $this->trait->disable($bjSearchData);
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $bjSearchData = $this->initBjSearchData(false, 'disable');

        $result = $this->trait->disable($bjSearchData);
        $this->assertFalse($result);
    }

    //fetchCrew
    public function testFetchCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockBjSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByObject'
                                ]
                           )
                           ->getMock();
        
        $bjSearchData = new BjSearchData();

        $adapter->expects($this->exactly(1))->method('fetchCrewByObject')->with($bjSearchData);

        $adapter->fetchCrew($bjSearchData);
    }

    public function testFetchCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockBjSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByList'
                                ]
                           )
                           ->getMock();
        
        $bjSearchDataOne = new BjSearchData(1);
        $bjSearchDataTwo = new BjSearchData(2);
        
        $bjSearchDataList[$bjSearchDataOne->getId()] = $bjSearchDataOne;
        $bjSearchDataList[$bjSearchDataTwo->getId()] = $bjSearchDataTwo;

        $adapter->expects($this->exactly(1))->method('fetchCrewByList')->with($bjSearchDataList);

        $adapter->fetchCrew($bjSearchDataList);
    }

    //fetchCrewByObject
    public function testFetchCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockBjSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $crewId = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($crewId);
        $bjSearchData = new BjSearchData();
        $bjSearchData->setCrew($crew);
        $bjSearchData->setSourceUnit($crew->getUserGroup());
        
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))->shouldBeCalledTimes(1)->willReturn($crew);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByObject($bjSearchData);
    }

    //fetchCrewByList
    public function testFetchCrewByList()
    {
        $adapter = $this->getMockBuilder(MockBjSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $bjSearchDataOne = \Base\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        $crewOne = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $bjSearchDataOne->setCrew($crewOne);
        $bjSearchDataOne->setSourceUnit($crewOne->getUserGroup());

        $bjSearchDataTwo = \Base\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(2);
        $crewTwo = \Base\Crew\Utils\MockFactory::generateCrew(2);
        $bjSearchDataTwo->setCrew($crewTwo);
        $bjSearchDataTwo->setSourceUnit($crewTwo->getUserGroup());
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $bjSearchDataList[$bjSearchDataOne->getId()] = $bjSearchDataOne;
        $bjSearchDataList[$bjSearchDataTwo->getId()] = $bjSearchDataTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))->shouldBeCalledTimes(1)->willReturn($crewList);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByList($bjSearchDataList);
    }
}
