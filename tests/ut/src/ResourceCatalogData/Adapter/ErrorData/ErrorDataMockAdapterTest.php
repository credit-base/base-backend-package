<?php
namespace Base\ResourceCatalogData\Adapter\ErrorData;

use PHPUnit\Framework\TestCase;

class ErrorDataMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new ErrorDataMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\ErrorData',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\ResourceCatalogData\Model\ErrorData',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\ResourceCatalogData\Model\ErrorData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
