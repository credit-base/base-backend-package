<?php
namespace Base\ResourceCatalogData\Adapter\ErrorData;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\ErrorData;

class ErrorDataSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockErrorDataSdkAdapter::class)
                           ->setMethods([
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIErrorDataAdapter()
    {
        $adapter = new ErrorDataSdkAdapter();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Repository\ErrorDataRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\NullErrorData',
            $this->adapter->getNullObject()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockErrorDataSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedErrorData = new ErrorData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedErrorData);

        //验证
        $errorData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedErrorData, $errorData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $errorDataOne = new ErrorData(1);
        $errorDataTwo = new ErrorData(2);

        $ids = [1, 2];

        $expectedErrorDataList = [];
        $expectedErrorDataList[$errorDataOne->getId()] = $errorDataOne;
        $expectedErrorDataList[$errorDataTwo->getId()] = $errorDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedErrorDataList);

        //验证
        $errorDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedErrorDataList, $errorDataList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $errorDataOne = new ErrorData(1);
        $errorDataTwo = new ErrorData(2);

        $expectedErrorDataList = [];
        $expectedErrorDataList[$errorDataOne->getId()] = $errorDataOne;
        $expectedErrorDataList[$errorDataTwo->getId()] = $errorDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedErrorDataList, 2]);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedErrorDataList, 2], $result);
    }
}
