<?php
namespace Base\ResourceCatalogData\Adapter\GbSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Common\Model\IObject;

use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Translator\GbSearchDataSdkTranslator;

use BaseSdk\ResourceCatalogData\Model\GbSearchData as GbSearchDataSdk;
use BaseSdk\ResourceCatalogData\Repository\GbSearchDataRestfulRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class GbSearchDataSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockGbSearchDataSdkAdapter::class)
                           ->setMethods([
                               'fetchCrew',
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIGbSearchDataAdapter()
    {
        $adapter = new GbSearchDataSdkAdapter();

        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\ResourceCatalogData\Repository\GbSearchDataRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\NullGbSearchData',
            $this->adapter->getNullObject()
        );
    }

    /**
     * 测试是否初始化 CrewRepository
     */
    public function testGetCrewRepository()
    {
        $adapter = new MockGbSearchDataSdkAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }
    
    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockGbSearchDataSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedGbSearchData = new GbSearchData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedGbSearchData);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedGbSearchData);

        //验证
        $gbSearchData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedGbSearchData, $gbSearchData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $gbSearchDataOne = new GbSearchData(1);
        $gbSearchDataTwo = new GbSearchData(2);

        $ids = [1, 2];

        $expectedGbSearchDataList = [];
        $expectedGbSearchDataList[$gbSearchDataOne->getId()] = $gbSearchDataOne;
        $expectedGbSearchDataList[$gbSearchDataTwo->getId()] = $gbSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedGbSearchDataList);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedGbSearchDataList);

        //验证
        $gbSearchDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedGbSearchDataList, $gbSearchDataList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $gbSearchDataOne = new GbSearchData(1);
        $gbSearchDataTwo = new GbSearchData(2);

        $expectedGbSearchDataList = [];
        $expectedGbSearchDataList[$gbSearchDataOne->getId()] = $gbSearchDataOne;
        $expectedGbSearchDataList[$gbSearchDataTwo->getId()] = $gbSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedGbSearchDataList, 2]);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedGbSearchDataList);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedGbSearchDataList, 2], $result);
    }

    private function initGbSearchData(bool $result, string $method)
    {
        $this->trait = $this->getMockBuilder(MockGbSearchDataSdkAdapter::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $gbSearchData = new GbSearchData(1);
        $gbSearchDataSdk = new GbSearchDataSdk(1);

        $translator = $this->prophesize(GbSearchDataSdkTranslator::class);
        $translator->objectToValueObject($gbSearchData)->shouldBeCalledTimes(1)->willReturn($gbSearchDataSdk);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(GbSearchDataRestfulRepository::class);
        $repository->$method($gbSearchDataSdk)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return $gbSearchData;
    }

    public function testConfirm()
    {
        $gbSearchData = $this->initGbSearchData(true, 'confirm');

        $result = $this->trait->confirm($gbSearchData);
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $gbSearchData = $this->initGbSearchData(false, 'confirm');

        $result = $this->trait->confirm($gbSearchData);
        $this->assertFalse($result);
    }

    public function testDeletes()
    {
        $gbSearchData = $this->initGbSearchData(true, 'deletes');

        $result = $this->trait->deletes($gbSearchData);
        $this->assertTrue($result);
    }

    public function testDeletesFail()
    {
        $gbSearchData = $this->initGbSearchData(false, 'deletes');

        $result = $this->trait->deletes($gbSearchData);
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $gbSearchData = $this->initGbSearchData(true, 'disable');

        $result = $this->trait->disable($gbSearchData);
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $gbSearchData = $this->initGbSearchData(false, 'disable');

        $result = $this->trait->disable($gbSearchData);
        $this->assertFalse($result);
    }

    //fetchCrew
    public function testFetchCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockGbSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByObject'
                                ]
                           )
                           ->getMock();
        
        $gbSearchData = new GbSearchData();

        $adapter->expects($this->exactly(1))->method('fetchCrewByObject')->with($gbSearchData);

        $adapter->fetchCrew($gbSearchData);
    }

    public function testFetchCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockGbSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByList'
                                ]
                           )
                           ->getMock();
        
        $gbSearchDataOne = new GbSearchData(1);
        $gbSearchDataTwo = new GbSearchData(2);
        
        $gbSearchDataList[$gbSearchDataOne->getId()] = $gbSearchDataOne;
        $gbSearchDataList[$gbSearchDataTwo->getId()] = $gbSearchDataTwo;

        $adapter->expects($this->exactly(1))->method('fetchCrewByList')->with($gbSearchDataList);

        $adapter->fetchCrew($gbSearchDataList);
    }

    //fetchCrewByObject
    public function testFetchCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockGbSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $crewId = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($crewId);
        $gbSearchData = new GbSearchData();
        $gbSearchData->setCrew($crew);
        $gbSearchData->setSourceUnit($crew->getUserGroup());
        
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))->shouldBeCalledTimes(1)->willReturn($crew);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByObject($gbSearchData);
    }

    //fetchCrewByList
    public function testFetchCrewByList()
    {
        $adapter = $this->getMockBuilder(MockGbSearchDataSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $gbSearchDataOne = \Base\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData(1);
        $crewOne = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $gbSearchDataOne->setCrew($crewOne);
        $gbSearchDataOne->setSourceUnit($crewOne->getUserGroup());

        $gbSearchDataTwo = \Base\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData(2);
        $crewTwo = \Base\Crew\Utils\MockFactory::generateCrew(2);
        $gbSearchDataTwo->setCrew($crewTwo);
        $gbSearchDataTwo->setSourceUnit($crewTwo->getUserGroup());
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $gbSearchDataList[$gbSearchDataOne->getId()] = $gbSearchDataOne;
        $gbSearchDataList[$gbSearchDataTwo->getId()] = $gbSearchDataTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))->shouldBeCalledTimes(1)->willReturn($crewList);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByList($gbSearchDataList);
    }
}
