<?php
namespace Base\ResourceCatalogData\Adapter\GbSearchData;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\GbSearchData;

class GbSearchDataMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new GbSearchDataMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testConfirm()
    {
        $this->assertTrue($this->adapter->confirm(new GbSearchData()));
    }

    public function testDeletes()
    {
        $this->assertTrue($this->adapter->deletes(new GbSearchData()));
    }

    public function testDisable()
    {
        $this->assertTrue($this->adapter->disable(new GbSearchData()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Model\GbSearchData',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\ResourceCatalogData\Model\GbSearchData',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\ResourceCatalogData\Model\GbSearchData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
