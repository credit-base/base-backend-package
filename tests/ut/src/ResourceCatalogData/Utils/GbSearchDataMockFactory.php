<?php
namespace Base\ResourceCatalogData\Utils;

use Base\ResourceCatalogData\Model\GbSearchData;

class GbSearchDataMockFactory
{
    use SearchDataMockFactoryTrait;
    
    public static function generateGbSearchData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : GbSearchData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $gbSearchData = new GbSearchData($id);
        $gbSearchData->setId($id);

        self::generateInfoClassify($gbSearchData, $faker, $value);
        self::generateInfoCategory($gbSearchData, $faker, $value);
        self::generateCrew($gbSearchData, $faker, $value);
        self::generateSourceUnit($gbSearchData, $faker, $value);
        self::generateSubjectCategory($gbSearchData, $faker, $value);
        self::generateDimension($gbSearchData, $faker, $value);
        self::generateExpirationDate($gbSearchData, $faker, $value);
        self::generateStatus($gbSearchData, $faker, $value);
        self::generateTemplate($gbSearchData, $faker, $value);
        self::generateItemsData($gbSearchData, $faker, $value);
        self::generateTask($gbSearchData, $faker, $value);
        self::generateFrontEndProcessorStatus($gbSearchData, $faker, $value);
        self::generateDescription($gbSearchData, $faker, $value);
        
        $gbSearchData->setCreateTime($faker->unixTime());
        $gbSearchData->setUpdateTime($faker->unixTime());
        $gbSearchData->setStatusTime($faker->unixTime());

        return $gbSearchData;
    }

    private static function generateTemplate($gbSearchData, $faker, $value) : void
    {
        $template = isset($value['template']) ?
        $value['template'] :
        \Base\Template\Utils\GbTemplateMockFactory::generateGbTemplate($faker->randomDigit());
        
        $gbSearchData->setTemplate($template);
    }

    private static function generateItemsData($gbSearchData, $faker, $value) : void
    {
        $itemsData = isset($value['itemsData']) ?
        $value['itemsData'] : GbItemsDataMockFactory::generateGbItemsData($faker->randomDigit());
        
        $gbSearchData->setItemsData($itemsData);
    }
    protected static function generateStatus($gbSearchData, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                GbSearchData::STATUS
            );
        
        $gbSearchData->setStatus($status);
    }

    protected static function generateFrontEndProcessorStatus($gbSearchData, $faker, $value) : void
    {
        $frontEndProcessorStatus = isset($value['frontEndProcessorStatus']) ?
            $value['frontEndProcessorStatus'] :
            $faker->randomElement(
                GbSearchData::FRONT_END_PROCESSOR_STATUS
            );
        
        $gbSearchData->setFrontEndProcessorStatus($frontEndProcessorStatus);
    }

    protected static function generateDescription($gbSearchData, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->word();
        
        $gbSearchData->setDescription($description);
    }
}
