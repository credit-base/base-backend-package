<?php
namespace Base\ResourceCatalogData\Utils;

trait WbjItemsDataSdkUtils
{
    private function compareValueObjectAndObject(
        $wbjItemsDataSdk,
        $wbjItemsData
    ) {
        $this->assertEquals($wbjItemsDataSdk->getId(), $wbjItemsData->getId());
        $this->assertEquals($wbjItemsDataSdk->getData(), $wbjItemsData->getData());
        $this->assertEquals($wbjItemsDataSdk->getStatus(), $wbjItemsData->getStatus());
        $this->assertEquals($wbjItemsDataSdk->getCreateTime(), $wbjItemsData->getCreateTime());
        $this->assertEquals($wbjItemsDataSdk->getStatusTime(), $wbjItemsData->getStatusTime());
        $this->assertEquals($wbjItemsDataSdk->getUpdateTime(), $wbjItemsData->getUpdateTime());
    }
}
