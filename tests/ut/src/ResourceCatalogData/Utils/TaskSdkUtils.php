<?php
namespace Base\ResourceCatalogData\Utils;

trait TaskSdkUtils
{
    private function compareValueObjectAndObject(
        $taskSdk,
        $task
    ) {
        $this->assertEquals($taskSdk->getId(), $task->getId());
        $this->assertEquals($taskSdk->getCrew()->getId(), $task->getCrew()->getId());
        $this->assertEquals($taskSdk->getPid(), $task->getPid());
        $this->assertEquals($taskSdk->getTotal(), $task->getTotal());
        $this->assertEquals($taskSdk->getSuccessNumber(), $task->getSuccessNumber());
        $this->assertEquals($taskSdk->getFailureNumber(), $task->getFailureNumber());
        $this->assertEquals($taskSdk->getSourceCategory(), $task->getSourceCategory());
        $this->assertEquals($taskSdk->getSourceTemplate(), $task->getSourceTemplate());
        $this->assertEquals($taskSdk->getTargetCategory(), $task->getTargetCategory());
        $this->assertEquals($taskSdk->getTargetTemplate(), $task->getTargetTemplate());
        $this->assertEquals($taskSdk->getTargetRule(), $task->getTargetRule());
        $this->assertEquals($taskSdk->getScheduleTask(), $task->getScheduleTask());
        $this->assertEquals($taskSdk->getErrorNumber(), $task->getErrorNumber());
        $this->assertEquals($taskSdk->getFileName(), $task->getFileName());
        $this->assertEquals($taskSdk->getStatus(), $task->getStatus());
        $this->assertEquals($taskSdk->getCreateTime(), $task->getCreateTime());
        $this->assertEquals($taskSdk->getUpdateTime(), $task->getUpdateTime());
        $this->assertEquals($taskSdk->getStatusTime(), $task->getStatusTime());
    }
}
