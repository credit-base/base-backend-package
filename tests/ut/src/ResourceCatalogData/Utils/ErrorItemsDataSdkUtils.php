<?php
namespace Base\ResourceCatalogData\Utils;

trait ErrorItemsDataSdkUtils
{
    private function compareValueObjectAndObject(
        $errorItemsDataSdk,
        $errorItemsData
    ) {
        $this->assertEquals($errorItemsDataSdk->getId(), $errorItemsData->getId());
        $this->assertEquals($errorItemsDataSdk->getData(), $errorItemsData->getData());
        $this->assertEquals($errorItemsDataSdk->getStatus(), $errorItemsData->getStatus());
        $this->assertEquals($errorItemsDataSdk->getCreateTime(), $errorItemsData->getCreateTime());
        $this->assertEquals($errorItemsDataSdk->getStatusTime(), $errorItemsData->getStatusTime());
        $this->assertEquals($errorItemsDataSdk->getUpdateTime(), $errorItemsData->getUpdateTime());
    }
}
