<?php
namespace Base\ResourceCatalogData\Utils;

use Base\ResourceCatalogData\Model\GbItemsData;

class GbItemsDataMockFactory
{
    public static function generateGbItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : GbItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $gbItemsData = new GbItemsData($id);
        $gbItemsData->setId($id);

        self::generateData($gbItemsData, $faker, $value);
        $gbItemsData->setStatus($faker->randomDigit());
        $gbItemsData->setCreateTime($faker->unixTime());
        $gbItemsData->setUpdateTime($faker->unixTime());
        $gbItemsData->setStatusTime($faker->unixTime());

        return $gbItemsData;
    }

    private static function generateData($gbItemsData, $faker, $value) : void
    {
        $data = isset($value['data']) ?
            $value['data'] :
            $faker->words(3, false);
        
        $gbItemsData->setData($data);
    }
}
