<?php
namespace Base\ResourceCatalogData\Utils;

use Base\Template\Model\Template;

use Base\ResourceCatalogData\Model\ErrorData;

use Base\Template\Utils\GbTemplateMockFactory;

class ErrorDataMockFactory
{
    public static function generateErrorData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ErrorData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $errorData = new ErrorData($id);
        $errorData->setId($id);

        //task
        self::generateTask($errorData, $faker, $value);
        //category
        self::generateCategory($errorData, $faker, $value);
        //template
        self::generateTemplate($errorData, $faker, $value);
        //itemsData
        self::generateItemsData($errorData, $faker, $value);
        //errorType
        self::generateErrorType($errorData, $faker, $value);
        $errorData->setErrorReason(array(ErrorData::ERROR_REASON[$errorData->getErrorType()]));

        $errorData->setCreateTime($faker->unixTime());
        $errorData->setUpdateTime($faker->unixTime());
        $errorData->setStatusTime($faker->unixTime());

        return $errorData;
    }

    protected static function generateTask($errorData, $faker, $value) : void
    {
        $task = isset($value['task']) ?
        $value['task'] : TaskMockFactory::generateTask($faker->randomDigit());
        
        $errorData->setTask($task);
    }

    protected static function generateCategory($errorData, $faker, $value) : void
    {
        $category = isset($value['category']) ?
            $value['category'] :
            $faker->randomElement(
                Template::CATEGORY
            );
        
        $errorData->setCategory($category);
    }

    protected static function generateTemplate($errorData, $faker, $value) : void
    {
        $template = isset($value['template']) ?
        $value['template'] :
        GbTemplateMockFactory::generateGbTemplate($faker->randomDigit());
        ;
        
        $errorData->setTemplate($template);
    }

    private static function generateItemsData($errorData, $faker, $value) : void
    {
        $itemsData = isset($value['itemsData']) ?
        $value['itemsData'] :
        $faker->words(3, false);
        
        $errorData->getItemsData()->setData($itemsData);
    }

    protected static function generateErrorType($errorData, $faker, $value) : void
    {
        $errorType = isset($value['errorType']) ?
            $value['errorType'] :
            $faker->randomElement(
                ErrorData::ERROR_TYPE
            );
        
        $errorData->setErrorType($errorType);
    }
}
