<?php
namespace Base\ResourceCatalogData\Utils;

use Base\ResourceCatalogData\Model\BjItemsData;

class BjItemsDataMockFactory
{
    public static function generateBjItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BjItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $bjItemsData = new BjItemsData($id);
        $bjItemsData->setId($id);

        self::generateData($bjItemsData, $faker, $value);
        $bjItemsData->setStatus($faker->randomDigit());
        $bjItemsData->setCreateTime($faker->unixTime());
        $bjItemsData->setUpdateTime($faker->unixTime());
        $bjItemsData->setStatusTime($faker->unixTime());

        return $bjItemsData;
    }

    private static function generateData($bjItemsData, $faker, $value) : void
    {
        $data = isset($value['data']) ?
            $value['data'] :
            $faker->words(3, false);
        
        $bjItemsData->setData($data);
    }
}
