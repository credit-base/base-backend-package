<?php
namespace Base\ResourceCatalogData\Utils;

trait BjItemsDataSdkUtils
{
    private function compareValueObjectAndObject(
        $bjItemsDataSdk,
        $bjItemsData
    ) {
        $this->assertEquals($bjItemsDataSdk->getId(), $bjItemsData->getId());
        $this->assertEquals($bjItemsDataSdk->getData(), $bjItemsData->getData());
        $this->assertEquals($bjItemsDataSdk->getStatus(), $bjItemsData->getStatus());
        $this->assertEquals($bjItemsDataSdk->getCreateTime(), $bjItemsData->getCreateTime());
        $this->assertEquals($bjItemsDataSdk->getStatusTime(), $bjItemsData->getStatusTime());
        $this->assertEquals($bjItemsDataSdk->getUpdateTime(), $bjItemsData->getUpdateTime());
    }
}
