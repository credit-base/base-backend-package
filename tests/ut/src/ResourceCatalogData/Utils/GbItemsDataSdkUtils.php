<?php
namespace Base\ResourceCatalogData\Utils;

trait GbItemsDataSdkUtils
{
    private function compareValueObjectAndObject(
        $gbItemsDataSdk,
        $gbItemsData
    ) {
        $this->assertEquals($gbItemsDataSdk->getId(), $gbItemsData->getId());
        $this->assertEquals($gbItemsDataSdk->getData(), $gbItemsData->getData());
        $this->assertEquals($gbItemsDataSdk->getStatus(), $gbItemsData->getStatus());
        $this->assertEquals($gbItemsDataSdk->getCreateTime(), $gbItemsData->getCreateTime());
        $this->assertEquals($gbItemsDataSdk->getStatusTime(), $gbItemsData->getStatusTime());
        $this->assertEquals($gbItemsDataSdk->getUpdateTime(), $gbItemsData->getUpdateTime());
    }
}
