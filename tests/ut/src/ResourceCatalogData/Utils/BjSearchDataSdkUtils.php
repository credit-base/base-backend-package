<?php
namespace Base\ResourceCatalogData\Utils;

trait BjSearchDataSdkUtils
{
    private function compareValueObjectAndObject(
        $bjSearchDataSdk,
        $bjSearchData
    ) {
        $this->assertEquals($bjSearchDataSdk->getId(), $bjSearchData->getId());
        $this->assertEquals($bjSearchDataSdk->getName(), $bjSearchData->getName());
        $this->assertEquals($bjSearchDataSdk->getIdentify(), $bjSearchData->getIdentify());
        $this->assertEquals($bjSearchDataSdk->getInfoClassify(), $bjSearchData->getInfoClassify());
        $this->assertEquals($bjSearchDataSdk->getInfoCategory(), $bjSearchData->getInfoCategory());
        $this->assertEquals($bjSearchDataSdk->getCrew()->getId(), $bjSearchData->getCrew()->getId());
        $this->assertEquals($bjSearchDataSdk->getSubjectCategory(), $bjSearchData->getSubjectCategory());
        $this->assertEquals($bjSearchDataSdk->getDimension(), $bjSearchData->getDimension());
        $this->assertEquals($bjSearchDataSdk->getExpirationDate(), $bjSearchData->getExpirationDate());
        $this->assertEquals($bjSearchDataSdk->getTemplate()->getId(), $bjSearchData->getTemplate()->getId());
        $this->assertEquals($bjSearchDataSdk->getItemsData()->getId(), $bjSearchData->getItemsData()->getId());
        $this->assertEquals($bjSearchDataSdk->getStatus(), $bjSearchData->getStatus());
        $this->assertEquals($bjSearchDataSdk->getHash(), $bjSearchData->getHash());
        $this->assertEquals($bjSearchDataSdk->getCreateTime(), $bjSearchData->getCreateTime());
        $this->assertEquals($bjSearchDataSdk->getUpdateTime(), $bjSearchData->getUpdateTime());
        $this->assertEquals($bjSearchDataSdk->getStatusTime(), $bjSearchData->getStatusTime());
        $this->assertEquals($bjSearchDataSdk->getTask()->getId(), $bjSearchData->getTask()->getId());
        $this->assertEquals($bjSearchDataSdk->getDescription(), $bjSearchData->getDescription());
        $this->assertEquals(
            $bjSearchDataSdk->getFrontEndProcessorStatus(),
            $bjSearchData->getFrontEndProcessorStatus()
        );
    }
}
