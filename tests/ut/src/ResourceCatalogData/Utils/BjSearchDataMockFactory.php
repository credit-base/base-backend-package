<?php
namespace Base\ResourceCatalogData\Utils;

use Base\ResourceCatalogData\Model\BjSearchData;

class BjSearchDataMockFactory
{
    use SearchDataMockFactoryTrait;
    
    public static function generateBjSearchData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BjSearchData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $bjSearchData = new BjSearchData($id);
        $bjSearchData->setId($id);

        self::generateInfoClassify($bjSearchData, $faker, $value);
        self::generateInfoCategory($bjSearchData, $faker, $value);
        self::generateCrew($bjSearchData, $faker, $value);
        self::generateSourceUnit($bjSearchData, $faker, $value);
        self::generateSubjectCategory($bjSearchData, $faker, $value);
        self::generateDimension($bjSearchData, $faker, $value);
        self::generateExpirationDate($bjSearchData, $faker, $value);
        self::generateStatus($bjSearchData, $faker, $value);
        self::generateTemplate($bjSearchData, $faker, $value);
        self::generateItemsData($bjSearchData, $faker, $value);
        self::generateTask($bjSearchData, $faker, $value);
        self::generateFrontEndProcessorStatus($bjSearchData, $faker, $value);
        self::generateDescription($bjSearchData, $faker, $value);
        
        $bjSearchData->setCreateTime($faker->unixTime());
        $bjSearchData->setUpdateTime($faker->unixTime());
        $bjSearchData->setStatusTime($faker->unixTime());

        return $bjSearchData;
    }

    private static function generateTemplate($bjSearchData, $faker, $value) : void
    {
        $template = isset($value['template']) ?
        $value['template'] :
        \Base\Template\Utils\BjTemplateMockFactory::generateBjTemplate($faker->randomDigit());
        
        $bjSearchData->setTemplate($template);
    }

    private static function generateItemsData($bjSearchData, $faker, $value) : void
    {
        $itemsData = isset($value['itemsData']) ?
        $value['itemsData'] : BjItemsDataMockFactory::generateBjItemsData($faker->randomDigit());
        
        $bjSearchData->setItemsData($itemsData);
    }

    protected static function generateStatus($bjSearchData, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                BjSearchData::STATUS
            );
        
        $bjSearchData->setStatus($status);
    }

    protected static function generateFrontEndProcessorStatus($bjSearchData, $faker, $value) : void
    {
        $frontEndProcessorStatus = isset($value['frontEndProcessorStatus']) ?
            $value['frontEndProcessorStatus'] :
            $faker->randomElement(
                BjSearchData::FRONT_END_PROCESSOR_STATUS
            );
        
        $bjSearchData->setFrontEndProcessorStatus($frontEndProcessorStatus);
    }

    protected static function generateDescription($bjSearchData, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->word();
        
        $bjSearchData->setDescription($description);
    }
}
