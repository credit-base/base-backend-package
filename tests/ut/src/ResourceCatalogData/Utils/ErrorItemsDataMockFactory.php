<?php
namespace Base\ResourceCatalogData\Utils;

use Base\ResourceCatalogData\Model\ErrorItemsData;

class ErrorItemsDataMockFactory
{
    public static function generateErrorItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ErrorItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $errorItemsData = new ErrorItemsData($id);
        $errorItemsData->setId($id);

        self::generateData($errorItemsData, $faker, $value);
        $errorItemsData->setStatus($faker->randomDigit());
        $errorItemsData->setCreateTime($faker->unixTime());
        $errorItemsData->setUpdateTime($faker->unixTime());
        $errorItemsData->setStatusTime($faker->unixTime());

        return $errorItemsData;
    }

    private static function generateData($errorItemsData, $faker, $value) : void
    {
        $data = isset($value['data']) ?
            $value['data'] :
            $faker->words(3, false);
        
        $errorItemsData->setData($data);
    }
}
