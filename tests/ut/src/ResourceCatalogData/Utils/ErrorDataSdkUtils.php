<?php
namespace Base\ResourceCatalogData\Utils;

trait ErrorDataSdkUtils
{
    private function compareValueObjectAndObject(
        $errorDataSdk,
        $errorData
    ) {
        $this->assertEquals($errorDataSdk->getId(), $errorData->getId());
        $this->assertEquals($errorDataSdk->getName(), $errorData->getName());
        $this->assertEquals($errorDataSdk->getIdentify(), $errorData->getIdentify());
        $this->assertEquals($errorDataSdk->getInfoClassify(), $errorData->getInfoClassify());
        $this->assertEquals($errorDataSdk->getInfoCategory(), $errorData->getInfoCategory());
        $this->assertEquals($errorDataSdk->getCrew()->getId(), $errorData->getCrew()->getId());
        $this->assertEquals($errorDataSdk->getSubjectCategory(), $errorData->getSubjectCategory());
        $this->assertEquals($errorDataSdk->getDimension(), $errorData->getDimension());
        $this->assertEquals($errorDataSdk->getExpirationDate(), $errorData->getExpirationDate());
        $this->assertEquals($errorDataSdk->getHash(), $errorData->getHash());
        $this->assertEquals($errorDataSdk->getTemplate()->getId(), $errorData->getTemplate()->getId());
        $this->assertEquals($errorDataSdk->getItemsData()->getId(), $errorData->getItemsData()->getId());
        $this->assertEquals($errorDataSdk->getTask()->getId(), $errorData->getTask()->getId());
        $this->assertEquals($errorDataSdk->getStatus(), $errorData->getStatus());
        $this->assertEquals($errorDataSdk->getCreateTime(), $errorData->getCreateTime());
        $this->assertEquals($errorDataSdk->getUpdateTime(), $errorData->getUpdateTime());
        $this->assertEquals($errorDataSdk->getStatusTime(), $errorData->getStatusTime());
        $this->assertEquals($errorDataSdk->getCategory(), $errorData->getCategory());
        $this->assertEquals($errorDataSdk->getErrorType(), $errorData->getErrorType());
        $this->assertEquals($errorDataSdk->getErrorReason(), $errorData->getErrorReason());
    }
}
