<?php
namespace Base\ResourceCatalogData\Utils;

trait GbSearchDataSdkUtils
{
    private function compareValueObjectAndObject(
        $gbSearchDataSdk,
        $gbSearchData
    ) {
        $this->assertEquals($gbSearchDataSdk->getId(), $gbSearchData->getId());
        $this->assertEquals($gbSearchDataSdk->getName(), $gbSearchData->getName());
        $this->assertEquals($gbSearchDataSdk->getIdentify(), $gbSearchData->getIdentify());
        $this->assertEquals($gbSearchDataSdk->getInfoClassify(), $gbSearchData->getInfoClassify());
        $this->assertEquals($gbSearchDataSdk->getInfoCategory(), $gbSearchData->getInfoCategory());
        $this->assertEquals($gbSearchDataSdk->getCrew()->getId(), $gbSearchData->getCrew()->getId());
        $this->assertEquals($gbSearchDataSdk->getSubjectCategory(), $gbSearchData->getSubjectCategory());
        $this->assertEquals($gbSearchDataSdk->getDimension(), $gbSearchData->getDimension());
        $this->assertEquals($gbSearchDataSdk->getExpirationDate(), $gbSearchData->getExpirationDate());
        $this->assertEquals($gbSearchDataSdk->getTemplate()->getId(), $gbSearchData->getTemplate()->getId());
        $this->assertEquals($gbSearchDataSdk->getItemsData()->getId(), $gbSearchData->getItemsData()->getId());
        $this->assertEquals($gbSearchDataSdk->getStatus(), $gbSearchData->getStatus());
        $this->assertEquals($gbSearchDataSdk->getHash(), $gbSearchData->getHash());
        $this->assertEquals($gbSearchDataSdk->getCreateTime(), $gbSearchData->getCreateTime());
        $this->assertEquals($gbSearchDataSdk->getUpdateTime(), $gbSearchData->getUpdateTime());
        $this->assertEquals($gbSearchDataSdk->getStatusTime(), $gbSearchData->getStatusTime());
        $this->assertEquals($gbSearchDataSdk->getTask()->getId(), $gbSearchData->getTask()->getId());
        $this->assertEquals($gbSearchDataSdk->getDescription(), $gbSearchData->getDescription());
        $this->assertEquals(
            $gbSearchDataSdk->getFrontEndProcessorStatus(),
            $gbSearchData->getFrontEndProcessorStatus()
        );
    }
}
