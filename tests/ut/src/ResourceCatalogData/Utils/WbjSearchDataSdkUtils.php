<?php
namespace Base\ResourceCatalogData\Utils;

trait WbjSearchDataSdkUtils
{
    private function compareValueObjectAndObject(
        $wbjSearchDataSdk,
        $wbjSearchData
    ) {
        $this->assertEquals($wbjSearchDataSdk->getId(), $wbjSearchData->getId());
        $this->assertEquals($wbjSearchDataSdk->getName(), $wbjSearchData->getName());
        $this->assertEquals($wbjSearchDataSdk->getIdentify(), $wbjSearchData->getIdentify());
        $this->assertEquals($wbjSearchDataSdk->getInfoClassify(), $wbjSearchData->getInfoClassify());
        $this->assertEquals($wbjSearchDataSdk->getInfoCategory(), $wbjSearchData->getInfoCategory());
        $this->assertEquals($wbjSearchDataSdk->getCrew()->getId(), $wbjSearchData->getCrew()->getId());
        $this->assertEquals($wbjSearchDataSdk->getSubjectCategory(), $wbjSearchData->getSubjectCategory());
        $this->assertEquals($wbjSearchDataSdk->getDimension(), $wbjSearchData->getDimension());
        $this->assertEquals($wbjSearchDataSdk->getExpirationDate(), $wbjSearchData->getExpirationDate());
        $this->assertEquals($wbjSearchDataSdk->getTemplate()->getId(), $wbjSearchData->getTemplate()->getId());
        $this->assertEquals($wbjSearchDataSdk->getItemsData()->getId(), $wbjSearchData->getItemsData()->getId());
        $this->assertEquals($wbjSearchDataSdk->getStatus(), $wbjSearchData->getStatus());
        $this->assertEquals($wbjSearchDataSdk->getHash(), $wbjSearchData->getHash());
        $this->assertEquals($wbjSearchDataSdk->getCreateTime(), $wbjSearchData->getCreateTime());
        $this->assertEquals($wbjSearchDataSdk->getUpdateTime(), $wbjSearchData->getUpdateTime());
        $this->assertEquals($wbjSearchDataSdk->getStatusTime(), $wbjSearchData->getStatusTime());
        $this->assertEquals($wbjSearchDataSdk->getTask()->getId(), $wbjSearchData->getTask()->getId());
    }
}
