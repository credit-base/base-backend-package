<?php
namespace Base\ResourceCatalogData\Strategy;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class DateStrategyTest extends TestCase
{
    private $dateStub;
    
    public function setUp()
    {
        $this->dateStub = new DateStrategy();
    }

    public function tearDown()
    {
        unset($this->dateStub);
    }

    public function testImplementsIDataStrategy()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Strategy\IDataStrategy',
            $this->dateStub
        );
    }

    /**
     * @dataProvider validateProvider
     */
    public function testValidate($actual, $expected)
    {
        $result = $this->dateStub->validate($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals('data', Core::getLastError()->getSource()['pointer']);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->date('Ymd'), true),
            array(['value'=>$faker->randomFloat(2, 0, 99999999)], false)
        );
    }

    public function testMock()
    {
        $result = $this->dateStub->mock();
        
        $this->assertIsNumeric($result);
    }
}
