<?php
namespace Base\ResourceCatalogData\Strategy;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Model\ISearchDataAble;

class CharacterStrategyTest extends TestCase
{
    private $characterStub;
    
    public function setUp()
    {
        $this->characterStub = new CharacterStrategy();
    }

    public function tearDown()
    {
        unset($this->characterStub);
    }

    public function testImplementsIDataStrategy()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Strategy\IDataStrategy',
            $this->characterStub
        );
    }

    /**
     * @dataProvider validateProvider
     */
    public function testValidate($actual, $rule, $expected)
    {
        $result = $this->characterStub->validate($actual, $rule);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals('data', Core::getLastError()->getSource()['pointer']);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $rule['length'] = 100;

        return array(
            array($faker->text($rule['length']), $rule, true),
            array($faker->sentence($rule['length']), $rule, false)
        );
    }

    public function testMockName()
    {
        $rule['length'] = 100;
        $rule['identify'] = 'ZTMC';
        $rule['subjectCategory'] = [ISearchDataAble::SUBJECT_CATEGORY['ZRR']];
        
        $result = $this->characterStub->mock($rule);
        
        $this->assertIsString($result);
    }

    public function testMockIdentify()
    {
        $rule['length'] = 100;
        $rule['identify'] = 'TYSHXYDM';
        
        $result = $this->characterStub->mock($rule);
        
        $this->assertIsString($result);
    }
}
