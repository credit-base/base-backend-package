<?php
namespace Base\ResourceCatalogData\Strategy;

use Marmot\Core;
use Prophecy\Argument;

use PHPUnit\Framework\TestCase;

class CollectionStrategyTest extends TestCase
{
    private $collectionStub;

    public function setUp()
    {
        $this->collectionStub = new CollectionStrategy();
    }

    public function tearDown()
    {
        unset($this->collectionStub);
    }

    public function testImplementsIDataStrategy()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Strategy\IDataStrategy',
            $this->collectionStub
        );
    }

    /**
     * @dataProvider validateProvider
     */
    public function testValidate($actual, $rule, $expected)
    {
        $result = $this->collectionStub->validate($actual, $rule);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals('data', Core::getLastError()->getSource()['pointer']);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $rule = array();

        $rule['options'] = $faker->words(3, false);
        ;

        return array(
            array($rule['options'], $rule, true),
            array(current(($rule['options'])), $rule, true),
            array(array($faker->date()), $rule, false)
        );
    }

    public function testMock()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $rule['options'] = $faker->words(3, false);

        $result = $this->collectionStub->mock($rule);

        $this->assertIsArray($result);
    }
}
