<?php
namespace Base\ResourceCatalogData\Strategy;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class IntegerStrategyTest extends TestCase
{
    private $integerStub;
    
    public function setUp()
    {
        $this->integerStub = new IntegerStrategy();
    }

    public function tearDown()
    {
        unset($this->integerStub);
    }

    public function testImplementsIDataStrategy()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Strategy\IDataStrategy',
            $this->integerStub
        );
    }

    /**
     * @dataProvider validateProvider
     */
    public function testValidate($actual, $rule, $expected)
    {
        $result = $this->integerStub->validate($actual, $rule);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals('data', Core::getLastError()->getSource()['pointer']);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $rule['length'] = 2;

        return array(
            array($faker->randomNumber($rule['length']), $rule, true),
            array($faker->sentence($rule['length']), $rule, false)
        );
    }

    public function testMock()
    {
        $rule['length'] = 2;
        
        $result = $this->integerStub->mock($rule);
        
        $this->assertIsNumeric($result);
    }
}
