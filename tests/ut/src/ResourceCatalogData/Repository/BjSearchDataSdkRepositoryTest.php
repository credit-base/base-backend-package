<?php
namespace Base\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use Base\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataSdkAdapter;

use Base\ResourceCatalogData\Utils\BjSearchDataMockFactory;

class BjSearchDataSdkRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(BjSearchDataSdkRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockBjSearchDataSdkRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new BjSearchDataSdkAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataSdkAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testConfirm()
    {
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData(1);
        
        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->confirm(Argument::exact($bjSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->confirm($bjSearchData);
    }

    public function testDeletes()
    {
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData(1);
        
        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->deletes(Argument::exact($bjSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->deletes($bjSearchData);
    }

    public function testDisable()
    {
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData(1);
        
        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->disable(Argument::exact($bjSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->disable($bjSearchData);
    }
}
