<?php
namespace Base\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use Base\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataSdkAdapter;

use Base\ResourceCatalogData\Utils\GbSearchDataMockFactory;

class GbSearchDataSdkRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(GbSearchDataSdkRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockGbSearchDataSdkRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new GbSearchDataSdkAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataSdkAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testConfirm()
    {
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData(1);
        
        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->confirm(Argument::exact($gbSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->confirm($gbSearchData);
    }

    public function testDeletes()
    {
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData(1);
        
        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->deletes(Argument::exact($gbSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->deletes($gbSearchData);
    }

    public function testDisable()
    {
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData(1);
        
        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->disable(Argument::exact($gbSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->disable($gbSearchData);
    }
}
