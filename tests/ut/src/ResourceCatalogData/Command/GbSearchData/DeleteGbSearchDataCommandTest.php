<?php
namespace Base\ResourceCatalogData\Command\GbSearchData;

use PHPUnit\Framework\TestCase;

class DeleteGbSearchDataCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = array(
            'id' => 1,
        );

        $this->command = new DeleteGbSearchDataCommand(
            $this->fakerData['id']
        );
    }

    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}
