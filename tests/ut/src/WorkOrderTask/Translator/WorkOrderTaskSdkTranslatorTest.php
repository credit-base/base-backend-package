<?php
namespace Base\WorkOrderTask\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\WorkOrderTask\Utils\WorkOrderTaskSdkUtils;
use BaseSdk\WorkOrderTask\Model\ParentTask as ParentTaskSdk;
use BaseSdk\WorkOrderTask\Model\WorkOrderTask as WorkOrderTaskSdk;

use Base\Template\Model\Template;
use Base\Template\Translator\BjTemplateSdkTranslator;
use BaseSdk\Template\Model\BjTemplate as BjTemplateSdk;

use BaseSdk\UserGroup\Model\UserGroup as UserGroupSdk;
use Base\UserGroup\Translator\UserGroupSdkTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WorkOrderTaskSdkTranslatorTest extends TestCase
{
    use WorkOrderTaskSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(WorkOrderTaskSdkTranslator::class)
                    ->setMethods([
                        'getParentTaskSdkTranslator',
                        'getUserGroupSdkTranslator',
                        'getTemplateTranslatorFactory',
                        'getTemplateSdkTranslator'
                    ])
                    ->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetParentTaskSdkTranslator()
    {
        $translator = new MockWorkOrderTaskSdkTranslator();
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Translator\ParentTaskSdkTranslator',
            $translator->getParentTaskSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getParentTaskSdkTranslator()
        );
    }

    public function testGetUserGroupSdkTranslator()
    {
        $translator = new MockWorkOrderTaskSdkTranslator();
        $this->assertInstanceOf(
            'Base\UserGroup\Translator\UserGroupSdkTranslator',
            $translator->getUserGroupSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getUserGroupSdkTranslator()
        );
    }

    public function testGetTemplateTranslatorFactory()
    {
        $translator = new MockWorkOrderTaskSdkTranslator();

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Translator\TemplateTranslatorFactory',
            $translator->getTemplateTranslatorFactory()
        );
    }

    public function testGetTemplateSdkTranslator()
    {
        $translator = $this->getMockBuilder(MockWorkOrderTaskSdkTranslator::class)
                                 ->setMethods(['getTemplateTranslatorFactory'])
                                 ->getMock();

        $bjTemplateTranslator = new BjTemplateSdkTranslator();
        $category = Template::CATEGORY['BJ'];

        $translatorFactory = $this->prophesize(TemplateTranslatorFactory::class);
        $translatorFactory->getTranslator(Argument::exact($category))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjTemplateTranslator);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getTemplateTranslatorFactory')
            ->willReturn($translatorFactory->reveal());

        $result = $translator->getTemplateSdkTranslator($category);
        $this->assertInstanceof('Base\Common\Translator\ISdkTranslator', $result);
        $this->assertEquals($bjTemplateTranslator, $result);
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $workOrderTask = \Base\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(1);

        $parentTaskSdk = new ParentTaskSdk($workOrderTask->getParentTask()->getId());
        $assignObjectSdk = new UserGroupSdk($workOrderTask->getAssignObject()->getId());
        $templateSdk = new BjTemplateSdk($workOrderTask->getTemplate()->getId());

        $workOrderTaskSdk = new WorkOrderTaskSdk(
            $workOrderTask->getId(),
            $parentTaskSdk,
            $templateSdk,
            $assignObjectSdk,
            $workOrderTask->getIsExistedTemplate(),
            $workOrderTask->getFeedbackRecords(),
            $workOrderTask->getReason(),
            $workOrderTask->getStatus(),
            $workOrderTask->getStatusTime(),
            $workOrderTask->getCreateTime(),
            $workOrderTask->getUpdateTime()
        );

        $parentTaskSdkTranslator = $this->prophesize(ParentTaskSdkTranslator::class);
        $parentTaskSdkTranslator->valueObjectToObject(Argument::exact($parentTaskSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($workOrderTask->getParentTask());

        $templateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $templateSdkTranslator->valueObjectToObject(Argument::exact($templateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($workOrderTask->getTemplate());

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->valueObjectToObject(Argument::exact($assignObjectSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($workOrderTask->getAssignObject());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getParentTaskSdkTranslator')
            ->willReturn($parentTaskSdkTranslator->reveal());

        $this->translator->expects($this->exactly(1))
            ->method('getTemplateSdkTranslator')
            ->willReturn($templateSdkTranslator->reveal());

        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $workOrderTaskObject = $this->translator->valueObjectToObject($workOrderTaskSdk);
        $this->assertInstanceof('Base\WorkOrderTask\Model\WorkOrderTask', $workOrderTaskObject);
        $this->compareValueObjectAndObject($workOrderTaskSdk, $workOrderTaskObject);
    }

    public function testValueObjectToObjectFail()
    {
        $workOrderTaskSdk = array();

        $workOrderTask = $this->translator->valueObjectToObject($workOrderTaskSdk);
        $this->assertInstanceof('Base\WorkOrderTask\Model\NullWorkOrderTask', $workOrderTask);
    }

    public function testObjectToValueObjectFail()
    {
        $workOrderTask = array();

        $workOrderTaskSdk = $this->translator->objectToValueObject($workOrderTask);
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\WorkOrderTask', $workOrderTaskSdk);
    }

    public function testObjectToValueObject()
    {
        $workOrderTask = \Base\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(1);

        $parentTaskSdk = new ParentTaskSdk($workOrderTask->getParentTask()->getId());
        $assignObjectSdk = new UserGroupSdk($workOrderTask->getAssignObject()->getId());
        $templateSdk = new BjTemplateSdk($workOrderTask->getTemplate()->getId());

        $parentTaskSdkTranslator = $this->prophesize(ParentTaskSdkTranslator::class);
        $parentTaskSdkTranslator->objectToValueObject(Argument::exact($workOrderTask->getParentTask()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($parentTaskSdk);

        $templateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $templateSdkTranslator->objectToValueObject(Argument::exact($workOrderTask->getTemplate()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($templateSdk);

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->objectToValueObject(Argument::exact($workOrderTask->getAssignObject()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($assignObjectSdk);
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getParentTaskSdkTranslator')
            ->willReturn($parentTaskSdkTranslator->reveal());

        $this->translator->expects($this->exactly(1))
            ->method('getTemplateSdkTranslator')
            ->willReturn($templateSdkTranslator->reveal());

        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $workOrderTaskSdk = $this->translator->objectToValueObject($workOrderTask);
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\WorkOrderTask', $workOrderTaskSdk);
        $this->compareValueObjectAndObject($workOrderTaskSdk, $workOrderTask);
    }
}
