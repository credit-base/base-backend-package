<?php
namespace Base\WorkOrderTask\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\WorkOrderTask\Utils\ParentTaskSdkUtils;
use BaseSdk\WorkOrderTask\Model\ParentTask as ParentTaskSdk;

use Base\Template\Model\Template;
use Base\Template\Translator\WbjTemplateSdkTranslator;
use BaseSdk\Template\Model\WbjTemplate as WbjTemplateSdk;

use BaseSdk\UserGroup\Model\UserGroup as UserGroupSdk;
use Base\UserGroup\Translator\UserGroupSdkTranslator;

class ParentTaskSdkTranslatorTest extends TestCase
{
    use ParentTaskSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(ParentTaskSdkTranslator::class)
                    ->setMethods([
                        'getUserGroupSdkTranslator',
                        'getTemplateTranslatorFactory',
                        'getTemplateSdkTranslator'
                    ])
                    ->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupSdkTranslator()
    {
        $translator = new MockParentTaskSdkTranslator();
        $this->assertInstanceOf(
            'Base\UserGroup\Translator\UserGroupSdkTranslator',
            $translator->getUserGroupSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getUserGroupSdkTranslator()
        );
    }

    public function testGetTemplateTranslatorFactory()
    {
        $translator = new MockParentTaskSdkTranslator();

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Translator\TemplateTranslatorFactory',
            $translator->getTemplateTranslatorFactory()
        );
    }

    public function testGetTemplateSdkTranslator()
    {
        $translator = $this->getMockBuilder(MockParentTaskSdkTranslator::class)
                                 ->setMethods(['getTemplateTranslatorFactory'])
                                 ->getMock();

        $wbjTemplateTranslator = new WbjTemplateSdkTranslator();
        $category = Template::CATEGORY['WBJ'];

        $translatorFactory = $this->prophesize(TemplateTranslatorFactory::class);
        $translatorFactory->getTranslator(Argument::exact($category))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($wbjTemplateTranslator);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getTemplateTranslatorFactory')
            ->willReturn($translatorFactory->reveal());

        $result = $translator->getTemplateSdkTranslator($category);
        $this->assertInstanceof('Base\Common\Translator\ISdkTranslator', $result);
        $this->assertEquals($wbjTemplateTranslator, $result);
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $parentTask = \Base\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(1);
        $assignObjects = $parentTask->getAssignObjects();
        $count = count($assignObjects);
        foreach ($assignObjects as $key => $assignObject) {
            $assignObjectsSdk[$key] = new UserGroupSdk($assignObject->getId());
        }

        $templateSdk = new WbjTemplateSdk($parentTask->getTemplate()->getId());

        $parentTaskSdk = new ParentTaskSdk(
            $parentTask->getId(),
            $parentTask->getTitle(),
            $parentTask->getDescription(),
            $parentTask->getEndTime(),
            $parentTask->getAttachment(),
            $parentTask->getTemplateType(),
            $templateSdk,
            $assignObjectsSdk,
            $parentTask->getFinishCount(),
            $parentTask->getRatio(),
            $parentTask->getReason(),
            $parentTask->getStatus(),
            $parentTask->getStatusTime(),
            $parentTask->getCreateTime(),
            $parentTask->getUpdateTime()
        );

        $templateSdkTranslator = $this->prophesize(WbjTemplateSdkTranslator::class);
        $templateSdkTranslator->valueObjectToObject(Argument::exact($templateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($parentTask->getTemplate());


        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        foreach ($assignObjectsSdk as $key => $assignObjectSdk) {
            $userGroupSdkTranslator->valueObjectToObject(Argument::exact($assignObjectSdk))
                                        ->shouldBeCalledTimes(1)
                                        ->willReturn($parentTask->getAssignObjects()[$key]);
        }

        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getTemplateSdkTranslator')
            ->willReturn($templateSdkTranslator->reveal());

        $this->translator->expects($this->exactly($count))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $parentTaskObject = $this->translator->valueObjectToObject($parentTaskSdk);
        $this->assertInstanceof('Base\WorkOrderTask\Model\ParentTask', $parentTaskObject);
        $this->compareValueObjectAndObject($parentTaskSdk, $parentTaskObject);
    }

    public function testValueObjectToObjectFail()
    {
        $parentTaskSdk = array();

        $parentTask = $this->translator->valueObjectToObject($parentTaskSdk);
        $this->assertInstanceof('Base\WorkOrderTask\Model\NullParentTask', $parentTask);
    }

    public function testObjectToValueObjectFail()
    {
        $parentTask = array();

        $parentTaskSdk = $this->translator->objectToValueObject($parentTask);
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\ParentTask', $parentTaskSdk);
    }

    public function testObjectToValueObject()
    {
        $parentTask = \Base\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(1);

        $assignObjects = $parentTask->getAssignObjects();
        $count = count($assignObjects);
        foreach ($assignObjects as $key => $assignObject) {
            $assignObjectsSdk[$key] = new UserGroupSdk($assignObject->getId());
        }

        $templateSdk = new WbjTemplateSdk($parentTask->getTemplate()->getId());
        $templateSdkTranslator = $this->prophesize(WbjTemplateSdkTranslator::class);
        $templateSdkTranslator->objectToValueObject(Argument::exact($parentTask->getTemplate()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($templateSdk);

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        foreach ($assignObjectsSdk as $key => $assignObjectSdk) {
            $userGroupSdkTranslator->objectToValueObject(Argument::exact($parentTask->getAssignObjects()[$key]))
                                        ->shouldBeCalledTimes(1)
                                        ->willReturn($assignObjectSdk);
        }

        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getTemplateSdkTranslator')
            ->willReturn($templateSdkTranslator->reveal());

        $this->translator->expects($this->exactly($count))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $parentTaskSdk = $this->translator->objectToValueObject($parentTask);
        $this->assertInstanceof('BaseSdk\WorkOrderTask\Model\ParentTask', $parentTaskSdk);
        $this->compareValueObjectAndObject($parentTaskSdk, $parentTask);
    }
}
