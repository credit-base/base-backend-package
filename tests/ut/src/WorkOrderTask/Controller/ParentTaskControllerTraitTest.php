<?php
namespace Base\WorkOrderTask\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class ParentTaskControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockParentTaskControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetWorkOrderTaskWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\WorkOrderTask\WidgetRule\ParentTaskWidgetRule',
            $this->trait->getParentTaskWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Repository\ParentTaskSdkRepository',
            $this->trait->getRepositoryPublic()
        );

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}
