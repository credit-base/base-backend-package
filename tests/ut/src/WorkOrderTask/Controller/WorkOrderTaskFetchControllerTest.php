<?php
namespace Base\WorkOrderTask\Controller;

use PHPUnit\Framework\TestCase;

class WorkOrderTaskFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(WorkOrderTaskFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockWorkOrderTaskFetchController();

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Repository\WorkOrderTaskSdkRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockWorkOrderTaskFetchController();

        $this->assertInstanceOf(
            'Base\WorkOrderTask\View\WorkOrderTaskView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockWorkOrderTaskFetchController();

        $this->assertEquals(
            'workOrderTasks',
            $controller->getResourceName()
        );
    }
}
