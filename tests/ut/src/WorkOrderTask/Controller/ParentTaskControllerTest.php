<?php
namespace Base\WorkOrderTask\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\WorkOrderTask\Model\ParentTask;
use Base\WorkOrderTask\WidgetRule\ParentTaskWidgetRule;
use Base\WorkOrderTask\Repository\ParentTaskSdkRepository;
use Base\WorkOrderTask\Command\ParentTask\AddParentTaskCommand;
use Base\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;

class ParentTaskControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new ParentTaskController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testAdd()
    {
        // 为 ParentTaskController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getParentTaskWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(ParentTaskController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getParentTaskWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 true
        $this->initAdd($controller, true);

        // 为 ParentTask 类建立预言
        $wbjParentTask  = $this->prophesize(ParentTask::class);

        // 为 ParentTaskSdkRepository 类建立预言, ParentTaskSdkRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的ParentTask, getRepository 方法被调用一次
        $id = 0;
        $repository = $this->prophesize(ParentTaskSdkRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($wbjParentTask);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->add 方法被调用一次, 且返回结果为 true
        $result = $controller->add();
        $this->assertTrue($result);
    }

    public function testAddFail()
    {
        // 为 ParentTaskController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getParentTaskWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(ParentTaskController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getParentTaskWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd($controller, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->add 方法被调用一次, 且返回结果为 false
        $result = $controller->add();
        $this->assertFalse($result);
    }

    protected function initAdd(ParentTaskController $controller, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestAddData();
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        foreach ($relationships['assignObjects']['data'] as $value) {
            $assignObjects[] = $value['id'];
        }

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->validateAddScenario($attributes, $relationships, $controller);

        // 为 CommandBus 类建立预言, 传入 ParentTask参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AddParentTaskCommand(
                    $attributes['templateType'],
                    $attributes['title'],
                    $attributes['description'],
                    $attributes['endTime'],
                    $attributes['attachment'],
                    $relationships['template']['data'][0]['id'],
                    $assignObjects
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    private function mockRequestAddData() : array
    {
        $data = array(
            "attributes" => array(
                "templateType" => 1,    //基础目录，国标目录 1 | 本级目录 2
                "title" => '归集地方性红名单信息',    //任务标题
                "description" => '依据国家要求，现需要按要求归集地方性红名单信息',    //任务描述
                "endTime" => '2020-01-01',    //终结时间
                "attachment" => array(  //依据附件
                    'name' => '国203号文',
                    'identify' => 'b900638af896a26de13bc89ce4f64124.pdf'
                )
            ),
            "relationships" => array(
                "template" => array( // 指派目录，根据所选择基础目录，在以下示例二选一
                    "data" => array(
                        array("type" => "gbTemplate", "id" => 1), //国标目录
                        // array("type" => "bjTemplate", "id" => 1)  //本级目录
                    )
                ),
                "assignObjects" => array( // 指派对象
                    "data" => array(
                        array("type" => "userGroups", "id" => 1),
                        array("type" => "userGroups", "id" => 2),
                        array("type" => "userGroups", "id" => 3)
                    )
                )
            )
        );

        return $data;
    }

    private function validateAddScenario(array $attributes, array $relationships, $controller)
    {
        $templateType = $attributes['templateType'];
        $title = $attributes['title'];
        $description = $attributes['description'];
        $endTime = $attributes['endTime'];
        $attachment = $attributes['attachment'];
        $template = $relationships['template']['data'][0]['id'];
        foreach ($relationships['assignObjects']['data'] as $value) {
            $assignObjects[] = $value['id'];
        }

        // 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用2次
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($template), Argument::exact('template'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatArray(Argument::exact($assignObjects), Argument::exact('assignObjects'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(2))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        // 为 ParentTaskWidgetRule 类建立预言, 验证请求参数, getParentTaskWidgetRule 方法被调用5次
        $parentTaskWidgetRule = $this->prophesize(ParentTaskWidgetRule::class);
        $parentTaskWidgetRule->templateType(Argument::exact($templateType))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $parentTaskWidgetRule->title(Argument::exact($title))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $parentTaskWidgetRule->description(Argument::exact($description))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $parentTaskWidgetRule->endTime(Argument::exact($endTime))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $parentTaskWidgetRule->pdf(Argument::exact($attachment))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(5))
            ->method('getParentTaskWidgetRule')
            ->willReturn($parentTaskWidgetRule->reveal());
    }

    /**
     * 测试 revoke 成功
     * 1. 为 ParentTaskController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getCommandBus、getRepository、render方法
     * 2. 调用 $this->initRevoke(), 期望结果为 true
     * 3. 为 ParentTask 类建立预言
     * 4. 为 ParentTaskSdkRepository 类建立预言, ParentTaskSdkRepository->fetchOne 方法被调用1次,
     *    且 返回结果为 预言的ParentTask, getRepository 方法被调用1次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->revoke 方法被调用1次, 且返回结果为 true
     */
    public function testRevoke()
    {
        // 为 ParentTaskController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(ParentTaskController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initRevoke($controller, $id, true);

        // 为 ParentTask 类建立预言
        $workOrderTask = $this->prophesize(ParentTask::class);

        // 为 ParentTaskSdkRepository 类建立预言, ParentTaskSdkRepository->fetchOne 方法被调用1次,
        // 且 返回结果为 预言的ParentTask, getRepository 方法被调用1次
        $repository = $this->prophesize(ParentTaskSdkRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($workOrderTask);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->revoke 方法被调用一次, 且返回结果为 true
        $result = $controller->revoke($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 revoke 失败
     * 1. 为 ParentTaskController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getCommandBus、displayError 方法
     * 2. 调用 $this->initRevoke(), 期望结果为 false
     * 3. displayError 方法被调用1次, 且controller返回结果为 false
     * 4. controller->revoke 方法被调用1次, 且返回结果为 false
     */
    public function testRevokeFail()
    {
        // 为 ParentTaskController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(ParentTaskController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initRevoke(), 期望结果为 false
        $this->initRevoke($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->revoke 方法被调用一次, 且返回结果为 false
        $result = $controller->revoke($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 revoke 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用1次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数, getCommonWidgetRule 方法被调用1次
     * 4. 为 CommandBus 类建立预言, 传入 RevokeParentTaskCommand参数, 且 send 方法被调1次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initRevoke(ParentTaskController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestRevokeData();
        $attributes = $data['attributes'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用1次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->validateRevokeScenario($attributes, $controller);

        // 为 CommandBus 类建立预言, 传入 RevokeParentTaskCommand参数, 且 send 方法被调用1次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用1次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new RevokeParentTaskCommand(
                    $attributes['reason'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    private function mockRequestRevokeData() : array
    {
        return array(
            "attributes" => array(
                "reason" => '原因'
            )
        );
    }

    private function validateRevokeScenario(array $attributes, $controller)
    {
        $reason = $attributes['reason'];

        // 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用1次
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->reason(Argument::exact($reason))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());
    }
}
