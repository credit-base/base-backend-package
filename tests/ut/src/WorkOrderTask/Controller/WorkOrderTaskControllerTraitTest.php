<?php
namespace Base\WorkOrderTask\Controller;

use PHPUnit\Framework\TestCase;

class WorkOrderTaskControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockWorkOrderTaskControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetWorkOrderTaskWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule',
            $this->trait->getWorkOrderTaskWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Repository\WorkOrderTaskSdkRepository',
            $this->trait->getRepositoryPublic()
        );

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}
