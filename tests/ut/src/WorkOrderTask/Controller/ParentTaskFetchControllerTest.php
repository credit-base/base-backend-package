<?php
namespace Base\WorkOrderTask\Controller;

use PHPUnit\Framework\TestCase;

class ParentTaskFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(ParentTaskFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockParentTaskFetchController();

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Repository\ParentTaskSdkRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockParentTaskFetchController();

        $this->assertInstanceOf(
            'Base\WorkOrderTask\View\ParentTaskView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockParentTaskFetchController();

        $this->assertEquals(
            'parentTasks',
            $controller->getResourceName()
        );
    }
}
