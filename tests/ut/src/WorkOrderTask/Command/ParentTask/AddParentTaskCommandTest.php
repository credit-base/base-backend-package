<?php
namespace Base\WorkOrderTask\Command\ParentTask;

use PHPUnit\Framework\TestCase;

class AddParentTaskCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'templateType' => $faker->randomNumber(),
            'title' => $faker->word,
            'description' => $faker->word,
            'endTime' => $faker->word,
            'attachment' => array($faker->word),
            'template' => $faker->randomNumber(),
            'assignObjects' => array($faker->randomNumber()),
            'id' => 0
        );

        $this->command = new AddParentTaskCommand(
            $this->fakerData['templateType'],
            $this->fakerData['title'],
            $this->fakerData['description'],
            $this->fakerData['endTime'],
            $this->fakerData['attachment'],
            $this->fakerData['template'],
            $this->fakerData['assignObjects'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testTemplateTypeParameter()
    {
        $this->assertEquals($this->fakerData['templateType'], $this->command->templateType);
    }

    public function testTitleParameter()
    {
        $this->assertEquals($this->fakerData['title'], $this->command->title);
    }

    public function testDescriptionParameter()
    {
        $this->assertEquals($this->fakerData['description'], $this->command->description);
    }

    public function testEndTimeParameter()
    {
        $this->assertEquals($this->fakerData['endTime'], $this->command->endTime);
    }

    public function testAttachmentParameter()
    {
        $this->assertEquals($this->fakerData['attachment'], $this->command->attachment);
    }

    public function testTemplateParameter()
    {
        $this->assertEquals($this->fakerData['template'], $this->command->template);
    }

    public function testAssignObjectsParameter()
    {
        $this->assertEquals($this->fakerData['assignObjects'], $this->command->assignObjects);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testDefaultIdParameter()
    {
        $this->command = new AddParentTaskCommand(
            $this->fakerData['templateType'],
            $this->fakerData['title'],
            $this->fakerData['description'],
            $this->fakerData['endTime'],
            $this->fakerData['attachment'],
            $this->fakerData['template'],
            $this->fakerData['assignObjects']
        );

        $this->assertEquals(0, $this->command->id);
    }
}
