<?php
namespace Base\WorkOrderTask\Utils;

trait WorkOrderTaskSdkUtils
{
    private function compareValueObjectAndObject(
        $workOrderTaskSdk,
        $workOrderTask
    ) {
        $this->assertEquals($workOrderTaskSdk->getId(), $workOrderTask->getId());
        $this->assertEquals($workOrderTaskSdk->getParentTask()->getId(), $workOrderTask->getParentTask()->getId());
        $this->assertEquals($workOrderTaskSdk->getTemplate()->getId(), $workOrderTask->getTemplate()->getId());
        $this->assertEquals(
            $workOrderTaskSdk->getAssignObject()->getId(),
            $workOrderTask->getAssignObject()->getId()
        );
        $this->assertEquals($workOrderTaskSdk->getIsExistedTemplate(), $workOrderTask->getIsExistedTemplate());
        $this->assertEquals($workOrderTaskSdk->getFeedbackRecords(), $workOrderTask->getFeedbackRecords());
        $this->assertEquals($workOrderTaskSdk->getReason(), $workOrderTask->getReason());
        $this->assertEquals($workOrderTaskSdk->getStatus(), $workOrderTask->getStatus());
        $this->assertEquals($workOrderTaskSdk->getCreateTime(), $workOrderTask->getCreateTime());
        $this->assertEquals($workOrderTaskSdk->getStatusTime(), $workOrderTask->getStatusTime());
        $this->assertEquals($workOrderTaskSdk->getUpdateTime(), $workOrderTask->getUpdateTime());
    }
}
