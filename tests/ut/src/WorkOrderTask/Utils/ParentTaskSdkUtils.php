<?php
namespace Base\WorkOrderTask\Utils;

trait ParentTaskSdkUtils
{
    private function compareValueObjectAndObject(
        $parentTaskSdk,
        $parentTask
    ) {
        $this->assertEquals($parentTaskSdk->getId(), $parentTask->getId());
        $this->assertEquals($parentTaskSdk->getTitle(), $parentTask->getTitle());
        $this->assertEquals($parentTaskSdk->getDescription(), $parentTask->getDescription());
        $this->assertEquals($parentTaskSdk->getEndTime(), $parentTask->getEndTime());
        $this->assertEquals($parentTaskSdk->getAttachment(), $parentTask->getAttachment());
        $this->assertEquals($parentTaskSdk->getTemplateType(), $parentTask->getTemplateType());
        $this->assertEquals($parentTaskSdk->getTemplate()->getId(), $parentTask->getTemplate()->getId());

        $assignObjectsSdk = $parentTaskSdk->getAssignObjects();
        $assignObjects = $parentTask->getAssignObjects();

        foreach ($assignObjectsSdk as $key => $assignObjectSdk) {
            $this->assertEquals($assignObjectSdk->getId(), $assignObjects[$key]->getId());
        }
        $this->assertEquals($parentTaskSdk->getFinishCount(), $parentTask->getFinishCount());
        $this->assertEquals($parentTaskSdk->getRatio(), $parentTask->getRatio());
        $this->assertEquals($parentTaskSdk->getReason(), $parentTask->getReason());
        $this->assertEquals($parentTaskSdk->getStatus(), $parentTask->getStatus());
        $this->assertEquals($parentTaskSdk->getCreateTime(), $parentTask->getCreateTime());
        $this->assertEquals($parentTaskSdk->getStatusTime(), $parentTask->getStatusTime());
        $this->assertEquals($parentTaskSdk->getUpdateTime(), $parentTask->getUpdateTime());
    }
}
