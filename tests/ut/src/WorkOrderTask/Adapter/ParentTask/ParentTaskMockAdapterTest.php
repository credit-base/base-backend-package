<?php
namespace Base\WorkOrderTask\Adapter\ParentTask;

use PHPUnit\Framework\TestCase;

use Base\WorkOrderTask\Model\ParentTask;

class ParentTaskMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new ParentTaskMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new ParentTask()));
    }

    public function testRevoke()
    {
        $this->assertTrue($this->adapter->revoke(new ParentTask(), array()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Model\ParentTask',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\WorkOrderTask\Model\ParentTask',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\WorkOrderTask\Model\ParentTask',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
