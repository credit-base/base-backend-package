<?php
namespace Base\WorkOrderTask\Adapter\ParentTask;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Common\Model\IObject;

use Base\WorkOrderTask\Model\ParentTask;
use Base\WorkOrderTask\Translator\ParentTaskSdkTranslator;

use BaseSdk\WorkOrderTask\Model\ParentTask as ParentTaskSdk;
use BaseSdk\WorkOrderTask\Repository\ParentTaskRestfulRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ParentTaskSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockParentTaskSdkAdapter::class)
                           ->setMethods([
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction',
                               'addAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIParentTaskAdapter()
    {
        $adapter = new ParentTaskSdkAdapter();

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Translator\ParentTaskSdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Repository\ParentTaskRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Model\NullParentTask',
            $this->adapter->getNullObject()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockParentTaskSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedParentTask = new ParentTask();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedParentTask);

        //验证
        $parentTask = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedParentTask, $parentTask);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $parentTaskOne = new ParentTask(1);
        $parentTaskTwo = new ParentTask(2);

        $ids = [1, 2];

        $expectedParentTaskList = [];
        $expectedParentTaskList[$parentTaskOne->getId()] = $parentTaskOne;
        $expectedParentTaskList[$parentTaskTwo->getId()] = $parentTaskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedParentTaskList);

        //验证
        $parentTaskList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedParentTaskList, $parentTaskList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $parentTaskOne = new ParentTask(1);
        $parentTaskTwo = new ParentTask(2);

        $expectedParentTaskList = [];
        $expectedParentTaskList[$parentTaskOne->getId()] = $parentTaskOne;
        $expectedParentTaskList[$parentTaskTwo->getId()] = $parentTaskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedParentTaskList, 2]);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedParentTaskList, 2], $result);
    }

    //add
    public function testAdd()
    {
        //初始化
        $parentTask = new ParentTask();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($parentTask)->willReturn(true);

        //验证
        $result = $this->adapter->add($parentTask);
        $this->assertTrue($result);
    }

    private function initParentTask(bool $result)
    {
        $this->trait = $this->getMockBuilder(MockParentTaskSdkAdapter::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $parentTask = new ParentTask(1);
        $parentTaskSdk = new ParentTaskSdk(1);
        $keys = array();

        $translator = $this->prophesize(ParentTaskSdkTranslator::class);
        $translator->objectToValueObject($parentTask, $keys)->shouldBeCalledTimes(1)->willReturn($parentTaskSdk);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(ParentTaskRestfulRepository::class);
        $repository->revoke($parentTaskSdk, $keys)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return [$parentTask, $keys];
    }

    public function testRevoke()
    {
        list($parentTask, $keys) = $this->initParentTask(true);

        $result = $this->trait->revoke($parentTask, $keys);
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        list($parentTask, $keys) = $this->initParentTask(false);

        $result = $this->trait->revoke($parentTask, $keys);
        $this->assertFalse($result);
    }
}
