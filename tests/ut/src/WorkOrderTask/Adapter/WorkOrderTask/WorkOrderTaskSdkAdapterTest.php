<?php
namespace Base\WorkOrderTask\Adapter\WorkOrderTask;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Common\Model\IObject;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\Translator\WorkOrderTaskSdkTranslator;

use BaseSdk\WorkOrderTask\Model\WorkOrderTask as WorkOrderTaskSdk;
use BaseSdk\WorkOrderTask\Repository\WorkOrderTaskRestfulRepository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WorkOrderTaskSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockWorkOrderTaskSdkAdapter::class)
                           ->setMethods([
                               'fetchAssignObject',
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIWorkOrderTaskAdapter()
    {
        $adapter = new WorkOrderTaskSdkAdapter();

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'Base\WorkOrderTask\Translator\WorkOrderTaskSdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\WorkOrderTask\Repository\WorkOrderTaskRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Model\NullWorkOrderTask',
            $this->adapter->getNullObject()
        );
    }

    public function testGetUserGroupRepository()
    {
        $adapter = new MockWorkOrderTaskSdkAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockWorkOrderTaskSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedWorkOrderTask = new WorkOrderTask();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedWorkOrderTask);
        $this->adapter->expects($this->exactly(1))->method('fetchAssignObject')->with($expectedWorkOrderTask);

        //验证
        $workOrderTask = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedWorkOrderTask, $workOrderTask);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $workOrderTaskOne = new WorkOrderTask(1);
        $workOrderTaskTwo = new WorkOrderTask(2);

        $ids = [1, 2];

        $expectedWorkOrderTaskList = [];
        $expectedWorkOrderTaskList[$workOrderTaskOne->getId()] = $workOrderTaskOne;
        $expectedWorkOrderTaskList[$workOrderTaskTwo->getId()] = $workOrderTaskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedWorkOrderTaskList);
        $this->adapter->expects($this->exactly(1))->method('fetchAssignObject')->with($expectedWorkOrderTaskList);

        //验证
        $workOrderTaskList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedWorkOrderTaskList, $workOrderTaskList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $workOrderTaskOne = new WorkOrderTask(1);
        $workOrderTaskTwo = new WorkOrderTask(2);

        $expectedWorkOrderTaskList = [];
        $expectedWorkOrderTaskList[$workOrderTaskOne->getId()] = $workOrderTaskOne;
        $expectedWorkOrderTaskList[$workOrderTaskTwo->getId()] = $workOrderTaskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedWorkOrderTaskList, 2]);

        $this->adapter->expects($this->exactly(1))->method('fetchAssignObject')->with($expectedWorkOrderTaskList);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedWorkOrderTaskList, 2], $result);
    }

    private function initConfirm(bool $result)
    {
        $this->trait = $this->getMockBuilder(MockWorkOrderTaskSdkAdapter::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $workOrderTask = new WorkOrderTask(1);
        $workOrderTaskSdk = new WorkOrderTaskSdk(1);

        $translator = $this->prophesize(WorkOrderTaskSdkTranslator::class);
        $translator->objectToValueObject($workOrderTask)->shouldBeCalledTimes(1)->willReturn($workOrderTaskSdk);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(WorkOrderTaskRestfulRepository::class);
        $repository->confirm($workOrderTaskSdk)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return $workOrderTask;
    }

    public function testConfirm()
    {
        $workOrderTask = $this->initConfirm(true);

        $result = $this->trait->confirm($workOrderTask);
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $workOrderTask = $this->initConfirm(false);

        $result = $this->trait->confirm($workOrderTask);
        $this->assertFalse($result);
    }

    private function initWorkOrderTask(bool $result, string $method)
    {
        $this->trait = $this->getMockBuilder(MockWorkOrderTaskSdkAdapter::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $workOrderTask = new WorkOrderTask(1);
        $workOrderTaskSdk = new WorkOrderTaskSdk(1);
        $keys = array();

        $translator = $this->prophesize(WorkOrderTaskSdkTranslator::class);
        $translator->objectToValueObject(
            $workOrderTask,
            $keys
        )->shouldBeCalledTimes(1)->willReturn($workOrderTaskSdk);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(WorkOrderTaskRestfulRepository::class);
        $repository->$method($workOrderTaskSdk, $keys)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return [$workOrderTask, $keys];
    }

    public function testEnd()
    {
        list($workOrderTask, $keys) = $this->initWorkOrderTask(true, 'end');

        $result = $this->trait->end($workOrderTask, $keys);
        $this->assertTrue($result);
    }

    public function testEndFail()
    {
        list($workOrderTask, $keys) = $this->initWorkOrderTask(false, 'end');

        $result = $this->trait->end($workOrderTask, $keys);
        $this->assertFalse($result);
    }

    public function testFeedback()
    {
        list($workOrderTask, $keys) = $this->initWorkOrderTask(true, 'feedback');

        $result = $this->trait->feedback($workOrderTask, $keys);
        $this->assertTrue($result);
    }

    public function testFeedbackFail()
    {
        list($workOrderTask, $keys) = $this->initWorkOrderTask(false, 'feedback');

        $result = $this->trait->feedback($workOrderTask, $keys);
        $this->assertFalse($result);
    }

    public function testRevoke()
    {
        list($workOrderTask, $keys) = $this->initWorkOrderTask(true, 'revoke');

        $result = $this->trait->revoke($workOrderTask, $keys);
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        list($workOrderTask, $keys) = $this->initWorkOrderTask(false, 'revoke');

        $result = $this->trait->revoke($workOrderTask, $keys);
        $this->assertFalse($result);
    }

    //fetchAssignObject
    public function testFetchAssignObjectIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockWorkOrderTaskSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchAssignObjectByObject'
                                ]
                           )
                           ->getMock();
        
        $workOrderTask = new WorkOrderTask();

        $adapter->expects($this->exactly(1))->method('fetchAssignObjectByObject')->with($workOrderTask);

        $adapter->fetchAssignObject($workOrderTask);
    }

    public function testFetchAssignObjectIsArray()
    {
        $adapter = $this->getMockBuilder(MockWorkOrderTaskSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchAssignObjectByList'
                                ]
                           )
                           ->getMock();
        
        $workOrderTaskOne = new WorkOrderTask(1);
        $workOrderTaskTwo = new WorkOrderTask(2);
        
        $workOrderTaskList[$workOrderTaskOne->getId()] = $workOrderTaskOne;
        $workOrderTaskList[$workOrderTaskTwo->getId()] = $workOrderTaskTwo;

        $adapter->expects($this->exactly(1))->method('fetchAssignObjectByList')->with($workOrderTaskList);

        $adapter->fetchAssignObject($workOrderTaskList);
    }

    //fetchAssignObjectByObject
    public function testFetchAssignObjectByObject()
    {
        $adapter = $this->getMockBuilder(MockWorkOrderTaskSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $userGroupId = 1;
        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup($userGroupId);
        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setAssignObject($userGroup);
        
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))->shouldBeCalledTimes(1)->willReturn($userGroup);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchAssignObjectByObject($workOrderTask);
    }

    //fetchAssignObjectByList
    public function testFetchAssignObjectByList()
    {
        $adapter = $this->getMockBuilder(MockWorkOrderTaskSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $workOrderTaskOne = \Base\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        $userGroupOne = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);
        $workOrderTaskOne->setAssignObject($userGroupOne);

        $workOrderTaskTwo = \Base\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(2);
        $userGroupTwo = \Base\UserGroup\Utils\MockFactory::generateUserGroup(2);
        $workOrderTaskTwo->setAssignObject($userGroupTwo);
        $userGroupIds = [1, 2];
        
        $userGroupList[$userGroupOne->getId()] = $userGroupOne;
        $userGroupList[$userGroupTwo->getId()] = $userGroupTwo;
        
        $workOrderTaskList[$workOrderTaskOne->getId()] = $workOrderTaskOne;
        $workOrderTaskList[$workOrderTaskTwo->getId()] = $workOrderTaskTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(
            Argument::exact($userGroupIds)
        )->shouldBeCalledTimes(1)->willReturn($userGroupList);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchAssignObjectByList($workOrderTaskList);
    }
}
