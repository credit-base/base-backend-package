<?php
namespace Base\WorkOrderTask\View;

use PHPUnit\Framework\TestCase;

use Base\WorkOrderTask\Model\WorkOrderTask;

class WorkOrderTaskViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $workOrderTask = new WorkOrderTaskView(new WorkOrderTask());
        $this->assertInstanceof('Base\Common\View\CommonView', $workOrderTask);
    }
}
