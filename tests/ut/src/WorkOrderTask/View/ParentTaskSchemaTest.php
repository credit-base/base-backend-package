<?php
namespace Base\WorkOrderTask\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class ParentTaskSchemaTest extends TestCase
{
    private $parentTaskSchema;

    private $parentTask;

    public function setUp()
    {
        $this->parentTaskSchema = new ParentTaskSchema(new Factory());

        $this->parentTask = \Base\WorkOrderTask\Utils\ParentTaskMockFactory::generateParentTask(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->parentTaskSchema);
        unset($this->parentTask);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->parentTaskSchema);
    }

    public function testGetId()
    {
        $result = $this->parentTaskSchema->getId($this->parentTask);

        $this->assertEquals($result, $this->parentTask->getId());
    }


    public function testGetAttributes()
    {
        $result = $this->parentTaskSchema->getAttributes($this->parentTask);

        $this->assertEquals($result['templateType'], $this->parentTask->getTemplateType());
        $this->assertEquals($result['title'], $this->parentTask->getTitle());
        $this->assertEquals($result['description'], $this->parentTask->getDescription());
        $this->assertEquals($result['endTime'], $this->parentTask->getEndTime());
        $this->assertEquals($result['attachment'], $this->parentTask->getAttachment());
        $this->assertEquals($result['ratio'], $this->parentTask->getRatio());
        $this->assertEquals($result['reason'], $this->parentTask->getReason());
        $this->assertEquals($result['status'], $this->parentTask->getStatus());
        $this->assertEquals($result['createTime'], $this->parentTask->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->parentTask->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->parentTask->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->parentTaskSchema->getRelationships($this->parentTask, 0, array());

        $this->assertEquals($result['template'], ['data' => $this->parentTask->getTemplate()]);
        $this->assertEquals($result['assignObjects'], ['data' => $this->parentTask->getAssignObjects()]);
    }
}
