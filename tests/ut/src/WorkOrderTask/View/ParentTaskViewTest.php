<?php
namespace Base\WorkOrderTask\View;

use PHPUnit\Framework\TestCase;

use Base\WorkOrderTask\Model\ParentTask;

class ParentTaskViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $parentTask = new ParentTaskView(new ParentTask());
        $this->assertInstanceof('Base\Common\View\CommonView', $parentTask);
    }
}
