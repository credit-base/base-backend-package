<?php
namespace Base\WorkOrderTask\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;

class ParentTaskTest extends TestCase
{
    private $parentTask;

    public function setUp()
    {
        $this->parentTask = new ParentTask();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->parentTask);
    }

    public function testImplementsIObjec()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->parentTask
        );
    }

    public function testGetRepository()
    {
        $parentTask = new MockParentTask();
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $parentTask->getRepository()
        );
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Repository\ParentTaskSdkRepository',
            $parentTask->getRepository()
        );
    }

    public function testAdd()
    {
        //初始化
        $parentTask = $this->getMockBuilder(MockParentTask::class)->setMethods(['getRepository'])->getMock();

        //预言 IParentTaskAdapter
        $repository = $this->prophesize(IParentTaskAdapter::class);
        $repository->add(Argument::exact($parentTask))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $parentTask->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $parentTask->add();
        $this->assertTrue($result);
    }

    public function testRevoke()
    {
        //初始化
        $parentTask = $this->getMockBuilder(MockParentTask::class)->setMethods(['getRepository'])->getMock();

        //预言 IParentTaskAdapter
        $repository = $this->prophesize(IParentTaskAdapter::class);
        $repository->revoke(
            Argument::exact($parentTask),
            Argument::exact(array('reason'))
        )->shouldBeCalledTimes(1)->willReturn(true);
        //绑定
        $parentTask->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $parentTask->revoke();
        $this->assertTrue($result);
    }
}
