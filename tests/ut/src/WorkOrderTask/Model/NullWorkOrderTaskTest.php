<?php
namespace Base\WorkOrderTask\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullWorkOrderTaskTest extends TestCase
{
    private $workOrderTask;

    public function setUp()
    {
        $this->workOrderTask = $this->getMockBuilder(NullWorkOrderTask::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->workOrderTask);
    }

    public function testExtendsWorkOrderTask()
    {
        $this->assertInstanceof('Base\WorkOrderTask\Model\WorkOrderTask', $this->workOrderTask);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->workOrderTask);
    }

    public function testRevoke()
    {
        $this->workOrderTask->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->workOrderTask->revoke());
    }

    public function testConfirm()
    {
        $this->workOrderTask->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->workOrderTask->confirm());
    }

    public function testEnd()
    {
        $this->workOrderTask->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->workOrderTask->end());
    }

    public function testFeedback()
    {
        $this->workOrderTask->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->workOrderTask->feedback());
    }

    public function testIsDqr()
    {
        $this->workOrderTask->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->workOrderTask->isDqr());
    }

    public function testIsGjz()
    {
        $this->workOrderTask->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->workOrderTask->isGjz());
    }

    public function testResourceNotExist()
    {
        $nullWorkOrderTask = new MockNullWorkOrderTask();
        $result = $nullWorkOrderTask->publicResourceNotExist();

        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }
}
