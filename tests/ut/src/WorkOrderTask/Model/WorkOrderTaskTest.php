<?php
namespace Base\WorkOrderTask\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class WorkOrderTaskTest extends TestCase
{
    private $workOrderTask;

    public function setUp()
    {
        $this->workOrderTask = new WorkOrderTask();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->workOrderTask);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->workOrderTask
        );
    }

    public function testGetRepository()
    {
        $workOrderTask = new MockWorkOrderTask();
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $workOrderTask->getRepository()
        );
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Repository\WorkOrderTaskSdkRepository',
            $workOrderTask->getRepository()
        );
    }

    public function testRevokeFail()
    {
        //初始化
        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setStatus(WorkOrderTask::STATUS['YZJ']);
        
        //验证
        $result = $workOrderTask->revoke();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testRevokeSuccess()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(WorkOrderTask::class)
                           ->setMethods([
                               'getRepository',
                               'isDqr'
                            ])->getMock();

        $workOrderTask->setStatus(WorkOrderTask::STATUS['DQR']);
        $workOrderTask->expects($this->exactly(1))->method('isDqr')->willReturn(true);
        
        //预言 IWorkOrderTaskAdapter
        $repository = $this->prophesize(IWorkOrderTaskAdapter::class);
        $repository->revoke(
            Argument::exact($workOrderTask),
            Argument::exact(['reason'])
        )->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $workOrderTask->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $workOrderTask->revoke();
        $this->assertTrue($result);
    }

    public function testConfirmSuccess()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(WorkOrderTask::class)
                           ->setMethods([
                               'isDqr',
                               'isGjz',
                               'getRepository'
                            ])->getMock();
                           
        $workOrderTask->setStatus(WorkOrderTask::STATUS['DQR']);
        
        $workOrderTask->expects($this->exactly(1))->method('isDqr')->willReturn(true);
        $workOrderTask->expects($this->any())->method('isGjz')->willReturn(true);

        //预言 IWorkOrderTaskAdapter
        $repository = $this->prophesize(IWorkOrderTaskAdapter::class);
        $repository->confirm(Argument::exact($workOrderTask))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $workOrderTask->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $workOrderTask->confirm();
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        //初始化
        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setStatus(WorkOrderTask::STATUS['YZJ']);
        
        //验证
        $result = $workOrderTask->confirm();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testEndFail()
    {
        //初始化
        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setStatus(WorkOrderTask::STATUS['YZJ']);
        
        //验证
        $result = $workOrderTask->end();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testEndSuccess()
    {
        //初始化
        $workOrderTaskEnd = $this->getMockBuilder(WorkOrderTask::class)
                           ->setMethods([
                               'getRepository',
                               'isGjz'
                            ])->getMock();
        $workOrderTaskEnd->setStatus(WorkOrderTask::STATUS['GJZ']);
        $workOrderTaskEnd->expects($this->exactly(1))->method('isGjz')->willReturn(true);
                           
        //预言 IWorkOrderTaskAdapter
        $repository = $this->prophesize(IWorkOrderTaskAdapter::class);
        $repository->end(
            Argument::exact($workOrderTaskEnd),
            Argument::exact(['reason'])
        )->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $workOrderTaskEnd->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $workOrderTaskEnd->end();
        $this->assertTrue($result);
    }

    public function testFeedbackSuccess()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(WorkOrderTask::class)
                           ->setMethods(['isDqr', 'isGjz', 'getRepository'])
                           ->getMock();

        $workOrderTask->expects($this->exactly(1))->method('isDqr')->willReturn(true);
        $workOrderTask->expects($this->any())->method('isGjz')->willReturn(true);

        //预言 IWorkOrderTaskAdapter
        $repository = $this->prophesize(IWorkOrderTaskAdapter::class);
        $repository->feedback(
            Argument::exact($workOrderTask),
            Argument::exact(
                ['feedbackRecords']
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $workOrderTask->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $workOrderTask->feedback();
        $this->assertTrue($result);
    }

    public function testFeedbackFail()
    {
        //初始化
        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setStatus(WorkOrderTask::STATUS['YZJ']);
        
        //验证
        $result = $workOrderTask->feedback();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testIsDqr()
    {
        $workOrderTask = new MockWorkOrderTask();

        $workOrderTask->setStatus(WorkOrderTask::STATUS['DQR']);

        $result = $workOrderTask->isDqr();

        $this->assertTrue($result);
    }

    public function testIsGjz()
    {
        $workOrderTask = new MockWorkOrderTask();

        $workOrderTask->setStatus(WorkOrderTask::STATUS['GJZ']);

        $result = $workOrderTask->isGjz();

        $this->assertTrue($result);
    }
}
