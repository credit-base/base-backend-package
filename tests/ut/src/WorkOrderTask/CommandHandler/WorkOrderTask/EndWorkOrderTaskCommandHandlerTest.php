<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;

class EndWorkOrderTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EndWorkOrderTaskCommandHandler::class)
                                     ->setMethods(['fetchWorkOrderTask'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $command = new EndWorkOrderTaskCommand(
            $this->faker->title(),
            $this->faker->randomNumber()
        );

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->setReason($command->reason)->shouldBeCalledTimes(1);
        $workOrderTask->end()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())->method(
            'fetchWorkOrderTask'
        ) ->willReturn($workOrderTask->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}
