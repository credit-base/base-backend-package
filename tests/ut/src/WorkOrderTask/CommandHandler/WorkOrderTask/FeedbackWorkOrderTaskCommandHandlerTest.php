<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;

class FeedbackWorkOrderTaskCommandHandlerTest extends TestCase
{
    private $stub;

    private $faker;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(FeedbackWorkOrderTaskCommandHandler::class)
                                     ->setMethods(['fetchWorkOrderTask'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->faker);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->stub->execute($command);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }
    
    public function testExecute()
    {
        $command = new FeedbackWorkOrderTaskCommand(
            array('feedbackRecords'),
            $this->faker->randomNumber()
        );

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->setFeedbackRecords($command->feedbackRecords)->shouldBeCalledTimes(1);
        $workOrderTask->feedback()->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->once())->method(
            'fetchWorkOrderTask'
        ) ->willReturn($workOrderTask->reveal());

        $result = $this->stub->execute($command);
        
        $this->assertTrue($result);
    }
}
