<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Base\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand;
use Base\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;
use Base\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;
use Base\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;

class WorkOrderTaskCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new WorkOrderTaskCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testRevokeWorkOrderTaskCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeWorkOrderTaskCommand(
                $this->faker->word,
                $this->faker->randomDigit
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\WorkOrderTask\CommandHandler\WorkOrderTask\RevokeWorkOrderTaskCommandHandler',
            $commandHandler
        );
    }

    public function testConfirmWorkOrderTaskCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ConfirmWorkOrderTaskCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\WorkOrderTask\CommandHandler\WorkOrderTask\ConfirmWorkOrderTaskCommandHandler',
            $commandHandler
        );
    }

    public function testEndWorkOrderTaskCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EndWorkOrderTaskCommand(
                $this->faker->word,
                $this->faker->randomDigit
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\WorkOrderTask\CommandHandler\WorkOrderTask\EndWorkOrderTaskCommandHandler',
            $commandHandler
        );
    }

    public function testFeedbackWorkOrderTaskCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new FeedbackWorkOrderTaskCommand(
                array($this->faker->word),
                $this->faker->randomDigit
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\WorkOrderTask\CommandHandler\WorkOrderTask\FeedbackWorkOrderTaskCommandHandler',
            $commandHandler
        );
    }
}
