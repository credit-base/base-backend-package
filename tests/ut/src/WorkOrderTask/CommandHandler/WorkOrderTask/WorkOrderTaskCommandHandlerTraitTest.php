<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class WorkOrderTaskCommandHandlerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockWorkOrderTaskCommandHandlerTrait::class)
                                     ->setMethods(['getWorkOrderTaskSdkRepository'])
                                     ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetWorkOrderTaskSdkRepository()
    {
        $trait = new MockWorkOrderTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->trait->getWorkOrderTaskSdkRepositoryPublic()
        );
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Repository\WorkOrderTaskSdkRepository',
            $trait->getWorkOrderTaskSdkRepositoryPublic()
        );
    }

    public function testFetchWorkOrderTask()
    {
        $id = 1;
        $workOrderTask = \Base\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask($id);

        $repository = $this->prophesize(IWorkOrderTaskAdapter::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($workOrderTask);

        $this->trait->expects($this->exactly(1))->method(
            'getWorkOrderTaskSdkRepository'
        )->willReturn($repository->reveal());

        $result = $this->trait->fetchWorkOrderTaskPublic($id);

        $this->assertEquals($workOrderTask, $result);
    }
}
