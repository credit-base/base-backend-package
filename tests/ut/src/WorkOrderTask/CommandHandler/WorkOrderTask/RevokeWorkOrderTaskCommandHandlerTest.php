<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand;

class RevokeWorkOrderTaskCommandHandlerTest extends TestCase
{
    private $taskStub;

    private $faker;

    public function setUp()
    {
        $this->taskStub = $this->getMockBuilder(RevokeWorkOrderTaskCommandHandler::class)
                                     ->setMethods(['fetchWorkOrderTask'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->taskStub);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->taskStub
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->taskStub->execute($command);
    }

    public function testExecute()
    {
        $command = new RevokeWorkOrderTaskCommand(
            $this->faker->title(),
            $this->faker->randomNumber()
        );

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->setReason($command->reason)->shouldBeCalledTimes(1);
        $workOrderTask->Revoke()->shouldBeCalledTimes(1)->willReturn(true);

        $this->taskStub->expects($this->once())->method(
            'fetchWorkOrderTask'
        ) ->willReturn($workOrderTask->reveal());

        $result = $this->taskStub->execute($command);
        
        $this->assertTrue($result);
    }
}
