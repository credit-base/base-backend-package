<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;

class ConfirmWorkOrderTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ConfirmWorkOrderTaskCommandHandler::class)
                                     ->setMethods(['fetchWorkOrderTask'])
                                     ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $command = new ConfirmWorkOrderTaskCommand(1);

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->confirm()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())->method(
            'fetchWorkOrderTask'
        ) ->willReturn($workOrderTask->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}
