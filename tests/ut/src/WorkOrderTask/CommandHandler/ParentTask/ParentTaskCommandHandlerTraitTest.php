<?php
namespace Base\WorkOrderTask\CommandHandler\ParentTask;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use Base\Template\Model\GbTemplate;

use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use Base\Template\Model\BjTemplate;

use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

class ParentTaskCommandHandlerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockParentTaskCommandHandlerTrait::class)
                           ->setMethods(
                               [
                                    'getGbTemplateSdkRepository',
                                    'getBjTemplateSdkRepository',
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetGbTemplateSdkRepository()
    {
        $trait = new MockParentTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $trait->getGbTemplateSdkRepositoryPublic()
        );
        $this->assertInstanceOf(
            'Base\Template\Repository\GbTemplateSdkRepository',
            $trait->getGbTemplateSdkRepositoryPublic()
        );
    }

    public function testGetBjTemplateSdkRepository()
    {
        $trait = new MockParentTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $trait->getBjTemplateSdkRepositoryPublic()
        );
        $this->assertInstanceOf(
            'Base\Template\Repository\BjTemplateSdkRepository',
            $trait->getBjTemplateSdkRepositoryPublic()
        );
    }

    public function testGetUserGroupRepository()
    {
        $trait = new MockParentTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter',
            $trait->getUserGroupRepositoryPublic()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $trait->getUserGroupRepositoryPublic()
        );
    }

    public function testFetchOneGbTemplate()
    {
        $id = 1;

        $repository = $this->prophesize(IGbTemplateAdapter::class);
        $repository->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->trait->expects($this->exactly(1))
                         ->method('getGbTemplateSdkRepository')
                         ->willReturn($repository->reveal());

        $this->trait->fetchOneGbTemplatePublic($id);
    }

    public function testFetchOneBjTemplate()
    {
        $id = 1;

        $repository = $this->prophesize(IBjTemplateAdapter::class);
        $repository->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->trait->expects($this->exactly(1))
                         ->method('getBjTemplateSdkRepository')
                         ->willReturn($repository->reveal());

        $this->trait->fetchOneBjTemplatePublic($id);
    }

    public function testFetchListUserGroup()
    {
        $ids = [1, 2, 3];

        $repository = $this->prophesize(IUserGroupAdapter::class);
        $repository->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->trait->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($repository->reveal());

        $this->trait->fetchListUserGroupPublic($ids);
    }
}
