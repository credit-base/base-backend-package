<?php
namespace Base\WorkOrderTask\CommandHandler\ParentTask;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Model\ParentTask;
use Base\WorkOrderTask\Repository\ParentTaskSdkRepository;
use Base\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;

class RevokeParentTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(RevokeParentTaskCommandHandler::class)
                                     ->setMethods(['getParentTaskSdkRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetParentTaskSdkRepository()
    {
        $commandHandler = new MockRevokeParentTaskCommandHandler();
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Repository\ParentTaskSdkRepository',
            $commandHandler->getParentTaskSdkRepository()
        );
    }

    public function testExecute()
    {
        $command = new RevokeParentTaskCommand(
            $this->faker->word,
            $this->faker->randomNumber()
        );

        $parentTask = $this->prophesize(ParentTask::class);
        $parentTask->setReason($command->reason)->shouldBeCalledTimes(1);
        $parentTask->revoke()->shouldBeCalledTimes(1)->willReturn(true);

        $parentTaskRepository = $this->prophesize(ParentTaskSdkRepository::class);
        $parentTaskRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($parentTask->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getParentTaskSdkRepository')
            ->willReturn($parentTaskRepository->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}
