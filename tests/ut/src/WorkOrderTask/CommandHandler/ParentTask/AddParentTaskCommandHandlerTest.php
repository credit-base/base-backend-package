<?php
namespace Base\WorkOrderTask\CommandHandler\ParentTask;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Model\ParentTask;
use Base\WorkOrderTask\Command\ParentTask\AddParentTaskCommand;

use Base\UserGroup\Model\UserGroup;
use Base\Template\Model\GbTemplate;
use Base\Template\Model\BjTemplate;

class AddParentTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddParentTaskCommandHandler::class)
                                     ->setMethods(
                                         [
                                            'getParentTask',
                                            'fetchOneGbTemplate',
                                            'fetchOneBjTemplate',
                                            'fetchListUserGroup'
                                         ]
                                     )->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetParentTask()
    {
        $commandHandler = new MockAddParentTaskCommandHandler();
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Model\ParentTask',
            $commandHandler->getParentTask()
        );
    }

    public function testExecuteSuccess()
    {
        $command = $this->initCommand();
        $parentTask = $this->initParentTaskGbTemplate($command, true);

        $expectId = 10;
        $parentTask->getId()->shouldBeCalledTimes(1)->willReturn($expectId);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
        $this->assertEquals($expectId, $command->id);
    }

    public function testExecuteFail()
    {
        $command = $this->initCommand();
        $parentTask = $this->initParentTaskBjTemplate($command, false);

        $parentTask->getId()->shouldBeCalledTimes(0);//调用0次

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }

    private function initCommand() : ICommand
    {
        $command = new AddParentTaskCommand(
            $this->faker->randomElement(ParentTask::TEMPLATE_TYPE),
            $this->faker->word,
            $this->faker->word,
            $this->faker->word,
            array($this->faker->word),
            $this->faker->randomDigitNotNull,
            array(1)
        );

        return $command;
    }

    private function initParentTaskGbTemplate(ICommand $command, bool $result)
    {
        $parentTask = $this->prophesize(ParentTask::class);
        $command->templateType = ParentTask::TEMPLATE_TYPE['GB'];
        $parentTask->setTemplateType(Argument::exact($command->templateType))->shouldBeCalledTimes(1);
        $parentTask->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $parentTask->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $parentTask->setEndTime(Argument::exact($command->endTime))->shouldBeCalledTimes(1);
        $parentTask->setAttachment(Argument::exact($command->attachment))->shouldBeCalledTimes(1);

        $gbTemplate = $this->prophesize(GbTemplate::class);
        $this->commandHandler->expects($this->any())
            ->method('fetchOneGbTemplate')
            ->with($command->template)
            ->willReturn($gbTemplate->reveal());
        $parentTask->setTemplate(Argument::exact($gbTemplate))->shouldBeCalledTimes(1);

        $userGroupOne = new UserGroup(1);
        $assignObjects[$userGroupOne->getId()] = $userGroupOne;
        $this->commandHandler->expects($this->any())
            ->method('fetchListUserGroup')
            ->with($command->assignObjects)
            ->willReturn($assignObjects);
        $parentTask->setAssignObjects(Argument::exact($assignObjects))->shouldBeCalledTimes(1);

        $parentTask->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->any())
            ->method('getParentTask')
            ->willReturn($parentTask->reveal());

        return $parentTask;
    }

    private function initParentTaskBjTemplate(ICommand $command, bool $result)
    {
        $parentTask = $this->prophesize(ParentTask::class);
        $command->templateType = ParentTask::TEMPLATE_TYPE['BJ'];
        $parentTask->setTemplateType(Argument::exact($command->templateType))->shouldBeCalledTimes(1);
        $parentTask->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $parentTask->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $parentTask->setEndTime(Argument::exact($command->endTime))->shouldBeCalledTimes(1);
        $parentTask->setAttachment(Argument::exact($command->attachment))->shouldBeCalledTimes(1);

        $bjTemplate = $this->prophesize(BjTemplate::class);
        $this->commandHandler->expects($this->any())
            ->method('fetchOneBjTemplate')
            ->with($command->template)
            ->willReturn($bjTemplate->reveal());
        $parentTask->setTemplate(Argument::exact($bjTemplate))->shouldBeCalledTimes(1);

        $userGroupOne = new UserGroup(1);
        $assignObjects[$userGroupOne->getId()] = $userGroupOne;
        $this->commandHandler->expects($this->any())
            ->method('fetchListUserGroup')
            ->with($command->assignObjects)
            ->willReturn($assignObjects);
        $parentTask->setAssignObjects(Argument::exact($assignObjects))->shouldBeCalledTimes(1);

        $parentTask->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->any())
            ->method('getParentTask')
            ->willReturn($parentTask->reveal());

        return $parentTask;
    }
}
