<?php
namespace Base\WorkOrderTask\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Base\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskSdkAdapter;

use Base\WorkOrderTask\Utils\WorkOrderTaskMockFactory;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WorkOrderTaskSdkRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(WorkOrderTaskSdkRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockWorkOrderTaskSdkRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new WorkOrderTaskSdkAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskSdkAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testConfirm()
    {
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        
        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->confirm(Argument::exact($workOrderTask))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->confirm($workOrderTask);
    }

    public function testEnd()
    {
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        $keys = array();
        
        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->end(Argument::exact($workOrderTask), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->end($workOrderTask, $keys);
    }

    public function testFeedback()
    {
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        $keys = array();
        
        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->feedback(Argument::exact($workOrderTask), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->feedback($workOrderTask, $keys);
    }

    public function testRevoke()
    {
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1);
        $keys = array();
        
        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->revoke(Argument::exact($workOrderTask), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->revoke($workOrderTask, $keys);
    }
}
