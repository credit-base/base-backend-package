<?php
namespace Base\ApplyForm\Adapter\ApplyForm;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\News\Model\UnAuditedNews;
use Base\News\Translator\UnAuditedNewsDbTranslator;

use Base\ApplyForm\Model\NullApplyForm;
use Base\ApplyForm\Translator\TranslatorFactory;
use Base\ApplyForm\Repository\RepositoryFactory;
use Base\ApplyForm\Translator\ApplyFormDbTranslator;
use Base\ApplyForm\Adapter\ApplyForm\Query\ApplyFormRowCacheQuery;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ApplyFormDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(ApplyFormDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchRelation',
                                'fetchApplyCrew',
                                'fetchApplyUserGroup'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IApplyFormAdapter
     */
    public function testImplementsIApplyFormAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 ApplyFormDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockApplyFormDbAdapter();
        $this->assertInstanceOf(
            'Base\ApplyForm\Translator\ApplyFormDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 ApplyFormRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockApplyFormDbAdapter();
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\Query\ApplyFormRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetRepositoryFactory()
    {
        $adapter = new MockApplyFormDbAdapter();
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\RepositoryFactory',
            $adapter->getRepositoryFactory()
        );
    }

    public function testGetTranslatorFactory()
    {
        $adapter = new MockApplyFormDbAdapter();
        $this->assertInstanceOf(
            'Base\ApplyForm\Translator\TranslatorFactory',
            $adapter->getTranslatorFactory()
        );
    }

    public function testGetCrewRepository()
    {
        $adapter = new MockApplyFormDbAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }

    public function testGetUserGroupRepository()
    {
        $adapter = new MockApplyFormDbAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockApplyFormDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedApplyForm = new UnAuditedNews();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($expectedApplyForm)->willReturn(true);
        
        //验证
        $result = $this->adapter->add($expectedApplyForm);
        $this->assertTrue($result);
    }

    private function initialAddAction($result)
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                        ->setMethods(['getDbTranslator', 'getRowQuery'])
                        ->getMock();

        //初始化
        $id = 1;
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $info = array($unAuditedNews);

        //预言
        $translator = $this->prophesize(ApplyFormDbTranslator::class);
        //期望objectToArray()被执行一次并返回$info
        $translator->objectToArray($unAuditedNews)->shouldBeCalledTimes(1)->willReturn($info);

        //预言
        $rowQuery = $this->prophesize(ApplyFormRowCacheQuery::class);
        //期望add()被执行一次返回$result
        $rowQuery->add($info)->shouldBeCalledTimes(1)->willReturn($result);
        
        //揭示
        $adapter->expects($this->any())->method('getRowQuery')->willReturn($rowQuery->reveal());
        $adapter->expects($this->exactly(1))->method('getDbTranslator')->willReturn($translator->reveal());
                
        return [$adapter, $unAuditedNews];
    }

    public function testAddActionFailure()
    {
        list($adapter, $unAuditedNews) = $this->initialAddAction(false);

        //验证
        $result = $adapter->addAction($unAuditedNews);
        
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        list($adapter, $unAuditedNews) = $this->initialAddAction(true);
        //验证
        $result = $adapter->addAction($unAuditedNews);
        
        $this->assertTrue($result);
    }
    //edit
    public function testEdit()
    {
        //初始化
        $expectedApplyForm = new UnAuditedNews();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'editAction'
        )->with($expectedApplyForm, $keys)->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedApplyForm, $keys);
        $this->assertTrue($result);
    }

    private function initialEditAction($result)
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                        ->setMethods(['getDbTranslator', 'getRowQuery'])
                        ->getMock();
        //初始化
        $id = 1;
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $info = array($unAuditedNews);
        $keys = array();

        $conditionArray[$unAuditedNews->getApplyId()] = $unAuditedNews->getApplyId();
        //预言
        $translator = $this->prophesize(ApplyFormDbTranslator::class);
        //期望objectToArray()被执行一次返回$info
        $translator->objectToArray($unAuditedNews, $keys)->shouldBeCalledTimes(1)->willReturn($info);
        //预言
        $rowQuery = $this->prophesize(ApplyFormRowCacheQuery::class);
        //期望getPrimaryKey()被执行一次返回applyId
        $rowQuery->getPrimaryKey()->shouldBeCalledTimes(1)->willReturn($unAuditedNews->getApplyId());
        //update()被执行一次返回$result
        $rowQuery->update($info, $conditionArray)->shouldBeCalledTimes(1)->willReturn($result);
        
        //揭示
        $adapter->expects($this->any())->method('getRowQuery')->willReturn($rowQuery->reveal());
        $adapter->expects($this->exactly(1))->method('getDbTranslator')->willReturn($translator->reveal());
                
        return [$adapter, $unAuditedNews, $keys];
    }

    public function testEditActionFailure()
    {
        list($adapter, $unAuditedNews, $keys) = $this->initialEditAction(false);

        //验证
        $result = $adapter->editAction($unAuditedNews, $keys);
        $this->assertFalse($result);
    }

    public function testEditActionSuccess()
    {
        list($adapter, $unAuditedNews, $keys) = $this->initialEditAction(true);

        //验证
        $result = $adapter->editAction($unAuditedNews, $keys);
        $this->assertTrue($result);
    }
    
    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedApplyForm = new UnAuditedNews();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'fetchOneAction'
        )->with($id)->willReturn($expectedApplyForm);
        $this->adapter->expects($this->exactly(1))->method('fetchRelation')->with($expectedApplyForm);
        $this->adapter->expects($this->exactly(1))->method('fetchApplyCrew')->with($expectedApplyForm);
        $this->adapter->expects($this->exactly(1))->method('fetchApplyUserGroup')->with($expectedApplyForm);

        //验证
        $applyForm = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedApplyForm, $applyForm);
    }

    public function testFetchOneActionNotExist()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                        ->setMethods(['getRowQuery'])
                        ->getMock();

        $applyFormId = 1;
        //预言
        $rowQuery = $this->prophesize(ApplyFormRowCacheQuery::class);
        //期望fetchOne()被执行一次返回array()
        $rowQuery->fetchOne(Argument::exact($applyFormId))->shouldBeCalledTimes(1)->willReturn(array());

        //揭示
        $adapter->expects($this->exactly(1))->method('getRowQuery')->willReturn($rowQuery->reveal());

        //验证
        $result = $adapter->fetchOneAction($applyFormId);
        $this->assertInstanceOf('Base\ApplyForm\Model\NullApplyForm', $result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testFetchOneActionSuccess()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                ->setMethods([
                    'getRowQuery',
                    'getTranslatorFactory'
                ])->getMock();

        //初始化
        $id = 1;
        $info = array(
            'apply_info'=>base64_encode(gzcompress(serialize(array('apply_info')))),
            'apply_info_category'=>1
        );
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);

        //预言
        $rowQuery = $this->prophesize(ApplyFormRowCacheQuery::class);
        //期望fetchOne()被执行一次并返回$info
        $rowQuery->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($info);
        //揭示
        $adapter->expects($this->exactly(1))->method('getRowQuery')->willReturn($rowQuery->reveal());

        $applyInfo =unserialize(gzuncompress(base64_decode($info['apply_info'], true)));

        $info = array_merge($applyInfo, $info);

        //预言
        $translatorFactory = $this->prophesize(TranslatorFactory::class);
        $unAuditedNewsTranslator = $this->prophesize(UnAuditedNewsDbTranslator::class);
        $unAuditedNewsTranslator->arrayToObject(
            Argument::exact($info)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedNews);
        $unAuditedNewsTranslator->arrayToObjects(
            Argument::exact($info),
            Argument::exact($unAuditedNews)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedNews);

        $translatorFactory->getTranslator(Argument::exact($info['apply_info_category']))
                                         ->shouldBeCalledTimes(1)
                                         ->willReturn($unAuditedNewsTranslator->reveal());
        //揭示
        $adapter->expects($this->exactly(1))->method('getTranslatorFactory')->willReturn($translatorFactory->reveal());

        //验证
        $result = $adapter->fetchOneAction($id);
        $this->assertEquals($result, $unAuditedNews);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $applyFormOne = new UnAuditedNews(1);
        $applyFormTwo = new UnAuditedNews(2);

        $ids = [1, 2];

        $expectedApplyFormList = [];
        $expectedApplyFormList[$applyFormOne->getId()] = $applyFormOne;
        $expectedApplyFormList[$applyFormTwo->getId()] = $applyFormTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'fetchListAction'
        )->with($ids)->willReturn($expectedApplyFormList);
        $this->adapter->expects($this->exactly(1))->method('fetchRelation')->with($expectedApplyFormList);
        $this->adapter->expects($this->exactly(1))->method('fetchApplyCrew')->with($expectedApplyFormList);
        $this->adapter->expects($this->exactly(1))->method('fetchApplyUserGroup')->with($expectedApplyFormList);

        //验证
        $applyFormList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedApplyFormList, $applyFormList);
    }

    public function testFetchListActionFail()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                        ->setMethods(['getRowQuery'])
                        ->getMock();

        //初始化
        $ids = [1, 2];
        $applyFormList = array();

        //预言
        $rowQuery = $this->prophesize(ApplyFormRowCacheQuery::class);
        $rowQuery->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1)->willReturn($applyFormList);
        //揭示
        $adapter->expects($this->exactly(1))->method('getRowQuery')->willReturn($rowQuery->reveal());

        //验证
        $result = $adapter->fetchListAction($ids);
        $this->assertEquals([], $result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testFetchListAction()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                        ->setMethods(['getRowQuery', 'getTranslatorFactory'])
                        ->getMock();
        //初始化
        $unAuditedNewsOne = new UnAuditedNews(1);
        $unAuditedNewsTwo = new UnAuditedNews(2);

        $info = array(
            $unAuditedNewsOne->getApplyId() => array(
                'apply_info'=>base64_encode(gzcompress(serialize(array('apply_info')))),
                'apply_info_category'=>1
            ),
            $unAuditedNewsTwo->getApplyId() => array(
                'apply_info'=>base64_encode(gzcompress(serialize(array('apply_info')))),
                'apply_info_category'=>2
            ),
        );
    
        $expectedObjectList = [];
        $expectedObjectList[$unAuditedNewsOne->getApplyId()] = $unAuditedNewsOne;
        $expectedObjectList[$unAuditedNewsTwo->getApplyId()] = $unAuditedNewsTwo;

        $ids = [1, 2];

        //预言
        $rowQuery = $this->prophesize(ApplyFormRowCacheQuery::class);
        $rowQuery->fetchList($ids)->shouldBeCalledTimes(1)->willReturn($info);

        $adapter->expects($this->exactly(1))->method('getRowQuery')->willReturn($rowQuery->reveal());
        
        //预言
        $translatorFactory = $this->prophesize(TranslatorFactory::class);
        $unAuditedNewsTranslator = $this->prophesize(UnAuditedNewsDbTranslator::class);
        
        foreach ($info as $key => $value) {
            $applyInfo =unserialize(gzuncompress(base64_decode($value['apply_info'], true)));

            $value = array_merge($applyInfo, $value);

            $unAuditedNewsTranslator->arrayToObject(
                Argument::exact($value)
            )->shouldBeCalledTimes(1)->willReturn($expectedObjectList[$key]);
            $unAuditedNewsTranslator->arrayToObjects(
                Argument::exact($value),
                Argument::exact($expectedObjectList[$key])
            )->shouldBeCalledTimes(1)->willReturn($expectedObjectList[$key]);
    
            $translatorFactory->getTranslator(Argument::exact($value['apply_info_category']))
                                             ->shouldBeCalledTimes(1)
                                             ->willReturn($unAuditedNewsTranslator->reveal());
        }
        //揭示
        $adapter->expects($this->any())->method('getTranslatorFactory')->willReturn($translatorFactory->reveal());
                         
        //验证
        $objectList = $adapter->fetchListAction($ids);
        $this->assertEquals($expectedObjectList, $objectList);
    }

    //fetchApplyUserGroup
    public function testFetchApplyUserGroupIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['fetchApplyUserGroupByObject'])
                           ->getMock();
        
        $news = new UnAuditedNews();

        $adapter->expects($this->exactly(1))->method('fetchApplyUserGroupByObject')->with($news);

        $adapter->fetchApplyUserGroup($news);
    }

    public function testFetchApplyUserGroupIsArray()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['fetchApplyUserGroupByList'])
                           ->getMock();
        
        $newsOne = new UnAuditedNews(1);
        $newsTwo = new UnAuditedNews(2);
        
        $newsList[$newsOne->getApplyId()] = $newsOne;
        $newsList[$newsTwo->getApplyId()] = $newsTwo;

        $adapter->expects($this->exactly(1))->method('fetchApplyUserGroupByList')->with($newsList);

        $adapter->fetchApplyUserGroup($newsList);
    }

    //fetchApplyUserGroupByObject
    public function testFetchApplyUserGroupByObject()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['getUserGroupRepository'])
                           ->getMock();

        $news = new UnAuditedNews();
        $news->setApplyUserGroup(new UserGroup(1));
        
        $userGroupId = 1;

        // 为 UserGroup 类建立预言
        $userGroup  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))->shouldBeCalledTimes(1)->willReturn($userGroup);

        //揭示
        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchApplyUserGroupByObject($news);
    }

    //fetchApplyUserGroupByList
    public function testFetchUserGroupByList()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['getUserGroupRepository'])
                           ->getMock();

        $newsOne = new UnAuditedNews(1);
        $userGroupOne = new UserGroup(1);
        $newsOne->setApplyUserGroup($userGroupOne);

        $newsTwo = new UnAuditedNews(2);
        $userGroupTwo = new UserGroup(2);
        $newsTwo->setApplyUserGroup($userGroupTwo);
        $userGroupIds = [1, 2];
        
        $userGroupList[$userGroupOne->getId()] = $userGroupOne;
        $userGroupList[$userGroupTwo->getId()] = $userGroupTwo;
        
        $newsList[$newsOne->getApplyId()] = $newsOne;
        $newsList[$newsTwo->getApplyId()] = $newsTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(
            Argument::exact($userGroupIds)
        )->shouldBeCalledTimes(1)->willReturn($userGroupList);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchApplyUserGroupByList($newsList);
    }

    //fetchApplyCrew
    public function testFetchApplyCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['fetchApplyCrewByObject'])
                           ->getMock();
        
        $news = new UnAuditedNews();

        $adapter->expects($this->exactly(1))->method('fetchApplyCrewByObject')->with($news);

        $adapter->fetchApplyCrew($news);
    }

    public function testFetchApplyCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['fetchApplyCrewByList'])
                           ->getMock();
        
        $newsOne = new UnAuditedNews(1);
        $newsTwo = new UnAuditedNews(2);
        
        $newsList[$newsOne->getApplyId()] = $newsOne;
        $newsList[$newsTwo->getApplyId()] = $newsTwo;

        $adapter->expects($this->exactly(1))->method('fetchApplyCrewByList')->with($newsList);

        $adapter->fetchApplyCrew($newsList);
    }

    //fetchApplyCrewByObject
    public function testFetchApplyCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['getCrewRepository'])
                           ->getMock();

        $news = new UnAuditedNews();
        $news->setApplyCrew(new Crew(1));
        
        $crewId = 1;

        // 为 Crew 类建立预言
        $crew  = $this->prophesize(Crew::class);
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))->shouldBeCalledTimes(1)->willReturn($crew);

        //揭示
        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchApplyCrewByObject($news);
    }

    //fetchApplyCrewByList
    public function testFetchApplyCrewByList()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['getCrewRepository'])
                           ->getMock();

        $newsOne = new UnAuditedNews(1);
        $crewOne = new Crew(1);
        $newsOne->setApplyCrew($crewOne);

        $newsTwo = new UnAuditedNews(2);
        $crewTwo = new Crew(2);
        $newsTwo->setApplyCrew($crewTwo);
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $newsList[$newsOne->getApplyId()] = $newsOne;
        $newsList[$newsTwo->getApplyId()] = $newsTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))->shouldBeCalledTimes(1)->willReturn($crewList);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchApplyCrewByList($newsList);
    }

    //fetchRelation
    public function testFetchRelationIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['fetchRelationByObject'])
                           ->getMock();
        
        $news = new UnAuditedNews();

        $adapter->expects($this->exactly(1)) ->method('fetchRelationByObject')->with($news);

        $adapter->fetchRelation($news);
    }

    public function testFetchRelationIsArray()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['fetchRelationByList'])
                           ->getMock();
        
        $newsOne = new UnAuditedNews(1);
        $newsTwo = new UnAuditedNews(2);
        
        $newsList[$newsOne->getApplyId()] = $newsOne;
        $newsList[$newsTwo->getApplyId()] = $newsTwo;

        $adapter->expects($this->exactly(1))->method('fetchRelationByList')->with($newsList);

        $adapter->fetchRelation($newsList);
    }

    //fetchRelationByObject
    public function testFetchRelationByObject()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['getRepositoryFactory'])
                           ->getMock();

        $news = new UnAuditedNews();
        $news->setRelation(new Crew(1));
        
        $relationId = 1;

        // 为 Relation 类建立预言
        $relation  = $this->prophesize(Crew::class);
        $relationRepository = $this->prophesize(CrewRepository::class);
        $relationRepository->fetchOne(
            Argument::exact($relationId)
        )->shouldBeCalledTimes(1)->willReturn($relation);

        $repositoryFactory = $this->prophesize(RepositoryFactory::class);
        $repositoryFactory->getRepository(
            Argument::exact($news->getApplyInfoCategory()->getCategory())
        )->shouldBeCalledTimes(1)->willReturn($relationRepository->reveal());

        $adapter->expects($this->exactly(1))
                         ->method('getRepositoryFactory')
                         ->willReturn($repositoryFactory->reveal());

        $adapter->fetchRelationByObject($news);
    }

    //fetchRelationByList
    public function testFetchRelationByList()
    {
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['getRepositoryFactory'])
                           ->getMock();

        $unAuditedNewsList = array();
        $relationList = array();
        $relationIds = array();

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews(1);

        $unAuditedNewsList[$unAuditedNews->getApplyId()] = $unAuditedNews;
        $relationList[$unAuditedNews->getRelation()->getId()] = $unAuditedNews->getRelation();

        $relationIds = array($unAuditedNews->getRelation()->getId());

        $relationRepository = $this->prophesize(CrewRepository::class);
        $relationRepository->fetchList(
            Argument::exact($relationIds)
        )->shouldBeCalledTimes(1)->willReturn($relationList);

        $repositoryFactory = $this->prophesize(RepositoryFactory::class);
        $repositoryFactory->getRepository(
            Argument::exact($unAuditedNews->getApplyInfoCategory()->getCategory())
        )->shouldBeCalledTimes(1)->willReturn($relationRepository->reveal());

        $adapter->expects($this->exactly(1))
                         ->method('getRepositoryFactory')
                         ->willReturn($repositoryFactory->reveal());

        $adapter->fetchRelationByList($unAuditedNewsList);
    }

    public function testApplyObject()
    {
        $adapter = new MockApplyFormDbAdapter();

        $this->assertInstanceOf(
            'Base\ApplyForm\Model\IApplyFormAble',
            $adapter->applyObject()
        );
    }

    public function testFormatFilter()
    {
        $adapter = new MockApplyFormDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-applyInfoCategory
    public function testFormatFilterApplyInfoCategory()
    {
        $filter = array(
            'applyInfoCategory' => 1
        );
        $adapter = new MockApplyFormDbAdapter();

        $expectedCondition = 'apply_info_category = '.$filter['applyInfoCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-applyInfoType
    public function testFormatFilterApplyInfoType()
    {
        $filter = array(
            'applyInfoType' => 1
        );

        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['applyObject'])
                           ->getMock();

        $news = new UnAuditedNews();

        $adapter->expects($this->exactly(1))->method('applyObject')->willReturn($news);

        $expectedCondition = 'apply_info_type IN ('.$filter['applyInfoType'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-relation
    public function testFormatFilterRelation()
    {
        $filter = array(
            'relation' => 1
        );
        $adapter = new MockApplyFormDbAdapter();

        $expectedCondition = 'relation_id = '.$filter['relation'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-applyUserGroup
    public function testFormatFilterApplyUserGroup()
    {
        $filter = array(
            'applyUserGroup' => 1
        );
        $adapter = new MockApplyFormDbAdapter();

        $expectedCondition = 'apply_usergroup_id = '.$filter['applyUserGroup'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-title
    public function testFormatFilterTitle()
    {
        $filter = array(
            'title' => 'title'
        );
        $adapter = new MockApplyFormDbAdapter();

        $expectedCondition = 'title LIKE \'%'.$filter['title'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatFilter-applyStatus
    public function testFormatFilterApplyStatus()
    {
        $filter = array(
            'applyStatus' => 1,2
        );
        
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['applyObject'])
                           ->getMock();

        $news = new UnAuditedNews();

        $adapter->expects($this->exactly(1))->method('applyObject')->willReturn($news);

        $expectedCondition = 'apply_status IN ('.$filter['applyStatus'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-operationType
    public function testFormatFilterOperationType()
    {
        $filter = array(
            'operationType' => 1
        );
        $adapter = new MockApplyFormDbAdapter();

        $expectedCondition = 'operation_type = '.$filter['operationType'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    public function testFormatSort()
    {
        $adapter = new MockApplyFormDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatSort--updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['applyObject'])
                           ->getMock();

        $news = new UnAuditedNews();

        $adapter->expects($this->exactly(1))->method('applyObject')->willReturn($news);

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['applyObject'])
                           ->getMock();

        $news = new UnAuditedNews();

        $adapter->expects($this->exactly(1))->method('applyObject')->willReturn($news);

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatSort--applyStatus
    public function testFormatSortApplyStatusDesc()
    {
        $sort = array('applyStatus' => -1);
        
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['applyObject'])
                           ->getMock();

        $news = new UnAuditedNews();

        $adapter->expects($this->exactly(1))->method('applyObject')->willReturn($news);

        $expectedCondition = ' ORDER BY apply_status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-applyStatus
    public function testFormatSortApplyStatusAsc()
    {
        $sort = array('applyStatus' => 1);
        
        $adapter = $this->getMockBuilder(MockApplyFormDbAdapter::class)
                           ->setMethods(['applyObject'])
                           ->getMock();

        $news = new UnAuditedNews();

        $adapter->expects($this->exactly(1))->method('applyObject')->willReturn($news);

        $expectedCondition = ' ORDER BY apply_status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
