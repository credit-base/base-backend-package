<?php
namespace Base\ApplyForm\Adapter\ApplyForm\Query\Persistence;

use PHPUnit\Framework\TestCase;

class ApplyFormCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockApplyFormCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(ApplyFormCache::KEY, $this->cache->getKey());
    }
}
