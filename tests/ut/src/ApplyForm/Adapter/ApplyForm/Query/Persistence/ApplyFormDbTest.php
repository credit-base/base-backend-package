<?php
namespace Base\ApplyForm\Adapter\ApplyForm\Query\Persistence;

use PHPUnit\Framework\TestCase;

class ApplyFormDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockApplyFormDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(ApplyFormDb::TABLE, $this->database->getTable());
    }
}
