<?php
namespace Base\ApplyForm\Adapter\ApplyForm\Query;

use PHPUnit\Framework\TestCase;

class ApplyFormRowCacheQueryTest extends TestCase
{
    private $rowCacheQuery;

    public function setUp()
    {
        $this->rowCacheQuery = new MockApplyFormRowCacheQuery();
    }

    public function tearDown()
    {
        unset($this->rowCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowCacheQuery', $this->rowCacheQuery);
    }

    /**
     * 测试是否cache层赋值正确
     */
    public function testCorrectCacheLayer()
    {
        $this->assertInstanceof(
            'Base\ApplyForm\Adapter\ApplyForm\Query\Persistence\ApplyFormCache',
            $this->rowCacheQuery->getCacheLayer()
        );
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'Base\ApplyForm\Adapter\ApplyForm\Query\Persistence\ApplyFormDb',
            $this->rowCacheQuery->getDbLayer()
        );
    }
}
