<?php
namespace Base\ApplyForm\Repository;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class RepositoryFactoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = new RepositoryFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->repository);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $repository = $this->repository->getRepository(0);
            $this->assertInstanceOf(
                'Base\Common\Repository\NullRepository',
                $repository
            );
    }

    public function testGetController()
    {
        foreach (RepositoryFactory::MAPS as $key => $repository) {
            $this->assertInstanceOf(
                $repository,
                $this->repository->getRepository($key)
            );
        }
    }
}
