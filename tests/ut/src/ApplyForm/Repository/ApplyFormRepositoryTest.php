<?php
namespace Base\ApplyForm\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;
use Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter;

class ApplyFormRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(ApplyFormRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockApplyFormRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new ApplyFormDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IApplyFormAdapter::class);
        $adapter->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IApplyFormAdapter::class);
        $adapter->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IApplyFormAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $applyForm = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $keys = array();
        
        $adapter = $this->prophesize(IApplyFormAdapter::class);
        $adapter->add(Argument::exact($applyForm))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->add($applyForm, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $applyForm = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $keys = array();
        
        $adapter = $this->prophesize(IApplyFormAdapter::class);
        $adapter->edit(Argument::exact($applyForm), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->edit($applyForm, $keys);
    }
}
