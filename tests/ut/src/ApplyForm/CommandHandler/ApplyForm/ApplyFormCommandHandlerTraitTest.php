<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Repository\CrewRepository;

use Base\ApplyForm\Repository\ApplyFormRepository;

class ApplyFormCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockApplyFormCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetCrewRepository()
    {
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $this->trait->publicGetCrewRepository()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->trait->publicGetRepository()
        );
    }

    public function testFetchCrew()
    {
        $trait = $this->getMockBuilder(MockApplyFormCommandHandlerTrait::class)
                        ->setMethods(['getCrewRepository'])
                        ->getMock();

        $id = 2;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($crew);
                   
        $trait->expects($this->exactly(1))->method('getCrewRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchCrew($id);
        $this->assertEquals($result, $crew);
    }

    public function testFetchApplyForm()
    {
        $trait = $this->getMockBuilder(MockApplyFormCommandHandlerTrait::class)
                 ->setMethods(['getRepository']) ->getMock();
                 
        $id = 1;
        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);

        $repository = $this->prophesize(ApplyFormRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditedNews);

        $trait->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchApplyForm($id);
        $this->assertEquals($result, $unAuditedNews);
    }
}
