<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\ApplyForm\Command\ApplyForm\ApproveApplyFormCommand;
use Base\ApplyForm\Command\ApplyForm\RejectApplyFormCommand;

class ApplyFormCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new ApplyFormCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testApproveApplyFormCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveApplyFormCommand(
                $this->faker->randomDigit(1),
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\ApplyForm\CommandHandler\ApplyForm\ApproveApplyFormCommandHandler',
            $commandHandler
        );
    }

    public function testRejectApplyFormCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectApplyFormCommand(
                $this->faker->randomDigit(1),
                $this->faker->word(),
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\ApplyForm\CommandHandler\ApplyForm\RejectApplyFormCommandHandler',
            $commandHandler
        );
    }
}
