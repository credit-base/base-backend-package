<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ApplyForm\Command\ApplyForm\ApproveApplyFormCommand;

use Base\News\Model\UnAuditedNews;

class ApproveApplyFormCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveApplyFormCommandHandler::class)
                    ->setMethods(['fetchApplyForm'])
                    ->getMock();
    }

    public function testExtendsApproveCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }

    public function testFetchIApplyObject()
    {
        //初始化
        $id = 1;
        $news = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);

        $this->stub->expects($this->once())->method('fetchApplyForm')->with($id)->willReturn($news);
        //验证
        $result = $this->stub->fetchIApplyObject($id);
        $this->assertEquals($result, $news);
    }

    public function testExecuteAction()
    {
        $stub = $this->getMockBuilder(MockApproveApplyFormCommandHandler::class)
                ->setMethods(['fetchIApplyObject', 'fetchCrew'])
                ->getMock();

        //初始化
        $id = 1;
        $news = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $crew = $news->getCrew();
        $command = new ApproveApplyFormCommand($crew->getId(), $id);

        $stub->expects($this->once())->method('fetchCrew')->with($crew->getId())->willReturn($crew);
        //预言
        $news = $this->prophesize(UnAuditedNews::class);
        $news->setApplyCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $news->approve()->shouldBeCalledTimes(1)->willReturn(true);

        //揭示
        $stub->expects($this->exactly(1))->method('fetchIApplyObject')->willReturn($news->reveal());

        //验证
        $result = $stub->executeAction($command);
        $this->assertTrue($result);
    }
}
