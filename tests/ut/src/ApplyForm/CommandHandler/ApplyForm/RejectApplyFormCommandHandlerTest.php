<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ApplyForm\Command\ApplyForm\RejectApplyFormCommand;

use Base\News\Model\UnAuditedNews;

class RejectApplyFormCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockRejectApplyFormCommandHandler::class)
                    ->setMethods(['fetchApplyForm'])
                    ->getMock();
    }

    public function testExtendsRejectCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Common\CommandHandler\RejectCommandHandler',
            $this->stub
        );
    }

    public function testFetchIApplyObject()
    {
        //初始化
        $id = 1;
        $news = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);

        $this->stub->expects($this->once())->method('fetchApplyForm')->with($id)->willReturn($news);

        //验证
        $result = $this->stub->fetchIApplyObject($id);
        $this->assertEquals($result, $news);
    }

    public function testExecuteAction()
    {
        $stub = $this->getMockBuilder(MockRejectApplyFormCommandHandler::class)
                ->setMethods(['fetchIApplyObject', 'fetchCrew'])
                ->getMock();

        //初始化
        $id = 1;
        $news = \Base\News\Utils\MockFactory::generateUnAuditedNews($id);
        $crew = $news->getCrew();
        $command = new RejectApplyFormCommand(
            $crew->getId(),
            $news->getRejectReason(),
            $id
        );

        $stub->expects($this->once())->method('fetchCrew')->with($crew->getId())->willReturn($crew);

        //预言
        $news = $this->prophesize(UnAuditedNews::class);
        $news->setApplyCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $news->setRejectReason(Argument::exact($command->rejectReason))->shouldBeCalledTimes(1);
        $news->reject()->shouldBeCalledTimes(1)->willReturn(true);

        //揭示
        $stub->expects($this->exactly(1))->method('fetchIApplyObject')->willReturn($news->reveal());

        //验证
        $result = $stub->executeAction($command);
        $this->assertTrue($result);
    }
}
