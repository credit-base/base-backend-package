<?php
namespace Base\ApplyForm\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\ApplyForm\Repository\ApplyFormRepository;
use Base\ApplyForm\Command\ApplyForm\RejectApplyFormCommand;
use Base\ApplyForm\Command\ApplyForm\ApproveApplyFormCommand;

use Base\Common\WidgetRule\CommonWidgetRule;

class ApplyFormControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockApplyFormControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetApplyFormRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->trait->getApplyFormRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    private function initialApprove($result)
    {
        $this->approveStub = $this->getMockBuilder(MockApplyFormControllerTrait::class)
                    ->setMethods([
                        'getRequest',
                        'validateCommonScenario',
                        'getCommandBus',
                        'getApplyFormRepository',
                        'displaySuccess',
                        'displayError'
                    ])->getMock();
                    
        //初始化
        $id = $applyCrewId = 2;
        $command = new ApproveApplyFormCommand($applyCrewId, $id);

        $data = array(
            'relationships' => array(
                "applyCrew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$applyCrewId)
                    )
                )
            ),
        );

        $this->approveStub->expects($this->exactly(1))->method('validateCommonScenario')->willReturn(true);
        //预言
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);
        //揭示
        $this->approveStub->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        //预言
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        //揭示
        $this->approveStub->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testApproveFailure()
    {
        $command = $this->initialApprove(false);

        $this->approveStub->expects($this->exactly(1))->method('displayError');

        //验证
        $result = $this->approveStub->approve($command->id);
        $this->assertFalse($result);
    }

    public function testApproveSuccess()
    {
        $command = $this->initialApprove(true);
        
        $news = \Base\News\Utils\MockFactory::generateUnAuditedNews($command->id);

        //预言
        $repository = $this->prophesize(ApplyFormRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($news);

        //揭示
        $this->approveStub->expects($this->exactly(1))->method(
            'getApplyFormRepository'
        )->willReturn($repository->reveal());
             
        $this->approveStub->expects($this->exactly(1))->method('displaySuccess');

        //验证
        $result = $this->approveStub->approve($command->id);
        $this->assertTrue($result);
    }

    private function initialReject($result)
    {
        $this->rejectStub = $this->getMockBuilder(MockApplyFormControllerTrait::class)
                    ->setMethods([
                        'getRequest',
                        'validateCommonScenario',
                        'validateRejectScenario',
                        'getCommandBus',
                        'getApplyFormRepository',
                        'displaySuccess',
                        'displayError'
                    ])->getMock();
                    
        //初始化
        $id = $applyCrewId = 2;
        $rejectReason = 'rejectReason';

        $data = array(
            'attributes' => array(
                'rejectReason' => $rejectReason
            ),
            'relationships' => array(
                "applyCrew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$applyCrewId)
                    )
                )
            ),
        );
        $command = new RejectApplyFormCommand($applyCrewId, $rejectReason, $id);

        $this->rejectStub->expects($this->exactly(1))->method('validateCommonScenario')->willReturn(true);
        $this->rejectStub->expects($this->exactly(1))->method('validateRejectScenario')->willReturn(true);

        //预言
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);

        //揭示
        $this->rejectStub->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        //预言
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        //揭示
        $this->rejectStub->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testRejectFailure()
    {
        $command = $this->initialReject(false);

        $this->rejectStub->expects($this->exactly(1))->method('displayError');

        //验证
        $result = $this->rejectStub->reject($command->id);
        $this->assertFalse($result);
    }

    public function testRejectSuccess()
    {
        $command = $this->initialReject(true);

        $unAuditedNews = \Base\News\Utils\MockFactory::generateUnAuditedNews($command->id);

        //预言
        $repository = $this->prophesize(ApplyFormRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($unAuditedNews);

        $this->rejectStub->expects($this->exactly(1))->method(
            'getApplyFormRepository'
        )->willReturn($repository->reveal());
             
        $this->rejectStub->expects($this->exactly(1))->method('displaySuccess');

        //验证
        $result = $this->rejectStub->reject($command->id);
        $this->assertTrue($result);
    }

    public function testValidateCommonScenario()
    {
        $controller = $this->getMockBuilder(MockApplyFormControllerTrait::class)
                    ->setMethods(['getCommonWidgetRule']) ->getMock();

        $applyCrewId = 1;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(
            Argument::exact($applyCrewId),
            Argument::exact('applyCrewId')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateCommonScenarioPublic($applyCrewId);
        
        $this->assertTrue($result);
    }

    public function testValidateRejectScenario()
    {
        $controller = $this->getMockBuilder(MockApplyFormControllerTrait::class)
                    ->setMethods(['getCommonWidgetRule']) ->getMock();

        $rejectReason = 'rejectReason';

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->description(
            Argument::exact($rejectReason),
            Argument::exact('rejectReason')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateRejectScenarioPublic($rejectReason);
        
        $this->assertTrue($result);
    }
}
