<?php
namespace Base\ApplyForm\Model;

use PHPUnit\Framework\TestCase;

class ApplyOperationAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockApplyOperationAbleTrait::class)
                            ->setMethods(['apply'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testAdd()
    {
        $this->trait->expects($this->exactly(1))->method('apply')->willReturn(true);

        $result = $this->trait->add();
        $this->assertTrue($result);
    }

    public function testEdit()
    {
        $this->trait->expects($this->exactly(1))->method('apply')->willReturn(true);

        $result = $this->trait->edit();
        $this->assertTrue($result);
    }
}
