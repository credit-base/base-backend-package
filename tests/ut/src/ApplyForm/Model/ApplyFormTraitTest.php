<?php
namespace Base\ApplyForm\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Common\Model\IApproveAble;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ApplyFormTraitTest extends TestCase
{
    private $applyForm;

    public function setUp()
    {
        $this->applyForm = new MockApplyFormTrait();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->applyForm);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->applyForm->publicApplyFormRepository()
        );
    }

    public function testSetApplyId()
    {
        $this->applyForm->setApplyId(1);
        $this->assertEquals(1, $this->applyForm->getApplyId());
    }

    //applyTitle 测试 ------------------------------------------------------- start
    /**
     * 设置 News setApplyTitle() 正确的传参类型,期望传值正确
     */
    public function testSetApplyTitleCorrectType()
    {
        $this->applyForm->setApplyTitle('string');
        $this->assertEquals('string', $this->applyForm->getApplyTitle());
    }

    /**
     * 设置 News setApplyTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyTitleWrongType()
    {
        $this->applyForm->setApplyTitle(array(1, 2, 3));
    }
    //applyTitle 测试 -------------------------------------------------------   end
    
    //relation 测试 --------------------------------------------- start
    /**
     * 设置 News setRelation() 正确的传参类型,期望传值正确
     */
    public function testSetRelationCorrectType()
    {
        $object = new Crew();
        $this->applyForm->setRelation($object);
        $this->assertSame($object, $this->applyForm->getRelation());
    }
    //relation 测试 ---------------------------------------------   end

    //operationType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 News setOperationType() 是否符合预定范围
     *
     * @dataProvider operationTypeProvider
     */
    public function testSetOperationType($actual, $expected)
    {
        $this->applyForm->setOperationType($actual);
        $this->assertEquals($expected, $this->applyForm->getOperationType());
    }

    /**
     * 循环测试 News setOperationType() 数据构建器
     */
    public function operationTypeProvider()
    {
        return array(
            array(IApproveAble::OPERATION_TYPE['ADD'],IApproveAble::OPERATION_TYPE['ADD']),
            array(IApproveAble::OPERATION_TYPE['EDIT'],IApproveAble::OPERATION_TYPE['EDIT']),
            array(IApproveAble::OPERATION_TYPE['ENABLE'],IApproveAble::OPERATION_TYPE['ENABLE']),
            array(IApproveAble::OPERATION_TYPE['DISABLE'],IApproveAble::OPERATION_TYPE['DISABLE']),
            array(IApproveAble::OPERATION_TYPE['TOP'],IApproveAble::OPERATION_TYPE['TOP']),
            array(IApproveAble::OPERATION_TYPE['CANCEL_TOP'],IApproveAble::OPERATION_TYPE['CANCEL_TOP']),
            array(IApproveAble::OPERATION_TYPE['MOVE'],IApproveAble::OPERATION_TYPE['MOVE']),
            array(999,IApproveAble::OPERATION_TYPE['NULL']),
        );
    }

    /**
     * 设置 News setOperationType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperationTypeWrongType()
    {
        $this->applyForm->setOperationType('string');
    }
    //operationType 测试 ------------------------------------------------------   end

    //applyInfoCategory 测试 --------------------------------------------- start
    /**
     * 设置 News setApplyInfoCategory() 正确的传参类型,期望传值正确
     */
    public function testSetApplyInfoCategoryCorrectType()
    {
        $object = new ApplyInfoCategory();
        $this->applyForm->setApplyInfoCategory($object);
        $this->assertSame($object, $this->applyForm->getApplyInfoCategory());
    }

    /**
     * 设置 News setApplyInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyInfoCategoryWrongType()
    {
        $this->applyForm->setApplyInfoCategory('string');
    }
    //applyInfoCategory 测试 ---------------------------------------------   end

    //applyInfo 测试 ------------------------------------------------------- start
    /**
     * 设置 News setApplyInfo() 正确的传参类型,期望传值正确
     */
    public function testSetApplyInfoCorrectType()
    {
        $this->applyForm->setApplyInfo(array('applyInfo'));
        $this->assertEquals(array('applyInfo'), $this->applyForm->getApplyInfo());
    }

    /**
     * 设置 News setApplyInfo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyInfoWrongType()
    {
        $this->applyForm->setApplyInfo('applyInfo');
    }
    //applyInfo 测试 -------------------------------------------------------   end
    
    //applyCrew 测试 --------------------------------------------- start
    /**
     * 设置 News setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $object = new Crew();
        $this->applyForm->setApplyCrew($object);
        $this->assertSame($object, $this->applyForm->getApplyCrew());
    }

    /**
     * 设置 News setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->applyForm->setApplyCrew('string');
    }
    //applyCrew 测试 ---------------------------------------------   end

    //applyUserGroup 测试 --------------------------------------------- start
    /**
     * 设置 News setApplyUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetApplyUserGroupCorrectType()
    {
        $object = new UserGroup();
        $this->applyForm->setApplyUserGroup($object);
        $this->assertSame($object, $this->applyForm->getApplyUserGroup());
    }

    /**
     * 设置 News setApplyUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyUserGroupWrongType()
    {
        $this->applyForm->setApplyUserGroup('string');
    }
    //applyUserGroup 测试 ---------------------------------------------   end

    public function testApply()
    {
        //初始化
        $applyForm = $this->getMockBuilder(MockApplyFormTrait::class)
                           ->setMethods(['getApplyFormRepository'])
                           ->getMock();
        
        //预言 INewsAdapter
        $repository = $this->prophesize(ApplyFormRepository::class);
        $repository->add(Argument::exact($applyForm))->shouldBeCalledTimes(1)->willReturn(true);

        //揭示
        $applyForm->expects($this->once())
             ->method('getApplyFormRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $applyForm->add();
        $this->assertTrue($result);
    }

    public function testResubmitAction()
    {
        //初始化
        $applyForm = $this->getMockBuilder(MockApplyFormTrait::class)
                           ->setMethods(['setUpdateTime', 'getApplyFormRepository', 'setStatusTime'])
                           ->getMock();

        //预言修改updateTime
        $applyForm->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));
        $applyForm->expects($this->exactly(1))->method('setStatusTime')->with(Core::$container->get('time'));

        //预言 INewsAdapter
        $repository = $this->prophesize(ApplyFormRepository::class);
        $repository->edit(
            Argument::exact($applyForm),
            Argument::exact(
                [
                    'applyTitle',
                    'relation',
                    'applyInfo',
                    'applyStatus',
                    'applyInfoType',
                    'updateTime',
                    'statusTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $applyForm->expects($this->once())->method('getApplyFormRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $applyForm->resubmitAction();
        $this->assertTrue($result);
    }

    public function testApproveAction()
    {
        //初始化
        $applyForm = $this->getMockBuilder(MockApplyFormTrait::class)
                           ->setMethods(['updateApplyStatus', 'approveApplyInfo', 'getOperationType'])
                           ->getMock();

        $operationType = IApproveAble::OPERATION_TYPE['ADD'];

        $applyForm->expects($this->exactly(1))->method('getOperationType')->willReturn($operationType);
        $applyForm->expects($this->exactly(1))->method(
            'updateApplyStatus'
        )->with(IApproveAble::APPLY_STATUS['APPROVE'])->willReturn(true);
        $applyForm->expects($this->exactly(1))->method('approveApplyInfo')->with($operationType)->willReturn(true);
        
        //验证
        $result = $applyForm->approveAction();
        $this->assertTrue($result);
    }

    public function testRejectAction()
    {
        //初始化
        $applyForm = $this->getMockBuilder(MockApplyFormTrait::class)
                           ->setMethods(['updateApplyStatus'])
                           ->getMock();

        $applyForm->expects($this->exactly(1))
             ->method('updateApplyStatus')
             ->with(IApproveAble::APPLY_STATUS['REJECT'])
             ->willReturn(true);

        //验证
        $result = $applyForm->rejectAction();
        $this->assertTrue($result);
    }

    public function testUpdateApplyStatus()
    {
        $applyForm = $this->getMockBuilder(MockApplyFormTrait::class)
            ->setMethods(['getApplyFormRepository', 'setUpdateTime', 'setStatusTime'])
            ->getMock();
            
        $applyStatus = IApproveAble::APPLY_STATUS['PENDING'];

        $applyForm->setApplyStatus($applyStatus);
        $applyForm->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));
        $applyForm->expects($this->exactly(1))->method('setStatusTime')->with(Core::$container->get('time'));

        $repository = $this->prophesize(ApplyFormRepository::class);
        $repository->edit(
            Argument::exact($applyForm),
            Argument::exact(array(
                'updateTime','applyStatus','statusTime', 'applyCrew', 'rejectReason'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $applyForm->expects($this->any())->method('getApplyFormRepository')->willReturn($repository->reveal());

        //验证
        $result = $applyForm->publicUpdateApplyStatus($applyStatus);
        $this->assertTrue($result);
    }

    public function testUpdateApplyStatusFail()
    {
        $applyStatus = IApproveAble::APPLY_STATUS['REJECT'];
        $this->applyForm->setApplyStatus($applyStatus);

        $result = $this->applyForm->publicUpdateApplyStatus($applyStatus);
        
        //验证
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }
}
