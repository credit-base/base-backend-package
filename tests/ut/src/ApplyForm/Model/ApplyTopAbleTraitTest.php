<?php
namespace Base\ApplyForm\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Common\Model\ITopAble;

class ApplyTopAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockApplyTopAbleTrait::class)
                            ->setMethods(['apply'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testTopSuccess()
    {
        $this->trait->setStick(ITopAble::STICK['DISABLED']);
        $this->trait->expects($this->exactly(1))->method('apply')->willReturn(true);

        $result = $this->trait->top();
        $this->assertTrue($result);
    }

    public function testTopFail()
    {
        $this->trait->setStick(ITopAble::STICK['ENABLED']);

        $result = $this->trait->top();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }

    public function testCancelTopSuccess()
    {
        $this->trait->setStick(ITopAble::STICK['ENABLED']);

        $this->trait->expects($this->exactly(1))->method('apply')->willReturn(true);

        $result = $this->trait->cancelTop();
        $this->assertTrue($result);
    }

    public function testCancelTopFail()
    {
        $this->trait->setStick(ITopAble::STICK['DISABLED']);

        $result = $this->trait->cancelTop();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }
}
