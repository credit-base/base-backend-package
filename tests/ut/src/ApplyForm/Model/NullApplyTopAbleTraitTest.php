<?php
namespace Base\ApplyForm\Model;

use PHPUnit\Framework\TestCase;

class NullApplyTopAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullApplyTopAbleTrait::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testTop()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->top();
        $this->assertFalse($result);
    }

    public function testCancelTop()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->cancelTop();
        $this->assertFalse($result);
    }
    
    private function mockResourceNotExist()
    {
        $this->trait->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);
    }
}
