<?php
namespace Base\ApplyForm\Model;

use PHPUnit\Framework\TestCase;

class ApplyInfoCategoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new ApplyInfoCategory(
            1,
            2
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCategory()
    {
        $this->assertEquals(1, $this->stub->getCategory());
    }
    
    public function testGetType()
    {
        $this->assertEquals(2, $this->stub->getType());
    }
}
