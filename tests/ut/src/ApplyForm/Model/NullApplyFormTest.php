<?php
namespace Base\ApplyForm\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullApplyFormTest extends TestCase
{
    private $applyForm;

    public function setUp()
    {
        $this->applyForm = $this->getMockBuilder(MockNullApplyForm::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->applyForm);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->applyForm);
    }

    public function testImplementsIApplyFormAble()
    {
        $this->assertInstanceof('Base\ApplyForm\Model\IApplyFormAble', $this->applyForm);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->applyForm);
    }

    public function testSetId()
    {
        $this->mockResourceNotExist();
        
        $result = $this->applyForm->setId(1);
        $this->assertFalse($result);
    }
    
    public function testGetId()
    {
        $this->mockResourceNotExist();
        
        $result = $this->applyForm->getId();
        $this->assertFalse($result);
    }

    public function testSetStatus()
    {
        $this->mockResourceNotExist();
        
        $result = $this->applyForm->setStatus(1);
        $this->assertFalse($result);
    }

    private function mockResourceNotExist()
    {
        $this->applyForm->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }

    public function testResourceNotExist()
    {
        $mockNullApplyForm = new MockNullApplyForm();
        $result = $mockNullApplyForm->resourceNotExist();

        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }
}
