<?php
namespace Base\ApplyForm\Model;

use PHPUnit\Framework\TestCase;

class NullApplyEnableAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullApplyEnableAbleTrait::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testEnable()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->enable();
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->disable();
        $this->assertFalse($result);
    }
    
    private function mockResourceNotExist()
    {
        $this->trait->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);
    }
}
