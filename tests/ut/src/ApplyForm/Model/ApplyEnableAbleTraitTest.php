<?php
namespace Base\ApplyForm\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Common\Model\IEnableAble;

class ApplyEnableAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockApplyEnableAbleTrait::class)
                            ->setMethods(['apply'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testEnableSuccess()
    {
        $this->trait->setStatus(IEnableAble::STATUS['DISABLED']);

        $this->trait->expects($this->exactly(1))->method('apply')->willReturn(true);

        //验证
        $result = $this->trait->enable();
        $this->assertTrue($result);
    }

    public function testEnableFail()
    {
        $this->trait->setStatus(IEnableAble::STATUS['ENABLED']);

        //验证
        $result = $this->trait->enable();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $this->trait->setStatus(IEnableAble::STATUS['ENABLED']);

        $this->trait->expects($this->exactly(1))->method('apply')->willReturn(true);

        //验证
        $result = $this->trait->disable();
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $this->trait->setStatus(IEnableAble::STATUS['DISABLED']);

        //验证
        $result = $this->trait->disable();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }
}
