<?php
namespace Base\ApplyForm\Command\ApplyForm;

use PHPUnit\Framework\TestCase;

use Base\Common\Model\IApproveAble;

class EditApplyFormCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'crew' => $faker->randomDigit(1),
            'id' => $faker->randomDigit(),
            'operationType' => IApproveAble::OPERATION_TYPE['EDIT']
        );

        $this->command = new EditApplyFormCommand(
            $this->fakerData['crew'],
            $this->fakerData['id'],
            $this->fakerData['operationType']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }

    public function testOperationTypeParameter()
    {
        $this->assertEquals($this->fakerData['operationType'], $this->command->operationType);
    }
}
