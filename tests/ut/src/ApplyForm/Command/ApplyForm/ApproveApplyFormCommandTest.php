<?php
namespace Base\ApplyForm\Command\ApplyForm;

use PHPUnit\Framework\TestCase;

class ApproveApplyFormCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'applyCrew' => $faker->randomDigit(1),
            'id' => $faker->randomDigit()
        );

        $this->command = new ApproveApplyFormCommand(
            $this->fakerData['applyCrew'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testApplyCrewParameter()
    {
        $this->assertEquals($this->fakerData['applyCrew'], $this->command->applyCrew);
    }
}
