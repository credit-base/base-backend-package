<?php
namespace ApplyForm\Utils;

use ApplyForm\Model\ApplyForm;

class ApplyFormMockFactory
{
    public static function generateApplyForm(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ApplyForm {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $applyForm = new ApplyForm($id);
        $applyForm->setId($id);

        self::generateStatus($applyForm, $faker, $value);
        
        $applyForm->setCreateTime($faker->unixTime());
        $applyForm->setUpdateTime($faker->unixTime());
        $applyForm->setStatusTime($faker->unixTime());

        return $applyForm;
    }

    private static function generateStatus($applyForm, $faker, $value) : void
    {
        unset($faker);
        $status = isset($value['status']) ? $value['status'] : 0;
        
        $applyForm->setStatus($status);
    }
}
