<?php
namespace ApplyForm\Utils;

trait ApplyFormUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $applyForm
    ) {
        $this->assertEquals($expectedArray['apply_form_id'], $applyForm->getId());

        $this->assertEquals($expectedArray['status'], $applyForm->getStatus());
        $this->assertEquals($expectedArray['create_time'], $applyForm->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $applyForm->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $applyForm->getStatusTime());
    }
}
