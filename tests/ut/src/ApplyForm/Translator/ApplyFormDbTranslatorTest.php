<?php
namespace Base\ApplyForm\Translator;

use PHPUnit\Framework\TestCase;

class ApplyFormDbTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new ApplyFormDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }
}
