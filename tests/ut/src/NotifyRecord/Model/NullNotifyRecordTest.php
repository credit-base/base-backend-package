<?php
namespace Base\NotifyRecord\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullNotifyRecordTest extends TestCase
{
    private $notifyRecord;

    public function setUp()
    {
        $this->notifyRecord = $this->getMockBuilder(NullNotifyRecord::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->notifyRecord);
    }

    public function testExtendsNotifyRecord()
    {
        $this->assertInstanceOf(
            'Base\NotifyRecord\Model\NotifyRecord',
            $this->notifyRecord
        );
    }
}
