<?php
namespace Base\NotifyRecord\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NotifyRecordTest extends TestCase
{
    private $record;

    public function setUp()
    {
        $this->record = new NotifyRecord();
    }

    public function tearDown()
    {
        unset($this->record);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("Marmot\Common\Model\IObject", $this->record);
    }

    /**
     * 测试构造函数
     */
    public function testConstructor()
    {
        $this->assertEquals(0, $this->record->getId());

        $this->assertEquals('', $this->record->getName());

        $this->assertEquals(0, $this->record->getStatus());

        //测试初始化创建时间
        $this->assertEquals(Core::$container->get('time'), $this->record->getCreateTime());

        //测试初始化更新时间
        $this->assertEquals(Core::$container->get('time'), $this->record->getUpdateTime());

        //测试初始化状态更新时间
        $this->assertEquals(0, $this->record->getStatusTime());
    }

    //name -- 开始
    /**
     * 设置 NotifyRecord setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->record->setName('name');
        $this->assertEquals('name', $this->record->getName());
    }
    /**
     * 设置 NotifyRecord setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->record->setName(array(1, 2, 3));
    }
    //name -- 结束

    //hash -- 开始
    /**
     * 设置 NotifyRecord setHash() 正确的传参类型,期望传值正确
     */
    public function testSetHashCorrectType()
    {
        $this->record->setHash('hash');
        $this->assertEquals('hash', $this->record->getHash());
    }
    /**
     * 设置 NotifyRecord setHash() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHashWrongType()
    {
        $this->record->setHash(array(1, 2, 3));
    }
    //hash -- 结束

    //status -- 开始
    /**
     * 循环测试 NotifyRecord setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->record->setStatus($actual);
        $this->assertEquals($expected, $this->record->getStatus());
    }
    /**
     * 循环测试 NotifyRecord setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(NotifyRecord::STATUS['PENDING'],NotifyRecord::STATUS['PENDING']),
            array(NotifyRecord::STATUS['SUCCESS'],NotifyRecord::STATUS['SUCCESS']),
            array(999,NotifyRecord::STATUS['PENDING']),
        );
    }
    /**
     * 设置 NotifyRecord setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->record->setStatus('string');
    }
    //status -- 结束
}
