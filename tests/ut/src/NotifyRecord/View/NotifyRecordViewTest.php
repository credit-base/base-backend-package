<?php
namespace Base\NotifyRecord\View;

use PHPUnit\Framework\TestCase;

use Base\NotifyRecord\Model\NotifyRecord;

class NotifyRecordViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $notifyRecord = new NotifyRecordView(new NotifyRecord());
        $this->assertInstanceof('Base\Common\View\CommonView', $notifyRecord);
    }
}
