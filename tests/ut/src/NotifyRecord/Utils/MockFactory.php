<?php
namespace Base\NotifyRecord\Utils;

use Base\NotifyRecord\Model\NotifyRecord;

class MockFactory
{
    public static function generateNotifyRecord(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : NotifyRecord {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $notifyRecord = new NotifyRecord($id);
        $notifyRecord->setId($id);

        //parentId
        self::generateName($notifyRecord, $faker, $value);
        //statues
        self::generateNotifyRecordStatus($notifyRecord, $faker, $value);

        //status
        $notifyRecord->setCreateTime($faker->unixTime());
        $notifyRecord->setUpdateTime($faker->unixTime());
        $notifyRecord->setStatusTime($faker->unixTime());

        return $notifyRecord;
    }

    private static function generateName($task, $faker, $value) : void
    {
        unset($faker);
        $name = isset($value['name']) ?
            $value['name'] :
            'XZXK_10_1_hash.xlsx';
        
        $task->setName($name);
    }

    private static function generateNotifyRecordStatus($task, $faker, $value) : void
    {
        $category = isset($value['status']) ? $value['status'] : $faker->randomElement(NotifyRecord::STATUS);
        $task->setStatus($category);
    }
}
