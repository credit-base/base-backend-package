<?php
namespace Base\NotifyRecord\Utils;

trait NotifyRecordSdkUtils
{
    private function compareValueObjectAndObject(
        $notifyRecordSdk,
        $notifyRecord
    ) {
        $this->assertEquals($notifyRecordSdk->getId(), $notifyRecord->getId());
        $this->assertEquals($notifyRecordSdk->getName(), $notifyRecord->getName());
        $this->assertEquals($notifyRecordSdk->getHash(), $notifyRecord->getHash());
        $this->assertEquals($notifyRecordSdk->getStatus(), $notifyRecord->getStatus());
        $this->assertEquals($notifyRecordSdk->getCreateTime(), $notifyRecord->getCreateTime());
        $this->assertEquals($notifyRecordSdk->getStatusTime(), $notifyRecord->getStatusTime());
        $this->assertEquals($notifyRecordSdk->getUpdateTime(), $notifyRecord->getUpdateTime());
    }
}
