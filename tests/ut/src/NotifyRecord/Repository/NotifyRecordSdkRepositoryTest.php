<?php
namespace Base\NotifyRecord\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter;
use Base\NotifyRecord\Adapter\NotifyRecord\NotifyRecordSdkAdapter;

class NotifyRecordSdkRepositoryTest extends TestCase
{
    private $repository;

    private $mockRepository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(NotifyRecordSdkRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();

        $this->mockRepository = new MockNotifyRecordRepository();
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->mockRepository);
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsINotifyRecordAdapter()
    {
        $this->assertInstanceOf(
            'Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new NotifyRecordSdkAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter',
            $this->repository->getActualAdapter()
        );

        $this->assertInstanceOf(
            'Base\NotifyRecord\Adapter\NotifyRecord\NotifyRecordSdkAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter',
            $this->repository->getMockAdapter()
        );

        $this->assertInstanceOf(
            'Base\NotifyRecord\Adapter\NotifyRecord\NotifyRecordMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(INotifyRecordAdapter::class);
        $adapter->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(INotifyRecordAdapter::class);
        $adapter->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 20;

        $adapter = $this->prophesize(INotifyRecordAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($number),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $number, $size);
    }
}
