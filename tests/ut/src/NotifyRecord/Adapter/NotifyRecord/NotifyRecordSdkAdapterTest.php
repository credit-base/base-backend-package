<?php
namespace Base\NotifyRecord\Adapter\NotifyRecord;

use PHPUnit\Framework\TestCase;

use Base\NotifyRecord\Model\NotifyRecord;

class NotifyRecordSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockNotifyRecordSdkAdapter::class)
                           ->setMethods(['fetchOneAction', 'fetchListAction', 'filterAction'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsINotifyRecordAdapter()
    {
        $adapter = new NotifyRecordSdkAdapter();

        $this->assertInstanceOf(
            'Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\NotifyRecord\Repository\NotifyRecordRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\NotifyRecord\Model\NullNotifyRecord',
            $this->adapter->getNullObject()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockNotifyRecordSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedNotifyRecord = new NotifyRecord();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedNotifyRecord);

        //验证
        $neotifyRecord = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedNotifyRecord, $neotifyRecord);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $neotifyRecordOne = new NotifyRecord(1);
        $neotifyRecordTwo = new NotifyRecord(2);

        $ids = [1, 2];

        $expectedNotifyRecordList = [];
        $expectedNotifyRecordList[$neotifyRecordOne->getId()] = $neotifyRecordOne;
        $expectedNotifyRecordList[$neotifyRecordTwo->getId()] = $neotifyRecordTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedNotifyRecordList);

        //验证
        $neotifyRecordList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedNotifyRecordList, $neotifyRecordList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $neotifyRecordOne = new NotifyRecord(1);
        $neotifyRecordTwo = new NotifyRecord(2);

        $expectedNotifyRecordList = [];
        $expectedNotifyRecordList[$neotifyRecordOne->getId()] = $neotifyRecordOne;
        $expectedNotifyRecordList[$neotifyRecordTwo->getId()] = $neotifyRecordTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedNotifyRecordList, 2]);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedNotifyRecordList, 2], $result);
    }
}
