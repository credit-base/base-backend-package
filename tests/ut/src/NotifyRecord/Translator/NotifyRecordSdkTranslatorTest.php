<?php
namespace Base\NotifyRecord\Translator;

use PHPUnit\Framework\TestCase;

use Base\NotifyRecord\Model\NullNotifyRecord;
use Base\NotifyRecord\Utils\NotifyRecordSdkUtils;

use BaseSdk\NotifyRecord\Model\NotifyRecord as NotifyRecordSdk;
use BaseSdk\NotifyRecord\Model\NullNotifyRecord as NullNotifyRecordSdk;

class NotifyRecordSdkTranslatorTest extends TestCase
{
    use NotifyRecordSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new NotifyRecordSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $notifyRecord = \Base\NotifyRecord\Utils\MockFactory::generateNotifyRecord(1);

        $notifyRecordSdk = new NotifyRecordSdk(
            $notifyRecord->getId(),
            $notifyRecord->getName(),
            $notifyRecord->getHash(),
            $notifyRecord->getStatus(),
            $notifyRecord->getStatusTime(),
            $notifyRecord->getCreateTime(),
            $notifyRecord->getUpdateTime()
        );

        $notifyRecordObject = $this->translator->valueObjectToObject($notifyRecordSdk);
        $this->assertInstanceof('Base\NotifyRecord\Model\NotifyRecord', $notifyRecordObject);
        $this->compareValueObjectAndObject($notifyRecordSdk, $notifyRecordObject);
    }

    public function testValueObjectToObjectFail()
    {
        $notifyRecordSdk = NullNotifyRecordSdk::getInstance();

        $notifyRecord = $this->translator->valueObjectToObject($notifyRecordSdk);
        $this->assertInstanceof('Base\NotifyRecord\Model\NullNotifyRecord', $notifyRecord);
    }

    public function testObjectToValueObject()
    {
        $notifyRecordSdk = NullNotifyRecordSdk::getInstance();

        $result = $this->translator->objectToValueObject($notifyRecordSdk);

        $this->assertEquals(
            $notifyRecordSdk,
            $result
        );
    }
}
