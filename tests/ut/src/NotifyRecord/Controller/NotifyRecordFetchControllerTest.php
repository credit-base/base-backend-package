<?php
namespace Base\NotifyRecord\Controller;

use PHPUnit\Framework\TestCase;

class NotifyRecordFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(NotifyRecordFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockNotifyRecordFetchController();

        $this->assertInstanceOf(
            'Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter',
            $controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\NotifyRecord\Repository\NotifyRecordSdkRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockNotifyRecordFetchController();

        $this->assertInstanceOf(
            'Base\NotifyRecord\View\NotifyRecordView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockNotifyRecordFetchController();

        $this->assertEquals(
            'notifyRecords',
            $controller->getResourceName()
        );
    }
}
