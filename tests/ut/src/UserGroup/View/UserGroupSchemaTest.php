<?php
namespace Base\UserGroup\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class UserGroupSchemaTest extends TestCase
{
    private $userGroupSchema;

    private $userGroup;

    public function setUp()
    {
        $this->userGroupSchema = new UserGroupSchema(new Factory());

        $this->userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->userGroupSchema);
        unset($this->userGroup);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->userGroupSchema);
    }

    public function testGetId()
    {
        $result = $this->userGroupSchema->getId($this->userGroup);

        $this->assertEquals($result, $this->userGroup->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->userGroupSchema->getAttributes($this->userGroup);

        $this->assertEquals($result['name'], $this->userGroup->getName());
        $this->assertEquals($result['shortName'], $this->userGroup->getShortName());
        $this->assertEquals($result['status'], $this->userGroup->getStatus());
        $this->assertEquals($result['createTime'], $this->userGroup->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->userGroup->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->userGroup->getStatusTime());
    }
}
