<?php
namespace Base\UserGroup\View;

use PHPUnit\Framework\TestCase;

use Base\UserGroup\Model\UserGroup;

class UserGroupViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $userGroup = new UserGroupView(new UserGroup());
        $this->assertInstanceof('Base\Common\View\CommonView', $userGroup);
    }
}
