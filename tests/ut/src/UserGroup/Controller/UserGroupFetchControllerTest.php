<?php
namespace Base\UserGroup\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use Base\UserGroup\Model\NullUserGroup;
use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\View\UserGroupView;

class UserGroupFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(UserGroupFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockUserGroupFetchController();

        $this->assertInstanceOf(
            'Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockUserGroupFetchController();

        $this->assertInstanceOf(
            'Base\UserGroup\View\UserGroupView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockUserGroupFetchController();

        $this->assertEquals(
            'userGroups',
            $controller->getResourceName()
        );
    }
}
