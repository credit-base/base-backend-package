<?php
namespace Base\UserGroup\Adapter\UserGroup\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class UserGroupDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockUserGroupDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(UserGroupDb::TABLE, $this->database->getTable());
    }
}
