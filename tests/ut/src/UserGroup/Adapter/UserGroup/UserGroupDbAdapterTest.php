<?php
namespace Base\UserGroup\Adapter\UserGroup;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Model\NullUserGroup;
use Base\UserGroup\Translator\UserGroupDbTranslator;
use Base\UserGroup\Adapter\UserGroup\Query\UserGroupRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class UserGroupDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UserGroupDbAdapter::class)
                           ->setMethods(['fetchListAction', 'fetchOneAction'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    /**
     * 测试是否 实现 IUserGroupAdapter
     */
    public function testImplementsIUserGroupAdapter()
    {
        $this->assertInstanceOf(
            'Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 UserGroupDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockUserGroupDbAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Translator\UserGroupDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 UserGroupRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockUserGroupDbAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Adapter\UserGroup\Query\UserGroupRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockUserGroupDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }
    
    //fetchOne
    /**
     * 测试获取单条成功情况
     */
    public function testFetchOne()
    {
        //初始化
        $expectedUserGroup = new UserGroup();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedUserGroup);

        //验证
        $userGroup = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedUserGroup, $userGroup);
    }

    //fetchList
    /**
     * 测试获取多条成功情况
     */
    public function testFetchList()
    {
        //初始化
        $userGroupOne = new UserGroup(1);
        $userGroupTwo = new UserGroup(2);

        $ids = [1, 2];

        $expectedUserGroupList = [];
        $expectedUserGroupList[$userGroupOne->getId()] = $userGroupOne;
        $expectedUserGroupList[$userGroupTwo->getId()] = $userGroupTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedUserGroupList);

        //验证
        $userGroupList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedUserGroupList, $userGroupList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockUserGroupDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterName()
    {
        $filter = array(
            'name' => 1
        );
        $adapter = new MockUserGroupDbAdapter();

        $expectedCondition = 'name LIKE \'%'.$filter['name'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockUserGroupDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockUserGroupDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
