<?php
namespace Base\UserGroup\Utils;

trait UserGroupSdkUtils
{
    private function compareValueObjectAndObject(
        $userGroupSdk,
        $userGroup
    ) {
        $this->assertEquals($userGroupSdk->getId(), $userGroup->getId());
    }
}
