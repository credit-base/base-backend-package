<?php
namespace Base\UserGroup\Translator;

use PHPUnit\Framework\TestCase;

use Base\UserGroup\Model\NullUserGroup;
use Base\UserGroup\Utils\UserGroupSdkUtils;

use BaseSdk\UserGroup\Model\UserGroup as UserGroupSdk;
use BaseSdk\UserGroup\Model\NullUserGroup as NullUserGroupSdk;

class UserGroupSdkTranslatorTest extends TestCase
{
    use UserGroupSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UserGroupSdkTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);

        $userGroupSdk = new UserGroupSdk($userGroup->getId());

        $userGroupObject = $this->translator->valueObjectToObject($userGroupSdk);
        $this->assertInstanceof('Base\UserGroup\Model\UserGroup', $userGroupObject);
        $this->compareValueObjectAndObject($userGroupSdk, $userGroupObject);
    }

    public function testValueObjectToObjectFail()
    {
        $userGroupSdk = NullUserGroupSdk::getInstance();

        $userGroup = $this->translator->valueObjectToObject($userGroupSdk);
        $this->assertInstanceof('Base\UserGroup\Model\NullUserGroup', $userGroup);
    }

    public function testObjectToValueObjectFail()
    {
        $userGroupSdk = NullUserGroupSdk::getInstance();

        $result = $this->translator->objectToValueObject($userGroupSdk);

        $this->assertEquals(
            $userGroupSdk,
            $result
        );
    }

    public function testObjectToValueObject()
    {
        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);

        $userGroupSdk = $this->translator->objectToValueObject($userGroup);
        $this->assertInstanceof('BaseSdk\UserGroup\Model\UserGroup', $userGroupSdk);
        $this->compareValueObjectAndObject($userGroupSdk, $userGroup);
    }
}
