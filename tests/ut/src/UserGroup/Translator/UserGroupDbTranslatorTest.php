<?php
namespace Base\UserGroup\Translator;

use PHPUnit\Framework\TestCase;

use Base\UserGroup\Utils\UserGroupUtils;

class UserGroupDbTranslatorTest extends TestCase
{
    use UserGroupUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UserGroupDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\UserGroup\Model\NullUserGroup', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);

        $expression['user_group_id'] = $userGroup->getId();
        $expression['name'] = $userGroup->getName();
        $expression['short_name'] = $userGroup->getShortName();
        $expression['status'] = $userGroup->getStatus();
        $expression['status_time'] = $userGroup->getStatusTime();
        $expression['create_time'] = $userGroup->getCreateTime();
        $expression['update_time'] = $userGroup->getUpdateTime();

        $userGroup = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\UserGroup\Model\UserGroup', $userGroup);
        $this->compareArrayAndObject($expression, $userGroup);
    }

    public function testObjectToArray()
    {
        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);

        $expression = $this->translator->objectToArray($userGroup);

        $this->compareArrayAndObject($expression, $userGroup);
    }
}
