<?php
namespace Base\UserGroup\Model;

use PHPUnit\Framework\TestCase;

class NullUserGroupTest extends TestCase
{
    private $userGroup;

    public function setUp()
    {
        $this->userGroup = NullUserGroup::getInstance();
    }

    public function tearDown()
    {
        unset($this->userGroup);
    }

    public function testExtendsNews()
    {
        $this->assertInstanceof('Base\UserGroup\Model\UserGroup', $this->userGroup);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->userGroup);
    }
}
