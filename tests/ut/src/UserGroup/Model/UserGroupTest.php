<?php
namespace Base\UserGroup\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */

class UserGroupTest extends TestCase
{
    private $userGroup;

    public function setUp()
    {
        $this->userGroup = new UserGroup();
    }

    public function tearDown()
    {
        unset($this->userGroup);
    }
    
    /**
     * UserGroup 单位领域对象,测试构造函数
     */
    public function testConstructor()
    {
        //测试初始化单位id
        $this->assertEquals(0, $this->userGroup->getId());

        //测试初始化单位名称
        $this->assertEmpty($this->userGroup->getName());

        //测试初始化简称
        $this->assertEmpty($this->userGroup->getShortName());

        //测试初始化创建时间
        $this->assertEquals(Core::$container->get('time'), $this->userGroup->getCreateTime());

        //测试初始化状态
        $this->assertEquals(UserGroup::STATUS_NORMAL, $this->userGroup->getStatus());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 UserGroup setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->userGroup->setId(1);
        $this->assertEquals(1, $this->userGroup->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 UserGroup setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->userGroup->setName('string');
        $this->assertEquals('string', $this->userGroup->getName());
    }

    /**
     * 设置 UserGroup setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->userGroup->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //shortName 测试 -------------------------------------------------------- start
    /**
     * 设置 UserGroup setShortName() 正确的传参类型,期望传值正确
     */
    public function testSetShortNameCorrectType()
    {
        $this->userGroup->setShortName('string');
        $this->assertEquals('string', $this->userGroup->getShortName());
    }

    /**
     * 设置 UserGroup setShortName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetShortNameWrongType()
    {
        $this->userGroup->setShortName(array(1,2,3));
    }
    //shortName 测试 --------------------------------------------------------   end
}
