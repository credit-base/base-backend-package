<?php
namespace Base\Journal\Model;

use PHPUnit\Framework\TestCase;

use Base\ApplyForm\Model\IApplyFormAble;

class UnAuditedJournalTest extends TestCase
{
    private $journalStub;

    public function setUp()
    {
        $this->journalStub = new UnAuditedJournal();
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Base\ApplyForm\Model\IApplyFormAble', $this->journalStub);
    }

    public function testCorrectExtendsJournal()
    {
        $this->assertInstanceof('Base\Journal\Model\Journal', $this->journalStub);
    }

    public function testUnAuditedJournalConstructor()
    {
        $this->assertEquals(0, $this->journalStub->getApplyId());
        $this->assertEquals('', $this->journalStub->getApplyTitle());
        $this->assertEquals(UnAuditedJournal::OPERATION_TYPE['NULL'], $this->journalStub->getOperationType());
        $this->assertEquals(array(), $this->journalStub->getApplyInfo());
        $this->assertEquals(UnAuditedJournal::APPLY_STATUS['PENDING'], $this->journalStub->getApplyStatus());
        $this->assertEquals('', $this->journalStub->getRejectReason());
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->journalStub->getRelation()
        );
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->journalStub->getApplyCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->journalStub->getApplyUserGroup()
        );
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\ApplyInfoCategory',
            $this->journalStub->getApplyInfoCategory()
        );
    }
}
