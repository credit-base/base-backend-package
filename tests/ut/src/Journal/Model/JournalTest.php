<?php
namespace Base\Journal\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Model\IEnableAble;

use Base\Crew\Model\Crew;
use Base\Crew\Model\NullCrew;

use Base\UserGroup\Model\UserGroup;

use Base\Journal\Adapter\Journal\IJournalAdapter;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class JournalTest extends TestCase
{
    private $journal;

    public function setUp()
    {
        $this->journal = new MockJournal();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->journal);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->journal
        );
    }

    public function testImplementsIEnableAble()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IEnableAble',
            $this->journal
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IOperate',
            $this->journal
        );
    }

    public function testGetRepository()
    {
        $journal = new MockJournal();
        $this->assertInstanceOf('Base\Journal\Adapter\Journal\IJournalAdapter', $journal->getRepository());
    }

    public function testJournalConstructor()
    {
        $this->assertEquals(0, $this->journal->getId());
        $this->assertEmpty($this->journal->getTitle());
        $this->assertEmpty($this->journal->getSource());
        $this->assertEquals('', $this->journal->getDescription());
        $this->assertEquals(array(), $this->journal->getCover());
        $this->assertEquals(array(), $this->journal->getAttachment());
        $this->assertEquals(array(), $this->journal->getAuthImages());
        $this->assertEquals(0, $this->journal->getYear());
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->journal->getCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->journal->getPublishUserGroup()
        );
        $this->assertEquals(IEnableAble::STATUS['ENABLED'], $this->journal->getStatus());
        $this->assertEquals(Core::$container->get('time'), $this->journal->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $this->journal->getCreateTime());
        $this->assertEquals(0, $this->journal->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Journal setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->journal->setId(1);
        $this->assertEquals(1, $this->journal->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //title 测试 ------------------------------------------------------- start
    /**
     * 设置 Journal setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->journal->setTitle('string');
        $this->assertEquals('string', $this->journal->getTitle());
    }

    /**
     * 设置 Journal setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->journal->setTitle(array(1, 2, 3));
    }
    //title 测试 -------------------------------------------------------   end

    //source 测试 ------------------------------------------------------ start
    /**
     * 设置 Journal setSource() 正确的传参类型,期望传值正确
     */
    public function testSetSourceCorrectType()
    {
        $this->journal->setSource('string');
        $this->assertEquals('string', $this->journal->getSource());
    }

    /**
     * 设置 Journal setSource() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceWrongType()
    {
        $this->journal->setSource(array(1, 2, 3));
    }
    //source 测试 ------------------------------------------------------   end

    //description 测试 ----------------------------------------------------- start
    /**
     * 设置 Journal setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->journal->setDescription('string');
        $this->assertEquals('string', $this->journal->getDescription());
    }

    /**
     * 设置 Journal setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->journal->setDescription(array(1, 2, 3));
    }
    //description 测试 -----------------------------------------------------   end

    //cover 测试 ------------------------------------------------------ start
    public function testSetCoverCorrectType()
    {
        $this->journal->setCover(array('cover'));
        $this->assertEquals(array('cover'), $this->journal->getCover());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetCoverWrongType()
    {
        $this->journal->setCover('cover');
    }
    //cover 测试 ------------------------------------------------------   end

    //attachment 测试 ------------------------------------------------- start
    public function testSetAttachmentCorrectType()
    {
        $this->journal->setAttachment(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->journal->getAttachment());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetAttachmentWrongType()
    {
        $this->journal->setAttachment(1);
    }
    //attachment 测试 -------------------------------------------------   end

    //authImages 测试 ------------------------------------------------- start
    public function testSetAuthImagesCorrectType()
    {
        $this->journal->setAuthImages(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->journal->getAuthImages());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetAuthImagesWrongType()
    {
        $this->journal->setAuthImages(1);
    }
    //authImages 测试 -------------------------------------------------   end

    //year 测试 ------------------------------------------------- start
    public function testSetYearCorrectType()
    {
        $this->journal->setYear('2021');
        $this->assertEquals('2021', $this->journal->getYear());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetYearWrongType()
    {
        $this->journal->setYear(array('1'));
    }
    //year 测试 -------------------------------------------------   end

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 Journal setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->journal->setCrew($object);
        $this->assertSame($object, $this->journal->getCrew());
    }

    /**
     * 设置 Journal setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->journal->setCrew('string');
    }
    //crew 测试 ---------------------------------------------   end

    //publishUserGroup 测试 --------------------------------------------- start
    /**
     * 设置 Journal setPublishUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetPublishUserGroupCorrectType()
    {
        $object = new UserGroup();
        $this->journal->setPublishUserGroup($object);
        $this->assertSame($object, $this->journal->getPublishUserGroup());
    }

    /**
     * 设置 Journal setPublishUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPublishUserGroupWrongType()
    {
        $this->journal->setPublishUserGroup('string');
    }
    //publishUserGroup 测试 ---------------------------------------------   end

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IJournalAdapter 调用 add 并且传参 $journal 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $journal = $this->getMockBuilder(MockJournal::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $journal->setCrew($crew);

        //预言 IJournalAdapter
        $repository = $this->prophesize(IJournalAdapter::class);
        $repository->add(Argument::exact($journal))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $journal->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $journal->add();
        $this->assertTrue($result);
    }

    /**
     * 测试添加失败-发布人
     * 1. 期望返回false
     * 2. 设置委办局为NullCrew
     * 3. 调用add,期望返回false
     */
    public function testAddFailCrewNull()
    {
        $this->journal->setCrew(new NullCrew());

        $result = $this->journal->add();

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('crewId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望编辑
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IJournalAdapter 调用 edit 并且传参 $journal 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $journal = $this->getMockBuilder(MockJournal::class)
                           ->setMethods(['setUpdateTime', 'getRepository'])
                           ->getMock();

        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $journal->setCrew($crew);
                   
        //预言修改updateTime
        $journal->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IJournalAdapter
        $repository = $this->prophesize(IJournalAdapter::class);
        $repository->edit(
            Argument::exact($journal),
            Argument::exact(
                [
                    'title',
                    'source',
                    'description',
                    'cover',
                    'attachment',
                    'authImages',
                    'year',
                    'status',
                    'crew',
                    'publishUserGroup',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $journal->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $journal->edit();
        $this->assertTrue($result);
    }

    public function testEditFailCrewNull()
    {
        $this->journal->setCrew(new NullCrew());

        $result = $this->journal->edit();

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('crewId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testUpdateStatus()
    {
        $journal = $this->getMockBuilder(MockJournal::class)
            ->setMethods(['getRepository'])
            ->getMock();
            
        $status = IEnableAble::STATUS['ENABLED'];
        $journal->setStatus($status);
        $journal->setUpdateTime(Core::$container->get('time'));
        $journal->setStatusTime(Core::$container->get('time'));

        $repository = $this->prophesize(IJournalAdapter::class);

        $repository->edit(
            Argument::exact($journal),
            Argument::exact(array(
                'statusTime',
                'status',
                'updateTime',
                'publishUserGroup',
                'crew'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $journal->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $journal->updateStatus($status);
        
        $this->assertTrue($result);
    }

    public function testEnable()
    {
        $journalEnable = $this->getMockBuilder(MockJournal::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
        
        // $journalEnable->expects($this->exactly(1))
        //     ->method('updateStatus')
        //     ->with(IEnableAble::STATUS['ENABLED'])
        //     ->willReturn(true);

        $status = IEnableAble::STATUS['ENABLED'];
        $journalEnable->setStatus($status);
        $journalEnable->setUpdateTime(Core::$container->get('time'));
        $journalEnable->setStatusTime(Core::$container->get('time'));

        $repository = $this->prophesize(IJournalAdapter::class);

        $repository->edit(
            Argument::exact($journalEnable),
            Argument::exact(array(
                'statusTime',
                'status',
                'updateTime',
                'publishUserGroup',
                'crew'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $journalEnable->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $journalEnable->enable();
        $this->assertTrue($result);
    }
}
