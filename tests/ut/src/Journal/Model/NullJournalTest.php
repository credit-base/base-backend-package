<?php
namespace Base\Journal\Model;

use PHPUnit\Framework\TestCase;

class NullJournalTest extends TestCase
{
    private $journal;

    public function setUp()
    {
        $this->journal = NullJournal::getInstance();
    }

    public function tearDown()
    {
        unset($this->journal);
    }

    public function testExtendsJournal()
    {
        $this->assertInstanceof('Base\Journal\Model\Journal', $this->journal);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->journal);
    }
}
