<?php
namespace Base\Journal\WidgetRule;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Journal\Model\Journal;
use Base\Journal\Model\Banner;

class JournalWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new JournalWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //attachment -- start
    /**
     * @dataProvider invalidAttachmentProvider
     */
    public function testAttachmentInvalid($actual, $expected)
    {
        $result = $this->widgetRule->attachment($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidAttachmentProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array('name'=>$faker->name,'identify'=>$faker->name), false),
            array(array('name'=>$faker->name,'identify'=>'1.pdf'), true),
        );
    }
    //attachment -- end

    /**
     * @dataProvider authImagesProvider
     */
    public function testAuthImages($actual, $expected)
    {
        $result = $this->widgetRule->authImages($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function authImagesProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        
        return array(
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                ], false),
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                ], true),
            array($faker->word, false),
            array(
                [
                    ['name'=>$faker->word, 'identify'=>$faker->word],
                    ['name'=>$faker->word, 'identify'=>$faker->word]
                ], false),
        );
    }

    //year -- start
    /**
     * @dataProvider invalidYearProvider
     */
    public function testYearInvalid($actual, $expected)
    {
        $result = $this->widgetRule->year($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidYearProvider()
    {
        return array(
            array('', false),
            array('year', false),
            array(JournalWidgetRule::YEAR_MIN_LENGTH, true),
            array(JournalWidgetRule::YEAR_MAX_LENGTH, true)
        );
    }
    //year -- end
}
