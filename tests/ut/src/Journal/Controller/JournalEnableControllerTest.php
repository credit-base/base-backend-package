<?php
namespace Base\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Journal\Utils\MockFactory;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Repository\UnAuditedJournalRepository;
use Base\Journal\Command\Journal\EnableJournalCommand;
use Base\Journal\Command\Journal\DisableJournalCommand;

class JournalEnableControllerTest extends TestCase
{
    private $journalStub;

    public function setUp()
    {
        $this->journalStub = $this->getMockBuilder(JournalEnableController::class)
                        ->setMethods([
                            'displayError',
                            ])
                        ->getMock();
    }

    public function teatDown()
    {
        unset($this->journalStub);
    }

    private function initialDisable($result)
    {
        $this->journalStub = $this->getMockBuilder(JournalEnableController::class)
                    ->setMethods([
                        'getRequest',
                        'validateCommonScenario',
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = $crewId = 1;

        $data = array(
            'relationships' => array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$crewId)
                    )
                )
            ),
        );

        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->journalStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->journalStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->willReturn(true);

        $command = new DisableJournalCommand($crewId, $id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->journalStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDisableFailure()
    {
        $command = $this->initialDisable(false);

        $this->journalStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->journalStub->disable($command->id);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $command = $this->initialDisable(true);

        $journal = MockFactory::generateJournal($command->id);

        $repository = $this->prophesize(JournalRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($journal);
        $this->journalStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->journalStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->journalStub->disable($command->id);
        $this->assertTrue($result);
    }

    private function initialEnable($result)
    {
        $this->journalStub = $this->getMockBuilder(JournalEnableController::class)
                    ->setMethods([
                        'getRequest',
                        'validateCommonScenario',
                        'getCommandBus',
                        'getUnAuditedJournalRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $crewId = 2;

        $data = array(
            'relationships' => array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$crewId)
                    )
                )
            ),
        );

        $this->journalStub->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->willReturn(true);
            
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->journalStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $command = new EnableJournalCommand($id, $crewId);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->journalStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testEnableFailure()
    {
        $command = $this->initialEnable(false);

        $this->journalStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->journalStub->enable($command->journalId);
        $this->assertFalse($result);
    }

    public function testEnableSuccess()
    {
        $command = $this->initialEnable(true);

        $journal = MockFactory::generateUnAuditedJournal($command->journalId);

        $repository = $this->prophesize(UnAuditedJournalRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($journal);

        $this->journalStub->expects($this->exactly(1))
             ->method('getUnAuditedJournalRepository')
             ->willReturn($repository->reveal());
             
        $this->journalStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->journalStub->enable($command->journalId);
        $this->assertTrue($result);
    }
}
