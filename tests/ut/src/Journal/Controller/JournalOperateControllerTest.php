<?php
namespace Base\Journal\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Command\Journal\AddJournalCommand;
use Base\Journal\Command\Journal\EditJournalCommand;
use Base\Journal\Repository\UnAuditedJournalRepository;

class JournalOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new JournalOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    private function journalData() : array
    {
        return array(
            "type"=>"journal",
            "attributes"=>array(
                "title"=>"标题",
                "source"=>"来源",
                "cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
                "attachment"=>array('name' => '附件名称', 'identify' => '附件地址.pdf'),
                "authImages"=>array(
                    array('name' => 'name', 'identify' => 'identify.jpg'),
                    array('name' => 'name', 'identify' => 'identify.jpg'),
                    array('name' => 'name', 'identify' => 'identify.jpg')
                ),
                "description"=>"简介",
                "year"=>2021,
                "status"=>0
            ),
            "relationships"=>array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>1)
                    )
                )
            )
        );
    }
    /**
     * 测试 add 成功
     * 1. 为 JournalOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getJournalWidgetRule、getCommandBus、getUnAuditedJournalRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 Journal 类建立预言
     * 4. 为 UnAuditedJournalRepository 类建立预言, UnAuditedJournalRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Journal, getUnAuditedJournalRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 为 JournalOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getJournalWidgetRule、getCommandBus、getUnAuditedJournalRepository、render方法
        $controller = $this->getMockBuilder(JournalOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateJournalScenario',
                    'getCommandBus',
                    'getUnAuditedJournalRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 true
        $command = $this->initAdd($controller, true);

        // 为 Journal 类建立预言
        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($command->id);

        // 为 UnAuditedJournalRepository 类建立预言, UnAuditedJournalRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Journal, getUnAuditedJournalRepository 方法被调用一次
        $repository = $this->prophesize(UnAuditedJournalRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedJournal);
        $controller->expects($this->once())
                             ->method('getUnAuditedJournalRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->add 方法被调用一次, 且返回结果为 true
        $result = $controller->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 JournalOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getJournalWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 为 JournalOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getJournalWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(JournalOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateJournalScenario',
                    'getCommandBus',
                    'getUnAuditedJournalRepository',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd($controller, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->add 方法被调用一次, 且返回结果为 false
        $result = $controller->add();
        $this->assertFalse($result);
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 JournalWidgetRule 类建立预言, 验证请求参数, getJournalWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddJournalCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(JournalOperateController $controller, bool $result)
    {
        // mock 请求参数
        $data = $this->journalData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $description = $attributes['description'];
        $cover = $attributes['cover'];
        $attachment = $attributes['attachment'];
        $authImages = $attributes['authImages'];
        $status = $attributes['status'];
        $year = $attributes['year'];

        $crew = $relationships['crew']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateJournalScenario')
            ->willReturn(true);

        $command = new AddJournalCommand(
            $title,
            $source,
            $description,
            $cover,
            $attachment,
            $authImages,
            $year,
            $status,
            $crew
        );
        // 为 CommandBus 类建立预言, 传入 AddJournalCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
            
        return $command;
    }

    /**
     * 测试 edit 成功
     * 1. 为 JournalOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getJournalWidgetRule、getCommandBus、getUnAuditedJournalRepository、render方法
     * 2. 调用 $this->initEdit(), 期望结果为 true
     * 3. 为 Journal 类建立预言
     * 4. 为 UnAuditedJournalRepository 类建立预言, UnAuditedJournalRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Journal, getUnAuditedJournalRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 JournalOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getJournalWidgetRule、getCommandBus、getUnAuditedJournalRepository、render方法
        $controller = $this->getMockBuilder(JournalOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateJournalScenario',
                    'getCommandBus',
                    'getUnAuditedJournalRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;

        $command = $this->initEdit($controller, $id, true);

        // 为 Journal 类建立预言
        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($command->id);

        // 为 UnAuditedJournalRepository 类建立预言, UnAuditedJournalRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Journal, getUnAuditedJournalRepository 方法被调用一次
        $repository = $this->prophesize(UnAuditedJournalRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedJournal);
        $controller->expects($this->once())
                             ->method('getUnAuditedJournalRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->edit 方法被调用一次, 且返回结果为 true
        $result = $controller->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 JournalOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getJournalWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initEdit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 JournalOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getJournalWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(JournalOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateJournalScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 false
        $this->initEdit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->edit 方法被调用一次, 且返回结果为 false
        $result = $controller->edit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 edit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 JournalWidgetRule 类建立预言, 验证请求参数, getJournalWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 EditJournalCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEdit(JournalOperateController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->journalData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $description = $attributes['description'];
        $cover = $attributes['cover'];
        $authImages = $attributes['authImages'];
        $attachment = $attributes['attachment'];
        $status = $attributes['status'];
        $year = $attributes['year'];

        $crew = $relationships['crew']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateJournalScenario')
            ->willReturn(true);

        $command = new EditJournalCommand(
            $title,
            $source,
            $description,
            $cover,
            $attachment,
            $authImages,
            $year,
            $status,
            $id,
            $crew
        );
        // 为 CommandBus 类建立预言, 传入 EditJournalCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        return $command;
    }
}
