<?php
namespace Base\Journal\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Command\Journal\ResubmitJournalCommand;
use Base\Journal\Repository\UnAuditedJournalRepository;

class JournalResubmitControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new JournalResubmitController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIResubmitAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IResubmitAbleController',
            $this->controller
        );
    }

    private function journalData() : array
    {
        return array(
            "type"=>"journal",
            "attributes"=>array(
                "title"=>"标题",
                "source"=>"来源",
                "cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
                "attachment"=>array('name' => '附件名称', 'identify' => '附件地址.pdf'),
                "authImages"=>array(
                    array('name' => 'name', 'identify' => 'identify.jpg'),
                    array('name' => 'name', 'identify' => 'identify.jpg')
                ),
                "description"=>"简介",
                "year"=>2020,
                "status"=>-2
            ),
            "relationships"=>array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>1)
                    )
                )
            )
        );
    }
    
    /**
     * 测试 resubmit 成功
     * 1. 为 JournalResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getJournalWidgetRule、getCommandBus、getUnAuditedJournalRepository、render方法
     * 2. 调用 $this->initResubmit(), 期望结果为 true
     * 3. 为 Journal 类建立预言
     * 4. 为 UnAuditedJournalRepository 类建立预言, UnAuditedJournalRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Journal, getUnAuditedJournalRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->resubmit 方法被调用一次, 且返回结果为 true
     */
    public function testResubmit()
    {
        // 为 JournalResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getJournalWidgetRule、getCommandBus、getUnAuditedJournalRepository、render方法
        $controller = $this->getMockBuilder(JournalResubmitController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateJournalScenario',
                    'getCommandBus',
                    'getUnAuditedJournalRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;

        $this->initResubmit($controller, $id, true);

        // 为 Journal 类建立预言
        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($id);

        // 为 UnAuditedJournalRepository 类建立预言, UnAuditedJournalRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Journal, getUnAuditedJournalRepository 方法被调用一次
        $repository = $this->prophesize(UnAuditedJournalRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedJournal);
        $controller->expects($this->once())
                             ->method('getUnAuditedJournalRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->resubmit 方法被调用一次, 且返回结果为 true
        $result = $controller->resubmit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 resubmit 失败
     * 1. 为 JournalResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getJournalWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initResubmit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->resubmit 方法被调用一次, 且返回结果为 false
     */
    public function testResubmitFail()
    {
        // 为 JournalResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getJournalWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(JournalResubmitController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateJournalScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        $id = 1;

        // 调用 $this->initResubmit(), 期望结果为 false
        $this->initResubmit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->resubmit 方法被调用一次, 且返回结果为 false
        $result = $controller->resubmit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 resubmit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 JournalWidgetRule 类建立预言, 验证请求参数, getJournalWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 ResubmitJournalCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initResubmit(JournalResubmitController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->journalData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $description = $attributes['description'];
        $cover = $attributes['cover'];
        $attachment = $attributes['attachment'];
        $authImages = $attributes['authImages'];
        $status = $attributes['status'];
        $year = $attributes['year'];

        $crew = $relationships['crew']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateJournalScenario')
            ->willReturn(true);

        $command = new ResubmitJournalCommand(
            $title,
            $source,
            $description,
            $cover,
            $attachment,
            $authImages,
            $year,
            $status,
            $crew,
            $id
        );
        // 为 CommandBus 类建立预言, 传入 ResubmitJournalCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }
}
