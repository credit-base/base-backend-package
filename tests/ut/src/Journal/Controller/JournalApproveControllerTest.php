<?php
namespace Base\Journal\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class JournalApproveControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockJournalApproveController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIApproveAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IApproveAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockJournalApproveController::class)
            ->setMethods(['render'])->getMock();

        $id = 1;
        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($id);
        
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        $controller->displaySuccess($unAuditedJournal);
    }
}
