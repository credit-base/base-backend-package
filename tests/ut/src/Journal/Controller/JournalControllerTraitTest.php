<?php
namespace Base\Journal\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Journal\WidgetRule\JournalWidgetRule;

class JournalControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockJournalControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetJournalWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Journal\WidgetRule\JournalWidgetRule',
            $this->trait->getJournalWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Journal\Repository\JournalRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetUnAuditedJournalRepository()
    {
        $this->assertInstanceOf(
            'Base\Journal\Repository\UnAuditedJournalRepository',
            $this->trait->getUnAuditedJournalRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    public function testValidateCommonScenario()
    {
        $controller = $this->getMockBuilder(MockJournalControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule']) ->getMock();

        $crew = 1;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($crew), Argument::exact('crewId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateCommonScenarioPublic($crew);
        
        $this->assertTrue($result);
    }

    public function testValidateJournalScenario()
    {
        $controller = $this->getMockBuilder(MockJournalControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule', 'getJournalWidgetRule']) ->getMock();

        $id = 1;
        $journal = \Base\Journal\Utils\MockFactory::generateJournal($id);
        $title = $journal->getTitle();
        $source = $journal->getSource();
        $description = $journal->getDescription();
        $cover = $journal->getCover();
        $attachment = $journal->getAttachment();
        $authImages = $journal->getAuthImages();
        $year = $journal->getYear();
        $status = $journal->getStatus();
        $crew = $journal->getCrew()->getId();

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->title(Argument::exact($title))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->source(Argument::exact($source))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->description(Argument::exact($description))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->image(
            Argument::exact($cover),
            Argument::exact('cover')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->status(Argument::exact($status))->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->formatNumeric(
            Argument::exact($crew),
            Argument::exact('crewId')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->any())
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $journalWidgetRule = $this->prophesize(JournalWidgetRule::class);

        $journalWidgetRule->attachment(Argument::exact($attachment))->shouldBeCalledTimes(1)->willReturn(true);
        $journalWidgetRule->authImages(Argument::exact($authImages))->shouldBeCalledTimes(1)->willReturn(true);
        $journalWidgetRule->year(Argument::exact($year))->shouldBeCalledTimes(1)->willReturn(true);
        $controller->expects($this->any())
            ->method('getJournalWidgetRule')
            ->willReturn($journalWidgetRule->reveal());

        $result = $controller->validateJournalScenarioPublic(
            $title,
            $source,
            $description,
            $cover,
            $attachment,
            $authImages,
            $year,
            $status,
            $crew
        );
        
        $this->assertTrue($result);
    }
}
