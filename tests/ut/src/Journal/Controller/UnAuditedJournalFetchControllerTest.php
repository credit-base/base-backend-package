<?php
namespace Base\Journal\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Repository\UnAuditedJournalRepository;

class UnAuditedJournalFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(UnAuditedJournalFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockUnAuditedJournalFetchController();

        $this->assertInstanceOf(
            'Base\Journal\Repository\UnAuditedJournalRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockUnAuditedJournalFetchController();

        $this->assertInstanceOf(
            'Base\Journal\View\UnAuditedJournalView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockUnAuditedJournalFetchController();

        $this->assertEquals(
            'unAuditedJournals',
            $controller->getResourceName()
        );
    }

    private function initialFilter($unAuditedJournalArray)
    {
        $this->stub = $this->getMockBuilder(MockUnAuditedJournalFetchController::class)
                    ->setMethods(['getRepository', 'renderView', 'displayError', 'formatParameters'])
                    ->getMock();

        $sort = array();
        $curpage = 1;
        $perpage = 20;
        $filter['applyInfoCategory'] = UnAuditedJournal::APPLY_JOURNAL_CATEGORY;
        $filter['applyInfoType'] = UnAuditedJournal::APPLY_JOURNAL_TYPE;

        $this->stub->expects($this->any())
        ->method('formatParameters')
        ->willReturn([$filter, $sort, $curpage, $perpage]);

        $repository = $this->prophesize(UnAuditedJournalRepository::class);

        $repository->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(($curpage-1)*$perpage),
            Argument::exact($perpage)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedJournalArray);

        $this->stub->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());
    }
 
    public function testFilterSuccess()
    {
        $unAuditedJournalArray = array(
            array(\Base\Journal\Utils\MockFactory::generateUnAuditedJournal(1)),
            1
        );

        $this->initialFilter($unAuditedJournalArray);
        
        $this->stub->expects($this->exactly(1))
        ->method('renderView')
        ->willReturn(true);
        
        $result = $this->stub->filter();

        $this->assertTrue($result);
    }

    public function testFilterFailure()
    {
        $unAuditedJournalArray = array(
            array(),
            0
        );

        $this->initialFilter($unAuditedJournalArray);
        
        $this->stub->expects($this->exactly(1))
        ->method('displayError')
        ->willReturn(false);
        
        $result = $this->stub->filter();

        $this->assertFalse($result);
    }
}
