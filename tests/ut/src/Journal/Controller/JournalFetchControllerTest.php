<?php
namespace Base\Journal\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Journal\Adapter\Journal\IJournalAdapter;
use Base\Journal\Model\NullJournal;
use Base\Journal\Model\Journal;
use Base\Journal\View\JournalView;

class JournalFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(JournalFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockJournalFetchController();

        $this->assertInstanceOf(
            'Base\Journal\Adapter\Journal\IJournalAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockJournalFetchController();

        $this->assertInstanceOf(
            'Base\Journal\View\JournalView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockJournalFetchController();

        $this->assertEquals(
            'journals',
            $controller->getResourceName()
        );
    }
}
