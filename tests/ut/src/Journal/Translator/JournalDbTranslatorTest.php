<?php
namespace Base\Journal\Translator;

use PHPUnit\Framework\TestCase;

use Base\Journal\Utils\JournalUtils;
use Base\Journal\Utils\MockFactory;

class JournalDbTranslatorTest extends TestCase
{
    use JournalUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new JournalDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Journal\Model\NullJournal', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $journal = MockFactory::generateJournal(1);

        $expression['journal_id'] = $journal->getId();
        $expression['title'] = $journal->getTitle();
        $expression['source'] = $journal->getSource();
        $expression['description'] = $journal->getDescription();
        $expression['cover'] = $journal->getCover();
        $expression['attachment'] = $journal->getAttachment();
        $expression['auth_images'] = $journal->getAuthImages();
        $expression['year'] = $journal->getYear();
        $expression['crew_id'] = $journal->getCrew()->getId();
        $expression['publish_usergroup_id'] = $journal->getPublishUserGroup()->getId();

        $expression['status'] = $journal->getStatus();
        $expression['status_time'] = $journal->getStatusTime();
        $expression['create_time'] = $journal->getCreateTime();
        $expression['update_time'] = $journal->getUpdateTime();

        $journal = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Journal\Model\Journal', $journal);
        $this->compareArrayAndObjectJournal($expression, $journal);
    }

    public function testObjectToArray()
    {
        $journal = MockFactory::generateJournal(1);

        $expression = $this->translator->objectToArray($journal);

        $this->compareArrayAndObjectJournal($expression, $journal);
    }
}
