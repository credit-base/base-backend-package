<?php
namespace Base\Journal\Translator;

use PHPUnit\Framework\TestCase;

use Base\Journal\Utils\JournalUtils;

class UnAuditedJournalDbTranslatorTest extends TestCase
{
    use JournalUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditedJournalDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testExtendsApplyFormDbTranslator()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Translator\ApplyFormDbTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal(1);

        $expression['apply_form_id'] = $unAuditedJournal->getId();
        $expression['journal_id'] = $unAuditedJournal->getId();
        $expression['title'] = $unAuditedJournal->getApplyTitle();
        $expression['relation_id'] = $unAuditedJournal->getRelation()->getId();
        $expression['operation_type'] = $unAuditedJournal->getOperationType();
        $expression['apply_info_category'] = $unAuditedJournal->getApplyInfoCategory()->getCategory();
        $expression['apply_info_type'] = $unAuditedJournal->getApplyInfoCategory()->getType();
        $expression['reject_reason'] = $unAuditedJournal->getRejectReason();
        $expression['apply_info'] = base64_encode(gzcompress(serialize($unAuditedJournal->getApplyInfo())));
        $expression['apply_crew_id'] = $unAuditedJournal->getApplyCrew()->getId();
        $expression['apply_usergroup_id'] = $unAuditedJournal->getApplyUserGroup()->getId();
        $expression['apply_status'] = $unAuditedJournal->getApplyStatus();
        $expression['status_time'] = $unAuditedJournal->getStatusTime();
        $expression['create_time'] = $unAuditedJournal->getCreateTime();
        $expression['update_time'] = $unAuditedJournal->getUpdateTime();

        $unAuditedJournal = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Journal\Model\UnAuditedJournal', $unAuditedJournal);
        $this->compareArrayAndObjectUnAuditedJournal($expression, $unAuditedJournal);
    }

    public function testArrayToObjects()
    {
        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal(1);

        $expression['journal_id'] = $unAuditedJournal->getApplyId();
        $expression['title'] = $unAuditedJournal->getTitle();
        $expression['source'] = $unAuditedJournal->getSource();
        $expression['description'] = $unAuditedJournal->getDescription();
        $expression['cover'] = json_encode($unAuditedJournal->getCover());
        $expression['attachment'] = json_encode($unAuditedJournal->getAttachment());
        $expression['auth_images'] = json_encode($unAuditedJournal->getAuthImages());
        $expression['year'] = $unAuditedJournal->getYear();
        $expression['crew_id'] = $unAuditedJournal->getCrew()->getId();
        $expression['publish_usergroup_id'] = $unAuditedJournal->getPublishUserGroup()->getId();
        $expression['status'] = $unAuditedJournal->getStatus();
        $expression['status_time'] = $unAuditedJournal->getStatusTime();
        $expression['create_time'] = $unAuditedJournal->getCreateTime();
        $expression['update_time'] = $unAuditedJournal->getUpdateTime();

        $unAuditedJournal = $this->translator->arrayToObjects($expression, $unAuditedJournal);

        $this->assertInstanceof('Base\Journal\Model\UnAuditedJournal', $unAuditedJournal);
        $this->compareArrayAndObjectsUnAuditedJournal($expression, $unAuditedJournal);
    }

    public function testObjectToArray()
    {
        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal(1);

        $expression = $this->translator->objectToArray($unAuditedJournal);

        $this->compareArrayAndObjectUnAuditedJournal($expression, $unAuditedJournal);
    }
}
