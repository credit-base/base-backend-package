<?php
namespace Base\Journal\Utils;

use Base\Journal\Model\Journal;
use Base\Journal\Model\Banner;
use Base\Journal\Model\UnAuditedJournal;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

use Base\ApplyForm\Model\ApplyInfoCategory;

class MockFactory
{
    public static function generateCommon(
        Journal $journal,
        int $seed = 0,
        array $value = array()
    ) {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);
        //title
        $title = isset($value['title']) ? $value['title'] : $faker->word();
        $journal->setTitle($title);

        //source
        $source = isset($value['source']) ? $value['source'] : $faker->word();
        $journal->setSource($source);

        //description
        $description = isset($value['description']) ? $value['description'] : $faker->text();
        $journal->setDescription($description);

        //cover
        $cover = isset($value['cover']) ? $value['cover'] : array(
            'name'=>$faker->name(),
            'identify'=>$faker->word().'.jpg'
        );
        $journal->setCover($cover);

        //attachment
        $attachment = isset($value['attachment']) ? $value['attachment'] : array(
            'name'=>$faker->name(), 'identify'=>$faker->word().'.pdf'
        );
        $journal->setAttachment($attachment);

        //authImages
        $authImages = isset($value['authImages']) ? $value['authImages'] : array(
            array('name'=>$faker->name(), 'identify'=>$faker->word().'.jpg')
        );
        $journal->setAuthImages($authImages);
        
        //year
        $year = isset($value['year']) ? $value['year'] : $faker->year();
        $journal->setYear($year);

        self::generateStatus($journal, $faker, $value);

        $journal->setCreateTime($faker->unixTime());
        $journal->setUpdateTime($faker->unixTime());
        $journal->setStatusTime($faker->unixTime());

        return $journal;
    }

    protected static function generateCrew($journal, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $journal->setCrew($crew);
    }

    protected static function generateStatus($journal, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                IEnableAble::STATUS
            );
        
        $journal->setStatus($status);
    }

    public static function generateJournal(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Journal {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $journal = new Journal($id);
        $journal->setId($id);

        $journal = self::generateCommon($journal, $seed, $value);

        return $journal;
    }

    public static function generateUnAuditedJournal(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedJournal {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditJournal = new UnAuditedJournal($id);
        
        $unAuditJournal->setApplyId($id);

        $unAuditJournal = self::generateCommon($unAuditJournal, $seed, $value);

        $unAuditJournal->setApplyTitle($unAuditJournal->getTitle());
        $unAuditJournal->setRelation($unAuditJournal->getCrew());
        $unAuditJournal->setApplyUserGroup($unAuditJournal->getPublishUserGroup());
        $unAuditJournal->setApplyCrew($unAuditJournal->getCrew());
        $unAuditJournal->setRejectReason($unAuditJournal->getTitle());
        $unAuditJournal->setApplyInfo(array('apply_info'));

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            IApproveAble::APPLY_STATUS
        );
        $unAuditJournal->setApplyStatus($applyStatus);

        //operationType
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomElement(
            IApproveAble::OPERATION_TYPE
        );
        $unAuditJournal->setOperationType($operationType);

        return $unAuditJournal;
    }
}
