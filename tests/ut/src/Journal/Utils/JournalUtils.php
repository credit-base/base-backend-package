<?php
namespace Base\Journal\Utils;

trait JournalUtils
{

    private function compareArrayAndObjectJournal(
        array $expectedArray,
        $journal
    ) {
        $this->assertEquals($expectedArray['journal_id'], $journal->getId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $journal);
    }

    private function compareArrayAndObjectsUnAuditedJournal(
        array $expectedArray,
        $unAuditedJournal
    ) {
        $this->assertEquals($expectedArray['journal_id'], $unAuditedJournal->getApplyId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedJournal);
    }

    private function compareArrayAndObjectUnAuditedJournal(
        array $expectedArray,
        $unAuditedJournal
    ) {
        $applyInfo = array();
        if (is_string($expectedArray['apply_info'])) {
            $applyInfo = unserialize(gzuncompress(base64_decode($expectedArray['apply_info'], true)));
        }
        if (is_array($expectedArray['apply_info'])) {
            $applyInfo = $expectedArray['apply_info'];
        }
        
        $this->assertEquals($applyInfo, $unAuditedJournal->getApplyInfo());
        $this->assertEquals($expectedArray['apply_form_id'], $unAuditedJournal->getApplyId());
        $this->assertEquals($expectedArray['title'], $unAuditedJournal->getApplyTitle());
        $this->assertEquals($expectedArray['relation_id'], $unAuditedJournal->getRelation()->getId());
        $this->assertEquals($expectedArray['operation_type'], $unAuditedJournal->getOperationType());
        $this->assertEquals(
            $expectedArray['apply_info_category'],
            $unAuditedJournal->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals($expectedArray['apply_info_type'], $unAuditedJournal->getApplyInfoCategory()->getType());
        $this->assertEquals($expectedArray['reject_reason'], $unAuditedJournal->getRejectReason());
        $this->assertEquals($expectedArray['apply_status'], $unAuditedJournal->getApplyStatus());
        $this->assertEquals($expectedArray['apply_crew_id'], $unAuditedJournal->getApplyCrew()->getId());
        $this->assertEquals($expectedArray['apply_usergroup_id'], $unAuditedJournal->getApplyUserGroup()->getId());
    }

    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $journal
    ) {
        $this->assertEquals($expectedArray['title'], $journal->getTitle());
        $this->assertEquals($expectedArray['source'], $journal->getSource());
        $this->assertEquals($expectedArray['description'], $journal->getDescription());
        $this->coverEquals($expectedArray, $journal);
        $this->attachmentEquals($expectedArray, $journal);
        $this->authImagesEquals($expectedArray, $journal);
        $this->assertEquals($expectedArray['year'], $journal->getYear());
        $this->assertEquals($expectedArray['crew_id'], $journal->getCrew()->getId());
        $this->assertEquals($expectedArray['publish_usergroup_id'], $journal->getPublishUserGroup()->getId());

        $this->assertEquals($expectedArray['status'], $journal->getStatus());
        $this->assertEquals($expectedArray['create_time'], $journal->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $journal->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $journal->getStatusTime());
    }

    private function coverEquals($expectedArray, $journal)
    {
        $cover = array();

        if (is_string($expectedArray['cover'])) {
            $cover = json_decode($expectedArray['cover'], true);
        }
        if (is_array($expectedArray['cover'])) {
            $cover = $expectedArray['cover'];
        }

        $this->assertEquals($cover, $journal->getCover());
    }

    private function attachmentEquals($expectedArray, $journal)
    {
        $attachment = array();

        if (is_string($expectedArray['attachment'])) {
            $attachment = json_decode($expectedArray['attachment'], true);
        }
        if (is_array($expectedArray['attachment'])) {
            $attachment = $expectedArray['attachment'];
        }

        $this->assertEquals($attachment, $journal->getAttachment());
    }

    private function authImagesEquals($expectedArray, $journal)
    {
        $authImages = array();

        if (is_string($expectedArray['auth_images'])) {
            $authImages = json_decode($expectedArray['auth_images'], true);
        }
        if (is_array($expectedArray['auth_images'])) {
            $authImages = $expectedArray['auth_images'];
        }

        $this->assertEquals($authImages, $journal->getAuthImages());
    }
}
