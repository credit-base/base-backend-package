<?php
namespace Base\Journal\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;
use Base\Journal\Adapter\Journal\UnAuditedJournalDbAdapter;
use Base\Journal\Adapter\Journal\UnAuditedJournalMockAdapter;

use Base\Journal\Utils\MockFactory;

class UnAuditedJournalRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditedJournalRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockUnAuditedJournalRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Journal\Adapter\Journal\UnAuditedJournalDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Journal\Adapter\Journal\UnAuditedJournalMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }
}
