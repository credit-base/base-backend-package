<?php
namespace Base\Journal\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Journal\Adapter\Journal\IJournalAdapter;
use Base\Journal\Adapter\Journal\JournalDbAdapter;

use Base\Journal\Utils\MockFactory;

class JournalRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(JournalRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockJournalRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new JournalDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Journal\Adapter\Journal\IJournalAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Journal\Adapter\Journal\JournalDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Journal\Adapter\Journal\IJournalAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Journal\Adapter\Journal\JournalMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IJournalAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IJournalAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IJournalAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $journal = MockFactory::generateJournal($id);
        $keys = array();
        
        $adapter = $this->prophesize(IJournalAdapter::class);
        $adapter->add(Argument::exact($journal))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($journal, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $journal = MockFactory::generateJournal($id);
        $keys = array();
        
        $adapter = $this->prophesize(IJournalAdapter::class);
        $adapter->edit(Argument::exact($journal), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($journal, $keys);
    }
}
