<?php
namespace Base\Journal\Adapter\Journal;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Journal\Model\UnAuditedJournal;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class UnAuditedJournalMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new UnAuditedJournalMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new UnAuditedJournal()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new UnAuditedJournal(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\IApplyFormAble',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Journal\Model\UnAuditedJournal',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Journal\Model\UnAuditedJournal',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
