<?php
namespace Base\Journal\Adapter\Journal;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Journal\Model\Journal;
use Base\Journal\Model\NullJournal;
use Base\Journal\Translator\JournalDbTranslator;
use Base\Journal\Adapter\Journal\Query\JournalRowCacheQuery;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class JournalDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(JournalDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchCrew',
                                'fetchPublishUserGroup'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IJournalAdapter
     */
    public function testImplementsIJournalAdapter()
    {
        $this->assertInstanceOf(
            'Base\Journal\Adapter\Journal\IJournalAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 JournalDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockJournalDbAdapter();
        $this->assertInstanceOf(
            'Base\Journal\Translator\JournalDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 JournalRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockJournalDbAdapter();
        $this->assertInstanceOf(
            'Base\Journal\Adapter\Journal\Query\JournalRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetCrewRepository()
    {
        $adapter = new MockJournalDbAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }

    public function testGetUserGroupRepository()
    {
        $adapter = new MockJournalDbAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockJournalDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedJournal = new Journal();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedJournal)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedJournal);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedJournal = new Journal();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedJournal, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedJournal, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedJournal = new Journal();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedJournal);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchPublishUserGroup')
                         ->with($expectedJournal);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedJournal);
        //验证
        $journal = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedJournal, $journal);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $journalOne = new Journal(1);
        $journalTwo = new Journal(2);

        $ids = [1, 2];

        $expectedJournalList = [];
        $expectedJournalList[$journalOne->getId()] = $journalOne;
        $expectedJournalList[$journalTwo->getId()] = $journalTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedJournalList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedJournalList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchPublishUserGroup')
                         ->with($expectedJournalList);
        //验证
        $journalList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedJournalList, $journalList);
    }

    //fetchPublishUserGroup
    public function testFetchPublishUserGroupIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockJournalDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchPublishUserGroupByObject'
                                ]
                           )
                           ->getMock();
        
        $journal = new Journal();

        $adapter->expects($this->exactly(1))
                         ->method('fetchPublishUserGroupByObject')
                         ->with($journal);

        $adapter->fetchPublishUserGroup($journal);
    }

    public function testFetchPublishUserGroupIsArray()
    {
        $adapter = $this->getMockBuilder(MockJournalDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchPublishUserGroupByList'
                                ]
                           )
                           ->getMock();
        
        $journalOne = new Journal(1);
        $journalTwo = new Journal(2);
        
        $journalList[$journalOne->getId()] = $journalOne;
        $journalList[$journalTwo->getId()] = $journalTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchPublishUserGroupByList')
                         ->with($journalList);

        $adapter->fetchPublishUserGroup($journalList);
    }

    //fetchPublishUserGroupByObject
    public function testFetchPublishUserGroupByObject()
    {
        $adapter = $this->getMockBuilder(MockJournalDbAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $journal = new Journal();
        $journal->setPublishUserGroup(new UserGroup(1));
        
        $userGroupId = 1;

        // 为 UserGroup 类建立预言
        $userGroup  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($userGroup);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchPublishUserGroupByObject($journal);
    }

    //fetchPublishUserGroupByList
    public function testFetchUserGroupByList()
    {
        $adapter = $this->getMockBuilder(MockJournalDbAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $journalOne = new Journal(1);
        $userGroupOne = new UserGroup(1);
        $journalOne->setPublishUserGroup($userGroupOne);

        $journalTwo = new Journal(2);
        $userGroupTwo = new UserGroup(2);
        $journalTwo->setPublishUserGroup($userGroupTwo);
        $userGroupIds = [1, 2];
        
        $userGroupList[$userGroupOne->getId()] = $userGroupOne;
        $userGroupList[$userGroupTwo->getId()] = $userGroupTwo;
        
        $journalList[$journalOne->getId()] = $journalOne;
        $journalList[$journalTwo->getId()] = $journalTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(Argument::exact($userGroupIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($userGroupList);

        $adapter->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->fetchPublishUserGroupByList($journalList);
    }

    //fetchCrew
    public function testFetchCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockJournalDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByObject'
                                ]
                           )
                           ->getMock();
        
        $journal = new Journal();

        $adapter->expects($this->exactly(1))
                         ->method('fetchCrewByObject')
                         ->with($journal);

        $adapter->fetchCrew($journal);
    }

    public function testFetchCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockJournalDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByList'
                                ]
                           )
                           ->getMock();
        
        $journalOne = new Journal(1);
        $journalTwo = new Journal(2);
        
        $journalList[$journalOne->getId()] = $journalOne;
        $journalList[$journalTwo->getId()] = $journalTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchCrewByList')
                         ->with($journalList);

        $adapter->fetchCrew($journalList);
    }

    //fetchCrewByObject
    public function testFetchCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockJournalDbAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $journal = new Journal();
        $journal->setCrew(new Crew(1));
        
        $crewId = 1;

        // 为 Crew 类建立预言
        $crew  = $this->prophesize(Crew::class);
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $adapter->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByObject($journal);
    }

    //fetchCrewByList
    public function testFetchCrewByList()
    {
        $adapter = $this->getMockBuilder(MockJournalDbAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $journalOne = new Journal(1);
        $crewOne = new Crew(1);
        $journalOne->setCrew($crewOne);

        $journalTwo = new Journal(2);
        $crewTwo = new Crew(2);
        $journalTwo->setCrew($crewTwo);
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $journalList[$journalOne->getId()] = $journalOne;
        $journalList[$journalTwo->getId()] = $journalTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crewList);

        $adapter->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByList($journalList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-publishUserGroup
    public function testFormatFilterPublishUserGroup()
    {
        $filter = array(
            'publishUserGroup' => 1
        );
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = 'publish_usergroup_id = '.$filter['publishUserGroup'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-title
    public function testFormatFilterTitle()
    {
        $filter = array(
            'title' => 1
        );
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = 'title LIKE \'%'.$filter['title'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-status
    public function testFormatFilterStatus()
    {
        $filter = array(
            'status' => -2
        );
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = 'status = '.$filter['status'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-year
    public function testFormatFilterStick()
    {
        $filter = array(
            'year' => 2021
        );
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = 'year = '.$filter['year'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--status
    public function testFormatSortStatusDesc()
    {
        $sort = array('status' => -1);
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = ' ORDER BY status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-status
    public function testFormatSortStatusAsc()
    {
        $sort = array('status' => 1);
        $adapter = new MockJournalDbAdapter();

        $expectedCondition = ' ORDER BY status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
