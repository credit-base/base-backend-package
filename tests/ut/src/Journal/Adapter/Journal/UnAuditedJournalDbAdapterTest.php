<?php
namespace Base\Journal\Adapter\Journal;

use PHPUnit\Framework\TestCase;

class UnAuditedJournalDbAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new UnAuditedJournalDbAdapter();
        $this->assertInstanceof('Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter', $adapter);
    }
}
