<?php
namespace Base\Journal\Adapter\Journal\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class JournalDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockJournalDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(JournalDb::TABLE, $this->database->getTable());
    }
}
