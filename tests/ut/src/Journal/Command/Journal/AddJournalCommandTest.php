<?php
namespace Base\Journal\Command\Journal;

use PHPUnit\Framework\TestCase;

use Base\Journal\Model\Journal;

use Base\Common\Model\IEnableAble;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class AddJournalCommandTest extends TestCase
{
    use JournalTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'title' => $faker->title(),
            'source' => $faker->word(),
            'description' => $faker->word(),
            'cover' => array($faker->md5()),
            'attachment' => array($faker->word()),
            'authImages' => array($faker->word()),
            'year' => $faker->year(),
            'status' => $faker->randomElement(IEnableAble::STATUS),
            'crew' => $faker->randomDigit(1),
            'id' => $faker->randomDigit()
        );

        $this->command = new AddJournalCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['description'],
            $this->fakerData['cover'],
            $this->fakerData['attachment'],
            $this->fakerData['authImages'],
            $this->fakerData['year'],
            $this->fakerData['status'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }
}
