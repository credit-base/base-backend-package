<?php
namespace Base\Journal\Command\Journal;

use PHPUnit\Framework\TestCase;

trait JournalTrait
{
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testTitleParameter()
    {
        $this->assertEquals($this->fakerData['title'], $this->command->title);
    }

    public function testSourceParameter()
    {
        $this->assertEquals($this->fakerData['source'], $this->command->source);
    }

    public function testDescriptionParameter()
    {
        $this->assertEquals($this->fakerData['description'], $this->command->description);
    }

    public function testCoverParameter()
    {
        $this->assertEquals($this->fakerData['cover'], $this->command->cover);
    }

    public function testAttachmentParameter()
    {
        $this->assertEquals($this->fakerData['attachment'], $this->command->attachment);
    }

    public function testAuthImagesParameter()
    {
        $this->assertEquals($this->fakerData['authImages'], $this->command->authImages);
    }

    public function testYearParameter()
    {
        $this->assertEquals($this->fakerData['year'], $this->command->year);
    }

    public function testStatusParameter()
    {
        $this->assertEquals($this->fakerData['status'], $this->command->status);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }
}
