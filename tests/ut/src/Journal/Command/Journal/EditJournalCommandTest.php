<?php
namespace Base\Journal\Command\Journal;

use PHPUnit\Framework\TestCase;

use Base\Journal\Model\Journal;
use Base\Journal\Model\Banner;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class EditJournalCommandTest extends TestCase
{
    use JournalTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'title' => $faker->title(),
            'source' => $faker->title(),
            'description' => $faker->word(),
            'cover' => array($faker->md5()),
            'attachment' => array($faker->word()),
            'authImages' => array($faker->word()),
            'year' => $faker->year(),
            'status' => $faker->randomElement(IEnableAble::STATUS),
            'journalId' => $faker->randomDigit(),
            'crew' => $faker->randomDigit(1),
            'id' => $faker->randomDigit()
        );

        $this->command = new EditJournalCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['description'],
            $this->fakerData['cover'],
            $this->fakerData['attachment'],
            $this->fakerData['authImages'],
            $this->fakerData['year'],
            $this->fakerData['status'],
            $this->fakerData['journalId'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }

    public function testJournalIdParameter()
    {
        $this->assertEquals($this->fakerData['journalId'], $this->command->journalId);
    }
}
