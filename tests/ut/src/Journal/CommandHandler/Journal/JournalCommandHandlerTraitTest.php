<?php
namespace Base\Journal\CommandHandler\Journal;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\Journal\Model\Journal;
use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Translator\JournalDbTranslator;
use Base\Journal\Command\Journal\AddJournalCommand;
use Base\Journal\Command\Journal\EnableJournalCommand;
use Base\Journal\Repository\UnAuditedJournalRepository;

use Base\ApplyForm\Model\ApplyInfoCategory;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class JournalCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockJournalCommandHandlerTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetCrewRepository()
    {
        $trait = new MockJournalCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $trait->publicGetCrewRepository()
        );
    }

    public function testGetJournalRepository()
    {
        $trait = new MockJournalCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Journal\Repository\JournalRepository',
            $trait->publicGetJournalRepository()
        );
    }

    public function testGetJournalDbTranslator()
    {
        $trait = new MockJournalCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Journal\Translator\JournalDbTranslator',
            $trait->publicGetJournalDbTranslator()
        );
    }

    public function testGetUnAuditedJournalRepository()
    {
        $trait = new MockJournalCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Journal\Repository\UnAuditedJournalRepository',
            $trait->publicGetUnAuditedJournalRepository()
        );
    }

    public function testGetJournal()
    {
        $trait = new MockJournalCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Journal\Model\Journal',
            $trait->publicGetJournal()
        );
    }

    public function testGetUnAuditedJournal()
    {
        $trait = new MockJournalCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Journal\Model\UnAuditedJournal',
            $trait->publicGetUnAuditedJournal()
        );
    }

    public function testFetchCrew()
    {
        $trait = $this->getMockBuilder(MockJournalCommandHandlerTrait::class)
                 ->setMethods(['getCrewRepository']) ->getMock();

        $id = 1;

        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $trait->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchCrew($id);

        $this->assertEquals($result, $crew);
    }

    public function testFetchJournal()
    {
        $trait = $this->getMockBuilder(MockJournalCommandHandlerTrait::class)
                 ->setMethods(['getJournalRepository']) ->getMock();

        $id = 1;

        $journal = \Base\Journal\Utils\MockFactory::generateJournal($id);

        $repository = $this->prophesize(JournalRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($journal);

        $trait->expects($this->exactly(1))
                         ->method('getJournalRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchJournal($id);

        $this->assertEquals($result, $journal);
    }

    public function testFetchUnAuditedJournal()
    {
        $trait = $this->getMockBuilder(MockJournalCommandHandlerTrait::class)
                 ->setMethods(['getUnAuditedJournalRepository']) ->getMock();
                 
        $id = 1;

        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($id);

        $repository = $this->prophesize(UnAuditedJournalRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedJournal);

        $trait->expects($this->exactly(1))
                         ->method('getUnAuditedJournalRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditedJournal($id);

        $this->assertEquals($result, $unAuditedJournal);
    }

    public function testExecuteAction()
    {
        $trait = $this->getMockBuilder(MockJournalCommandHandlerTrait::class)
                 ->setMethods(['fetchCrew']) ->getMock();
        $id = 1;

        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($id);

        $command = new AddJournalCommand(
            $unAuditedJournal->getTitle(),
            $unAuditedJournal->getSource(),
            $unAuditedJournal->getDescription(),
            $unAuditedJournal->getCover(),
            $unAuditedJournal->getAttachment(),
            $unAuditedJournal->getAuthImages(),
            $unAuditedJournal->getYear(),
            $unAuditedJournal->getStatus(),
            $unAuditedJournal->getCrew()->getId()
        );

        $trait->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($unAuditedJournal->getCrew()->getId())
                         ->willReturn($unAuditedJournal->getCrew());
                         
        $result = $trait->publicExecuteAction($command, $unAuditedJournal);

        $this->assertEquals($result, $unAuditedJournal);
    }

    public function testJournalExecuteAction()
    {
        $trait = $this->getMockBuilder(MockJournalCommandHandlerTrait::class)
                 ->setMethods(['fetchCrew', 'fetchJournal']) ->getMock();
        $id = 1;

        $journal = \Base\Journal\Utils\MockFactory::generateJournal($id);
        $unAuditedJournal = new UnAuditedJournal();

        $command = new EnableJournalCommand(
            $id,
            $journal->getCrew()->getId()
        );

        $trait->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($journal->getCrew()->getId())
                         ->willReturn($journal->getCrew());

        $trait->expects($this->exactly(1))
                         ->method('fetchJournal')
                         ->with($id)
                         ->willReturn($journal);
                         
        $result = $trait->publicJournalExecuteAction($command, $unAuditedJournal);

        $this->assertEquals($result, $unAuditedJournal);
    }

    public function testApplyInfo()
    {
        $trait = $this->getMockBuilder(MockJournalCommandHandlerTrait::class)
                 ->setMethods(['getJournalDbTranslator']) ->getMock();

        $applyInfo = array('applyInfo');
        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal(1);

        $translator = $this->prophesize(JournalDbTranslator::class);
        $translator->objectToArray(Argument::exact($unAuditedJournal))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($applyInfo);

        $trait->expects($this->exactly(1))
                         ->method('getJournalDbTranslator')
                         ->willReturn($translator->reveal());

                         
        $result = $trait->publicApplyInfo($unAuditedJournal);

        $this->assertEquals($result, $applyInfo);
    }
}
