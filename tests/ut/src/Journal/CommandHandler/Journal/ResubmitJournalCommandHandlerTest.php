<?php
namespace Base\Journal\CommandHandler\Journal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Command\Journal\ResubmitJournalCommand;

class ResubmitJournalCommandHandlerTest extends TestCase
{
    private $resubmitCommandHandler;

    private $faker;

    public function setUp()
    {
        $this->resubmitCommandHandler = $this->getMockBuilder(ResubmitJournalCommandHandler::class)
                                     ->setMethods(['fetchUnAuditedJournal', 'executeAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->resubmitCommandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->resubmitCommandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->resubmitCommandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 2;

        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($id);
        $applyInfo = array('applyInfo');

        $command = new ResubmitJournalCommand(
            $unAuditedJournal->getTitle(),
            $unAuditedJournal->getSource(),
            $unAuditedJournal->getDescription(),
            $unAuditedJournal->getCover(),
            $unAuditedJournal->getAttachment(),
            $unAuditedJournal->getAuthImages(),
            $unAuditedJournal->getYear(),
            $unAuditedJournal->getStatus(),
            $unAuditedJournal->getCrew()->getId()
        );

        $unAuditedJournal = $this->prophesize(UnAuditedJournal::class);
    
        $this->resubmitCommandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $this->resubmitCommandHandler->expects($this->exactly(1))
             ->method('executeAction')
             ->willReturn($unAuditedJournal->reveal());

        $unAuditedJournal->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);

        $unAuditedJournal->resubmit()->shouldBeCalledTimes(1)->willReturn($result);

        $this->resubmitCommandHandler->expects($this->exactly(1))
            ->method('fetchUnAuditedJournal')
            ->willReturn($unAuditedJournal->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->resubmitCommandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->resubmitCommandHandler->execute($command);

        $this->assertFalse($result);
    }
}
