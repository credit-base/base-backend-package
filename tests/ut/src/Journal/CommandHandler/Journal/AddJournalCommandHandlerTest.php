<?php
namespace Base\Journal\CommandHandler\Journal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Command\Journal\AddJournalCommand;

class AddJournalCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddJournalCommandHandler::class)
                                     ->setMethods(['getUnAuditedJournal', 'executeAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($id);
        $applyInfo = array('applyInfo');

        $command = new AddJournalCommand(
            $unAuditedJournal->getTitle(),
            $unAuditedJournal->getSource(),
            $unAuditedJournal->getDescription(),
            $unAuditedJournal->getCover(),
            $unAuditedJournal->getAttachment(),
            $unAuditedJournal->getAuthImages(),
            $unAuditedJournal->getYear(),
            $unAuditedJournal->getStatus(),
            $unAuditedJournal->getCrew()->getId()
        );

        $unAuditedJournal = $this->prophesize(UnAuditedJournal::class);
    
        $this->commandHandler->expects($this->exactly(1))
             ->method('executeAction')
             ->willReturn($unAuditedJournal->reveal());

        $this->commandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $unAuditedJournal->setOperationType(Argument::exact($command->operationType))
                       ->shouldBeCalledTimes(1);
        $unAuditedJournal->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);

        $unAuditedJournal->add()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $unAuditedJournal->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
            ->method('getUnAuditedJournal')
            ->willReturn($unAuditedJournal->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
