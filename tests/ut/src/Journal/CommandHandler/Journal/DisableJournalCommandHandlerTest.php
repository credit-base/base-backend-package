<?php
namespace Base\Journal\CommandHandler\Journal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Journal\Command\Journal\DisableJournalCommand;

use Base\Journal\Model\Journal;

class DisableJournalCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDisableJournalCommandHandler::class)
        ->setMethods(['fetchJournal'])
        ->getMock();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $journal = \Base\Journal\Utils\MockFactory::generateJournal($id);

        $this->stub->expects($this->once())
             ->method('fetchJournal')
             ->with($id)
             ->willReturn($journal);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $journal);
    }

    public function testExecuteAction()
    {
        $stub = $this->getMockBuilder(MockDisableJournalCommandHandler::class)
                ->setMethods(['fetchIEnableObject', 'fetchCrew'])
                ->getMock();

        $id = 1;
        $journal = \Base\Journal\Utils\MockFactory::generateJournal($id);
        $crew = $journal->getCrew();

        $command = new DisableJournalCommand($crew->getId(), $id);

        $stub->expects($this->once())
             ->method('fetchCrew')
             ->with($crew->getId())
             ->willReturn($crew);

        $journal = $this->prophesize(Journal::class);

        $journal->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $journal->setPublishUserGroup(Argument::exact($crew->getUserGroup()))->shouldBeCalledTimes(1);

        $journal->disable()->shouldBeCalledTimes(1)->willReturn(true);

        $stub->expects($this->exactly(1))->method('fetchIEnableObject')->willReturn($journal->reveal());

        $result = $stub->executeAction($command);

        $this->assertTrue($result);
    }
}
