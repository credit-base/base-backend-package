<?php
namespace Base\Journal\CommandHandler\Journal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Command\Journal\EnableJournalCommand;

class EnableJournalCommandHandlerTest extends TestCase
{
    private $enableCommandHandler;

    private $faker;

    public function setUp()
    {
        $this->enableCommandHandler = $this->getMockBuilder(EnableJournalCommandHandler::class)
                                     ->setMethods(['getUnAuditedJournal', 'journalExecuteAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->enableCommandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->enableCommandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->enableCommandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 2;

        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($id);
        $applyInfo = array('applyInfo');

        $command = new EnableJournalCommand(
            $id,
            $unAuditedJournal->getCrew()->getId()
        );

        $unAuditedJournal = $this->prophesize(UnAuditedJournal::class);
    
        $this->enableCommandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $this->enableCommandHandler->expects($this->exactly(1))
             ->method('journalExecuteAction')
             ->willReturn($unAuditedJournal->reveal());

        $unAuditedJournal->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);

        $unAuditedJournal->enable()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $unAuditedJournal->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->enableCommandHandler->expects($this->exactly(1))
            ->method('getUnAuditedJournal')
            ->willReturn($unAuditedJournal->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->enableCommandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->enableCommandHandler->execute($command);

        $this->assertFalse($result);
    }
}
