<?php
namespace Base\Journal\CommandHandler\Journal;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Base\Journal\Command\Journal\AddJournalCommand;
use Base\Journal\Command\Journal\DisableJournalCommand;
use Base\Journal\Command\Journal\EditJournalCommand;
use Base\Journal\Command\Journal\EnableJournalCommand;
use Base\Journal\Command\Journal\ResubmitJournalCommand;

class JournalCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new JournalCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddJournalCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddJournalCommand(
                $this->faker->title(),
                $this->faker->title(),
                $this->faker->word(),
                array($this->faker->title()),
                array($this->faker->title()),
                array($this->faker->title()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Journal\CommandHandler\Journal\AddJournalCommandHandler',
            $commandHandler
        );
    }

    public function testEditJournalCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditJournalCommand(
                $this->faker->title(),
                $this->faker->word(),
                $this->faker->word(),
                array($this->faker->title()),
                array($this->faker->title()),
                array($this->faker->title()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Journal\CommandHandler\Journal\EditJournalCommandHandler',
            $commandHandler
        );
    }


    public function testDisableJournalCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableJournalCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Journal\CommandHandler\Journal\DisableJournalCommandHandler',
            $commandHandler
        );
    }

    public function testEnableJournalCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EnableJournalCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Journal\CommandHandler\Journal\EnableJournalCommandHandler',
            $commandHandler
        );
    }

    public function testResubmitJournalCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResubmitJournalCommand(
                $this->faker->title(),
                $this->faker->word(),
                $this->faker->word(),
                array($this->faker->title()),
                array($this->faker->title()),
                array($this->faker->md5()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Journal\CommandHandler\Journal\ResubmitJournalCommandHandler',
            $commandHandler
        );
    }
}
