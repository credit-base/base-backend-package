<?php
namespace Base\Journal\CommandHandler\Journal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Command\Journal\EditJournalCommand;

class EditJournalCommandHandlerTest extends TestCase
{
    private $editCommandHandler;

    private $faker;

    public function setUp()
    {
        $this->editCommandHandler = $this->getMockBuilder(EditJournalCommandHandler::class)
                                     ->setMethods(['getUnAuditedJournal', 'executeAction', 'applyInfo'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->editCommandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->editCommandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->editCommandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 2;

        $unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal($id);
        $applyInfo = array('applyInfo');

        $command = new EditJournalCommand(
            $unAuditedJournal->getTitle(),
            $unAuditedJournal->getSource(),
            $unAuditedJournal->getDescription(),
            $unAuditedJournal->getCover(),
            $unAuditedJournal->getAttachment(),
            $unAuditedJournal->getAuthImages(),
            $unAuditedJournal->getYear(),
            $unAuditedJournal->getStatus(),
            $id,
            $unAuditedJournal->getCrew()->getId()
        );

        $unAuditedJournal = $this->prophesize(UnAuditedJournal::class);
    
        $this->editCommandHandler->expects($this->exactly(1))
             ->method('applyInfo')
             ->willReturn($applyInfo);

        $this->editCommandHandler->expects($this->exactly(1))
             ->method('executeAction')
             ->willReturn($unAuditedJournal->reveal());

        $unAuditedJournal->setOperationType(Argument::exact($command->operationType))
                       ->shouldBeCalledTimes(1);
        $unAuditedJournal->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);
        $unAuditedJournal->setId(Argument::exact($command->journalId))->shouldBeCalledTimes(1);

        $unAuditedJournal->edit()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $unAuditedJournal->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->editCommandHandler->expects($this->exactly(1))
            ->method('getUnAuditedJournal')
            ->willReturn($unAuditedJournal->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->editCommandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->editCommandHandler->execute($command);

        $this->assertFalse($result);
    }
}
