<?php
namespace Base\Journal\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class UnAuditedJournalSchemaTest extends TestCase
{
    private $unAuditedJournalSchema;

    private $unAuditedJournal;

    public function setUp()
    {
        $this->unAuditedJournalSchema = new UnAuditedJournalSchema(new Factory());

        $this->unAuditedJournal = \Base\Journal\Utils\MockFactory::generateUnAuditedJournal(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->unAuditedJournalSchema);
        unset($this->unAuditedJournal);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->unAuditedJournalSchema);
    }

    public function testGetId()
    {
        $result = $this->unAuditedJournalSchema->getId($this->unAuditedJournal);

        $this->assertEquals($result, $this->unAuditedJournal->getApplyId());
    }

    public function testGetAttributes()
    {
        $result = $this->unAuditedJournalSchema->getAttributes($this->unAuditedJournal);

        $this->assertEquals($result['title'], $this->unAuditedJournal->getTitle());
        $this->assertEquals($result['source'], $this->unAuditedJournal->getSource());
        $this->assertEquals($result['description'], $this->unAuditedJournal->getDescription());
        $this->assertEquals($result['cover'], $this->unAuditedJournal->getCover());
        $this->assertEquals($result['attachment'], $this->unAuditedJournal->getAttachment());
        $this->assertEquals($result['authImages'], $this->unAuditedJournal->getAuthImages());
        $this->assertEquals($result['year'], $this->unAuditedJournal->getYear());
        $this->assertEquals($result['status'], $this->unAuditedJournal->getStatus());
        $this->assertEquals($result['createTime'], $this->unAuditedJournal->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->unAuditedJournal->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->unAuditedJournal->getStatusTime());
        $this->assertEquals(
            $result['applyInfoCategory'],
            $this->unAuditedJournal->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals(
            $result['applyInfoType'],
            $this->unAuditedJournal->getApplyInfoCategory()->getType()
        );
        $this->assertEquals($result['applyStatus'], $this->unAuditedJournal->getApplyStatus());
        $this->assertEquals($result['rejectReason'], $this->unAuditedJournal->getRejectReason());
        $this->assertEquals($result['operationType'], $this->unAuditedJournal->getOperationType());
        $this->assertEquals($result['journalId'], $this->unAuditedJournal->getId());
    }

    public function testGetRelationships()
    {
        $result = $this->unAuditedJournalSchema->getRelationships($this->unAuditedJournal, 0, array());

        $this->assertEquals($result['publishUserGroup'], ['data' => $this->unAuditedJournal->getPublishUserGroup()]);
        $this->assertEquals($result['crew'], ['data' => $this->unAuditedJournal->getCrew()]);
        $this->assertEquals($result['relation'], ['data' => $this->unAuditedJournal->getRelation()]);
        $this->assertEquals($result['applyCrew'], ['data' => $this->unAuditedJournal->getApplyCrew()]);
        $this->assertEquals($result['applyUserGroup'], ['data' => $this->unAuditedJournal->getApplyUserGroup()]);
    }
}
