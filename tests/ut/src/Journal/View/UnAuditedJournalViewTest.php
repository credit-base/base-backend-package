<?php
namespace Base\Journal\View;

use PHPUnit\Framework\TestCase;

use Base\Journal\Model\UnAuditedJournal;

class UnAuditedJournalViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $journal = new UnAuditedJournalView(new UnAuditedJournal());
        $this->assertInstanceof('Base\Common\View\CommonView', $journal);
    }
}
