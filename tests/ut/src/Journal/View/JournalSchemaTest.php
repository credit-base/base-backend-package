<?php
namespace Base\Journal\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class JournalSchemaTest extends TestCase
{
    private $journalSchema;

    private $journal;

    public function setUp()
    {
        $this->journalSchema = new JournalSchema(new Factory());

        $this->journal = \Base\Journal\Utils\MockFactory::generateJournal(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->journalSchema);
        unset($this->journal);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->journalSchema);
    }

    public function testGetId()
    {
        $result = $this->journalSchema->getId($this->journal);

        $this->assertEquals($result, $this->journal->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->journalSchema->getAttributes($this->journal);

        $this->assertEquals($result['title'], $this->journal->getTitle());
        $this->assertEquals($result['source'], $this->journal->getSource());
        $this->assertEquals($result['description'], $this->journal->getDescription());
        $this->assertEquals($result['cover'], $this->journal->getCover());
        $this->assertEquals($result['attachment'], $this->journal->getAttachment());
        $this->assertEquals($result['authImages'], $this->journal->getAuthImages());
        $this->assertEquals($result['year'], $this->journal->getYear());
        $this->assertEquals($result['status'], $this->journal->getStatus());
        $this->assertEquals($result['createTime'], $this->journal->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->journal->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->journal->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->journalSchema->getRelationships($this->journal, 0, array());

        $this->assertEquals($result['publishUserGroup'], ['data' => $this->journal->getPublishUserGroup()]);
        $this->assertEquals($result['crew'], ['data' => $this->journal->getCrew()]);
    }
}
