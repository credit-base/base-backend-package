<?php
namespace Base\Journal\View;

use PHPUnit\Framework\TestCase;

use Base\Journal\Model\Journal;

class JournalViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $journal = new JournalView(new Journal());
        $this->assertInstanceof('Base\Common\View\CommonView', $journal);
    }
}
