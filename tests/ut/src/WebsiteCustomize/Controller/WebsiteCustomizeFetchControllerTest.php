<?php
namespace Base\WebsiteCustomize\Controller;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\View\WebsiteCustomizeView;
use Base\WebsiteCustomize\Model\NullWebsiteCustomize;
use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeFragmentCacheQueryFactory;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeHomePageFragmentCacheQuery;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WebsiteCustomizeFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(WebsiteCustomizeFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockWebsiteCustomizeFetchController();

        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter',
            $controller->getRepository()
        );
    }

    public function testGetTranslator()
    {
        $controller = new MockWebsiteCustomizeFetchController();

        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator',
            $controller->getTranslator()
        );
    }

    public function testGetFragmentCacheQueryFactory()
    {
        $controller = new MockWebsiteCustomizeFetchController();

        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeFragmentCacheQueryFactory',
            $controller->getFragmentCacheQueryFactory()
        );
    }

    public function testGetFragmentCacheQuery()
    {
        $controller = new MockWebsiteCustomizeFetchController();

        $this->assertInstanceOf(
            'Marmot\Framework\Query\FragmentCacheQuery',
            $controller->getFragmentCacheQuery(WebsiteCustomize::CATEGORY['HOME_PAGE'])
        );
    }

    public function testGenerateView()
    {
        $controller = new MockWebsiteCustomizeFetchController();

        $this->assertInstanceOf(
            'Base\WebsiteCustomize\View\WebsiteCustomizeView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockWebsiteCustomizeFetchController();

        $this->assertEquals(
            'websiteCustomizes',
            $controller->getResourceName()
        );
    }

    public function testHomePageSuccess()
    {
        $controller = $this->getMockBuilder(WebsiteCustomizeFetchController::class)
                           ->setMethods(
                               ['getFragmentCacheQuery', 'getTranslator', 'render']
                           )
                           ->getMock();

        $data = array('data');
        $websiteCustomize = new WebsiteCustomize(1);

        $cache = $this->prophesize(WebsiteCustomizeHomePageFragmentCacheQuery::class);
        $cache->get()->shouldBeCalledTimes(1)->willReturn($data);
        $controller->expects($this->exactly(1))
                         ->method('getFragmentCacheQuery')
                         ->willReturn($cache->reveal());

        $translator = $this->prophesize(WebsiteCustomizeDbTranslator::class);
        $translator->arrayToObject($data)->shouldBeCalledTimes(1)->willReturn($websiteCustomize);
        $controller->expects($this->exactly(1))
                         ->method('getTranslator')
                         ->willReturn($translator->reveal());

        $controller->expects($this->exactly(1))
                         ->method('render');

        //验证
        $result = $controller->homePage();
        $this->assertTrue($result);
    }

    public function testHomePageFalse()
    {
        $controller = $this->getMockBuilder(WebsiteCustomizeFetchController::class)
                           ->setMethods(
                               ['getFragmentCacheQuery', 'getTranslator', 'displayError']
                           )
                           ->getMock();

        $data = array();

        $cache = $this->prophesize(WebsiteCustomizeHomePageFragmentCacheQuery::class);
        $cache->get()->shouldBeCalledTimes(1)->willReturn($data);
        $controller->expects($this->exactly(1))
                         ->method('getFragmentCacheQuery')
                         ->willReturn($cache->reveal());

        $translator = $this->prophesize(WebsiteCustomizeDbTranslator::class);
        $translator->arrayToObject($data)->shouldBeCalledTimes(1)->willReturn(NullWebsiteCustomize::getInstance());
        $controller->expects($this->exactly(1))
                         ->method('getTranslator')
                         ->willReturn($translator->reveal());

        $controller->expects($this->exactly(1))
                         ->method('displayError');

        //验证
        $result = $controller->homePage();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
