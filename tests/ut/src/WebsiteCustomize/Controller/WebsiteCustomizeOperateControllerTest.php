<?php
namespace Base\WebsiteCustomize\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;
use Base\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;

class WebsiteCustomizeOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new WebsiteCustomizeOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    private function websiteCustomizeData() : array
    {
        return array(
            "type"=>"websiteCustomizes",
            "attributes"=>array(
                "category"=>1,
                "content"=>array('content'),
                'status' => 0
            ),
            "relationships"=>array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>1)
                    )
                )
            )
        );
    }

    public function testAdd()
    {
        $controller = $this->getMockBuilder(WebsiteCustomizeOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateAddScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $command = $this->initAdd($controller, true);

        $websiteCustomize = \Base\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize($command->id);

        $repository = $this->prophesize(WebsiteCustomizeRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($websiteCustomize);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        $result = $controller->add();
        $this->assertTrue($result);
    }

    public function testAddFail()
    {
        $controller = $this->getMockBuilder(WebsiteCustomizeOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateAddScenario',
                    'getCommandBus',
                    'getRepository',
                    'displayError'
                ]
            )->getMock();

        $this->initAdd($controller, false);

        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $controller->add();
        $this->assertFalse($result);
    }

    protected function initAdd(WebsiteCustomizeOperateController $controller, bool $result)
    {
        // mock 请求参数
        $data = $this->websiteCustomizeData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $category = $attributes['category'];
        $content = $attributes['content'];
        $status = $attributes['status'];

        $crew = $relationships['crew']['data'][0]['id'];

        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateAddScenario')
            ->willReturn(true);

        $command = new AddWebsiteCustomizeCommand(
            $content,
            $category,
            $status,
            $crew
        );

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
            
        return $command;
    }

    public function testEdit()
    {
        $id = 1;
        $result = $this->controller ->edit($id);
        $this->assertFalse($result);
    }

    private function initialPublish($result)
    {
        $this->stub = $this->getMockBuilder(WebsiteCustomizeOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new PublishWebsiteCustomizeCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->stub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testPublishSuccess()
    {
        $command = $this->initialPublish(true);

        $websiteCustomize = \Base\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize($command->id);

        $repository = $this->prophesize(WebsiteCustomizeRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($websiteCustomize);
        $this->stub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->stub->expects($this->exactly(1))
        ->method('render');

        $result = $this->stub->publish($command->id);
        $this->assertTrue($result);
    }

    public function testPublishFailure()
    {
        $command = $this->initialPublish(false);

        $this->stub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->stub->publish($command->id);
        $this->assertFalse($result);
    }
}
