<?php
namespace Base\WebsiteCustomize\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\WebsiteCustomize\WidgetRule\WebsiteCustomizeWidgetRule;

class WebsiteCustomizeControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockWebsiteCustomizeControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetWebsiteCustomizeWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\WidgetRule\WebsiteCustomizeWidgetRule',
            $this->trait->getWebsiteCustomizeWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    public function testValidateAddScenario()
    {
        $controller = $this->getMockBuilder(MockWebsiteCustomizeControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule', 'getWebsiteCustomizeWidgetRule']) ->getMock();

        $category = 1;
        $content = array('content');
        $status = 1;
        $crew = 1;

        $websiteCustomizeWidgetRule = $this->prophesize(WebsiteCustomizeWidgetRule::class);
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $websiteCustomizeWidgetRule->category(Argument::exact($category))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $websiteCustomizeWidgetRule->content(Argument::exact($category), Argument::exact($content))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $websiteCustomizeWidgetRule->status(Argument::exact($status))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($crew), Argument::exact('crewId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(3))
            ->method('getWebsiteCustomizeWidgetRule')
            ->willReturn($websiteCustomizeWidgetRule->reveal());
        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateAddScenarioPublic($category, $content, $status, $crew);
        
        $this->assertTrue($result);
    }
}
