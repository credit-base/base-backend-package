<?php
namespace Base\WebsiteCustomize\Utils;

trait WebsiteCustomizeUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $websiteCustomize
    ) {
        $this->assertEquals($expectedArray['website_customize_id'], $websiteCustomize->getId());

        $this->assertEquals($expectedArray['category'], $websiteCustomize->getCategory());
        $this->contentEquals($expectedArray, $websiteCustomize);
        $this->assertEquals($expectedArray['crew_id'], $websiteCustomize->getCrew()->getId());
        $this->assertEquals($expectedArray['version'], $websiteCustomize->getVersion());
        $this->assertEquals($expectedArray['status'], $websiteCustomize->getStatus());
        $this->assertEquals($expectedArray['status'], $websiteCustomize->getStatus());
        $this->assertEquals($expectedArray['create_time'], $websiteCustomize->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $websiteCustomize->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $websiteCustomize->getStatusTime());
    }

    private function contentEquals($expectedArray, $websiteCustomize)
    {
        $content = array();

        if (is_string($expectedArray['content'])) {
            $content = json_decode($expectedArray['content'], true);
        }
        if (is_array($expectedArray['content'])) {
            $content = $expectedArray['content'];
        }

        $this->assertEquals($content, $websiteCustomize->getContent());
    }
}
