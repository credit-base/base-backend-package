<?php
namespace Base\WebsiteCustomize\Utils;

use Base\Common\Model\IApproveAble;

use Base\WebsiteCustomize\Model\WebsiteCustomize;

class MockFactory
{
    public static function generateWebsiteCustomize(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WebsiteCustomize {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $websiteCustomize = new WebsiteCustomize($id);
        $websiteCustomize->setId($id);

        self::generateCategory($websiteCustomize, $faker, $value);
        //content
        $content = isset($value['content']) ? $value['content'] : array($faker->word());
        $websiteCustomize->setContent($content);

        self::generateCrew($websiteCustomize, $faker, $value);
        self::generateStatus($websiteCustomize, $faker, $value);
        
        $websiteCustomize->setCreateTime($faker->unixTime());
        $websiteCustomize->setUpdateTime($faker->unixTime());
        $websiteCustomize->setStatusTime($faker->unixTime());
        //version
        $websiteCustomize->generateVersion();
        
        return $websiteCustomize;
    }

    protected static function generateCategory($websiteCustomize, $faker, $value) : void
    {
        //category
        $category = isset($value['category']) ?
        $value['category'] :
        $faker->randomElement(
            WebsiteCustomize::CATEGORY
        );

        $websiteCustomize->setCategory($category);
    }

    protected static function generateCrew($websiteCustomize, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $websiteCustomize->setCrew($crew);
    }

    protected static function generateStatus($websiteCustomize, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                WebsiteCustomize::STATUS
            );
        
        $websiteCustomize->setStatus($status);
    }
}
