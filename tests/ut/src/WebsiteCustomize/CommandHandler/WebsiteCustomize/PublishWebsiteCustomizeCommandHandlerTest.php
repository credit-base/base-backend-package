<?php
namespace Base\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;

class PublishWebsiteCustomizeCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(PublishWebsiteCustomizeCommandHandler::class)
                                     ->setMethods(['fetchWebsiteCustomize'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $command = new PublishWebsiteCustomizeCommand(
            $this->faker->randomNumber()
        );

        $websiteCustomize = $this->prophesize(WebsiteCustomize::class);
        $websiteCustomize->publish()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
             ->method('fetchWebsiteCustomize')
             ->with($command->id)
             ->willReturn($websiteCustomize->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}
