<?php
namespace Base\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Base\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;
use Base\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;

class WebsiteCustomizeCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new WebsiteCustomizeCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddWebsiteCustomizeCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddWebsiteCustomizeCommand(
                array($this->faker->md5()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\CommandHandler\WebsiteCustomize\AddWebsiteCustomizeCommandHandler',
            $commandHandler
        );
    }

    public function testPublishWebsiteCustomizeCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new PublishWebsiteCustomizeCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\CommandHandler\WebsiteCustomize\PublishWebsiteCustomizeCommandHandler',
            $commandHandler
        );
    }
}
