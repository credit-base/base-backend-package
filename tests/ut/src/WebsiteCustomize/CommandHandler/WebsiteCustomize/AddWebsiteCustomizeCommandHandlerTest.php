<?php
namespace Base\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;

class AddWebsiteCustomizeCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddWebsiteCustomizeCommandHandler::class)
                                     ->setMethods(['fetchCrew', 'getWebsiteCustomize'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $command = new AddWebsiteCustomizeCommand(
            array('content'),
            1,
            1,
            1
        );
        
        $expectId = 10;

        $crew = \Base\Crew\Utils\MockFactory::generateCrew($command->crew);

        $this->commandHandler->expects($this->exactly(1))
             ->method('fetchCrew')
             ->with($command->crew)
             ->willReturn($crew);

        $websiteCustomize = $this->prophesize(WebsiteCustomize::class);
        $websiteCustomize->setCategory($command->category)->shouldBeCalledTimes(1);
        $websiteCustomize->setContent($command->content)->shouldBeCalledTimes(1);
        $websiteCustomize->setStatus($command->status)->shouldBeCalledTimes(1);
        $websiteCustomize->setCrew($crew)->shouldBeCalledTimes(1);
        $websiteCustomize->generateVersion()->shouldBeCalledTimes(1);
        $websiteCustomize->add()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $websiteCustomize->getId()->shouldBeCalledTimes(1)->willReturn($expectId);
        }

        $this->commandHandler->expects($this->exactly(1))
            ->method('getWebsiteCustomize')
            ->willReturn($websiteCustomize->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
