<?php
namespace Base\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

class WebsiteCustomizeCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockWebsiteCustomizeCommandHandlerTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetCrewRepository()
    {
        $trait = new MockWebsiteCustomizeCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $trait->publicGetCrewRepository()
        );
    }

    public function testGetWebsiteCustomizeRepository()
    {
        $trait = new MockWebsiteCustomizeCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository',
            $trait->publicGetWebsiteCustomizeRepository()
        );
    }

    public function testGetWebsiteCustomize()
    {
        $trait = new MockWebsiteCustomizeCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Model\WebsiteCustomize',
            $trait->publicGetWebsiteCustomize()
        );
    }

    public function testFetchCrew()
    {
        $trait = $this->getMockBuilder(MockWebsiteCustomizeCommandHandlerTrait::class)
                 ->setMethods(['getCrewRepository']) ->getMock();

        $id = 1;

        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $trait->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchCrew($id);

        $this->assertEquals($result, $crew);
    }

    public function testFetchWebsiteCustomize()
    {
        $trait = $this->getMockBuilder(MockWebsiteCustomizeCommandHandlerTrait::class)
                 ->setMethods(['getWebsiteCustomizeRepository']) ->getMock();

        $id = 1;

        $websiteCustomize = \Base\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize($id);

        $repository = $this->prophesize(WebsiteCustomizeRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($websiteCustomize);

        $trait->expects($this->exactly(1))
                         ->method('getWebsiteCustomizeRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchWebsiteCustomize($id);

        $this->assertEquals($result, $websiteCustomize);
    }
}
