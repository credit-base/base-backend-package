<?php
namespace Base\WebsiteCustomize\Translator;

use PHPUnit\Framework\TestCase;

use Base\WebsiteCustomize\Utils\MockFactory;
use Base\WebsiteCustomize\Utils\WebsiteCustomizeUtils;

class WebsiteCustomizeDbTranslatorTest extends TestCase
{
    use WebsiteCustomizeUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WebsiteCustomizeDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\WebsiteCustomize\Model\NullWebsiteCustomize', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $websiteCustomize = MockFactory::generateWebsiteCustomize(1);

        $expression['website_customize_id'] = $websiteCustomize->getId();

        $expression['category'] = $websiteCustomize->getCategory();
        $expression['version'] = $websiteCustomize->getVersion();
        $expression['content'] = $websiteCustomize->getContent();
        $expression['crew_id'] = $websiteCustomize->getCrew()->getId();
        $expression['status'] = $websiteCustomize->getStatus();
        $expression['status_time'] = $websiteCustomize->getStatusTime();
        $expression['create_time'] = $websiteCustomize->getCreateTime();
        $expression['update_time'] = $websiteCustomize->getUpdateTime();

        $websiteCustomize = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\WebsiteCustomize\Model\WebsiteCustomize', $websiteCustomize);
        $this->compareArrayAndObject($expression, $websiteCustomize);
    }

    public function testArrayToObjectContent()
    {
        $websiteCustomize = MockFactory::generateWebsiteCustomize(1);

        $expression['website_customize_id'] = $websiteCustomize->getId();

        $expression['category'] = $websiteCustomize->getCategory();
        $expression['version'] = $websiteCustomize->getVersion();
        $expression['content'] =  json_encode($websiteCustomize->getContent());
        $expression['crew_id'] = $websiteCustomize->getCrew()->getId();
        $expression['status'] = $websiteCustomize->getStatus();
        $expression['status_time'] = $websiteCustomize->getStatusTime();
        $expression['create_time'] = $websiteCustomize->getCreateTime();
        $expression['update_time'] = $websiteCustomize->getUpdateTime();

        $websiteCustomize = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\WebsiteCustomize\Model\WebsiteCustomize', $websiteCustomize);
        $this->compareArrayAndObject($expression, $websiteCustomize);
    }

    public function testObjectToArray()
    {
        $websiteCustomize = MockFactory::generateWebsiteCustomize(1);

        $expression = $this->translator->objectToArray($websiteCustomize);

        $this->compareArrayAndObject($expression, $websiteCustomize);
    }
}
