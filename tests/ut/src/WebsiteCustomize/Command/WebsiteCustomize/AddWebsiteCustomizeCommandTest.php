<?php
namespace Base\WebsiteCustomize\Command\WebsiteCustomize;

use PHPUnit\Framework\TestCase;

class AddWebsiteCustomizeCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'content' => array($faker->word()),
            'category' => $faker->randomDigit(),
            'status' => $faker->randomDigit(),
            'crew' => $faker->randomDigit(),
            'id' => $faker->randomDigit()
        );

        $this->command = new AddWebsiteCustomizeCommand(
            $this->fakerData['content'],
            $this->fakerData['category'],
            $this->fakerData['status'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }

    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testCategoryParameter()
    {
        $this->assertEquals($this->fakerData['category'], $this->command->category);
    }

    public function testContentParameter()
    {
        $this->assertEquals($this->fakerData['content'], $this->command->content);
    }

    public function testStatusParameter()
    {
        $this->assertEquals($this->fakerData['status'], $this->command->status);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }
}
