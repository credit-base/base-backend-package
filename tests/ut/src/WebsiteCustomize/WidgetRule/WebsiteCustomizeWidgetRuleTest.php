<?php
namespace Base\WebsiteCustomize\WidgetRule;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Common\Utils\StringGenerate;

use Base\Common\Model\IEnableAble;

use Base\WebsiteCustomize\Model\WebsiteCustomize;

/**
 * @SuppressWarnings(PHPMD)
 */
class WebsiteCustomizeWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new MockWebsiteCustomizeWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    /**
     * @dataProvider validateContentImageProvider
     */
    public function testValidateContentImage($type, $actual, $expected)
    {
        $result = $this->widgetRule->validateContentImage($type, $actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateContentImageProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(WebsiteCustomizeWidgetRule::CONTENT_IMAGE_TYPE['FORMAT_ONE'], $faker->word, false),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                array('name'=>'name'),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_IMAGE_TYPE['FORMAT_TWO']+10,
                array('name'=>$faker->word, 'identify'=>'1.jpg'),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                array('name'=>$faker->word, 'identify'=>'1.jpg'),
                true
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                array('name'=>$faker->word, 'identify'=>'1.pdf'),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
                array('name'=>$faker->word, 'identify'=>'1.png'),
                true
            ),
        );
    }

    //validateContentStatus -- start
    /**
     * @dataProvider validateContentStatusProvider
     */
    public function testValidateContentStatus($actual, $expected)
    {
        $result = $this->widgetRule->validateContentStatus($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateContentStatusProvider()
    {
        return array(
            array('', false),
            array(IEnableAble::STATUS['DISABLED'], true),
            array(IEnableAble::STATUS['ENABLED'], true),
            array('status', false),
            array(999, false),
        );
    }
    //validateContentStatus -- end

    //validateContentType -- start
    /**
     * @dataProvider validateContentTypeProvider
     */
    public function testValidateContentType($actual, $expected)
    {
        $result = $this->widgetRule->validateContentType($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateContentTypeProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::TYPE['PV'], true),
            array(WebsiteCustomizeWidgetRule::TYPE['FONT'], true),
            array('status', false),
            array(999, false),
        );
    }
    //validateContentType -- end

    /**
     * @dataProvider validateContentNameProvider
     */
    public function testValidateContentName($type, $actual, $expected)
    {
        $result = $this->widgetRule->validateContentName($type, $actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateContentNameProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_ONE'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH-1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_ONE'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH+1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_ONE'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH),
                true
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_TWO'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MIN_LENGTH-1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_TWO'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH+1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_TWO'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH),
                true
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_THREE'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_THREE_NAME_MIN_LENGTH-1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_THREE'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_THREE_NAME_MAX_LENGTH+1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_THREE'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_THREE_NAME_MAX_LENGTH),
                true
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH-1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH+1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH),
                true
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_NAME_TYPE['FORMAT_THREE']+10,
                $faker->word,
                false
            ),
        );
    }


    /**
     * @dataProvider validateContentUrlProvider
     */
    public function testValidateContentUrl($type, $actual, $expected)
    {
        $result = $this->widgetRule->validateContentUrl($type, $actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateContentUrlProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(
                WebsiteCustomizeWidgetRule::CONTENT_URL_TYPE['FORMAT_ONE'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_URL_TYPE['FORMAT_ONE'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                true
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_URL_TYPE['FORMAT_TWO'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH-1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_URL_TYPE['FORMAT_TWO'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MAX_LENGTH+1),
                false
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_URL_TYPE['FORMAT_TWO'],
                StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MAX_LENGTH),
                true
            ),
            array(
                WebsiteCustomizeWidgetRule::CONTENT_URL_TYPE['FORMAT_TWO']+10,
                $faker->word,
                false
            ),
        );
    }

    /**
     * @dataProvider contentProvider
     */
    public function testContent($category, $actual, $expected)
    {
        $widgetRule = $this->getMockBuilder(MockWebsiteCustomizeWidgetRule::class)
                        ->setMethods(['validateHomePageContent'])
                        ->getMock();

        if ($category == WebsiteCustomize::CATEGORY['HOME_PAGE']) {
            $widgetRule->expects($this->any())->method('validateHomePageContent')->willReturn(true);
        }

        $result = $widgetRule->content($category, $actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function contentProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(
                WebsiteCustomize::CATEGORY['HOME_PAGE'],
                'content',
                false
            ),
            array(
                WebsiteCustomize::CATEGORY['HOME_PAGE'],
                array('content'),
                true
            ),
            array(
                WebsiteCustomize::CATEGORY['HOME_PAGE']+100,
                array('content'),
                false
            ),
        );
    }

    public function testValidateHomePageContent()
    {
        $widgetRule = $this->getMockBuilder(MockWebsiteCustomizeWidgetRule::class)
                        ->setMethods(['homePageContentKeysExist', 'homePageContentKeysRequired', 'homePageFormat'])
                        ->getMock();

        $widgetRule->expects($this->exactly(1))->method('homePageContentKeysExist')->willReturn(true);
        $widgetRule->expects($this->exactly(1))->method('homePageContentKeysRequired')->willReturn(true);
        $widgetRule->expects($this->exactly(1))->method('homePageFormat')->willReturn(true);

        $result = $widgetRule->validateHomePageContent(array('content'));

        $this->assertTrue($result);
    }

    //homePageContentKeysExist -- start
    /**
     * @dataProvider homePageContentKeysExistProvider
     */
    public function testHomePageContentKeysExist($actual, $expected)
    {
        $result = $this->widgetRule->homePageContentKeysExist($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function homePageContentKeysExistProvider()
    {
        return array(
            array(array('nav'=>array('nav')), true),
            array(array('test'=>'test'), false)
        );
    }
    //homePageContentKeysExist -- end

    //homePageContentKeysRequired -- start
    /**
     * @dataProvider homePageContentKeysRequiredProvider
     */
    public function testHomePageContentKeysRequired($actual, $expected)
    {
        $result = $this->widgetRule->homePageContentKeysRequired($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function homePageContentKeysRequiredProvider()
    {
        return array(
            array(array('nav'=>'nav'), false),
            array(array(
                'memorialStatus' => 1,
                'theme' => array('theme'),
                'headerBarLeft' => array('headerBarLeft'),
                'headerBarRight' => array('headerBarRight'),
                'headerBg' => array('headerBg'),
                'logo' => array('logo'),
                'headerSearch' => array('headerSearch'),
                'nav' => array('nav'),
                'rightToolBar' => array('rightToolBar'),
                'footerNav' => array('footerNav'),
                'footerThree' => array('footerThree')
            ), true)
        );
    }
    //homePageContentKeysRequired -- end

    public function testHomePageFormatFail()
    {
        $widgetRule = $this->getMockBuilder(MockWebsiteCustomizeWidgetRule::class)
                        ->setMethods(['validateHomePageContentFragment'])
                        ->getMock();

        $widgetRule->expects($this->exactly(1))->method('validateHomePageContentFragment')->willReturn(false);

        $result = $widgetRule->homePageFormat(array('content'=>'content'));

        $this->assertFalse($result);
    }

    public function testHomePageFormat()
    {
        $widgetRule = $this->getMockBuilder(MockWebsiteCustomizeWidgetRule::class)
                        ->setMethods(['validateHomePageContentFragment'])
                        ->getMock();

        $widgetRule->expects($this->exactly(1))->method('validateHomePageContentFragment')->willReturn(true);

        $result = $widgetRule->homePageFormat(array('content'=>'content'));

        $this->assertTrue($result);
    }

    //validateHomePageContentFragment -- start
    /**
     * @dataProvider validateHomePageContentFragmentProvider
     */
    public function testValidateHomePageContentFragment($key, $actual, $expected)
    {
        $result = $this->widgetRule->validateHomePageContentFragment($key, $actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateHomePageContentFragmentProvider()
    {
        return array(
            array('test', array('nav'=>'nav'), false),
            array('memorialStatus', WebsiteCustomizeWidgetRule::MEMORIAL_STATUS['YES'], true)
        );
    }
    //validateHomePageContentFragment -- end

    //validateMemorialStatus -- start
    /**
     * @dataProvider validateMemorialStatusProvider
     */
    public function testValidateMemorialStatus($actual, $expected)
    {
        $result = $this->widgetRule->validateMemorialStatus($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateMemorialStatusProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::MEMORIAL_STATUS['YES'], true),
            array(WebsiteCustomizeWidgetRule::MEMORIAL_STATUS['NO'], true),
            array('status', false),
            array(999, false),
        );
    }
    //validateMemorialStatus -- end

    //validateTheme -- start
    /**
     * @dataProvider validateThemeProvider
     */
    public function testValidateTheme($actual, $expected)
    {
        $result = $this->widgetRule->validateTheme($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateThemeProvider()
    {
        return array(
            array('', false),
            array(array('color'=>'color'), false),
            array(array('color'=>array('color'), 'image'=> 'image'), false),
            array(array('color'=>'color', 'image'=> 'image'), false),
            array(array('color'=>'color', 'image'=> array('name'=>'name')), false),
            array(array('color'=>'color', 'image'=> array('name'=>'name', 'identify'=>'1.pdf')), false),
            array(array('color'=>'color', 'image'=> array('name'=>'name', 'identify'=>'1.png')), true),
        );
    }
    //validateTheme -- end

    //validateHeaderBarLeft -- start
    /**
     * @dataProvider validateHeaderBarProvider
     */
    public function testValidateHeaderBarLeft($actual, $expected)
    {
        $result = $this->widgetRule->validateHeaderBarLeft($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateHeaderBarProvider()
    {
        return array(
            array('', false),
            array(array('color'=>'color'), false),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['PV']
                    ),
                ),
                true
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH-1),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['PV']
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED']+100,
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['PV']
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+1),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['PV']
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['PV']+10000
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => '',
                        'type' => WebsiteCustomizeWidgetRule::TYPE['NULL']
                    ),
                ),
                false
            ),
        );
    }
    //validateHeaderBarLeft -- end

    //validateHeaderBarRight -- start
    /**
     * @dataProvider validateHeaderBarProvider
     */
    public function testValidateHeaderBarRight($actual, $expected)
    {
        $result = $this->widgetRule->validateHeaderBarRight($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }
    //validateHeaderBarRight -- end

    /**
     * @dataProvider validateImageProvider
     */
    public function testValidateHeaderBg($actual, $expected)
    {
        $result = $this->widgetRule->validateHeaderBg($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateImageProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->word, false),
            array(array('name'=>'name'), false),
            array(array('name'=>$faker->word, 'identify'=>'1.png'), true),
        );
    }

    /**
     * @dataProvider validateImageProvider
     */
    public function testValidateLogo($actual, $expected)
    {
        $result = $this->widgetRule->validateLogo($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    //validateHeaderSearch -- start
    /**
     * @dataProvider validateHeaderSearchProvider
     */
    public function testValidateHeaderSearch($actual, $expected)
    {
        $result = $this->widgetRule->validateHeaderSearch($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateHeaderSearchProvider()
    {
        return array(
            array('', false),
            array(array('test'=>'test'), false),
            array(
                array(
                    'creditInformation' => array('status'=> IEnableAble::STATUS['ENABLED']),
                    'personalCreditInformation' => array('status'=> IEnableAble::STATUS['ENABLED']),
                    'unifiedSocialCreditCode' => array('status'=> IEnableAble::STATUS['ENABLED']),
                    'legalPerson' => array('status'=> IEnableAble::STATUS['ENABLED']),
                    'newsTitle' => array('status'=> IEnableAble::STATUS['ENABLED'])
                ),
                true
            ),
            array(
                array(
                    'creditInformation' => array('status'=> IEnableAble::STATUS['ENABLED']+100),
                    'personalCreditInformation' => array('status'=> IEnableAble::STATUS['ENABLED']),
                    'unifiedSocialCreditCode' => array('status'=> IEnableAble::STATUS['ENABLED']),
                    'legalPerson' => array('status'=> IEnableAble::STATUS['ENABLED']),
                    'newsTitle' => array('status'=> IEnableAble::STATUS['ENABLED'])
                ),
                false
            ),
        );
    }
    //validateHeaderSearch -- end

    //validateNav -- start
    /**
     * @dataProvider validateNavProvider
     */
    public function testValidateNav($actual, $expected)
    {
        $result = $this->widgetRule->validateNav($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateNavProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::HOME_PAGE_CONTENT_KEYS, false),
            array(array('content'=>'content'), false),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_THREE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.png'),
                        'nav' => 1
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_THREE_NAME_MIN_LENGTH-1),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.png'),
                        'nav' => 1
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_THREE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED']+1000,
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.png'),
                        'nav' => 1
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_THREE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH-1),
                        'image' => array('name'=> 'name', 'identify'=>'1.png'),
                        'nav' => 1
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_THREE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.pdf'),
                        'nav' => 1
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_THREE_NAME_MIN_LENGTH),
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.png'),
                        'nav' => 'nav'
                    )
                ),
                false
            ),
        );
    }
    //validateNav -- end

    //validateCenterDialog -- start
    /**
     * @dataProvider validateCenterDialogProvider
     */
    public function testValidateCenterDialog($actual, $expected)
    {
        $result = $this->widgetRule->validateCenterDialog($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateCenterDialogProvider()
    {
        return array(
            array('', false),
            array(array('test'=>'test'), false),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                    'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                ),
                true
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED']+1000,
                    'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                    'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+1),
                    'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                    'image' => array('name'=> 'name', 'identify'=>'1.pdf')
                ),
                false
            ),
        );
    }
    //validateCenterDialog -- end

    //validateAnimateWindow -- start
    /**
     * @dataProvider validateAnimateWindowProvider
     */
    public function testValidateAnimateWindow($actual, $expected)
    {
        $result = $this->widgetRule->validateAnimateWindow($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateAnimateWindowProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::HOME_PAGE_CONTENT_KEYS, false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+100,
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+2),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.xls')
                    )
                ),
                false
            ),
        );
    }
    //validateAnimateWindow -- end

    //validateLeftFloatCard -- start
    /**
     * @dataProvider validateLeftFloatCardProvider
     */
    public function testValidateLeftFloatCard($actual, $expected)
    {
        $result = $this->widgetRule->validateLeftFloatCard($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateLeftFloatCardProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::HOME_PAGE_CONTENT_KEYS, false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.png')
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'image' => array('name'=> 'name', 'identify'=>'1.png')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+200,
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.png')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+10),
                        'image' => array('name'=> 'name', 'identify'=>'1.png')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
        );
    }
    //validateLeftFloatCard -- end

    //validateFrameWindow -- start
    /**
     * @dataProvider validateFrameWindowProvider
     */
    public function testValidateFrameWindow($actual, $expected)
    {
        $result = $this->widgetRule->validateFrameWindow($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateFrameWindowProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::HOME_PAGE_CONTENT_KEYS, false),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                ),
                true
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED']+100000,
                    'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'url' => '',
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED']+100000,
                    'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MAX_LENGTH+10),
                ),
                false
            ),
        );
    }
    //validateFrameWindow -- end

    //validateRightToolBar -- start
    /**
     * @dataProvider validateRightToolBarProvider
     */
    public function testValidateRightToolBar($actual, $expected)
    {
        $result = $this->widgetRule->validateRightToolBar($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateRightToolBarProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::HOME_PAGE_CONTENT_KEYS, false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MIN_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['LINK'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                    ),
                ),
                true
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MIN_LENGTH)
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+10000,
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MIN_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['LINK'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH+10),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['LINK'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH+10),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['LINK']+10000,
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH),
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['IMAGE'],
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['IMAGE'],
                        'images'=>'images'
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['IMAGE'],
                        'images'=>array('image','images','images')
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['IMAGE'],
                        'images'=>array(
                            array('title'=>'title')
                        )
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['IMAGE'],
                        'images'=>array(
                            array(
                                'title'=>StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH
                                ),
                                'image' => array('name' => 'name', 'identify'=>'2.png')
                            )
                        )
                    ),
                ),
                true
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['IMAGE'],
                        'images'=>array(
                            array(
                                'title'=>StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH+10
                                ),
                                'image' => array('name' => 'name', 'identify'=>'1.png')
                            )
                        )
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MAX_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['IMAGE'],
                        'images'=>array(
                            array(
                                'title'=>StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH
                                ),
                                'image' => array('name' => 'name', 'identify'=>'1.pdf')
                            )
                        )
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MIN_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['LINK'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MAX_LENGTH+10),
                    ),
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MIN_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['WRITTEN_WORDS'],
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_DESCRIPTION_MIN_LENGTH
                        ),
                    ),
                ),
                true
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_TWO_NAME_MIN_LENGTH),
                        'category' => WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_CATEGORY['WRITTEN_WORDS'],
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::RIGHT_TOOL_BAR_DESCRIPTION_MAX_LENGTH+10
                        ),
                    ),
                ),
                false
            ),
        );
    }
    //validateRightToolBar -- end

    //validateSpecialColumn -- start
    /**
     * @dataProvider validateSpecialColumnProvider
     */
    public function testValidateSpecialColumn($actual, $expected)
    {
        $result = $this->widgetRule->validateSpecialColumn($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateSpecialColumnProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::HOME_PAGE_CONTENT_KEYS, false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpeg')
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpeg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+200,
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpeg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+10),
                        'image' => array('name'=> 'name', 'identify'=>'1.png')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.pdf')
                    )
                ),
                false
            ),
        );
    }
    //validateSpecialColumn -- end

    //validateRelatedLinks -- start
    /**
     * @dataProvider validateRelatedLinksProvider
     */
    public function testValidateRelatedLinks($actual, $expected)
    {
        $result = $this->widgetRule->validateRelatedLinks($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateRelatedLinksProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::HOME_PAGE_CONTENT_KEYS, false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH),
                        'items' => array(
                            array(
                                'status' => IEnableAble::STATUS['ENABLED'],
                                'name' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                                ),
                                'url' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH
                                ),
                            )
                        )
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH)
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH),
                        'items' => 'item'
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+100,
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH),
                        'items' => array(
                            array(
                                'status' => IEnableAble::STATUS['ENABLED'],
                                'name' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                                ),
                                'url' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH
                                ),
                            )
                        )
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH+1),
                        'items' => array(
                            array(
                                'status' => IEnableAble::STATUS['ENABLED'],
                                'name' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                                ),
                                'url' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH
                                ),
                            )
                        )
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH),
                        'items' => array(
                            array(
                                'name' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                                ),
                                'url' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH
                                ),
                            )
                        )
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH),
                        'items' => array(
                            array(
                                'status' => IEnableAble::STATUS['ENABLED']+100,
                                'name' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                                ),
                                'url' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH
                                ),
                            )
                        )
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH),
                        'items' => array(
                            array(
                                'status' => IEnableAble::STATUS['ENABLED'],
                                'name' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH+10
                                ),
                                'url' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MIN_LENGTH
                                ),
                            )
                        )
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH),
                        'items' => array(
                            array(
                                'status' => IEnableAble::STATUS['ENABLED'],
                                'name' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH
                                ),
                                'url' => StringGenerate::generate(
                                    WebsiteCustomizeWidgetRule::FORMAT_TWO_URL_MAX_LENGTH+10
                                ),
                            )
                        )
                    )
                ),
                false
            ),
        );
    }
    //validateRelatedLinks -- end

    //validateFooterBanner -- start
    /**
     * @dataProvider validateFooterBannerProvider
     */
    public function testValidateFooterBanner($actual, $expected)
    {
        $result = $this->widgetRule->validateFooterBanner($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateFooterBannerProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::HOME_PAGE_CONTENT_KEYS, false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+200,
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+10),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.pdf')
                    )
                ),
                false
            ),
        );
    }
    //validateFooterBanner -- end

    //validateOrganizationGroup -- start
    /**
     * @dataProvider validateOrganizationGroupProvider
     */
    public function testValidateOrganizationGroup($actual, $expected)
    {
        $result = $this->widgetRule->validateOrganizationGroup($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateOrganizationGroupProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomizeWidgetRule::HOME_PAGE_CONTENT_KEYS, false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+200,
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+10),
                        'image' => array('name'=> 'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'image' => array('name'=> 'name', 'identify'=>'1.pdf')
                    )
                ),
                false
            ),
        );
    }
    //validateOrganizationGroup -- end

    //validateSilhouette -- start
    /**
     * @dataProvider validateSilhouetteProvider
     */
    public function testValidateSilhouette($actual, $expected)
    {
        $result = $this->widgetRule->validateSilhouette($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateSilhouetteProvider()
    {
        return array(
            array('', false),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'description' => StringGenerate::generate(
                        WebsiteCustomizeWidgetRule::SILHOUETTE_DESCRIPTION_MIN_LENGTH
                    ),
                    'image' => array('name'=> 'name', 'identify'=>'1.png')
                ),
                true
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'image' => array('name'=> 'name', 'identify'=>'1.png'),
                    'description' => ''
                ),
                true
            ),
            array(
                array(
                    'image' => array('name'=> 'name', 'identify'=>'1.png')
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED']+100,
                    'description' => StringGenerate::generate(
                        WebsiteCustomizeWidgetRule::SILHOUETTE_DESCRIPTION_MIN_LENGTH
                    ),
                    'image' => array('name'=> 'name', 'identify'=>'1.png')
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'description' => StringGenerate::generate(
                        WebsiteCustomizeWidgetRule::SILHOUETTE_DESCRIPTION_MAX_LENGTH+10
                    ),
                    'image' => array('name'=> 'name', 'identify'=>'1.png')
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'description' => StringGenerate::generate(
                        WebsiteCustomizeWidgetRule::SILHOUETTE_DESCRIPTION_MAX_LENGTH
                    ),
                    'image' => array('name'=> 'name', 'identify'=>'1.pdf')
                ),
                false
            ),
        );
    }
    //validateSilhouette -- end

    //validatePartyAndGovernmentOrgans -- start
    /**
     * @dataProvider validatePartyAndGovernmentOrgansProvider
     */
    public function testValidatePartyAndGovernmentOrgans($actual, $expected)
    {
        $result = $this->widgetRule->validatePartyAndGovernmentOrgans($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validatePartyAndGovernmentOrgansProvider()
    {
        return array(
            array('', false),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'url' => StringGenerate::generate(
                        WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH
                    ),
                    'image' => array('name'=> 'name', 'identify'=>'1.png')
                ),
                true
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED']
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED']+100,
                    'url' => StringGenerate::generate(
                        WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH
                    ),
                    'image' => array('name'=> 'name', 'identify'=>'1.png')
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'url' => StringGenerate::generate(
                        WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+10
                    ),
                    'image' => array('name'=> 'name', 'identify'=>'1.png')
                ),
                false
            ),
            array(
                array(
                    'status' => IEnableAble::STATUS['ENABLED'],
                    'url' => StringGenerate::generate(
                        WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH
                    ),
                    'image' => array('name'=> 'name', 'identify'=>'1.jpeg')
                ),
                false
            ),
        );
    }
    //validatePartyAndGovernmentOrgans -- end

    //validateGovernmentErrorCorrection -- start
    /**
     * @dataProvider validatePartyAndGovernmentOrgansProvider
     */
    public function testValidateGovernmentErrorCorrection($actual, $expected)
    {
        $result = $this->widgetRule->validateGovernmentErrorCorrection($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    //validateFooterNav -- start
    /**
     * @dataProvider validateFooterNavProvider
     */
    public function testValidateFooterNav($actual, $expected)
    {
        $result = $this->widgetRule->validateFooterNav($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateFooterNavProvider()
    {
        return array(
            array('', false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+100,
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+100),
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH+10),
                    )
                ),
                false
            ),
        );
    }
    //validateFooterNav -- end

    //validateFooterTwo -- start
    /**
     * @dataProvider validateFooterTwoProvider
     */
    public function testValidateFooterTwo($actual, $expected)
    {
        $result = $this->widgetRule->validateFooterTwo($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateFooterTwoProvider()
    {
        return array(
            array('', false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+100,
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH+10),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+10),
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH+10
                        ),
                    )
                ),
                false
            ),
        );
    }
    //validateFooterTwo -- end

    //validateFooterThree -- start
    /**
     * @dataProvider validateFooterThreeProvider
     */
    public function testValidateFooterThree($actual, $expected)
    {
        $result = $this->widgetRule->validateFooterThree($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function validateFooterThreeProvider()
    {
        return array(
            array('', false),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MIN_LENGTH),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['NULL'],
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'url' => ''
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED']+100,
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MIN_LENGTH),
                        'url' => '',
                        'type' => WebsiteCustomizeWidgetRule::TYPE['NULL'],
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH+10),
                        'url' => '',
                        'type' => WebsiteCustomizeWidgetRule::TYPE['NULL'],
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH+10),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['NULL'],
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['NULL']+10000,
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['NULL'],
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MAX_LENGTH+10
                        ),
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['PUBLIC_NETWORK_SECURITY'],
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                        'image'=>array('name'=>'name', 'identify'=>'1.jpg')
                    )
                ),
                false
            ),
            array(
                array(
                    array(
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'name' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_NAME_MAX_LENGTH),
                        'url' => StringGenerate::generate(WebsiteCustomizeWidgetRule::FORMAT_ONE_URL_MAX_LENGTH),
                        'type' => WebsiteCustomizeWidgetRule::TYPE['PUBLIC_NETWORK_SECURITY'],
                        'description' => StringGenerate::generate(
                            WebsiteCustomizeWidgetRule::FORMAT_FOUR_NAME_MIN_LENGTH
                        ),
                        'image'=>array('name'=>'name', 'identify'=>'1.png')
                    )
                ),
                true
            ),
        );
    }
    //validateFooterThree -- end

    //status -- start
    /**
     * @dataProvider invalidStatusProvider
     */
    public function testStatusInvalid($actual, $expected)
    {
        $result = $this->widgetRule->status($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidStatusProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomize::STATUS['PUBLISHED'], true),
            array(WebsiteCustomize::STATUS['UNPUBLISHED'], true),
            array('status', false),
            array(999, false),
        );
    }
    //status -- end

    //category -- start
    /**
     * @dataProvider invalidCategoryProvider
     */
    public function testCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->category($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidCategoryProvider()
    {
        return array(
            array('', false),
            array(WebsiteCustomize::CATEGORY['HOME_PAGE'], true),
            array('category', false),
            array(999, false),
        );
    }
    //category -- end
}
