<?php
namespace Base\WebsiteCustomize\View;

use PHPUnit\Framework\TestCase;

use Base\WebsiteCustomize\Model\WebsiteCustomize;

class WebsiteCustomizeViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $websiteCustomize = new WebsiteCustomizeView(new WebsiteCustomize());
        $this->assertInstanceof('Base\Common\View\CommonView', $websiteCustomize);
    }
}
