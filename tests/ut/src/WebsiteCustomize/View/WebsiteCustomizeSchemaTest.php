<?php
namespace Base\WebsiteCustomize\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class WebsiteCustomizeSchemaTest extends TestCase
{
    private $websiteCustomizeSchema;

    private $websiteCustomize;

    public function setUp()
    {
        $this->websiteCustomizeSchema = new WebsiteCustomizeSchema(new Factory());

        $this->websiteCustomize = \Base\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->websiteCustomizeSchema);
        unset($this->websiteCustomize);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->websiteCustomizeSchema);
    }

    public function testGetId()
    {
        $result = $this->websiteCustomizeSchema->getId($this->websiteCustomize);

        $this->assertEquals($result, $this->websiteCustomize->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->websiteCustomizeSchema->getAttributes($this->websiteCustomize);

        $this->assertEquals($result['category'], $this->websiteCustomize->getCategory());
        $this->assertEquals($result['content'], $this->websiteCustomize->getContent());
        $this->assertEquals($result['version'], $this->websiteCustomize->getVersion());
        $this->assertEquals($result['status'], $this->websiteCustomize->getStatus());
        $this->assertEquals($result['createTime'], $this->websiteCustomize->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->websiteCustomize->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->websiteCustomize->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->websiteCustomizeSchema->getRelationships($this->websiteCustomize, 0, array());

        $this->assertEquals($result['crew'], ['data' => $this->websiteCustomize->getCrew()]);
    }
}
