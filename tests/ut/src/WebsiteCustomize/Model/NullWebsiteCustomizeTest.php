<?php
namespace Base\WebsiteCustomize\Model;

use PHPUnit\Framework\TestCase;

class NullWebsiteCustomizeTest extends TestCase
{
    private $websiteCustomize;

    public function setUp()
    {
        $this->websiteCustomize = $this->getMockBuilder(NullWebsiteCustomize::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->websiteCustomize);
    }

    public function testExtendsWebsiteCustomize()
    {
        $this->assertInstanceof('Base\WebsiteCustomize\Model\WebsiteCustomize', $this->websiteCustomize);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->websiteCustomize);
    }

    public function testIsUnPublished()
    {
        $this->websiteCustomize->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);

        $result = $this->websiteCustomize->isUnPublished();
        $this->assertFalse($result);
    }

    public function testAddActionPublishedStatus()
    {
        $this->websiteCustomize->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);

        $result = $this->websiteCustomize->addActionPublishedStatus();
        $this->assertFalse($result);
    }

    public function testAddActionUnPublishedStatus()
    {
        $this->websiteCustomize->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);

        $result = $this->websiteCustomize->addActionUnPublishedStatus();
        $this->assertFalse($result);
    }

    public function testPublish()
    {
        $this->websiteCustomize->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);

        $result = $this->websiteCustomize->publish();
        $this->assertFalse($result);
    }

    public function testUpdateStatus()
    {
        $this->websiteCustomize->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);

        $result = $this->websiteCustomize->updateStatus(WebsiteCustomize::STATUS['PUBLISHED']);
        $this->assertFalse($result);
    }

    public function testUpdatePublishWebsiteCustomize()
    {
        $this->websiteCustomize->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);

        $result = $this->websiteCustomize->updatePublishWebsiteCustomize();
        $this->assertFalse($result);
    }
}
