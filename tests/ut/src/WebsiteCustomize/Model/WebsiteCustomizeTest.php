<?php
namespace Base\WebsiteCustomize\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Query\FragmentCacheQuery;

use Base\Crew\Model\Crew;
use Base\Crew\Model\NullCrew;

use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeFragmentCacheQueryFactory;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WebsiteCustomizeTest extends TestCase
{
    private $websiteCustomize;

    public function setUp()
    {
        $this->websiteCustomize = new MockWebsiteCustomize();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->websiteCustomize);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->websiteCustomize
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IOperate',
            $this->websiteCustomize
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter',
            $this->websiteCustomize->getRepository()
        );
    }

    public function testGetFragmentCacheQueryFactory()
    {
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeFragmentCacheQueryFactory',
            $this->websiteCustomize->getFragmentCacheQueryFactory()
        );
    }

    public function testGetFragmentCacheQuery()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Query\FragmentCacheQuery',
            $this->websiteCustomize->getFragmentCacheQuery()
        );
    }

    public function testWebsiteCustomizeConstructor()
    {
        $this->assertEquals(0, $this->websiteCustomize->getId());
        $this->assertEquals(0, $this->websiteCustomize->getVersion());
        $this->assertEquals(array(), $this->websiteCustomize->getContent());
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->websiteCustomize->getCrew()
        );
        $this->assertEquals(WebsiteCustomize::CATEGORY['HOME_PAGE'], $this->websiteCustomize->getCategory());
        $this->assertEquals(WebsiteCustomize::STATUS['UNPUBLISHED'], $this->websiteCustomize->getStatus());
        $this->assertEquals(Core::$container->get('time'), $this->websiteCustomize->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $this->websiteCustomize->getCreateTime());
        $this->assertEquals(0, $this->websiteCustomize->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 WebsiteCustomize setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->websiteCustomize->setId(1);
        $this->assertEquals(1, $this->websiteCustomize->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //version 测试 ------------------------------------------------------- start
    /**
     * 设置 WebsiteCustomize setVersion() 正确的传参类型,期望传值正确
     */
    public function testSetVersionCorrectType()
    {
        $this->websiteCustomize->setVersion('2108062345');
        $this->assertEquals('2108062345', $this->websiteCustomize->getVersion());
    }

    /**
     * 设置 WebsiteCustomize setVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVersionWrongType()
    {
        $this->websiteCustomize->setVersion(array(1, 2, 3));
    }
    //version 测试 -------------------------------------------------------   end
    
    //content 测试 ------------------------------------------------------- start
    /**
     * 设置 WebsiteCustomize setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->websiteCustomize->setContent(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->websiteCustomize->getContent());
    }

    /**
     * 设置 WebsiteCustomize setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->websiteCustomize->setContent('websiteCustomize');
    }
    //content 测试 -------------------------------------------------------   end

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->websiteCustomize->setCrew($object);
        $this->assertSame($object, $this->websiteCustomize->getCrew());
    }

    /**
     * 设置 setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->websiteCustomize->setCrew('string');
    }
    //crew 测试 ---------------------------------------------   end
    
    //category 测试 ------------------------------------------------------ start
    /**
     * 循环测试 WebsiteCustomize setCategory() 是否符合预定范围
     *
     * @dataProvider categoryProvider
     */
    public function testSetCategory($actual, $expected)
    {
        $this->websiteCustomize->setCategory($actual);
        $this->assertEquals($expected, $this->websiteCustomize->getCategory());
    }

    /**
     * 循环测试 WebsiteCustomize setCategory() 数据构建器
     */
    public function categoryProvider()
    {
        return array(
            array(WebsiteCustomize::CATEGORY['HOME_PAGE'],WebsiteCustomize::CATEGORY['HOME_PAGE']),
            array(999,WebsiteCustomize::CATEGORY['HOME_PAGE']),
        );
    }

    /**
     * 设置 WebsiteCustomize setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->websiteCustomize->setCategory('string');
    }
    //category 测试 ------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 WebsiteCustomize setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->websiteCustomize->setStatus($actual);
        $this->assertEquals($expected, $this->websiteCustomize->getStatus());
    }

    /**
     * 循环测试 WebsiteCustomize setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(WebsiteCustomize::STATUS['UNPUBLISHED'],WebsiteCustomize::STATUS['UNPUBLISHED']),
            array(WebsiteCustomize::STATUS['PUBLISHED'],WebsiteCustomize::STATUS['PUBLISHED']),
            array(999,WebsiteCustomize::STATUS['UNPUBLISHED']),
        );
    }

    /**
     * 设置 WebsiteCustomize setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->websiteCustomize->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    public function testGenerateVersion()
    {
        $this->websiteCustomize->generateVersion();

        $this->assertIsInt($this->websiteCustomize->getVersion());
    }

    public function testIsUnPublished()
    {
        $this->websiteCustomize->setStatus(WebsiteCustomize::STATUS['UNPUBLISHED']);

        $result = $this->websiteCustomize->isUnPublished();
        $this->assertTrue($result);
    }

    public function testAddFailCrewNull()
    {
        $this->websiteCustomize->setCrew(new NullCrew());

        $result = $this->websiteCustomize->add();

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('crewId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }
    
    public function testAddSuccessPublished()
    {
        //初始化
        $websiteCustomize = $this->getMockBuilder(MockWebsiteCustomize::class)
                           ->setMethods(['isUnPublished', 'addActionUnPublishedStatus'])
                           ->getMock();

        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $websiteCustomize->setCrew($crew);

        $websiteCustomize->expects($this->exactly(1))
             ->method('isUnPublished')
             ->willReturn(true);


        $websiteCustomize->expects($this->exactly(1))
             ->method('addActionUnPublishedStatus')
             ->willReturn(true);

        //验证
        $result = $websiteCustomize->add();
        $this->assertTrue($result);
    }

    public function testAddSuccessUnPublished()
    {
        //初始化
        $websiteCustomize = $this->getMockBuilder(MockWebsiteCustomize::class)
                           ->setMethods(['isUnPublished', 'addActionPublishedStatus'])
                           ->getMock();

        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $websiteCustomize->setCrew($crew);

        $websiteCustomize->expects($this->exactly(1))
             ->method('isUnPublished')
             ->willReturn(false);

        $websiteCustomize->expects($this->exactly(1))
             ->method('addActionPublishedStatus')
             ->willReturn(true);

        //验证
        $result = $websiteCustomize->add();
        $this->assertTrue($result);
    }

    public function testAddActionPublishedStatus()
    {
        $websiteCustomize = $this->getMockBuilder(MockWebsiteCustomize::class)
            ->setMethods(['getRepository', 'updatePublishWebsiteCustomize', 'getFragmentCacheQuery'])
            ->getMock();

        $repository = $this->prophesize(IWebsiteCustomizeAdapter::class);
        $repository->add($websiteCustomize)->shouldBeCalledTimes(1)->willReturn(true);
        $websiteCustomize->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $websiteCustomize->expects($this->exactly(1))->method('updatePublishWebsiteCustomize')->willReturn(true);
    
        $fragmentCacheQuery = $this->prophesize(FragmentCacheQuery::class);
        $fragmentCacheQuery->clear()->shouldBeCalledTimes(1)->willReturn(true);
        $websiteCustomize->expects($this->once())->method(
            'getFragmentCacheQuery'
        )->willReturn($fragmentCacheQuery->reveal());

        $result = $websiteCustomize->addActionPublishedStatus();
        
        $this->assertTrue($result);
    }

    public function testAddActionPublishedStatusFail()
    {
        $websiteCustomize = $this->getMockBuilder(MockWebsiteCustomize::class)
            ->setMethods(['updatePublishWebsiteCustomize'])
            ->getMock();

        $websiteCustomize->expects($this->exactly(1))->method('updatePublishWebsiteCustomize')->willReturn(false);
    
        $result = $websiteCustomize->addActionPublishedStatus();
        
        $this->assertFalse($result);
    }

    public function testAddActionUnPublishedStatus()
    {
        $websiteCustomize = $this->getMockBuilder(MockWebsiteCustomize::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $repository = $this->prophesize(IWebsiteCustomizeAdapter::class);
        $repository->add($websiteCustomize)->shouldBeCalledTimes(1)->willReturn(true);
        $websiteCustomize->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $websiteCustomize->addActionUnPublishedStatus();
        
        $this->assertTrue($result);
    }

    public function testEdit()
    {
        $result = $this->websiteCustomize->edit();
        $this->assertFalse($result);
    }

    public function testPublishFail()
    {
        $status = WebsiteCustomize::STATUS['PUBLISHED'];
        $this->websiteCustomize->setStatus($status);

        $result = $this->websiteCustomize->publish();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testPublish()
    {
        $websiteCustomize = $this->getMockBuilder(MockWebsiteCustomize::class)
            ->setMethods(['getFragmentCacheQuery', 'updateStatus', 'updatePublishWebsiteCustomize'])
            ->getMock();
            
        $status = WebsiteCustomize::STATUS['UNPUBLISHED'];

        $websiteCustomize->expects($this->exactly(1))->method('updateStatus')->willReturn(true);
        $websiteCustomize->expects($this->exactly(1))->method('updatePublishWebsiteCustomize')->willReturn(true);

        $fragmentCacheQuery = $this->prophesize(FragmentCacheQuery::class);
        $fragmentCacheQuery->clear()->shouldBeCalledTimes(1)->willReturn(true);
        $websiteCustomize->expects($this->once())->method(
            'getFragmentCacheQuery'
        )->willReturn($fragmentCacheQuery->reveal());

        $result = $websiteCustomize->publish();
        
        $this->assertTrue($result);
    }

    public function testPublishFalse()
    {
        $websiteCustomize = $this->getMockBuilder(MockWebsiteCustomize::class)
            ->setMethods(['updatePublishWebsiteCustomize'])
            ->getMock();
            
        $status = WebsiteCustomize::STATUS['UNPUBLISHED'];

        $websiteCustomize->expects($this->exactly(1))->method('updatePublishWebsiteCustomize')->willReturn(true);

        $result = $websiteCustomize->publish();
        
        $this->assertFalse($result);
    }

    public function testUpdateStatus()
    {
        $websiteCustomize = $this->getMockBuilder(MockWebsiteCustomize::class)
            ->setMethods(['getRepository', 'setUpdateTime', 'setStatusTime'])
            ->getMock();
            
        $status = WebsiteCustomize::STATUS['PUBLISHED'];

        $websiteCustomize->setStatus($status);
        $websiteCustomize->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));
        $websiteCustomize->expects($this->exactly(1))
             ->method('setStatusTime')
             ->with(Core::$container->get('time'));

        $repository = $this->prophesize(IWebsiteCustomizeAdapter::class);

        $repository->edit(
            Argument::exact($websiteCustomize),
            Argument::exact(array('updateTime','status','statusTime'))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $websiteCustomize->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $websiteCustomize->updateStatus($status);
        
        $this->assertTrue($result);
    }

    public function testUpdatePublishWebsiteCustomizeFail()
    {
        $websiteCustomize = $this->getMockBuilder(MockWebsiteCustomize::class)
                        ->setMethods(['fetchPublishWebsiteCustomize'])
                        ->getMock();

        $websiteCustomize->expects($this->exactly(1))->method(
            'fetchPublishWebsiteCustomize'
        )->willReturn(NullWebsiteCustomize::getInstance());

        $result = $websiteCustomize->updatePublishWebsiteCustomize();
        
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('websiteCustomizeId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testUpdatePublishWebsiteCustomizeSuccess()
    {
        $stub = $this->getMockBuilder(MockWebsiteCustomize::class)
                        ->setMethods(['fetchPublishWebsiteCustomize'])
                        ->getMock();

        $websiteCustomize = $this->prophesize(MockWebsiteCustomize::class);
        $websiteCustomize->updateStatus(
            Argument::exact(WebsiteCustomize::STATUS['UNPUBLISHED'])
        )->shouldBeCalledTimes(1)->willReturn(true);

        $stub->expects($this->once())->method(
            'fetchPublishWebsiteCustomize'
        )->willReturn($websiteCustomize->reveal());

        $result = $stub->updatePublishWebsiteCustomize();

        $this->assertTrue($result);
    }

    private function initialFetchPublishWebsiteCustomize($count)
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomize::class)
                        ->setMethods(['getRepository'])
                        ->getMock();

        $list = array(new WebsiteCustomize());
        $category = WebsiteCustomize::CATEGORY['HOME_PAGE'];
        $this->stub->setCategory($category);
        $filter['category'] = $category;
        $filter['status'] = WebsiteCustomize::STATUS['PUBLISHED'];

        $repository = $this->prophesize(WebsiteCustomizeRepository::class);

        $repository->filter(
            Argument::exact($filter)
        )->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->stub->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
    }

    public function testFetchPublishWebsiteCustomizeTrue()
    {
        $this->initialFetchPublishWebsiteCustomize(WebsiteCustomize::PUBLISHED_STATUS_COUNT);

        $result = $this->stub->fetchPublishWebsiteCustomize();

        $this->assertInstanceof(
            'Base\WebsiteCustomize\Model\WebsiteCustomize',
            $result
        );
    }

    public function testFetchPublishWebsiteCustomizeFalse()
    {
        $this->initialFetchPublishWebsiteCustomize(WebsiteCustomize::PUBLISHED_STATUS_COUNT+1);

        $result = $this->stub->fetchPublishWebsiteCustomize();

        $this->assertInstanceof(
            'Base\WebsiteCustomize\Model\NullWebsiteCustomize',
            $result
        );
    }
}
