<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\WebsiteCustomize\Model\WebsiteCustomize;

class WebsiteCustomizeMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new WebsiteCustomizeMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new WebsiteCustomize()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new WebsiteCustomize(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Model\WebsiteCustomize',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\WebsiteCustomize\Model\WebsiteCustomize',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\WebsiteCustomize\Model\WebsiteCustomize',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
