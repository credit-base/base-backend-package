<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use PHPUnit\Framework\TestCase;

class NullWebsiteCustomizeHomeFragmentCacheQueryTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = NullWebsiteCustomizeHomeFragmentCacheQuery::getInstance();
    }

    public function tearDown()
    {
        unset($this->cache);
    }

    public function testExtendsWebsiteCustomizeHomePageFragmentCacheQuery()
    {
        $this->assertInstanceof(
            'Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeHomePageFragmentCacheQuery',
            $this->cache
        );
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->cache);
    }

    public function testFetchCacheData()
    {
        $result = $this->cache->fetchCacheData();

        $this->assertEquals(array(), $result);
    }
}
