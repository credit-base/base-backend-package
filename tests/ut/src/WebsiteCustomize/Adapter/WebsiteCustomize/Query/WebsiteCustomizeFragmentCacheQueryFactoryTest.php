<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class WebsiteCustomizeFragmentCacheQueryFactoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new WebsiteCustomizeFragmentCacheQueryFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullCache()
    {
        $stub = $this->stub->getCache(0);
            $this->assertInstanceOf(
                'Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\NullWebsiteCustomizeHomeFragmentCacheQuery',
                $stub
            );
    }

    public function testGetCache()
    {
        foreach (WebsiteCustomizeFragmentCacheQueryFactory::MAPS as $key => $cache) {
            $this->assertInstanceOf(
                $cache,
                $this->stub->getCache($key)
            );
        }
    }
}
