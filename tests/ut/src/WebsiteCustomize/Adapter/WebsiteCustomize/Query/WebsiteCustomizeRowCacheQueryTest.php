<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class WebsiteCustomizeRowCacheQueryTest extends TestCase
{
    private $rowCacheQuery;

    public function setUp()
    {
        $this->rowCacheQuery = new MockWebsiteCustomizeRowCacheQuery();
    }

    public function tearDown()
    {
        unset($this->rowCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowCacheQuery', $this->rowCacheQuery);
    }

    /**
     * 测试是否cache层赋值正确
     */
    public function testCorrectCacheLayer()
    {
        $this->assertInstanceof(
            'Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\Persistence\WebsiteCustomizeCache',
            $this->rowCacheQuery->getCacheLayer()
        );
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\Persistence\WebsiteCustomizeDb',
            $this->rowCacheQuery->getDbLayer()
        );
    }
}
