<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Model\NullWebsiteCustomize;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;

class WebsiteCustomizeHomePageFragmentCacheQueryTest extends TestCase
{
    private $fragmentCacheQuery;

    public function setUp()
    {
        $this->fragmentCacheQuery = new MockWebsiteCustomizeHomePageFragmentCacheQuery();
    }

    public function tearDown()
    {
        unset($this->fragmentCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承FragmentCacheQuery类
     */
    public function testCorrectInstanceExtendsFragmentCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\FragmentCacheQuery', $this->fragmentCacheQuery);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository',
            $this->fragmentCacheQuery->getRepository()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceof(
            'Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator',
            $this->fragmentCacheQuery->getTranslator()
        );
    }

    private function initialFetchCacheData($count)
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeHomePageFragmentCacheQuery::class)
                        ->setMethods(['getRepository', 'getTranslator'])
                        ->getMock();

        $list = array(new WebsiteCustomize());

        $filter['category'] = WebsiteCustomize::CATEGORY['HOME_PAGE'];
        $filter['status'] = WebsiteCustomize::STATUS['PUBLISHED'];

        $repository = $this->prophesize(WebsiteCustomizeRepository::class);
        $repository->filter(
            Argument::exact($filter)
        )->shouldBeCalledTimes(1)->willReturn([$list, $count]);
        $this->stub->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
    }

    public function testFetchCacheDataTrue()
    {
        $this->initialFetchCacheData(WebsiteCustomize::PUBLISHED_STATUS_COUNT);

        $websiteCustomize = new WebsiteCustomize();
        $data = array('websiteCustomize');
        
        $translator = $this->prophesize(WebsiteCustomizeDbTranslator::class);
        $translator->objectToArray(Argument::exact($websiteCustomize))->shouldBeCalledTimes(1)->willReturn($data);
        $this->stub->expects($this->once())->method('getTranslator')->willReturn($translator->reveal());

        $result = $this->stub->fetchCacheData();

        $this->assertEquals($data, $result);
    }

    public function testFetchCacheDataFalse()
    {
        $this->initialFetchCacheData(WebsiteCustomize::PUBLISHED_STATUS_COUNT+1);
        
        $data = array();
        
        $translator = $this->prophesize(WebsiteCustomizeDbTranslator::class);
        $translator->objectToArray(
            Argument::exact(NullWebsiteCustomize::getInstance())
        )->shouldBeCalledTimes(1)->willReturn($data);
        $this->stub->expects($this->once())->method('getTranslator')->willReturn($translator->reveal());

        $result = $this->stub->fetchCacheData();

        $this->assertEquals($data, $result);
    }
}
