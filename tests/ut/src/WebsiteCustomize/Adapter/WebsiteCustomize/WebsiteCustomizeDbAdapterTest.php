<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Model\NullWebsiteCustomize;
use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeRowCacheQuery;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WebsiteCustomizeDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(WebsiteCustomizeDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchCrew'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IWebsiteCustomizeAdapter
     */
    public function testImplementsIWebsiteCustomizeAdapter()
    {
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 WebsiteCustomizeDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockWebsiteCustomizeDbAdapter();
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 WebsiteCustomizeRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockWebsiteCustomizeDbAdapter();
        $this->assertInstanceOf(
            'Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetCrewRepository()
    {
        $adapter = new MockWebsiteCustomizeDbAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockWebsiteCustomizeDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedWebsiteCustomize = new WebsiteCustomize();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedWebsiteCustomize)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedWebsiteCustomize);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedWebsiteCustomize = new WebsiteCustomize();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedWebsiteCustomize, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedWebsiteCustomize, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedWebsiteCustomize = new WebsiteCustomize();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedWebsiteCustomize);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedWebsiteCustomize);
        //验证
        $websiteCustomize = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedWebsiteCustomize, $websiteCustomize);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $websiteCustomizeOne = new WebsiteCustomize(1);
        $websiteCustomizeTwo = new WebsiteCustomize(2);

        $ids = [1, 2];

        $expectedWebsiteCustomizeList = [];
        $expectedWebsiteCustomizeList[$websiteCustomizeOne->getId()] = $websiteCustomizeOne;
        $expectedWebsiteCustomizeList[$websiteCustomizeTwo->getId()] = $websiteCustomizeTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedWebsiteCustomizeList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchCrew')
                         ->with($expectedWebsiteCustomizeList);
        //验证
        $websiteCustomizeList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedWebsiteCustomizeList, $websiteCustomizeList);
    }

    //fetchCrew
    public function testFetchCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockWebsiteCustomizeDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByObject'
                                ]
                           )
                           ->getMock();
        
        $websiteCustomize = new WebsiteCustomize();

        $adapter->expects($this->exactly(1))
                         ->method('fetchCrewByObject')
                         ->with($websiteCustomize);

        $adapter->fetchCrew($websiteCustomize);
    }

    public function testFetchCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockWebsiteCustomizeDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByList'
                                ]
                           )
                           ->getMock();
        
        $websiteCustomizeOne = new WebsiteCustomize(1);
        $websiteCustomizeTwo = new WebsiteCustomize(2);
        
        $websiteCustomizeList[$websiteCustomizeOne->getId()] = $websiteCustomizeOne;
        $websiteCustomizeList[$websiteCustomizeTwo->getId()] = $websiteCustomizeTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchCrewByList')
                         ->with($websiteCustomizeList);

        $adapter->fetchCrew($websiteCustomizeList);
    }

    //fetchCrewByObject
    public function testFetchCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockWebsiteCustomizeDbAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $websiteCustomize = new WebsiteCustomize();
        $websiteCustomize->setCrew(new Crew(1));
        
        $crewId = 1;

        // 为 Crew 类建立预言
        $crew  = $this->prophesize(Crew::class);
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $adapter->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByObject($websiteCustomize);
    }

    //fetchCrewByList
    public function testFetchCrewByList()
    {
        $adapter = $this->getMockBuilder(MockWebsiteCustomizeDbAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $websiteCustomizeOne = new WebsiteCustomize(1);
        $crewOne = new Crew(1);
        $websiteCustomizeOne->setCrew($crewOne);

        $websiteCustomizeTwo = new WebsiteCustomize(2);
        $crewTwo = new Crew(2);
        $websiteCustomizeTwo->setCrew($crewTwo);
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $websiteCustomizeList[$websiteCustomizeOne->getId()] = $websiteCustomizeOne;
        $websiteCustomizeList[$websiteCustomizeTwo->getId()] = $websiteCustomizeTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crewList);

        $adapter->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByList($websiteCustomizeList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-crew
    public function testFormatFilterCrew()
    {
        $filter = array(
            'crew' => 1
        );
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = 'crew_id = '.$filter['crew'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-status
    public function testFormatFilterStatus()
    {
        $filter = array(
            'status' => 2
        );
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = 'status = '.$filter['status'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-category
    public function testFormatFilterCategory()
    {
        $filter = array(
            'category' => 1
        );
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = 'category = '.$filter['category'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-version
    public function testFormatFilterVersion()
    {
        $filter = array(
            'version' => 1
        );
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = 'version = '.$filter['version'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    public function testFormatSort()
    {
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--status
    public function testFormatSortStatusDesc()
    {
        $sort = array('status' => -1);
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = ' ORDER BY status DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-status
    public function testFormatSortStatusAsc()
    {
        $sort = array('status' => 1);
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = ' ORDER BY status ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--id
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = ' ORDER BY website_customize_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-id
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockWebsiteCustomizeDbAdapter();

        $expectedCondition = ' ORDER BY website_customize_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
