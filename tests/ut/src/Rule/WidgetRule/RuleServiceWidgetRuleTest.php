<?php
namespace Base\Rule\WidgetRule;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Rule\Model\RuleService;

use Base\Template\Model\Template;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class RuleServiceWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new MockRuleServiceWidgetRule();
    }

    public function tearDown()
    {
        unset($this->widgetRule);
    }

    //rule
    public function testRulesSuccess()
    {
        $rules = ['ruleName'=>['rules']];

        $this->widgetRule = $this->getMockBuilder(MockRuleServiceWidgetRule::class)
            ->setMethods([
                'ruleName',
                'ruleFormat',
                'validateRule'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('ruleName')
                   ->willReturn(true);

        $this->widgetRule->expects($this->once())
                   ->method('ruleFormat')
                   ->willReturn(true);

        $this->widgetRule->expects($this->once())
                   ->method('validateRule')
                   ->with('ruleName', ['rules'])
                   ->willReturn(true);

        $result = $this->widgetRule->rules($rules);
        $this->assertTrue($result);
    }

    public function testRulesFailNotArray()
    {
        $rules = 'rules';
        $result = $this->widgetRule->rules($rules);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'rules'), Core::getLastError()->getSource());
    }

    public function testRulesFail()
    {
        $rules = ['ruleName'=>['rules']];

        $this->widgetRule = $this->getMockBuilder(MockRuleServiceWidgetRule::class)
            ->setMethods([
                'ruleName',
                'ruleFormat',
                'validateRule'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('ruleName')
                   ->willReturn(false);

        $this->widgetRule->expects($this->exactly(0))
                   ->method('ruleFormat');

        $this->widgetRule->expects($this->exactly(0))
                   ->method('validateRule')
                   ->with('ruleName', ['rules']);

        $result = $this->widgetRule->rules($rules);
        $this->assertFalse($result);
    }

    //validateRule
    public function testValidateTransformationRule()
    {
        $rule = ['rule'];

        $result = $this->widgetRule->validateTransformationRule($rule);
        $this->assertTrue($result);
    }

    public function testValidateRuleCompletionRules()
    {
        $name = 'completionRule';
        $rule = ['rule'];

        $this->widgetRule = $this->getMockBuilder(MockRuleServiceWidgetRule::class)
            ->setMethods([
                'validateCompletionRules'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('validateCompletionRules')
                   ->with($rule)
                   ->willReturn(true);

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertTrue($result);
    }

    public function testValidateRuleComparisonRules()
    {
        $name = 'comparisonRule';
        $rule = ['rule'];

        $this->widgetRule = $this->getMockBuilder(MockRuleServiceWidgetRule::class)
            ->setMethods([
                'validateComparisonRules'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('validateComparisonRules')
                   ->with($rule)
                   ->willReturn(true);

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertTrue($result);
    }

    public function testValidateDeDuplicationRules()
    {
        $name = 'deDuplicationRule';
        $rule = ['rule'];

        $this->widgetRule = $this->getMockBuilder(MockRuleServiceWidgetRule::class)
            ->setMethods([
                'validateDeDuplicationRules'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('validateDeDuplicationRules')
                   ->with($rule)
                   ->willReturn(true);

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertTrue($result);
    }
    public function testValidateRuleFalse()
    {
        $name = 'name';
        $rule = ['rule'];

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertFalse($result);
    }

    //ruleName
    public function testRuleNameSuccess()
    {
        $name = 'transformationRule';
        $result = $this->widgetRule->ruleName($name);
        $this->assertTrue($result);
    }

    public function testRuleNameFail()
    {
        $name = 'name';
        $result = $this->widgetRule->ruleName($name);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'rulesName'), Core::getLastError()->getSource());
    }

    //ruleFormat
    public function testRuleFormatSuccess()
    {
        $rule = ['rule'];
        $result = $this->widgetRule->ruleFormat($rule);
        $this->assertTrue($result);
    }

    public function testRuleFormatFail()
    {
        $rule = 'rule';
        $result = $this->widgetRule->ruleFormat($rule);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'rules'), Core::getLastError()->getSource());
    }

    //category
    public function testCategorySuccess()
    {
        $category = Template::CATEGORY['BJ'];
        $result = $this->widgetRule->category($category);
        $this->assertTrue($result);
    }

    public function testCategoryFailNotNumber()
    {
        $category = 'category';
        $result = $this->widgetRule->category($category);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'transformationCategory'), Core::getLastError()->getSource());
    }

    public function testCategoryFailOutOfRange()
    {
        $category = 10000;
        $result = $this->widgetRule->category($category);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'transformationCategory'), Core::getLastError()->getSource());
    }

    //validateCompletionAndComparisonSuccess
    private function validateCompletionAndComparisonSuccess($function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];
        $result = $this->widgetRule->$function($rules);
        $this->assertTrue($result);
    }

    public function testValidateCompletionRulesSuccess()
    {
        $this->validateCompletionAndComparisonSuccess('validateCompletionRules');
    }

    public function testValidateComparisonRulesSuccess()
    {
        $this->validateCompletionAndComparisonSuccess('validateComparisonRules');
    }

    private function prepareCompletionAndComparisonRulesFail($rules, $pointer, $function)
    {
        $result = $this->widgetRule->$function($rules);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => $pointer), Core::getLastError()->getSource());
    }

    //validateCompletionAndComparisonRulesFailNotArray
    private function validateCompletionAndComparisonRulesFailNotArray($pointer, $function)
    {
        $rules = [
        'ZTMC' => 'rule',
        'TYSHXYDM' => array(
            array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
            array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
        )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailNotArray('completionRules', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailNotArray('comparisonRules', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailMaxRules
    private function validateCompletionAndComparisonRulesFailMaxRules($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                array('id'=>3, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            ),
            'TEST' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailMaxRules()
    {
        $this->validateCompletionAndComparisonRulesFailMaxRules('completionRulesCount', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailMaxRules()
    {
        $this->validateCompletionAndComparisonRulesFailMaxRules('comparisonRulesCount', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailSetKey
    private function validateCompletionAndComparisonRulesFailSetKey($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailNotSetKey()
    {
        $this->validateCompletionAndComparisonRulesFailSetKey('completionRules', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailNotSetKey()
    {
        $this->validateCompletionAndComparisonRulesFailSetKey('comparisonRules', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailIdNotNumber
    private function validateCompletionAndComparisonRulesFailIdNotNumber($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>'id', 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailIdNotNumber()
    {
        $this->validateCompletionAndComparisonRulesFailIdNotNumber('completionRulesId', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailIdNotNumber()
    {
        $this->validateCompletionAndComparisonRulesFailIdNotNumber('comparisonRulesId', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailIdBaseNotArray
    private function validateCompletionAndComparisonRulesFailIdBaseNotArray($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> 'base', 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailBaseNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailIdBaseNotArray('completionRulesBase', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailBaseNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailIdBaseNotArray('comparisonRulesBase', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailIdItemNotString
    private function validateCompletionAndComparisonRulesFailIdItemNotString($pointer, $function)
    {
        $rules = [
           'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>['ZTMC']),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailItemNotString()
    {
        $this->validateCompletionAndComparisonRulesFailIdItemNotString(
            'completionRulesItem',
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailItemNotString()
    {
         $this->validateCompletionAndComparisonRulesFailIdItemNotString(
             'comparisonRulesItem',
             'validateComparisonRules'
         );
    }

    //validateCompletionAndComparisonRulesFailBaseOutOfRange
    private function validateCompletionAndComparisonRulesFailBaseOutOfRange($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,3), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailBaseOutOfRange()
    {
        $this->validateCompletionAndComparisonRulesFailBaseOutOfRange(
            'completionRulesBase',
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailBaseOutOfRange()
    {
        $this->validateCompletionAndComparisonRulesFailBaseOutOfRange(
            'comparisonRulesBase',
            'validateComparisonRules'
        );
    }

    //validateDeDuplicationRules
    public function testValidateDeDuplicationRulesSuccess()
    {
        $rules = [
             'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC", "TYSHXYDM"))
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertTrue($result);
    }

    public function testValidateDeDuplicationRulesFailNotSetResult()
    {
        $rules = [
             'deDuplicationRule' => array("items"=>array("ZTMC", "TYSHXYDM"))
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'deDuplicationRules'), Core::getLastError()->getSource());
    }

    public function testValidateDeDuplicationRulesFailItemsNotArray()
    {
        $rules = [
             'deDuplicationRule' => array("result"=>1, "items"=>"items")
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'deDuplicationRulesItems'), Core::getLastError()->getSource());
    }

    public function testValidateDeDuplicationRulesFailResultOtOfRange()
    {
        $rules = [
             'deDuplicationRule' => array("result"=>3, "items"=>array("ZTMC", "TYSHXYDM"))
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'deDuplicationRulesResult'), Core::getLastError()->getSource());
    }
}
