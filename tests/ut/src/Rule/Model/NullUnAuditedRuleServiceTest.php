<?php
namespace Base\Rule\Model;

use PHPUnit\Framework\TestCase;

class NullUnAuditedRuleServiceTest extends TestCase
{
    private $ruleService;

    public function setUp()
    {
        $this->ruleService = $this->getMockBuilder(NullUnAuditedRuleService::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->ruleService);
    }

    public function testExtendsRule()
    {
        $this->assertInstanceOf(
            'Base\Rule\Model\UnAuditedRuleService',
            $this->ruleService
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->ruleService
        );
    }

    public function testRevoke()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->revoke();
        $this->assertFalse($result);
    }
    
    private function mockResourceNotExist()
    {
        $this->ruleService->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }
}
