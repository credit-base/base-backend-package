<?php
namespace Base\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\Model\IApproveAble;
use Base\Common\Model\IResubmitAble;
use Base\Common\Model\ApproveAbleTrait;
use Base\Common\Model\ResubmitAbleTrait;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

use Base\Template\Model\Template;
use Base\Template\Model\WbjTemplate;

use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditedRuleServiceTest extends TestCase
{
    private $unAuditedRuleService;

    public function setUp()
    {
        $this->unAuditedRuleService = new MockUnAuditedRuleService();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->unAuditedRuleService);
    }

    public function testExtendsRuleService()
    {
        $unAuditedRuleService = new UnAuditedRuleService();
        $this->assertInstanceOf(
            'Base\Rule\Model\RuleService',
            $unAuditedRuleService
        );
    }

    public function testImplementsIApproveAble()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IApproveAble',
            $this->unAuditedRuleService
        );
    }

    public function testImplementsIResubmitAble()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IResubmitAble',
            $this->unAuditedRuleService
        );
    }

    public function testGetUnAuditedRuleServiceSdkRepository()
    {
        $this->assertInstanceOf(
            'Base\Rule\Repository\UnAuditedRuleServiceSdkRepository',
            $this->unAuditedRuleService->getUnAuditedRuleServiceSdkRepository()
        );
    }

    public function testUnAuditedRuleServiceConstructor()
    {
        $this->assertEquals(0, $this->unAuditedRuleService->getApplyId());
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->unAuditedRuleService->getPublishCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->unAuditedRuleService->getUserGroup()
        );
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->unAuditedRuleService->getApplyCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->unAuditedRuleService->getApplyUserGroup()
        );
        $this->assertEquals(
            IApproveAble::OPERATION_TYPE['NULL'],
            $this->unAuditedRuleService->getOperationType()
        );
        $this->assertEquals(
            Template::CATEGORY['BJ'],
            $this->unAuditedRuleService->getTransformationCategory()
        );
        $this->assertEquals(
            Template::CATEGORY['WBJ'],
            $this->unAuditedRuleService->getSourceCategory()
        );
        $this->assertInstanceOf(
            'Base\Template\Model\Template',
            $this->unAuditedRuleService->getTransformationTemplate()
        );
        $this->assertInstanceOf(
            'Base\Template\Model\Template',
            $this->unAuditedRuleService->getSourceTemplate()
        );
        $this->assertEquals(0, $this->unAuditedRuleService->getRelationId());
        $this->assertEmpty($this->unAuditedRuleService->getRejectReason());
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $this->unAuditedRuleService->getApplyStatus());
    }
    
    public function testSetApplyId()
    {
        $this->unAuditedRuleService->setApplyId(1);
        $this->assertEquals(1, $this->unAuditedRuleService->getApplyId());
    }
    
    //relationId 测试 ------------------------------------------------------- start
    /**
     * 设置 News setRelationId() 正确的传参类型,期望传值正确
     */
    public function testSetRelationIdCorrectType()
    {
        $this->unAuditedRuleService->setRelationId(1);
        $this->assertEquals(1, $this->unAuditedRuleService->getRelationId());
    }

    /**
     * 设置 News setRelationId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRelationIdWrongType()
    {
        $this->unAuditedRuleService->setRelationId(array(1, 2, 3));
    }
    //relationId 测试 -------------------------------------------------------   end

    //operationType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 News setOperationType() 是否符合预定范围
     *
     * @dataProvider operationTypeProvider
     */
    public function testSetOperationType($actual, $expected)
    {
        $this->unAuditedRuleService->setOperationType($actual);
        $this->assertEquals($expected, $this->unAuditedRuleService->getOperationType());
    }

    /**
     * 循环测试 News setOperationType() 数据构建器
     */
    public function operationTypeProvider()
    {
        return array(
            array(IApproveAble::OPERATION_TYPE['ADD'],IApproveAble::OPERATION_TYPE['ADD']),
            array(IApproveAble::OPERATION_TYPE['EDIT'],IApproveAble::OPERATION_TYPE['EDIT']),
            array(999,IApproveAble::OPERATION_TYPE['NULL']),
        );
    }

    /**
     * 设置 News setOperationType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperationTypeWrongType()
    {
        $this->unAuditedRuleService->setOperationType('string');
    }
    //operationType 测试 ------------------------------------------------------   end

    //publishCrew 测试 --------------------------------------------- start
    /**
     * 设置 News setPublishCrew() 正确的传参类型,期望传值正确
     */
    public function testSetPublishCrewCorrectType()
    {
        $crew = new Crew();
        $this->unAuditedRuleService->setPublishCrew($crew);
        $this->assertSame($crew, $this->unAuditedRuleService->getPublishCrew());
    }

    /**
     * 设置 News setPublishCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPublishCrewWrongType()
    {
        $this->unAuditedRuleService->setPublishCrew('string');
    }
    //publishCrew 测试 ---------------------------------------------   end

    //applyCrew 测试 --------------------------------------------- start
    /**
     * 设置 News setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $crew = new Crew();
        $this->unAuditedRuleService->setApplyCrew($crew);
        $this->assertSame($crew, $this->unAuditedRuleService->getApplyCrew());
    }

    /**
     * 设置 News setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->unAuditedRuleService->setApplyCrew('string');
    }
    //applyCrew 测试 ---------------------------------------------   end

    //userGroup 测试 --------------------------------------------- start
    /**
     * 设置 News setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $userGroup = new UserGroup();
        $this->unAuditedRuleService->setUserGroup($userGroup);
        $this->assertSame($userGroup, $this->unAuditedRuleService->getUserGroup());
    }

    /**
     * 设置 News setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->unAuditedRuleService->setUserGroup('string');
    }
    //userGroup 测试 ---------------------------------------------   end

    //applyUserGroup 测试 --------------------------------------------- start
    /**
     * 设置 News setApplyUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetApplyUserGroupCorrectType()
    {
        $userGroup = new UserGroup();
        $this->unAuditedRuleService->setApplyUserGroup($userGroup);
        $this->assertSame($userGroup, $this->unAuditedRuleService->getApplyUserGroup());
    }

    /**
     * 设置 News setApplyUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyUserGroupWrongType()
    {
        $this->unAuditedRuleService->setApplyUserGroup('string');
    }
    //applyUserGroup 测试 ---------------------------------------------   end
    
    //transformationTemplate 测试 --------------------------------------------- start
    /**
     * 设置 News setTransformationTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTransformationTemplateCorrectType()
    {
        $template = new Template();
        $this->unAuditedRuleService->setTransformationTemplate($template);
        $this->assertSame($template, $this->unAuditedRuleService->getTransformationTemplate());
    }

    /**
     * 设置 News setTransformationTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransformationTemplateWrongType()
    {
        $this->unAuditedRuleService->setTransformationTemplate('string');
    }
    //transformationTemplate 测试 ---------------------------------------------   end

    //sourceTemplate 测试 --------------------------------------------- start
    /**
     * 设置 News setSourceTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetSourceTemplateCorrectType()
    {
        $wbjTemplate = new WbjTemplate();
        $this->unAuditedRuleService->setSourceTemplate($wbjTemplate);
        $this->assertSame($wbjTemplate, $this->unAuditedRuleService->getSourceTemplate());
    }

    /**
     * 设置 News setSourceTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceTemplateWrongType()
    {
        $this->unAuditedRuleService->setSourceTemplate('string');
    }
    //sourceTemplate 测试 ---------------------------------------------   end

    public function testResubmitAction()
    {
        //初始化
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                           ->setMethods(['getUnAuditedRuleServiceSdkRepository'])
                           ->getMock();

        //预言 INewsAdapter
        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->resubmit(
            Argument::exact($unAuditedRuleService),
            Argument::exact(
                [
                    'rules',
                    'crew'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $unAuditedRuleService->expects($this->once())
             ->method('getUnAuditedRuleServiceSdkRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $unAuditedRuleService->resubmitAction();
        $this->assertTrue($result);
    }

    public function testApproveAction()
    {
        //初始化
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                           ->setMethods(['getUnAuditedRuleServiceSdkRepository'])
                           ->getMock();

        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->approve(
            Argument::exact($unAuditedRuleService),
            Argument::exact(['applyCrew'])
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $unAuditedRuleService->expects($this->once())
             ->method('getUnAuditedRuleServiceSdkRepository')
             ->willReturn($repository->reveal());
        //验证
        $result = $unAuditedRuleService->approveAction();
        $this->assertTrue($result);
    }

    public function testRejectAction()
    {
        //初始化
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                           ->setMethods(['getUnAuditedRuleServiceSdkRepository'])
                           ->getMock();

        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->reject(
            Argument::exact($unAuditedRuleService),
            Argument::exact(['applyCrew', 'rejectReason'])
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $unAuditedRuleService->expects($this->once())
             ->method('getUnAuditedRuleServiceSdkRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $unAuditedRuleService->rejectAction();
        $this->assertTrue($result);
    }

    public function testRevokeSuccess()
    {
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                                ->setMethods(['getUnAuditedRuleServiceSdkRepository'])
                                ->getMock();
            
        $applyStatus = IApproveAble::APPLY_STATUS['PENDING'];

        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->revoke(
            Argument::exact($unAuditedRuleService),
            Argument::exact(['crew'])
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $unAuditedRuleService->expects($this->once())
             ->method('getUnAuditedRuleServiceSdkRepository')
             ->willReturn($repository->reveal());

        $result = $unAuditedRuleService->revoke();
        
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        $applyStatus = IApproveAble::APPLY_STATUS['REJECT'];
        $this->unAuditedRuleService->setApplyStatus($applyStatus);

        $result = $this->unAuditedRuleService->revoke();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('applyStatus', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }
}
