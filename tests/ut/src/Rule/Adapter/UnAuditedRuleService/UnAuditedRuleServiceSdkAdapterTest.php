<?php
namespace Base\Rule\Adapter\UnAuditedRuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Common\Model\IObject;

use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\Translator\UnAuditedRuleServiceSdkTranslator;

use BaseSdk\Rule\Model\UnAuditedRuleService as UnAuditedRuleServiceSdk;
use BaseSdk\Rule\Repository\UnAuditedRuleServiceRestfulRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditedRuleServiceSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods([
                               'fetchCrew',
                               'fetchApplyCrew',
                               'fetchUserGroup',
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIUnAuditedRuleServiceAdapter()
    {
        $adapter = new UnAuditedRuleServiceSdkAdapter();

        $this->assertInstanceOf(
            'Base\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'Base\Rule\Translator\UnAuditedRuleServiceSdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Repository\UnAuditedRuleServiceRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\Rule\Model\NullUnAuditedRuleService',
            $this->adapter->getNullObject()
        );
    }

    /**
     * 测试是否初始化 CrewRepository
     */
    public function testGetCrewRepository()
    {
        $adapter = new MockUnAuditedRuleServiceSdkAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }

    public function testGetUserGroupRepository()
    {
        $adapter = new MockUnAuditedRuleServiceSdkAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedUnAuditedRuleService = new UnAuditedRuleService();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedUnAuditedRuleService);
        $this->adapter->expects($this->exactly(1))->method('fetchCrew')->with($expectedUnAuditedRuleService);
        $this->adapter->expects($this->exactly(1))->method('fetchApplyCrew')->with($expectedUnAuditedRuleService);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedUnAuditedRuleService);

        //验证
        $unAuditedRuleService = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedUnAuditedRuleService, $unAuditedRuleService);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $unAuditedRuleServiceOne = new UnAuditedRuleService(1);
        $unAuditedRuleServiceTwo = new UnAuditedRuleService(2);

        $ids = [1, 2];

        $expectedUnAuditedList = [];
        $expectedUnAuditedList[$unAuditedRuleServiceOne->getId()] = $unAuditedRuleServiceOne;
        $expectedUnAuditedList[$unAuditedRuleServiceTwo->getId()] = $unAuditedRuleServiceTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedUnAuditedList);
        $this->adapter->expects($this->exactly(1))->method('fetchCrew')->with($expectedUnAuditedList);
        $this->adapter->expects($this->exactly(1))->method('fetchApplyCrew')->with($expectedUnAuditedList);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedUnAuditedList);

        //验证
        $unAuditedRuleServiceList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedUnAuditedList, $unAuditedRuleServiceList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $unAuditedRuleServiceOne = new UnAuditedRuleService(1);
        $unAuditedRuleServiceTwo = new UnAuditedRuleService(2);

        $expectedUnAuditedList = [];
        $expectedUnAuditedList[$unAuditedRuleServiceOne->getId()] = $unAuditedRuleServiceOne;
        $expectedUnAuditedList[$unAuditedRuleServiceTwo->getId()] = $unAuditedRuleServiceTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedUnAuditedList, 2]);

        $this->adapter->expects($this->exactly(1))->method('fetchCrew')->with($expectedUnAuditedList);
        $this->adapter->expects($this->exactly(1))->method('fetchApplyCrew')->with($expectedUnAuditedList);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedUnAuditedList);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedUnAuditedList, 2], $result);
    }

    private function initUnAuditedRuleService(bool $result, string $method)
    {
        $this->trait = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $unAuditedRuleService = new UnAuditedRuleService(1);
        $unAuditedRuleServiceSdk = new UnAuditedRuleServiceSdk(1);
        $keys = array();

        $translator = $this->prophesize(UnAuditedRuleServiceSdkTranslator::class);
        $translator->objectToValueObject(
            $unAuditedRuleService,
            $keys
        )->shouldBeCalledTimes(1)->willReturn($unAuditedRuleServiceSdk);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(UnAuditedRuleServiceRestfulRepository::class);
        $repository->$method($unAuditedRuleServiceSdk, $keys)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return [$unAuditedRuleService, $keys];
    }

    public function testResubmit()
    {
        list($unAuditedRuleService, $keys) = $this->initUnAuditedRuleService(true, 'resubmit');

        $result = $this->trait->resubmit($unAuditedRuleService, $keys);
        $this->assertTrue($result);
    }

    public function testResubmitFail()
    {
        list($unAuditedRuleService, $keys) = $this->initUnAuditedRuleService(false, 'resubmit');

        $result = $this->trait->resubmit($unAuditedRuleService, $keys);
        $this->assertFalse($result);
    }

    public function testApprove()
    {
        list($unAuditedRuleService, $keys) = $this->initUnAuditedRuleService(true, 'approve');

        $result = $this->trait->approve($unAuditedRuleService, $keys);
        $this->assertTrue($result);
    }

    public function testApproveFail()
    {
        list($unAuditedRuleService, $keys) = $this->initUnAuditedRuleService(false, 'approve');

        $result = $this->trait->approve($unAuditedRuleService, $keys);
        $this->assertFalse($result);
    }

    public function testReject()
    {
        list($unAuditedRuleService, $keys) = $this->initUnAuditedRuleService(true, 'reject');

        $result = $this->trait->reject($unAuditedRuleService, $keys);
        $this->assertTrue($result);
    }

    public function testRejectFail()
    {
        list($unAuditedRuleService, $keys) = $this->initUnAuditedRuleService(false, 'reject');

        $result = $this->trait->reject($unAuditedRuleService, $keys);
        $this->assertFalse($result);
    }

    public function testRevoke()
    {
        list($unAuditedRuleService, $keys) = $this->initUnAuditedRuleService(true, 'revoke');

        $result = $this->trait->revoke($unAuditedRuleService, $keys);
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        list($unAuditedRuleService, $keys) = $this->initUnAuditedRuleService(false, 'revoke');

        $result = $this->trait->revoke($unAuditedRuleService, $keys);
        $this->assertFalse($result);
    }

    //fetchCrew
    public function testFetchCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByObject'
                                ]
                           )
                           ->getMock();
        
        $unAuditedRuleService = new UnAuditedRuleService();

        $adapter->expects($this->exactly(1))->method('fetchCrewByObject')->with($unAuditedRuleService);

        $adapter->fetchCrew($unAuditedRuleService);
    }

    public function testFetchCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByList'
                                ]
                           )
                           ->getMock();
        
        $unAuditedRuleServiceOne = new UnAuditedRuleService(1);
        $unAuditedRuleServiceTwo = new UnAuditedRuleService(2);
        
        $unAuditedRuleServiceList[$unAuditedRuleServiceOne->getId()] = $unAuditedRuleServiceOne;
        $unAuditedRuleServiceList[$unAuditedRuleServiceTwo->getId()] = $unAuditedRuleServiceTwo;

        $adapter->expects($this->exactly(1))->method('fetchCrewByList')->with($unAuditedRuleServiceList);

        $adapter->fetchCrew($unAuditedRuleServiceList);
    }

    //fetchCrewByObject
    public function testFetchCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $crewId = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($crewId);
        $unAuditedRuleService = new UnAuditedRuleService();
        $unAuditedRuleService->setCrew($crew);
        $unAuditedRuleService->setPublishCrew($crew);
        
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))->shouldBeCalledTimes(1)->willReturn($crew);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByObject($unAuditedRuleService);
    }

    //fetchCrewByList
    public function testFetchCrewByList()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $unAuditedRuleServiceOne = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);
        $crewOne = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $unAuditedRuleServiceOne->setCrew($crewOne);
        $unAuditedRuleServiceOne->setPublishCrew($crewOne);

        $unAuditedRuleServiceTwo = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService(2);
        $crewTwo = \Base\Crew\Utils\MockFactory::generateCrew(2);
        $unAuditedRuleServiceTwo->setCrew($crewTwo);
        $unAuditedRuleServiceTwo->setPublishCrew($crewTwo);
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $unAuditedRuleServiceList[$unAuditedRuleServiceOne->getId()] = $unAuditedRuleServiceOne;
        $unAuditedRuleServiceList[$unAuditedRuleServiceTwo->getId()] = $unAuditedRuleServiceTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))->shouldBeCalledTimes(1)->willReturn($crewList);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByList($unAuditedRuleServiceList);
    }

    //fetchUserGroup
    public function testFetchUserGroupIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchUserGroupByObject'
                                ]
                           )
                           ->getMock();
        
        $unAuditedRuleService = new UnAuditedRuleService();

        $adapter->expects($this->exactly(1))->method('fetchUserGroupByObject')->with($unAuditedRuleService);

        $adapter->fetchUserGroup($unAuditedRuleService);
    }

    public function testFetchUserGroupIsArray()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchUserGroupByList'
                                ]
                           )
                           ->getMock();
        
        $unAuditedRuleServiceOne = new UnAuditedRuleService(1);
        $unAuditedRuleServiceTwo = new UnAuditedRuleService(2);
        
        $unAuditedRuleServiceList[$unAuditedRuleServiceOne->getId()] = $unAuditedRuleServiceOne;
        $unAuditedRuleServiceList[$unAuditedRuleServiceTwo->getId()] = $unAuditedRuleServiceTwo;

        $adapter->expects($this->exactly(1))->method('fetchUserGroupByList')->with($unAuditedRuleServiceList);

        $adapter->fetchUserGroup($unAuditedRuleServiceList);
    }

    //fetchUserGroupByObject
    public function testFetchUserGroupByObject()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $userGroupId = 1;
        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup($userGroupId);
        $unAuditedRuleService = new UnAuditedRuleService();
        $unAuditedRuleService->setUserGroup($userGroup);
        
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))->shouldBeCalledTimes(1)->willReturn($userGroup);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByObject($unAuditedRuleService);
    }

    //fetchUserGroupByList
    public function testFetchUserGroupByList()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $unAuditedRuleServiceOne = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);
        $userGroupOne = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);
        $unAuditedRuleServiceOne->setUserGroup($userGroupOne);

        $unAuditedRuleServiceTwo = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService(2);
        $userGroupTwo = \Base\UserGroup\Utils\MockFactory::generateUserGroup(2);
        $unAuditedRuleServiceTwo->setUserGroup($userGroupTwo);
        $userGroupIds = [1, 2];
        
        $userGroupList[$userGroupOne->getId()] = $userGroupOne;
        $userGroupList[$userGroupTwo->getId()] = $userGroupTwo;
        
        $unAuditedRuleServiceList[$unAuditedRuleServiceOne->getId()] = $unAuditedRuleServiceOne;
        $unAuditedRuleServiceList[$unAuditedRuleServiceTwo->getId()] = $unAuditedRuleServiceTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(
            Argument::exact($userGroupIds)
        )->shouldBeCalledTimes(1)->willReturn($userGroupList);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByList($unAuditedRuleServiceList);
    }

    //fetchApplyCrew
    public function testFetchApplyCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchApplyCrewByObject'
                                ]
                           )
                           ->getMock();
        
        $unAuditedRuleService = new UnAuditedRuleService();

        $adapter->expects($this->exactly(1))->method('fetchApplyCrewByObject')->with($unAuditedRuleService);

        $adapter->fetchApplyCrew($unAuditedRuleService);
    }

    public function testFetchApplyCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchApplyCrewByList'
                                ]
                           )
                           ->getMock();
        
        $unAuditedRuleServiceOne = new UnAuditedRuleService(1);
        $unAuditedRuleServiceTwo = new UnAuditedRuleService(2);
        
        $unAuditedRuleServiceList[$unAuditedRuleServiceOne->getId()] = $unAuditedRuleServiceOne;
        $unAuditedRuleServiceList[$unAuditedRuleServiceTwo->getId()] = $unAuditedRuleServiceTwo;

        $adapter->expects($this->exactly(1))->method('fetchApplyCrewByList')->with($unAuditedRuleServiceList);

        $adapter->fetchApplyCrew($unAuditedRuleServiceList);
    }

    //fetchApplyCrewByObject
    public function testFetchApplyCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $applyCrewId = 1;
        $applyCrew = \Base\Crew\Utils\MockFactory::generateCrew($applyCrewId);
        $unAuditedRuleService = new UnAuditedRuleService();
        $unAuditedRuleService->setApplyCrew($applyCrew);
        $unAuditedRuleService->setApplyUserGroup($applyCrew->getUserGroup());
        
        $applyCrewRepository = $this->prophesize(CrewRepository::class);
        $applyCrewRepository->fetchOne(Argument::exact($applyCrewId))->shouldBeCalledTimes(1)->willReturn($applyCrew);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($applyCrewRepository->reveal());

        $adapter->fetchApplyCrewByObject($unAuditedRuleService);
    }

    //fetchApplyCrewByList
    public function testFetchApplyCrewByList()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $unAuditedRuleServiceOne = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);
        $applyCrewOne = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $unAuditedRuleServiceOne->setApplyCrew($applyCrewOne);
        $unAuditedRuleServiceOne->setApplyUserGroup($applyCrewOne->getUserGroup());

        $unAuditedRuleServiceTwo = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService(2);
        $applyCrewTwo = \Base\Crew\Utils\MockFactory::generateCrew(2);
        $unAuditedRuleServiceTwo->setApplyCrew($applyCrewTwo);
        $unAuditedRuleServiceTwo->setApplyUserGroup($applyCrewTwo->getUserGroup());
        $applyCrewIds = [1, 2];
        
        $applyCrewList[$applyCrewOne->getId()] = $applyCrewOne;
        $applyCrewList[$applyCrewTwo->getId()] = $applyCrewTwo;
        
        $unAuditedRuleServiceList[$unAuditedRuleServiceOne->getId()] = $unAuditedRuleServiceOne;
        $unAuditedRuleServiceList[$unAuditedRuleServiceTwo->getId()] = $unAuditedRuleServiceTwo;

        $applyCrewRepository = $this->prophesize(CrewRepository::class);
        $applyCrewRepository->fetchList(
            Argument::exact($applyCrewIds)
        )->shouldBeCalledTimes(1)->willReturn($applyCrewList);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($applyCrewRepository->reveal());

        $adapter->fetchApplyCrewByList($unAuditedRuleServiceList);
    }
}
