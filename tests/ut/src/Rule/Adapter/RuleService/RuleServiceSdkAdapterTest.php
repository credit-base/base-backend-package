<?php
namespace Base\Rule\Adapter\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Common\Model\IObject;

use Base\Rule\Model\RuleService;
use Base\Rule\Translator\RuleServiceSdkTranslator;

use BaseSdk\Rule\Model\RuleService as RuleServiceSdk;
use BaseSdk\Rule\Repository\RuleServiceRestfulRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceSdkAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods([
                               'fetchCrew',
                               'fetchUserGroup',
                               'fetchOneAction',
                               'fetchListAction',
                               'filterAction',
                               'addAction',
                               'editAction'
                               ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIRuleServiceAdapter()
    {
        $adapter = new RuleServiceSdkAdapter();

        $this->assertInstanceOf(
            'Base\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->adapter->getTranslator()
        );

        $this->assertInstanceOf(
            'Base\Rule\Translator\RuleServiceSdkTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetRestfulRepository()
    {
        $this->assertInstanceOf(
            'BaseSdk\Rule\Repository\RuleServiceRestfulRepository',
            $this->adapter->getRestfulRepository()
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Base\Rule\Model\NullRuleService',
            $this->adapter->getNullObject()
        );
    }

    /**
     * 测试是否初始化 CrewRepository
     */
    public function testGetCrewRepository()
    {
        $adapter = new MockRuleServiceSdkAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }

    public function testGetUserGroupRepository()
    {
        $adapter = new MockRuleServiceSdkAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }

    public function testGetMapErrors()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods(['commonMapErrors'])
                           ->getMock();

        $commonMapErrors = array(1);

        $adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [];
        $result = $adapter->getMapErrors();

        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedRuleService = new RuleService();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedRuleService);
        $this->adapter->expects($this->exactly(1))->method('fetchCrew')->with($expectedRuleService);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedRuleService);

        //验证
        $ruleService = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedRuleService, $ruleService);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $ruleServiceOne = new RuleService(1);
        $ruleServiceTwo = new RuleService(2);

        $ids = [1, 2];

        $expectedRuleServiceList = [];
        $expectedRuleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $expectedRuleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedRuleServiceList);
        $this->adapter->expects($this->exactly(1))->method('fetchCrew')->with($expectedRuleServiceList);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedRuleServiceList);

        //验证
        $ruleServiceList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedRuleServiceList, $ruleServiceList);
    }

    //filter
    public function testFilter()
    {
        //初始化
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 2;
        $ruleServiceOne = new RuleService(1);
        $ruleServiceTwo = new RuleService(2);

        $expectedRuleServiceList = [];
        $expectedRuleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $expectedRuleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('filterAction')
                         ->with($filter, $sort, $number, $size)
                         ->willReturn([$expectedRuleServiceList, 2]);

        $this->adapter->expects($this->exactly(1))->method('fetchCrew')->with($expectedRuleServiceList);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedRuleServiceList);

        //验证
        $result = $this->adapter->filter($filter, $sort, $number, $size);

        $this->assertEquals([$expectedRuleServiceList, 2], $result);
    }

    //add
    public function testAdd()
    {
        //初始化
        $ruleService = new RuleService();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($ruleService)->willReturn(true);

        //验证
        $result = $this->adapter->add($ruleService);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $ruleService = new RuleService();
        $keys = array();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('editAction')->with($ruleService, $keys)->willReturn(true);

        //验证
        $result = $this->adapter->edit($ruleService, $keys);
        $this->assertTrue($result);
    }

    private function initRuleService(bool $result)
    {
        $this->trait = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                      ->setMethods(['getTranslator', 'getRestfulRepository', 'mapErrors'])
                      ->getMock();

        $ruleService = new RuleService(1);
        $ruleServiceSdk = new RuleServiceSdk(1);
        $keys = array();

        $translator = $this->prophesize(RuleServiceSdkTranslator::class);
        $translator->objectToValueObject($ruleService, $keys)->shouldBeCalledTimes(1)->willReturn($ruleServiceSdk);
        $this->trait->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $repository = $this->prophesize(RuleServiceRestfulRepository::class);
        $repository->deletes($ruleServiceSdk, $keys)->shouldBeCalledTimes(1)->willReturn($result);
        $this->trait->expects($this->exactly(1))->method('getRestfulRepository')->willReturn($repository->reveal());

        if (!$result) {
            $this->trait->expects($this->once())->method('mapErrors');
        }
        return [$ruleService, $keys];
    }

    public function testDeletes()
    {
        list($ruleService, $keys) = $this->initRuleService(true);

        $result = $this->trait->deletes($ruleService, $keys);
        $this->assertTrue($result);
    }

    public function testDeletesFail()
    {
        list($ruleService, $keys) = $this->initRuleService(false);

        $result = $this->trait->deletes($ruleService, $keys);
        $this->assertFalse($result);
    }

    //fetchCrew
    public function testFetchCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByObject'
                                ]
                           )
                           ->getMock();
        
        $ruleService = new RuleService();

        $adapter->expects($this->exactly(1))->method('fetchCrewByObject')->with($ruleService);

        $adapter->fetchCrew($ruleService);
    }

    public function testFetchCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchCrewByList'
                                ]
                           )
                           ->getMock();
        
        $ruleServiceOne = new RuleService(1);
        $ruleServiceTwo = new RuleService(2);
        
        $ruleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $ruleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        $adapter->expects($this->exactly(1))->method('fetchCrewByList')->with($ruleServiceList);

        $adapter->fetchCrew($ruleServiceList);
    }

    //fetchCrewByObject
    public function testFetchCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $crewId = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($crewId);
        $ruleService = new RuleService();
        $ruleService->setCrew($crew);
        
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))->shouldBeCalledTimes(1)->willReturn($crew);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByObject($ruleService);
    }

    //fetchCrewByList
    public function testFetchCrewByList()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getCrewRepository'
                                ]
                           )
                           ->getMock();

        $ruleServiceOne = \Base\Rule\Utils\MockFactory::generateRuleService(1);
        $crewOne = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $ruleServiceOne->setCrew($crewOne);

        $ruleServiceTwo = \Base\Rule\Utils\MockFactory::generateRuleService(2);
        $crewTwo = \Base\Crew\Utils\MockFactory::generateCrew(2);
        $ruleServiceTwo->setCrew($crewTwo);
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $ruleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $ruleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))->shouldBeCalledTimes(1)->willReturn($crewList);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByList($ruleServiceList);
    }

    //fetchUserGroup
    public function testFetchUserGroupIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchUserGroupByObject'
                                ]
                           )
                           ->getMock();
        
        $ruleService = new RuleService();

        $adapter->expects($this->exactly(1))->method('fetchUserGroupByObject')->with($ruleService);

        $adapter->fetchUserGroup($ruleService);
    }

    public function testFetchUserGroupIsArray()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'fetchUserGroupByList'
                                ]
                           )
                           ->getMock();
        
        $ruleServiceOne = new RuleService(1);
        $ruleServiceTwo = new RuleService(2);
        
        $ruleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $ruleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        $adapter->expects($this->exactly(1))->method('fetchUserGroupByList')->with($ruleServiceList);

        $adapter->fetchUserGroup($ruleServiceList);
    }

    //fetchUserGroupByObject
    public function testFetchUserGroupByObject()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $userGroupId = 1;
        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup($userGroupId);
        $ruleService = new RuleService();
        $ruleService->setUserGroup($userGroup);
        
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))->shouldBeCalledTimes(1)->willReturn($userGroup);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByObject($ruleService);
    }

    //fetchUserGroupByList
    public function testFetchUserGroupByList()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceSdkAdapter::class)
                           ->setMethods(
                               [
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();

        $ruleServiceOne = \Base\Rule\Utils\MockFactory::generateRuleService(1);
        $userGroupOne = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);
        $ruleServiceOne->setUserGroup($userGroupOne);

        $ruleServiceTwo = \Base\Rule\Utils\MockFactory::generateRuleService(2);
        $userGroupTwo = \Base\UserGroup\Utils\MockFactory::generateUserGroup(2);
        $ruleServiceTwo->setUserGroup($userGroupTwo);
        $userGroupIds = [1, 2];
        
        $userGroupList[$userGroupOne->getId()] = $userGroupOne;
        $userGroupList[$userGroupTwo->getId()] = $userGroupTwo;
        
        $ruleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $ruleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(
            Argument::exact($userGroupIds)
        )->shouldBeCalledTimes(1)->willReturn($userGroupList);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByList($ruleServiceList);
    }
}
