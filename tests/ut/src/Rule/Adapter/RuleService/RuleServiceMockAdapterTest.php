<?php
namespace Base\Rule\Adapter\RuleService;

use PHPUnit\Framework\TestCase;

use Base\Rule\Model\RuleService;

class RuleServiceMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new RuleServiceMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new RuleService()));
    }

    public function testDeletes()
    {
        $this->assertTrue($this->adapter->deletes(new RuleService(), array()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new RuleService(), array()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Rule\Model\RuleService',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Rule\Model\RuleService',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Rule\Model\RuleService',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
