<?php
namespace Base\Rule\Controller;

use PHPUnit\Framework\TestCase;

class RuleServiceFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockRuleServiceFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Rule\Repository\RuleServiceSdkRepository',
            $this->controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $this->controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $this->assertInstanceOf(
            'Base\Rule\View\RuleServiceView',
            $this->controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $this->assertEquals(
            'rules',
            $this->controller->getResourceName()
        );
    }
}
