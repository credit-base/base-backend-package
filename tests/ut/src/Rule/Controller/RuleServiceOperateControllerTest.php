<?php
namespace Base\Rule\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Template\Model\Template;

use Base\Rule\Repository\RuleServiceSdkRepository;
use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;
use Base\Rule\Command\RuleService\AddRuleServiceCommand;
use Base\Rule\Command\RuleService\EditRuleServiceCommand;
use Base\Rule\Command\RuleService\DeleteRuleServiceCommand;

class RuleServiceOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new RuleServiceOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    private function rulesData() : array
    {
        return array(
            "type"=>"rules",
            "attributes"=>array(
                "rules"=>array(
                    'transformationRule' => array(
                        "TYSHXYDM" => "TYSHXYDM",
                        "ZTMC" => "ZTMC",
                    ),
                    'completionRule' =>  array(
                        'ZTMC' => array(
                            array('id'=>1, 'base'=> array(1), 'item'=>'ZTMC'),
                            array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                        ),
                        'TYSHXYDM' => array(
                            array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                            array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                        ),
                    ),
                    'comparisonRule' =>  array(
                        'ZTMC' => array(
                            array('id'=>1, 'base'=> array(1), 'item'=>'ZTMC'),
                            array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                        ),
                        'TYSHXYDM' => array(
                            array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                            array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                        ),
                    ),
                    'deDuplicationRule' => array("result"=>1, "items"=>"TYSHXYDM")
                ),
                "transformationCategory" => 1,
                "sourceCategory" => 1
            ),
            "relationships"=>array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>1)
                    )
                ),
                "transformationTemplate"=>array(
                    "data"=>array(
                        array("type"=>"gbTemplates","id"=>1)
                    )
                ),
                "sourceTemplate"=>array(
                    "data"=>array(
                        array("type"=>"wbjTemplates","id"=>1)
                    )
                ),
            )
        );
    }
    /**
     * 测试 add 成功
     * 1. 为 RuleServiceOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getRuleServiceWidgetRule、getCommandBus、getUnAuditedRuleServiceSdkRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 RuleService 类建立预言
     * 4. 为 UnAuditedRuleServiceSdkRepository 类建立预言, UnAuditedRuleServiceSdkRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的RuleService, getUnAuditedRuleServiceSdkRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 为 RuleServiceOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getRuleServiceWidgetRule、getCommandBus、getUnAuditedRuleServiceSdkRepository、render方法
        $controller = $this->getMockBuilder(RuleServiceOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateCommonScenario',
                    'validateAddScenario',
                    'getCommandBus',
                    'getUnAuditedRuleServiceSdkRepository',
                    'render'
                ]
            )->getMock();

        $data = $this->rulesData();
        // 调用 $this->initAdd(), 期望结果为 true
        $command = $this->initAdd($controller, true, $data);

        // 为 RuleService 类建立预言
        $unAuditedRuleService = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService($command->id);

        // 为 UnAuditedRuleServiceSdkRepository 类建立预言, UnAuditedRuleServiceSdkRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的RuleService, getUnAuditedRuleServiceSdkRepository 方法被调用一次
        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedRuleService);
        $controller->expects($this->once())
                             ->method('getUnAuditedRuleServiceSdkRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->add 方法被调用一次, 且返回结果为 true
        $result = $controller->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 RuleServiceOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getRuleServiceWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 为 RuleServiceOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getRuleServiceWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(RuleServiceOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateCommonScenario',
                    'validateAddScenario',
                    'getCommandBus',
                    'getUnAuditedRuleServiceSdkRepository',
                    'displayError'
                ]
            )->getMock();

        $data = $this->rulesData();
        $data['attributes']['sourceCategory'] = 10;
        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd($controller, false, $data);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->add 方法被调用一次, 且返回结果为 false
        $result = $controller->add();
        $this->assertFalse($result);
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 RuleServiceWidgetRule 类建立预言, 验证请求参数, getRuleServiceWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddRuleServiceCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(RuleServiceOperateController $controller, bool $result, array $data)
    {
        // mock 请求参数

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $rules = $attributes['rules'];
        $transformationCategory = $attributes['transformationCategory'];
        $sourceCategory = $attributes['sourceCategory'];

        $crew = $relationships['crew']['data'][0]['id'];
        $transformationTemplate = $relationships['transformationTemplate']['data'][0]['id'];
        $sourceTemplate = $relationships['sourceTemplate']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('validateAddScenario')
            ->willReturn(true);

        $command = new AddRuleServiceCommand(
            $rules,
            $transformationTemplate,
            $sourceTemplate,
            $transformationCategory,
            $sourceCategory,
            $crew
        );
        // 为 CommandBus 类建立预言, 传入 AddRuleServiceCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
            
        return $command;
    }

    /**
     * 测试 edit 成功
     * 1. 为 RuleServiceOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getRuleServiceWidgetRule、getCommandBus、getUnAuditedRuleServiceSdkRepository、render方法
     * 2. 调用 $this->initEdit(), 期望结果为 true
     * 3. 为 RuleService 类建立预言
     * 4. 为 UnAuditedRuleServiceSdkRepository 类建立预言, UnAuditedRuleServiceSdkRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的RuleService, getUnAuditedRuleServiceSdkRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 RuleServiceOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getRuleServiceWidgetRule、getCommandBus、getUnAuditedRuleServiceSdkRepository、render方法
        $controller = $this->getMockBuilder(RuleServiceOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateCommonScenario',
                    'getCommandBus',
                    'getUnAuditedRuleServiceSdkRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;

        $command = $this->initEdit($controller, $id, true);

        // 为 RuleService 类建立预言
        $unAuditedRuleService = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService($command->id);

        // 为 UnAuditedRuleServiceSdkRepository 类建立预言, UnAuditedRuleServiceSdkRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的RuleService, getUnAuditedRuleServiceSdkRepository 方法被调用一次
        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedRuleService);
        $controller->expects($this->once())
                             ->method('getUnAuditedRuleServiceSdkRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->edit 方法被调用一次, 且返回结果为 true
        $result = $controller->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 RuleServiceOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getRuleServiceWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initEdit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 RuleServiceOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getRuleServiceWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(RuleServiceOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateCommonScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 false
        $this->initEdit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->edit 方法被调用一次, 且返回结果为 false
        $result = $controller->edit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 edit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 RuleServiceWidgetRule 类建立预言, 验证请求参数, getRuleServiceWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 EditRuleServiceCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEdit(RuleServiceOperateController $editController, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->rulesData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $rules = $attributes['rules'];

        $crew = $relationships['crew']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $editController->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $editController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->willReturn(true);

        $command = new EditRuleServiceCommand(
            $rules,
            $id,
            $crew
        );
        // 为 CommandBus 类建立预言, 传入 EditRuleServiceCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $editController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());

        return $command;
    }

    private function initialDelete($result)
    {
        $this->ruleServiceStub = $this->getMockBuilder(RuleServiceOperateController::class)
                    ->setMethods([
                        'getRequest',
                        'validateStatusScenario',
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $crewId = 2;

        $data = array(
            'relationships' => array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$crewId)
                    )
                )
            ),
        );

        $this->ruleServiceStub->expects($this->exactly(1))
            ->method('validateStatusScenario')
            ->willReturn(true);
            
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->ruleServiceStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());
            
        $command = new DeleteRuleServiceCommand($crewId, $id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->ruleServiceStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDeleteFailure()
    {
        $command = $this->initialDelete(false);

        $this->ruleServiceStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->ruleServiceStub->delete($command->id);
        $this->assertFalse($result);
    }

    public function testDeleteSuccess()
    {
        $command = $this->initialDelete(true);

        $ruleService = \Base\Rule\Utils\MockFactory::generateRuleService($command->id);

        $repository = $this->prophesize(RuleServiceSdkRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($ruleService);

        $this->ruleServiceStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->ruleServiceStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->ruleServiceStub->delete($command->id);
        $this->assertTrue($result);
    }
}
