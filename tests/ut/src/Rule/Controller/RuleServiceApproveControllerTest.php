<?php
namespace Base\Rule\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\Response;
use Marmot\Framework\Classes\CommandBus;

use Base\Rule\Utils\MockFactory;
use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;
use Base\Rule\Command\RuleService\ApproveRuleServiceCommand;
use Base\Rule\Command\RuleService\RejectRuleServiceCommand;

class RuleServiceApproveControllerTest extends TestCase
{
    private $ruleServiceStub;

    public function setUp()
    {
        $this->ruleServiceStub = new RuleServiceApproveController();
    }

    public function teatDown()
    {
        unset($this->ruleServiceStub);
    }

    public function testImplementsIApproveAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IApproveAbleController',
            $this->ruleServiceStub
        );
    }

    private function initialApprove($result)
    {
        $this->ruleServiceStub = $this->getMockBuilder(RuleServiceApproveController::class)
                    ->setMethods([
                        'getRequest',
                        'validateApproveScenario',
                        'getCommandBus',
                        'getUnAuditedRuleServiceSdkRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $applyCrewId = 2;

        $data = array(
            'relationships' => array(
                "applyCrew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$applyCrewId)
                    )
                )
            ),
        );

        $this->ruleServiceStub->expects($this->exactly(1))
            ->method('validateApproveScenario')
            ->willReturn(true);
            
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->ruleServiceStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $command = new ApproveRuleServiceCommand($applyCrewId, $id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->ruleServiceStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testApproveFailure()
    {
        $command = $this->initialApprove(false);

        $this->ruleServiceStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->ruleServiceStub->approve($command->id);
        $this->assertFalse($result);
    }

    public function testApproveSuccess()
    {
        $command = $this->initialApprove(true);

        $ruleService = MockFactory::generateUnAuditedRuleService($command->id);

        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($ruleService);

        $this->ruleServiceStub->expects($this->exactly(1))
             ->method('getUnAuditedRuleServiceSdkRepository')
             ->willReturn($repository->reveal());
             
        $this->ruleServiceStub->expects($this->exactly(1))->method('render')->willReturn(true);

        $result = $this->ruleServiceStub->approve($command->id);
        $this->assertTrue($result);
    }

    private function initialReject($result)
    {
        $this->stub = $this->getMockBuilder(RuleServiceApproveController::class)
                    ->setMethods([
                        'getRequest',
                        'validateRejectScenario',
                        'getCommandBus',
                        'getUnAuditedRuleServiceSdkRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $applyCrew = 1;
        $rejectReason = 'rejectReason';

        $data = array(
            'attributes' => array(
                'rejectReason' => $rejectReason
            ),
            'relationships' => array(
                "applyCrew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$applyCrew)
                    )
                )
            ),
        );

        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->stub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('validateRejectScenario')
            ->willReturn(true);

        $command = new RejectRuleServiceCommand($applyCrew, $rejectReason, $id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->stub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testRejectSuccess()
    {
        $command = $this->initialReject(true);

        $ruleService = MockFactory::generateUnAuditedRuleService($command->id);

        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($ruleService);

        $this->stub->expects($this->exactly(1))
             ->method('getUnAuditedRuleServiceSdkRepository')
             ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))->method('render')->willReturn(true);

        $result = $this->stub->reject($command->id);
        $this->assertTrue($result);
    }

    public function testRejectFailure()
    {
        $command = $this->initialReject(false);

        $this->stub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->stub->reject($command->id);
        $this->assertFalse($result);
    }
}
