<?php
namespace Base\Rule\Controller;

use PHPUnit\Framework\TestCase;

class UnAuditedRuleServiceFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockUnAuditedRuleServiceFetchController();
        ;
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Rule\Repository\UnAuditedRuleServiceSdkRepository',
            $this->controller->getRepository()
        );

        $this->assertInstanceOf(
            'Base\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $this->controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $this->assertInstanceOf(
            'Base\Rule\View\UnAuditedRuleServiceView',
            $this->controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $this->assertEquals(
            'unAuditedRules',
            $this->controller->getResourceName()
        );
    }
}
