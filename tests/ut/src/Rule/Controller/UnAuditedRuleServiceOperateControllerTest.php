<?php
namespace Base\Rule\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;
use Base\Rule\Command\RuleService\RevokeRuleServiceCommand;

class UnAuditedRuleServiceOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new UnAuditedRuleServiceOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    private function initialRevoke($result)
    {
        $this->ruleServiceStub = $this->getMockBuilder(UnAuditedRuleServiceOperateController::class)
                    ->setMethods([
                        'getRequest',
                        'validateStatusScenario',
                        'getCommandBus',
                        'getUnAuditedRuleServiceSdkRepository',
                        'render',
                        'displayError'
                    ])->getMock();
                    
        $id = $crewId = 1;

        $data = array(
            'relationships' => array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>$crewId)
                    )
                )
            ),
        );

        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $this->ruleServiceStub->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->ruleServiceStub->expects($this->exactly(1))
            ->method('validateStatusScenario')
            ->willReturn(true);
            
        $command = new RevokeRuleServiceCommand($crewId, $id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->ruleServiceStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testRevokeFailure()
    {
        $command = $this->initialRevoke(false);

        $this->ruleServiceStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->ruleServiceStub->revoke($command->id);
        $this->assertFalse($result);
    }

    public function testRevokeSuccess()
    {
        $command = $this->initialRevoke(true);

        $ruleService = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService($command->id);

        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($ruleService);

        $this->ruleServiceStub->expects($this->exactly(1))
             ->method('getUnAuditedRuleServiceSdkRepository')
             ->willReturn($repository->reveal());
             
        $this->ruleServiceStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->ruleServiceStub->revoke($command->id);
        $this->assertTrue($result);
    }
}
