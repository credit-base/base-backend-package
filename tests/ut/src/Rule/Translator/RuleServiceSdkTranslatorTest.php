<?php
namespace Base\Rule\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Rule\Utils\RuleServiceSdkUtils;
use BaseSdk\Rule\Model\RuleService as RuleServiceSdk;

use Base\Template\Model\Template;
use Base\Template\Translator\TranslatorFactory;
use Base\Template\Translator\BjTemplateSdkTranslator;
use Base\Template\Translator\WbjTemplateSdkTranslator;
use BaseSdk\Template\Model\BjTemplate as BjTemplateSdk;
use BaseSdk\Template\Model\WbjTemplate as WbjTemplateSdk;

use BaseSdk\Crew\Model\Crew as CrewSdk;
use Base\Crew\Translator\CrewSdkTranslator;

use BaseSdk\UserGroup\Model\UserGroup as UserGroupSdk;
use Base\UserGroup\Translator\UserGroupSdkTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceSdkTranslatorTest extends TestCase
{
    use RuleServiceSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(RuleServiceSdkTranslator::class)
                    ->setMethods([
                        'getCrewSdkTranslator',
                        'getUserGroupSdkTranslator',
                        'getTranslatorFactory',
                        'getTemplateSdkTranslator'
                    ])
                    ->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsISdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $this->translator
        );
    }

    public function testGetCrewSdkTranslator()
    {
        $translator = new MockRuleServiceSdkTranslator();
        $this->assertInstanceOf(
            'Base\Crew\Translator\CrewSdkTranslator',
            $translator->getCrewSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getCrewSdkTranslator()
        );
    }

    public function testGetUserGroupSdkTranslator()
    {
        $translator = new MockRuleServiceSdkTranslator();
        $this->assertInstanceOf(
            'Base\UserGroup\Translator\UserGroupSdkTranslator',
            $translator->getUserGroupSdkTranslator()
        );

        $this->assertInstanceOf(
            'Base\Common\Translator\ISdkTranslator',
            $translator->getUserGroupSdkTranslator()
        );
    }

    public function testGetTranslatorFactory()
    {
        $translator = new MockRuleServiceSdkTranslator();

        $this->assertInstanceOf(
            'Base\Template\Translator\TranslatorFactory',
            $translator->getTranslatorFactory()
        );
    }

    public function testGetTemplateSdkTranslator()
    {
        $translator = $this->getMockBuilder(MockRuleServiceSdkTranslator::class)
                                 ->setMethods(['getTranslatorFactory'])
                                 ->getMock();

        $bjTemplateTranslator = new BjTemplateSdkTranslator();
        $category = Template::CATEGORY['BJ'];

        $translatorFactory = $this->prophesize(TranslatorFactory::class);
        $translatorFactory->getTranslator(Argument::exact($category))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($bjTemplateTranslator);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getTranslatorFactory')
            ->willReturn($translatorFactory->reveal());

        $result = $translator->getTemplateSdkTranslator($category);
        $this->assertInstanceof('Base\Common\Translator\ISdkTranslator', $result);
        $this->assertEquals($bjTemplateTranslator, $result);
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $ruleService = \Base\Rule\Utils\MockFactory::generateRuleService(1);

        $crewSdk = new CrewSdk($ruleService->getCrew()->getId());
        $userGroupSdk = new UserGroupSdk($ruleService->getUserGroup()->getId());
        $sourceTemplateSdk = new WbjTemplateSdk($ruleService->getSourceTemplate()->getId());
        $transformationTemplateSdk = new BjTemplateSdk($ruleService->getTransformationTemplate()->getId());

        $ruleServiceSdk = new RuleServiceSdk(
            $ruleService->getId(),
            $ruleService->getSourceCategory(),
            $sourceTemplateSdk,
            $ruleService->getTransformationCategory(),
            $transformationTemplateSdk,
            $ruleService->getRules(),
            $ruleService->getVersion(),
            $ruleService->getDataTotal(),
            $crewSdk,
            $userGroupSdk,
            $ruleService->getStatus(),
            $ruleService->getStatusTime(),
            $ruleService->getCreateTime(),
            $ruleService->getUpdateTime()
        );

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->valueObjectToObject(Argument::exact($crewSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($ruleService->getCrew());

        $sourceTemplateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $sourceTemplateSdkTranslator->valueObjectToObject(Argument::exact($sourceTemplateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($ruleService->getSourceTemplate());

        $transformationSdkTranslator = $this->prophesize(WbjTemplateSdkTranslator::class);
        $transformationSdkTranslator->valueObjectToObject(Argument::exact($transformationTemplateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($ruleService->getTransformationTemplate());

        $this->translator->expects($this->exactly(2))
                   ->method('getTemplateSdkTranslator')
                   ->will($this->returnValueMap(
                       [
                        [$ruleServiceSdk->getSourceCategory(), $sourceTemplateSdkTranslator->reveal()],
                        [$ruleServiceSdk->getTransformationCategory(), $transformationSdkTranslator->reveal()]
                       ]
                   ));

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->valueObjectToObject(Argument::exact($userGroupSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($ruleService->getUserGroup());
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());

        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $ruleServiceObject = $this->translator->valueObjectToObject($ruleServiceSdk);
        $this->assertInstanceof('Base\Rule\Model\RuleService', $ruleServiceObject);
        $this->compareValueObjectAndObjectRuleService($ruleServiceSdk, $ruleServiceObject);
    }

    public function testValueObjectToObjectFail()
    {
        $ruleServiceSdk = array();

        $ruleService = $this->translator->valueObjectToObject($ruleServiceSdk);
        $this->assertInstanceof('Base\Rule\Model\NullRuleService', $ruleService);
    }

    public function testObjectToValueObjectFail()
    {
        $ruleService = array();

        $ruleServiceSdk = $this->translator->objectToValueObject($ruleService);
        $this->assertInstanceof('BaseSdk\Rule\Model\RuleService', $ruleServiceSdk);
    }

    public function testObjectToValueObject()
    {
        $ruleService = \Base\Rule\Utils\MockFactory::generateRuleService(1);

        $crewSdk = new CrewSdk($ruleService->getCrew()->getId());
        $userGroupSdk = new UserGroupSdk($ruleService->getUserGroup()->getId());
        $sourceTemplateSdk = new WbjTemplateSdk($ruleService->getSourceTemplate()->getId());
        $transformationTemplateSdk = new BjTemplateSdk($ruleService->getTransformationTemplate()->getId());

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->objectToValueObject(Argument::exact($ruleService->getCrew()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crewSdk);

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->objectToValueObject(Argument::exact($ruleService->getUserGroup()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroupSdk);
        //绑定
        $this->translator->expects($this->exactly(1))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());
        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $sourceTemplateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $sourceTemplateSdkTranslator->objectToValueObject(Argument::exact($ruleService->getSourceTemplate()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceTemplateSdk);

        $transformationTemplateSdkTranslator = $this->prophesize(WbjTemplateSdkTranslator::class);
        $transformationTemplateSdkTranslator->objectToValueObject(
            Argument::exact($ruleService->getTransformationTemplate())
        )->shouldBeCalledTimes(1)->willReturn($transformationTemplateSdk);

        $this->translator->expects($this->exactly(2))
                   ->method('getTemplateSdkTranslator')
                   ->will($this->returnValueMap(
                       [
                        [$ruleService->getSourceCategory(), $sourceTemplateSdkTranslator->reveal()],
                        [$ruleService->getTransformationCategory(), $transformationTemplateSdkTranslator->reveal()]
                       ]
                   ));

        $ruleServiceSdk = $this->translator->objectToValueObject($ruleService);
        $this->assertInstanceof('BaseSdk\Rule\Model\RuleService', $ruleServiceSdk);
        $this->compareValueObjectAndObjectRuleService($ruleServiceSdk, $ruleService);
    }
}
