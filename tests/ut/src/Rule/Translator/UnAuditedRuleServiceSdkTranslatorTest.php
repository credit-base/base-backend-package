<?php
namespace Base\Rule\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Rule\Utils\RuleServiceSdkUtils;
use BaseSdk\Rule\Model\UnAuditedRuleService as UnAuditedRuleServiceSdk;

use Base\Template\Model\Template;
use Base\Template\Translator\TranslatorFactory;
use Base\Template\Translator\BjTemplateSdkTranslator;
use Base\Template\Translator\WbjTemplateSdkTranslator;
use BaseSdk\Template\Model\BjTemplate as BjTemplateSdk;
use BaseSdk\Template\Model\WbjTemplate as WbjTemplateSdk;

use BaseSdk\Crew\Model\Crew as CrewSdk;
use Base\Crew\Translator\CrewSdkTranslator;

use BaseSdk\UserGroup\Model\UserGroup as UserGroupSdk;
use Base\UserGroup\Translator\UserGroupSdkTranslator;

class UnAuditedRuleServiceSdkTranslatorTest extends TestCase
{
    use RuleServiceSdkUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder(UnAuditedRuleServiceSdkTranslator::class)
                    ->setMethods([
                        'getCrewSdkTranslator',
                        'getUserGroupSdkTranslator',
                        'getTranslatorFactory',
                        'getTemplateSdkTranslator'
                    ])
                    ->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testExtendsRuleServiceSdkTranslator()
    {
        $this->assertInstanceOf(
            'Base\Rule\Translator\RuleServiceSdkTranslator',
            $this->translator
        );
    }

    public function testValueObjectToObjectWithoutIncluded()
    {
        $unAuditedRuleService = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);

        $crewSdk = new CrewSdk($unAuditedRuleService->getCrew()->getId());
        $applyCrewSdk = new CrewSdk($unAuditedRuleService->getApplyCrew()->getId());
        $userGroupSdk = new UserGroupSdk($unAuditedRuleService->getUserGroup()->getId());
        $sourceTemplateSdk = new WbjTemplateSdk($unAuditedRuleService->getSourceTemplate()->getId());
        $transformationTemplateSdk = new BjTemplateSdk($unAuditedRuleService->getTransformationTemplate()->getId());

        $unAuditedRuleServiceSdk = new UnAuditedRuleServiceSdk(
            $unAuditedRuleService->getId(),
            $unAuditedRuleService->getSourceCategory(),
            $sourceTemplateSdk,
            $unAuditedRuleService->getTransformationCategory(),
            $transformationTemplateSdk,
            $unAuditedRuleService->getRules(),
            $unAuditedRuleService->getVersion(),
            $unAuditedRuleService->getDataTotal(),
            $crewSdk,
            $userGroupSdk,
            $applyCrewSdk,
            $unAuditedRuleService->getOperationType(),
            $unAuditedRuleService->getRelationId(),
            $unAuditedRuleService->getApplyStatus(),
            $unAuditedRuleService->getRejectReason(),
            $unAuditedRuleService->getStatus(),
            $unAuditedRuleService->getStatusTime(),
            $unAuditedRuleService->getCreateTime(),
            $unAuditedRuleService->getUpdateTime()
        );

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->valueObjectToObject(Argument::exact($crewSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($unAuditedRuleService->getCrew());
        $crewSdkTranslator->valueObjectToObject(Argument::exact($applyCrewSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($unAuditedRuleService->getApplyCrew());

        $sourceTemplateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $sourceTemplateSdkTranslator->valueObjectToObject(Argument::exact($sourceTemplateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($unAuditedRuleService->getSourceTemplate());

        $transformationSdkTranslator = $this->prophesize(WbjTemplateSdkTranslator::class);
        $transformationSdkTranslator->valueObjectToObject(Argument::exact($transformationTemplateSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($unAuditedRuleService->getTransformationTemplate());

        $this->translator->expects($this->exactly(2))
                   ->method('getTemplateSdkTranslator')
                   ->will($this->returnValueMap(
                       [
                        [$unAuditedRuleServiceSdk->getSourceCategory(), $sourceTemplateSdkTranslator->reveal()],
                        [
                            $unAuditedRuleServiceSdk->getTransformationCategory(),
                            $transformationSdkTranslator->reveal()
                        ]
                       ]
                   ));

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->valueObjectToObject(Argument::exact($userGroupSdk))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($unAuditedRuleService->getUserGroup());
        //绑定
        $this->translator->expects($this->exactly(2))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());

        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $unAuditedRuleServiceObject = $this->translator->valueObjectToObject($unAuditedRuleServiceSdk);
        $this->assertInstanceof('Base\Rule\Model\UnAuditedRuleService', $unAuditedRuleServiceObject);
        $this->compareValueObjectAndObjectUnAuditedRuleService($unAuditedRuleServiceSdk, $unAuditedRuleServiceObject);
    }

    public function testValueObjectToObjectFail()
    {
        $unAuditedRuleServiceSdk = array();

        $unAuditedRuleService = $this->translator->valueObjectToObject($unAuditedRuleServiceSdk);
        $this->assertInstanceof('Base\Rule\Model\NullUnAuditedRuleService', $unAuditedRuleService);
    }

    public function testObjectToValueObjectFail()
    {
        $unAuditedRuleService = array();

        $unAuditedRuleServiceSdk = $this->translator->objectToValueObject($unAuditedRuleService);
        $this->assertInstanceof('BaseSdk\Rule\Model\UnAuditedRuleService', $unAuditedRuleServiceSdk);
    }

    public function testObjectToValueObject()
    {
        $unAuditedRuleService = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);

        $crewSdk = new CrewSdk($unAuditedRuleService->getCrew()->getId());
        $userGroupSdk = new UserGroupSdk($unAuditedRuleService->getUserGroup()->getId());
        $sourceTemplateSdk = new WbjTemplateSdk($unAuditedRuleService->getSourceTemplate()->getId());
        $transformationTemplateSdk = new BjTemplateSdk($unAuditedRuleService->getTransformationTemplate()->getId());
        $applyCrewSdk = new CrewSdk($unAuditedRuleService->getApplyCrew()->getId());

        $crewSdkTranslator = $this->prophesize(CrewSdkTranslator::class);
        $crewSdkTranslator->objectToValueObject(Argument::exact($unAuditedRuleService->getCrew()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crewSdk);
        $crewSdkTranslator->objectToValueObject(Argument::exact($unAuditedRuleService->getApplyCrew()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyCrewSdk);

        $sourceTemplateSdkTranslator = $this->prophesize(BjTemplateSdkTranslator::class);
        $sourceTemplateSdkTranslator->objectToValueObject(Argument::exact($unAuditedRuleService->getSourceTemplate()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceTemplateSdk);

        $transformationSdkTranslator = $this->prophesize(WbjTemplateSdkTranslator::class);
        $transformationSdkTranslator->objectToValueObject(
            Argument::exact($unAuditedRuleService->getTransformationTemplate())
        )->shouldBeCalledTimes(1)->willReturn($transformationTemplateSdk);

        $this->translator->expects($this->exactly(2))
                   ->method('getTemplateSdkTranslator')
                   ->will($this->returnValueMap(
                       [
                        [$unAuditedRuleService->getSourceCategory(), $sourceTemplateSdkTranslator->reveal()],
                        [
                            $unAuditedRuleService->getTransformationCategory(),
                            $transformationSdkTranslator->reveal()
                        ]
                       ]
                   ));

        $userGroupSdkTranslator = $this->prophesize(UserGroupSdkTranslator::class);
        $userGroupSdkTranslator->objectToValueObject(Argument::exact($unAuditedRuleService->getUserGroup()))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroupSdk);
        //绑定
        $this->translator->expects($this->exactly(2))
            ->method('getCrewSdkTranslator')
            ->willReturn($crewSdkTranslator->reveal());

        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupSdkTranslator')
            ->willReturn($userGroupSdkTranslator->reveal());

        $unAuditedRuleServiceSdk = $this->translator->objectToValueObject($unAuditedRuleService);
        $this->assertInstanceof('BaseSdk\Rule\Model\UnAuditedRuleService', $unAuditedRuleServiceSdk);
        $this->compareValueObjectAndObjectUnAuditedRuleService($unAuditedRuleServiceSdk, $unAuditedRuleService);
    }
}
