<?php
namespace Base\Rule\Command\RuleService;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class AddRuleServiceCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'rules' => ['rules'],
            'transformationTemplate' => $faker->randomDigit(),
            'sourceTemplate' => $faker->randomDigit(),
            'transformationCategory' => $faker->randomDigit(),
            'sourceCategory' => $faker->randomDigit(),
            'crew' => $faker->randomDigit(),
            'id' => $faker->randomDigit(),
        );

        $this->command = new AddRuleServiceCommand(
            $this->fakerData['rules'],
            $this->fakerData['transformationTemplate'],
            $this->fakerData['sourceTemplate'],
            $this->fakerData['transformationCategory'],
            $this->fakerData['sourceCategory'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }

    public function tearDown()
    {
        unset($this->command);
    }
    
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testRulesParameter()
    {
        $this->assertEquals($this->fakerData['rules'], $this->command->rules);
    }

    public function testTransformationTemplateParameter()
    {
        $this->assertEquals($this->fakerData['transformationTemplate'], $this->command->transformationTemplate);
    }

    public function testSourceTemplateParameter()
    {
        $this->assertEquals($this->fakerData['sourceTemplate'], $this->command->sourceTemplate);
    }

    public function testSourceCategoryParameter()
    {
        $this->assertEquals($this->fakerData['sourceCategory'], $this->command->sourceCategory);
    }

    public function testTransformationCategoryParameter()
    {
        $this->assertEquals($this->fakerData['transformationCategory'], $this->command->transformationCategory);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testDefaultIdParameter()
    {
        $command = new AddRuleServiceCommand(
            $this->fakerData['rules'],
            $this->fakerData['transformationTemplate'],
            $this->fakerData['sourceTemplate'],
            $this->fakerData['transformationCategory'],
            $this->fakerData['sourceCategory'],
            $this->fakerData['crew']
        );

        $this->assertEquals(0, $command->id);
    }
}
