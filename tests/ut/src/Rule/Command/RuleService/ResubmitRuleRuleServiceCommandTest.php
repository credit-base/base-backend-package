<?php
namespace Base\Rule\Command\RuleService;

use PHPUnit\Framework\TestCase;

/**
 * @author chloroplast
 */
class ResubmitRuleRuleServiceCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'rules' => ['rules'],
            'crew' => $faker->randomDigit(),
            'id' => $faker->randomDigit(),
        );

        $this->command = new ResubmitRuleServiceCommand(
            $this->fakerData['rules'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }

    public function tearDown()
    {
        unset($this->command);
    }

    public function testExtendsAddApplyFormCommand()
    {
        $this->assertInstanceOf(
            'Base\Common\Command\ResubmitCommand',
            $this->command
        );
    }

    public function testRulesParameter()
    {
        $this->assertEquals($this->fakerData['rules'], $this->command->rules);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}
