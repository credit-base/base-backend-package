<?php
namespace Base\Rule\Command\RuleService;

use PHPUnit\Framework\TestCase;

/**
 * @author chloroplast
 */
class RejectRuleServiceCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'applyCrew' => $faker->randomDigit(),
            'rejectReason' => 'rejectReason',
            'id' => $faker->randomDigit(),
        );

        $this->command = new RejectRuleServiceCommand(
            $this->fakerData['applyCrew'],
            $this->fakerData['rejectReason'],
            $this->fakerData['id']
        );
    }

    public function tearDown()
    {
        unset($this->command);
    }
    
    public function testExtendsRejectCommand()
    {
        $this->assertInstanceOf(
            'Base\Common\Command\RejectCommand',
            $this->command
        );
    }
}
