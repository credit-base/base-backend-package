<?php
namespace Base\Rule\Utils;

use Base\Rule\Model\IRule;

trait RuleServiceSdkUtils
{
    private function compareValueObjectAndObjectRuleService(
        $ruleServiceSdk,
        $ruleService
    ) {
        $this->compareValueObjectAndObjectCommon($ruleServiceSdk, $ruleService);
    }

    private function compareValueObjectAndObjectUnAuditedRuleService(
        $unAuditedRuleServiceSdk,
        $unAuditedRuleService
    ) {
        $this->compareValueObjectAndObjectCommon($unAuditedRuleServiceSdk, $unAuditedRuleService);
        
        $this->assertEquals(
            $unAuditedRuleServiceSdk->getApplyCrew()->getId(),
            $unAuditedRuleService->getApplyCrew()->getId()
        );
        $this->assertEquals($unAuditedRuleServiceSdk->getOperationType(), $unAuditedRuleService->getOperationType());
        $this->assertEquals($unAuditedRuleServiceSdk->getRelationId(), $unAuditedRuleService->getRelationId());
        $this->assertEquals($unAuditedRuleServiceSdk->getOperationType(), $unAuditedRuleService->getOperationType());
        $this->assertEquals($unAuditedRuleServiceSdk->getApplyStatus(), $unAuditedRuleService->getApplyStatus());
        $this->assertEquals($unAuditedRuleServiceSdk->getOperationType(), $unAuditedRuleService->getOperationType());
    }

    private function compareValueObjectAndObjectCommon(
        $ruleServiceSdk,
        $ruleService
    ) {
        $this->assertEquals($ruleServiceSdk->getId(), $ruleService->getId());
        $this->assertEquals($ruleServiceSdk->getSourceCategory(), $ruleService->getSourceCategory());
        $this->assertEquals(
            $ruleServiceSdk->getSourceTemplate()->getId(),
            $ruleService->getSourceTemplate()->getId()
        );
        $this->assertEquals(
            $ruleServiceSdk->getTransformationCategory(),
            $ruleService->getTransformationCategory()
        );
        $this->assertEquals(
            $ruleServiceSdk->getTransformationTemplate()->getId(),
            $ruleService->getTransformationTemplate()->getId()
        );
        $this->assertEquals($ruleServiceSdk->getRules(), $ruleService->getRules());
        $this->assertEquals($ruleServiceSdk->getVersion(), $ruleService->getVersion());
        $this->assertEquals($ruleServiceSdk->getDataTotal(), $ruleService->getDataTotal());
        $this->assertEquals($ruleServiceSdk->getCrew()->getId(), $ruleService->getCrew()->getId());
        $this->assertEquals($ruleServiceSdk->getUserGroup()->getId(), $ruleService->getUserGroup()->getId());
        $this->assertEquals($ruleServiceSdk->getStatus(), $ruleService->getStatus());
        $this->assertEquals($ruleServiceSdk->getCreateTime(), $ruleService->getCreateTime());
        $this->assertEquals($ruleServiceSdk->getStatusTime(), $ruleService->getStatusTime());
        $this->assertEquals($ruleServiceSdk->getUpdateTime(), $ruleService->getUpdateTime());
    }
}
