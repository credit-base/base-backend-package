<?php
namespace Base\Rule\Utils;

use Base\Template\Model\Template;

use Base\Rule\Model\RuleService;
use Base\Rule\Model\UnAuditedRuleService;

use Base\Common\Model\IApproveAble;

class MockFactory
{
    public static function generateCommon(
        RuleService $ruleService,
        int $seed = 0,
        array $value = array()
    ) {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        //transformationTemplate
        self::generateTransformationTemplate($ruleService, $faker, $value);
        //sourceTemplate
        self::generateSourceTemplate($ruleService, $faker, $value);
        //rules
        self::generateRules($ruleService, $faker, $value);
        //crew
        self::generateCrew($ruleService, $faker, $value);
        //status
        self::generateStatus($ruleService, $faker, $value);
        //transformationCategory
        $ruleService->setTransformationCategory($ruleService->getTransformationTemplate()->getCategory());
        //sourceCategory
        $ruleService->setSourceCategory($ruleService->getSourceTemplate()->getCategory());
        //userGroup
        $ruleService->setUserGroup($ruleService->getSourceTemplate()->getSourceUnit());
        $ruleService->setVersion($faker->unixTime());
        $ruleService->setDataTotal($faker->randomDigit());
        $ruleService->setCreateTime($faker->unixTime());
        $ruleService->setUpdateTime($faker->unixTime());
        $ruleService->setStatusTime($faker->unixTime());

        return $ruleService;
    }

    protected static function generateTransformationTemplate($ruleService, $faker, $value) : void
    {
        $transformationTemplate = \Base\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $faker->randomDigit()
        );
        
        $transformationTemplate = isset($value['transformationTemplate']) ?
            $value['transformationTemplate'] :
            $transformationTemplate;
        
        $ruleService->setTransformationTemplate($transformationTemplate);
    }

    protected static function generateSourceTemplate($ruleService, $faker, $value) : void
    {
        $sourceTemplate = isset($value['sourceTemplate']) ?
            $value['sourceTemplate'] :
            \Base\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(
                $faker->randomDigit()
            );
        
        $ruleService->setSourceTemplate($sourceTemplate);
    }

    protected static function generateRules($ruleService, $faker, $value) : void
    {
        unset($faker);
        $rules = array(
            'transformationRule' => array(
                "TYSHXYDM" => "TYSHXYDM",
                "ZTMC" => "ZTMC",
            ),
            'completionRule' =>  array(
                'ZTMC' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                ),
                'TYSHXYDM' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                ),
            ),
            'comparisonRule' =>  array(
                'ZTMC' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                ),
                'TYSHXYDM' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                ),
            ),
            'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC", "TYSHXYDM")),
        );
    
        $rules = isset($value['rules']) ? $value['rules'] : $rules;
        
        $ruleService->setRules($rules);
    }

    protected static function generateCrew($ruleService, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $ruleService->setCrew($crew);
    }

    protected static function generateStatus($ruleService, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                RuleService::STATUS
            );
        
        $ruleService->setStatus($status);
    }

    public static function generateRuleService(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : RuleService {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $ruleService = new RuleService($id);
        $ruleService->setId($id);

        $ruleService = self::generateCommon($ruleService, $seed, $value);

        return $ruleService;
    }

    public static function generateUnAuditedRuleService(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedRuleService {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditRuleService = new UnAuditedRuleService($id);
        
        $unAuditRuleService->setApplyId($id);
        $unAuditRuleService->setId($id);

        $unAuditRuleService = self::generateCommon($unAuditRuleService, $seed, $value);
        $unAuditRuleService->setPublishCrew($unAuditRuleService->getCrew());
        self::generateApplyCrew($unAuditRuleService, $faker, $value);
        $unAuditRuleService->setApplyUserGroup($unAuditRuleService->getApplyCrew()->getUserGroup());
        $unAuditRuleService->setRelationId($id);
        $unAuditRuleService->setRejectReason($faker->word());

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            IApproveAble::APPLY_STATUS
        );
        $unAuditRuleService->setApplyStatus($applyStatus);

        //operationType
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomElement(
            IApproveAble::OPERATION_TYPE
        );
        $unAuditRuleService->setOperationType($operationType);

        return $unAuditRuleService;
    }

    protected static function generateApplyCrew($unAuditRuleService, $faker, $value) : void
    {
        $id = $unAuditRuleService->getCrew()->getId()+$faker->randomDigit();
        $applyCrew = isset($value['applyCrew']) ?
        $value['applyCrew'] :
        \Base\Crew\Utils\MockFactory::generateCrew($id);
        
        $unAuditRuleService->setApplyCrew($applyCrew);
    }
}
