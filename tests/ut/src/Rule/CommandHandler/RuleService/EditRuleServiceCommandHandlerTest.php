<?php
namespace Base\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\Rule\Model\RuleService;
use Base\Rule\Command\RuleService\EditRuleServiceCommand;

class EditRuleServiceCommandHandlerTest extends TestCase
{
    private $command;

    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         [
                                            'fetchCrew',
                                            'fetchRuleService'
                                         ]
                                     ) ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $this->command = new class implements ICommand {
        };
        $this->commandHandler->execute($this->command);
    }

    private function initialExecute($result)
    {
        //初始化
        $id = 1;
        $crew = 2;
        $rules = ['rules'];

        $this->command = new EditRuleServiceCommand($rules, $crew, $id);

        $ruleService = \Base\Rule\Utils\MockFactory::generateRuleService(1);
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $this->commandHandler->expects($this->once())->method('fetchCrew')->willReturn($crew);

        $ruleService = $this->prophesize(RuleService::class);

        $ruleService->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $ruleService->setRules(Argument::exact($rules))->shouldBeCalledTimes(1);

        if ($result) {
            $ruleService->getId()->shouldBeCalledTimes(1)->willReturn($id);
        }
        $ruleService->edit()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->once())->method('fetchRuleService')->willReturn($ruleService->reveal());
    }

    public function testExecuteSuccess()
    {
        $this->initialExecute(true);

        $result = $this->commandHandler->execute($this->command);
        $this->assertTrue($result);
    }

    public function testExecuteFalse()
    {
        $this->initialExecute(false);

        $result = $this->commandHandler->execute($this->command);
        $this->assertFalse($result);
    }
}
