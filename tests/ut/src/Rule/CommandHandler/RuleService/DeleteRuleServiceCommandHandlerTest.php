<?php
namespace Base\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Rule\Model\RuleService;
use Base\Rule\Command\RuleService\DeleteRuleServiceCommand;

use Base\Crew\Model\Crew;

class DeleteRuleServiceCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new DeleteRuleServiceCommandHandler();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        //初始化
        $id = 1;
        $crew = 2;
        $command = new DeleteRuleServiceCommand(
            $crew,
            $id
        );

        $crew = new Crew();

        $commandHandler = $this->getMockBuilder(DeleteRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         ['fetchRuleService','fetchCrew']
                                     )
                                     ->getMock();

        $ruleService = $this->prophesize(RuleService::class);
        $ruleService->setCrew(Argument::exact($crew))
                             ->shouldBeCalledTimes(1);
        $ruleService->delete()->shouldBeCalledTimes(1)
                             ->willReturn(true);

        $commandHandler->expects($this->once())
                       ->method('fetchRuleService')
                       ->with($command->id)
                       ->willReturn($ruleService->reveal());

        $commandHandler->expects($this->once())
                       ->method('fetchCrew')
                       ->with($command->crew)
                       ->willReturn($crew);

        $result = $commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
