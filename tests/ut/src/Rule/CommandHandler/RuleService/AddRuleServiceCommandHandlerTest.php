<?php
namespace Base\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Template\Model\BjTemplate;
use Base\Template\Model\WbjTemplate;
use Base\Template\Repository\WbjTemplateSdkRepository;
use Base\Template\Repository\BjTemplateSdkRepository;

use Base\Rule\Model\RuleService;
use Base\Rule\Repository\RepositoryFactory;
use Base\Rule\Command\RuleService\AddRuleServiceCommand;

class AddRuleServiceCommandHandlerTest extends TestCase
{
    private $command;
    
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         [
                                            'getRepository',
                                            'fetchCrew',
                                            'getRuleService'
                                         ]
                                     )
                                     ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $this->command = new class implements ICommand {
        };
        $this->commandHandler->execute($this->command);
    }

    private function initialExecute($result, $id = 0)
    {
        //初始化
        $transformationTemplateId = 1;
        $sourceTemplateId = 2;
        $transformationCategory = 1;
        $sourceCategory = 10;
        $crew = 4;
        $rules = ['rules'];

        $this->command = new AddRuleServiceCommand(
            $rules,
            $transformationTemplateId,
            $sourceTemplateId,
            $transformationCategory,
            $sourceCategory,
            $crew
        );

        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $sourceTemplate = \Base\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate($sourceTemplateId);
        $transformationTemplate = \Base\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $transformationTemplateId
        );

        $transformationRepository =  $this->prophesize(BjTemplateSdkRepository::class);
        $transformationRepository->fetchOne(
            Argument::exact($transformationTemplateId)
        )->willReturn($transformationTemplate);

        $sourceRepository =  $this->prophesize(WbjTemplateSdkRepository::class);
        $sourceRepository->fetchOne(Argument::exact($sourceTemplateId))->willReturn($sourceTemplate);

        $this->commandHandler->expects($this->exactly(2))
                   ->method('getRepository')
                   ->will($this->returnValueMap(
                       [
                        [$sourceCategory, $sourceRepository->reveal()],
                        [$transformationCategory, $transformationRepository->reveal()]
                       ]
                   ));

        $ruleService = $this->prophesize(RuleService::class);
        $ruleService->setTransformationTemplate(Argument::exact($transformationTemplate))->shouldBeCalledTimes(1);
        $ruleService->setTransformationCategory(Argument::exact($transformationCategory))->shouldBeCalledTimes(1);
        $ruleService->setSourceTemplate(Argument::exact($sourceTemplate))->shouldBeCalledTimes(1);
        $ruleService->setSourceCategory(Argument::exact($sourceCategory))->shouldBeCalledTimes(1);
        $ruleService->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $ruleService->setRules(Argument::exact($rules))->shouldBeCalledTimes(1);

        if ($result) {
            $ruleService->getId()->shouldBeCalledTimes(1)->willReturn($id);
        }

        $ruleService->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->once())->method('fetchCrew')->willReturn($crew);

        $this->commandHandler->expects($this->once())->method(
            'getRuleService'
        )->willReturn($ruleService->reveal());
    }

    public function testExecuteSuccess()
    {
        $id = 10;

        $this->initialExecute(true, $id);

        $result = $this->commandHandler->execute($this->command);
        $this->assertTrue($result);
        $this->assertEquals($id, $this->command->id);
    }

    public function testExecuteFalse()
    {
        $this->initialExecute(false);

        $result = $this->commandHandler->execute($this->command);
        $this->assertFalse($result);
        $this->assertEquals(0, $this->command->id);
    }
}
