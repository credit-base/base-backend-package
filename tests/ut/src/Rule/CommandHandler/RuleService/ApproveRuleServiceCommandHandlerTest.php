<?php
namespace Base\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Rule\Command\RuleService\ApproveRuleServiceCommand;
use Base\Rule\Model\UnAuditedRuleService;

use Base\Crew\Model\Crew;

class ApproveRuleServiceCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ApproveRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         ['fetchUnAuditedRuleService','fetchCrew']
                                     )
                                     ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $this->command = new class implements ICommand {
        };
        $this->commandHandler->execute($this->command);
    }

    public function testExecuteAction()
    {
        //初始化
        $command = new ApproveRuleServiceCommand(2, 1);
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $unAuditedRuleService = $this->prophesize(UnAuditedRuleService::class);
        $unAuditedRuleService->setApplyCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $unAuditedRuleService->approve()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
                       ->method('fetchUnAuditedRuleService')
                       ->with($command->id)
                       ->willReturn($unAuditedRuleService->reveal());

        $this->commandHandler->expects($this->once())->method(
            'fetchCrew'
        )->with($command->applyCrew)->willReturn($crew);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
