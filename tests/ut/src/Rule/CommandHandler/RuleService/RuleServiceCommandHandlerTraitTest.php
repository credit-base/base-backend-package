<?php
namespace Base\Rule\CommandHandler\RuleService;

use PHPUnit\Framework\TestCase;

use Base\Crew\Repository\CrewRepository;

use Base\Rule\Repository\RepositoryFactory;
use Base\Rule\Repository\RuleServiceSdkRepository;
use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;

use Base\Template\Model\Template;
use Base\Template\Repository\BjTemplateSdkRepository;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @author chloroplast
 */
class RuleServiceCommandHandlerTraitTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new MockRuleServiceCommandHandlerTrait();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testGetCrewRepository()
    {
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $this->commandHandler->getCrewRepositoryPublic()
        );
    }

    public function testFetchCrew()
    {
        $id = 1;
        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);

        $this->commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getCrewRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($crew);

        $this->commandHandler->expects($this->once())
                       ->method('getCrewRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->fetchCrewPublic($id);
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $result
        );
    }

    public function testGetRuleServiceSdkRepository()
    {
        $this->assertInstanceOf(
            'Base\Rule\Repository\RuleServiceSdkRepository',
            $this->commandHandler->getRuleServiceSdkRepositoryPublic()
        );
    }

    public function testGetRuleService()
    {
        $this->assertInstanceOf(
            'Base\Rule\Model\RuleService',
            $this->commandHandler->getRuleServicePublic()
        );
    }

    public function testFetchRuleService()
    {
        $id = 1;
        $ruleService = \Base\Rule\Utils\MockFactory::generateRuleService($id);

        $this->commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getRuleServiceSdkRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(RuleServiceSdkRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($ruleService);

        $this->commandHandler->expects($this->once())
                       ->method('getRuleServiceSdkRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->fetchRuleServicePublic($id);
        $this->assertInstanceOf(
            'Base\Rule\Model\RuleService',
            $result
        );
    }

    public function testGetUnAuditedRuleServiceSdkRepository()
    {
        $this->assertInstanceOf(
            'Base\Rule\Repository\UnAuditedRuleServiceSdkRepository',
            $this->commandHandler->getUnAuditedRuleServiceSdkRepositoryPublic()
        );
    }

    public function testFetchUnAuditedRuleService()
    {
        $id = 1;
        $unAuditedRuleService = \Base\Rule\Utils\MockFactory::generateUnAuditedRuleService($id);

        $this->commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getUnAuditedRuleServiceSdkRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(UnAuditedRuleServiceSdkRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($unAuditedRuleService);

        $this->commandHandler->expects($this->once())
                       ->method('getUnAuditedRuleServiceSdkRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->fetchUnAuditedRuleServicePublic($id);
        $this->assertInstanceOf(
            'Base\Rule\Model\UnAuditedRuleService',
            $result
        );
    }

    public function testGetUnAuditedRuleService()
    {
        $this->assertInstanceOf(
            'Base\Rule\Model\UnAuditedRuleService',
            $this->commandHandler->getUnAuditedRuleServicePublic()
        );
    }

    public function testGetRepositoryFactory()
    {
        $this->assertInstanceOf(
            'Base\Rule\Repository\RepositoryFactory',
            $this->commandHandler->getRepositoryFactoryPublic()
        );
    }

    public function testGetRepository()
    {
        $commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                            ->setMethods(['getRepositoryFactory'])
                            ->getMock();

        $bjTemplateSdkRepository = new BjTemplateSdkRepository();
        $category = Template::CATEGORY['BJ'];
        
        $factory = $this->prophesize(RepositoryFactory::class);
        $factory->getRepository($category)->shouldBeCalledTimes(1)->willReturn($bjTemplateSdkRepository);

        $commandHandler->expects($this->once())->method('getRepositoryFactory')->willReturn($factory->reveal());
        $result = $commandHandler->getRepositoryPublic($category);

        $this->assertEquals($bjTemplateSdkRepository, $result);
        $this->assertInstanceOf('Marmot\Framework\Classes\Repository', $result);
    }
}
