<?php
namespace Base\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\Command\RuleService\RevokeRuleServiceCommand;

class RevokeRuleServiceCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(RevokeRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         ['fetchUnAuditedRuleService','fetchCrew']
                                     )
                                     ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        //初始化
        $id = 1;
        $crew = 2;
        $command = new RevokeRuleServiceCommand(
            $crew,
            $id
        );

        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);

        $this->commandHandler->expects($this->once())->method('fetchCrew')->willReturn($crew);

        $unAuditedRuleService = $this->prophesize(UnAuditedRuleService::class);
        $unAuditedRuleService->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $unAuditedRuleService->revoke()->shouldBeCalledTimes(1) ->willReturn(true);

        $this->commandHandler->expects($this->once())
                       ->method('fetchUnAuditedRuleService')
                       ->with($command->id)
                       ->willReturn($unAuditedRuleService->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
