<?php
namespace Base\Rule\CommandHandler\RuleService;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Rule\Command\RuleService\AddRuleServiceCommand;
use Base\Rule\Command\RuleService\EditRuleServiceCommand;
use Base\Rule\Command\RuleService\DeleteRuleServiceCommand;
use Base\Rule\Command\RuleService\RevokeRuleServiceCommand;
use Base\Rule\Command\RuleService\ResubmitRuleServiceCommand;
use Base\Rule\Command\RuleService\ApproveRuleServiceCommand;
use Base\Rule\Command\RuleService\RejectRuleServiceCommand;

class RuleServiceCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new RuleServiceCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddRuleServiceCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddRuleServiceCommand(
                ['rules'],
                $this->faker->randomDigit(),
                $this->faker->randomDigit(),
                $this->faker->randomDigit(),
                $this->faker->randomDigit(),
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf(
            'Base\Rule\CommandHandler\RuleService\AddRuleServiceCommandHandler',
            $commandHandler
        );
    }

    public function testEditRuleServiceCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditRuleServiceCommand(
                ['rules'],
                $this->faker->randomDigit(),
                $this->faker->randomDigit(),
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf(
            'Base\Rule\CommandHandler\RuleService\EditRuleServiceCommandHandler',
            $commandHandler
        );
    }

    public function testDeleteRuleServiceCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DeleteRuleServiceCommand(
                $this->faker->randomDigit(),
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf(
            'Base\Rule\CommandHandler\RuleService\DeleteRuleServiceCommandHandler',
            $commandHandler
        );
    }

    public function testRevokeRuleServiceCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeRuleServiceCommand(
                $this->faker->randomDigit(),
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf(
            'Base\Rule\CommandHandler\RuleService\RevokeRuleServiceCommandHandler',
            $commandHandler
        );
    }

    public function testResubmitRuleServiceCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResubmitRuleServiceCommand(
                ['rules'],
                $this->faker->randomDigit(),
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf(
            'Base\Rule\CommandHandler\RuleService\ResubmitRuleServiceCommandHandler',
            $commandHandler
        );
    }

    public function testApproveRuleServiceCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveRuleServiceCommand(
                $this->faker->randomDigit(),
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf(
            'Base\Rule\CommandHandler\RuleService\ApproveRuleServiceCommandHandler',
            $commandHandler
        );
    }

    public function testRejectRuleServiceCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectRuleServiceCommand(
                $this->faker->randomDigit(),
                'rejectReason',
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf(
            'Base\Rule\CommandHandler\RuleService\RejectRuleServiceCommandHandler',
            $commandHandler
        );
    }
}
