<?php
namespace Base\Rule\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;
use Base\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceSdkAdapter;

use Base\Rule\Utils\MockFactory;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditedRuleServiceSdkRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditedRuleServiceSdkRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockUnAuditedRuleServiceSdkRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new UnAuditedRuleServiceSdkAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceSdkAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testResubmit()
    {
        $unAuditedRuleService = MockFactory::generateUnAuditedRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->resubmit(Argument::exact($unAuditedRuleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->resubmit($unAuditedRuleService, $keys);
    }

    public function testApprove()
    {
        $unAuditedRuleService = MockFactory::generateUnAuditedRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->approve(Argument::exact($unAuditedRuleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->approve($unAuditedRuleService, $keys);
    }

    public function testReject()
    {
        $unAuditedRuleService = MockFactory::generateUnAuditedRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->reject(Argument::exact($unAuditedRuleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->reject($unAuditedRuleService, $keys);
    }

    public function testRevoke()
    {
        $unAuditedRuleService = MockFactory::generateUnAuditedRuleService(1);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->revoke(Argument::exact($unAuditedRuleService), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->revoke($unAuditedRuleService, $keys);
    }
}
