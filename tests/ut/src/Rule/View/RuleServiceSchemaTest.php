<?php
namespace Base\Rule\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class RuleServiceSchemaTest extends TestCase
{
    private $ruleServiceSchema;

    private $ruleService;

    public function setUp()
    {
        $this->ruleServiceSchema = new RuleServiceSchema(new Factory());

        $this->ruleService = \Base\Rule\Utils\MockFactory::generateRuleService(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->ruleServiceSchema);
        unset($this->ruleService);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->ruleServiceSchema);
    }

    public function testGetId()
    {
        $result = $this->ruleServiceSchema->getId($this->ruleService);

        $this->assertEquals($result, $this->ruleService->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->ruleServiceSchema->getAttributes($this->ruleService);

        $this->assertEquals($result['rules'], $this->ruleService->getRules());
        $this->assertEquals($result['version'], $this->ruleService->getVersion());
        $this->assertEquals($result['version'], $this->ruleService->getVersion());
        $this->assertEquals($result['dataTotal'], $this->ruleService->getDataTotal());
        $this->assertEquals($result['transformationCategory'], $this->ruleService->getTransformationCategory());
        $this->assertEquals($result['sourceCategory'], $this->ruleService->getSourceCategory());
        $this->assertEquals($result['status'], $this->ruleService->getStatus());
        $this->assertEquals($result['createTime'], $this->ruleService->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->ruleService->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->ruleService->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->ruleServiceSchema->getRelationships($this->ruleService, 0, array());

        $this->assertEquals(
            $result['transformationTemplate'],
            ['data' => $this->ruleService->getTransformationTemplate()]
        );
        $this->assertEquals($result['sourceTemplate'], ['data' => $this->ruleService->getSourceTemplate()]);
        $this->assertEquals($result['userGroup'], ['data' => $this->ruleService->getUserGroup()]);
        $this->assertEquals($result['crew'], ['data' => $this->ruleService->getCrew()]);
    }
}
