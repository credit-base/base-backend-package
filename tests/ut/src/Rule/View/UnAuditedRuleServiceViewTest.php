<?php
namespace Base\Rule\View;

use PHPUnit\Framework\TestCase;

use Base\Rule\Model\UnAuditedRuleService;

class UnAuditedRuleServiceViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $unAuditedRuleService = new UnAuditedRuleServiceView(new UnAuditedRuleService());
        $this->assertInstanceof('Base\Common\View\CommonView', $unAuditedRuleService);
    }
}
