<?php
namespace Base\Rule\View;

use PHPUnit\Framework\TestCase;

use Base\Rule\Model\RuleService;

class RuleServiceViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $ruleService = new RuleServiceView(new RuleService());
        $this->assertInstanceof('Base\Common\View\CommonView', $ruleService);
    }
}
