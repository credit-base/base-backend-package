<?php
namespace Base\Interaction\Repository\QA;

use PHPUnit\Framework\TestCase;

class UnAuditedQARepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditedQARepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockUnAuditedQARepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\QA\UnAuditedQADbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\QA\UnAuditedQAMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }
}
