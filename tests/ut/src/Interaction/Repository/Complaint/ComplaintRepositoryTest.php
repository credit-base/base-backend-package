<?php
namespace Base\Interaction\Repository\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\ComplaintMockFactory;
use Base\Interaction\Adapter\Complaint\IComplaintAdapter;
use Base\Interaction\Adapter\Complaint\ComplaintDbAdapter;

class ComplaintRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(ComplaintRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockComplaintRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new ComplaintDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\IComplaintAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\ComplaintDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\IComplaintAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\ComplaintMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IComplaintAdapter::class);
        $adapter->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IComplaintAdapter::class);
        $adapter->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IComplaintAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $complaint = ComplaintMockFactory::generateComplaint($id);
        $keys = array();
        
        $adapter = $this->prophesize(IComplaintAdapter::class);
        $adapter->add(Argument::exact($complaint))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->add($complaint, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $complaint = ComplaintMockFactory::generateComplaint($id);
        $keys = array();
        
        $adapter = $this->prophesize(IComplaintAdapter::class);
        $adapter->edit(Argument::exact($complaint), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->edit($complaint, $keys);
    }
}
