<?php
namespace Base\Interaction\Repository\Complaint;

use PHPUnit\Framework\TestCase;

class UnAuditedComplaintRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditedComplaintRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockUnAuditedComplaintRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\UnAuditedComplaintDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\UnAuditedComplaintMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }
}
