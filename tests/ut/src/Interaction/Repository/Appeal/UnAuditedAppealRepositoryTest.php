<?php
namespace Base\Interaction\Repository\Appeal;

use PHPUnit\Framework\TestCase;

class UnAuditedAppealRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditedAppealRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockUnAuditedAppealRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\UnAuditedAppealDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\UnAuditedAppealMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }
}
