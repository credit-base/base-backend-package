<?php
namespace Base\Interaction\Repository\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\AppealMockFactory;
use Base\Interaction\Adapter\Appeal\IAppealAdapter;
use Base\Interaction\Adapter\Appeal\AppealDbAdapter;

class AppealRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(AppealRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockAppealRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new AppealDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\IAppealAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\AppealDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\IAppealAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\AppealMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IAppealAdapter::class);
        $adapter->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IAppealAdapter::class);
        $adapter->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IAppealAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $appeal = AppealMockFactory::generateAppeal($id);
        $keys = array();
        
        $adapter = $this->prophesize(IAppealAdapter::class);
        $adapter->add(Argument::exact($appeal))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->add($appeal, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $appeal = AppealMockFactory::generateAppeal($id);
        $keys = array();
        
        $adapter = $this->prophesize(IAppealAdapter::class);
        $adapter->edit(Argument::exact($appeal), Argument::exact($keys))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->edit($appeal, $keys);
    }
}
