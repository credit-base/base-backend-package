<?php
namespace Base\Interaction\Service;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\Appeal;
use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Model\IInteractionAble;

class AcceptInteractionServiceTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockAcceptInteractionService(new UnAuditedAppeal(), new Appeal(), new Reply());
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->stub);
    }

    public function testGetUnAuditedInteraction()
    {
        $this->assertInstanceof(
            'Base\ApplyForm\Model\IApplyFormAble',
            $this->stub->getUnAuditedInteraction()
        );
    }

    public function testGetInteraction()
    {
        $this->assertInstanceof(
            'Base\Interaction\Model\IInteractionAble',
            $this->stub->getInteraction()
        );
    }

    public function testGetReply()
    {
        $this->assertInstanceof(
            'Base\Interaction\Model\Reply',
            $this->stub->getReply()
        );
    }

    public function testAcceptFail()
    {
        $stub = $this->getMockBuilder(MockAcceptInteractionService::class)
                        ->setConstructorArgs(array(new UnAuditedAppeal(), new Appeal(), new Reply()))
                        ->setMethods(['getReply'])
                        ->getMock();

        $reply = $this->prophesize(Reply::class);
        $reply->add()->shouldBeCalledTimes(1)->willReturn(false);

        $stub->expects($this->once())->method('getReply')->willReturn($reply->reveal());

        $result = $stub->accept();

        $this->assertFalse($result);
    }

    public function testAccept()
    {
        $stub = $this->getMockBuilder(MockAcceptInteractionService::class)
                        ->setConstructorArgs(array(new UnAuditedAppeal(), new Appeal(), new Reply()))
                        ->setMethods(['getReply', 'getUnAuditedInteraction', 'getInteraction'])
                        ->getMock();

        $reply = $this->prophesize(Reply::class);
        $reply->add()->shouldBeCalledTimes(1)->willReturn(true);
        $reply->setId(1)->shouldBeCalledTimes(2);
        $reply->getId()->shouldBeCalledTimes(2)->willReturn(1);

        $unAuditedInteraction = $this->prophesize(UnAuditedAppeal::class);
        $unAuditedInteraction->add()->shouldBeCalledTimes(1)->willReturn(true);
        $unAuditedInteraction->getReply()->shouldBeCalledTimes(1)->willReturn($reply);

        $interaction = $this->prophesize(Appeal::class);
        $interaction->updateAcceptStatus(
            Argument::exact(IInteractionAble::ACCEPT_STATUS['ACCEPTING'])
        )->shouldBeCalledTimes(1)->willReturn(true);
        $interaction->getReply()->shouldBeCalledTimes(1)->willReturn($reply);

        $stub->expects($this->once())->method('getReply')->willReturn($reply->reveal());
        $stub->expects($this->once())->method('getUnAuditedInteraction')->willReturn($unAuditedInteraction->reveal());
        $stub->expects($this->once())->method('getInteraction')->willReturn($interaction->reveal());

        $result = $stub->accept();

        $this->assertTrue($result);
    }
}
