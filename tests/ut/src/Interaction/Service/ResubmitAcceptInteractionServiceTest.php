<?php
namespace Base\Interaction\Service;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\UnAuditedAppeal;

class ResubmitAcceptInteractionServiceTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockResubmitAcceptInteractionService(new UnAuditedAppeal(), new Reply());
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->stub);
    }

    public function testGetUnAuditedInteraction()
    {
        $this->assertInstanceof(
            'Base\ApplyForm\Model\IApplyFormAble',
            $this->stub->getUnAuditedInteraction()
        );
    }

    public function testGetReply()
    {
        $this->assertInstanceof(
            'Base\Interaction\Model\Reply',
            $this->stub->getReply()
        );
    }

    public function testResubmitAccept()
    {
        $stub = $this->getMockBuilder(MockResubmitAcceptInteractionService::class)
                        ->setConstructorArgs(array(new UnAuditedAppeal(), new Reply()))
                        ->setMethods(['getReply', 'getUnAuditedInteraction'])
                        ->getMock();

        $reply = $this->prophesize(Reply::class);
        $reply->edit()->shouldBeCalledTimes(1)->willReturn(true);
        $stub->expects($this->once())->method('getReply')->willReturn($reply->reveal());

        $unAuditedInteraction = $this->prophesize(UnAuditedAppeal::class);
        $unAuditedInteraction->resubmit()->shouldBeCalledTimes(1)->willReturn(true);
        $stub->expects($this->once())->method('getUnAuditedInteraction')->willReturn($unAuditedInteraction->reveal());

        $result = $stub->resubmitAccept();

        $this->assertTrue($result);
    }
}
