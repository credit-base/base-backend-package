<?php
namespace Base\Interaction\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Translator\ComplaintDbTranslator;

class UnAuditedComplaintTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new UnAuditedComplaint();
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Base\ApplyForm\Model\IApplyFormAble', $this->stub);
    }

    public function testCorrectExtendsComplaint()
    {
        $this->assertInstanceof('Base\Interaction\Model\Complaint', $this->stub);
    }

    public function testUnAuditedComplaintConstructor()
    {
        $this->assertEquals(0, $this->stub->getApplyId());
        $this->assertEquals('', $this->stub->getApplyTitle());
        $this->assertEquals(UnAuditedComplaint::OPERATION_TYPE['NULL'], $this->stub->getOperationType());
        $this->assertEquals(array(), $this->stub->getApplyInfo());
        $this->assertEquals(UnAuditedComplaint::APPLY_STATUS['PENDING'], $this->stub->getApplyStatus());
        $this->assertEquals('', $this->stub->getRejectReason());
        $this->assertInstanceOf(
            'Base\Member\Model\Member',
            $this->stub->getRelation()
        );
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->stub->getApplyCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->stub->getApplyUserGroup()
        );
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\ApplyInfoCategory',
            $this->stub->getApplyInfoCategory()
        );
    }

    public function testAdd()
    {
        $unAuditedComplaint = $this->getMockBuilder(MockUnAuditedComplaint::class)
                           ->setMethods(['applyInfo', 'setApplyInfo', 'apply'])
                           ->getMock();

        $applyInfo = array('applyInfo');
        $unAuditedComplaint->expects($this->exactly(1))->method('applyInfo')->willReturn($applyInfo);
        $unAuditedComplaint->expects($this->exactly(1))->method('setApplyInfo')->with($applyInfo);
        $unAuditedComplaint->expects($this->exactly(1))->method('apply')->willReturn(true);

        $result = $unAuditedComplaint->add();
        $this->assertTrue($result);
    }

    public function testGetComplaintDbTranslator()
    {
        $unAuditedComplaint = new MockUnAuditedComplaint();

        $this->assertInstanceOf(
            'Base\Interaction\Translator\ComplaintDbTranslator',
            $unAuditedComplaint->getComplaintDbTranslator()
        );
    }

    public function testApplyInfo()
    {
        $unAuditedComplaint = $this->getMockBuilder(MockUnAuditedComplaint::class)
                           ->setMethods(['getComplaintDbTranslator'])
                           ->getMock();

        $applyInfo = array('applyInfo');

        $translator = $this->prophesize(ComplaintDbTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditedComplaint)
        )->shouldBeCalledTimes(1)->willReturn($applyInfo);

        $unAuditedComplaint->expects($this->once())->method(
            'getComplaintDbTranslator'
        )->willReturn($translator->reveal());

        $result = $unAuditedComplaint->applyInfo();
        $this->assertEquals($applyInfo, $result);
    }
}
