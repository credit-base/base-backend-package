<?php
namespace Base\Interaction\Model;

use PHPUnit\Framework\TestCase;

class NullPraiseTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullPraise::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsPraise()
    {
        $this->assertInstanceof('Base\Interaction\Model\Praise', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
