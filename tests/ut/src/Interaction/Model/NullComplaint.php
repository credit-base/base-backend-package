<?php
namespace Base\Interaction\Model;

use PHPUnit\Framework\TestCase;

class NullComplaintTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullComplaint::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsComplaint()
    {
        $this->assertInstanceof('Base\Interaction\Model\Complaint', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
