<?php
namespace Base\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class PraiseTest extends TestCase
{
    private $praise;

    private $faker;

    public function setUp()
    {
        $this->praise = new MockPraise();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->praise);
        unset($this->faker);
    }

    public function testExtendsCommentInteraction()
    {
        $this->assertInstanceof('Base\Interaction\Model\CommentInteraction', $this->praise);
    }
    
    public function testSearchDataConstructor()
    {
        $this->assertEquals('', $this->praise->getSubject());
    }

    //subject 测试 ------------------------------------------------------- start
    /**
     * 设置 setSubject() 正确的传参类型,期望传值正确
     */
    public function testSetSubjectCorrectType()
    {
        $subject = $this->faker->word();

        $this->praise->setSubject($subject);
        $this->assertEquals($subject, $this->praise->getSubject());
    }

    /**
     * 设置 setSubject() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectWrongType()
    {
        $this->praise->setSubject($this->faker->words(3, false));
    }
    //subject 测试 -------------------------------------------------------   end

    public function testGetRepository()
    {
        $praise = new MockPraise();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Praise\IPraiseAdapter',
            $praise->getRepository()
        );
    }
}
