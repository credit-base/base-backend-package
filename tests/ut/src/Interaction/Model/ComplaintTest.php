<?php
namespace Base\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ComplaintTest extends TestCase
{
    private $complaint;

    private $faker;

    public function setUp()
    {
        $this->complaint = new MockComplaint();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->complaint);
        unset($this->faker);
    }

    public function testExtendsCommentInteraction()
    {
        $this->assertInstanceof('Base\Interaction\Model\CommentInteraction', $this->complaint);
    }
    
    public function testSearchDataConstructor()
    {
        $this->assertEquals('', $this->complaint->getSubject());
    }

    //subject 测试 ------------------------------------------------------- start
    /**
     * 设置 setSubject() 正确的传参类型,期望传值正确
     */
    public function testSetSubjectCorrectType()
    {
        $subject = $this->faker->word();

        $this->complaint->setSubject($subject);
        $this->assertEquals($subject, $this->complaint->getSubject());
    }

    /**
     * 设置 setSubject() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectWrongType()
    {
        $this->complaint->setSubject($this->faker->words(3, false));
    }
    //subject 测试 -------------------------------------------------------   end

    public function testGetRepository()
    {
        $complaint = new MockComplaint();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\IComplaintAdapter',
            $complaint->getRepository()
        );
    }
}
