<?php
namespace Base\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class FeedbackTest extends TestCase
{
    private $feedback;

    private $faker;

    public function setUp()
    {
        $this->feedback = new MockFeedback();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->feedback);
        unset($this->faker);
    }

    public function testExtendsInteraction()
    {
        $this->assertInstanceof('Base\Interaction\Model\Interaction', $this->feedback);
    }
    
    public function testGetRepository()
    {
        $feedback = new MockFeedback();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Feedback\IFeedbackAdapter',
            $feedback->getRepository()
        );
    }
}
