<?php
namespace Base\Interaction\Model;

use PHPUnit\Framework\TestCase;

class NullQATest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullQA::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsQA()
    {
        $this->assertInstanceof('Base\Interaction\Model\QA', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
