<?php
namespace Base\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Member\Model\Member;
use Base\Member\Model\NullMember;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Model\NullUserGroup;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class InteractionTest extends TestCase
{
    private $interaction;

    private $faker;

    public function setUp()
    {
        $this->interaction = $this->getMockBuilder(MockInteraction::class)
                                ->getMockForAbstractClass();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->interaction);
        unset($this->faker);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->interaction
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IOperate',
            $this->interaction
        );
    }

    public function testImplementsIInteractionAble()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Model\IInteractionAble',
            $this->interaction
        );
    }

    public function testInteractionConstructor()
    {
        $this->assertEquals(0, $this->interaction->getId());
        $this->assertEquals('', $this->interaction->getTitle());
        $this->assertEquals('', $this->interaction->getContent());
        $this->assertEquals(IInteractionAble::ACCEPT_STATUS['PENDING'], $this->interaction->getAcceptStatus());
        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['NORMAL'], $this->interaction->getStatus());
        $this->assertInstanceOf(
            'Base\Member\Model\Member',
            $this->interaction->getMember()
        );
        $this->assertInstanceOf(
            'Base\Interaction\Model\Reply',
            $this->interaction->getReply()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->interaction->getAcceptUserGroup()
        );
    }

    public function testSetId()
    {
        $id = $this->faker->randomNumber();
        $this->interaction->setId($id);
        $this->assertEquals($id, $this->interaction->getId());
    }
    
    //title 测试 ------------------------------------------------------- start
    /**
     * 设置 setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $title = $this->faker->title();

        $this->interaction->setTitle($title);
        $this->assertEquals($title, $this->interaction->getTitle());
    }

    /**
     * 设置 setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->interaction->setTitle($this->faker->words(3, false));
    }
    //title 测试 -------------------------------------------------------   end
    
    //content 测试 ------------------------------------------------------- start
    /**
     * 设置 setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $content = $this->faker->word();

        $this->interaction->setContent($content);
        $this->assertEquals($content, $this->interaction->getContent());
    }

    /**
     * 设置 setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->interaction->setContent($this->faker->words(3, false));
    }
    //content 测试 -------------------------------------------------------   end
    
    //member 测试 ---------------------------------------------------- start
    /**
     * 设置 setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $member = new Member();

        $this->interaction->setMember($member);
        $this->assertEquals($member, $this->interaction->getMember());
    }

    /**
     * 设置 setMember() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $member = array($this->faker->randomNumber());

        $this->interaction->setMember($member);
    }
    //member 测试 ----------------------------------------------------   end
    
    //acceptUserGroup 测试 ---------------------------------------------------- start
    /**
     * 设置 setAcceptUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptUserGroupCorrectType()
    {
        $acceptUserGroup = new UserGroup();

        $this->interaction->setAcceptUserGroup($acceptUserGroup);
        $this->assertEquals($acceptUserGroup, $this->interaction->getAcceptUserGroup());
    }

    /**
     * 设置 setAcceptUserGroup() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptUserGroupWrongType()
    {
        $acceptUserGroup = array($this->faker->randomNumber());

        $this->interaction->setAcceptUserGroup($acceptUserGroup);
    }
    //acceptUserGroup 测试 ----------------------------------------------------   end
        
    //reply 测试 ---------------------------------------------------- start
    /**
     * 设置 setReply() 正确的传参类型,期望传值正确
     */
    public function testSetReplyCorrectType()
    {
        $reply = new Reply();

        $this->interaction->setReply($reply);
        $this->assertEquals($reply, $this->interaction->getReply());
    }

    /**
     * 设置 setReply() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReplyWrongType()
    {
        $reply = array($this->faker->randomNumber());

        $this->interaction->setReply($reply);
    }
    //reply 测试 ----------------------------------------------------   end
    
    //acceptStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setAcceptStatus() 是否符合预定范围
     *
     * @dataProvider acceptStatusProvider
     */
    public function testSetAcceptStatus($actual, $expected)
    {
        $this->interaction->setAcceptStatus($actual);
        $this->assertEquals($expected, $this->interaction->getAcceptStatus());
    }
    /**
     * 循环测试 DispatchDepartment setAcceptStatus() 数据构建器
     */
    public function acceptStatusProvider()
    {
        return array(
            array(IInteractionAble::ACCEPT_STATUS['PENDING'], IInteractionAble::ACCEPT_STATUS['PENDING']),
            array(IInteractionAble::ACCEPT_STATUS['ACCEPTING'], IInteractionAble::ACCEPT_STATUS['ACCEPTING']),
            array(IInteractionAble::ACCEPT_STATUS['COMPLETE'], IInteractionAble::ACCEPT_STATUS['COMPLETE']),
            array(999, IInteractionAble::ACCEPT_STATUS['PENDING'])
        );
    }
    //acceptStatus 测试 ------------------------------------------------------ end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->interaction->setStatus($actual);
        $this->assertEquals($expected, $this->interaction->getStatus());
    }
    /**
     * 循环测试 DispatchDepartment setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(IInteractionAble::INTERACTION_STATUS['NORMAL'], IInteractionAble::INTERACTION_STATUS['NORMAL']),
            array(IInteractionAble::INTERACTION_STATUS['REVOKE'], IInteractionAble::INTERACTION_STATUS['REVOKE']),
            array(IInteractionAble::INTERACTION_STATUS['PUBLISH'], IInteractionAble::INTERACTION_STATUS['PUBLISH']),
            array(999, IInteractionAble::INTERACTION_STATUS['NORMAL'])
        );
    }
    //status 测试 ------------------------------------------------------ end

    public function testIsMemberNullTrue()
    {
        $member = \Base\Member\Utils\MockFactory::generateMember($this->faker->randomNumber());
        $this->interaction->setMember($member);

        $result = $this->interaction->isMemberNull();
        $this->assertTrue($result);
    }

    public function testIsMemberNullFalse()
    {
        $member = new NullMember();
        $this->interaction->setMember($member);
        
        $result = $this->interaction->isMemberNull();
        
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('memberId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsAcceptUserGroupNullTrue()
    {
        $acceptUserGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup($this->faker->randomNumber());
        $this->interaction->setAcceptUserGroup($acceptUserGroup);

        $result = $this->interaction->isAcceptUserGroupNull();
        $this->assertTrue($result);
    }

    public function testIsAcceptUserGroupNullFalse()
    {
        $acceptUserGroup = new NullUserGroup();
        $this->interaction->setAcceptUserGroup($acceptUserGroup);
        
        $result = $this->interaction->isAcceptUserGroupNull();
        
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('acceptUserGroupId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testValidateFalse()
    {
        $this->interaction = $this->getMockBuilder(MockInteraction::class)
                                ->setMethods(['isMemberNull'])
                                ->getMock();

        $this->interaction->expects($this->once())
            ->method('isMemberNull')
            ->willReturn(false);

        $result = $this->interaction->validate();

        $this->assertFalse($result);
    }

    public function testValidateTrue()
    {
        $this->interaction = $this->getMockBuilder(MockInteraction::class)
                                ->setMethods([
                                    'isMemberNull',
                                    'isAcceptUserGroupNull'
                                ])
                                ->getMock();

        $this->interaction->expects($this->once())
            ->method('isMemberNull')
            ->willReturn(true);

        $this->interaction->expects($this->once())
                ->method('isAcceptUserGroupNull')
                ->willReturn(true);
        
        $result = $this->interaction->validate();

        $this->assertTrue($result);
    }

    public function testAccept()
    {
        $this->interaction = $this->getMockBuilder(MockInteraction::class)
                                ->setMethods([
                                    'updateAcceptStatus'
                                ])
                                ->getMock();

        $this->interaction->expects($this->once())
            ->method('updateAcceptStatus')
            ->willReturn(true);

        $result = $this->interaction->accept();

        $this->assertTrue($result);
    }

    public function testEdit()
    {
        $result = $this->interaction->edit();

        $this->assertFalse($result);
    }

    public function testIsNormal()
    {
        $this->interaction->setStatus(IInteractionAble::INTERACTION_STATUS['NORMAL']);

        $result = $this->interaction->isNormal();
        $this->assertTrue($result);
    }

    public function testIsPublish()
    {
        $this->interaction->setStatus(IInteractionAble::INTERACTION_STATUS['PUBLISH']);

        $result = $this->interaction->isPublish();
        $this->assertTrue($result);
    }

    public function testIsAcceptStatusPending()
    {
        $this->interaction->setAcceptStatus(IInteractionAble::ACCEPT_STATUS['PENDING']);

        $result = $this->interaction->isAcceptStatusPending();
        $this->assertTrue($result);
    }

    public function testIsAcceptStatusComplete()
    {
        $this->interaction->setAcceptStatus(IInteractionAble::ACCEPT_STATUS['COMPLETE']);

        $result = $this->interaction->isAcceptStatusComplete();
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        $this->interaction->setStatus(IInteractionAble::INTERACTION_STATUS['PUBLISH']);

        $result = $this->interaction->revoke();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testRevoke()
    {
        $interaction = $this->getMockBuilder(MockInteraction::class)
            ->setMethods(['updateStatus'])
            ->getMock();
            
        $interaction->setStatus(IInteractionAble::INTERACTION_STATUS['NORMAL']);
        $interaction->setAcceptStatus(IInteractionAble::ACCEPT_STATUS['PENDING']);

        $interaction->expects($this->once())
            ->method('updateStatus')
            ->willReturn(true);

        $result = $interaction->revoke();
        $this->assertTrue($result);
    }

    public function testPublishFail()
    {
        $this->interaction->setAcceptStatus(IInteractionAble::ACCEPT_STATUS['PENDING']);

        $result = $this->interaction->publish();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testPublish()
    {
        $interaction = $this->getMockBuilder(MockInteraction::class)
            ->setMethods(['updateStatus'])
            ->getMock();
            
        $interaction->setAcceptStatus(IInteractionAble::ACCEPT_STATUS['COMPLETE']);

        $interaction->expects($this->once())
            ->method('updateStatus')
            ->willReturn(true);

        $result = $interaction->publish();
        $this->assertTrue($result);
    }

    public function testUnPublishFail()
    {
        $this->interaction->setStatus(IInteractionAble::INTERACTION_STATUS['NORMAL']);

        $result = $this->interaction->unPublish();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testUnPublish()
    {
        $interaction = $this->getMockBuilder(MockInteraction::class)
            ->setMethods(['updateStatus'])
            ->getMock();
            
        $interaction->setStatus(IInteractionAble::INTERACTION_STATUS['PUBLISH']);

        $interaction->expects($this->once())
            ->method('updateStatus')
            ->willReturn(true);

        $result = $interaction->unPublish();
        $this->assertTrue($result);
    }
}
