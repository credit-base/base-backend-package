<?php
namespace Base\Interaction\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Adapter\Appeal\IAppealAdapter;

class AppealTest extends TestCase
{
    private $appeal;

    private $faker;

    public function setUp()
    {
        $this->appeal = new MockAppeal();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->appeal);
        unset($this->faker);
    }

    public function testExtendsCommentInteraction()
    {
        $this->assertInstanceof('Base\Interaction\Model\CommentInteraction', $this->appeal);
    }
    
    public function testSearchDataConstructor()
    {
        $this->assertEquals(array(), $this->appeal->getCertificates());
    }

    //certificates 测试 ------------------------------------------------------- start
    /**
     * 设置 setCertificates() 正确的传参类型,期望传值正确
     */
    public function testSetCertificatesCorrectType()
    {
        $certificates = $this->faker->words(3, false);

        $this->appeal->setCertificates($certificates);
        $this->assertEquals($certificates, $this->appeal->getCertificates());
    }

    /**
     * 设置 setCertificates() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCertificatesWrongType()
    {
        $this->appeal->setCertificates($this->faker->word());
    }
    //certificates 测试 -------------------------------------------------------   end

    public function testGetRepository()
    {
        $appeal = new MockAppeal();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\IAppealAdapter',
            $appeal->getRepository()
        );
    }

    public function testAdd()
    {
        $appeal = $this->getMockBuilder(MockAppeal::class)
                        ->setMethods(['validate', 'getRepository'])
                        ->getMock();

        $appeal->expects($this->once())->method('validate') ->willReturn(true);

        $repository = $this->prophesize(IAppealAdapter::class);
        $repository->add(Argument::exact($appeal))->shouldBeCalledTimes(1)->willReturn(true);

        $appeal->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $appeal->add();

        $this->assertTrue($result);
    }

    public function testUpdateAcceptStatus()
    {
        $appeal = $this->getMockBuilder(MockAppeal::class)
                    ->setMethods(['getRepository', 'setUpdateTime'])
                    ->getMock();
            
        $acceptStatus = IInteractionAble::ACCEPT_STATUS['PENDING'];

        $appeal->setAcceptStatus($acceptStatus);
        $appeal->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));

        $repository = $this->prophesize(IAppealAdapter::class);
        $repository->edit(
            Argument::exact($appeal),
            Argument::exact(array(
                'acceptStatus','updateTime','reply'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $appeal->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        $result = $appeal->updateAcceptStatus($acceptStatus);
        $this->assertTrue($result);
    }

    public function testUpdateStatus()
    {
        $appeal = $this->getMockBuilder(MockAppeal::class)
                    ->setMethods(['getRepository', 'setUpdateTime', 'setStatusTime'])
                    ->getMock();
            
        $status = IInteractionAble::INTERACTION_STATUS['NORMAL'];

        $appeal->setStatus($status);
        $appeal->expects($this->exactly(1))->method('setStatusTime')->with(Core::$container->get('time'));
        $appeal->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));

        $repository = $this->prophesize(IAppealAdapter::class);
        $repository->edit(
            Argument::exact($appeal),
            Argument::exact(array(
                'statusTime','status','updateTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $appeal->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        $result = $appeal->updateStatus($status);
        $this->assertTrue($result);
    }
}
