<?php
namespace Base\Interaction\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Translator\QADbTranslator;

class UnAuditedQATest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new UnAuditedQA();
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Base\ApplyForm\Model\IApplyFormAble', $this->stub);
    }

    public function testCorrectExtendsQA()
    {
        $this->assertInstanceof('Base\Interaction\Model\QA', $this->stub);
    }

    public function testUnAuditedQAConstructor()
    {
        $this->assertEquals(0, $this->stub->getApplyId());
        $this->assertEquals('', $this->stub->getApplyTitle());
        $this->assertEquals(UnAuditedQA::OPERATION_TYPE['NULL'], $this->stub->getOperationType());
        $this->assertEquals(array(), $this->stub->getApplyInfo());
        $this->assertEquals(UnAuditedQA::APPLY_STATUS['PENDING'], $this->stub->getApplyStatus());
        $this->assertEquals('', $this->stub->getRejectReason());
        $this->assertInstanceOf(
            'Base\Member\Model\Member',
            $this->stub->getRelation()
        );
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->stub->getApplyCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->stub->getApplyUserGroup()
        );
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\ApplyInfoCategory',
            $this->stub->getApplyInfoCategory()
        );
    }

    public function testAdd()
    {
        $unAuditedQA = $this->getMockBuilder(MockUnAuditedQA::class)
                           ->setMethods(['applyInfo', 'setApplyInfo', 'apply'])
                           ->getMock();

        $applyInfo = array('applyInfo');
        $unAuditedQA->expects($this->exactly(1))->method('applyInfo')->willReturn($applyInfo);
        $unAuditedQA->expects($this->exactly(1))->method('setApplyInfo')->with($applyInfo);
        $unAuditedQA->expects($this->exactly(1))->method('apply')->willReturn(true);

        $result = $unAuditedQA->add();
        $this->assertTrue($result);
    }

    public function testGetQADbTranslator()
    {
        $unAuditedQA = new MockUnAuditedQA();

        $this->assertInstanceOf(
            'Base\Interaction\Translator\QADbTranslator',
            $unAuditedQA->getQADbTranslator()
        );
    }

    public function testApplyInfo()
    {
        $unAuditedQA = $this->getMockBuilder(MockUnAuditedQA::class)
                           ->setMethods(['getQADbTranslator'])
                           ->getMock();

        $applyInfo = array('applyInfo');

        $translator = $this->prophesize(QADbTranslator::class);
        $translator->objectToArray(Argument::exact($unAuditedQA))->shouldBeCalledTimes(1)->willReturn($applyInfo);

        $unAuditedQA->expects($this->once())->method('getQADbTranslator')->willReturn($translator->reveal());

        $result = $unAuditedQA->applyInfo();
        $this->assertEquals($applyInfo, $result);
    }
}
