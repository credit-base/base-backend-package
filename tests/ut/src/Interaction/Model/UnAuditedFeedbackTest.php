<?php
namespace Base\Interaction\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Translator\FeedbackDbTranslator;

class UnAuditedFeedbackTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new UnAuditedFeedback();
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Base\ApplyForm\Model\IApplyFormAble', $this->stub);
    }

    public function testCorrectExtendsFeedback()
    {
        $this->assertInstanceof('Base\Interaction\Model\Feedback', $this->stub);
    }

    public function testUnAuditedFeedbackConstructor()
    {
        $this->assertEquals(0, $this->stub->getApplyId());
        $this->assertEquals('', $this->stub->getApplyTitle());
        $this->assertEquals(UnAuditedFeedback::OPERATION_TYPE['NULL'], $this->stub->getOperationType());
        $this->assertEquals(array(), $this->stub->getApplyInfo());
        $this->assertEquals(UnAuditedFeedback::APPLY_STATUS['PENDING'], $this->stub->getApplyStatus());
        $this->assertEquals('', $this->stub->getRejectReason());
        $this->assertInstanceOf(
            'Base\Member\Model\Member',
            $this->stub->getRelation()
        );
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->stub->getApplyCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->stub->getApplyUserGroup()
        );
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\ApplyInfoCategory',
            $this->stub->getApplyInfoCategory()
        );
    }

    public function testAdd()
    {
        $unAuditedFeedback = $this->getMockBuilder(MockUnAuditedFeedback::class)
                           ->setMethods(['applyInfo', 'setApplyInfo', 'apply'])
                           ->getMock();

        $applyInfo = array('applyInfo');
        $unAuditedFeedback->expects($this->exactly(1))->method('applyInfo')->willReturn($applyInfo);
        $unAuditedFeedback->expects($this->exactly(1))->method('setApplyInfo')->with($applyInfo);
        $unAuditedFeedback->expects($this->exactly(1))->method('apply')->willReturn(true);

        $result = $unAuditedFeedback->add();
        $this->assertTrue($result);
    }

    public function testGetFeedbackDbTranslator()
    {
        $unAuditedFeedback = new MockUnAuditedFeedback();

        $this->assertInstanceOf(
            'Base\Interaction\Translator\FeedbackDbTranslator',
            $unAuditedFeedback->getFeedbackDbTranslator()
        );
    }

    public function testApplyInfo()
    {
        $unAuditedFeedback = $this->getMockBuilder(MockUnAuditedFeedback::class)
                           ->setMethods(['getFeedbackDbTranslator'])
                           ->getMock();

        $applyInfo = array('applyInfo');

        $translator = $this->prophesize(FeedbackDbTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditedFeedback)
        )->shouldBeCalledTimes(1)->willReturn($applyInfo);

        $unAuditedFeedback->expects($this->once())->method(
            'getFeedbackDbTranslator'
        )->willReturn($translator->reveal());

        $result = $unAuditedFeedback->applyInfo();
        $this->assertEquals($applyInfo, $result);
    }
}
