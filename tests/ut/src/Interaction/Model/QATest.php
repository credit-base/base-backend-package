<?php
namespace Base\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class QATest extends TestCase
{
    private $qaInteraction;

    private $faker;

    public function setUp()
    {
        $this->qaInteraction = new MockQA();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->qaInteraction);
        unset($this->faker);
    }

    public function testExtendsInteraction()
    {
        $this->assertInstanceof('Base\Interaction\Model\Interaction', $this->qaInteraction);
    }
    
    public function testGetRepository()
    {
        $qaInteraction = new MockQA();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\QA\IQAAdapter',
            $qaInteraction->getRepository()
        );
    }
}
