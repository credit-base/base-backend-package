<?php
namespace Base\Interaction\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Translator\AppealDbTranslator;

class UnAuditedAppealTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new UnAuditedAppeal();
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Base\ApplyForm\Model\IApplyFormAble', $this->stub);
    }

    public function testCorrectExtendsAppeal()
    {
        $this->assertInstanceof('Base\Interaction\Model\Appeal', $this->stub);
    }

    public function testUnAuditedAppealConstructor()
    {
        $this->assertEquals(0, $this->stub->getApplyId());
        $this->assertEquals('', $this->stub->getApplyTitle());
        $this->assertEquals(UnAuditedAppeal::OPERATION_TYPE['NULL'], $this->stub->getOperationType());
        $this->assertEquals(array(), $this->stub->getApplyInfo());
        $this->assertEquals(UnAuditedAppeal::APPLY_STATUS['PENDING'], $this->stub->getApplyStatus());
        $this->assertEquals('', $this->stub->getRejectReason());
        $this->assertInstanceOf(
            'Base\Member\Model\Member',
            $this->stub->getRelation()
        );
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->stub->getApplyCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->stub->getApplyUserGroup()
        );
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\ApplyInfoCategory',
            $this->stub->getApplyInfoCategory()
        );
    }

    public function testAdd()
    {
        $unAuditedAppeal = $this->getMockBuilder(MockUnAuditedAppeal::class)
                           ->setMethods(['applyInfo', 'setApplyInfo', 'apply'])
                           ->getMock();

        $applyInfo = array('applyInfo');
        $unAuditedAppeal->expects($this->exactly(1))->method('applyInfo')->willReturn($applyInfo);
        $unAuditedAppeal->expects($this->exactly(1))->method('setApplyInfo')->with($applyInfo);
        $unAuditedAppeal->expects($this->exactly(1))->method('apply')->willReturn(true);

        $result = $unAuditedAppeal->add();
        $this->assertTrue($result);
    }

    public function testGetAppealDbTranslator()
    {
        $unAuditedAppeal = new MockUnAuditedAppeal();

        $this->assertInstanceOf(
            'Base\Interaction\Translator\AppealDbTranslator',
            $unAuditedAppeal->getAppealDbTranslator()
        );
    }

    public function testApplyInfo()
    {
        $unAuditedAppeal = $this->getMockBuilder(MockUnAuditedAppeal::class)
                           ->setMethods(['getAppealDbTranslator'])
                           ->getMock();

        $applyInfo = array('applyInfo');

        $translator = $this->prophesize(AppealDbTranslator::class);
        $translator->objectToArray(Argument::exact($unAuditedAppeal))->shouldBeCalledTimes(1)->willReturn($applyInfo);

        $unAuditedAppeal->expects($this->once())->method('getAppealDbTranslator')->willReturn($translator->reveal());

        $result = $unAuditedAppeal->applyInfo();
        $this->assertEquals($applyInfo, $result);
    }
}
