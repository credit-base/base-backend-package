<?php
namespace Base\Interaction\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Model\Crew;
use Base\Crew\Model\NullCrew;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Model\NullUserGroup;

use Base\Interaction\Adapter\Reply\IReplyAdapter;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ReplyTest extends TestCase
{
    private $reply;

    public function setUp()
    {
        $this->reply = new MockReply();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->reply);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->reply
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'Base\Common\Model\IOperate',
            $this->reply
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Reply\IReplyAdapter',
            $this->reply->getRepository()
        );
    }

    public function testReplyConstructor()
    {
        $this->assertEquals(0, $this->reply->getId());
        $this->assertEmpty($this->reply->getContent());
        $this->assertEquals(array(), $this->reply->getImages());
        $this->assertEquals(IInteractionAble::ADMISSIBILITY['ADMISSIBLE'], $this->reply->getAdmissibility());
        $this->assertInstanceOf('Base\Crew\Model\Crew', $this->reply->getCrew());
        $this->assertInstanceOf('Base\UserGroup\Model\UserGroup', $this->reply->getAcceptUserGroup());
        $this->assertEquals(Reply::STATUS_NORMAL, $this->reply->getStatus());
        $this->assertEquals(Core::$container->get('time'), $this->reply->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $this->reply->getCreateTime());
        $this->assertEquals(0, $this->reply->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Reply setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->reply->setId(1);
        $this->assertEquals(1, $this->reply->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //content 测试 ------------------------------------------------------- start
    /**
     * 设置 Reply setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->reply->setContent('string');
        $this->assertEquals('string', $this->reply->getContent());
    }

    /**
     * 设置 Reply setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->reply->setContent(array(1, 2, 3));
    }
    //content 测试 -------------------------------------------------------   end
    
    //images 测试 ------------------------------------------------------- start
    /**
     * 设置 Reply setImages() 正确的传参类型,期望传值正确
     */
    public function testSetImagesCorrectType()
    {
        $this->reply->setImages(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->reply->getImages());
    }

    /**
     * 设置 Reply setImages() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetImagesWrongType()
    {
        $this->reply->setImages('reply');
    }
    //images 测试 -------------------------------------------------------   end

    //acceptUserGroup 测试 --------------------------------------------- start
    /**
     * 设置 setAcceptUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptUserGroupCorrectType()
    {
        $object = new UserGroup();
        $this->reply->setAcceptUserGroup($object);
        $this->assertSame($object, $this->reply->getAcceptUserGroup());
    }

    /**
     * 设置 setAcceptUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptUserGroupWrongType()
    {
        $this->reply->setAcceptUserGroup('string');
    }
    //acceptUserGroup 测试 ---------------------------------------------   end

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->reply->setCrew($object);
        $this->assertSame($object, $this->reply->getCrew());
    }

    /**
     * 设置 setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->reply->setCrew('string');
    }
    //crew 测试 ---------------------------------------------   end

    //admissibility 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setAdmissibility() 是否符合预定范围
     *
     * @dataProvider admissibilityProvider
     */
    public function testSetAdmissibility($actual, $expected)
    {
        $this->reply->setAdmissibility($actual);
        $this->assertEquals($expected, $this->reply->getAdmissibility());
    }
    /**
     * 循环测试 DispatchDepartment setAdmissibility() 数据构建器
     */
    public function admissibilityProvider()
    {
        return array(
            array(IInteractionAble::ADMISSIBILITY['ADMISSIBLE'], IInteractionAble::ADMISSIBILITY['ADMISSIBLE']),
            array(IInteractionAble::ADMISSIBILITY['INADMISSIBLE'], IInteractionAble::ADMISSIBILITY['INADMISSIBLE']),
            array(999, IInteractionAble::ADMISSIBILITY['ADMISSIBLE'])
        );
    }
    //admissibility 测试 ------------------------------------------------------ end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Reply setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->reply->setStatus($actual);
        $this->assertEquals($expected, $this->reply->getStatus());
    }

    /**
     * 循环测试 Reply setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Reply::STATUS_NORMAL,Reply::STATUS_NORMAL)
        );
    }

    /**
     * 设置 Reply setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->reply->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    public function testIsCrewNullTrue()
    {
        $crew = \Base\Crew\Utils\MockFactory::generateCrew(1);
        $this->reply->setCrew($crew);

        $result = $this->reply->isCrewNull();
        $this->assertTrue($result);
    }

    public function testIsCrewNullFalse()
    {
        $crew = new NullCrew();
        $this->reply->setCrew($crew);
        
        $result = $this->reply->isCrewNull();
        
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('crewId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsAcceptUserGroupNullTrue()
    {
        $acceptUserGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup(1);
        $this->reply->setAcceptUserGroup($acceptUserGroup);

        $result = $this->reply->isAcceptUserGroupNull();
        $this->assertTrue($result);
    }

    public function testIsAcceptUserGroupNullFalse()
    {
        $acceptUserGroup = new NullUserGroup();
        $this->reply->setAcceptUserGroup($acceptUserGroup);
        
        $result = $this->reply->isAcceptUserGroupNull();
        
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('acceptUserGroupId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testAdd()
    {
        //初始化
        $reply = $this->getMockBuilder(MockReply::class)
                           ->setMethods(['getRepository', 'isCrewNull', 'isAcceptUserGroupNull'])
                           ->getMock();

        $reply->expects($this->once())->method('isCrewNull')->willReturn(true);
        $reply->expects($this->once())->method('isAcceptUserGroupNull')->willReturn(true);

        //预言 IReplyAdapter
        $repository = $this->prophesize(IReplyAdapter::class);
        $repository->add(Argument::exact($reply))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $reply->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        //验证
        $result = $reply->add();
        $this->assertTrue($result);
    }

    public function testEdit()
    {
        $reply = $this->getMockBuilder(MockReply::class)
            ->setMethods(['getRepository', 'setUpdateTime', 'isCrewNull'])
            ->getMock();

        $reply->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));
        $reply->expects($this->once())->method('isCrewNull')->willReturn(true);

        //预言 IReplyAdapter
        $repository = $this->prophesize(IReplyAdapter::class);
        $repository->edit(
            Argument::exact($reply),
            Argument::exact(array(
                'crew',
                'images',
                'content',
                'admissibility',
                'updateTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $reply->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $reply->edit();
        $this->assertTrue($result);
    }
}
