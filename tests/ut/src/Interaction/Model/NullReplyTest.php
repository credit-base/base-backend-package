<?php
namespace Base\Interaction\Model;

use PHPUnit\Framework\TestCase;

class NullReplyTest extends TestCase
{
    private $reply;

    public function setUp()
    {
        $this->reply = $this->getMockBuilder(NullReply::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->reply);
    }

    public function testExtendsReply()
    {
        $this->assertInstanceof('Base\Interaction\Model\Reply', $this->reply);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->reply);
    }

    public function testIsCrewNull()
    {
        $this->reply->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $this->reply->isCrewNull();
        $this->assertFalse($result);
    }

    public function testIsAcceptUserGroupNull()
    {
        $this->reply->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $this->reply->isAcceptUserGroupNull();
        $this->assertFalse($result);
    }
}
