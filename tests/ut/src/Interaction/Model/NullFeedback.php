<?php
namespace Base\Interaction\Model;

use PHPUnit\Framework\TestCase;

class NullFeedbackTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullFeedback::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsFeedback()
    {
        $this->assertInstanceof('Base\Interaction\Model\Feedback', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
