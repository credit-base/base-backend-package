<?php
namespace Base\Interaction\Model;

use PHPUnit\Framework\TestCase;

/**
 *
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullInteractionTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullInteractionTrait::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testIsMemberNull()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isMemberNull();
        $this->assertFalse($result);
    }

    public function testIsAcceptUserGroupNull()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isAcceptUserGroupNull();
        $this->assertFalse($result);
    }

    public function testValidate()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->validate();
        $this->assertFalse($result);
    }

    public function testUpdateAcceptStatus()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->updateAcceptStatus(IInteractionAble::ACCEPT_STATUS['PENDING']);
        $this->assertFalse($result);
    }

    public function testUpdateStatus()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->updateStatus(IInteractionAble::INTERACTION_STATUS['NORMAL']);
        $this->assertFalse($result);
    }

    public function testAccept()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->accept();
        $this->assertFalse($result);
    }

    public function testIsNormal()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isNormal();
        $this->assertFalse($result);
    }

    public function testIsPublish()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isPublish();
        $this->assertFalse($result);
    }

    public function testIsAcceptStatusComplete()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isAcceptStatusComplete();
        $this->assertFalse($result);
    }

    public function testRevoke()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->revoke();
        $this->assertFalse($result);
    }

    public function testPublish()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->publish();
        $this->assertFalse($result);
    }

    public function testUnPublish()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->unPublish();
        $this->assertFalse($result);
    }

    private function mockResourceNotExist()
    {
        $this->trait->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }
}
