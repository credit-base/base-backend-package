<?php
namespace Base\Interaction\Model;

use PHPUnit\Framework\TestCase;

class NullAppealTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullAppeal::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsAppeal()
    {
        $this->assertInstanceof('Base\Interaction\Model\Appeal', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
