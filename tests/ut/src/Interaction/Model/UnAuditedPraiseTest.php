<?php
namespace Base\Interaction\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Translator\PraiseDbTranslator;

class UnAuditedPraiseTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new UnAuditedPraise();
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Base\ApplyForm\Model\IApplyFormAble', $this->stub);
    }

    public function testCorrectExtendsPraise()
    {
        $this->assertInstanceof('Base\Interaction\Model\Praise', $this->stub);
    }

    public function testUnAuditedPraiseConstructor()
    {
        $this->assertEquals(0, $this->stub->getApplyId());
        $this->assertEquals('', $this->stub->getApplyTitle());
        $this->assertEquals(UnAuditedPraise::OPERATION_TYPE['NULL'], $this->stub->getOperationType());
        $this->assertEquals(array(), $this->stub->getApplyInfo());
        $this->assertEquals(UnAuditedPraise::APPLY_STATUS['PENDING'], $this->stub->getApplyStatus());
        $this->assertEquals('', $this->stub->getRejectReason());
        $this->assertInstanceOf(
            'Base\Member\Model\Member',
            $this->stub->getRelation()
        );
        $this->assertInstanceOf(
            'Base\Crew\Model\Crew',
            $this->stub->getApplyCrew()
        );
        $this->assertInstanceOf(
            'Base\UserGroup\Model\UserGroup',
            $this->stub->getApplyUserGroup()
        );
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\ApplyInfoCategory',
            $this->stub->getApplyInfoCategory()
        );
    }

    public function testAdd()
    {
        $unAuditedPraise = $this->getMockBuilder(MockUnAuditedPraise::class)
                           ->setMethods(['applyInfo', 'setApplyInfo', 'apply'])
                           ->getMock();

        $applyInfo = array('applyInfo');
        $unAuditedPraise->expects($this->exactly(1))->method('applyInfo')->willReturn($applyInfo);
        $unAuditedPraise->expects($this->exactly(1))->method('setApplyInfo')->with($applyInfo);
        $unAuditedPraise->expects($this->exactly(1))->method('apply')->willReturn(true);

        $result = $unAuditedPraise->add();
        $this->assertTrue($result);
    }

    public function testGetPraiseDbTranslator()
    {
        $unAuditedPraise = new MockUnAuditedPraise();

        $this->assertInstanceOf(
            'Base\Interaction\Translator\PraiseDbTranslator',
            $unAuditedPraise->getPraiseDbTranslator()
        );
    }

    public function testApplyInfo()
    {
        $unAuditedPraise = $this->getMockBuilder(MockUnAuditedPraise::class)
                           ->setMethods(['getPraiseDbTranslator'])
                           ->getMock();

        $applyInfo = array('applyInfo');

        $translator = $this->prophesize(PraiseDbTranslator::class);
        $translator->objectToArray(Argument::exact($unAuditedPraise))->shouldBeCalledTimes(1)->willReturn($applyInfo);

        $unAuditedPraise->expects($this->once())->method('getPraiseDbTranslator')->willReturn($translator->reveal());

        $result = $unAuditedPraise->applyInfo();
        $this->assertEquals($applyInfo, $result);
    }
}
