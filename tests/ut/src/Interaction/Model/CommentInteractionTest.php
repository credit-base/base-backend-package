<?php
namespace Base\Interaction\Model;

use PHPUnit\Framework\TestCase;

/**
 *
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class CommentInteractionTest extends TestCase
{
    private $interaction;

    private $faker;

    public function setUp()
    {
        $this->interaction = $this->getMockBuilder(CommentInteraction::class)
                                ->getMockForAbstractClass();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->interaction);
        unset($this->faker);
    }

    public function testExtendsInteraction()
    {
        $this->assertInstanceof('Base\Interaction\Model\Interaction', $this->interaction);
    }

    public function testInteractionConstructor()
    {
        $this->assertEquals('', $this->interaction->getName());
        $this->assertEquals('', $this->interaction->getIdentify());
        $this->assertEquals('', $this->interaction->getContact());
        $this->assertEquals(array(), $this->interaction->getImages());
        $this->assertEquals(IInteractionAble::TYPE['PERSONAL'], $this->interaction->getType());
    }
    
    //name 测试 ------------------------------------------------------- start
    /**
     * 设置 setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $name = $this->faker->name();

        $this->interaction->setName($name);
        $this->assertEquals($name, $this->interaction->getName());
    }

    /**
     * 设置 setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->interaction->setName($this->faker->words(3, false));
    }
    //name 测试 -------------------------------------------------------   end
    
    //identify 测试 ------------------------------------------------------- start
    /**
     * 设置 setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $identify = $this->faker->bothify('##############????');

        $this->interaction->setIdentify($identify);
        $this->assertEquals($identify, $this->interaction->getIdentify());
    }

    /**
     * 设置 setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->interaction->setIdentify($this->faker->words(3, false));
    }
    //identify 测试 -------------------------------------------------------   end

    //contact 测试 ------------------------------------------------------- start
    /**
     * 设置 setContact() 正确的传参类型,期望传值正确
     */
    public function testSetContactCorrectType()
    {
        $contact = $this->faker->bothify('##############????');

        $this->interaction->setContact($contact);
        $this->assertEquals($contact, $this->interaction->getContact());
    }

    /**
     * 设置 setContact() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContactWrongType()
    {
        $this->interaction->setContact($this->faker->words(3, false));
    }
    //contact 测试 -------------------------------------------------------   end

    //images 测试 ------------------------------------------------------- start
    /**
     * 设置 setImages() 正确的传参类型,期望传值正确
     */
    public function testSetImagesCorrectType()
    {
        $images = $this->faker->words(3, false);

        $this->interaction->setImages($images);
        $this->assertEquals($images, $this->interaction->getImages());
    }

    /**
     * 设置 setImages() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetImagesWrongType()
    {
        $this->interaction->setImages($this->faker->word());
    }
    //images 测试 -------------------------------------------------------   end

    //type 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setType() 是否符合预定范围
     *
     * @dataProvider typeProvider
     */
    public function testSetType($actual, $expected)
    {
        $this->interaction->setType($actual);
        $this->assertEquals($expected, $this->interaction->getType());
    }
    /**
     * 循环测试 DispatchDepartment setType() 数据构建器
     */
    public function typeProvider()
    {
        return array(
            array(IInteractionAble::TYPE['PERSONAL'], IInteractionAble::TYPE['PERSONAL']),
            array(IInteractionAble::TYPE['ENTERPRISE'], IInteractionAble::TYPE['ENTERPRISE']),
            array(IInteractionAble::TYPE['GOVERNMENT'], IInteractionAble::TYPE['GOVERNMENT']),
            array(999, IInteractionAble::TYPE['PERSONAL'])
        );
    }
    //type 测试 ------------------------------------------------------ end
}
