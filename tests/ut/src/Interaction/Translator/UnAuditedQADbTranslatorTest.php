<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\QAUtils;

class UnAuditedQADbTranslatorTest extends TestCase
{
    use QAUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditedQADbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testExtendsApplyFormDbTranslator()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Translator\ApplyFormDbTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $unAuditedQA = \Base\Interaction\Utils\QAMockFactory::generateUnAuditedQA(1);

        $expression['apply_form_id'] = $unAuditedQA->getId();
        $expression['qa_id'] = $unAuditedQA->getId();

        $expression['title'] = $unAuditedQA->getApplyTitle();
        $expression['relation_id'] = $unAuditedQA->getRelation()->getId();
        $expression['operation_type'] = $unAuditedQA->getOperationType();
        $expression['apply_info_category'] = $unAuditedQA->getApplyInfoCategory()->getCategory();
        $expression['apply_info_type'] = $unAuditedQA->getApplyInfoCategory()->getType();
        $expression['reject_reason'] = $unAuditedQA->getRejectReason();
        $expression['apply_info'] = base64_encode(gzcompress(serialize($unAuditedQA->getApplyInfo())));
        $expression['apply_crew_id'] = $unAuditedQA->getApplyCrew()->getId();
        $expression['apply_usergroup_id'] = $unAuditedQA->getApplyUserGroup()->getId();
        $expression['apply_status'] = $unAuditedQA->getApplyStatus();
        $expression['status_time'] = $unAuditedQA->getStatusTime();
        $expression['create_time'] = $unAuditedQA->getCreateTime();
        $expression['update_time'] = $unAuditedQA->getUpdateTime();

        $unAuditedQA = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\UnAuditedQA', $unAuditedQA);
        $this->compareArrayAndObjectUnAuditedQA($expression, $unAuditedQA);
    }

    public function testArrayToObjects()
    {
        $unAuditedQA = \Base\Interaction\Utils\QAMockFactory::generateUnAuditedQA(1);

        $expression['qa_id'] = $unAuditedQA->getApplyId();

        $expression['title'] = $unAuditedQA->getTitle();
        $expression['content'] = $unAuditedQA->getContent();
        $expression['accept_status'] = $unAuditedQA->getAcceptStatus();
        $expression['member_id'] = $unAuditedQA->getMember()->getId();
        $expression['reply_id'] = $unAuditedQA->getReply()->getId();
        $expression['accept_usergroup_id'] = $unAuditedQA->getAcceptUserGroup()->getId();
        $expression['status'] = $unAuditedQA->getStatus();
        $expression['status_time'] = $unAuditedQA->getStatusTime();
        $expression['create_time'] = $unAuditedQA->getCreateTime();
        $expression['update_time'] = $unAuditedQA->getUpdateTime();

        $unAuditedQA = $this->translator->arrayToObjects($expression, $unAuditedQA);

        $this->assertInstanceof('Base\Interaction\Model\UnAuditedQA', $unAuditedQA);
        $this->compareArrayAndObjectsUnAuditedQA($expression, $unAuditedQA);
    }

    public function testObjectToArray()
    {
        $unAuditedQA = \Base\Interaction\Utils\QAMockFactory::generateUnAuditedQA(1);

        $expression = $this->translator->objectToArray($unAuditedQA);

        $this->compareArrayAndObjectUnAuditedQA($expression, $unAuditedQA);
    }
}
