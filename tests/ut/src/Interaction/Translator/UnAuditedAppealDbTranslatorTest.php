<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\AppealUtils;

class UnAuditedAppealDbTranslatorTest extends TestCase
{
    use AppealUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditedAppealDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testExtendsApplyFormDbTranslator()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Translator\ApplyFormDbTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(1);

        $expression['apply_form_id'] = $unAuditedAppeal->getId();
        $expression['appeal_id'] = $unAuditedAppeal->getId();

        $expression['title'] = $unAuditedAppeal->getApplyTitle();
        $expression['relation_id'] = $unAuditedAppeal->getRelation()->getId();
        $expression['operation_type'] = $unAuditedAppeal->getOperationType();
        $expression['apply_info_category'] = $unAuditedAppeal->getApplyInfoCategory()->getCategory();
        $expression['apply_info_type'] = $unAuditedAppeal->getApplyInfoCategory()->getType();
        $expression['reject_reason'] = $unAuditedAppeal->getRejectReason();
        $expression['apply_info'] = base64_encode(gzcompress(serialize($unAuditedAppeal->getApplyInfo())));
        $expression['apply_crew_id'] = $unAuditedAppeal->getApplyCrew()->getId();
        $expression['apply_usergroup_id'] = $unAuditedAppeal->getApplyUserGroup()->getId();
        $expression['apply_status'] = $unAuditedAppeal->getApplyStatus();
        $expression['status_time'] = $unAuditedAppeal->getStatusTime();
        $expression['create_time'] = $unAuditedAppeal->getCreateTime();
        $expression['update_time'] = $unAuditedAppeal->getUpdateTime();

        $unAuditedAppeal = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\UnAuditedAppeal', $unAuditedAppeal);
        $this->compareArrayAndObjectUnAuditedAppeal($expression, $unAuditedAppeal);
    }

    public function testArrayToObjects()
    {
        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(1);

        $expression['appeal_id'] = $unAuditedAppeal->getApplyId();

        $expression['title'] = $unAuditedAppeal->getTitle();
        $expression['content'] = $unAuditedAppeal->getContent();
        $expression['name'] = $unAuditedAppeal->getName();
        $expression['identify'] = $unAuditedAppeal->getIdentify();
        $expression['type'] = $unAuditedAppeal->getType();
        $expression['contact'] = $unAuditedAppeal->getContact();
        $expression['accept_status'] = $unAuditedAppeal->getAcceptStatus();
        $expression['images'] = json_encode($unAuditedAppeal->getImages());
        $expression['certificates'] = json_encode($unAuditedAppeal->getCertificates());
        $expression['member_id'] = $unAuditedAppeal->getMember()->getId();
        $expression['reply_id'] = $unAuditedAppeal->getReply()->getId();
        $expression['accept_usergroup_id'] = $unAuditedAppeal->getAcceptUserGroup()->getId();
        $expression['status'] = $unAuditedAppeal->getStatus();
        $expression['status_time'] = $unAuditedAppeal->getStatusTime();
        $expression['create_time'] = $unAuditedAppeal->getCreateTime();
        $expression['update_time'] = $unAuditedAppeal->getUpdateTime();

        $unAuditedAppeal = $this->translator->arrayToObjects($expression, $unAuditedAppeal);

        $this->assertInstanceof('Base\Interaction\Model\UnAuditedAppeal', $unAuditedAppeal);
        $this->compareArrayAndObjectsUnAuditedAppeal($expression, $unAuditedAppeal);
    }

    public function testArrayToObjectsCertificates()
    {
        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(1);

        $expression['appeal_id'] = $unAuditedAppeal->getApplyId();

        $expression['title'] = $unAuditedAppeal->getTitle();
        $expression['content'] = $unAuditedAppeal->getContent();
        $expression['name'] = $unAuditedAppeal->getName();
        $expression['identify'] = $unAuditedAppeal->getIdentify();
        $expression['type'] = $unAuditedAppeal->getType();
        $expression['contact'] = $unAuditedAppeal->getContact();
        $expression['accept_status'] = $unAuditedAppeal->getAcceptStatus();
        $expression['images'] = $unAuditedAppeal->getImages();
        $expression['certificates'] = $unAuditedAppeal->getCertificates();
        $expression['member_id'] = $unAuditedAppeal->getMember()->getId();
        $expression['reply_id'] = $unAuditedAppeal->getReply()->getId();
        $expression['accept_usergroup_id'] = $unAuditedAppeal->getAcceptUserGroup()->getId();
        $expression['status'] = $unAuditedAppeal->getStatus();
        $expression['status_time'] = $unAuditedAppeal->getStatusTime();
        $expression['create_time'] = $unAuditedAppeal->getCreateTime();
        $expression['update_time'] = $unAuditedAppeal->getUpdateTime();

        $unAuditedAppeal = $this->translator->arrayToObjects($expression, $unAuditedAppeal);

        $this->assertInstanceof('Base\Interaction\Model\UnAuditedAppeal', $unAuditedAppeal);
        $this->compareArrayAndObjectsUnAuditedAppeal($expression, $unAuditedAppeal);
    }

    public function testObjectToArray()
    {
        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(1);

        $expression = $this->translator->objectToArray($unAuditedAppeal);

        $this->compareArrayAndObjectUnAuditedAppeal($expression, $unAuditedAppeal);
    }
}
