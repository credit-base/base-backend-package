<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\QAUtils;
use Base\Interaction\Utils\QAMockFactory;

class QADbTranslatorTest extends TestCase
{
    use QAUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new QADbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Interaction\Model\NullQA', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $qaInteraction = QAMockFactory::generateQA(1);

        $expression['qa_id'] = $qaInteraction->getId();
        $expression['title'] = $qaInteraction->getTitle();
        $expression['content'] = $qaInteraction->getContent();
        $expression['accept_status'] = $qaInteraction->getAcceptStatus();
        $expression['member_id'] = $qaInteraction->getMember()->getId();
        $expression['reply_id'] = $qaInteraction->getReply()->getId();
        $expression['accept_usergroup_id'] = $qaInteraction->getAcceptUserGroup()->getId();

        $expression['status'] = $qaInteraction->getStatus();
        $expression['status_time'] = $qaInteraction->getStatusTime();
        $expression['create_time'] = $qaInteraction->getCreateTime();
        $expression['update_time'] = $qaInteraction->getUpdateTime();

        $qaInteraction = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\QA', $qaInteraction);
        $this->compareArrayAndObjectQA($expression, $qaInteraction);
    }

    public function testObjectToArray()
    {
        $qaInteraction = QAMockFactory::generateQA(1);

        $expression = $this->translator->objectToArray($qaInteraction);

        $this->compareArrayAndObjectQA($expression, $qaInteraction);
    }
}
