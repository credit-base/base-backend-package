<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\ComplaintUtils;

class UnAuditedComplaintDbTranslatorTest extends TestCase
{
    use ComplaintUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditedComplaintDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testExtendsApplyFormDbTranslator()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Translator\ApplyFormDbTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $unAuditedComplaint = \Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint(1);

        $expression['apply_form_id'] = $unAuditedComplaint->getId();
        $expression['complaint_id'] = $unAuditedComplaint->getId();

        $expression['title'] = $unAuditedComplaint->getApplyTitle();
        $expression['relation_id'] = $unAuditedComplaint->getRelation()->getId();
        $expression['operation_type'] = $unAuditedComplaint->getOperationType();
        $expression['apply_info_category'] = $unAuditedComplaint->getApplyInfoCategory()->getCategory();
        $expression['apply_info_type'] = $unAuditedComplaint->getApplyInfoCategory()->getType();
        $expression['reject_reason'] = $unAuditedComplaint->getRejectReason();
        $expression['apply_info'] = base64_encode(gzcompress(serialize($unAuditedComplaint->getApplyInfo())));
        $expression['apply_crew_id'] = $unAuditedComplaint->getApplyCrew()->getId();
        $expression['apply_usergroup_id'] = $unAuditedComplaint->getApplyUserGroup()->getId();
        $expression['apply_status'] = $unAuditedComplaint->getApplyStatus();
        $expression['status_time'] = $unAuditedComplaint->getStatusTime();
        $expression['create_time'] = $unAuditedComplaint->getCreateTime();
        $expression['update_time'] = $unAuditedComplaint->getUpdateTime();

        $unAuditedComplaint = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\UnAuditedComplaint', $unAuditedComplaint);
        $this->compareArrayAndObjectUnAuditedComplaint($expression, $unAuditedComplaint);
    }

    public function testArrayToObjects()
    {
        $unAuditedComplaint = \Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint(1);

        $expression['complaint_id'] = $unAuditedComplaint->getApplyId();

        $expression['title'] = $unAuditedComplaint->getTitle();
        $expression['content'] = $unAuditedComplaint->getContent();
        $expression['name'] = $unAuditedComplaint->getName();
        $expression['identify'] = $unAuditedComplaint->getIdentify();
        $expression['type'] = $unAuditedComplaint->getType();
        $expression['contact'] = $unAuditedComplaint->getContact();
        $expression['accept_status'] = $unAuditedComplaint->getAcceptStatus();
        $expression['images'] = json_encode($unAuditedComplaint->getImages());
        $expression['subject'] = $unAuditedComplaint->getSubject();
        $expression['member_id'] = $unAuditedComplaint->getMember()->getId();
        $expression['reply_id'] = $unAuditedComplaint->getReply()->getId();
        $expression['accept_usergroup_id'] = $unAuditedComplaint->getAcceptUserGroup()->getId();
        $expression['status'] = $unAuditedComplaint->getStatus();
        $expression['status_time'] = $unAuditedComplaint->getStatusTime();
        $expression['create_time'] = $unAuditedComplaint->getCreateTime();
        $expression['update_time'] = $unAuditedComplaint->getUpdateTime();

        $unAuditedComplaint = $this->translator->arrayToObjects($expression, $unAuditedComplaint);

        $this->assertInstanceof('Base\Interaction\Model\UnAuditedComplaint', $unAuditedComplaint);
        $this->compareArrayAndObjectsUnAuditedComplaint($expression, $unAuditedComplaint);
    }

    public function testObjectToArray()
    {
        $unAuditedComplaint = \Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint(1);

        $expression = $this->translator->objectToArray($unAuditedComplaint);

        $this->compareArrayAndObjectUnAuditedComplaint($expression, $unAuditedComplaint);
    }
}
