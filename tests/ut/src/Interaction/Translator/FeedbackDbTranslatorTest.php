<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\FeedbackUtils;
use Base\Interaction\Utils\FeedbackMockFactory;

class FeedbackDbTranslatorTest extends TestCase
{
    use FeedbackUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new FeedbackDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Interaction\Model\NullFeedback', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $feedback = FeedbackMockFactory::generateFeedback(1);

        $expression['feedback_id'] = $feedback->getId();
        $expression['title'] = $feedback->getTitle();
        $expression['content'] = $feedback->getContent();
        $expression['accept_status'] = $feedback->getAcceptStatus();
        $expression['member_id'] = $feedback->getMember()->getId();
        $expression['reply_id'] = $feedback->getReply()->getId();
        $expression['accept_usergroup_id'] = $feedback->getAcceptUserGroup()->getId();

        $expression['status'] = $feedback->getStatus();
        $expression['status_time'] = $feedback->getStatusTime();
        $expression['create_time'] = $feedback->getCreateTime();
        $expression['update_time'] = $feedback->getUpdateTime();

        $feedback = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\Feedback', $feedback);
        $this->compareArrayAndObjectFeedback($expression, $feedback);
    }

    public function testObjectToArray()
    {
        $feedback = FeedbackMockFactory::generateFeedback(1);

        $expression = $this->translator->objectToArray($feedback);

        $this->compareArrayAndObjectFeedback($expression, $feedback);
    }
}
