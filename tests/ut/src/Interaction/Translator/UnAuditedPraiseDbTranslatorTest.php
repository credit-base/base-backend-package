<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\PraiseUtils;

class UnAuditedPraiseDbTranslatorTest extends TestCase
{
    use PraiseUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditedPraiseDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testExtendsApplyFormDbTranslator()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Translator\ApplyFormDbTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $unAuditedPraise = \Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise(1);

        $expression['apply_form_id'] = $unAuditedPraise->getId();
        $expression['praise_id'] = $unAuditedPraise->getId();

        $expression['title'] = $unAuditedPraise->getApplyTitle();
        $expression['relation_id'] = $unAuditedPraise->getRelation()->getId();
        $expression['operation_type'] = $unAuditedPraise->getOperationType();
        $expression['apply_info_category'] = $unAuditedPraise->getApplyInfoCategory()->getCategory();
        $expression['apply_info_type'] = $unAuditedPraise->getApplyInfoCategory()->getType();
        $expression['reject_reason'] = $unAuditedPraise->getRejectReason();
        $expression['apply_info'] = base64_encode(gzcompress(serialize($unAuditedPraise->getApplyInfo())));
        $expression['apply_crew_id'] = $unAuditedPraise->getApplyCrew()->getId();
        $expression['apply_usergroup_id'] = $unAuditedPraise->getApplyUserGroup()->getId();
        $expression['apply_status'] = $unAuditedPraise->getApplyStatus();
        $expression['status_time'] = $unAuditedPraise->getStatusTime();
        $expression['create_time'] = $unAuditedPraise->getCreateTime();
        $expression['update_time'] = $unAuditedPraise->getUpdateTime();

        $unAuditedPraise = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\UnAuditedPraise', $unAuditedPraise);
        $this->compareArrayAndObjectUnAuditedPraise($expression, $unAuditedPraise);
    }

    public function testArrayToObjects()
    {
        $unAuditedPraise = \Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise(1);

        $expression['praise_id'] = $unAuditedPraise->getApplyId();

        $expression['title'] = $unAuditedPraise->getTitle();
        $expression['content'] = $unAuditedPraise->getContent();
        $expression['name'] = $unAuditedPraise->getName();
        $expression['identify'] = $unAuditedPraise->getIdentify();
        $expression['type'] = $unAuditedPraise->getType();
        $expression['contact'] = $unAuditedPraise->getContact();
        $expression['accept_status'] = $unAuditedPraise->getAcceptStatus();
        $expression['images'] = json_encode($unAuditedPraise->getImages());
        $expression['subject'] = $unAuditedPraise->getSubject();
        $expression['member_id'] = $unAuditedPraise->getMember()->getId();
        $expression['reply_id'] = $unAuditedPraise->getReply()->getId();
        $expression['accept_usergroup_id'] = $unAuditedPraise->getAcceptUserGroup()->getId();
        $expression['status'] = $unAuditedPraise->getStatus();
        $expression['status_time'] = $unAuditedPraise->getStatusTime();
        $expression['create_time'] = $unAuditedPraise->getCreateTime();
        $expression['update_time'] = $unAuditedPraise->getUpdateTime();

        $unAuditedPraise = $this->translator->arrayToObjects($expression, $unAuditedPraise);

        $this->assertInstanceof('Base\Interaction\Model\UnAuditedPraise', $unAuditedPraise);
        $this->compareArrayAndObjectsUnAuditedPraise($expression, $unAuditedPraise);
    }

    public function testObjectToArray()
    {
        $unAuditedPraise = \Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise(1);

        $expression = $this->translator->objectToArray($unAuditedPraise);

        $this->compareArrayAndObjectUnAuditedPraise($expression, $unAuditedPraise);
    }
}
