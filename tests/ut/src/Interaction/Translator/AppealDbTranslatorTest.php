<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\AppealUtils;
use Base\Interaction\Utils\AppealMockFactory;

class AppealDbTranslatorTest extends TestCase
{
    use AppealUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new AppealDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Interaction\Model\NullAppeal', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $appeal = AppealMockFactory::generateAppeal(1);

        $expression['appeal_id'] = $appeal->getId();
        $expression['title'] = $appeal->getTitle();
        $expression['content'] = $appeal->getContent();
        $expression['name'] = $appeal->getName();
        $expression['identify'] = $appeal->getIdentify();
        $expression['type'] = $appeal->getType();
        $expression['contact'] = $appeal->getContact();
        $expression['accept_status'] = $appeal->getAcceptStatus();
        $expression['images'] = $appeal->getImages();
        $expression['certificates'] = $appeal->getCertificates();
        $expression['member_id'] = $appeal->getMember()->getId();
        $expression['reply_id'] = $appeal->getReply()->getId();
        $expression['accept_usergroup_id'] = $appeal->getAcceptUserGroup()->getId();

        $expression['status'] = $appeal->getStatus();
        $expression['status_time'] = $appeal->getStatusTime();
        $expression['create_time'] = $appeal->getCreateTime();
        $expression['update_time'] = $appeal->getUpdateTime();

        $appeal = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\Appeal', $appeal);
        $this->compareArrayAndObjectAppeal($expression, $appeal);
    }

    public function testArrayToObjectCertificates()
    {
        $appeal = AppealMockFactory::generateAppeal(1);

        $expression['appeal_id'] = $appeal->getId();
        $expression['title'] = $appeal->getTitle();
        $expression['content'] = $appeal->getContent();
        $expression['name'] = $appeal->getName();
        $expression['identify'] = $appeal->getIdentify();
        $expression['type'] = $appeal->getType();
        $expression['contact'] = $appeal->getContact();
        $expression['accept_status'] = $appeal->getAcceptStatus();
        $expression['images'] = $appeal->getImages();
        $expression['certificates'] = json_encode($appeal->getCertificates());
        $expression['member_id'] = $appeal->getMember()->getId();
        $expression['reply_id'] = $appeal->getReply()->getId();
        $expression['accept_usergroup_id'] = $appeal->getAcceptUserGroup()->getId();

        $expression['status'] = $appeal->getStatus();
        $expression['status_time'] = $appeal->getStatusTime();
        $expression['create_time'] = $appeal->getCreateTime();
        $expression['update_time'] = $appeal->getUpdateTime();

        $appeal = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\Appeal', $appeal);
        $this->compareArrayAndObjectAppeal($expression, $appeal);
    }

    public function testObjectToArray()
    {
        $appeal = AppealMockFactory::generateAppeal(1);

        $expression = $this->translator->objectToArray($appeal);

        $this->compareArrayAndObjectAppeal($expression, $appeal);
    }
}
