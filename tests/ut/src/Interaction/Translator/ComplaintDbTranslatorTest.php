<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\ComplaintUtils;
use Base\Interaction\Utils\ComplaintMockFactory;

class ComplaintDbTranslatorTest extends TestCase
{
    use ComplaintUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ComplaintDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Interaction\Model\NullComplaint', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $complaint = ComplaintMockFactory::generateComplaint(1);

        $expression['complaint_id'] = $complaint->getId();
        $expression['title'] = $complaint->getTitle();
        $expression['content'] = $complaint->getContent();
        $expression['name'] = $complaint->getName();
        $expression['identify'] = $complaint->getIdentify();
        $expression['type'] = $complaint->getType();
        $expression['contact'] = $complaint->getContact();
        $expression['accept_status'] = $complaint->getAcceptStatus();
        $expression['images'] = $complaint->getImages();
        $expression['subject'] = $complaint->getSubject();
        $expression['member_id'] = $complaint->getMember()->getId();
        $expression['reply_id'] = $complaint->getReply()->getId();
        $expression['accept_usergroup_id'] = $complaint->getAcceptUserGroup()->getId();

        $expression['status'] = $complaint->getStatus();
        $expression['status_time'] = $complaint->getStatusTime();
        $expression['create_time'] = $complaint->getCreateTime();
        $expression['update_time'] = $complaint->getUpdateTime();

        $complaint = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\Complaint', $complaint);
        $this->compareArrayAndObjectComplaint($expression, $complaint);
    }

    public function testObjectToArray()
    {
        $complaint = ComplaintMockFactory::generateComplaint(1);

        $expression = $this->translator->objectToArray($complaint);

        $this->compareArrayAndObjectComplaint($expression, $complaint);
    }
}
