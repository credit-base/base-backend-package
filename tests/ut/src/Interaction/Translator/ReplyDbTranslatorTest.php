<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\ReplyUtils;
use Base\Interaction\Utils\ReplyMockFactory;

class ReplyDbTranslatorTest extends TestCase
{
    use ReplyUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ReplyDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Interaction\Model\NullReply', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $reply = ReplyMockFactory::generateReply(1);

        $expression['reply_id'] = $reply->getId();
        $expression['content'] = $reply->getContent();
        $expression['images'] = $reply->getImages();
        $expression['admissibility'] = $reply->getAdmissibility();
        $expression['crew_id'] = $reply->getCrew()->getId();
        $expression['accept_usergroup_id'] = $reply->getAcceptUserGroup()->getId();
        $expression['status'] = $reply->getStatus();
        $expression['status_time'] = $reply->getStatusTime();
        $expression['create_time'] = $reply->getCreateTime();
        $expression['update_time'] = $reply->getUpdateTime();

        $reply = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\Reply', $reply);
        $this->compareArrayAndObject($expression, $reply);
    }

    public function testArrayToObjectImages()
    {
        $reply = ReplyMockFactory::generateReply(1);

        $expression['reply_id'] = $reply->getId();
        $expression['content'] = $reply->getContent();
        $expression['images'] = json_encode($reply->getImages());
        $expression['admissibility'] = $reply->getAdmissibility();
        $expression['crew_id'] = $reply->getCrew()->getId();
        $expression['accept_usergroup_id'] = $reply->getAcceptUserGroup()->getId();
        $expression['status'] = $reply->getStatus();
        $expression['status_time'] = $reply->getStatusTime();
        $expression['create_time'] = $reply->getCreateTime();
        $expression['update_time'] = $reply->getUpdateTime();

        $reply = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\Reply', $reply);
        $this->compareArrayAndObject($expression, $reply);
    }

    public function testObjectToArray()
    {
        $reply = ReplyMockFactory::generateReply(1);

        $expression = $this->translator->objectToArray($reply);

        $this->compareArrayAndObject($expression, $reply);
    }
}
