<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\PraiseUtils;
use Base\Interaction\Utils\PraiseMockFactory;

class PraiseDbTranslatorTest extends TestCase
{
    use PraiseUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new PraiseDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('Base\Interaction\Model\NullPraise', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $praise = PraiseMockFactory::generatePraise(1);

        $expression['praise_id'] = $praise->getId();
        $expression['title'] = $praise->getTitle();
        $expression['content'] = $praise->getContent();
        $expression['name'] = $praise->getName();
        $expression['identify'] = $praise->getIdentify();
        $expression['type'] = $praise->getType();
        $expression['contact'] = $praise->getContact();
        $expression['accept_status'] = $praise->getAcceptStatus();
        $expression['images'] = $praise->getImages();
        $expression['subject'] = $praise->getSubject();
        $expression['member_id'] = $praise->getMember()->getId();
        $expression['reply_id'] = $praise->getReply()->getId();
        $expression['accept_usergroup_id'] = $praise->getAcceptUserGroup()->getId();

        $expression['status'] = $praise->getStatus();
        $expression['status_time'] = $praise->getStatusTime();
        $expression['create_time'] = $praise->getCreateTime();
        $expression['update_time'] = $praise->getUpdateTime();

        $praise = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\Praise', $praise);
        $this->compareArrayAndObjectPraise($expression, $praise);
    }

    public function testObjectToArray()
    {
        $praise = PraiseMockFactory::generatePraise(1);

        $expression = $this->translator->objectToArray($praise);

        $this->compareArrayAndObjectPraise($expression, $praise);
    }
}
