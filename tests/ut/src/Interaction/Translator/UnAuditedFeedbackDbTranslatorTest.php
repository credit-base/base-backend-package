<?php
namespace Base\Interaction\Translator;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Utils\FeedbackUtils;

class UnAuditedFeedbackDbTranslatorTest extends TestCase
{
    use FeedbackUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditedFeedbackDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testExtendsApplyFormDbTranslator()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Translator\ApplyFormDbTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback(1);

        $expression['apply_form_id'] = $unAuditedFeedback->getId();
        $expression['feedback_id'] = $unAuditedFeedback->getId();

        $expression['title'] = $unAuditedFeedback->getApplyTitle();
        $expression['relation_id'] = $unAuditedFeedback->getRelation()->getId();
        $expression['operation_type'] = $unAuditedFeedback->getOperationType();
        $expression['apply_info_category'] = $unAuditedFeedback->getApplyInfoCategory()->getCategory();
        $expression['apply_info_type'] = $unAuditedFeedback->getApplyInfoCategory()->getType();
        $expression['reject_reason'] = $unAuditedFeedback->getRejectReason();
        $expression['apply_info'] = base64_encode(gzcompress(serialize($unAuditedFeedback->getApplyInfo())));
        $expression['apply_crew_id'] = $unAuditedFeedback->getApplyCrew()->getId();
        $expression['apply_usergroup_id'] = $unAuditedFeedback->getApplyUserGroup()->getId();
        $expression['apply_status'] = $unAuditedFeedback->getApplyStatus();
        $expression['status_time'] = $unAuditedFeedback->getStatusTime();
        $expression['create_time'] = $unAuditedFeedback->getCreateTime();
        $expression['update_time'] = $unAuditedFeedback->getUpdateTime();

        $unAuditedFeedback = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Interaction\Model\UnAuditedFeedback', $unAuditedFeedback);
        $this->compareArrayAndObjectUnAuditedFeedback($expression, $unAuditedFeedback);
    }

    public function testArrayToObjects()
    {
        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback(1);

        $expression['feedback_id'] = $unAuditedFeedback->getApplyId();

        $expression['title'] = $unAuditedFeedback->getTitle();
        $expression['content'] = $unAuditedFeedback->getContent();
        $expression['accept_status'] = $unAuditedFeedback->getAcceptStatus();
        $expression['member_id'] = $unAuditedFeedback->getMember()->getId();
        $expression['reply_id'] = $unAuditedFeedback->getReply()->getId();
        $expression['accept_usergroup_id'] = $unAuditedFeedback->getAcceptUserGroup()->getId();
        $expression['status'] = $unAuditedFeedback->getStatus();
        $expression['status_time'] = $unAuditedFeedback->getStatusTime();
        $expression['create_time'] = $unAuditedFeedback->getCreateTime();
        $expression['update_time'] = $unAuditedFeedback->getUpdateTime();

        $unAuditedFeedback = $this->translator->arrayToObjects($expression, $unAuditedFeedback);

        $this->assertInstanceof('Base\Interaction\Model\UnAuditedFeedback', $unAuditedFeedback);
        $this->compareArrayAndObjectsUnAuditedFeedback($expression, $unAuditedFeedback);
    }

    public function testObjectToArray()
    {
        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback(1);

        $expression = $this->translator->objectToArray($unAuditedFeedback);

        $this->compareArrayAndObjectUnAuditedFeedback($expression, $unAuditedFeedback);
    }
}
