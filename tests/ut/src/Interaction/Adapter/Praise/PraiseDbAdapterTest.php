<?php
namespace Base\Interaction\Adapter\Praise;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Praise;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class PraiseDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(PraiseDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchMember',
                                'fetchAcceptUserGroup',
                                'fetchReply',
                                'interactionFormatFilter',
                                'interactionFormatSort'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IPraiseAdapter
     */
    public function testImplementsIPraiseAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Praise\IPraiseAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 PraiseDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockPraiseDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Translator\PraiseDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 PraiseRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockPraiseDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Praise\Query\PraiseRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockPraiseDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedPraise = new Praise();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($expectedPraise)->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedPraise);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedPraise = new Praise();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'editAction'
        )->with($expectedPraise, $keys)->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedPraise, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedPraise = new Praise();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))->method('fetchOneAction')->with($id)->willReturn($expectedPraise);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedPraise);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedPraise);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedPraise);
        //验证
        $praise = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedPraise, $praise);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $praiseOne = new Praise(1);
        $praiseTwo = new Praise(2);

        $ids = [1, 2];

        $expectedPraiseList = [];
        $expectedPraiseList[$praiseOne->getId()] = $praiseOne;
        $expectedPraiseList[$praiseTwo->getId()] = $praiseTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'fetchListAction'
        )->with($ids)->willReturn($expectedPraiseList);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedPraiseList);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedPraiseList);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedPraiseList);
        //验证
        $praiseList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedPraiseList, $praiseList);
    }

    public function testFormatFilter()
    {
        $adapter = $this->getMockBuilder(MockPraiseDbAdapter::class)
                        ->setMethods(['interactionFormatFilter'])
                        ->getMock();

        $filter = array();
        $praise = new Praise();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatFilter')
                        ->with($filter, $praise)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = $this->getMockBuilder(MockPraiseDbAdapter::class)
                        ->setMethods(['interactionFormatSort'])
                        ->getMock();

        $sort = array();
        $praise = new Praise();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatSort')
                        ->with($sort, $praise)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
