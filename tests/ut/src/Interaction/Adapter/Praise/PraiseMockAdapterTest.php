<?php
namespace Base\Interaction\Adapter\Praise;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Praise;

class PraiseMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new PraiseMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new Praise()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new Praise(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Model\Praise',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\Praise',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\Praise',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
