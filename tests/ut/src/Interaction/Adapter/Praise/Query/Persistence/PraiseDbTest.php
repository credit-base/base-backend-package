<?php
namespace Base\Interaction\Adapter\Praise\Query\Persistence;

use PHPUnit\Framework\TestCase;

class PraiseDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockPraiseDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(PraiseDb::TABLE, $this->database->getTable());
    }
}
