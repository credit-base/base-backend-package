<?php
namespace Base\Interaction\Adapter\Praise;

use PHPUnit\Framework\TestCase;

class UnAuditedPraiseDbAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new UnAuditedPraiseDbAdapter();
        $this->assertInstanceof('Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter', $adapter);
    }
}
