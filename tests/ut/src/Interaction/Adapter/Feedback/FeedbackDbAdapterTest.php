<?php
namespace Base\Interaction\Adapter\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Feedback;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class FeedbackDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(FeedbackDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchMember',
                                'fetchAcceptUserGroup',
                                'fetchReply',
                                'interactionFormatFilter',
                                'interactionFormatSort'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IFeedbackAdapter
     */
    public function testImplementsIFeedbackAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Feedback\IFeedbackAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 FeedbackDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockFeedbackDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Translator\FeedbackDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 FeedbackRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockFeedbackDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Feedback\Query\FeedbackRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockFeedbackDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedFeedback = new Feedback();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($expectedFeedback)->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedFeedback);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedFeedback = new Feedback();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'editAction'
        )->with($expectedFeedback, $keys)->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedFeedback, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedFeedback = new Feedback();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))->method('fetchOneAction')->with($id)->willReturn($expectedFeedback);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedFeedback);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedFeedback);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedFeedback);
        //验证
        $feedback = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedFeedback, $feedback);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $feedbackOne = new Feedback(1);
        $feedbackTwo = new Feedback(2);

        $ids = [1, 2];

        $expectedFeedbackList = [];
        $expectedFeedbackList[$feedbackOne->getId()] = $feedbackOne;
        $expectedFeedbackList[$feedbackTwo->getId()] = $feedbackTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'fetchListAction'
        )->with($ids)->willReturn($expectedFeedbackList);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedFeedbackList);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedFeedbackList);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedFeedbackList);
        //验证
        $feedbackList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedFeedbackList, $feedbackList);
    }

    public function testFormatFilter()
    {
        $adapter = $this->getMockBuilder(MockFeedbackDbAdapter::class)
                        ->setMethods(['interactionFormatFilter'])
                        ->getMock();

        $filter = array();
        $feedback = new Feedback();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatFilter')
                        ->with($filter, $feedback)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = $this->getMockBuilder(MockFeedbackDbAdapter::class)
                        ->setMethods(['interactionFormatSort'])
                        ->getMock();

        $sort = array();
        $feedback = new Feedback();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatSort')
                        ->with($sort, $feedback)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
