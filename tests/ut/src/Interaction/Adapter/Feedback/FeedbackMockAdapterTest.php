<?php
namespace Base\Interaction\Adapter\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Feedback;

class FeedbackMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new FeedbackMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new Feedback()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new Feedback(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Model\Feedback',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\Feedback',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\Feedback',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
