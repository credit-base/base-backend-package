<?php
namespace Base\Interaction\Adapter\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedFeedback;

class UnAuditedFeedbackMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new UnAuditedFeedbackMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new UnAuditedFeedback()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new UnAuditedFeedback(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\IApplyFormAble',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\UnAuditedFeedback',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\UnAuditedFeedback',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
