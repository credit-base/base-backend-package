<?php
namespace Base\Interaction\Adapter\Feedback\Query\Persistence;

use PHPUnit\Framework\TestCase;

class FeedbackCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockFeedbackCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(FeedbackCache::KEY, $this->cache->getKey());
    }
}
