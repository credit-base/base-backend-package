<?php
namespace Base\Interaction\Adapter\Feedback\Query;

use PHPUnit\Framework\TestCase;

class FeedbackRowCacheQueryTest extends TestCase
{
    private $rowCacheQuery;

    public function setUp()
    {
        $this->rowCacheQuery = new MockFeedbackRowCacheQuery();
    }

    public function tearDown()
    {
        unset($this->rowCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowCacheQuery', $this->rowCacheQuery);
    }

    /**
     * 测试是否cache层赋值正确
     */
    public function testCorrectCacheLayer()
    {
        $this->assertInstanceof(
            'Base\Interaction\Adapter\Feedback\Query\Persistence\FeedbackCache',
            $this->rowCacheQuery->getCacheLayer()
        );
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'Base\Interaction\Adapter\Feedback\Query\Persistence\FeedbackDb',
            $this->rowCacheQuery->getDbLayer()
        );
    }
}
