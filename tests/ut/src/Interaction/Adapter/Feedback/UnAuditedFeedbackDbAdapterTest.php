<?php
namespace Base\Interaction\Adapter\Feedback;

use PHPUnit\Framework\TestCase;

class UnAuditedFeedbackDbAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new UnAuditedFeedbackDbAdapter();
        $this->assertInstanceof('Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter', $adapter);
    }
}
