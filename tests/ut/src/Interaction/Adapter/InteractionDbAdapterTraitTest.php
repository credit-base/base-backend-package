<?php
namespace Base\Interaction\Adapter;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\Reply\ReplyRepository;

use Base\Member\Repository\MemberRepository;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 *
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class InteractionDbAdapterTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockInteractionDbAdapterTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetUserGroupRepository()
    {
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $this->trait->publicGetUserGroupRepository()
        );
    }

    public function testGetMemberRepository()
    {
        $this->assertInstanceOf(
            'Base\Member\Repository\MemberRepository',
            $this->trait->publicGetMemberRepository()
        );
    }

    public function testGetReplyRepository()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Reply\ReplyRepository',
            $this->trait->publicGetReplyRepository()
        );
    }

    //fetchAcceptUserGroup
    public function testFetchAcceptUserGroupIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['fetchAcceptUserGroupByObject'])
                        ->getMock();
        
        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(
            $this->faker->randomNumber()
        );

        $adapter->expects($this->exactly(1))->method('fetchAcceptUserGroupByObject')->with($appeal);

        $adapter->publicFetchAcceptUserGroup($appeal);
    }

    public function testFetchAcceptUserGroupIsArray()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['fetchAcceptUserGroupByList'])
                        ->getMock();
                           
        $appealOne = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(
            $this->faker->randomNumber(1)
        );

        $appealTwo = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(
            $this->faker->randomNumber(2)
        );

        $appealList[$appealOne->getId()] = $appealOne;
        $appealList[$appealTwo->getId()] = $appealTwo;

        $adapter->expects($this->exactly(1))->method('fetchAcceptUserGroupByList')->with($appealList);

        $adapter->publicFetchAcceptUserGroup($appealList);
    }

    //fetchAcceptUserGroupByObject
    public function testFetchAcceptUserGroupByObject()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['getUserGroupRepository'])
                        ->getMock();

        $id = $acceptUserGroupId = $this->faker->randomNumber();
        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);
        $acceptUserGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup($acceptUserGroupId);
        $appeal->setAcceptUserGroup($acceptUserGroup);

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(
            Argument::exact($acceptUserGroupId)
        )->shouldBeCalledTimes(1)->willReturn($acceptUserGroup);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->publicFetchAcceptUserGroupByObject($appeal);
    }

    //fetchAcceptUserGroupByList
    public function testFetchAcceptUserGroupByList()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['getUserGroupRepository'])
                        ->getMock();

        $id = $acceptUserGroupId = $this->faker->randomNumber();
        $appealOne = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);
        $acceptUserGroupOne = \Base\UserGroup\Utils\MockFactory::generateUserGroup($acceptUserGroupId);
        $appealOne->setAcceptUserGroup($acceptUserGroupOne);

        $idTwo = $acceptUserGroupIdTwo = $this->faker->randomNumber(2);
        $appealTwo = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($idTwo);
        $acceptUserGroupTwo = \Base\UserGroup\Utils\MockFactory::generateUserGroup($acceptUserGroupIdTwo);
        $appealTwo->setAcceptUserGroup($acceptUserGroupTwo);

        $appealTwo->setAcceptUserGroup($acceptUserGroupTwo);
        $acceptUserGroupIds = [$acceptUserGroupId, $acceptUserGroupIdTwo];
        
        $acceptUserGroupList[$acceptUserGroupOne->getId()] = $acceptUserGroupOne;
        $acceptUserGroupList[$acceptUserGroupTwo->getId()] = $acceptUserGroupTwo;
        
        $appealList[$appealOne->getId()] = $appealOne;
        $appealList[$appealTwo->getId()] = $appealTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(
            Argument::exact($acceptUserGroupIds)
        )->shouldBeCalledTimes(1)->willReturn($acceptUserGroupList);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->publicFetchAcceptUserGroupByList($appealList);
    }

    //fetchMember
    public function testFetchMemberIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['fetchMemberByObject'])
                        ->getMock();
        
        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(
            $this->faker->randomNumber()
        );

        $adapter->expects($this->exactly(1))->method('fetchMemberByObject')->with($appeal);

        $adapter->publicFetchMember($appeal);
    }

    public function testFetchMemberIsArray()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['fetchMemberByList'])
                        ->getMock();
                           
        $appealOne = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(
            $this->faker->randomNumber(1)
        );

        $appealTwo = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(
            $this->faker->randomNumber(2)
        );

        $appealList[$appealOne->getId()] = $appealOne;
        $appealList[$appealTwo->getId()] = $appealTwo;

        $adapter->expects($this->exactly(1))->method('fetchMemberByList')->with($appealList);

        $adapter->publicFetchMember($appealList);
    }

    //fetchMemberByObject
    public function testFetchMemberByObject()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['getMemberRepository'])
                        ->getMock();

        $id = $memberId = $this->faker->randomNumber();
        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);
        $member = \Base\Member\Utils\MockFactory::generateMember($memberId);
        $appeal->setMember($member);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($memberId))->shouldBeCalledTimes(1)->willReturn($member);

        $adapter->expects($this->exactly(1))
                         ->method('getMemberRepository')
                         ->willReturn($repository->reveal());

        $adapter->publicFetchMemberByObject($appeal);
    }

    //fetchMemberByList
    public function testFetchMemberByList()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['getMemberRepository'])
                        ->getMock();

        $id = $memberId = $this->faker->randomNumber();
        $appealOne = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);
        $memberOne = \Base\Member\Utils\MockFactory::generateMember($memberId);
        $appealOne->setMember($memberOne);

        $idTwo = $memberIdTwo = $this->faker->randomNumber(2);
        $appealTwo = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($idTwo);
        $memberTwo = \Base\Member\Utils\MockFactory::generateMember($memberIdTwo);
        $appealTwo->setMember($memberTwo);

        $appealTwo->setMember($memberTwo);
        $memberIds = [$memberId, $memberIdTwo];
        
        $memberList[$memberOne->getId()] = $memberOne;
        $memberList[$memberTwo->getId()] = $memberTwo;
        
        $appealList[$appealOne->getId()] = $appealOne;
        $appealList[$appealTwo->getId()] = $appealTwo;

        $memberRepository = $this->prophesize(MemberRepository::class);
        $memberRepository->fetchList(Argument::exact($memberIds))->shouldBeCalledTimes(1)->willReturn($memberList);

        $adapter->expects($this->exactly(1))->method('getMemberRepository')->willReturn($memberRepository->reveal());

        $adapter->publicFetchMemberByList($appealList);
    }

    //fetchReply
    public function testFetchReplyIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['fetchReplyByObject'])
                        ->getMock();
        
        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(
            $this->faker->randomNumber()
        );

        $adapter->expects($this->exactly(1))->method('fetchReplyByObject')->with($appeal);

        $adapter->publicFetchReply($appeal);
    }

    public function testFetchReplyIsArray()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['fetchReplyByList'])
                        ->getMock();
                           
        $appealOne = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(
            $this->faker->randomNumber(1)
        );

        $appealTwo = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(
            $this->faker->randomNumber(2)
        );

        $appealList[$appealOne->getId()] = $appealOne;
        $appealList[$appealTwo->getId()] = $appealTwo;

        $adapter->expects($this->exactly(1))->method('fetchReplyByList')->with($appealList);

        $adapter->publicFetchReply($appealList);
    }

    //fetchReplyByObject
    public function testFetchReplyByObject()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['getReplyRepository'])
                        ->getMock();

        $id = $replyId = $this->faker->randomNumber();
        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);
        $reply = \Base\Interaction\Utils\ReplyMockFactory::generateReply($replyId);
        $appeal->setReply($reply);

        $repository = $this->prophesize(ReplyRepository::class);
        $repository->fetchOne(Argument::exact($replyId))->shouldBeCalledTimes(1)->willReturn($reply);

        $adapter->expects($this->exactly(1))->method('getReplyRepository')->willReturn($repository->reveal());

        $adapter->publicFetchReplyByObject($appeal);
    }

    //fetchReplyByList
    public function testFetchReplyByList()
    {
        $adapter = $this->getMockBuilder(MockInteractionDbAdapterTrait::class)
                        ->setMethods(['getReplyRepository'])
                        ->getMock();

        $id = $replyId = $this->faker->randomNumber();
        $appealOne = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);
        $replyOne = \Base\Interaction\Utils\ReplyMockFactory::generateReply($replyId);
        $appealOne->setReply($replyOne);

        $idTwo = $replyIdTwo = $this->faker->randomNumber(2);
        $appealTwo = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($idTwo);
        $replyTwo = \Base\Interaction\Utils\ReplyMockFactory::generateReply($replyIdTwo);
        $appealTwo->setReply($replyTwo);

        $appealTwo->setReply($replyTwo);
        $replyIds = [$replyId, $replyIdTwo];
        
        $replyList[$replyOne->getId()] = $replyOne;
        $replyList[$replyTwo->getId()] = $replyTwo;
        
        $appealList[$appealOne->getId()] = $appealOne;
        $appealList[$appealTwo->getId()] = $appealTwo;

        $repository = $this->prophesize(ReplyRepository::class);
        $repository->fetchList(Argument::exact($replyIds))->shouldBeCalledTimes(1)->willReturn($replyList);

        $adapter->expects($this->exactly(1))->method('getReplyRepository')->willReturn($repository->reveal());

        $adapter->publicFetchReplyByList($appealList);
    }
    
    public function testInteractionFormatFilter()
    {
        $adapter = new MockInteractionDbAdapterTrait();

        $filter = array();
        $interaction = new Appeal();

        $expectedCondition = ' 1 ';
        $condition = $adapter->publicInteractionFormatFilter($filter, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/title
    public function testInteractionFormatFilterTitle()
    {
        $filter = array(
            'title' => $this->faker->title()
        );
        $adapter = new MockInteractionDbAdapterTrait();
        $interaction = new Appeal();

        $expectedCondition = 'title LIKE \'%'.$filter['title'].'%\'';
        
        //验证
        $condition = $adapter->publicInteractionFormatFilter($filter, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/Status
    public function testInteractionFormatFilterStatus()
    {
        $filter = array(
            'status' => $this->faker->randomElement(
                IInteractionAble::INTERACTION_STATUS
            )
        );
        $adapter = new MockInteractionDbAdapterTrait();
        $interaction = new Appeal();

        $expectedCondition = 'status IN ('.$filter['status'].')';

        //验证
        $condition = $adapter->publicInteractionFormatFilter($filter, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/acceptStatus
    public function testInteractionFormatFilterAcceptStatus()
    {
        $filter = array(
            'acceptStatus' => $this->faker->randomElement(
                IInteractionAble::ACCEPT_STATUS
            )
        );
        $adapter = new MockInteractionDbAdapterTrait();
        $interaction = new Appeal();

        $expectedCondition = 'accept_status IN ('.$filter['acceptStatus'].')';

        //验证
        $condition = $adapter->publicInteractionFormatFilter($filter, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/acceptUserGroup
    public function testInteractionFormatFilterAcceptUserGroup()
    {
        $filter = array(
            'acceptUserGroup' => $this->faker->randomNumber()
        );
        $adapter = new MockInteractionDbAdapterTrait();
        $interaction = new Appeal();

        $expectedCondition = 'accept_usergroup_id = '.$filter['acceptUserGroup'];

        //验证
        $condition = $adapter->publicInteractionFormatFilter($filter, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/member
    public function testInteractionFormatFilterMember()
    {
        $filter = array(
            'member' => $this->faker->randomNumber()
        );
        $adapter = new MockInteractionDbAdapterTrait();
        $interaction = new Appeal();

        $expectedCondition = 'member_id = '.$filter['member'];

        //验证
        $condition = $adapter->publicInteractionFormatFilter($filter, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-acceptStatus
    public function testFormatSortAcceptStatusDesc()
    {
        $sort = array('acceptStatus' => -1);
        $adapter = new MockInteractionDbAdapterTrait();

        $expectedCondition = ' ORDER BY accept_status DESC';
        $interaction = new Appeal();

        //验证
        $condition = $adapter->publicInteractionFormatSort($sort, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-acceptStatus
    public function testFormatSortAcceptStatusAsc()
    {
        $sort = array('acceptStatus' => 1);
        $adapter = new MockInteractionDbAdapterTrait();

        $expectedCondition = ' ORDER BY accept_status ASC';
        $interaction = new Appeal();

        //验证
        $condition = $adapter->publicInteractionFormatSort($sort, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockInteractionDbAdapterTrait();
        $interaction = new Appeal();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->publicInteractionFormatSort($sort, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockInteractionDbAdapterTrait();
        $interaction = new Appeal();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->publicInteractionFormatSort($sort, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-status
    public function testFormatSortStatusDesc()
    {
        $sort = array('status' => -1);
        $adapter = new MockInteractionDbAdapterTrait();

        $expectedCondition = ' ORDER BY status DESC';
        $interaction = new Appeal();

        //验证
        $condition = $adapter->publicInteractionFormatSort($sort, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-status
    public function testFormatSortStatusAsc()
    {
        $sort = array('status' => 1);
        $adapter = new MockInteractionDbAdapterTrait();

        $expectedCondition = ' ORDER BY status ASC';
        $interaction = new Appeal();

        //验证
        $condition = $adapter->publicInteractionFormatSort($sort, $interaction);
        $this->assertEquals($expectedCondition, $condition);
    }
}
