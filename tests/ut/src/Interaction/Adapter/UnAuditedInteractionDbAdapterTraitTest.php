<?php
namespace Base\Interaction\Adapter;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Repository\Reply\ReplyRepository;

class UnAuditedInteractionDbAdapterTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockUnAuditedInteractionDbAdapterTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    //fetchOne
    public function testFetchOne()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedInteractionDbAdapterTrait::class)
                           ->setMethods(
                               [
                                'fetchOneAction',
                                'fetchRelation',
                                'fetchApplyCrew',
                                'fetchApplyUserGroup',
                                'fetchReply'
                                ]
                           )
                           ->getMock();
        //初始化
        $expectedInteraction = new UnAuditedAppeal();
        $id = 1;

        //绑定
        $adapter->expects($this->exactly(1))->method('fetchOneAction')->with($id)->willReturn($expectedInteraction);
        $adapter->expects($this->exactly(1))->method('fetchRelation')->with($expectedInteraction);
        $adapter->expects($this->exactly(1))->method('fetchApplyCrew')->with($expectedInteraction);
        $adapter->expects($this->exactly(1))->method('fetchApplyUserGroup')->with($expectedInteraction);
        $adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedInteraction);

        //验证
        $interaction = $adapter->fetchOne($id);
        $this->assertEquals($expectedInteraction, $interaction);
    }

    //fetchList
    public function testFetchList()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedInteractionDbAdapterTrait::class)
                           ->setMethods(
                               [
                                'fetchListAction',
                                'fetchRelation',
                                'fetchApplyCrew',
                                'fetchApplyUserGroup',
                                'fetchReply'
                                ]
                           )
                           ->getMock();
        //初始化
        $interactionOne = new UnAuditedAppeal(1);
        $interactionTwo = new UnAuditedAppeal(2);

        $ids = [1, 2];

        $expectedInteractionList = [];
        $expectedInteractionList[$interactionOne->getId()] = $interactionOne;
        $expectedInteractionList[$interactionTwo->getId()] = $interactionTwo;

        //绑定
        $adapter->expects($this->exactly(1))->method(
            'fetchListAction'
        )->with($ids)->willReturn($expectedInteractionList);
        $adapter->expects($this->exactly(1))->method('fetchRelation')->with($expectedInteractionList);
        $adapter->expects($this->exactly(1))->method('fetchApplyCrew')->with($expectedInteractionList);
        $adapter->expects($this->exactly(1))->method('fetchApplyUserGroup')->with($expectedInteractionList);
        $adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedInteractionList);
        //验证
        $interactionList = $adapter->fetchList($ids);
        $this->assertEquals($expectedInteractionList, $interactionList);
    }

    public function testGetReplyRepository()
    {
        $trait = new MockUnAuditedInteractionDbAdapterTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Reply\ReplyRepository',
            $trait->publicGetReplyRepository()
        );
    }

    //fetchReply
    public function testFetchReplyIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedInteractionDbAdapterTrait::class)
                        ->setMethods(['fetchReplyByObject'])
                        ->getMock();
        
        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(
            $this->faker->randomNumber()
        );

        $adapter->expects($this->exactly(1))->method('fetchReplyByObject')->with($unAuditedAppeal);

        $adapter->publicFetchReply($unAuditedAppeal);
    }

    public function testFetchReplyIsArray()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedInteractionDbAdapterTrait::class)
                        ->setMethods(['fetchReplyByList'])
                        ->getMock();
                           
        $unAuditedAppealOne = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(
            $this->faker->randomNumber(1)
        );

        $unAuditedAppealTwo = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(
            $this->faker->randomNumber(2)
        );

        $unAuditedAppealList[$unAuditedAppealOne->getId()] = $unAuditedAppealOne;
        $unAuditedAppealList[$unAuditedAppealTwo->getId()] = $unAuditedAppealTwo;

        $adapter->expects($this->exactly(1))->method('fetchReplyByList')->with($unAuditedAppealList);

        $adapter->publicFetchReply($unAuditedAppealList);
    }

    //fetchReplyByObject
    public function testFetchReplyByObject()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedInteractionDbAdapterTrait::class)
                        ->setMethods(['getReplyRepository'])
                        ->getMock();

        $id = $replyId = $this->faker->randomNumber();
        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal($id);
        $reply = \Base\Interaction\Utils\ReplyMockFactory::generateReply($replyId);
        $unAuditedAppeal->setReply($reply);

        $repository = $this->prophesize(ReplyRepository::class);
        $repository->fetchOne(Argument::exact($replyId))->shouldBeCalledTimes(1)->willReturn($reply);

        $adapter->expects($this->exactly(1))->method('getReplyRepository')->willReturn($repository->reveal());

        $adapter->publicFetchReplyByObject($unAuditedAppeal);
    }

    //fetchReplyByList
    public function testFetchReplyByList()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedInteractionDbAdapterTrait::class)
                        ->setMethods(['getReplyRepository'])
                        ->getMock();

        $id = $replyId = $this->faker->randomNumber();
        $unAuditedAppealOne = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal($id);
        $replyOne = \Base\Interaction\Utils\ReplyMockFactory::generateReply($replyId);
        $unAuditedAppealOne->setReply($replyOne);

        $idTwo = $replyIdTwo = $this->faker->randomNumber(2);
        $unAuditedAppealTwo = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal($idTwo);
        $replyTwo = \Base\Interaction\Utils\ReplyMockFactory::generateReply($replyIdTwo);
        $unAuditedAppealTwo->setReply($replyTwo);

        $unAuditedAppealTwo->setReply($replyTwo);
        $replyIds = [$replyId, $replyIdTwo];
        
        $replyList[$replyOne->getId()] = $replyOne;
        $replyList[$replyTwo->getId()] = $replyTwo;
        
        $unAuditedAppealList[$unAuditedAppealOne->getApplyId()] = $unAuditedAppealOne;
        $unAuditedAppealList[$unAuditedAppealTwo->getApplyId()] = $unAuditedAppealTwo;

        $repository = $this->prophesize(ReplyRepository::class);
        $repository->fetchList(Argument::exact($replyIds))->shouldBeCalledTimes(1)->willReturn($replyList);

        $adapter->expects($this->exactly(1))->method('getReplyRepository')->willReturn($repository->reveal());

        $adapter->publicFetchReplyByList($unAuditedAppealList);
    }
}
