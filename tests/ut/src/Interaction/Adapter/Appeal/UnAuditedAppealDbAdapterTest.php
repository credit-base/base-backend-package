<?php
namespace Base\Interaction\Adapter\Appeal;

use PHPUnit\Framework\TestCase;

class UnAuditedAppealDbAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new UnAuditedAppealDbAdapter();
        $this->assertInstanceof('Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter', $adapter);
    }
}
