<?php
namespace Base\Interaction\Adapter\Appeal;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Appeal;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class AppealDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(AppealDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchMember',
                                'fetchAcceptUserGroup',
                                'fetchReply',
                                'interactionFormatFilter',
                                'interactionFormatSort'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IAppealAdapter
     */
    public function testImplementsIAppealAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\IAppealAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 AppealDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockAppealDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Translator\AppealDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 AppealRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockAppealDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\Query\AppealRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockAppealDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedAppeal = new Appeal();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($expectedAppeal)->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedAppeal);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedAppeal = new Appeal();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'editAction'
        )->with($expectedAppeal, $keys)->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedAppeal, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedAppeal = new Appeal();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))->method('fetchOneAction')->with($id)->willReturn($expectedAppeal);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedAppeal);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedAppeal);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedAppeal);
        //验证
        $appeal = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedAppeal, $appeal);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $appealOne = new Appeal(1);
        $appealTwo = new Appeal(2);

        $ids = [1, 2];

        $expectedList = [];
        $expectedList[$appealOne->getId()] = $appealOne;
        $expectedList[$appealTwo->getId()] = $appealTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))->method('fetchListAction')->with($ids)->willReturn($expectedList);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedList);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedList);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedList);
        //验证
        $appealList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedList, $appealList);
    }

    public function testFormatFilter()
    {
        $adapter = $this->getMockBuilder(MockAppealDbAdapter::class)
                        ->setMethods(['interactionFormatFilter'])
                        ->getMock();

        $filter = array();
        $appeal = new Appeal();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatFilter')
                        ->with($filter, $appeal)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = $this->getMockBuilder(MockAppealDbAdapter::class)
                        ->setMethods(['interactionFormatSort'])
                        ->getMock();


        $sort = array();
        $appeal = new Appeal();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatSort')
                        ->with($sort, $appeal)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
