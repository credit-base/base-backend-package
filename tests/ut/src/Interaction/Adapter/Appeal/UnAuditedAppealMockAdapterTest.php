<?php
namespace Base\Interaction\Adapter\Appeal;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedAppeal;

class UnAuditedAppealMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new UnAuditedAppealMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new UnAuditedAppeal()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new UnAuditedAppeal(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\IApplyFormAble',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\UnAuditedAppeal',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\UnAuditedAppeal',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
