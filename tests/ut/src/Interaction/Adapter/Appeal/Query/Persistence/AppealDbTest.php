<?php
namespace Base\Interaction\Adapter\Appeal\Query\Persistence;

use PHPUnit\Framework\TestCase;

class AppealDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockAppealDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(AppealDb::TABLE, $this->database->getTable());
    }
}
