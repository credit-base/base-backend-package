<?php
namespace Base\Interaction\Adapter\Appeal\Query\Persistence;

use PHPUnit\Framework\TestCase;

class AppealCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockAppealCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(AppealCache::KEY, $this->cache->getKey());
    }
}
