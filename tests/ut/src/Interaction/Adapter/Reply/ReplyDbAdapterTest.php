<?php
namespace Base\Interaction\Adapter\Reply;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Reply;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ReplyDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(ReplyDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchCrew',
                                'fetchUserGroup'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IReplyAdapter
     */
    public function testImplementsIReplyAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Reply\IReplyAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 ReplyDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockReplyDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Translator\ReplyDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 ReplyRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockReplyDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Reply\Query\ReplyRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetCrewRepository()
    {
        $adapter = new MockReplyDbAdapter();
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $adapter->getCrewRepository()
        );
    }

    public function testGetUserGroupRepository()
    {
        $adapter = new MockReplyDbAdapter();
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $adapter->getUserGroupRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockReplyDbAdapter();
        $this->assertInstanceOf('Marmot\Interfaces\INull', $adapter->getNullObject());
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedReply = new Reply();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($expectedReply)->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedReply);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedReply = new Reply();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'editAction'
        )->with($expectedReply, $keys)->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedReply, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedReply = new Reply();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))->method('fetchOneAction')->with($id)->willReturn($expectedReply);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedReply);
        $this->adapter->expects($this->exactly(1))->method('fetchCrew')->with($expectedReply);
        //验证
        $reply = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedReply, $reply);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $replyOne = new Reply(1);
        $replyTwo = new Reply(2);

        $ids = [1, 2];

        $expectedReplyList = [];
        $expectedReplyList[$replyOne->getId()] = $replyOne;
        $expectedReplyList[$replyTwo->getId()] = $replyTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'fetchListAction'
        )->with($ids)->willReturn($expectedReplyList);
        $this->adapter->expects($this->exactly(1))->method('fetchCrew')->with($expectedReplyList);
        $this->adapter->expects($this->exactly(1))->method('fetchUserGroup')->with($expectedReplyList);
        //验证
        $replyList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedReplyList, $replyList);
    }

    //fetchUserGroup
    public function testFetchUserGroupIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockReplyDbAdapter::class)
                        ->setMethods(['fetchUserGroupByObject'])
                        ->getMock();
        
        $reply = new Reply();

        $adapter->expects($this->exactly(1))->method('fetchUserGroupByObject')->with($reply);

        $adapter->fetchUserGroup($reply);
    }

    public function testFetchUserGroupIsArray()
    {
        $adapter = $this->getMockBuilder(MockReplyDbAdapter::class)
                           ->setMethods(['fetchUserGroupByList'])
                           ->getMock();
        
        $replyOne = new Reply(1);
        $replyTwo = new Reply(2);
        
        $replyList[$replyOne->getId()] = $replyOne;
        $replyList[$replyTwo->getId()] = $replyTwo;

        $adapter->expects($this->exactly(1))->method('fetchUserGroupByList')->with($replyList);

        $adapter->fetchUserGroup($replyList);
    }

    //fetchUserGroupByObject
    public function testFetchUserGroupByObject()
    {
        $adapter = $this->getMockBuilder(MockReplyDbAdapter::class)
                        ->setMethods(['getUserGroupRepository'])
                        ->getMock();

        $reply = new Reply();
        $reply->setAcceptUserGroup(new UserGroup(1));
        
        $userGroupId = 1;

        // 为 UserGroup 类建立预言
        $userGroup  = $this->prophesize(UserGroup::class);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))->shouldBeCalledTimes(1)->willReturn($userGroup);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByObject($reply);
    }

    //fetchUserGroupByList
    public function testFetchUserGroupByList()
    {
        $adapter = $this->getMockBuilder(MockReplyDbAdapter::class)
                        ->setMethods(['getUserGroupRepository'])
                        ->getMock();

        $replyOne = new Reply(1);
        $userGroupOne = new UserGroup(1);
        $replyOne->setAcceptUserGroup($userGroupOne);

        $replyTwo = new Reply(2);
        $userGroupTwo = new UserGroup(2);
        $replyTwo->setAcceptUserGroup($userGroupTwo);
        $userGroupIds = [1, 2];
        
        $userGroupList[$userGroupOne->getId()] = $userGroupOne;
        $userGroupList[$userGroupTwo->getId()] = $userGroupTwo;
        
        $replyList[$replyOne->getId()] = $replyOne;
        $replyList[$replyTwo->getId()] = $replyTwo;

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchList(
            Argument::exact($userGroupIds)
        )->shouldBeCalledTimes(1)->willReturn($userGroupList);

        $adapter->expects($this->exactly(1))->method(
            'getUserGroupRepository'
        )->willReturn($userGroupRepository->reveal());

        $adapter->fetchUserGroupByList($replyList);
    }

    //fetchCrew
    public function testFetchCrewIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockReplyDbAdapter::class)
                        ->setMethods(['fetchCrewByObject'])
                        ->getMock();
        
        $reply = new Reply();

        $adapter->expects($this->exactly(1))->method('fetchCrewByObject')->with($reply);

        $adapter->fetchCrew($reply);
    }

    public function testFetchCrewIsArray()
    {
        $adapter = $this->getMockBuilder(MockReplyDbAdapter::class)
                        ->setMethods(['fetchCrewByList'])
                        ->getMock();
        
        $replyOne = new Reply(1);
        $replyTwo = new Reply(2);
        
        $replyList[$replyOne->getId()] = $replyOne;
        $replyList[$replyTwo->getId()] = $replyTwo;

        $adapter->expects($this->exactly(1))->method('fetchCrewByList')->with($replyList);

        $adapter->fetchCrew($replyList);
    }

    //fetchCrewByObject
    public function testFetchCrewByObject()
    {
        $adapter = $this->getMockBuilder(MockReplyDbAdapter::class)
                        ->setMethods(['getCrewRepository'])
                        ->getMock();

        $reply = new Reply();
        $reply->setCrew(new Crew(1));
        
        $crewId = 1;

        // 为 Crew 类建立预言
        $crew  = $this->prophesize(Crew::class);
        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($crewId))->shouldBeCalledTimes(1)->willReturn($crew);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByObject($reply);
    }

    //fetchCrewByList
    public function testFetchCrewByList()
    {
        $adapter = $this->getMockBuilder(MockReplyDbAdapter::class)
                        ->setMethods(['getCrewRepository'])
                        ->getMock();

        $replyOne = new Reply(1);
        $crewOne = new Crew(1);
        $replyOne->setCrew($crewOne);

        $replyTwo = new Reply(2);
        $crewTwo = new Crew(2);
        $replyTwo->setCrew($crewTwo);
        $crewIds = [1, 2];
        
        $crewList[$crewOne->getId()] = $crewOne;
        $crewList[$crewTwo->getId()] = $crewTwo;
        
        $replyList[$replyOne->getId()] = $replyOne;
        $replyList[$replyTwo->getId()] = $replyTwo;

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchList(Argument::exact($crewIds))->shouldBeCalledTimes(1)->willReturn($crewList);

        $adapter->expects($this->exactly(1))->method('getCrewRepository')->willReturn($crewRepository->reveal());

        $adapter->fetchCrewByList($replyList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockReplyDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockReplyDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }
}
