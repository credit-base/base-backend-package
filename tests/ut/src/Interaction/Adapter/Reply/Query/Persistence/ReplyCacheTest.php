<?php
namespace Base\Interaction\Adapter\Reply\Query\Persistence;

use PHPUnit\Framework\TestCase;

class ReplyCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockReplyCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(ReplyCache::KEY, $this->cache->getKey());
    }
}
