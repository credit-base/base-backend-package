<?php
namespace Base\Interaction\Adapter\Reply;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Reply;

class ReplyMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new ReplyMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new Reply()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new Reply(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Model\Reply',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\Reply',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\Reply',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
