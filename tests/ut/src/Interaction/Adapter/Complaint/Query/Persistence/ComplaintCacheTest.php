<?php
namespace Base\Interaction\Adapter\Complaint\Query\Persistence;

use PHPUnit\Framework\TestCase;

class ComplaintCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockComplaintCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(ComplaintCache::KEY, $this->cache->getKey());
    }
}
