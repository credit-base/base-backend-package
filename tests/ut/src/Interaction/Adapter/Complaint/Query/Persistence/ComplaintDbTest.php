<?php
namespace Base\Interaction\Adapter\Complaint\Query\Persistence;

use PHPUnit\Framework\TestCase;

class ComplaintDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockComplaintDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(ComplaintDb::TABLE, $this->database->getTable());
    }
}
