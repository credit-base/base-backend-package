<?php
namespace Base\Interaction\Adapter\Complaint;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Complaint;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ComplaintDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(ComplaintDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchMember',
                                'fetchAcceptUserGroup',
                                'fetchReply',
                                'interactionFormatFilter',
                                'interactionFormatSort'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IComplaintAdapter
     */
    public function testImplementsIComplaintAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\IComplaintAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 ComplaintDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockComplaintDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Translator\ComplaintDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 ComplaintRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockComplaintDbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\Query\ComplaintRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockComplaintDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedComplaint = new Complaint();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($expectedComplaint)->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedComplaint);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedComplaint = new Complaint();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'editAction'
        )->with($expectedComplaint, $keys)->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedComplaint, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedComplaint = new Complaint();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'fetchOneAction'
        )->with($id)->willReturn($expectedComplaint);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedComplaint);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedComplaint);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedComplaint);
        //验证
        $complaint = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedComplaint, $complaint);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $complaintOne = new Complaint(1);
        $complaintTwo = new Complaint(2);

        $ids = [1, 2];

        $expectedComplaintList = [];
        $expectedComplaintList[$complaintOne->getId()] = $complaintOne;
        $expectedComplaintList[$complaintTwo->getId()] = $complaintTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))->method(
            'fetchListAction'
        )->with($ids)->willReturn($expectedComplaintList);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedComplaintList);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedComplaintList);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedComplaintList);
        //验证
        $complaintList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedComplaintList, $complaintList);
    }

    public function testFormatFilter()
    {
        $adapter = $this->getMockBuilder(MockComplaintDbAdapter::class)
                        ->setMethods(['interactionFormatFilter'])
                        ->getMock();

        $filter = array();
        $complaint = new Complaint();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatFilter')
                        ->with($filter, $complaint)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = $this->getMockBuilder(MockComplaintDbAdapter::class)
                        ->setMethods(['interactionFormatSort'])
                        ->getMock();

        $sort = array();
        $complaint = new Complaint();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatSort')
                        ->with($sort, $complaint)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
