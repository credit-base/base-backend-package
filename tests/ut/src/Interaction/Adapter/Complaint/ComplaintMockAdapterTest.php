<?php
namespace Base\Interaction\Adapter\Complaint;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Complaint;

class ComplaintMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new ComplaintMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new Complaint()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new Complaint(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Model\Complaint',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\Complaint',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\Complaint',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
