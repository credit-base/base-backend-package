<?php
namespace Base\Interaction\Adapter\Complaint;

use PHPUnit\Framework\TestCase;

class UnAuditedComplaintDbAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new UnAuditedComplaintDbAdapter();
        $this->assertInstanceof('Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter', $adapter);
    }
}
