<?php
namespace Base\Interaction\Adapter\QA;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedQA;

class UnAuditedQAMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new UnAuditedQAMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new UnAuditedQA()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new UnAuditedQA(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Model\IApplyFormAble',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\UnAuditedQA',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\UnAuditedQA',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
