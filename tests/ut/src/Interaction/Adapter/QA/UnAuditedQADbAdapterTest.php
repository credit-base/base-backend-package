<?php
namespace Base\Interaction\Adapter\QA;

use PHPUnit\Framework\TestCase;

class UnAuditedQADbAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new UnAuditedQADbAdapter();
        $this->assertInstanceof('Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter', $adapter);
    }
}
