<?php
namespace Base\Interaction\Adapter\QA;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\QA;

class QAMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new QAMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new QA()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new QA(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Model\QA',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\QA',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Interaction\Model\QA',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
