<?php
namespace Base\Interaction\Adapter\QA\Query\Persistence;

use PHPUnit\Framework\TestCase;

class QACacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockQACache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(QACache::KEY, $this->cache->getKey());
    }
}
