<?php
namespace Base\Interaction\Adapter\QA;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\QA;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class QADbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(QADbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchMember',
                                'fetchAcceptUserGroup',
                                'fetchReply',
                                'interactionFormatFilter',
                                'interactionFormatSort'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IQAAdapter
     */
    public function testImplementsIQAAdapter()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\QA\IQAAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 QADbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockQADbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Translator\QADbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 QARowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockQADbAdapter();
        $this->assertInstanceOf(
            'Base\Interaction\Adapter\QA\Query\QARowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockQADbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedQA = new QA();

        //绑定
        $this->adapter->expects($this->exactly(1))->method('addAction')->with($expectedQA)->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedQA);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedQA = new QA();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))->method('editAction')->with($expectedQA, $keys)->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedQA, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedQA = new QA();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))->method('fetchOneAction')->with($id)->willReturn($expectedQA);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedQA);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedQA);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedQA);
        //验证
        $qaInteraction = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedQA, $qaInteraction);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $qaInteractionOne = new QA(1);
        $qaInteractionTwo = new QA(2);

        $ids = [1, 2];

        $expectedQAList = [];
        $expectedQAList[$qaInteractionOne->getId()] = $qaInteractionOne;
        $expectedQAList[$qaInteractionTwo->getId()] = $qaInteractionTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))->method('fetchListAction')->with($ids)->willReturn($expectedQAList);
        $this->adapter->expects($this->exactly(1))->method('fetchMember')->with($expectedQAList);
        $this->adapter->expects($this->exactly(1))->method('fetchAcceptUserGroup')->with($expectedQAList);
        $this->adapter->expects($this->exactly(1))->method('fetchReply')->with($expectedQAList);
        //验证
        $qaInteractionList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedQAList, $qaInteractionList);
    }

    public function testFormatFilter()
    {
        $adapter = $this->getMockBuilder(MockQADbAdapter::class)
                        ->setMethods(['interactionFormatFilter'])
                        ->getMock();

        $filter = array();
        $qaInteraction = new QA();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatFilter')
                        ->with($filter, $qaInteraction)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = $this->getMockBuilder(MockQADbAdapter::class)
                        ->setMethods(['interactionFormatSort'])
                        ->getMock();

        $sort = array();
        $qaInteraction = new QA();
        $expectedCondition = ' 1 ';

        $adapter->expects($this->exactly(1))
                        ->method('interactionFormatSort')
                        ->with($sort, $qaInteraction)
                        ->willReturn($expectedCondition);

        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}
