<?php
namespace Base\Interaction\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Interaction\WidgetRule\InteractionWidgetRule;

class InteractionControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockInteractionControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetInteractionWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Interaction\WidgetRule\InteractionWidgetRule',
            $this->trait->getInteractionWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'Base\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testValidateCommonScenario()
    {
        $controller = $this->getMockBuilder(MockInteractionControllerTrait::class)
                           ->setMethods(['getCommonWidgetRule', 'getInteractionWidgetRule']) ->getMock();

        $title = 'title';
        $content = 'content';
        $member = 1;
        $acceptUserGroup = 1;

        $interactionWidgetRule = $this->prophesize(InteractionWidgetRule::class);
        $interactionWidgetRule->title(Argument::exact($title))->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRule->content(
            Argument::exact($content),
            Argument::exact('content')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($member), Argument::exact('memberId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $commonWidgetRule->formatNumeric(Argument::exact($acceptUserGroup), Argument::exact('acceptUserGroupId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(2))->method(
            'getCommonWidgetRule'
        )->willReturn($commonWidgetRule->reveal());

        $controller->expects($this->exactly(2))->method(
            'getInteractionWidgetRule'
        )->willReturn($interactionWidgetRule->reveal());

        $result = $controller->validateCommonScenarioPublic(
            $title,
            $content,
            $member,
            $acceptUserGroup
        );
        
        $this->assertTrue($result);
    }

    public function testValidateCommentCommonScenario()
    {
        $controller = $this->getMockBuilder(MockInteractionControllerTrait::class)
                           ->setMethods(['getInteractionWidgetRule']) ->getMock();

        $name = 'name';
        $identify = 'identify';
        $type = 1;
        $contact = 'contact';
        $images = array('images');

        $interactionWidgetRule = $this->prophesize(InteractionWidgetRule::class);
        $interactionWidgetRule->name(Argument::exact($name))->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRule->identify(Argument::exact($identify))->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRule->type(Argument::exact($type))->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRule->contact(Argument::exact($contact))->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRule->images(
            Argument::exact($images),
            Argument::exact('images')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(5))
            ->method('getInteractionWidgetRule')
            ->willReturn($interactionWidgetRule->reveal());

        $result = $controller->validateCommentCommonScenarioPublic(
            $name,
            $identify,
            $type,
            $contact,
            $images
        );
        
        $this->assertTrue($result);
    }

    public function testValidateComplaintAddScenario()
    {
        $controller = $this->getMockBuilder(MockInteractionControllerTrait::class)
                           ->setMethods(['getInteractionWidgetRule']) ->getMock();

        $subject = 'subject';

        $interactionWidgetRule = $this->prophesize(InteractionWidgetRule::class);
        $interactionWidgetRule->subject(Argument::exact($subject))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))->method(
            'getInteractionWidgetRule'
        )->willReturn($interactionWidgetRule->reveal());

        $result = $controller->validateComplaintAddScenarioPublic($subject);
        
        $this->assertTrue($result);
    }

    public function testValidatePraiseAddScenario()
    {
        $controller = $this->getMockBuilder(MockInteractionControllerTrait::class)
                           ->setMethods(['getInteractionWidgetRule']) ->getMock();

        $subject = 'subject';

        $interactionWidgetRule = $this->prophesize(InteractionWidgetRule::class);
        $interactionWidgetRule->subject(Argument::exact($subject))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))->method(
            'getInteractionWidgetRule'
        )->willReturn($interactionWidgetRule->reveal());

        $result = $controller->validatePraiseAddScenarioPublic($subject);
        
        $this->assertTrue($result);
    }

    public function testValidateAppealAddScenario()
    {
        $controller = $this->getMockBuilder(MockInteractionControllerTrait::class)
                           ->setMethods(['getInteractionWidgetRule']) ->getMock();

        $certificates = array('certificates');

        $interactionWidgetRule = $this->prophesize(InteractionWidgetRule::class);
        $interactionWidgetRule->images(
            Argument::exact($certificates),
            Argument::exact('certificates')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))->method(
            'getInteractionWidgetRule'
        )->willReturn($interactionWidgetRule->reveal());

        $result = $controller->validateAppealAddScenarioPublic($certificates);
        
        $this->assertTrue($result);
    }

    public function testValidateAcceptScenario()
    {
        $controller = $this->getMockBuilder(MockInteractionControllerTrait::class)
                           ->setMethods(['getCommonWidgetRule', 'getInteractionWidgetRule']) ->getMock();

        $replyContent = 'replyContent';
        $replyImages = array('replyImages');
        $admissibility = 1;
        $crew = 1;

        $interactionWidgetRule = $this->prophesize(InteractionWidgetRule::class);
        $interactionWidgetRule->content(
            Argument::exact($replyContent),
            Argument::exact('replyContent')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRule->images(
            Argument::exact($replyImages),
            Argument::exact('replyImages')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRule->admissibility(
            Argument::exact($admissibility)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($crew), Argument::exact('replyCrew'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(1))->method(
            'getCommonWidgetRule'
        )->willReturn($commonWidgetRule->reveal());

        $controller->expects($this->exactly(3))->method(
            'getInteractionWidgetRule'
        )->willReturn($interactionWidgetRule->reveal());

        $result = $controller->validateAcceptScenarioPublic(
            $replyContent,
            $replyImages,
            $admissibility,
            $crew
        );
        
        $this->assertTrue($result);
    }
}
