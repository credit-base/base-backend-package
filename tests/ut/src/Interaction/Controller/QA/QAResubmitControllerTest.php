<?php
namespace Base\Interaction\Controller\QA;

use PHPUnit\Framework\TestCase;

class QAResubmitControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new QAResubmitController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIResubmitAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IResubmitAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockQAResubmitController::class)->setMethods(['render'])->getMock();

        $unAuditedQA = \Base\Interaction\Utils\QAMockFactory::generateUnAuditedQA(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedQA);
    }

    public function testGetAcceptCommand()
    {
        $controller = new MockQAResubmitController();

        $data = array(
            'replyContent' => 'replyContent',
            'replyImages' => array('replyImages'),
            'admissibility' => 1,
            'crew' => 1,
            'id' => 1
        );

        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $controller->getAcceptCommand($data)
        );
    }
}
