<?php
namespace Base\Interaction\Controller\QA;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedQA;
use Base\Interaction\Repository\QA\UnAuditedQARepository;

class UnAuditedQAFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new UnAuditedQAFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockUnAuditedQAFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Repository\QA\UnAuditedQARepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockUnAuditedQAFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\QA\UnAuditedQAView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockUnAuditedQAFetchController();

        $this->assertEquals(
            'unAuditedQAs',
            $controller->getResourceName()
        );
    }

    private function initialFilter($unAuditedQAArray)
    {
        $this->stub = $this->getMockBuilder(MockUnAuditedQAFetchController::class)
                    ->setMethods(['getRepository', 'renderView', 'displayError', 'formatParameters'])
                    ->getMock();

        $sort = array();
        $curpage = 1;
        $perpage = 20;
        $filter['applyInfoCategory'] = UnAuditedQA::APPLY_QA_CATEGORY;
        $filter['applyInfoType'] = UnAuditedQA::APPLY_QA_TYPE;

        $this->stub->expects($this->any())->method(
            'formatParameters'
        )->willReturn([$filter, $sort, $curpage, $perpage]);

        $repository = $this->prophesize(UnAuditedQARepository::class);

        $repository->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(($curpage-1)*$perpage),
            Argument::exact($perpage)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedQAArray);

        $this->stub->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
    }
 
    public function testFilterSuccess()
    {
        $unAuditedQAArray = array(
            array(\Base\Interaction\Utils\QAMockFactory::generateUnAuditedQA(1)),
            1
        );

        $this->initialFilter($unAuditedQAArray);
        
        $this->stub->expects($this->exactly(1))->method('renderView')->willReturn(true);
        
        $result = $this->stub->filter();
        $this->assertTrue($result);
    }

    public function testFilterFailure()
    {
        $unAuditedQAArray = array(
            array(),
            0
        );

        $this->initialFilter($unAuditedQAArray);
        
        $this->stub->expects($this->exactly(1))->method('displayError')->willReturn(false);
        
        $result = $this->stub->filter();
        $this->assertFalse($result);
    }
}
