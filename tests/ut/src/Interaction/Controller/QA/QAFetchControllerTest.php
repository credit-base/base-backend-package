<?php
namespace Base\Interaction\Controller\QA;

use PHPUnit\Framework\TestCase;

class QAFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new QAFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockQAFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Adapter\QA\IQAAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockQAFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\QA\QAView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockQAFetchController();

        $this->assertEquals(
            'qas',
            $controller->getResourceName()
        );
    }
}
