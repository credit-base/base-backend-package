<?php
namespace Base\Interaction\Controller\QA;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Interaction\Command\QA\AddQACommand;
use Base\Interaction\Command\QA\RevokeQACommand;
use Base\Interaction\Repository\QA\QARepository;
use Base\Interaction\Command\QA\PublishQACommand;
use Base\Interaction\Command\QA\UnPublishQACommand;

/**
 *
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class QAOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new QAOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockQAOperateController::class)->setMethods(['render'])->getMock();

        $unAuditedQA = \Base\Interaction\Utils\QAMockFactory::generateUnAuditedQA(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedQA);
    }

    public function testGetAcceptCommand()
    {
        $controller = new MockQAOperateController();

        $data = array(
            'replyContent' => 'replyContent',
            'replyImages' => array('replyImages'),
            'admissibility' => 1,
            'crew' => 1,
            'id' => 1
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $controller->getAcceptCommand($data));
    }

    private function qaData() : array
    {
        return array(
            "type"=>"qas",
            "attributes"=>array(
                "title"=>"信用问答",
                "content"=>"信用问答"
            ),
            "relationships"=>array(
                "member"=>array(
                    "data"=>array(
                        array("type"=>"members","id"=>3)
                    )
                ),
                "acceptUserGroup"=>array(
                    "data"=>array(
                        array("type"=>"userGroups","id"=>3)
                    )
                )
            )
        );
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 QAWidgetRule 类建立预言, 验证请求参数, getQAWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddQACommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(bool $result)
    {
        $this->controller = $this->getMockBuilder(QAOperateController::class)
                            ->setMethods(
                                [
                                    'getRequest',
                                    'validateCommonScenario',
                                    'getCommandBus',
                                    'getRepository',
                                    'render',
                                    'displayError'
                                ]
                            )->getMock();

        // mock 请求参数
        $data = $this->qaData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $content = $attributes['content'];

        $member = $relationships['member']['data'][0]['id'];
        $acceptUserGroup = $relationships['acceptUserGroup']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);
        $this->controller->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $this->controller->expects($this->exactly(1))->method('validateCommonScenario')->willReturn(true);

        $command = new AddQACommand(
            $title,
            $content,
            $acceptUserGroup,
            $member
        );
        // 为 CommandBus 类建立预言, 传入 AddQACommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $this->controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
            
        return $command;
    }

    /**
     * 测试 add 成功
     * 1. 为 QAOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getQAWidgetRule、getCommandBus、getUnAuditedQARepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 QA 类建立预言
     * 4. 为 UnAuditedQARepository 类建立预言, UnAuditedQARepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的QA, getUnAuditedQARepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 调用 $this->initAdd(), 期望结果为 true
        $command = $this->initAdd(true);

        // 为 QA 类建立预言
        $qaInteraction = \Base\Interaction\Utils\QAMockFactory::generateQA($command->id);

        // 为 UnAuditedQARepository 类建立预言, UnAuditedQARepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的QA, getUnAuditedQARepository 方法被调用一次
        $repository = $this->prophesize(QARepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($qaInteraction);
        $this->controller->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $this->controller->expects($this->exactly(1))->method('render')->willReturn(true);

        // this->controller->add 方法被调用一次, 且返回结果为 true
        $result = $this->controller->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 QAOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getQAWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且this->controller返回结果为 false
     * 4. this->controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd(false);

        // displayError 方法被调用一次, 且this->controller返回结果为 false
        $this->controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        // this->controller->add 方法被调用一次, 且返回结果为 false
        $result = $this->controller->add();
        $this->assertFalse($result);
    }

    public function testEdit()
    {
        $result = $this->controller->edit(1);
        $this->assertFalse($result);
    }

    private function initialRevoke($result)
    {
        $this->qaInteraction = $this->getMockBuilder(QAOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new RevokeQACommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->qaInteraction->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testRevokeSuccess()
    {
        $command = $this->initialRevoke(true);

        $qaInteraction = \Base\Interaction\Utils\QAMockFactory::generateQA($command->id);

        $repository = $this->prophesize(QARepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($qaInteraction);

        $this->qaInteraction->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->qaInteraction->expects($this->exactly(1))->method('render');

        $result = $this->qaInteraction->revoke($command->id);
        $this->assertTrue($result);
    }

    public function testRevokeFailure()
    {
        $command = $this->initialRevoke(false);

        $this->qaInteraction->expects($this->exactly(1))->method('displayError');

        $result = $this->qaInteraction->revoke($command->id);
        $this->assertFalse($result);
    }

    private function initialPublish($result)
    {
        $this->qaInteraction = $this->getMockBuilder(QAOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new PublishQACommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->qaInteraction->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testPublishSuccess()
    {
        $command = $this->initialPublish(true);

        $qaInteraction = \Base\Interaction\Utils\QAMockFactory::generateQA($command->id);

        $repository = $this->prophesize(QARepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($qaInteraction);

        $this->qaInteraction->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->qaInteraction->expects($this->exactly(1))->method('render');

        $result = $this->qaInteraction->publish($command->id);
        $this->assertTrue($result);
    }

    public function testPublishFailure()
    {
        $command = $this->initialPublish(false);

        $this->qaInteraction->expects($this->exactly(1))->method('displayError');

        $result = $this->qaInteraction->publish($command->id);
        $this->assertFalse($result);
    }

    private function initialUnPublish($result)
    {
        $this->qaInteraction = $this->getMockBuilder(QAOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new UnPublishQACommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->qaInteraction->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testUnPublishSuccess()
    {
        $command = $this->initialUnPublish(true);

        $qaInteraction = \Base\Interaction\Utils\QAMockFactory::generateQA($command->id);

        $repository = $this->prophesize(QARepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($qaInteraction);

        $this->qaInteraction->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->qaInteraction->expects($this->exactly(1))->method('render');

        $result = $this->qaInteraction->unPublish($command->id);
        $this->assertTrue($result);
    }

    public function testUnPublishFailure()
    {
        $command = $this->initialUnPublish(false);

        $this->qaInteraction->expects($this->exactly(1))->method('displayError');

        $result = $this->qaInteraction->unPublish($command->id);
        $this->assertFalse($result);
    }
}
