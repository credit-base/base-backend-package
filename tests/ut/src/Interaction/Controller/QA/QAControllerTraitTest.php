<?php
namespace Base\Interaction\Controller\QA;

use PHPUnit\Framework\TestCase;

class QAControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockQAControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Repository\QA\QARepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetUnAuditedInteractionRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->trait->getUnAuditedInteractionRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}
