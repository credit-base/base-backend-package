<?php
namespace Base\Interaction\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Interaction\Command\AcceptCommand;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

class InteractionAcceptControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockInteractionAcceptControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testAccept()
    {
        $controller = $this->getMockBuilder(MockInteractionAcceptControllerTrait::class)
                           ->setMethods(['acceptInteraction'])->getMock();

        $controller->expects($this->exactly(1))->method('acceptInteraction')->willReturn(true);

        $result = $controller->accept(1);
        $this->assertTrue($result);
    }

    public function testResubmit()
    {
        $controller = $this->getMockBuilder(MockInteractionAcceptControllerTrait::class)
                           ->setMethods(['acceptInteraction'])->getMock();

        $controller->expects($this->exactly(1))->method('acceptInteraction')->willReturn(true);

        $result = $controller->resubmit(1);
        $this->assertTrue($result);
    }

    private function interactionData() : array
    {
        return array(
                "type"=>"appeals",
                "relationships"=>array(
                    "reply"=>array(
                        "data"=>array(
                            array(
                                "type"=>"replies",
                                "attributes"=>array(
                                    "content"=>"回复内容",
                                    "images"=>array(
                                        array('name' => '回复图片', 'identify' => 'identify.jpg'),
                                        array('name' => '回复图片', 'identify' => 'identify.jpg'),
                                        array('name' => '回复图片', 'identify' => 'identify.jpg')
                                    ),
                                    "admissibility"=> 1
                                ),
                                "relationships"=>array(
                                    "crew"=>array(
                                        "data"=>array(
                                            array("type"=>"crews","id"=>1)
                                        )
                                    )
                                ),
                            )
                        )
                    )
                )
            );
    }

    protected function initAcceptInteraction(bool $result)
    {
        $this->controller = $this->getMockBuilder(MockInteractionAcceptControllerTrait::class)
                                ->setMethods(
                                    [
                                        'getRequest',
                                        'validateAcceptScenario',
                                        'getAcceptCommand',
                                        'getCommandBus',
                                        'getUnAuditedInteractionRepository',
                                        'displaySuccess',
                                        'displayError'
                                    ]
                                )->getMock();

        // mock 请求参数
        $data = $this->interactionData();

        $relationships = $data['relationships'];
        $replyAttributes = $relationships['reply']['data'][0]['attributes'];
        $replyRelationships = $relationships['reply']['data'][0]['relationships'];

        $acceptData['replyContent'] = $replyAttributes['content'];
        $acceptData['replyImages'] = $replyAttributes['images'];
        $acceptData['admissibility'] = $replyAttributes['admissibility'];
        $acceptData['crew'] = $replyRelationships['crew']['data'][0]['id'];
        $acceptData['id'] = 1;

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);
        $this->controller->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $this->controller->expects($this->exactly(1))->method('validateAcceptScenario')->willReturn(true);

        $command = new AcceptCommand(
            $acceptData['replyContent'],
            $acceptData['replyImages'],
            $acceptData['admissibility'],
            $acceptData['crew'],
            $acceptData['id']
        );

        $this->controller->expects($this->exactly(1))->method('getAcceptCommand')->willReturn($command);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $this->controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
            
        return $command;
    }

    public function testAcceptInteraction()
    {
        $command = $this->initAcceptInteraction(true);

        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal($command->id);

        $repository = $this->prophesize(UnAuditedAppealRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($unAuditedAppeal);
        $this->controller->expects($this->once())->method(
            'getUnAuditedInteractionRepository'
        )->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))->method('displaySuccess')->with($unAuditedAppeal);

        $result = $this->controller->acceptInteractionPublic($command->id);
        $this->assertTrue($result);
    }

    public function testAcceptInteractionFail()
    {
        // 调用 $this->initAcceptInteraction(), 期望结果为 false
        $command = $this->initAcceptInteraction(false);

        // displayError 方法被调用一次, 且this->controller返回结果为 false
        $this->controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        // this->controller->acceptInteraction 方法被调用一次, 且返回结果为 false
        $result = $this->controller->acceptInteractionPublic($command->id);
        $this->assertFalse($result);
    }
}
