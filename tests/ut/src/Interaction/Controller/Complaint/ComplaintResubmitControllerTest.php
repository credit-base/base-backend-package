<?php
namespace Base\Interaction\Controller\Complaint;

use PHPUnit\Framework\TestCase;

class ComplaintResubmitControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new ComplaintResubmitController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIResubmitAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IResubmitAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockComplaintResubmitController::class)->setMethods(['render'])->getMock();

        $unAuditedComplaint = \Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedComplaint);
    }

    public function testGetAcceptCommand()
    {
        $controller = new MockComplaintResubmitController();

        $data = array(
            'replyContent' => 'replyContent',
            'replyImages' => array('replyImages'),
            'admissibility' => 1,
            'crew' => 1,
            'id' => 1
        );

        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $controller->getAcceptCommand($data)
        );
    }
}
