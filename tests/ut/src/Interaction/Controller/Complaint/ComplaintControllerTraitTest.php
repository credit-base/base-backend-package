<?php
namespace Base\Interaction\Controller\Complaint;

use PHPUnit\Framework\TestCase;

class ComplaintControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockComplaintControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Complaint\ComplaintRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetUnAuditedInteractionRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->trait->getUnAuditedInteractionRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}
