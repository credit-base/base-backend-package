<?php
namespace Base\Interaction\Controller\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedComplaint;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

class UnAuditedComplaintFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new UnAuditedComplaintFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockUnAuditedComplaintFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockUnAuditedComplaintFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\Complaint\UnAuditedComplaintView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockUnAuditedComplaintFetchController();

        $this->assertEquals(
            'unAuditedComplaints',
            $controller->getResourceName()
        );
    }

    private function initialFilter($unAuditedComplaintArray)
    {
        $this->stub = $this->getMockBuilder(MockUnAuditedComplaintFetchController::class)
                    ->setMethods(['getRepository', 'renderView', 'displayError', 'formatParameters'])
                    ->getMock();

        $sort = array();
        $curpage = 1;
        $perpage = 20;
        $filter['applyInfoCategory'] = UnAuditedComplaint::APPLY_COMPLAINT_CATEGORY;
        $filter['applyInfoType'] = UnAuditedComplaint::APPLY_COMPLAINT_TYPE;

        $this->stub->expects($this->any())->method(
            'formatParameters'
        )->willReturn([$filter, $sort, $curpage, $perpage]);

        $repository = $this->prophesize(UnAuditedComplaintRepository::class);

        $repository->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(($curpage-1)*$perpage),
            Argument::exact($perpage)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedComplaintArray);

        $this->stub->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
    }
 
    public function testFilterSuccess()
    {
        $unAuditedComplaintArray = array(
            array(\Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint(1)),
            1
        );

        $this->initialFilter($unAuditedComplaintArray);
        
        $this->stub->expects($this->exactly(1))->method('renderView')->willReturn(true);
        
        $result = $this->stub->filter();
        $this->assertTrue($result);
    }

    public function testFilterFailure()
    {
        $unAuditedComplaintArray = array(
            array(),
            0
        );

        $this->initialFilter($unAuditedComplaintArray);
        
        $this->stub->expects($this->exactly(1))->method('displayError')->willReturn(false);
        
        $result = $this->stub->filter();
        $this->assertFalse($result);
    }
}
