<?php
namespace Base\Interaction\Controller\Complaint;

use PHPUnit\Framework\TestCase;

class ComplaintApproveControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockComplaintApproveController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIApproveAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IApproveAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockComplaintApproveController::class)->setMethods(['render'])->getMock();

        $unAuditedComplaint = \Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedComplaint);
    }
}
