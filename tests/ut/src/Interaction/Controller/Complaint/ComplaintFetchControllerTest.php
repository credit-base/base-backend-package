<?php
namespace Base\Interaction\Controller\Complaint;

use PHPUnit\Framework\TestCase;

class ComplaintFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new ComplaintFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockComplaintFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Complaint\IComplaintAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockComplaintFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\Complaint\ComplaintView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockComplaintFetchController();

        $this->assertEquals(
            'complaints',
            $controller->getResourceName()
        );
    }
}
