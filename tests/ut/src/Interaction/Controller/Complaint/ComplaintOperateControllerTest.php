<?php
namespace Base\Interaction\Controller\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Interaction\Command\Complaint\AddComplaintCommand;
use Base\Interaction\Command\Complaint\RevokeComplaintCommand;
use Base\Interaction\Repository\Complaint\ComplaintRepository;

class ComplaintOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new ComplaintOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockComplaintOperateController::class)->setMethods(['render'])->getMock();

        $unAuditedComplaint = \Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedComplaint);
    }

    public function testGetAcceptCommand()
    {
        $controller = new MockComplaintOperateController();

        $data = array(
            'replyContent' => 'replyContent',
            'replyImages' => array('replyImages'),
            'admissibility' => 1,
            'crew' => 1,
            'id' => 1
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $controller->getAcceptCommand($data));
    }

    private function complaintData() : array
    {
        return array(
            "type"=>"complaints",
            "attributes"=>array(
                "title"=>"个人投诉",
                "content"=>"个人投诉",
                "subject"=>"主体名称",
                "name"=>"反馈人真实姓名/企业名称",
                "identify"=>"统一社会信用代码/反馈人身份证号",
                "type"=>1,
                "contact"=>"联系方式",
                "images"=>array(
                    array('name' => 'name', 'identify' => 'identify.jpg'),
                    array('name' => '图片', 'identify' => '图片.jpg')
                )
            ),
            "relationships"=>array(
                "member"=>array(
                    "data"=>array(
                        array("type"=>"members","id"=>1)
                    )
                ),
                "acceptUserGroup"=>array(
                    "data"=>array(
                        array("type"=>"userGroups","id"=>1)
                    )
                )
            )
        );
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 ComplaintWidgetRule 类建立预言, 验证请求参数, getComplaintWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddComplaintCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(bool $result)
    {
        $this->complaint = $this->getMockBuilder(ComplaintOperateController::class)
                            ->setMethods(
                                [
                                    'getRequest',
                                    'validateCommonScenario',
                                    'validateCommentCommonScenario',
                                    'validateComplaintAddScenario',
                                    'getCommandBus',
                                    'getRepository',
                                    'render',
                                    'displayError'
                                ]
                            )->getMock();

        // mock 请求参数
        $data = $this->complaintData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $type = $attributes['type'];
        $name = $attributes['name'];
        $title = $attributes['title'];
        $content = $attributes['content'];
        $identify = $attributes['identify'];
        $contact = $attributes['contact'];
        $images = $attributes['images'];
        $subject = $attributes['subject'];

        $member = $relationships['member']['data'][0]['id'];
        $acceptUserGroup = $relationships['acceptUserGroup']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);
        $this->complaint->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $this->complaint->expects($this->exactly(1))->method('validateCommonScenario')->willReturn(true);
        $this->complaint->expects($this->exactly(1))->method('validateCommentCommonScenario')->willReturn(true);
        $this->complaint->expects($this->exactly(1))->method('validateComplaintAddScenario')->willReturn(true);

        $command = new AddComplaintCommand(
            $subject,
            $name,
            $identify,
            $contact,
            $images,
            $type,
            $title,
            $content,
            $acceptUserGroup,
            $member
        );
        // 为 CommandBus 类建立预言, 传入 AddComplaintCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $this->complaint->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
            
        return $command;
    }

    /**
     * 测试 add 成功
     * 1. 为 ComplaintOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getComplaintWidgetRule、getCommandBus、getUnAuditedComplaintRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 Complaint 类建立预言
     * 4. 为 UnAuditedComplaintRepository 类建立预言, UnAuditedComplaintRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Complaint, getUnAuditedComplaintRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 调用 $this->initAdd(), 期望结果为 true
        $command = $this->initAdd(true);

        // 为 Complaint 类建立预言
        $complaint = \Base\Interaction\Utils\ComplaintMockFactory::generateComplaint($command->id);

        // 为 UnAuditedComplaintRepository 类建立预言, UnAuditedComplaintRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Complaint, getUnAuditedComplaintRepository 方法被调用一次
        $repository = $this->prophesize(ComplaintRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($complaint);
        $this->complaint->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $this->complaint->expects($this->exactly(1))->method('render')->willReturn(true);

        // this->controller->add 方法被调用一次, 且返回结果为 true
        $result = $this->complaint->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 ComplaintOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getComplaintWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且this->controller返回结果为 false
     * 4. this->controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd(false);

        // displayError 方法被调用一次, 且this->controller返回结果为 false
        $this->complaint->expects($this->exactly(1))->method('displayError')->willReturn(false);

        // this->controller->add 方法被调用一次, 且返回结果为 false
        $result = $this->complaint->add();
        $this->assertFalse($result);
    }

    public function testEdit()
    {
        $result = $this->controller->edit(1);
        $this->assertFalse($result);
    }

    private function initialRevoke($result)
    {
        $this->complaint = $this->getMockBuilder(ComplaintOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new RevokeComplaintCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->complaint->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testRevokeSuccess()
    {
        $command = $this->initialRevoke(true);

        $complaint = \Base\Interaction\Utils\ComplaintMockFactory::generateComplaint($command->id);

        $repository = $this->prophesize(ComplaintRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($complaint);

        $this->complaint->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->complaint->expects($this->exactly(1))->method('render');

        $result = $this->complaint->revoke($command->id);
        $this->assertTrue($result);
    }

    public function testRevokeFailure()
    {
        $command = $this->initialRevoke(false);

        $this->complaint->expects($this->exactly(1))->method('displayError');

        $result = $this->complaint->revoke($command->id);
        $this->assertFalse($result);
    }
}
