<?php
namespace Base\Interaction\Controller\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Interaction\Command\Praise\AddPraiseCommand;
use Base\Interaction\Command\Praise\RevokePraiseCommand;
use Base\Interaction\Repository\Praise\PraiseRepository;
use Base\Interaction\Command\Praise\PublishPraiseCommand;
use Base\Interaction\Command\Praise\UnPublishPraiseCommand;

/**
 *
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class PraiseOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new PraiseOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockPraiseOperateController::class)->setMethods(['render'])->getMock();

        $unAuditedPraise = \Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedPraise);
    }

    public function testGetAcceptCommand()
    {
        $controller = new MockPraiseOperateController();

        $data = array(
            'replyContent' => 'replyContent',
            'replyImages' => array('replyImages'),
            'admissibility' => 1,
            'crew' => 1,
            'id' => 1
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $controller->getAcceptCommand($data));
    }

    private function praiseData() : array
    {
        return array(
            "type"=>"praises",
            "attributes"=>array(
                "title"=>"个人表扬",
                "content"=>"个人表扬",
                "subject"=>"主体名称",
                "name"=>"反馈人真实姓名/企业名称",
                "identify"=>"统一社会信用代码/反馈人身份证号",
                "type"=>1,
                "contact"=>"联系方式",
                "images"=>array(
                    array('name' => '图片', 'identify' => '图片.jpg'),
                    array('name' => 'name', 'identify' => 'identify.jpg')
                )
            ),
            "relationships"=>array(
                "member"=>array(
                    "data"=>array(
                        array("type"=>"members","id"=>2)
                    )
                ),
                "acceptUserGroup"=>array(
                    "data"=>array(
                        array("type"=>"userGroups","id"=>2)
                    )
                )
            )
        );
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 PraiseWidgetRule 类建立预言, 验证请求参数, getPraiseWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddPraiseCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(bool $result)
    {
        $this->praise = $this->getMockBuilder(PraiseOperateController::class)
                            ->setMethods(
                                [
                                    'getRequest',
                                    'validateCommonScenario',
                                    'validateCommentCommonScenario',
                                    'validatePraiseAddScenario',
                                    'getCommandBus',
                                    'getRepository',
                                    'render',
                                    'displayError'
                                ]
                            )->getMock();

        // mock 请求参数
        $data = $this->praiseData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $type = $attributes['type'];
        $name = $attributes['name'];
        $title = $attributes['title'];
        $images = $attributes['images'];
        $contact = $attributes['contact'];
        $subject = $attributes['subject'];
        $content = $attributes['content'];
        $identify = $attributes['identify'];

        $member = $relationships['member']['data'][0]['id'];
        $acceptUserGroup = $relationships['acceptUserGroup']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);
        $this->praise->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $this->praise->expects($this->exactly(1))->method('validateCommonScenario')->willReturn(true);
        $this->praise->expects($this->exactly(1))->method('validateCommentCommonScenario')->willReturn(true);
        $this->praise->expects($this->exactly(1))->method('validatePraiseAddScenario')->willReturn(true);

        $command = new AddPraiseCommand(
            $subject,
            $name,
            $identify,
            $contact,
            $images,
            $type,
            $title,
            $content,
            $acceptUserGroup,
            $member
        );
        // 为 CommandBus 类建立预言, 传入 AddPraiseCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $this->praise->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
            
        return $command;
    }

    /**
     * 测试 add 成功
     * 1. 为 PraiseOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getPraiseWidgetRule、getCommandBus、getUnAuditedPraiseRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 Praise 类建立预言
     * 4. 为 UnAuditedPraiseRepository 类建立预言, UnAuditedPraiseRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Praise, getUnAuditedPraiseRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 调用 $this->initAdd(), 期望结果为 true
        $command = $this->initAdd(true);

        // 为 Praise 类建立预言
        $praise = \Base\Interaction\Utils\PraiseMockFactory::generatePraise($command->id);

        // 为 UnAuditedPraiseRepository 类建立预言, UnAuditedPraiseRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Praise, getUnAuditedPraiseRepository 方法被调用一次
        $repository = $this->prophesize(PraiseRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($praise);
        $this->praise->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $this->praise->expects($this->exactly(1))->method('render')->willReturn(true);

        // this->controller->add 方法被调用一次, 且返回结果为 true
        $result = $this->praise->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 PraiseOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getPraiseWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且this->controller返回结果为 false
     * 4. this->controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd(false);

        // displayError 方法被调用一次, 且this->controller返回结果为 false
        $this->praise->expects($this->exactly(1))->method('displayError')->willReturn(false);

        // this->controller->add 方法被调用一次, 且返回结果为 false
        $result = $this->praise->add();
        $this->assertFalse($result);
    }

    public function testEdit()
    {
        $result = $this->controller->edit(1);
        $this->assertFalse($result);
    }

    private function initialRevoke($result)
    {
        $this->praise = $this->getMockBuilder(PraiseOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new RevokePraiseCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->praise->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testRevokeSuccess()
    {
        $command = $this->initialRevoke(true);

        $praise = \Base\Interaction\Utils\PraiseMockFactory::generatePraise($command->id);

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($praise);

        $this->praise->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->praise->expects($this->exactly(1))->method('render');

        $result = $this->praise->revoke($command->id);
        $this->assertTrue($result);
    }

    public function testRevokeFailure()
    {
        $command = $this->initialRevoke(false);

        $this->praise->expects($this->exactly(1))->method('displayError');

        $result = $this->praise->revoke($command->id);
        $this->assertFalse($result);
    }

    private function initialPublish($result)
    {
        $this->praise = $this->getMockBuilder(PraiseOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new PublishPraiseCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->praise->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testPublishSuccess()
    {
        $command = $this->initialPublish(true);

        $praise = \Base\Interaction\Utils\PraiseMockFactory::generatePraise($command->id);

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($praise);

        $this->praise->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->praise->expects($this->exactly(1))->method('render');

        $result = $this->praise->publish($command->id);
        $this->assertTrue($result);
    }

    public function testPublishFailure()
    {
        $command = $this->initialPublish(false);

        $this->praise->expects($this->exactly(1))->method('displayError');

        $result = $this->praise->publish($command->id);
        $this->assertFalse($result);
    }

    private function initialUnPublish($result)
    {
        $this->praise = $this->getMockBuilder(PraiseOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new UnPublishPraiseCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->praise->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testUnPublishSuccess()
    {
        $command = $this->initialUnPublish(true);

        $praise = \Base\Interaction\Utils\PraiseMockFactory::generatePraise($command->id);

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($praise);

        $this->praise->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->praise->expects($this->exactly(1))->method('render');

        $result = $this->praise->unPublish($command->id);
        $this->assertTrue($result);
    }

    public function testUnPublishFailure()
    {
        $command = $this->initialUnPublish(false);

        $this->praise->expects($this->exactly(1))->method('displayError');

        $result = $this->praise->unPublish($command->id);
        $this->assertFalse($result);
    }
}
