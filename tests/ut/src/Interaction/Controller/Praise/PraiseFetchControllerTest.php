<?php
namespace Base\Interaction\Controller\Praise;

use PHPUnit\Framework\TestCase;

class PraiseFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new PraiseFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockPraiseFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Praise\IPraiseAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockPraiseFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\Praise\PraiseView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockPraiseFetchController();

        $this->assertEquals(
            'praises',
            $controller->getResourceName()
        );
    }
}
