<?php
namespace Base\Interaction\Controller\Praise;

use PHPUnit\Framework\TestCase;

class PraiseResubmitControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new PraiseResubmitController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIResubmitAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IResubmitAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockPraiseResubmitController::class)->setMethods(['render'])->getMock();

        $unAuditedPraise = \Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedPraise);
    }

    public function testGetAcceptCommand()
    {
        $controller = new MockPraiseResubmitController();

        $data = array(
            'replyContent' => 'replyContent',
            'replyImages' => array('replyImages'),
            'admissibility' => 1,
            'crew' => 1,
            'id' => 1
        );

        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $controller->getAcceptCommand($data)
        );
    }
}
