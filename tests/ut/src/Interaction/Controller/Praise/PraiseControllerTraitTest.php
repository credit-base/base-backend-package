<?php
namespace Base\Interaction\Controller\Praise;

use PHPUnit\Framework\TestCase;

class PraiseControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockPraiseControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Praise\PraiseRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetUnAuditedInteractionRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->trait->getUnAuditedInteractionRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}
