<?php
namespace Base\Interaction\Controller\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedPraise;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

class UnAuditedPraiseFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new UnAuditedPraiseFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockUnAuditedPraiseFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Repository\Praise\UnAuditedPraiseRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockUnAuditedPraiseFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\Praise\UnAuditedPraiseView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockUnAuditedPraiseFetchController();

        $this->assertEquals(
            'unAuditedPraises',
            $controller->getResourceName()
        );
    }

    private function initialFilter($unAuditedPraiseArray)
    {
        $this->stub = $this->getMockBuilder(MockUnAuditedPraiseFetchController::class)
                    ->setMethods(['getRepository', 'renderView', 'displayError', 'formatParameters'])
                    ->getMock();

        $sort = array();
        $curpage = 1;
        $perpage = 20;
        $filter['applyInfoCategory'] = UnAuditedPraise::APPLY_PRAISE_CATEGORY;
        $filter['applyInfoType'] = UnAuditedPraise::APPLY_PRAISE_TYPE;

        $this->stub->expects($this->any())->method(
            'formatParameters'
        )->willReturn([$filter, $sort, $curpage, $perpage]);

        $repository = $this->prophesize(UnAuditedPraiseRepository::class);

        $repository->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(($curpage-1)*$perpage),
            Argument::exact($perpage)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedPraiseArray);

        $this->stub->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
    }
 
    public function testFilterSuccess()
    {
        $unAuditedPraiseArray = array(
            array(\Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise(1)),
            1
        );

        $this->initialFilter($unAuditedPraiseArray);
        
        $this->stub->expects($this->exactly(1))->method('renderView')->willReturn(true);
        
        $result = $this->stub->filter();
        $this->assertTrue($result);
    }

    public function testFilterFailure()
    {
        $unAuditedPraiseArray = array(
            array(),
            0
        );

        $this->initialFilter($unAuditedPraiseArray);
        
        $this->stub->expects($this->exactly(1))->method('displayError')->willReturn(false);
        
        $result = $this->stub->filter();
        $this->assertFalse($result);
    }
}
