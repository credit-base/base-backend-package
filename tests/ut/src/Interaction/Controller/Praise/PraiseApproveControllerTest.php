<?php
namespace Base\Interaction\Controller\Praise;

use PHPUnit\Framework\TestCase;

class PraiseApproveControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockPraiseApproveController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIApproveAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IApproveAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockPraiseApproveController::class)->setMethods(['render'])->getMock();

        $unAuditedPraise = \Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedPraise);
    }
}
