<?php
namespace Base\Interaction\Controller\Appeal;

use PHPUnit\Framework\TestCase;

class AppealApproveControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockAppealApproveController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIApproveAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IApproveAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockAppealApproveController::class)->setMethods(['render'])->getMock();

        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedAppeal);
    }
}
