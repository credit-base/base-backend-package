<?php
namespace Base\Interaction\Controller\Appeal;

use PHPUnit\Framework\TestCase;

class AppealResubmitControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new AppealResubmitController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIResubmitAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IResubmitAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockAppealResubmitController::class)->setMethods(['render'])->getMock();

        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedAppeal);
    }

    public function testGetAcceptCommand()
    {
        $controller = new MockAppealResubmitController();

        $data = array(
            'replyContent' => 'replyContent',
            'replyImages' => array('replyImages'),
            'admissibility' => 1,
            'crew' => 1,
            'id' => 1
        );

        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $controller->getAcceptCommand($data)
        );
    }
}
