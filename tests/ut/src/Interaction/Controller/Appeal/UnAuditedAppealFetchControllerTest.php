<?php
namespace Base\Interaction\Controller\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

class UnAuditedAppealFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new UnAuditedAppealFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockUnAuditedAppealFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Repository\Appeal\UnAuditedAppealRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockUnAuditedAppealFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\Appeal\UnAuditedAppealView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockUnAuditedAppealFetchController();

        $this->assertEquals(
            'unAuditedAppeals',
            $controller->getResourceName()
        );
    }

    private function initialFilter($unAuditedAppealArray)
    {
        $this->stub = $this->getMockBuilder(MockUnAuditedAppealFetchController::class)
                    ->setMethods(['getRepository', 'renderView', 'displayError', 'formatParameters'])
                    ->getMock();

        $sort = array();
        $curpage = 1;
        $perpage = 20;
        $filter['applyInfoCategory'] = UnAuditedAppeal::APPLY_APPEAL_CATEGORY;
        $filter['applyInfoType'] = UnAuditedAppeal::APPLY_APPEAL_TYPE;

        $this->stub->expects($this->any())->method(
            'formatParameters'
        )->willReturn([$filter, $sort, $curpage, $perpage]);

        $repository = $this->prophesize(UnAuditedAppealRepository::class);

        $repository->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(($curpage-1)*$perpage),
            Argument::exact($perpage)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedAppealArray);

        $this->stub->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
    }
 
    public function testFilterSuccess()
    {
        $unAuditedAppealArray = array(
            array(\Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(1)),
            1
        );

        $this->initialFilter($unAuditedAppealArray);
        
        $this->stub->expects($this->exactly(1))->method('renderView')->willReturn(true);
        
        $result = $this->stub->filter();
        $this->assertTrue($result);
    }

    public function testFilterFailure()
    {
        $unAuditedAppealArray = array(
            array(),
            0
        );

        $this->initialFilter($unAuditedAppealArray);
        
        $this->stub->expects($this->exactly(1))->method('displayError')->willReturn(false);
        
        $result = $this->stub->filter();
        $this->assertFalse($result);
    }
}
