<?php
namespace Base\Interaction\Controller\Appeal;

use PHPUnit\Framework\TestCase;

class AppealFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new AppealFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockAppealFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Appeal\IAppealAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockAppealFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\Appeal\AppealView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockAppealFetchController();

        $this->assertEquals(
            'appeals',
            $controller->getResourceName()
        );
    }
}
