<?php
namespace Base\Interaction\Controller\Appeal;

use PHPUnit\Framework\TestCase;

class AppealControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockAppealControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Appeal\AppealRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetUnAuditedInteractionRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->trait->getUnAuditedInteractionRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}
