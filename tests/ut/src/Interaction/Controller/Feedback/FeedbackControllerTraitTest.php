<?php
namespace Base\Interaction\Controller\Feedback;

use PHPUnit\Framework\TestCase;

class FeedbackControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockFeedbackControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Feedback\FeedbackRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetUnAuditedInteractionRepository()
    {
        $this->assertInstanceOf(
            'Base\ApplyForm\Repository\ApplyFormRepository',
            $this->trait->getUnAuditedInteractionRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}
