<?php
namespace Base\Interaction\Controller\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\Interaction\Command\Feedback\AddFeedbackCommand;
use Base\Interaction\Command\Feedback\RevokeFeedbackCommand;
use Base\Interaction\Repository\Feedback\FeedbackRepository;

class FeedbackOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new FeedbackOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockFeedbackOperateController::class)->setMethods(['render'])->getMock();

        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedFeedback);
    }

    public function testGetAcceptCommand()
    {
        $controller = new MockFeedbackOperateController();

        $data = array(
            'replyContent' => 'replyContent',
            'replyImages' => array('replyImages'),
            'admissibility' => 1,
            'crew' => 1,
            'id' => 1
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $controller->getAcceptCommand($data));
    }

    private function feedbackData() : array
    {
        return array(
            "type"=>"feedbacks",
            "attributes"=>array(
                "title"=>"问题反馈",
                "content"=>"问题反馈"
            ),
            "relationships"=>array(
                "member"=>array(
                    "data"=>array(
                        array("type"=>"members","id"=>1)
                    )
                ),
                "acceptUserGroup"=>array(
                    "data"=>array(
                        array("type"=>"userGroups","id"=>1)
                    )
                )
            )
        );
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 FeedbackWidgetRule 类建立预言, 验证请求参数, getFeedbackWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddFeedbackCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(bool $result)
    {
        $this->feedback = $this->getMockBuilder(FeedbackOperateController::class)
                            ->setMethods(
                                [
                                    'getRequest',
                                    'validateCommonScenario',
                                    'getCommandBus',
                                    'getRepository',
                                    'render',
                                    'displayError'
                                ]
                            )->getMock();

        // mock 请求参数
        $data = $this->feedbackData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $content = $attributes['content'];

        $member = $relationships['member']['data'][0]['id'];
        $acceptUserGroup = $relationships['acceptUserGroup']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);
        $this->feedback->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $this->feedback->expects($this->exactly(1))->method('validateCommonScenario')->willReturn(true);

        $command = new AddFeedbackCommand(
            $title,
            $content,
            $acceptUserGroup,
            $member
        );
        // 为 CommandBus 类建立预言, 传入 AddFeedbackCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $this->feedback->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
            
        return $command;
    }

    /**
     * 测试 add 成功
     * 1. 为 FeedbackOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getFeedbackWidgetRule、getCommandBus、getUnAuditedFeedbackRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 Feedback 类建立预言
     * 4. 为 UnAuditedFeedbackRepository 类建立预言, UnAuditedFeedbackRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的Feedback, getUnAuditedFeedbackRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 调用 $this->initAdd(), 期望结果为 true
        $command = $this->initAdd(true);

        // 为 Feedback 类建立预言
        $feedback = \Base\Interaction\Utils\FeedbackMockFactory::generateFeedback($command->id);

        // 为 UnAuditedFeedbackRepository 类建立预言, UnAuditedFeedbackRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的Feedback, getUnAuditedFeedbackRepository 方法被调用一次
        $repository = $this->prophesize(FeedbackRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($feedback);
        $this->feedback->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $this->feedback->expects($this->exactly(1))->method('render')->willReturn(true);

        // this->controller->add 方法被调用一次, 且返回结果为 true
        $result = $this->feedback->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 FeedbackOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getFeedbackWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且this->controller返回结果为 false
     * 4. this->controller->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd(false);

        // displayError 方法被调用一次, 且this->controller返回结果为 false
        $this->feedback->expects($this->exactly(1))->method('displayError')->willReturn(false);

        // this->controller->add 方法被调用一次, 且返回结果为 false
        $result = $this->feedback->add();
        $this->assertFalse($result);
    }

    public function testEdit()
    {
        $result = $this->controller->edit(1);
        $this->assertFalse($result);
    }

    private function initialRevoke($result)
    {
        $this->feedback = $this->getMockBuilder(FeedbackOperateController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new RevokeFeedbackCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->feedback->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());

        return $command;
    }

    public function testRevokeSuccess()
    {
        $command = $this->initialRevoke(true);

        $feedback = \Base\Interaction\Utils\FeedbackMockFactory::generateFeedback($command->id);

        $repository = $this->prophesize(FeedbackRepository::class);
        $repository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($feedback);

        $this->feedback->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
             
        $this->feedback->expects($this->exactly(1))->method('render');

        $result = $this->feedback->revoke($command->id);
        $this->assertTrue($result);
    }

    public function testRevokeFailure()
    {
        $command = $this->initialRevoke(false);

        $this->feedback->expects($this->exactly(1))->method('displayError');

        $result = $this->feedback->revoke($command->id);
        $this->assertFalse($result);
    }
}
