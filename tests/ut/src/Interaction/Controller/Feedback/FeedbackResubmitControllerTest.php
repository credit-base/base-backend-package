<?php
namespace Base\Interaction\Controller\Feedback;

use PHPUnit\Framework\TestCase;

class FeedbackResubmitControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new FeedbackResubmitController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIResubmitAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IResubmitAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockFeedbackResubmitController::class)->setMethods(['render'])->getMock();

        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedFeedback);
    }

    public function testGetAcceptCommand()
    {
        $controller = new MockFeedbackResubmitController();

        $data = array(
            'replyContent' => 'replyContent',
            'replyImages' => array('replyImages'),
            'admissibility' => 1,
            'crew' => 1,
            'id' => 1
        );

        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $controller->getAcceptCommand($data)
        );
    }
}
