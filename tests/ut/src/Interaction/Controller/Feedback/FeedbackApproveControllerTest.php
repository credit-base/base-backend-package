<?php
namespace Base\Interaction\Controller\Feedback;

use PHPUnit\Framework\TestCase;

class FeedbackApproveControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockFeedbackApproveController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIApproveAbleController()
    {
        $this->assertInstanceOf(
            'Base\Common\Controller\Interfaces\IApproveAbleController',
            $this->controller
        );
    }

    public function testDisplaySuccess()
    {
        $controller = $this->getMockBuilder(MockFeedbackApproveController::class)->setMethods(['render'])->getMock();

        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback(1);
        
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        $controller->displaySuccess($unAuditedFeedback);
    }
}
