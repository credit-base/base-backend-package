<?php
namespace Base\Interaction\Controller\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedFeedback;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

class UnAuditedFeedbackFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new UnAuditedFeedbackFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockUnAuditedFeedbackFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockUnAuditedFeedbackFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\Feedback\UnAuditedFeedbackView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockUnAuditedFeedbackFetchController();

        $this->assertEquals(
            'unAuditedFeedbacks',
            $controller->getResourceName()
        );
    }

    private function initialFilter($unAuditedFeedbackArray)
    {
        $this->stub = $this->getMockBuilder(MockUnAuditedFeedbackFetchController::class)
                    ->setMethods(['getRepository', 'renderView', 'displayError', 'formatParameters'])
                    ->getMock();

        $sort = array();
        $curpage = 1;
        $perpage = 20;
        $filter['applyInfoCategory'] = UnAuditedFeedback::APPLY_FEEDBACK_CATEGORY;
        $filter['applyInfoType'] = UnAuditedFeedback::APPLY_FEEDBACK_TYPE;

        $this->stub->expects($this->any())->method(
            'formatParameters'
        )->willReturn([$filter, $sort, $curpage, $perpage]);

        $repository = $this->prophesize(UnAuditedFeedbackRepository::class);

        $repository->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(($curpage-1)*$perpage),
            Argument::exact($perpage)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedFeedbackArray);

        $this->stub->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
    }
 
    public function testFilterSuccess()
    {
        $unAuditedFeedbackArray = array(
            array(\Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback(1)),
            1
        );

        $this->initialFilter($unAuditedFeedbackArray);
        
        $this->stub->expects($this->exactly(1))->method('renderView')->willReturn(true);
        
        $result = $this->stub->filter();
        $this->assertTrue($result);
    }

    public function testFilterFailure()
    {
        $unAuditedFeedbackArray = array(
            array(),
            0
        );

        $this->initialFilter($unAuditedFeedbackArray);
        
        $this->stub->expects($this->exactly(1))->method('displayError')->willReturn(false);
        
        $result = $this->stub->filter();
        $this->assertFalse($result);
    }
}
