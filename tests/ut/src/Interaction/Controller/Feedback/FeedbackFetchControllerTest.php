<?php
namespace Base\Interaction\Controller\Feedback;

use PHPUnit\Framework\TestCase;

class FeedbackFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new FeedbackFetchController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockFeedbackFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\Adapter\Feedback\IFeedbackAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockFeedbackFetchController();

        $this->assertInstanceOf(
            'Base\Interaction\View\Feedback\FeedbackView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockFeedbackFetchController();

        $this->assertEquals(
            'feedbacks',
            $controller->getResourceName()
        );
    }
}
