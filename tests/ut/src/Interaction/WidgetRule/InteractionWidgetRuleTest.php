<?php
namespace Base\Interaction\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\IInteractionAble;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class InteractionWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new InteractionWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //title -- start
    /**
     * @dataProvider invalidTitleProvider
     */
    public function testTitle($actual, $expected)
    {
        $result = $this->widgetRule->title($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidTitleProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.InteractionWidgetRule::TITLE_MIN_LENGTH.
                    ','.InteractionWidgetRule::TITLE_MAX_LENGTH.'}'
            ), true),
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.
                    (InteractionWidgetRule::TITLE_MAX_LENGTH + 1 ).','.
                    (InteractionWidgetRule::TITLE_MAX_LENGTH + 5 ).
                '}'
            ), false),
            array('', false),
            array($faker->randomDigit(1), false)
        );
    }
    //title -- end

    //content -- start
    /**
     * @dataProvider invalidContentProvider
     */
    public function testContent($actual, $expected)
    {
        $result = $this->widgetRule->content($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidContentProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.InteractionWidgetRule::CONTENT_MIN_LENGTH.
                    ','.InteractionWidgetRule::CONTENT_MAX_LENGTH.'}'
            ), true),
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.
                    (InteractionWidgetRule::CONTENT_MAX_LENGTH + 10 ).','.
                    (InteractionWidgetRule::CONTENT_MAX_LENGTH + 50 ).
                '}'
            ), false),
            array('', false),
            array($faker->randomDigit(1), false)
        );
    }
    //content -- end

    //name -- start
    /**
     * @dataProvider invalidNameProvider
     */
    public function testName($actual, $expected)
    {
        $result = $this->widgetRule->name($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidNameProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.InteractionWidgetRule::NAME_MIN_LENGTH.
                    ','.InteractionWidgetRule::NAME_MAX_LENGTH.'}'
            ), true),
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.
                    (InteractionWidgetRule::NAME_MAX_LENGTH + 1 ).','.
                    (InteractionWidgetRule::NAME_MAX_LENGTH + 5 ).
                '}'
            ), false),
            array('', false),
            array(($faker->randomDigit(1)), false)
        );
    }
    //name -- end

    //contact -- start
    /**
     * @dataProvider invalidContactProvider
     */
    public function testContact($actual, $expected)
    {
        $result = $this->widgetRule->contact($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidContactProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.InteractionWidgetRule::CONTACT_MIN_LENGTH.
                    ','.InteractionWidgetRule::CONTACT_MAX_LENGTH.'}'
            ), true),
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.
                    (InteractionWidgetRule::CONTACT_MAX_LENGTH + 1 ).','.
                    (InteractionWidgetRule::CONTACT_MAX_LENGTH + 5 ).
                '}'
            ), false),
            array('', false),
            array($faker->randomDigit(1), false)
        );
    }
    //contact -- end

    //subject -- start
    /**
     * @dataProvider invalidSubjectProvider
     */
    public function testSubject($actual, $expected)
    {
        $result = $this->widgetRule->subject($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidSubjectProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.InteractionWidgetRule::SUBJECT_MIN_LENGTH.
                    ','.InteractionWidgetRule::SUBJECT_MAX_LENGTH.'}'
            ), true),
            array($faker->regexify(
                '[A-Za-z0-9.%+-]{'.
                    (InteractionWidgetRule::SUBJECT_MAX_LENGTH + 1 ).','.
                    (InteractionWidgetRule::SUBJECT_MAX_LENGTH + 5 ).
                '}'
            ), false),
            array('', false),
            array(($faker->randomDigit(1)), false)
        );
    }
    //subject -- end

    //identify -- start
    /**
     * @dataProvider invalidIdentifyProvider
     */
    public function testIdentifyInvalid($actual, $expected)
    {
        $result = $this->widgetRule->identify($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidIdentifyProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array($faker->bothify(''), false),
            array($faker->bothify('##??'), false),//四位混编
            array($faker->bothify('???###???####????##'), false),//十九位
            array($faker->bothify('###############'), true),//十五位
            array($faker->bothify('#################X'), true),//十八位
        );
    }
    //identify -- end

    /**
     * @dataProvider imagesProvider
     */
    public function testImages($actual, $expected)
    {
        $result = $this->widgetRule->images($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function imagesProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(
                [
                    ['name'=>$faker->word, 'identify'=>'1.jpg'],
                    ['name'=>$faker->word, 'identify'=>'1.png'],
                    ['name'=>$faker->word, 'identify'=>'1.jpeg']
                ], true),
            array(
                [
                    ['name'=>$faker->word, 'identify'=>'1.jpg'],
                    ['name'=>$faker->word, 'identify'=>'1.pdf']
                ], false),
            array($faker->word, false),
            array(
                [
                    ['name'=>$faker->word, 'identify'=>$faker->word],
                    ['name'=>$faker->word, 'identify'=>$faker->word]
                ], false),
            array(
                [
                    ['name'=>$faker->word, 'identify'=>'1.jpg'],
                    ['name'=>$faker->word, 'identify'=>'2.jpg'],
                    ['name'=>$faker->word, 'identify'=>'3.jpg'],
                    ['name'=>$faker->word, 'identify'=>'4.jpg'],
                    ['name'=>$faker->word, 'identify'=>'5.jpg'],
                    ['name'=>$faker->word, 'identify'=>'6.jpg'],
                    ['name'=>$faker->word, 'identify'=>'7.jpg'],
                    ['name'=>$faker->word, 'identify'=>'8.jpg'],
                    ['name'=>$faker->word, 'identify'=>'9.jpg'],
                    ['name'=>$faker->word, 'identify'=>'10.jpg']
                ], false),
        );
    }

    //type -- start
    /**
     * @dataProvider invalidTypeProvider
     */
    public function testTypeInvalid($actual, $expected)
    {
        $result = $this->widgetRule->type($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidTypeProvider()
    {
        return array(
            array('', false),
            array(IInteractionAble::TYPE['PERSONAL'], true),
            array(IInteractionAble::TYPE['ENTERPRISE'], true),
            array(IInteractionAble::TYPE['GOVERNMENT'], true),
            array('type', false),
            array(999, false),
        );
    }
    //type -- end

    //admissibility -- start
    /**
     * @dataProvider invalidAdmissibilityProvider
     */
    public function testAdmissibilityInvalid($actual, $expected)
    {
        $result = $this->widgetRule->admissibility($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidAdmissibilityProvider()
    {
        return array(
            array('', false),
            array(IInteractionAble::ADMISSIBILITY['ADMISSIBLE'], true),
            array(IInteractionAble::ADMISSIBILITY['INADMISSIBLE'], true),
            array('admissibility', false),
            array(999, false),
        );
    }
    //admissibility -- end
}
