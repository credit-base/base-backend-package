<?php
namespace Base\Interaction\Utils;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Model\UnAuditedAppeal;

class AppealMockFactory
{
    use InteractionMockFactoryTrait;
    
    public static function generateAppeal(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Appeal {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $appeal = new Appeal($id);
        $appeal->setId($id);

        self::generateCommon($appeal, $seed, $value);
        self::generateCommentCommon($appeal, $seed, $value);

        //certificates
        $certificates = isset($value['certificates']) ? $value['certificates'] :  array(
            array(
                'name'=>$faker->name(),
                'identify'=>$faker->word().'.jpg'
            )
        );
        $appeal->setCertificates($certificates);

        return $appeal;
    }

    public static function generateUnAuditedAppeal(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedAppeal {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditedAppeal = new UnAuditedAppeal($id);
        $unAuditedAppeal->setApplyId($id);

        self::generateCommon($unAuditedAppeal, $seed, $value);
        self::generateCommentCommon($unAuditedAppeal, $seed, $value);
        self::generateUnAuditedInteraction($unAuditedAppeal, $seed, $value);

        //certificates
        $certificates = isset($value['certificates']) ? $value['certificates'] :  array(
            array(
                'name'=>$faker->name(),
                'identify'=>$faker->word().'.jpg'
            )
        );
        $unAuditedAppeal->setCertificates($certificates);

        return $unAuditedAppeal;
    }
}
