<?php
namespace Base\Interaction\Utils;

trait InteractionUtilsTrait
{
    private function compareArrayAndObjectUnAuditedInteraction(
        array $expectedArray,
        $unAuditedInteraction
    ) {
        $applyInfo = array();
        if (is_string($expectedArray['apply_info'])) {
            $applyInfo = unserialize(gzuncompress(base64_decode($expectedArray['apply_info'], true)));
        }
        if (is_array($expectedArray['apply_info'])) {
            $applyInfo = $expectedArray['apply_info'];
        }
        
        $this->assertEquals($applyInfo, $unAuditedInteraction->getApplyInfo());
        $this->assertEquals($expectedArray['apply_form_id'], $unAuditedInteraction->getApplyId());
        $this->assertEquals($expectedArray['title'], $unAuditedInteraction->getApplyTitle());
        $this->assertEquals($expectedArray['relation_id'], $unAuditedInteraction->getRelation()->getId());
        $this->assertEquals($expectedArray['operation_type'], $unAuditedInteraction->getOperationType());
        $this->assertEquals(
            $expectedArray['apply_info_category'],
            $unAuditedInteraction->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals(
            $expectedArray['apply_info_type'],
            $unAuditedInteraction->getApplyInfoCategory()->getType()
        );
        $this->assertEquals($expectedArray['reject_reason'], $unAuditedInteraction->getRejectReason());
        $this->assertEquals($expectedArray['apply_status'], $unAuditedInteraction->getApplyStatus());
        $this->assertEquals($expectedArray['apply_crew_id'], $unAuditedInteraction->getApplyCrew()->getId());
        $this->assertEquals($expectedArray['apply_usergroup_id'], $unAuditedInteraction->getApplyUserGroup()->getId());
    }

    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $interaction
    ) {
        $this->assertEquals($expectedArray['title'], $interaction->getTitle());
        $this->assertEquals($expectedArray['content'], $interaction->getContent());
        $this->assertEquals($expectedArray['accept_status'], $interaction->getAcceptStatus());

        $this->assertEquals($expectedArray['member_id'], $interaction->getMember()->getId());
        $this->assertEquals($expectedArray['accept_usergroup_id'], $interaction->getAcceptUserGroup()->getId());
        $this->assertEquals($expectedArray['reply_id'], $interaction->getReply()->getId());

        $this->assertEquals($expectedArray['status'], $interaction->getStatus());
        $this->assertEquals($expectedArray['create_time'], $interaction->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $interaction->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $interaction->getStatusTime());
    }

    private function compareArrayAndObjectCommentCommon(
        array $expectedArray,
        $interaction
    ) {
        $this->assertEquals($expectedArray['name'], $interaction->getName());
        $this->assertEquals($expectedArray['identify'], $interaction->getIdentify());
        $this->assertEquals($expectedArray['type'], $interaction->getType());
        $this->assertEquals($expectedArray['contact'], $interaction->getContact());
        $this->imagesEquals($expectedArray, $interaction);
    }

    private function imagesEquals($expectedArray, $interaction)
    {
        $images = array();

        if (is_string($expectedArray['images'])) {
            $images = json_decode($expectedArray['images'], true);
        }
        if (is_array($expectedArray['images'])) {
            $images = $expectedArray['images'];
        }

        $this->assertEquals($images, $interaction->getImages());
    }
}
