<?php
namespace Base\Interaction\Utils;

trait PraiseUtils
{
    use InteractionUtilsTrait;

    private function compareArrayAndObjectPraise(
        array $expectedArray,
        $praise
    ) {
        $this->assertEquals($expectedArray['praise_id'], $praise->getId());

        $this->compareArrayAndObjectCommon($expectedArray, $praise);
        $this->compareArrayAndObjectCommentCommon($expectedArray, $praise);
        $this->assertEquals($expectedArray['subject'], $praise->getSubject());
    }

    private function compareArrayAndObjectsUnAuditedPraise(
        array $expectedArray,
        $unAuditedPraise
    ) {
        $this->assertEquals($expectedArray['praise_id'], $unAuditedPraise->getApplyId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedPraise);
        $this->compareArrayAndObjectCommentCommon($expectedArray, $unAuditedPraise);
        $this->assertEquals($expectedArray['subject'], $unAuditedPraise->getSubject());
    }

    private function compareArrayAndObjectUnAuditedPraise(
        array $expectedArray,
        $unAuditedPraise
    ) {
        $this->compareArrayAndObjectUnAuditedInteraction($expectedArray, $unAuditedPraise);
    }
}
