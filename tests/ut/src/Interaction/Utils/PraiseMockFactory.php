<?php
namespace Base\Interaction\Utils;

use Base\Interaction\Model\Praise;
use Base\Interaction\Model\UnAuditedPraise;

class PraiseMockFactory
{
    use InteractionMockFactoryTrait;
    
    public static function generatePraise(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Praise {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $praise = new Praise($id);
        $praise->setId($id);

        self::generateCommon($praise, $seed, $value);
        self::generateCommentCommon($praise, $seed, $value);

        //subject
        $subject = isset($value['subject']) ? $value['subject'] :  $faker->name();
        $praise->setSubject($subject);

        return $praise;
    }

    public static function generateUnAuditedPraise(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedPraise {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditedPraise = new UnAuditedPraise($id);
        $unAuditedPraise->setApplyId($id);

        self::generateCommon($unAuditedPraise, $seed, $value);
        self::generateCommentCommon($unAuditedPraise, $seed, $value);
        self::generateUnAuditedInteraction($unAuditedPraise, $seed, $value);

        //subject
        $subject = isset($value['subject']) ? $value['subject'] :  $faker->name();
        $unAuditedPraise->setSubject($subject);

        return $unAuditedPraise;
    }
}
