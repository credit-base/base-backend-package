<?php
namespace Base\Interaction\Utils;

trait FeedbackUtils
{
    use InteractionUtilsTrait;

    private function compareArrayAndObjectFeedback(
        array $expectedArray,
        $feedback
    ) {
        $this->assertEquals($expectedArray['feedback_id'], $feedback->getId());

        $this->compareArrayAndObjectCommon($expectedArray, $feedback);
    }

    private function compareArrayAndObjectsUnAuditedFeedback(
        array $expectedArray,
        $unAuditedFeedback
    ) {
        $this->assertEquals($expectedArray['feedback_id'], $unAuditedFeedback->getApplyId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedFeedback);
    }

    private function compareArrayAndObjectUnAuditedFeedback(
        array $expectedArray,
        $unAuditedFeedback
    ) {
        $this->compareArrayAndObjectUnAuditedInteraction($expectedArray, $unAuditedFeedback);
    }
}
