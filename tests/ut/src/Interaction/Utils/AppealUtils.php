<?php
namespace Base\Interaction\Utils;

trait AppealUtils
{
    use InteractionUtilsTrait;

    private function compareArrayAndObjectAppeal(
        array $expectedArray,
        $appeal
    ) {
        $this->assertEquals($expectedArray['appeal_id'], $appeal->getId());

        $this->compareArrayAndObjectCommon($expectedArray, $appeal);
        $this->compareArrayAndObjectCommentCommon($expectedArray, $appeal);
        $this->certificatesEquals($expectedArray, $appeal);
    }

    private function compareArrayAndObjectsUnAuditedAppeal(
        array $expectedArray,
        $unAuditedAppeal
    ) {
        $this->assertEquals($expectedArray['appeal_id'], $unAuditedAppeal->getApplyId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedAppeal);
        $this->compareArrayAndObjectCommentCommon($expectedArray, $unAuditedAppeal);
        $this->certificatesEquals($expectedArray, $unAuditedAppeal);
    }

    private function compareArrayAndObjectUnAuditedAppeal(
        array $expectedArray,
        $unAuditedAppeal
    ) {
        $this->compareArrayAndObjectUnAuditedInteraction($expectedArray, $unAuditedAppeal);
    }

    private function certificatesEquals($expectedArray, $interaction)
    {
        $certificates = array();

        if (is_string($expectedArray['certificates'])) {
            $certificates = json_decode($expectedArray['certificates'], true);
        }
        if (is_array($expectedArray['certificates'])) {
            $certificates = $expectedArray['certificates'];
        }

        $this->assertEquals($certificates, $interaction->getCertificates());
    }
}
