<?php
namespace Base\Interaction\Utils;

use Base\Interaction\Model\Complaint;
use Base\Interaction\Model\UnAuditedComplaint;

class ComplaintMockFactory
{
    use InteractionMockFactoryTrait;
    
    public static function generateComplaint(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Complaint {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $complaint = new Complaint($id);
        $complaint->setId($id);

        self::generateCommon($complaint, $seed, $value);
        self::generateCommentCommon($complaint, $seed, $value);

        //subject
        $subject = isset($value['subject']) ? $value['subject'] :  $faker->name();
        $complaint->setSubject($subject);

        return $complaint;
    }

    public static function generateUnAuditedComplaint(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedComplaint {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditedComplaint = new UnAuditedComplaint($id);
        $unAuditedComplaint->setApplyId($id);

        self::generateCommon($unAuditedComplaint, $seed, $value);
        self::generateCommentCommon($unAuditedComplaint, $seed, $value);
        self::generateUnAuditedInteraction($unAuditedComplaint, $seed, $value);

        //subject
        $subject = isset($value['subject']) ? $value['subject'] :  $faker->name();
        $unAuditedComplaint->setSubject($subject);

        return $unAuditedComplaint;
    }
}
