<?php
namespace Base\Interaction\Utils;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\IInteractionAble;

class ReplyMockFactory
{
    public static function generateReply(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Reply {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $reply = new Reply($id);
        $reply->setId($id);

        //content
        $content = isset($value['content']) ? $value['content'] : $faker->word();
        $reply->setContent($content);

        //images
        $images = isset($value['images']) ? $value['images'] : array(
            array('name'=>$faker->name(), 'identify'=>$faker->word().'.png')
        );
        $reply->setImages($images);

        self::generateCrew($reply, $faker, $value);
        self::generateAcceptUserGroup($reply, $faker, $value);
        self::generateAdmissibility($reply, $faker, $value);
        self::generateStatus($reply, $faker, $value);
        
        $reply->setCreateTime($faker->unixTime());
        $reply->setUpdateTime($faker->unixTime());
        $reply->setStatusTime($faker->unixTime());

        return $reply;
    }

    protected static function generateCrew($reply, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $reply->setCrew($crew);
    }

    protected static function generateAcceptUserGroup($reply, $faker, $value) : void
    {
        $acceptUserGroup = isset($value['acceptUserGroup']) ?
        $value['acceptUserGroup'] :
        \Base\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
        
        $reply->setAcceptUserGroup($acceptUserGroup);
    }

    protected static function generateStatus($reply, $faker, $value) : void
    {
        unset($faker);
        $status = isset($value['status']) ? $value['status'] : Reply::STATUS_NORMAL;
        $reply->setStatus($status);
    }

    protected static function generateAdmissibility($reply, $faker, $value) : void
    {
        //admissibility
        $admissibility = isset($value['admissibility']) ?
        $value['admissibility'] :
        $faker->randomElement(
            IInteractionAble::ADMISSIBILITY
        );

        $reply->setAdmissibility($admissibility);
    }
}
