<?php
namespace Base\Interaction\Utils;

use Base\Interaction\Model\Feedback;
use Base\Interaction\Model\UnAuditedFeedback;

class FeedbackMockFactory
{
    use InteractionMockFactoryTrait;
    
    public static function generateFeedback(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Feedback {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $feedback = new Feedback($id);
        $feedback->setId($id);

        self::generateCommon($feedback, $seed, $value);

        return $feedback;
    }

    public static function generateUnAuditedFeedback(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedFeedback {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditedFeedback = new UnAuditedFeedback($id);
        $unAuditedFeedback->setApplyId($id);

        self::generateCommon($unAuditedFeedback, $seed, $value);
        self::generateUnAuditedInteraction($unAuditedFeedback, $seed, $value);

        return $unAuditedFeedback;
    }
}
