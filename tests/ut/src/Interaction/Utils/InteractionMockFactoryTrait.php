<?php
namespace Base\Interaction\Utils;

use Base\Common\Model\IApproveAble;
use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\IInteractionAble;

trait InteractionMockFactoryTrait
{
    protected static function generateCommon(
        IInteractionAble $interaction,
        int $seed = 0,
        array $value = array()
    ) {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);
        //title
        $title = isset($value['title']) ? $value['title'] : $faker->word();
        $interaction->setTitle($title);

        //content
        $content = isset($value['content']) ? $value['content'] : $faker->word();
        $interaction->setContent($content);

        //acceptStatus
        self::generateAcceptStatus($interaction, $faker, $value);

        //member
        self::generateMember($interaction, $faker, $value);

        //acceptUserGroup
        self::generateAcceptUserGroup($interaction, $faker, $value);

        //reply
        self::generateReply($interaction, $faker, $value);
        
        //status
        self::generateStatus($interaction, $faker, $value);

        $interaction->setCreateTime($faker->unixTime());
        $interaction->setUpdateTime($faker->unixTime());
        $interaction->setStatusTime($faker->unixTime());

        return $interaction;
    }

    protected static function generateCommentCommon(
        IInteractionAble $interaction,
        int $seed = 0,
        array $value = array()
    ) {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);
        //name
        $name = isset($value['name']) ? $value['name'] : $faker->name();
        $interaction->setName($name);

        //identify
        $identify = isset($value['identify']) ? $value['identify'] : $faker->bothify('##############????');
        $interaction->setIdentify($identify);

        //type
        self::generateType($interaction, $faker, $value);
        
        //contact
        $contact = isset($value['contact']) ? $value['contact'] : $faker->phoneNumber();
        $interaction->setContact($contact);

        //images
        $images = isset($value['images']) ? $value['images'] :  array(
            array(
                'name'=>$faker->name(),
                'identify'=>$faker->word().'.jpg'
            )
        );
        $interaction->setImages($images);

        return $interaction;
    }

    public static function generateUnAuditedInteraction(
        IApplyFormAble $unAuditInteraction,
        int $seed = 0,
        array $value = array()
    ) : IApplyFormAble {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditInteraction->setApplyTitle($unAuditInteraction->getTitle());
        $unAuditInteraction->setRelation($unAuditInteraction->getMember());
        $unAuditInteraction->setApplyUserGroup($unAuditInteraction->getAcceptUserGroup());
        //crew
        self::generateCrew($unAuditInteraction, $faker, $value);
        $unAuditInteraction->setRejectReason($unAuditInteraction->getTitle());
        $unAuditInteraction->setApplyInfo(array('apply_info'));

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            IApproveAble::APPLY_STATUS
        );
        $unAuditInteraction->setApplyStatus($applyStatus);

        //operationType
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomElement(
            IApproveAble::OPERATION_TYPE
        );
        $unAuditInteraction->setOperationType($operationType);

        return $unAuditInteraction;
    }

    protected static function generateAcceptStatus(IInteractionAble $interaction, $faker, $value) : void
    {
        $acceptStatus = isset($value['acceptStatus']) ?
            $value['acceptStatus'] :
            $faker->randomElement(
                IInteractionAble::ACCEPT_STATUS
            );
        
        $interaction->setAcceptStatus($acceptStatus);
    }
    
    protected static function generateMember(IInteractionAble $interaction, $faker, $value) : void
    {
        $member = isset($value['member']) ?
        $value['member'] :
        \Base\Member\Utils\MockFactory::generateMember($faker->randomDigit());
        
        $interaction->setMember($member);
    }

    protected static function generateAcceptUserGroup(IInteractionAble $interaction, $faker, $value) : void
    {
        $acceptUserGroup = isset($value['acceptUserGroup']) ?
        $value['acceptUserGroup'] :
        \Base\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
        
        $interaction->setAcceptUserGroup($acceptUserGroup);
    }

    protected static function generateReply(IInteractionAble $interaction, $faker, $value) : void
    {
        $reply = isset($value['reply']) ?
        $value['reply'] :
        \Base\Interaction\Utils\ReplyMockFactory::generateReply($faker->randomDigit());
        
        $interaction->setReply($reply);
    }

    protected static function generateStatus(IInteractionAble $interaction, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                IInteractionAble::INTERACTION_STATUS
            );
        
        $interaction->setStatus($status);
    }

    protected static function generateType(IInteractionAble $interaction, $faker, $value) : void
    {
        $type = isset($value['type']) ?
            $value['type'] :
            $faker->randomElement(
                IInteractionAble::TYPE
            );
        
        $interaction->setType($type);
    }

    protected static function generateCrew(IApplyFormAble $unAuditInteraction, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $unAuditInteraction->setApplyCrew($crew);
    }
}
