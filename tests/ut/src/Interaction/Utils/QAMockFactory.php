<?php
namespace Base\Interaction\Utils;

use Base\Interaction\Model\QA;
use Base\Interaction\Model\UnAuditedQA;

class QAMockFactory
{
    use InteractionMockFactoryTrait;
    
    public static function generateQA(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : QA {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $qaInteraction = new QA($id);
        $qaInteraction->setId($id);

        self::generateCommon($qaInteraction, $seed, $value);

        return $qaInteraction;
    }

    public static function generateUnAuditedQA(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedQA {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditedQA = new UnAuditedQA($id);
        $unAuditedQA->setApplyId($id);

        self::generateCommon($unAuditedQA, $seed, $value);
        self::generateUnAuditedInteraction($unAuditedQA, $seed, $value);

        return $unAuditedQA;
    }
}
