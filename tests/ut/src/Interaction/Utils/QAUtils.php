<?php
namespace Base\Interaction\Utils;

trait QAUtils
{
    use InteractionUtilsTrait;

    private function compareArrayAndObjectQA(
        array $expectedArray,
        $qaInteraction
    ) {
        $this->assertEquals($expectedArray['qa_id'], $qaInteraction->getId());

        $this->compareArrayAndObjectCommon($expectedArray, $qaInteraction);
    }

    private function compareArrayAndObjectsUnAuditedQA(
        array $expectedArray,
        $unAuditedQA
    ) {
        $this->assertEquals($expectedArray['qa_id'], $unAuditedQA->getApplyId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedQA);
    }

    private function compareArrayAndObjectUnAuditedQA(
        array $expectedArray,
        $unAuditedQA
    ) {
        $this->compareArrayAndObjectUnAuditedInteraction($expectedArray, $unAuditedQA);
    }
}
