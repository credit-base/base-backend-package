<?php
namespace Base\Interaction\Utils;

trait ComplaintUtils
{
    use InteractionUtilsTrait;

    private function compareArrayAndObjectComplaint(
        array $expectedArray,
        $complaint
    ) {
        $this->assertEquals($expectedArray['complaint_id'], $complaint->getId());

        $this->compareArrayAndObjectCommon($expectedArray, $complaint);
        $this->compareArrayAndObjectCommentCommon($expectedArray, $complaint);
        $this->assertEquals($expectedArray['subject'], $complaint->getSubject());
    }

    private function compareArrayAndObjectsUnAuditedComplaint(
        array $expectedArray,
        $unAuditedComplaint
    ) {
        $this->assertEquals($expectedArray['complaint_id'], $unAuditedComplaint->getApplyId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedComplaint);
        $this->compareArrayAndObjectCommentCommon($expectedArray, $unAuditedComplaint);
        $this->assertEquals($expectedArray['subject'], $unAuditedComplaint->getSubject());
    }

    private function compareArrayAndObjectUnAuditedComplaint(
        array $expectedArray,
        $unAuditedComplaint
    ) {
        $this->compareArrayAndObjectUnAuditedInteraction($expectedArray, $unAuditedComplaint);
    }
}
