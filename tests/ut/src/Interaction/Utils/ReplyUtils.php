<?php
namespace Base\Interaction\Utils;

trait ReplyUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $reply
    ) {
        $this->assertEquals($expectedArray['reply_id'], $reply->getId());

        $this->assertEquals($expectedArray['content'], $reply->getContent());
        $this->imagesEquals($expectedArray, $reply);
        $this->assertEquals($expectedArray['admissibility'], $reply->getAdmissibility());
        $this->assertEquals($expectedArray['crew_id'], $reply->getCrew()->getId());
        $this->assertEquals($expectedArray['accept_usergroup_id'], $reply->getAcceptUserGroup()->getId());
        $this->assertEquals($expectedArray['status'], $reply->getStatus());
        $this->assertEquals($expectedArray['create_time'], $reply->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $reply->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $reply->getStatusTime());
    }

    private function imagesEquals($expectedArray, $reply)
    {
        $images = array();

        if (is_string($expectedArray['images'])) {
            $images = json_decode($expectedArray['images'], true);
        }
        if (is_array($expectedArray['images'])) {
            $images = $expectedArray['images'];
        }

        $this->assertEquals($images, $reply->getImages());
    }
}
