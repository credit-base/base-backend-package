<?php
namespace Base\Interaction\CommandHandler\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\Praise;
use Base\Interaction\Command\Praise\AddPraiseCommand;

class AddPraiseCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddPraiseCommandHandler::class)
                                     ->setMethods(['getPraise', 'commentInteractionAddExecuteAction'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $praise = \Base\Interaction\Utils\PraiseMockFactory::generatePraise($id);

        $command = new AddPraiseCommand(
            $praise->getSubject(),
            $praise->getName(),
            $praise->getIdentify(),
            $praise->getContact(),
            $praise->getImages(),
            $praise->getType(),
            $praise->getTitle(),
            $praise->getContent(),
            $praise->getAcceptUserGroup()->getId(),
            $praise->getMember()->getId()
        );

        $praise = $this->prophesize(Praise::class);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'commentInteractionAddExecuteAction'
        )->willReturn($praise->reveal());

        $praise->setSubject(Argument::exact($command->subject))->shouldBeCalledTimes(1);
        $praise->add()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $praise->getId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
            ->method('getPraise')
            ->willReturn($praise->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
