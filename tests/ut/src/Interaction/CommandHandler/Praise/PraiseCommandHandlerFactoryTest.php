<?php
namespace Base\Interaction\CommandHandler\Praise;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Command\Praise\AddPraiseCommand;
use Base\Interaction\Command\Praise\AcceptPraiseCommand;
use Base\Interaction\Command\Praise\RevokePraiseCommand;
use Base\Interaction\Command\Praise\PublishPraiseCommand;
use Base\Interaction\Command\Praise\ResubmitPraiseCommand;
use Base\Interaction\Command\Praise\UnPublishPraiseCommand;

class PraiseCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new PraiseCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddPraiseCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddPraiseCommand(
                $this->faker->name(),
                $this->faker->name(),
                $this->faker->word(),
                $this->faker->word(),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->title(),
                $this->faker->title(),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Praise\AddPraiseCommandHandler',
            $commandHandler
        );
    }

    public function testAcceptPraiseCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AcceptPraiseCommand(
                $this->faker->title(),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Praise\AcceptPraiseCommandHandler',
            $commandHandler
        );
    }

    public function testRevokePraiseCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokePraiseCommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Praise\RevokePraiseCommandHandler',
            $commandHandler
        );
    }

    public function testResubmitPraiseCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResubmitPraiseCommand(
                $this->faker->title(),
                array($this->faker->word()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Praise\ResubmitPraiseCommandHandler',
            $commandHandler
        );
    }

    public function testPublishPraiseCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new PublishPraiseCommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Praise\PublishPraiseCommandHandler',
            $commandHandler
        );
    }

    public function testUnPublishPraiseCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new UnPublishPraiseCommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Praise\UnPublishPraiseCommandHandler',
            $commandHandler
        );
    }
}
