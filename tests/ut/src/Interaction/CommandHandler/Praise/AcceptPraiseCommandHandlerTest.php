<?php
namespace Base\Interaction\CommandHandler\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\UnAuditedPraise;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Command\Praise\AcceptPraiseCommand;

class AcceptPraiseCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptPraiseCommandHandler::class)
                                     ->setMethods([
                                            'fetchPraise',
                                            'commentInteractionAcceptExecuteAction',
                                            'getAcceptInteractionService'
                                        ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $praise = \Base\Interaction\Utils\PraiseMockFactory::generatePraise($id);

        $command = new AcceptPraiseCommand(
            $praise->getReply()->getContent(),
            $praise->getReply()->getImages(),
            $praise->getReply()->getAdmissibility(),
            $praise->getReply()->getCrew()->getId(),
            $id
        );

        $this->commandHandler->expects($this->exactly(1))->method('fetchPraise')->with($id)->willReturn($praise);

        $unAuditedPraise = $this->prophesize(UnAuditedPraise::class);
        $unAuditedPraise->setSubject(Argument::exact($praise->getSubject()))->shouldBeCalledTimes(1);
        $unAuditedPraise->getReply()->shouldBeCalledTimes(1)->willReturn($praise->getReply());

        $service = $this->prophesize(AcceptInteractionService::class);
        $service->accept()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'getAcceptInteractionService'
        )->willReturn($service->reveal());

        if ($result) {
            $unAuditedPraise->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
             ->method('commentInteractionAcceptExecuteAction')
             ->with($command, $praise)
             ->willReturn($unAuditedPraise->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
