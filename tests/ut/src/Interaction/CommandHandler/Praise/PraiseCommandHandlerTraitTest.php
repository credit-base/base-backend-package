<?php
namespace Base\Interaction\CommandHandler\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Repository\Praise\PraiseRepository;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

class PraiseCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockPraiseCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetPraise()
    {
        $trait = new MockPraiseCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\Praise',
            $trait->publicGetPraise()
        );
    }

    public function testGetUnAuditedInteraction()
    {
        $trait = new MockPraiseCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\UnAuditedPraise',
            $trait->publicGetUnAuditedInteraction()
        );
    }

    public function testGetPraiseRepository()
    {
        $trait = new MockPraiseCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Praise\PraiseRepository',
            $trait->publicGetPraiseRepository()
        );
    }
    
    public function testGetUnAuditedPraiseRepository()
    {
        $trait = new MockPraiseCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Praise\UnAuditedPraiseRepository',
            $trait->publicGetUnAuditedPraiseRepository()
        );
    }

    public function testFetchPraise()
    {
        $trait = $this->getMockBuilder(MockPraiseCommandHandlerTrait::class)
                 ->setMethods(['getPraiseRepository']) ->getMock();

        $id = 1;

        $praise = \Base\Interaction\Utils\PraiseMockFactory::generatePraise($id);

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($praise);

        $trait->expects($this->exactly(1))->method('getPraiseRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchPraise($id);
        $this->assertEquals($result, $praise);
    }

    public function testFetchUnAuditedPraise()
    {
        $trait = $this->getMockBuilder(MockPraiseCommandHandlerTrait::class)
                 ->setMethods(['getUnAuditedPraiseRepository']) ->getMock();
                 
        $id = 1;

        $unAuditedPraise = \Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise($id);

        $repository = $this->prophesize(UnAuditedPraiseRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditedPraise);

        $trait->expects($this->exactly(1))->method('getUnAuditedPraiseRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditedPraise($id);

        $this->assertEquals($result, $unAuditedPraise);
    }
}
