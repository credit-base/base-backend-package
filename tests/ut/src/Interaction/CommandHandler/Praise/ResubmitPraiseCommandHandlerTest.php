<?php
namespace Base\Interaction\CommandHandler\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\UnAuditedPraise;
use Base\Interaction\Command\Praise\ResubmitPraiseCommand;
use Base\Interaction\Service\ResubmitAcceptInteractionService;

class ResubmitPraiseCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitPraiseCommandHandler::class)
                                     ->setMethods([
                                            'fetchUnAuditedPraise',
                                            'interactionResubmitExecuteAction',
                                            'getResubmitAcceptInteractionService'
                                        ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $unAuditedPraise = \Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise($id);
        $reply = $unAuditedPraise->getReply();

        $command = new ResubmitPraiseCommand(
            $unAuditedPraise->getReply()->getContent(),
            $unAuditedPraise->getReply()->getImages(),
            $unAuditedPraise->getReply()->getAdmissibility(),
            $unAuditedPraise->getReply()->getCrew()->getId(),
            $id
        );

        $unAuditedPraise = $this->prophesize(UnAuditedPraise::class);
        $unAuditedPraise->getReply()->shouldBeCalledTimes(1)->willReturn($reply);

        $service = $this->prophesize(ResubmitAcceptInteractionService::class);
        $service->resubmitAccept()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'getResubmitAcceptInteractionService'
        )->willReturn($service->reveal());

        $this->commandHandler->expects($this->exactly(1))->method(
            'fetchUnAuditedPraise'
        )->with($id)->willReturn($unAuditedPraise->reveal());

        $this->commandHandler->expects($this->exactly(1))
             ->method('interactionResubmitExecuteAction')
             ->willReturn($unAuditedPraise->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
