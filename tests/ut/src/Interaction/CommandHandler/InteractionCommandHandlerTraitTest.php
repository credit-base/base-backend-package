<?php
namespace Base\Interaction\CommandHandler;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Repository\UserGroupRepository;

use Base\Member\Repository\MemberRepository;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\Appeal;
use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Command\ReplyCommand;
use Base\Interaction\Command\AcceptCommand;
use Base\Interaction\Command\InteractionCommand;
use Base\Interaction\Repository\Reply\ReplyRepository;
use Base\Interaction\Command\CommentInteractionCommand;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class InteractionCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockInteractionCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetMemberRepository()
    {
        $this->assertInstanceOf(
            'Base\Member\Repository\MemberRepository',
            $this->trait->publicGetMemberRepository()
        );
    }

    public function testFetchMember()
    {
        $trait = $this->getMockBuilder(MockInteractionCommandHandlerTrait::class)
                      ->setMethods(['getMemberRepository']) ->getMock();

        $id = 1;

        $member = \Base\Member\Utils\MockFactory::generateMember($id);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($member);

        $trait->expects($this->exactly(1))->method('getMemberRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchMember($id);

        $this->assertEquals($result, $member);
    }

    public function testGetUserGroupRepository()
    {
        $this->assertInstanceOf(
            'Base\UserGroup\Repository\UserGroupRepository',
            $this->trait->publicGetUserGroupRepository()
        );
    }

    public function testFetchUserGroup()
    {
        $trait = $this->getMockBuilder(MockInteractionCommandHandlerTrait::class)
                      ->setMethods(['getUserGroupRepository']) ->getMock();

        $id = 1;

        $userGroup = \Base\UserGroup\Utils\MockFactory::generateUserGroup($id);

        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($userGroup);

        $trait->expects($this->exactly(1))->method('getUserGroupRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchUserGroup($id);

        $this->assertEquals($result, $userGroup);
    }

    public function testGetReplyRepository()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Reply\ReplyRepository',
            $this->trait->publicGetReplyRepository()
        );
    }

    public function testFetchReply()
    {
        $trait = $this->getMockBuilder(MockInteractionCommandHandlerTrait::class)
                      ->setMethods(['getReplyRepository']) ->getMock();

        $id = 1;

        $reply = \Base\Interaction\Utils\ReplyMockFactory::generateReply($id);

        $repository = $this->prophesize(ReplyRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($reply);

        $trait->expects($this->exactly(1))->method('getReplyRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchReply($id);

        $this->assertEquals($result, $reply);
    }

    public function testGetCrewRepository()
    {
        $this->assertInstanceOf(
            'Base\Crew\Repository\CrewRepository',
            $this->trait->publicGetCrewRepository()
        );
    }

    public function testFetchCrew()
    {
        $trait = $this->getMockBuilder(MockInteractionCommandHandlerTrait::class)
                      ->setMethods(['getCrewRepository']) ->getMock();

        $id = 1;

        $crew = \Base\Crew\Utils\MockFactory::generateCrew($id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($crew);

        $trait->expects($this->exactly(1))->method('getCrewRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchCrew($id);

        $this->assertEquals($result, $crew);
    }

    public function testGetReply()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Model\Reply',
            $this->trait->publicGetReply()
        );
    }

    public function testGetAcceptInteractionService()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Service\AcceptInteractionService',
            $this->trait->publicGetAcceptInteractionService(new UnAuditedAppeal(), new Appeal(), new Reply())
        );
    }

    public function testGetResubmitAcceptInteractionService()
    {
        $this->assertInstanceOf(
            'Base\Interaction\Service\ResubmitAcceptInteractionService',
            $this->trait->publicGetResubmitAcceptInteractionService(new UnAuditedAppeal(), new Reply())
        );
    }

    public function testInteractionAddExecuteAction()
    {
        $trait = $this->getMockBuilder(MockInteractionCommandHandlerTrait::class)
                      ->setMethods(['fetchMember', 'fetchUserGroup']) ->getMock();
        $id = 1;

        $feedback = \Base\Interaction\Utils\FeedbackMockFactory::generateFeedback($id);

        $command = new InteractionCommand(
            $feedback->getTitle(),
            $feedback->getContent(),
            $feedback->getAcceptUserGroup()->getId(),
            $feedback->getMember()->getId()
        );

        $trait->expects($this->exactly(1))->method(
            'fetchMember'
        )->with($feedback->getMember()->getId())->willReturn($feedback->getMember());

        $trait->expects($this->exactly(1))->method(
            'fetchUserGroup'
        )->with($feedback->getAcceptUserGroup()->getId())->willReturn($feedback->getAcceptUserGroup());
                         
        $result = $trait->publicInteractionAddExecuteAction($command, $feedback);

        $this->assertEquals($result, $feedback);
    }

    public function testCommentInteractionAddExecuteAction()
    {
        $trait = $this->getMockBuilder(MockInteractionCommandHandlerTrait::class)
                      ->setMethods(['interactionAddExecuteAction']) ->getMock();
        $id = 1;

        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);

        $command = new CommentInteractionCommand(
            $appeal->getName(),
            $appeal->getIdentify(),
            $appeal->getContact(),
            $appeal->getImages(),
            $appeal->getType(),
            $appeal->getTitle(),
            $appeal->getContent(),
            $appeal->getAcceptUserGroup()->getId(),
            $appeal->getMember()->getId()
        );

        $trait->expects($this->exactly(1))->method('interactionAddExecuteAction')->willReturn($appeal);

        $result = $trait->publicCommentInteractionAddExecuteAction($command, $appeal);

        $this->assertEquals($result, $appeal);
    }

    public function testInteractionAcceptExecuteAction()
    {
        $trait = $this->getMockBuilder(MockInteractionCommandHandlerTrait::class)
                      ->setMethods(['fetchCrew', 'getReply', 'getUnAuditedInteraction']) ->getMock();
        $id = 1;

        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback($id);

        $command = new AcceptCommand(
            $unAuditedFeedback->getReply()->getContent(),
            $unAuditedFeedback->getReply()->getImages(),
            $unAuditedFeedback->getReply()->getAdmissibility(),
            $unAuditedFeedback->getReply()->getCrew()->getId(),
            $unAuditedFeedback->getId()
        );

        $trait->expects($this->exactly(1))->method(
            'fetchCrew'
        )->with($command->crew)->willReturn($unAuditedFeedback->getReply()->getCrew());

        $trait->expects($this->exactly(1))->method('getReply')->willReturn($unAuditedFeedback->getReply());
        $trait->expects($this->exactly(1))->method('getUnAuditedInteraction')->willReturn($unAuditedFeedback);
                         
        $result = $trait->publicInteractionAcceptExecuteAction($command, $unAuditedFeedback);

        $this->assertEquals($result, $unAuditedFeedback);
    }

    public function testCommentInteractionAcceptExecuteAction()
    {
        $trait = $this->getMockBuilder(MockInteractionCommandHandlerTrait::class)
                      ->setMethods(['interactionAcceptExecuteAction']) ->getMock();
        $id = 1;

        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal($id);

        $command = new AcceptCommand(
            $unAuditedAppeal->getReply()->getContent(),
            $unAuditedAppeal->getReply()->getImages(),
            $unAuditedAppeal->getReply()->getAdmissibility(),
            $unAuditedAppeal->getReply()->getCrew()->getId(),
            $unAuditedAppeal->getId()
        );

        $trait->expects($this->exactly(1))->method('interactionAcceptExecuteAction')->willReturn($unAuditedAppeal);

        $result = $trait->publicCommentInteractionAcceptExecuteAction($command, $unAuditedAppeal);

        $this->assertEquals($result, $unAuditedAppeal);
    }

    public function testInteractionResubmitExecuteAction()
    {
        $trait = $this->getMockBuilder(MockInteractionCommandHandlerTrait::class)
                      ->setMethods(['fetchCrew']) ->getMock();
        $id = 1;

        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback($id);

        $command = new ReplyCommand(
            $unAuditedFeedback->getReply()->getContent(),
            $unAuditedFeedback->getReply()->getImages(),
            $unAuditedFeedback->getReply()->getAdmissibility(),
            $unAuditedFeedback->getReply()->getCrew()->getId(),
            $unAuditedFeedback->getId()
        );

        $trait->expects($this->exactly(1))->method(
            'fetchCrew'
        )->with($command->crew)->willReturn($unAuditedFeedback->getReply()->getCrew());
                         
        $result = $trait->publicInteractionResubmitExecuteAction($command, $unAuditedFeedback);

        $this->assertEquals($result, $unAuditedFeedback);
    }
}
