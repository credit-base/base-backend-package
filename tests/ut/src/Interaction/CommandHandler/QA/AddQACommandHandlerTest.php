<?php
namespace Base\Interaction\CommandHandler\QA;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\QA;
use Base\Interaction\Command\QA\AddQACommand;

class AddQACommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddQACommandHandler::class)
                                     ->setMethods(['getQA', 'interactionAddExecuteAction'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $qaInteraction = \Base\Interaction\Utils\QAMockFactory::generateQA($id);

        $command = new AddQACommand(
            $qaInteraction->getTitle(),
            $qaInteraction->getContent(),
            $qaInteraction->getAcceptUserGroup()->getId(),
            $qaInteraction->getMember()->getId()
        );

        $qaInteraction = $this->prophesize(QA::class);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'interactionAddExecuteAction'
        )->willReturn($qaInteraction->reveal());

        $qaInteraction->add()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $qaInteraction->getId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
            ->method('getQA')
            ->willReturn($qaInteraction->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
