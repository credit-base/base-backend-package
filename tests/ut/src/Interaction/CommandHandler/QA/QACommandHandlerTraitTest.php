<?php
namespace Base\Interaction\CommandHandler\QA;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Repository\QA\QARepository;
use Base\Interaction\Repository\QA\UnAuditedQARepository;

class QACommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockQACommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetQA()
    {
        $trait = new MockQACommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\QA',
            $trait->publicGetQA()
        );
    }

    public function testGetUnAuditedInteraction()
    {
        $trait = new MockQACommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\UnAuditedQA',
            $trait->publicGetUnAuditedInteraction()
        );
    }

    public function testGetQARepository()
    {
        $trait = new MockQACommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\QA\QARepository',
            $trait->publicGetQARepository()
        );
    }
    
    public function testGetUnAuditedQARepository()
    {
        $trait = new MockQACommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\QA\UnAuditedQARepository',
            $trait->publicGetUnAuditedQARepository()
        );
    }

    public function testFetchQA()
    {
        $trait = $this->getMockBuilder(MockQACommandHandlerTrait::class)
                 ->setMethods(['getQARepository']) ->getMock();

        $id = 1;

        $qaInteraction = \Base\Interaction\Utils\QAMockFactory::generateQA($id);

        $repository = $this->prophesize(QARepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($qaInteraction);

        $trait->expects($this->exactly(1))->method('getQARepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchQA($id);
        $this->assertEquals($result, $qaInteraction);
    }

    public function testFetchUnAuditedQA()
    {
        $trait = $this->getMockBuilder(MockQACommandHandlerTrait::class)
                 ->setMethods(['getUnAuditedQARepository']) ->getMock();
                 
        $id = 1;

        $unAuditedQA = \Base\Interaction\Utils\QAMockFactory::generateUnAuditedQA($id);

        $repository = $this->prophesize(UnAuditedQARepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditedQA);

        $trait->expects($this->exactly(1))->method('getUnAuditedQARepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditedQA($id);

        $this->assertEquals($result, $unAuditedQA);
    }
}
