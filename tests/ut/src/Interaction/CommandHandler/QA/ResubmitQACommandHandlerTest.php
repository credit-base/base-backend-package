<?php
namespace Base\Interaction\CommandHandler\QA;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\UnAuditedQA;
use Base\Interaction\Command\QA\ResubmitQACommand;
use Base\Interaction\Service\ResubmitAcceptInteractionService;

class ResubmitQACommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitQACommandHandler::class)
                                     ->setMethods([
                                            'fetchUnAuditedQA',
                                            'interactionResubmitExecuteAction',
                                            'getResubmitAcceptInteractionService'
                                        ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $unAuditedQA = \Base\Interaction\Utils\QAMockFactory::generateUnAuditedQA($id);
        $reply = $unAuditedQA->getReply();

        $command = new ResubmitQACommand(
            $unAuditedQA->getReply()->getContent(),
            $unAuditedQA->getReply()->getImages(),
            $unAuditedQA->getReply()->getAdmissibility(),
            $unAuditedQA->getReply()->getCrew()->getId(),
            $id
        );

        $unAuditedQA = $this->prophesize(UnAuditedQA::class);
        $unAuditedQA->getReply()->shouldBeCalledTimes(1)->willReturn($reply);

        $service = $this->prophesize(ResubmitAcceptInteractionService::class);
        $service->resubmitAccept()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'getResubmitAcceptInteractionService'
        )->willReturn($service->reveal());

        $this->commandHandler->expects($this->exactly(1))->method(
            'fetchUnAuditedQA'
        )->with($id)->willReturn($unAuditedQA->reveal());

        $this->commandHandler->expects($this->exactly(1))
             ->method('interactionResubmitExecuteAction')
             ->willReturn($unAuditedQA->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
