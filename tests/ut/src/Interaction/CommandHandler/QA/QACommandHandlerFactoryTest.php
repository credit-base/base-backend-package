<?php
namespace Base\Interaction\CommandHandler\QA;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Command\QA\AddQACommand;
use Base\Interaction\Command\QA\AcceptQACommand;
use Base\Interaction\Command\QA\RevokeQACommand;
use Base\Interaction\Command\QA\PublishQACommand;
use Base\Interaction\Command\QA\ResubmitQACommand;
use Base\Interaction\Command\QA\UnPublishQACommand;

class QACommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new QACommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddQACommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddQACommand(
                $this->faker->title(),
                $this->faker->title(),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(2)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\QA\AddQACommandHandler',
            $commandHandler
        );
    }

    public function testAcceptQACommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AcceptQACommand(
                $this->faker->title(),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\QA\AcceptQACommandHandler',
            $commandHandler
        );
    }

    public function testRevokeQACommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeQACommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\QA\RevokeQACommandHandler',
            $commandHandler
        );
    }

    public function testResubmitQACommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResubmitQACommand(
                $this->faker->title(),
                array($this->faker->word()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\QA\ResubmitQACommandHandler',
            $commandHandler
        );
    }

    public function testPublishQACommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new PublishQACommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\QA\PublishQACommandHandler',
            $commandHandler
        );
    }

    public function testUnPublishQACommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new UnPublishQACommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\QA\UnPublishQACommandHandler',
            $commandHandler
        );
    }
}
