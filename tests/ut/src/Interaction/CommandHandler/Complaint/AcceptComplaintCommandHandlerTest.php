<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\UnAuditedComplaint;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Command\Complaint\AcceptComplaintCommand;

class AcceptComplaintCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptComplaintCommandHandler::class)
                                     ->setMethods([
                                            'fetchComplaint',
                                            'commentInteractionAcceptExecuteAction',
                                            'getAcceptInteractionService'
                                        ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $complaint = \Base\Interaction\Utils\ComplaintMockFactory::generateComplaint($id);

        $command = new AcceptComplaintCommand(
            $complaint->getReply()->getContent(),
            $complaint->getReply()->getImages(),
            $complaint->getReply()->getAdmissibility(),
            $complaint->getReply()->getCrew()->getId(),
            $id
        );

        $this->commandHandler->expects($this->exactly(1))->method('fetchComplaint')->with($id)->willReturn($complaint);

        $unAuditedComplaint = $this->prophesize(UnAuditedComplaint::class);
        $unAuditedComplaint->setSubject(Argument::exact($complaint->getSubject()))->shouldBeCalledTimes(1);
        $unAuditedComplaint->getReply()->shouldBeCalledTimes(1)->willReturn($complaint->getReply());

        $service = $this->prophesize(AcceptInteractionService::class);
        $service->accept()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'getAcceptInteractionService'
        )->willReturn($service->reveal());

        if ($result) {
            $unAuditedComplaint->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
             ->method('commentInteractionAcceptExecuteAction')
             ->with($command, $complaint)
             ->willReturn($unAuditedComplaint->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
