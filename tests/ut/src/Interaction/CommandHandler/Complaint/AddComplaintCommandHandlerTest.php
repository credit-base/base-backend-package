<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\Complaint;
use Base\Interaction\Command\Complaint\AddComplaintCommand;

class AddComplaintCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddComplaintCommandHandler::class)
                                     ->setMethods(['getComplaint', 'commentInteractionAddExecuteAction'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $complaint = \Base\Interaction\Utils\ComplaintMockFactory::generateComplaint($id);

        $command = new AddComplaintCommand(
            $complaint->getSubject(),
            $complaint->getName(),
            $complaint->getIdentify(),
            $complaint->getContact(),
            $complaint->getImages(),
            $complaint->getType(),
            $complaint->getTitle(),
            $complaint->getContent(),
            $complaint->getAcceptUserGroup()->getId(),
            $complaint->getMember()->getId()
        );

        $complaint = $this->prophesize(Complaint::class);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'commentInteractionAddExecuteAction'
        )->willReturn($complaint->reveal());

        $complaint->setSubject(Argument::exact($command->subject))->shouldBeCalledTimes(1);
        $complaint->add()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $complaint->getId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
            ->method('getComplaint')
            ->willReturn($complaint->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
