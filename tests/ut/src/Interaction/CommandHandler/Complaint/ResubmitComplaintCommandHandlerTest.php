<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\UnAuditedComplaint;
use Base\Interaction\Command\Complaint\ResubmitComplaintCommand;
use Base\Interaction\Service\ResubmitAcceptInteractionService;

class ResubmitComplaintCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitComplaintCommandHandler::class)
                                     ->setMethods([
                                            'fetchUnAuditedComplaint',
                                            'interactionResubmitExecuteAction',
                                            'getResubmitAcceptInteractionService'
                                        ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $unAuditedComplaint = \Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint($id);
        $reply = $unAuditedComplaint->getReply();

        $command = new ResubmitComplaintCommand(
            $unAuditedComplaint->getReply()->getContent(),
            $unAuditedComplaint->getReply()->getImages(),
            $unAuditedComplaint->getReply()->getAdmissibility(),
            $unAuditedComplaint->getReply()->getCrew()->getId(),
            $id
        );

        $unAuditedComplaint = $this->prophesize(UnAuditedComplaint::class);
        $unAuditedComplaint->getReply()->shouldBeCalledTimes(1)->willReturn($reply);

        $service = $this->prophesize(ResubmitAcceptInteractionService::class);
        $service->resubmitAccept()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'getResubmitAcceptInteractionService'
        )->willReturn($service->reveal());

        $this->commandHandler->expects($this->exactly(1))->method(
            'fetchUnAuditedComplaint'
        )->with($id)->willReturn($unAuditedComplaint->reveal());

        $this->commandHandler->expects($this->exactly(1))
             ->method('interactionResubmitExecuteAction')
             ->willReturn($unAuditedComplaint->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
