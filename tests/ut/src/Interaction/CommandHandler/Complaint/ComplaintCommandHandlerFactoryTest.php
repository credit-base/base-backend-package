<?php
namespace Base\Interaction\CommandHandler\Complaint;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Command\Complaint\AddComplaintCommand;
use Base\Interaction\Command\Complaint\AcceptComplaintCommand;
use Base\Interaction\Command\Complaint\RevokeComplaintCommand;
use Base\Interaction\Command\Complaint\ResubmitComplaintCommand;

class ComplaintCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new ComplaintCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddComplaintCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddComplaintCommand(
                $this->faker->name(),
                $this->faker->name(),
                $this->faker->word(),
                $this->faker->word(),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->title(),
                $this->faker->title(),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Complaint\AddComplaintCommandHandler',
            $commandHandler
        );
    }

    public function testAcceptComplaintCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AcceptComplaintCommand(
                $this->faker->title(),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Complaint\AcceptComplaintCommandHandler',
            $commandHandler
        );
    }

    public function testRevokeComplaintCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeComplaintCommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Complaint\RevokeComplaintCommandHandler',
            $commandHandler
        );
    }

    public function testResubmitComplaintCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResubmitComplaintCommand(
                $this->faker->title(),
                array($this->faker->word()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Complaint\ResubmitComplaintCommandHandler',
            $commandHandler
        );
    }
}
