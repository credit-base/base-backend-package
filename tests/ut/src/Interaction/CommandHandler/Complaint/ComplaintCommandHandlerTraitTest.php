<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Repository\Complaint\ComplaintRepository;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

class ComplaintCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockComplaintCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetComplaint()
    {
        $trait = new MockComplaintCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\Complaint',
            $trait->publicGetComplaint()
        );
    }

    public function testGetUnAuditedInteraction()
    {
        $trait = new MockComplaintCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\UnAuditedComplaint',
            $trait->publicGetUnAuditedInteraction()
        );
    }

    public function testGetComplaintRepository()
    {
        $trait = new MockComplaintCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Complaint\ComplaintRepository',
            $trait->publicGetComplaintRepository()
        );
    }
    
    public function testGetUnAuditedComplaintRepository()
    {
        $trait = new MockComplaintCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository',
            $trait->publicGetUnAuditedComplaintRepository()
        );
    }

    public function testFetchComplaint()
    {
        $trait = $this->getMockBuilder(MockComplaintCommandHandlerTrait::class)
                 ->setMethods(['getComplaintRepository']) ->getMock();

        $id = 1;

        $complaint = \Base\Interaction\Utils\ComplaintMockFactory::generateComplaint($id);

        $repository = $this->prophesize(ComplaintRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($complaint);

        $trait->expects($this->exactly(1))->method('getComplaintRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchComplaint($id);
        $this->assertEquals($result, $complaint);
    }

    public function testFetchUnAuditedComplaint()
    {
        $trait = $this->getMockBuilder(MockComplaintCommandHandlerTrait::class)
                 ->setMethods(['getUnAuditedComplaintRepository']) ->getMock();
                 
        $id = 1;

        $unAuditedComplaint = \Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint($id);

        $repository = $this->prophesize(UnAuditedComplaintRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditedComplaint);

        $trait->expects($this->exactly(1))->method(
            'getUnAuditedComplaintRepository'
        )->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditedComplaint($id);

        $this->assertEquals($result, $unAuditedComplaint);
    }
}
