<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\UnAuditedFeedback;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Command\Feedback\AcceptFeedbackCommand;

class AcceptFeedbackCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptFeedbackCommandHandler::class)
                                     ->setMethods([
                                            'fetchFeedback',
                                            'interactionAcceptExecuteAction',
                                            'getAcceptInteractionService'
                                        ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $feedback = \Base\Interaction\Utils\FeedbackMockFactory::generateFeedback($id);

        $command = new AcceptFeedbackCommand(
            $feedback->getReply()->getContent(),
            $feedback->getReply()->getImages(),
            $feedback->getReply()->getAdmissibility(),
            $feedback->getReply()->getCrew()->getId(),
            $id
        );

        $this->commandHandler->expects($this->exactly(1))->method('fetchFeedback')->with($id)->willReturn($feedback);

        $unAuditedFeedback = $this->prophesize(UnAuditedFeedback::class);
        $unAuditedFeedback->getReply()->shouldBeCalledTimes(1)->willReturn($feedback->getReply());

        $service = $this->prophesize(AcceptInteractionService::class);
        $service->accept()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'getAcceptInteractionService'
        )->willReturn($service->reveal());

        if ($result) {
            $unAuditedFeedback->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
             ->method('interactionAcceptExecuteAction')
             ->with($command, $feedback)
             ->willReturn($unAuditedFeedback->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
