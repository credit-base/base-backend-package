<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Repository\Feedback\FeedbackRepository;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

class FeedbackCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockFeedbackCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetFeedback()
    {
        $trait = new MockFeedbackCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\Feedback',
            $trait->publicGetFeedback()
        );
    }

    public function testGetUnAuditedInteraction()
    {
        $trait = new MockFeedbackCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\UnAuditedFeedback',
            $trait->publicGetUnAuditedInteraction()
        );
    }

    public function testGetFeedbackRepository()
    {
        $trait = new MockFeedbackCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Feedback\FeedbackRepository',
            $trait->publicGetFeedbackRepository()
        );
    }
    
    public function testGetUnAuditedFeedbackRepository()
    {
        $trait = new MockFeedbackCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository',
            $trait->publicGetUnAuditedFeedbackRepository()
        );
    }

    public function testFetchFeedback()
    {
        $trait = $this->getMockBuilder(MockFeedbackCommandHandlerTrait::class)
                 ->setMethods(['getFeedbackRepository']) ->getMock();

        $id = 1;

        $feedback = \Base\Interaction\Utils\FeedbackMockFactory::generateFeedback($id);

        $repository = $this->prophesize(FeedbackRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($feedback);

        $trait->expects($this->exactly(1))->method('getFeedbackRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchFeedback($id);
        $this->assertEquals($result, $feedback);
    }

    public function testFetchUnAuditedFeedback()
    {
        $trait = $this->getMockBuilder(MockFeedbackCommandHandlerTrait::class)
                 ->setMethods(['getUnAuditedFeedbackRepository']) ->getMock();
                 
        $id = 1;

        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback($id);

        $repository = $this->prophesize(UnAuditedFeedbackRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditedFeedback);

        $trait->expects($this->exactly(1))->method(
            'getUnAuditedFeedbackRepository'
        )->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditedFeedback($id);

        $this->assertEquals($result, $unAuditedFeedback);
    }
}
