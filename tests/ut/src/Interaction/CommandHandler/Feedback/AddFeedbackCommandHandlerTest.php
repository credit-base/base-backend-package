<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\Feedback;
use Base\Interaction\Command\Feedback\AddFeedbackCommand;

class AddFeedbackCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddFeedbackCommandHandler::class)
                                     ->setMethods(['getFeedback', 'interactionAddExecuteAction'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $feedback = \Base\Interaction\Utils\FeedbackMockFactory::generateFeedback($id);

        $command = new AddFeedbackCommand(
            $feedback->getTitle(),
            $feedback->getContent(),
            $feedback->getAcceptUserGroup()->getId(),
            $feedback->getMember()->getId()
        );

        $feedback = $this->prophesize(Feedback::class);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'interactionAddExecuteAction'
        )->willReturn($feedback->reveal());

        $feedback->add()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $feedback->getId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
            ->method('getFeedback')
            ->willReturn($feedback->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
