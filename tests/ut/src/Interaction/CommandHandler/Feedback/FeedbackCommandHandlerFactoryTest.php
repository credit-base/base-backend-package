<?php
namespace Base\Interaction\CommandHandler\Feedback;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Command\Feedback\AddFeedbackCommand;
use Base\Interaction\Command\Feedback\AcceptFeedbackCommand;
use Base\Interaction\Command\Feedback\RevokeFeedbackCommand;
use Base\Interaction\Command\Feedback\ResubmitFeedbackCommand;

class FeedbackCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new FeedbackCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddFeedbackCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddFeedbackCommand(
                $this->faker->title(),
                $this->faker->title(),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Feedback\AddFeedbackCommandHandler',
            $commandHandler
        );
    }

    public function testAcceptFeedbackCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AcceptFeedbackCommand(
                $this->faker->title(),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Feedback\AcceptFeedbackCommandHandler',
            $commandHandler
        );
    }

    public function testRevokeFeedbackCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeFeedbackCommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Feedback\RevokeFeedbackCommandHandler',
            $commandHandler
        );
    }

    public function testResubmitFeedbackCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResubmitFeedbackCommand(
                $this->faker->title(),
                array($this->faker->word()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Feedback\ResubmitFeedbackCommandHandler',
            $commandHandler
        );
    }
}
