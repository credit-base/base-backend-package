<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\UnAuditedFeedback;
use Base\Interaction\Command\Feedback\ResubmitFeedbackCommand;
use Base\Interaction\Service\ResubmitAcceptInteractionService;

class ResubmitFeedbackCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitFeedbackCommandHandler::class)
                                     ->setMethods([
                                            'fetchUnAuditedFeedback',
                                            'interactionResubmitExecuteAction',
                                            'getResubmitAcceptInteractionService'
                                        ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback($id);
        $reply = $unAuditedFeedback->getReply();

        $command = new ResubmitFeedbackCommand(
            $unAuditedFeedback->getReply()->getContent(),
            $unAuditedFeedback->getReply()->getImages(),
            $unAuditedFeedback->getReply()->getAdmissibility(),
            $unAuditedFeedback->getReply()->getCrew()->getId(),
            $id
        );

        $unAuditedFeedback = $this->prophesize(UnAuditedFeedback::class);
        $unAuditedFeedback->getReply()->shouldBeCalledTimes(1)->willReturn($reply);

        $service = $this->prophesize(ResubmitAcceptInteractionService::class);
        $service->resubmitAccept()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'getResubmitAcceptInteractionService'
        )->willReturn($service->reveal());

        $this->commandHandler->expects($this->exactly(1))->method(
            'fetchUnAuditedFeedback'
        )->with($id)->willReturn($unAuditedFeedback->reveal());

        $this->commandHandler->expects($this->exactly(1))
             ->method('interactionResubmitExecuteAction')
             ->willReturn($unAuditedFeedback->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
