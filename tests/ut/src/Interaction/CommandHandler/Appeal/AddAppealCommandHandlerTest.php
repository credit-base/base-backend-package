<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Command\Appeal\AddAppealCommand;

class AddAppealCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddAppealCommandHandler::class)
                                     ->setMethods(['getAppeal', 'commentInteractionAddExecuteAction'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);

        $command = new AddAppealCommand(
            $appeal->getCertificates(),
            $appeal->getName(),
            $appeal->getIdentify(),
            $appeal->getContact(),
            $appeal->getImages(),
            $appeal->getType(),
            $appeal->getTitle(),
            $appeal->getContent(),
            $appeal->getAcceptUserGroup()->getId(),
            $appeal->getMember()->getId()
        );

        $appeal = $this->prophesize(Appeal::class);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'commentInteractionAddExecuteAction'
        )->willReturn($appeal->reveal());

        $appeal->setCertificates(Argument::exact($command->certificates))->shouldBeCalledTimes(1);
        $appeal->add()->shouldBeCalledTimes(1)->willReturn($result);

        if ($result) {
            $appeal->getId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
            ->method('getAppeal')
            ->willReturn($appeal->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
