<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Command\Appeal\ResubmitAppealCommand;
use Base\Interaction\Service\ResubmitAcceptInteractionService;

class ResubmitAppealCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitAppealCommandHandler::class)
                                     ->setMethods([
                                            'fetchUnAuditedAppeal',
                                            'interactionResubmitExecuteAction',
                                            'getResubmitAcceptInteractionService'
                                        ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal($id);
        $reply = $unAuditedAppeal->getReply();

        $command = new ResubmitAppealCommand(
            $unAuditedAppeal->getReply()->getContent(),
            $unAuditedAppeal->getReply()->getImages(),
            $unAuditedAppeal->getReply()->getAdmissibility(),
            $unAuditedAppeal->getReply()->getCrew()->getId(),
            $id
        );

        $unAuditedAppeal = $this->prophesize(UnAuditedAppeal::class);
        $unAuditedAppeal->getReply()->shouldBeCalledTimes(1)->willReturn($reply);

        $service = $this->prophesize(ResubmitAcceptInteractionService::class);
        $service->resubmitAccept()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'getResubmitAcceptInteractionService'
        )->willReturn($service->reveal());

        $this->commandHandler->expects($this->exactly(1))->method(
            'fetchUnAuditedAppeal'
        )->with($id)->willReturn($unAuditedAppeal->reveal());

        $this->commandHandler->expects($this->exactly(1))
             ->method('interactionResubmitExecuteAction')
             ->willReturn($unAuditedAppeal->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
