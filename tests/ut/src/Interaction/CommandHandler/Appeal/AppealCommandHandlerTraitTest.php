<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Interaction\Repository\Appeal\AppealRepository;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

class AppealCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockAppealCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetAppeal()
    {
        $trait = new MockAppealCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\Appeal',
            $trait->publicGetAppeal()
        );
    }

    public function testGetUnAuditedInteraction()
    {
        $trait = new MockAppealCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Model\UnAuditedAppeal',
            $trait->publicGetUnAuditedInteraction()
        );
    }

    public function testGetAppealRepository()
    {
        $trait = new MockAppealCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Appeal\AppealRepository',
            $trait->publicGetAppealRepository()
        );
    }
    
    public function testGetUnAuditedAppealRepository()
    {
        $trait = new MockAppealCommandHandlerTrait();
        $this->assertInstanceOf(
            'Base\Interaction\Repository\Appeal\UnAuditedAppealRepository',
            $trait->publicGetUnAuditedAppealRepository()
        );
    }

    public function testFetchAppeal()
    {
        $trait = $this->getMockBuilder(MockAppealCommandHandlerTrait::class)
                 ->setMethods(['getAppealRepository']) ->getMock();

        $id = 1;

        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);

        $repository = $this->prophesize(AppealRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($appeal);

        $trait->expects($this->exactly(1))->method('getAppealRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchAppeal($id);
        $this->assertEquals($result, $appeal);
    }

    public function testFetchUnAuditedAppeal()
    {
        $trait = $this->getMockBuilder(MockAppealCommandHandlerTrait::class)
                 ->setMethods(['getUnAuditedAppealRepository']) ->getMock();
                 
        $id = 1;

        $unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal($id);

        $repository = $this->prophesize(UnAuditedAppealRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditedAppeal);

        $trait->expects($this->exactly(1))->method('getUnAuditedAppealRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditedAppeal($id);

        $this->assertEquals($result, $unAuditedAppeal);
    }
}
