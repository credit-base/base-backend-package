<?php
namespace Base\Interaction\CommandHandler\Appeal;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Command\Appeal\AddAppealCommand;
use Base\Interaction\Command\Appeal\AcceptAppealCommand;
use Base\Interaction\Command\Appeal\RevokeAppealCommand;
use Base\Interaction\Command\Appeal\ResubmitAppealCommand;

class AppealCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new AppealCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddAppealCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddAppealCommand(
                array($this->faker->title()),
                $this->faker->name(),
                $this->faker->word(),
                $this->faker->word(),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->title(),
                $this->faker->title(),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Appeal\AddAppealCommandHandler',
            $commandHandler
        );
    }

    public function testAcceptAppealCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AcceptAppealCommand(
                $this->faker->title(),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Appeal\AcceptAppealCommandHandler',
            $commandHandler
        );
    }

    public function testRevokeAppealCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeAppealCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Appeal\RevokeAppealCommandHandler',
            $commandHandler
        );
    }

    public function testResubmitAppealCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResubmitAppealCommand(
                $this->faker->title(),
                array($this->faker->word()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Base\Interaction\CommandHandler\Appeal\ResubmitAppealCommandHandler',
            $commandHandler
        );
    }
}
