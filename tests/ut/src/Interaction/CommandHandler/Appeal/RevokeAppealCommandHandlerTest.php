<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Command\Appeal\RevokeAppealCommand;

class RevokeAppealCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(RevokeAppealCommandHandler::class)
                                     ->setMethods(['fetchAppeal'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $command = new RevokeAppealCommand(1);

        $appeal = $this->prophesize(Appeal::class);
        $appeal->revoke()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method('fetchAppeal')->willReturn($appeal->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
