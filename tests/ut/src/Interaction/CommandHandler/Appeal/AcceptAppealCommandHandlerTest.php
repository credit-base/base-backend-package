<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Command\Appeal\AcceptAppealCommand;

class AcceptAppealCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptAppealCommandHandler::class)
                                     ->setMethods([
                                            'fetchAppeal',
                                            'commentInteractionAcceptExecuteAction',
                                            'getAcceptInteractionService'
                                        ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $id = 1;

        $appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal($id);

        $command = new AcceptAppealCommand(
            $appeal->getReply()->getContent(),
            $appeal->getReply()->getImages(),
            $appeal->getReply()->getAdmissibility(),
            $appeal->getReply()->getCrew()->getId(),
            $id
        );

        $this->commandHandler->expects($this->exactly(1))->method('fetchAppeal')->with($id)->willReturn($appeal);

        $unAuditedAppeal = $this->prophesize(UnAuditedAppeal::class);
        $unAuditedAppeal->setCertificates(Argument::exact($appeal->getCertificates()))->shouldBeCalledTimes(1);
        $unAuditedAppeal->getReply()->shouldBeCalledTimes(1)->willReturn($appeal->getReply());

        $service = $this->prophesize(AcceptInteractionService::class);
        $service->accept()->shouldBeCalledTimes(1)->willReturn($result);
    
        $this->commandHandler->expects($this->exactly(1))->method(
            'getAcceptInteractionService'
        )->willReturn($service->reveal());

        if ($result) {
            $unAuditedAppeal->getApplyId()->shouldBeCalledTimes(1);
        }

        $this->commandHandler->expects($this->exactly(1))
             ->method('commentInteractionAcceptExecuteAction')
             ->with($command, $appeal)
             ->willReturn($unAuditedAppeal->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
