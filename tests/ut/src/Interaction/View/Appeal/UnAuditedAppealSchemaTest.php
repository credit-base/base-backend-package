<?php
namespace Base\Interaction\View\Appeal;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class UnAuditedAppealSchemaTest extends TestCase
{
    private $unAuditedAppealSchema;

    private $unAuditedAppeal;

    public function setUp()
    {
        $this->unAuditedAppealSchema = new UnAuditedAppealSchema(new Factory());

        $this->unAuditedAppeal = \Base\Interaction\Utils\AppealMockFactory::generateUnAuditedAppeal(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->unAuditedAppealSchema);
        unset($this->unAuditedAppeal);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->unAuditedAppealSchema);
    }

    public function testGetId()
    {
        $result = $this->unAuditedAppealSchema->getId($this->unAuditedAppeal);

        $this->assertEquals($result, $this->unAuditedAppeal->getApplyId());
    }

    public function testGetAttributes()
    {
        $result = $this->unAuditedAppealSchema->getAttributes($this->unAuditedAppeal);


        $this->assertEquals($result['title'], $this->unAuditedAppeal->getTitle());
        $this->assertEquals($result['content'], $this->unAuditedAppeal->getContent());
        $this->assertEquals($result['name'], $this->unAuditedAppeal->getName());
        $this->assertEquals($result['identify'], $this->unAuditedAppeal->getIdentify());
        $this->assertEquals($result['appealType'], $this->unAuditedAppeal->getType());
        $this->assertEquals($result['contact'], $this->unAuditedAppeal->getContact());
        $this->assertEquals($result['images'], $this->unAuditedAppeal->getImages());
        $this->assertEquals($result['acceptStatus'], $this->unAuditedAppeal->getAcceptStatus());
        $this->assertEquals($result['certificates'], $this->unAuditedAppeal->getCertificates());
        $this->assertEquals($result['status'], $this->unAuditedAppeal->getStatus());
        $this->assertEquals($result['createTime'], $this->unAuditedAppeal->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->unAuditedAppeal->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->unAuditedAppeal->getStatusTime());
        $this->assertEquals(
            $result['applyInfoCategory'],
            $this->unAuditedAppeal->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals(
            $result['applyInfoType'],
            $this->unAuditedAppeal->getApplyInfoCategory()->getType()
        );
        $this->assertEquals($result['applyStatus'], $this->unAuditedAppeal->getApplyStatus());
        $this->assertEquals($result['rejectReason'], $this->unAuditedAppeal->getRejectReason());
        $this->assertEquals($result['operationType'], $this->unAuditedAppeal->getOperationType());
        $this->assertEquals($result['appealId'], $this->unAuditedAppeal->getId());
    }

    public function testGetRelationships()
    {
        $result = $this->unAuditedAppealSchema->getRelationships($this->unAuditedAppeal, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->unAuditedAppeal->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->unAuditedAppeal->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->unAuditedAppeal->getReply()]);
        $this->assertEquals($result['relation'], ['data' => $this->unAuditedAppeal->getRelation()]);
        $this->assertEquals($result['applyCrew'], ['data' => $this->unAuditedAppeal->getApplyCrew()]);
        $this->assertEquals($result['applyUserGroup'], ['data' => $this->unAuditedAppeal->getApplyUserGroup()]);
    }
}
