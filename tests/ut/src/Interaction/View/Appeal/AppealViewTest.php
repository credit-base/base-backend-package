<?php
namespace Base\Interaction\View\Appeal;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Appeal;

class AppealViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $appeal = new AppealView(new Appeal());
        $this->assertInstanceof('Base\Common\View\CommonView', $appeal);
    }
}
