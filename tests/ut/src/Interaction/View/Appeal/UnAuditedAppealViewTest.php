<?php
namespace Base\Interaction\View\Appeal;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedAppeal;

class UnAuditedAppealViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $appeal = new UnAuditedAppealView(new UnAuditedAppeal());
        $this->assertInstanceof('Base\Common\View\CommonView', $appeal);
    }
}
