<?php
namespace Base\Interaction\View\Appeal;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class AppealSchemaTest extends TestCase
{
    private $appealSchema;

    private $appeal;

    public function setUp()
    {
        $this->appealSchema = new AppealSchema(new Factory());

        $this->appeal = \Base\Interaction\Utils\AppealMockFactory::generateAppeal(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->appealSchema);
        unset($this->appeal);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->appealSchema);
    }

    public function testGetId()
    {
        $result = $this->appealSchema->getId($this->appeal);

        $this->assertEquals($result, $this->appeal->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->appealSchema->getAttributes($this->appeal);

        $this->assertEquals($result['title'], $this->appeal->getTitle());
        $this->assertEquals($result['content'], $this->appeal->getContent());
        $this->assertEquals($result['name'], $this->appeal->getName());
        $this->assertEquals($result['identify'], $this->appeal->getIdentify());
        $this->assertEquals($result['appealType'], $this->appeal->getType());
        $this->assertEquals($result['contact'], $this->appeal->getContact());
        $this->assertEquals($result['images'], $this->appeal->getImages());
        $this->assertEquals($result['acceptStatus'], $this->appeal->getAcceptStatus());
        $this->assertEquals($result['certificates'], $this->appeal->getCertificates());
        $this->assertEquals($result['status'], $this->appeal->getStatus());
        $this->assertEquals($result['createTime'], $this->appeal->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->appeal->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->appeal->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->appealSchema->getRelationships($this->appeal, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->appeal->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->appeal->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->appeal->getReply()]);
    }
}
