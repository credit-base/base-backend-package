<?php
namespace Base\Interaction\View\Reply;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class ReplySchemaTest extends TestCase
{
    private $replySchema;

    private $reply;

    public function setUp()
    {
        $this->replySchema = new ReplySchema(new Factory());

        $this->reply = \Base\Interaction\Utils\ReplyMockFactory::generateReply(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->replySchema);
        unset($this->reply);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->replySchema);
    }

    public function testGetId()
    {
        $result = $this->replySchema->getId($this->reply);

        $this->assertEquals($result, $this->reply->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->replySchema->getAttributes($this->reply);

        $this->assertEquals($result['content'], $this->reply->getContent());
        $this->assertEquals($result['images'], $this->reply->getImages());
        $this->assertEquals($result['admissibility'], $this->reply->getAdmissibility());
        $this->assertEquals($result['status'], $this->reply->getStatus());
        $this->assertEquals($result['createTime'], $this->reply->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->reply->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->reply->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->replySchema->getRelationships($this->reply, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->reply->getAcceptUserGroup()]);
        $this->assertEquals($result['crew'], ['data' => $this->reply->getCrew()]);
    }
}
