<?php
namespace Base\Interaction\View\Reply;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Reply;

class ReplyViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $praise = new ReplyView(new Reply());
        $this->assertInstanceof('Base\Common\View\CommonView', $praise);
    }
}
