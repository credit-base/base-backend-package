<?php
namespace Base\Interaction\View\QA;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class QASchemaTest extends TestCase
{
    private $qaInteractionSchema;

    private $qaInteraction;

    public function setUp()
    {
        $this->qaInteractionSchema = new QASchema(new Factory());

        $this->qaInteraction = \Base\Interaction\Utils\QAMockFactory::generateQA(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->qaInteractionSchema);
        unset($this->qaInteraction);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->qaInteractionSchema);
    }

    public function testGetId()
    {
        $result = $this->qaInteractionSchema->getId($this->qaInteraction);

        $this->assertEquals($result, $this->qaInteraction->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->qaInteractionSchema->getAttributes($this->qaInteraction);

        $this->assertEquals($result['title'], $this->qaInteraction->getTitle());
        $this->assertEquals($result['content'], $this->qaInteraction->getContent());
        $this->assertEquals($result['acceptStatus'], $this->qaInteraction->getAcceptStatus());
        $this->assertEquals($result['status'], $this->qaInteraction->getStatus());
        $this->assertEquals($result['createTime'], $this->qaInteraction->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->qaInteraction->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->qaInteraction->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->qaInteractionSchema->getRelationships($this->qaInteraction, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->qaInteraction->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->qaInteraction->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->qaInteraction->getReply()]);
    }
}
