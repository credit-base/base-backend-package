<?php
namespace Base\Interaction\View\QA;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class UnAuditedQASchemaTest extends TestCase
{
    private $unAuditedQASchema;

    private $unAuditedQA;

    public function setUp()
    {
        $this->unAuditedQASchema = new UnAuditedQASchema(new Factory());

        $this->unAuditedQA = \Base\Interaction\Utils\QAMockFactory::generateUnAuditedQA(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->unAuditedQASchema);
        unset($this->unAuditedQA);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->unAuditedQASchema);
    }

    public function testGetId()
    {
        $result = $this->unAuditedQASchema->getId($this->unAuditedQA);

        $this->assertEquals($result, $this->unAuditedQA->getApplyId());
    }

    public function testGetAttributes()
    {
        $result = $this->unAuditedQASchema->getAttributes($this->unAuditedQA);


        $this->assertEquals($result['title'], $this->unAuditedQA->getTitle());
        $this->assertEquals($result['content'], $this->unAuditedQA->getContent());
        $this->assertEquals($result['acceptStatus'], $this->unAuditedQA->getAcceptStatus());
        $this->assertEquals($result['status'], $this->unAuditedQA->getStatus());
        $this->assertEquals($result['createTime'], $this->unAuditedQA->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->unAuditedQA->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->unAuditedQA->getStatusTime());
        $this->assertEquals(
            $result['applyInfoCategory'],
            $this->unAuditedQA->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals(
            $result['applyInfoType'],
            $this->unAuditedQA->getApplyInfoCategory()->getType()
        );
        $this->assertEquals($result['applyStatus'], $this->unAuditedQA->getApplyStatus());
        $this->assertEquals($result['rejectReason'], $this->unAuditedQA->getRejectReason());
        $this->assertEquals($result['operationType'], $this->unAuditedQA->getOperationType());
        $this->assertEquals($result['qaId'], $this->unAuditedQA->getId());
    }

    public function testGetRelationships()
    {
        $result = $this->unAuditedQASchema->getRelationships($this->unAuditedQA, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->unAuditedQA->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->unAuditedQA->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->unAuditedQA->getReply()]);
        $this->assertEquals($result['relation'], ['data' => $this->unAuditedQA->getRelation()]);
        $this->assertEquals($result['applyCrew'], ['data' => $this->unAuditedQA->getApplyCrew()]);
        $this->assertEquals($result['applyUserGroup'], ['data' => $this->unAuditedQA->getApplyUserGroup()]);
    }
}
