<?php
namespace Base\Interaction\View\QA;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\QA;

class QAViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $qaInteraction = new QAView(new QA());
        $this->assertInstanceof('Base\Common\View\CommonView', $qaInteraction);
    }
}
