<?php
namespace Base\Interaction\View\QA;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedQA;

class UnAuditedQAViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $qaInteraction = new UnAuditedQAView(new UnAuditedQA());
        $this->assertInstanceof('Base\Common\View\CommonView', $qaInteraction);
    }
}
