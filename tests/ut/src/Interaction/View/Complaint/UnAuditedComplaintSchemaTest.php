<?php
namespace Base\Interaction\View\Complaint;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class UnAuditedComplaintSchemaTest extends TestCase
{
    private $unAuditedComplaintSchema;

    private $unAuditedComplaint;

    public function setUp()
    {
        $this->unAuditedComplaintSchema = new UnAuditedComplaintSchema(new Factory());

        $this->unAuditedComplaint = \Base\Interaction\Utils\ComplaintMockFactory::generateUnAuditedComplaint(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->unAuditedComplaintSchema);
        unset($this->unAuditedComplaint);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->unAuditedComplaintSchema);
    }

    public function testGetId()
    {
        $result = $this->unAuditedComplaintSchema->getId($this->unAuditedComplaint);

        $this->assertEquals($result, $this->unAuditedComplaint->getApplyId());
    }

    public function testGetAttributes()
    {
        $result = $this->unAuditedComplaintSchema->getAttributes($this->unAuditedComplaint);


        $this->assertEquals($result['title'], $this->unAuditedComplaint->getTitle());
        $this->assertEquals($result['content'], $this->unAuditedComplaint->getContent());
        $this->assertEquals($result['name'], $this->unAuditedComplaint->getName());
        $this->assertEquals($result['identify'], $this->unAuditedComplaint->getIdentify());
        $this->assertEquals($result['complaintType'], $this->unAuditedComplaint->getType());
        $this->assertEquals($result['contact'], $this->unAuditedComplaint->getContact());
        $this->assertEquals($result['images'], $this->unAuditedComplaint->getImages());
        $this->assertEquals($result['acceptStatus'], $this->unAuditedComplaint->getAcceptStatus());
        $this->assertEquals($result['subject'], $this->unAuditedComplaint->getSubject());
        $this->assertEquals($result['status'], $this->unAuditedComplaint->getStatus());
        $this->assertEquals($result['createTime'], $this->unAuditedComplaint->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->unAuditedComplaint->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->unAuditedComplaint->getStatusTime());
        $this->assertEquals(
            $result['applyInfoCategory'],
            $this->unAuditedComplaint->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals(
            $result['applyInfoType'],
            $this->unAuditedComplaint->getApplyInfoCategory()->getType()
        );
        $this->assertEquals($result['applyStatus'], $this->unAuditedComplaint->getApplyStatus());
        $this->assertEquals($result['rejectReason'], $this->unAuditedComplaint->getRejectReason());
        $this->assertEquals($result['operationType'], $this->unAuditedComplaint->getOperationType());
        $this->assertEquals($result['complaintId'], $this->unAuditedComplaint->getId());
    }

    public function testGetRelationships()
    {
        $result = $this->unAuditedComplaintSchema->getRelationships($this->unAuditedComplaint, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->unAuditedComplaint->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->unAuditedComplaint->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->unAuditedComplaint->getReply()]);
        $this->assertEquals($result['relation'], ['data' => $this->unAuditedComplaint->getRelation()]);
        $this->assertEquals($result['applyCrew'], ['data' => $this->unAuditedComplaint->getApplyCrew()]);
        $this->assertEquals($result['applyUserGroup'], ['data' => $this->unAuditedComplaint->getApplyUserGroup()]);
    }
}
