<?php
namespace Base\Interaction\View\Complaint;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class ComplaintSchemaTest extends TestCase
{
    private $complaintSchema;

    private $complaint;

    public function setUp()
    {
        $this->complaintSchema = new ComplaintSchema(new Factory());

        $this->complaint = \Base\Interaction\Utils\ComplaintMockFactory::generateComplaint(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->complaintSchema);
        unset($this->complaint);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->complaintSchema);
    }

    public function testGetId()
    {
        $result = $this->complaintSchema->getId($this->complaint);

        $this->assertEquals($result, $this->complaint->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->complaintSchema->getAttributes($this->complaint);

        $this->assertEquals($result['title'], $this->complaint->getTitle());
        $this->assertEquals($result['content'], $this->complaint->getContent());
        $this->assertEquals($result['name'], $this->complaint->getName());
        $this->assertEquals($result['identify'], $this->complaint->getIdentify());
        $this->assertEquals($result['complaintType'], $this->complaint->getType());
        $this->assertEquals($result['contact'], $this->complaint->getContact());
        $this->assertEquals($result['images'], $this->complaint->getImages());
        $this->assertEquals($result['acceptStatus'], $this->complaint->getAcceptStatus());
        $this->assertEquals($result['subject'], $this->complaint->getSubject());
        $this->assertEquals($result['status'], $this->complaint->getStatus());
        $this->assertEquals($result['createTime'], $this->complaint->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->complaint->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->complaint->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->complaintSchema->getRelationships($this->complaint, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->complaint->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->complaint->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->complaint->getReply()]);
    }
}
