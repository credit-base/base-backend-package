<?php
namespace Base\Interaction\View\Complaint;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedComplaint;

class UnAuditedComplaintViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $complaint = new UnAuditedComplaintView(new UnAuditedComplaint());
        $this->assertInstanceof('Base\Common\View\CommonView', $complaint);
    }
}
