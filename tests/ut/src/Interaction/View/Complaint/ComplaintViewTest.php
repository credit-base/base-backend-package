<?php
namespace Base\Interaction\View\Complaint;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Complaint;

class ComplaintViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $complaint = new ComplaintView(new Complaint());
        $this->assertInstanceof('Base\Common\View\CommonView', $complaint);
    }
}
