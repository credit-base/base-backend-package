<?php
namespace Base\Interaction\View\Feedback;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class FeedbackSchemaTest extends TestCase
{
    private $feedbackSchema;

    private $feedback;

    public function setUp()
    {
        $this->feedbackSchema = new FeedbackSchema(new Factory());

        $this->feedback = \Base\Interaction\Utils\FeedbackMockFactory::generateFeedback(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->feedbackSchema);
        unset($this->feedback);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->feedbackSchema);
    }

    public function testGetId()
    {
        $result = $this->feedbackSchema->getId($this->feedback);

        $this->assertEquals($result, $this->feedback->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->feedbackSchema->getAttributes($this->feedback);

        $this->assertEquals($result['title'], $this->feedback->getTitle());
        $this->assertEquals($result['content'], $this->feedback->getContent());
        $this->assertEquals($result['acceptStatus'], $this->feedback->getAcceptStatus());
        $this->assertEquals($result['status'], $this->feedback->getStatus());
        $this->assertEquals($result['createTime'], $this->feedback->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->feedback->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->feedback->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->feedbackSchema->getRelationships($this->feedback, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->feedback->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->feedback->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->feedback->getReply()]);
    }
}
