<?php
namespace Base\Interaction\View\Feedback;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class UnAuditedFeedbackSchemaTest extends TestCase
{
    private $unAuditedFeedbackSchema;

    private $unAuditedFeedback;

    public function setUp()
    {
        $this->unAuditedFeedbackSchema = new UnAuditedFeedbackSchema(new Factory());

        $this->unAuditedFeedback = \Base\Interaction\Utils\FeedbackMockFactory::generateUnAuditedFeedback(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->unAuditedFeedbackSchema);
        unset($this->unAuditedFeedback);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->unAuditedFeedbackSchema);
    }

    public function testGetId()
    {
        $result = $this->unAuditedFeedbackSchema->getId($this->unAuditedFeedback);

        $this->assertEquals($result, $this->unAuditedFeedback->getApplyId());
    }

    public function testGetAttributes()
    {
        $result = $this->unAuditedFeedbackSchema->getAttributes($this->unAuditedFeedback);


        $this->assertEquals($result['title'], $this->unAuditedFeedback->getTitle());
        $this->assertEquals($result['content'], $this->unAuditedFeedback->getContent());
        $this->assertEquals($result['acceptStatus'], $this->unAuditedFeedback->getAcceptStatus());
        $this->assertEquals($result['status'], $this->unAuditedFeedback->getStatus());
        $this->assertEquals($result['createTime'], $this->unAuditedFeedback->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->unAuditedFeedback->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->unAuditedFeedback->getStatusTime());
        $this->assertEquals(
            $result['applyInfoCategory'],
            $this->unAuditedFeedback->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals(
            $result['applyInfoType'],
            $this->unAuditedFeedback->getApplyInfoCategory()->getType()
        );
        $this->assertEquals($result['applyStatus'], $this->unAuditedFeedback->getApplyStatus());
        $this->assertEquals($result['rejectReason'], $this->unAuditedFeedback->getRejectReason());
        $this->assertEquals($result['operationType'], $this->unAuditedFeedback->getOperationType());
        $this->assertEquals($result['feedbackId'], $this->unAuditedFeedback->getId());
    }

    public function testGetRelationships()
    {
        $result = $this->unAuditedFeedbackSchema->getRelationships($this->unAuditedFeedback, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->unAuditedFeedback->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->unAuditedFeedback->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->unAuditedFeedback->getReply()]);
        $this->assertEquals($result['relation'], ['data' => $this->unAuditedFeedback->getRelation()]);
        $this->assertEquals($result['applyCrew'], ['data' => $this->unAuditedFeedback->getApplyCrew()]);
        $this->assertEquals($result['applyUserGroup'], ['data' => $this->unAuditedFeedback->getApplyUserGroup()]);
    }
}
