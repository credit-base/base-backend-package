<?php
namespace Base\Interaction\View\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Feedback;

class FeedbackViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $feedback = new FeedbackView(new Feedback());
        $this->assertInstanceof('Base\Common\View\CommonView', $feedback);
    }
}
