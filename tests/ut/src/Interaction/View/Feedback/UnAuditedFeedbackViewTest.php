<?php
namespace Base\Interaction\View\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedFeedback;

class UnAuditedFeedbackViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $feedback = new UnAuditedFeedbackView(new UnAuditedFeedback());
        $this->assertInstanceof('Base\Common\View\CommonView', $feedback);
    }
}
