<?php
namespace Base\Interaction\View\Praise;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class UnAuditedPraiseSchemaTest extends TestCase
{
    private $unAuditedPraiseSchema;

    private $unAuditedPraise;

    public function setUp()
    {
        $this->unAuditedPraiseSchema = new UnAuditedPraiseSchema(new Factory());

        $this->unAuditedPraise = \Base\Interaction\Utils\PraiseMockFactory::generateUnAuditedPraise(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->unAuditedPraiseSchema);
        unset($this->unAuditedPraise);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->unAuditedPraiseSchema);
    }

    public function testGetId()
    {
        $result = $this->unAuditedPraiseSchema->getId($this->unAuditedPraise);

        $this->assertEquals($result, $this->unAuditedPraise->getApplyId());
    }

    public function testGetAttributes()
    {
        $result = $this->unAuditedPraiseSchema->getAttributes($this->unAuditedPraise);


        $this->assertEquals($result['title'], $this->unAuditedPraise->getTitle());
        $this->assertEquals($result['content'], $this->unAuditedPraise->getContent());
        $this->assertEquals($result['name'], $this->unAuditedPraise->getName());
        $this->assertEquals($result['identify'], $this->unAuditedPraise->getIdentify());
        $this->assertEquals($result['praiseType'], $this->unAuditedPraise->getType());
        $this->assertEquals($result['contact'], $this->unAuditedPraise->getContact());
        $this->assertEquals($result['images'], $this->unAuditedPraise->getImages());
        $this->assertEquals($result['acceptStatus'], $this->unAuditedPraise->getAcceptStatus());
        $this->assertEquals($result['subject'], $this->unAuditedPraise->getSubject());
        $this->assertEquals($result['status'], $this->unAuditedPraise->getStatus());
        $this->assertEquals($result['createTime'], $this->unAuditedPraise->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->unAuditedPraise->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->unAuditedPraise->getStatusTime());
        $this->assertEquals(
            $result['applyInfoCategory'],
            $this->unAuditedPraise->getApplyInfoCategory()->getCategory()
        );
        $this->assertEquals(
            $result['applyInfoType'],
            $this->unAuditedPraise->getApplyInfoCategory()->getType()
        );
        $this->assertEquals($result['applyStatus'], $this->unAuditedPraise->getApplyStatus());
        $this->assertEquals($result['rejectReason'], $this->unAuditedPraise->getRejectReason());
        $this->assertEquals($result['operationType'], $this->unAuditedPraise->getOperationType());
        $this->assertEquals($result['praiseId'], $this->unAuditedPraise->getId());
    }

    public function testGetRelationships()
    {
        $result = $this->unAuditedPraiseSchema->getRelationships($this->unAuditedPraise, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->unAuditedPraise->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->unAuditedPraise->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->unAuditedPraise->getReply()]);
        $this->assertEquals($result['relation'], ['data' => $this->unAuditedPraise->getRelation()]);
        $this->assertEquals($result['applyCrew'], ['data' => $this->unAuditedPraise->getApplyCrew()]);
        $this->assertEquals($result['applyUserGroup'], ['data' => $this->unAuditedPraise->getApplyUserGroup()]);
    }
}
