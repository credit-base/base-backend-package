<?php
namespace Base\Interaction\View\Praise;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class PraiseSchemaTest extends TestCase
{
    private $praiseSchema;

    private $praise;

    public function setUp()
    {
        $this->praiseSchema = new PraiseSchema(new Factory());

        $this->praise = \Base\Interaction\Utils\PraiseMockFactory::generatePraise(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->praiseSchema);
        unset($this->praise);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->praiseSchema);
    }

    public function testGetId()
    {
        $result = $this->praiseSchema->getId($this->praise);

        $this->assertEquals($result, $this->praise->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->praiseSchema->getAttributes($this->praise);

        $this->assertEquals($result['title'], $this->praise->getTitle());
        $this->assertEquals($result['content'], $this->praise->getContent());
        $this->assertEquals($result['name'], $this->praise->getName());
        $this->assertEquals($result['identify'], $this->praise->getIdentify());
        $this->assertEquals($result['praiseType'], $this->praise->getType());
        $this->assertEquals($result['contact'], $this->praise->getContact());
        $this->assertEquals($result['images'], $this->praise->getImages());
        $this->assertEquals($result['acceptStatus'], $this->praise->getAcceptStatus());
        $this->assertEquals($result['subject'], $this->praise->getSubject());
        $this->assertEquals($result['status'], $this->praise->getStatus());
        $this->assertEquals($result['createTime'], $this->praise->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->praise->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->praise->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->praiseSchema->getRelationships($this->praise, 0, array());

        $this->assertEquals($result['acceptUserGroup'], ['data' => $this->praise->getAcceptUserGroup()]);
        $this->assertEquals($result['member'], ['data' => $this->praise->getMember()]);
        $this->assertEquals($result['reply'], ['data' => $this->praise->getReply()]);
    }
}
