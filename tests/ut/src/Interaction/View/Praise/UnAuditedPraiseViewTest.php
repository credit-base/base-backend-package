<?php
namespace Base\Interaction\View\Praise;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\UnAuditedPraise;

class UnAuditedPraiseViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $praise = new UnAuditedPraiseView(new UnAuditedPraise());
        $this->assertInstanceof('Base\Common\View\CommonView', $praise);
    }
}
