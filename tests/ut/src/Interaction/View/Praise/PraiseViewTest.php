<?php
namespace Base\Interaction\View\Praise;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\Praise;

class PraiseViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $praise = new PraiseView(new Praise());
        $this->assertInstanceof('Base\Common\View\CommonView', $praise);
    }
}
