<?php
namespace Base\Interaction\Command\Feedback;

use PHPUnit\Framework\TestCase;

class ResubmitFeedbackCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new ResubmitFeedbackCommand('content', array('images'), 1, 1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsReplyCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\ReplyCommand', $this->stub);
    }
}
