<?php
namespace Base\Interaction\Command\Feedback;

use PHPUnit\Framework\TestCase;

class AcceptFeedbackCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new AcceptFeedbackCommand('content', array('images'), 1, 1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsAcceptCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\AcceptCommand', $this->stub);
    }
}
