<?php
namespace Base\Interaction\Command\Feedback;

use PHPUnit\Framework\TestCase;

class RevokeFeedbackCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new RevokeFeedbackCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsRevokeCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\RevokeCommand', $this->stub);
    }
}
