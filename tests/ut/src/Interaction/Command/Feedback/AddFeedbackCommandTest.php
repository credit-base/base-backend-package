<?php
namespace Base\Interaction\Command\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Command\InteractionTrait;

class AddFeedbackCommandTest extends TestCase
{
    use InteractionTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'title' => $faker->title(),
            'content' => $faker->word(),
            'acceptUserGroup' => $faker->randomDigit(1),
            'member' => $faker->randomDigit(1),
            'id' => $faker->randomDigit(1)
        );

        $this->command = new AddFeedbackCommand(
            $this->fakerData['title'],
            $this->fakerData['content'],
            $this->fakerData['acceptUserGroup'],
            $this->fakerData['member'],
            $this->fakerData['id']
        );
    }

    public function testExtendsInteractionCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\InteractionCommand', $this->command);
    }
}
