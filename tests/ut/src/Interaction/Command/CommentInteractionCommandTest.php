<?php
namespace Base\Interaction\Command;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\IInteractionAble;

class CommentInteractionCommandTest extends TestCase
{
    use InteractionTrait, CommentInteractionTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'name' => $faker->name(),
            'identify' => $faker->word(),
            'contact' => $faker->word(),
            'images' => array($faker->word()),
            'type' => $faker->randomElement(IInteractionAble::TYPE),
            'title' => $faker->title(),
            'content' => $faker->word(),
            'acceptUserGroup' => $faker->randomDigit(2),
            'member' => $faker->randomDigit(1),
            'id' => $faker->randomDigit()
        );

        $this->command = new CommentInteractionCommand(
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['contact'],
            $this->fakerData['images'],
            $this->fakerData['type'],
            $this->fakerData['title'],
            $this->fakerData['content'],
            $this->fakerData['acceptUserGroup'],
            $this->fakerData['member'],
            $this->fakerData['id']
        );
    }

    public function testExtendsInteractionCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\InteractionCommand', $this->command);
    }
}
