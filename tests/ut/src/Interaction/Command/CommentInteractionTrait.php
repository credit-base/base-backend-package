<?php
namespace Base\Interaction\Command;

trait CommentInteractionTrait
{
    public function testNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }

    public function testIdentifyParameter()
    {
        $this->assertEquals($this->fakerData['identify'], $this->command->identify);
    }

    public function testContactParameter()
    {
        $this->assertEquals($this->fakerData['contact'], $this->command->contact);
    }

    public function testImagesParameter()
    {
        $this->assertEquals($this->fakerData['images'], $this->command->images);
    }

    public function testTypeParameter()
    {
        $this->assertEquals($this->fakerData['type'], $this->command->type);
    }
}
