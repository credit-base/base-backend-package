<?php
namespace Base\Interaction\Command;

trait InteractionTrait
{
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testTitleParameter()
    {
        $this->assertEquals($this->fakerData['title'], $this->command->title);
    }

    public function testContentParameter()
    {
        $this->assertEquals($this->fakerData['content'], $this->command->content);
    }

    public function testMemberParameter()
    {
        $this->assertEquals($this->fakerData['member'], $this->command->member);
    }

    public function testAcceptUserGroupParameter()
    {
        $this->assertEquals($this->fakerData['acceptUserGroup'], $this->command->acceptUserGroup);
    }
}
