<?php
namespace Base\Interaction\Command;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\IInteractionAble;

class ReplyCommandTest extends TestCase
{
    use ReplyTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'content' => $faker->word(),
            'images' => array($faker->word(), $faker->word()),
            'admissibility' => $faker->randomElement(IInteractionAble::ADMISSIBILITY),
            'crew' => $faker->randomDigit(1),
            'id' => $faker->randomDigit(1)
        );

        $this->command = new ReplyCommand(
            $this->fakerData['content'],
            $this->fakerData['images'],
            $this->fakerData['admissibility'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }
}
