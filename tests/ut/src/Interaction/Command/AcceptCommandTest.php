<?php
namespace Base\Interaction\Command;

use PHPUnit\Framework\TestCase;

use Base\Common\Model\IApproveAble;
use Base\Interaction\Model\IInteractionAble;

class AcceptCommandTest extends TestCase
{
    use ReplyTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'content' => $faker->word(),
            'images' => array($faker->word(), $faker->word()),
            'admissibility' => $faker->randomElement(IInteractionAble::ADMISSIBILITY),
            'crew' => $faker->randomDigit(1),
            'id' => $faker->randomDigit(1),
            'operationType' => IApproveAble::OPERATION_TYPE['ACCEPT']
        );

        $this->command = new AcceptCommand(
            $this->fakerData['content'],
            $this->fakerData['images'],
            $this->fakerData['admissibility'],
            $this->fakerData['crew'],
            $this->fakerData['id'],
            $this->fakerData['operationType']
        );
    }

    public function testExtendsReplyCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\ReplyCommand', $this->command);
    }
    
    public function testOperationTypeParameter()
    {
        $this->assertEquals($this->fakerData['operationType'], $this->command->operationType);
    }
}
