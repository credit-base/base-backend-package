<?php
namespace Base\Interaction\Command\Praise;

use PHPUnit\Framework\TestCase;

class ResubmitPraiseCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new ResubmitPraiseCommand('content', array('images'), 1, 1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsReplyCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\ReplyCommand', $this->stub);
    }
}
