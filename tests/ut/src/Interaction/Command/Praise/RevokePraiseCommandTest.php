<?php
namespace Base\Interaction\Command\Praise;

use PHPUnit\Framework\TestCase;

class RevokePraiseCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new RevokePraiseCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsRevokeCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\RevokeCommand', $this->stub);
    }
}
