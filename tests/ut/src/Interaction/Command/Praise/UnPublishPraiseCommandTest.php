<?php
namespace Base\Interaction\Command\Praise;

use PHPUnit\Framework\TestCase;

class UnPublishPraiseCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new UnPublishPraiseCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsUnPublishCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\UnPublishCommand', $this->stub);
    }
}
