<?php
namespace Base\Interaction\Command\Praise;

use PHPUnit\Framework\TestCase;

class AcceptPraiseCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new AcceptPraiseCommand('content', array('images'), 1, 1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsAcceptCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\AcceptCommand', $this->stub);
    }
}
