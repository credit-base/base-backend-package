<?php
namespace Base\Interaction\Command\Praise;

use PHPUnit\Framework\TestCase;

use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Command\InteractionTrait;
use Base\Interaction\Command\CommentInteractionTrait;

class AddPraiseCommandTest extends TestCase
{
    use InteractionTrait, CommentInteractionTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'subject' =>$faker->word(),
            'name' => $faker->name(),
            'identify' => $faker->bothify('##############????'),
            'contact' => $faker->phoneNumber(),
            'images' => array($faker->word(), $faker->word()),
            'type' => $faker->randomElement(IInteractionAble::TYPE),
            'title' => $faker->title(),
            'content' => $faker->word(),
            'acceptUserGroup' => $faker->randomDigit(1),
            'member' => $faker->randomDigit(1),
            'id' => $faker->randomDigit(2)
        );

        $this->command = new AddPraiseCommand(
            $this->fakerData['subject'],
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['contact'],
            $this->fakerData['images'],
            $this->fakerData['type'],
            $this->fakerData['title'],
            $this->fakerData['content'],
            $this->fakerData['acceptUserGroup'],
            $this->fakerData['member'],
            $this->fakerData['id']
        );
    }

    public function testExtendsCommentInteractionCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\CommentInteractionCommand', $this->command);
    }
    
    public function testSubjectParameter()
    {
        $this->assertEquals($this->fakerData['subject'], $this->command->subject);
    }
}
