<?php
namespace Base\Interaction\Command\Praise;

use PHPUnit\Framework\TestCase;

class PublishPraiseCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new PublishPraiseCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsPublishCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\PublishCommand', $this->stub);
    }
}
