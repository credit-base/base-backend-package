<?php
namespace Base\Interaction\Command\Complaint;

use PHPUnit\Framework\TestCase;

class RevokeComplaintCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new RevokeComplaintCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsRevokeCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\RevokeCommand', $this->stub);
    }
}
