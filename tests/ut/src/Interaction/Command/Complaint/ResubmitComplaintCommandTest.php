<?php
namespace Base\Interaction\Command\Complaint;

use PHPUnit\Framework\TestCase;

class ResubmitComplaintCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new ResubmitComplaintCommand('content', array('images'), 1, 1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsReplyCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\ReplyCommand', $this->stub);
    }
}
