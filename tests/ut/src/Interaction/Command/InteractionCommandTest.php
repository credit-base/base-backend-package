<?php
namespace Base\Interaction\Command;

use PHPUnit\Framework\TestCase;

class InteractionCommandTest extends TestCase
{
    use InteractionTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'title' => $faker->title(),
            'content' => $faker->word(),
            'acceptUserGroup' => $faker->randomDigit(2),
            'member' => $faker->randomDigit(1),
            'id' => $faker->randomDigit()
        );

        $this->command = new InteractionCommand(
            $this->fakerData['title'],
            $this->fakerData['content'],
            $this->fakerData['acceptUserGroup'],
            $this->fakerData['member'],
            $this->fakerData['id']
        );
    }
}
