<?php
namespace Base\Interaction\Command\Appeal;

use PHPUnit\Framework\TestCase;

class ResubmitAppealCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new ResubmitAppealCommand('content', array('images'), 1, 1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsReplyCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\ReplyCommand', $this->stub);
    }
}
