<?php
namespace Base\Interaction\Command\Appeal;

use PHPUnit\Framework\TestCase;

class AcceptAppealCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new AcceptAppealCommand('content', array('images'), 1, 1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsAcceptCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\AcceptCommand', $this->stub);
    }
}
