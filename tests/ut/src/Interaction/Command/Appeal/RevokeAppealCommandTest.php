<?php
namespace Base\Interaction\Command\Appeal;

use PHPUnit\Framework\TestCase;

class RevokeAppealCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new RevokeAppealCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsRevokeCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\RevokeCommand', $this->stub);
    }
}
