<?php
namespace Base\Interaction\Command;

trait ReplyTrait
{
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testContentParameter()
    {
        $this->assertEquals($this->fakerData['content'], $this->command->content);
    }

    public function testImagesParameter()
    {
        $this->assertEquals($this->fakerData['images'], $this->command->images);
    }

    public function testAdmissibilityParameter()
    {
        $this->assertEquals($this->fakerData['admissibility'], $this->command->admissibility);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }
}
