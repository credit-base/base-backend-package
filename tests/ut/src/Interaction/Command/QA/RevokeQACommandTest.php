<?php
namespace Base\Interaction\Command\QA;

use PHPUnit\Framework\TestCase;

class RevokeQACommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new RevokeQACommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsRevokeCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\RevokeCommand', $this->stub);
    }
}
