<?php
namespace Base\Interaction\Command\QA;

use PHPUnit\Framework\TestCase;

class AcceptQACommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new AcceptQACommand('content', array('images'), 1, 1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsAcceptCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\AcceptCommand', $this->stub);
    }
}
