<?php
namespace Base\Interaction\Command\QA;

use PHPUnit\Framework\TestCase;

class PublishQACommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new PublishQACommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsPublishCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\PublishCommand', $this->stub);
    }
}
