<?php
namespace Base\Interaction\Command\QA;

use PHPUnit\Framework\TestCase;

class UnPublishQACommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new UnPublishQACommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsUnPublishCommand()
    {
        $this->assertInstanceof('Base\Interaction\Command\UnPublishCommand', $this->stub);
    }
}
