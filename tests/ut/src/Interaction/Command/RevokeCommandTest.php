<?php
namespace Base\Interaction\Command;

use PHPUnit\Framework\TestCase;

class RevokeCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'id' => $faker->randomDigit()
        );

        $this->command = new RevokeCommand(
            $this->fakerData['id']
        );
    }

    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}
