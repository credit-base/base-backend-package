<?php
namespace Base\Interaction\Service;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\Reply;

class MockResubmitAcceptInteractionService extends ResubmitAcceptInteractionService
{
    public function getUnAuditedInteraction() : IApplyFormAble
    {
        return parent::getUnAuditedInteraction();
    }

    public function getReply() : Reply
    {
        return parent::getReply();
    }
}
