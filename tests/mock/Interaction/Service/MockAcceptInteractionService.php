<?php
namespace Base\Interaction\Service;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\IInteractionAble;

class MockAcceptInteractionService extends AcceptInteractionService
{
    public function getUnAuditedInteraction() : IApplyFormAble
    {
        return parent::getUnAuditedInteraction();
    }

    public function getInteraction() : IInteractionAble
    {
        return parent::getInteraction();
    }

    public function getReply() : Reply
    {
        return parent::getReply();
    }
}
