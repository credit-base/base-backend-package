<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\QA\IQAAdapter;

class MockQA extends QA
{
    public function getRepository() : IQAAdapter
    {
        return parent::getRepository();
    }
}
