<?php
namespace Base\Interaction\Model;

use Base\Interaction\Translator\ComplaintDbTranslator;

class MockUnAuditedComplaint extends UnAuditedComplaint
{
    public function getComplaintDbTranslator() : ComplaintDbTranslator
    {
        return parent::getComplaintDbTranslator();
    }

    public function applyInfo() : array
    {
        return parent::applyInfo();
    }
}
