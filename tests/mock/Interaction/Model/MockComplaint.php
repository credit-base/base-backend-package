<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\Complaint\IComplaintAdapter;

class MockComplaint extends Complaint
{
    public function getRepository() : IComplaintAdapter
    {
        return parent::getRepository();
    }
}
