<?php
namespace Base\Interaction\Model;

use Base\Interaction\Translator\FeedbackDbTranslator;

class MockUnAuditedFeedback extends UnAuditedFeedback
{
    public function getFeedbackDbTranslator() : FeedbackDbTranslator
    {
        return parent::getFeedbackDbTranslator();
    }

    public function applyInfo() : array
    {
        return parent::applyInfo();
    }
}
