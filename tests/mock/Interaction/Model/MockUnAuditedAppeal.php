<?php
namespace Base\Interaction\Model;

use Base\Interaction\Translator\AppealDbTranslator;

class MockUnAuditedAppeal extends UnAuditedAppeal
{
    public function getAppealDbTranslator() : AppealDbTranslator
    {
        return parent::getAppealDbTranslator();
    }

    public function applyInfo() : array
    {
        return parent::applyInfo();
    }
}
