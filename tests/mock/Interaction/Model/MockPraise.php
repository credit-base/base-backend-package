<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\Praise\IPraiseAdapter;

class MockPraise extends Praise
{
    public function getRepository() : IPraiseAdapter
    {
        return parent::getRepository();
    }
}
