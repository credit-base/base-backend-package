<?php
namespace Base\Interaction\Model;

class MockNullInteractionTrait
{
    use NullInteractionTrait;

    public function publicResourceNotExist() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        return false;
    }
}
