<?php
namespace Base\Interaction\Model;

use Base\Interaction\Translator\QADbTranslator;

class MockUnAuditedQA extends UnAuditedQA
{
    public function getQADbTranslator() : QADbTranslator
    {
        return parent::getQADbTranslator();
    }

    public function applyInfo() : array
    {
        return parent::applyInfo();
    }
}
