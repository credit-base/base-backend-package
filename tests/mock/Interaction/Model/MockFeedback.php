<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\Feedback\IFeedbackAdapter;

class MockFeedback extends Feedback
{
    public function getRepository() : IFeedbackAdapter
    {
        return parent::getRepository();
    }
}
