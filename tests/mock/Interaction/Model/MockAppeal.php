<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\Appeal\IAppealAdapter;

class MockAppeal extends Appeal
{
    public function getRepository() : IAppealAdapter
    {
        return parent::getRepository();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function updateStatus(int $status) : bool
    {
        return parent::updateStatus($status);
    }
}
