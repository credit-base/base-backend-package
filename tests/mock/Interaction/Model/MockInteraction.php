<?php
namespace Base\Interaction\Model;

class MockInteraction extends Interaction
{
    public function getRepository()
    {
    }

    public function isMemberNull() : bool
    {
        return parent::isMemberNull();
    }

    public function isAcceptUserGroupNull() : bool
    {
        return parent::isAcceptUserGroupNull();
    }

    public function validate() : bool
    {
        return parent::validate();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function updateStatus(int $status) : bool
    {
        return parent::updateStatus($status);
    }
}
