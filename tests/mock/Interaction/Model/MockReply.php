<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\Reply\IReplyAdapter;

class MockReply extends Reply
{
    public function getRepository() : IReplyAdapter
    {
        return parent::getRepository();
    }

    public function isCrewNull() : bool
    {
        return parent::isCrewNull();
    }

    public function isAcceptUserGroupNull() : bool
    {
        return parent::isAcceptUserGroupNull();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }
}
