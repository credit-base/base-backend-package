<?php
namespace Base\Interaction\Model;

use Base\Interaction\Translator\PraiseDbTranslator;

class MockUnAuditedPraise extends UnAuditedPraise
{
    public function getPraiseDbTranslator() : PraiseDbTranslator
    {
        return parent::getPraiseDbTranslator();
    }

    public function applyInfo() : array
    {
        return parent::applyInfo();
    }
}
