<?php
namespace Base\Interaction\Controller\Appeal;

use Marmot\Interfaces\ICommand;
use Base\ApplyForm\Model\IApplyFormAble;

class MockAppealResubmitController extends AppealResubmitController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }

    public function getAcceptCommand(array $data) : ICommand
    {
        return parent::getAcceptCommand($data);
    }
}
