<?php
namespace Base\Interaction\Controller\Appeal;

use Marmot\Interfaces\IView;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

class MockUnAuditedAppealFetchController extends UnAuditedAppealFetchController
{
    public function getRepository() : UnAuditedAppealRepository
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
