<?php
namespace Base\Interaction\Controller\Appeal;

use Base\ApplyForm\Model\IApplyFormAble;

class MockAppealApproveController extends AppealApproveController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }
}
