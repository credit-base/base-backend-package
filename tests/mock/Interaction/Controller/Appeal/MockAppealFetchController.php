<?php
namespace Base\Interaction\Controller\Appeal;

use Marmot\Interfaces\IView;
use Base\Interaction\Adapter\Appeal\IAppealAdapter;

class MockAppealFetchController extends AppealFetchController
{
    public function getRepository() : IAppealAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
