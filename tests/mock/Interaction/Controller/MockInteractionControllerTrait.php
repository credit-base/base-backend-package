<?php
namespace Base\Interaction\Controller;

use Marmot\Framework\Classes\Controller;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Interaction\WidgetRule\InteractionWidgetRule;

class MockInteractionControllerTrait extends Controller
{
    use InteractionControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getInteractionWidgetRulePublic() : InteractionWidgetRule
    {
        return $this->getInteractionWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }
    
    public function validateCommonScenarioPublic(
        $title,
        $content,
        $member,
        $acceptUserGroup
    ) {
        return $this->validateCommonScenario(
            $title,
            $content,
            $member,
            $acceptUserGroup
        );
    }
    
    public function validateCommentCommonScenarioPublic(
        $name,
        $identify,
        $type,
        $contact,
        $images
    ) {
        return $this->validateCommentCommonScenario(
            $name,
            $identify,
            $type,
            $contact,
            $images
        );
    }

    public function validateComplaintAddScenarioPublic($subject)
    {
        return $this->validateComplaintAddScenario($subject);
    }

    public function validatePraiseAddScenarioPublic($subject)
    {
        return $this->validatePraiseAddScenario($subject);
    }

    public function validateAppealAddScenarioPublic($certificates)
    {
        return $this->validateAppealAddScenario($certificates);
    }

    public function validateAcceptScenarioPublic(
        $replyContent,
        $replyImages,
        $admissibility,
        $crew
    ) {
        return $this->validateAcceptScenario(
            $replyContent,
            $replyImages,
            $admissibility,
            $crew
        );
    }
}
