<?php
namespace Base\Interaction\Controller;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Command\Appeal\AcceptAppealCommand;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;
use Base\Interaction\CommandHandler\Appeal\AppealCommandHandlerFactory;

class MockInteractionAcceptControllerTrait extends Controller
{
    use InteractionAcceptControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new AppealCommandHandlerFactory());
    }

    protected function getUnAuditedInteractionRepository() : ApplyFormRepository
    {
        return new UnAuditedAppealRepository();
    }

    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        unset($applyForm);
    }

    protected function getAcceptCommand(array $data) : ICommand
    {
        return new AcceptAppealCommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }

    protected function validateAcceptScenario(
        $replyContent,
        $replyImages,
        $admissibility,
        $crew
    ) : bool {
        unset($replyContent);
        unset($replyImages);
        unset($admissibility);
        unset($crew);
        return false;
    }

    public function acceptInteractionPublic(int $id)
    {
        return $this->acceptInteraction($id);
    }
}
