<?php
namespace Base\Interaction\Controller\Complaint;

use Base\ApplyForm\Model\IApplyFormAble;

class MockComplaintApproveController extends ComplaintApproveController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }
}
