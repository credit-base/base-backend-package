<?php
namespace Base\Interaction\Controller\Complaint;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Repository\Complaint\ComplaintRepository;

class MockComplaintControllerTrait extends Controller
{
    use ComplaintControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getRepositoryPublic() : ComplaintRepository
    {
        return $this->getRepository();
    }

    public function getUnAuditedInteractionRepositoryPublic() : ApplyFormRepository
    {
        return $this->getUnAuditedInteractionRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
