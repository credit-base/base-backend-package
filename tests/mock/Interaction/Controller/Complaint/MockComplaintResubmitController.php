<?php
namespace Base\Interaction\Controller\Complaint;

use Marmot\Interfaces\ICommand;
use Base\ApplyForm\Model\IApplyFormAble;

class MockComplaintResubmitController extends ComplaintResubmitController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }

    public function getAcceptCommand(array $data) : ICommand
    {
        return parent::getAcceptCommand($data);
    }
}
