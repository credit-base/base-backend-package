<?php
namespace Base\Interaction\Controller\Complaint;

use Marmot\Interfaces\IView;
use Base\Interaction\Adapter\Complaint\IComplaintAdapter;

class MockComplaintFetchController extends ComplaintFetchController
{
    public function getRepository() : IComplaintAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
