<?php
namespace Base\Interaction\Controller\Complaint;

use Marmot\Interfaces\IView;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

class MockUnAuditedComplaintFetchController extends UnAuditedComplaintFetchController
{
    public function getRepository() : UnAuditedComplaintRepository
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
