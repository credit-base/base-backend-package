<?php
namespace Base\Interaction\Controller\Feedback;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Repository\Feedback\FeedbackRepository;

class MockFeedbackControllerTrait extends Controller
{
    use FeedbackControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getRepositoryPublic() : FeedbackRepository
    {
        return $this->getRepository();
    }

    public function getUnAuditedInteractionRepositoryPublic() : ApplyFormRepository
    {
        return $this->getUnAuditedInteractionRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
