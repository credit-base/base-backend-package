<?php
namespace Base\Interaction\Controller\Feedback;

use Base\ApplyForm\Model\IApplyFormAble;

class MockFeedbackApproveController extends FeedbackApproveController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }
}
