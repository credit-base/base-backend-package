<?php
namespace Base\Interaction\Controller\Feedback;

use Marmot\Interfaces\IView;
use Base\Interaction\Adapter\Feedback\IFeedbackAdapter;

class MockFeedbackFetchController extends FeedbackFetchController
{
    public function getRepository() : IFeedbackAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
