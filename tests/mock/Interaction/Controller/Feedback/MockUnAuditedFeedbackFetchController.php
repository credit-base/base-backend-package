<?php
namespace Base\Interaction\Controller\Feedback;

use Marmot\Interfaces\IView;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

class MockUnAuditedFeedbackFetchController extends UnAuditedFeedbackFetchController
{
    public function getRepository() : UnAuditedFeedbackRepository
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
