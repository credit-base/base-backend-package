<?php
namespace Base\Interaction\Controller\QA;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Repository\QA\QARepository;

class MockQAControllerTrait extends Controller
{
    use QAControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getRepositoryPublic() : QARepository
    {
        return $this->getRepository();
    }

    public function getUnAuditedInteractionRepositoryPublic() : ApplyFormRepository
    {
        return $this->getUnAuditedInteractionRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
