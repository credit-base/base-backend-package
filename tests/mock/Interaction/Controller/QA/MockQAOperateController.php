<?php
namespace Base\Interaction\Controller\QA;

use Marmot\Interfaces\ICommand;
use Base\ApplyForm\Model\IApplyFormAble;

class MockQAOperateController extends QAOperateController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }

    public function getAcceptCommand(array $data) : ICommand
    {
        return parent::getAcceptCommand($data);
    }
}
