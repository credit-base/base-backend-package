<?php
namespace Base\Interaction\Controller\QA;

use Marmot\Interfaces\IView;
use Base\Interaction\Repository\QA\UnAuditedQARepository;

class MockUnAuditedQAFetchController extends UnAuditedQAFetchController
{
    public function getRepository() : UnAuditedQARepository
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
