<?php
namespace Base\Interaction\Controller\QA;

use Marmot\Interfaces\IView;
use Base\Interaction\Adapter\QA\IQAAdapter;

class MockQAFetchController extends QAFetchController
{
    public function getRepository() : IQAAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
