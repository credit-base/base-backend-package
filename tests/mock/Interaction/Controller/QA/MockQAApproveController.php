<?php
namespace Base\Interaction\Controller\QA;

use Base\ApplyForm\Model\IApplyFormAble;

class MockQAApproveController extends QAApproveController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }
}
