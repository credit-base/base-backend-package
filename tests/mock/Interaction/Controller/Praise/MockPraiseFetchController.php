<?php
namespace Base\Interaction\Controller\Praise;

use Marmot\Interfaces\IView;
use Base\Interaction\Adapter\Praise\IPraiseAdapter;

class MockPraiseFetchController extends PraiseFetchController
{
    public function getRepository() : IPraiseAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
