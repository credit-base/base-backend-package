<?php
namespace Base\Interaction\Controller\Praise;

use Marmot\Interfaces\IView;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

class MockUnAuditedPraiseFetchController extends UnAuditedPraiseFetchController
{
    public function getRepository() : UnAuditedPraiseRepository
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
