<?php
namespace Base\Interaction\Controller\Praise;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Repository\Praise\PraiseRepository;

class MockPraiseControllerTrait extends Controller
{
    use PraiseControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getRepositoryPublic() : PraiseRepository
    {
        return $this->getRepository();
    }

    public function getUnAuditedInteractionRepositoryPublic() : ApplyFormRepository
    {
        return $this->getUnAuditedInteractionRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
