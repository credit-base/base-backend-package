<?php
namespace Base\Interaction\Controller\Praise;

use Marmot\Interfaces\ICommand;
use Base\ApplyForm\Model\IApplyFormAble;

class MockPraiseOperateController extends PraiseOperateController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }

    public function getAcceptCommand(array $data) : ICommand
    {
        return parent::getAcceptCommand($data);
    }
}
