<?php
namespace Base\Interaction\Controller\Praise;

use Base\ApplyForm\Model\IApplyFormAble;

class MockPraiseApproveController extends PraiseApproveController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }
}
