<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Repository\Appeal\AppealRepository;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

class MockAppealCommandHandlerTrait
{
    use AppealCommandHandlerTrait;

    public function publicGetAppeal() : Appeal
    {
        return $this->getAppeal();
    }

    public function publicGetUnAuditedInteraction() : UnAuditedAppeal
    {
        return $this->getUnAuditedInteraction();
    }

    public function publicGetAppealRepository() : AppealRepository
    {
        return $this->getAppealRepository();
    }

    public function publicFetchAppeal(int $id) : Appeal
    {
        return $this->fetchAppeal($id);
    }

    public function publicGetUnAuditedAppealRepository() : UnAuditedAppealRepository
    {
        return $this->getUnAuditedAppealRepository();
    }

    public function publicFetchUnAuditedAppeal(int $id) : UnAuditedAppeal
    {
        return $this->fetchUnAuditedAppeal($id);
    }
}
