<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Base\Interaction\Model\Feedback;
use Base\Interaction\Model\UnAuditedFeedback;
use Base\Interaction\Repository\Feedback\FeedbackRepository;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

class MockFeedbackCommandHandlerTrait
{
    use FeedbackCommandHandlerTrait;

    public function publicGetFeedback() : Feedback
    {
        return $this->getFeedback();
    }

    public function publicGetUnAuditedInteraction() : UnAuditedFeedback
    {
        return $this->getUnAuditedInteraction();
    }

    public function publicGetFeedbackRepository() : FeedbackRepository
    {
        return $this->getFeedbackRepository();
    }

    public function publicFetchFeedback(int $id) : Feedback
    {
        return $this->fetchFeedback($id);
    }

    public function publicGetUnAuditedFeedbackRepository() : UnAuditedFeedbackRepository
    {
        return $this->getUnAuditedFeedbackRepository();
    }

    public function publicFetchUnAuditedFeedback(int $id) : UnAuditedFeedback
    {
        return $this->fetchUnAuditedFeedback($id);
    }
}
