<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Base\Interaction\Model\Complaint;
use Base\Interaction\Model\UnAuditedComplaint;
use Base\Interaction\Repository\Complaint\ComplaintRepository;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

class MockComplaintCommandHandlerTrait
{
    use ComplaintCommandHandlerTrait;

    public function publicGetComplaint() : Complaint
    {
        return $this->getComplaint();
    }

    public function publicGetUnAuditedInteraction() : UnAuditedComplaint
    {
        return $this->getUnAuditedInteraction();
    }

    public function publicGetComplaintRepository() : ComplaintRepository
    {
        return $this->getComplaintRepository();
    }

    public function publicFetchComplaint(int $id) : Complaint
    {
        return $this->fetchComplaint($id);
    }

    public function publicGetUnAuditedComplaintRepository() : UnAuditedComplaintRepository
    {
        return $this->getUnAuditedComplaintRepository();
    }

    public function publicFetchUnAuditedComplaint(int $id) : UnAuditedComplaint
    {
        return $this->fetchUnAuditedComplaint($id);
    }
}
