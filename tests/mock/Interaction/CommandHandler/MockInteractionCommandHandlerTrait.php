<?php
namespace Base\Interaction\CommandHandler;

use Marmot\Interfaces\ICommand;
use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\UnAuditedQA;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\Reply\ReplyRepository;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Service\ResubmitAcceptInteractionService;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class MockInteractionCommandHandlerTrait
{
    use InteractionCommandHandlerTrait;

    public function getUnAuditedInteraction() : IApplyFormAble
    {
        return new UnAuditedQA();
    }

    public function publicGetMemberRepository() : MemberRepository
    {
        return $this->getMemberRepository();
    }

    public function publicFetchMember(int $id) : Member
    {
        return $this->fetchMember($id);
    }

    public function publicGetUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicFetchUserGroup(int $id) : UserGroup
    {
        return $this->fetchUserGroup($id);
    }

    public function publicGetReplyRepository() : ReplyRepository
    {
        return $this->getReplyRepository();
    }

    public function publicFetchReply(int $id) : Reply
    {
        return $this->fetchReply($id);
    }

    public function publicGetCrewRepository() : CrewRepository
    {
        return $this->getCrewRepository();
    }

    public function publicFetchCrew(int $id) : Crew
    {
        return $this->fetchCrew($id);
    }

    public function publicGetReply() : Reply
    {
        return $this->getReply();
    }

    public function publicGetAcceptInteractionService(
        IApplyFormAble $unAuditedInteraction,
        IInteractionAble $interaction,
        Reply $reply
    ) : AcceptInteractionService {
        return $this->getAcceptInteractionService($unAuditedInteraction, $interaction, $reply);
    }

    public function publicGetResubmitAcceptInteractionService(
        IApplyFormAble $unAuditedInteraction,
        Reply $reply
    ) : ResubmitAcceptInteractionService {
        return $this->getResubmitAcceptInteractionService($unAuditedInteraction, $reply);
    }

    public function publicInteractionAddExecuteAction(
        ICommand $command,
        IInteractionAble $interaction
    ) : IInteractionAble {
        return $this->interactionAddExecuteAction($command, $interaction);
    }

    public function publicCommentInteractionAddExecuteAction(
        ICommand $command,
        IInteractionAble $interaction
    ) : IInteractionAble {
        return $this->commentInteractionAddExecuteAction($command, $interaction);
    }

    public function publicInteractionAcceptExecuteAction(
        ICommand $command,
        IInteractionAble $interaction
    ) : IApplyFormAble {
        return $this->interactionAcceptExecuteAction($command, $interaction);
    }

    public function publicCommentInteractionAcceptExecuteAction(
        ICommand $command,
        IInteractionAble $interaction
    ) : IApplyFormAble {
        return $this->commentInteractionAcceptExecuteAction($command, $interaction);
    }

    public function publicInteractionResubmitExecuteAction(
        ICommand $command,
        IApplyFormAble $unAuditedInteraction
    ) : IApplyFormAble {
        return $this->interactionResubmitExecuteAction($command, $unAuditedInteraction);
    }
}
