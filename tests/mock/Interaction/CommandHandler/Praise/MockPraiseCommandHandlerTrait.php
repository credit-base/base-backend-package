<?php
namespace Base\Interaction\CommandHandler\Praise;

use Base\Interaction\Model\Praise;
use Base\Interaction\Model\UnAuditedPraise;
use Base\Interaction\Repository\Praise\PraiseRepository;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

class MockPraiseCommandHandlerTrait
{
    use PraiseCommandHandlerTrait;

    public function publicGetPraise() : Praise
    {
        return $this->getPraise();
    }

    public function publicGetUnAuditedInteraction() : UnAuditedPraise
    {
        return $this->getUnAuditedInteraction();
    }

    public function publicGetPraiseRepository() : PraiseRepository
    {
        return $this->getPraiseRepository();
    }

    public function publicFetchPraise(int $id) : Praise
    {
        return $this->fetchPraise($id);
    }

    public function publicGetUnAuditedPraiseRepository() : UnAuditedPraiseRepository
    {
        return $this->getUnAuditedPraiseRepository();
    }

    public function publicFetchUnAuditedPraise(int $id) : UnAuditedPraise
    {
        return $this->fetchUnAuditedPraise($id);
    }
}
