<?php
namespace Base\Interaction\CommandHandler\QA;

use Base\Interaction\Model\QA;
use Base\Interaction\Model\UnAuditedQA;
use Base\Interaction\Repository\QA\QARepository;
use Base\Interaction\Repository\QA\UnAuditedQARepository;

class MockQACommandHandlerTrait
{
    use QACommandHandlerTrait;

    public function publicGetQA() : QA
    {
        return $this->getQA();
    }

    public function publicGetUnAuditedInteraction() : UnAuditedQA
    {
        return $this->getUnAuditedInteraction();
    }

    public function publicGetQARepository() : QARepository
    {
        return $this->getQARepository();
    }

    public function publicFetchQA(int $id) : QA
    {
        return $this->fetchQA($id);
    }

    public function publicGetUnAuditedQARepository() : UnAuditedQARepository
    {
        return $this->getUnAuditedQARepository();
    }

    public function publicFetchUnAuditedQA(int $id) : UnAuditedQA
    {
        return $this->fetchUnAuditedQA($id);
    }
}
