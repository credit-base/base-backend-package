<?php
namespace Base\Interaction\Repository\Complaint;

use Base\Interaction\Adapter\Complaint\IComplaintAdapter;

class MockComplaintRepository extends ComplaintRepository
{
    public function getAdapter() : IComplaintAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IComplaintAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IComplaintAdapter
    {
        return parent::getMockAdapter();
    }
}
