<?php
namespace Base\Interaction\Repository\Complaint;

use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class MockUnAuditedComplaintRepository extends UnAuditedComplaintRepository
{
    public function getAdapter() : IApplyFormAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IApplyFormAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IApplyFormAdapter
    {
        return parent::getMockAdapter();
    }
}
