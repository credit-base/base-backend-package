<?php
namespace Base\Interaction\Repository\Reply;

use Base\Interaction\Adapter\Reply\IReplyAdapter;

class MockReplyRepository extends ReplyRepository
{
    public function getAdapter() : IReplyAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IReplyAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IReplyAdapter
    {
        return parent::getMockAdapter();
    }
}
