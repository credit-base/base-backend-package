<?php
namespace Base\Interaction\Repository\QA;

use Base\Interaction\Adapter\QA\IQAAdapter;

class MockQARepository extends QARepository
{
    public function getAdapter() : IQAAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IQAAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IQAAdapter
    {
        return parent::getMockAdapter();
    }
}
