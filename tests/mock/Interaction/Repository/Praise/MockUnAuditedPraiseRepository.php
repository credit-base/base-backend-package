<?php
namespace Base\Interaction\Repository\Praise;

use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class MockUnAuditedPraiseRepository extends UnAuditedPraiseRepository
{
    public function getAdapter() : IApplyFormAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IApplyFormAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IApplyFormAdapter
    {
        return parent::getMockAdapter();
    }
}
