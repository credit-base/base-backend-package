<?php
namespace Base\Interaction\Repository\Praise;

use Base\Interaction\Adapter\Praise\IPraiseAdapter;

class MockPraiseRepository extends PraiseRepository
{
    public function getAdapter() : IPraiseAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IPraiseAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IPraiseAdapter
    {
        return parent::getMockAdapter();
    }
}
