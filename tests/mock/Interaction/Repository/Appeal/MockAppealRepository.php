<?php
namespace Base\Interaction\Repository\Appeal;

use Base\Interaction\Adapter\Appeal\IAppealAdapter;

class MockAppealRepository extends AppealRepository
{
    public function getAdapter() : IAppealAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IAppealAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IAppealAdapter
    {
        return parent::getMockAdapter();
    }
}
