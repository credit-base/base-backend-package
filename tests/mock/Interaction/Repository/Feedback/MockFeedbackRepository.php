<?php
namespace Base\Interaction\Repository\Feedback;

use Base\Interaction\Adapter\Feedback\IFeedbackAdapter;

class MockFeedbackRepository extends FeedbackRepository
{
    public function getAdapter() : IFeedbackAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IFeedbackAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IFeedbackAdapter
    {
        return parent::getMockAdapter();
    }
}
