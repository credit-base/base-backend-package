<?php
namespace Base\Interaction\Repository\Feedback;

use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class MockUnAuditedFeedbackRepository extends UnAuditedFeedbackRepository
{
    public function getAdapter() : IApplyFormAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IApplyFormAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IApplyFormAdapter
    {
        return parent::getMockAdapter();
    }
}
