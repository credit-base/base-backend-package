<?php
namespace Base\Interaction\Adapter\Appeal\Query\Persistence;

class MockAppealCache extends AppealCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
