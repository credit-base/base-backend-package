<?php
namespace Base\Interaction\Adapter\Appeal;

use Marmot\Interfaces\INull;

use Base\Interaction\Translator\AppealDbTranslator;
use Base\Interaction\Adapter\Appeal\Query\AppealRowCacheQuery;

class MockAppealDbAdapter extends AppealDbAdapter
{
    public function getDbTranslator() : AppealDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : AppealRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
