<?php
namespace Base\Interaction\Adapter\Reply;

use Marmot\Interfaces\INull;

use Base\Interaction\Model\Reply;
use Base\Interaction\Translator\ReplyDbTranslator;
use Base\Interaction\Adapter\Reply\Query\ReplyRowCacheQuery;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

class MockReplyDbAdapter extends ReplyDbAdapter
{
    public function getDbTranslator() : ReplyDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : ReplyRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }

    public function fetchUserGroup($reply)
    {
        return parent::fetchUserGroup($reply);
    }

    public function fetchUserGroupByObject(Reply $reply)
    {
        return parent::fetchUserGroupByObject($reply);
    }

    public function fetchUserGroupByList(array $replyList)
    {
        return parent::fetchUserGroupByList($replyList);
    }

    public function fetchCrew($reply)
    {
        return parent::fetchCrew($reply);
    }

    public function fetchCrewByObject(Reply $reply)
    {
        return parent::fetchCrewByObject($reply);
    }

    public function fetchCrewByList(array $replyList)
    {
        return parent::fetchCrewByList($replyList);
    }
}
