<?php
namespace Base\Interaction\Adapter\Reply\Query;

use Marmot\Framework\Interfaces\DbLayer;
use Marmot\Interfaces\CacheLayer;

class MockReplyRowCacheQuery extends ReplyRowCacheQuery
{
    public function getCacheLayer() : CacheLayer
    {
        return parent::getCacheLayer();
    }

    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}
