<?php
namespace Base\Interaction\Adapter\Reply\Query\Persistence;

class MockReplyCache extends ReplyCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
