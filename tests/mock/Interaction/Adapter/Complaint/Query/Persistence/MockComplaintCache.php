<?php
namespace Base\Interaction\Adapter\Complaint\Query\Persistence;

class MockComplaintCache extends ComplaintCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
