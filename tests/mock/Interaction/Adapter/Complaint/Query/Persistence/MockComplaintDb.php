<?php
namespace Base\Interaction\Adapter\Complaint\Query\Persistence;

class MockComplaintDb extends ComplaintDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
