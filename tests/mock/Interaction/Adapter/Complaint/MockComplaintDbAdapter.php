<?php
namespace Base\Interaction\Adapter\Complaint;

use Marmot\Interfaces\INull;

use Base\Interaction\Translator\ComplaintDbTranslator;
use Base\Interaction\Adapter\Complaint\Query\ComplaintRowCacheQuery;

class MockComplaintDbAdapter extends ComplaintDbAdapter
{
    public function getDbTranslator() : ComplaintDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : ComplaintRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
