<?php
namespace Base\Interaction\Adapter\Praise\Query\Persistence;

class MockPraiseCache extends PraiseCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
