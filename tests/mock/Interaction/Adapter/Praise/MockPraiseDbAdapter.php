<?php
namespace Base\Interaction\Adapter\Praise;

use Marmot\Interfaces\INull;

use Base\Interaction\Translator\PraiseDbTranslator;
use Base\Interaction\Adapter\Praise\Query\PraiseRowCacheQuery;

class MockPraiseDbAdapter extends PraiseDbAdapter
{
    public function getDbTranslator() : PraiseDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : PraiseRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
