<?php
namespace Base\Interaction\Adapter\QA\Query\Persistence;

class MockQADb extends QADb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
