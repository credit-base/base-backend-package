<?php
namespace Base\Interaction\Adapter\QA\Query\Persistence;

class MockQACache extends QACache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
