<?php
namespace Base\Interaction\Adapter\QA;

use Marmot\Interfaces\INull;

use Base\Interaction\Translator\QADbTranslator;
use Base\Interaction\Adapter\QA\Query\QARowCacheQuery;

class MockQADbAdapter extends QADbAdapter
{
    public function getDbTranslator() : QADbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : QARowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
