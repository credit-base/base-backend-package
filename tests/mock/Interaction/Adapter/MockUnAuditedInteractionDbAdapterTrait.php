<?php
namespace Base\Interaction\Adapter;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Repository\Reply\ReplyRepository;

class MockUnAuditedInteractionDbAdapterTrait
{
    use UnAuditedInteractionDbAdapterTrait;

    public function publicGetReplyRepository() : ReplyRepository
    {
        return $this->getReplyRepository();
    }

    public function publicFetchReply($unAuditedInteraction)
    {
        return $this->fetchReply($unAuditedInteraction);
    }

    public function publicFetchReplyByObject(IApplyFormAble $unAuditedInteraction)
    {
        return $this->fetchReplyByObject($unAuditedInteraction);
    }

    public function publicFetchReplyByList(array $unAuditedInteractionList)
    {
        return $this->fetchReplyByList($unAuditedInteractionList);
    }
}
