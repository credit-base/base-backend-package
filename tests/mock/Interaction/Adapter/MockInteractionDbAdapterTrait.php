<?php
namespace Base\Interaction\Adapter;

use Marmot\Interfaces\ITranslator;

use Base\Member\Repository\MemberRepository;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Translator\AppealDbTranslator;
use Base\Interaction\Repository\Reply\ReplyRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MockInteractionDbAdapterTrait
{
    use InteractionDbAdapterTrait;

    protected function getDbTranslator() : ITranslator
    {
        return new AppealDbTranslator();
    }

    public function publicGetUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicGetMemberRepository() : MemberRepository
    {
        return $this->getMemberRepository();
    }

    public function publicGetReplyRepository() : ReplyRepository
    {
        return $this->getReplyRepository();
    }

    public function publicFetchAcceptUserGroup($interaction)
    {
        return $this->fetchAcceptUserGroup($interaction);
    }

    public function publicFetchAcceptUserGroupByObject(IInteractionAble $interaction)
    {
        return $this->fetchAcceptUserGroupByObject($interaction);
    }

    public function publicFetchAcceptUserGroupByList(array $interactionList)
    {
        return $this->fetchAcceptUserGroupByList($interactionList);
    }

    public function publicFetchMember($interaction)
    {
        return $this->fetchMember($interaction);
    }

    public function publicFetchMemberByObject(IInteractionAble $interaction)
    {
        return $this->fetchMemberByObject($interaction);
    }

    public function publicFetchMemberByList(array $interactionList)
    {
        return $this->fetchMemberByList($interactionList);
    }

    public function publicFetchReply($interaction)
    {
        return $this->fetchReply($interaction);
    }

    public function publicFetchReplyByObject(IInteractionAble $interaction)
    {
        return $this->fetchReplyByObject($interaction);
    }

    public function publicFetchReplyByList(array $interactionList)
    {
        return $this->fetchReplyByList($interactionList);
    }

    public function publicInteractionFormatFilter(array $filter, IInteractionAble $interaction)
    {
        return $this->interactionFormatFilter($filter, $interaction);
    }

    public function publicInteractionFormatSort(array $sort, IInteractionAble $interaction)
    {
        return $this->interactionFormatSort($sort, $interaction);
    }
}
