<?php
namespace Base\Interaction\Adapter\Feedback\Query\Persistence;

class MockFeedbackCache extends FeedbackCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
