<?php
namespace Base\Interaction\Adapter\Feedback\Query\Persistence;

class MockFeedbackDb extends FeedbackDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
