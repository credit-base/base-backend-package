<?php
namespace Base\Interaction\Adapter\Feedback;

use Marmot\Interfaces\INull;

use Base\Interaction\Translator\FeedbackDbTranslator;
use Base\Interaction\Adapter\Feedback\Query\FeedbackRowCacheQuery;

class MockFeedbackDbAdapter extends FeedbackDbAdapter
{
    public function getDbTranslator() : FeedbackDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : FeedbackRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
