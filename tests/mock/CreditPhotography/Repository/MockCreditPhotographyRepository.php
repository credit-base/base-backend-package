<?php
namespace Base\CreditPhotography\Repository;

use Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter;

class MockCreditPhotographyRepository extends CreditPhotographyRepository
{
    public function getAdapter() : ICreditPhotographyAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : ICreditPhotographyAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : ICreditPhotographyAdapter
    {
        return parent::getMockAdapter();
    }
}
