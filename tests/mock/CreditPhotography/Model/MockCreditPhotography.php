<?php
namespace Base\CreditPhotography\Model;

use Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter;

class MockCreditPhotography extends CreditPhotography
{
    public function getRepository() : ICreditPhotographyAdapter
    {
        return parent::getRepository();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function approveAction() : bool
    {
        return parent::approveAction();
    }

    public function rejectAction() : bool
    {
        return parent::rejectAction();
    }

    public function updateApplyStatus(int $applyStatus) : bool
    {
        return parent::updateApplyStatus($applyStatus);
    }
}
