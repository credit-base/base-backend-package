<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence;

class MockCreditPhotographyDb extends CreditPhotographyDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
