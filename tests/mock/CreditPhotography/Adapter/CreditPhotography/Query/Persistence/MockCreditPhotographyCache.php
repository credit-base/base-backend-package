<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence;

class MockCreditPhotographyCache extends CreditPhotographyCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
