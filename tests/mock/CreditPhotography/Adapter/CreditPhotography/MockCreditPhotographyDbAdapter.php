<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography;

use Marmot\Interfaces\INull;

use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;
use Base\CreditPhotography\Adapter\CreditPhotography\Query\CreditPhotographyRowCacheQuery;

use Base\Crew\Repository\CrewRepository;
use Base\Member\Repository\MemberRepository;

class MockCreditPhotographyDbAdapter extends CreditPhotographyDbAdapter
{
    public function getDbTranslator() : CreditPhotographyDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : CreditPhotographyRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function getMemberRepository() : MemberRepository
    {
        return parent::getMemberRepository();
    }

    public function fetchMember($creditPhotography)
    {
        return parent::fetchMember($creditPhotography);
    }

    public function fetchMemberByObject($creditPhotography)
    {
        return parent::fetchMemberByObject($creditPhotography);
    }

    public function fetchMemberByList(array $creditPhotographyList)
    {
        return parent::fetchMemberByList($creditPhotographyList);
    }

    public function fetchApplyCrew($creditPhotography)
    {
        return parent::fetchApplyCrew($creditPhotography);
    }

    public function fetchApplyCrewByObject($creditPhotography)
    {
        return parent::fetchApplyCrewByObject($creditPhotography);
    }

    public function fetchApplyCrewByList(array $creditPhotographyList)
    {
        return parent::fetchApplyCrewByList($creditPhotographyList);
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
