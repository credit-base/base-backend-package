<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MockCreditPhotographyCommandHandlerTrait
{
    use CreditPhotographyCommandHandlerTrait;

    public function publicGetCrewRepository() : CrewRepository
    {
        return $this->getCrewRepository();
    }

    public function publicFetchApplyCrew(int $id) : Crew
    {
        return $this->fetchApplyCrew($id);
    }

    public function publicGetMemberRepository() : MemberRepository
    {
        return $this->getMemberRepository();
    }

    public function publicFetchMember(int $id) : Member
    {
        return $this->fetchMember($id);
    }

    public function publicGetCreditPhotographyRepository() : CreditPhotographyRepository
    {
        return $this->getCreditPhotographyRepository();
    }

    public function publicGetCreditPhotographyDbTranslator() : CreditPhotographyDbTranslator
    {
        return $this->getCreditPhotographyDbTranslator();
    }

    public function publicFetchCreditPhotography(int $id) : CreditPhotography
    {
        return $this->fetchCreditPhotography($id);
    }

    public function publicGetCreditPhotography() : CreditPhotography
    {
        return $this->getCreditPhotography();
    }
}
