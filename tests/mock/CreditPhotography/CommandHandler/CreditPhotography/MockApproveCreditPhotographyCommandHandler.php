<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Common\Model\IApproveAble;

use Base\Common\Command\ApproveCommand;

class MockApproveCreditPhotographyCommandHandler extends ApproveCreditPhotographyCommandHandler
{
    public function fetchIApplyObject($id) : IApproveAble
    {
        return parent::fetchIApplyObject($id);
    }

    public function executeAction(ApproveCommand $command)
    {
        return parent::executeAction($command);
    }
}
