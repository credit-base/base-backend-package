<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Common\Model\IApproveAble;

use Base\Common\Command\RejectCommand;

class MockRejectCreditPhotographyCommandHandler extends RejectCreditPhotographyCommandHandler
{
    public function fetchIApplyObject($id) : IApproveAble
    {
        return parent::fetchIApplyObject($id);
    }

    public function executeAction(RejectCommand $command)
    {
        return parent::executeAction($command);
    }
}
