<?php
namespace Base\CreditPhotography\Controller;

use Marmot\Interfaces\IView;

use Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter;

class MockCreditPhotographyFetchController extends CreditPhotographyFetchController
{
    public function getRepository() : ICreditPhotographyAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
