<?php
namespace Base\CreditPhotography\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\CreditPhotography\WidgetRule\CreditPhotographyWidgetRule;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;

class MockCreditPhotographyControllerTrait extends Controller
{
    use CreditPhotographyControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getCreditPhotographyWidgetRulePublic() : CreditPhotographyWidgetRule
    {
        return $this->getCreditPhotographyWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : CreditPhotographyRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    public function validateAddScenarioPublic(
        $description,
        $attachments,
        $member
    ) {
        return $this->validateAddScenario(
            $description,
            $attachments,
            $member
        );
    }

    public function validateApproveScenarioPublic(
        $applyCrew
    ) {
        return $this->validateApproveScenario(
            $applyCrew
        );
    }

    public function validateRejectScenarioPublic(
        $rejectReason,
        $applyCrew
    ) {
        return $this->validateRejectScenario(
            $rejectReason,
            $applyCrew
        );
    }
}
