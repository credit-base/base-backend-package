<?php
namespace Base\Department\Adapter\Department\Query\Persistence;

class MockDepartmentDb extends DepartmentDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
