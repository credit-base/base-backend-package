<?php
namespace Base\Department\Adapter\Department\Query\Persistence;

class MockDepartmentCache extends DepartmentCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
