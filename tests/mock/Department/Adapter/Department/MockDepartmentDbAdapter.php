<?php
namespace Base\Department\Adapter\Department;

use Marmot\Interfaces\INull;

use Base\Department\Translator\DepartmentDbTranslator;
use Base\Department\Adapter\Department\Query\DepartmentRowCacheQuery;

use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

class MockDepartmentDbAdapter extends DepartmentDbAdapter
{
    public function getDbTranslator() : DepartmentDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : DepartmentRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getUserGroupRepository() : IUserGroupAdapter
    {
        return parent::getUserGroupRepository();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }

    public function fetchUserGroup($crew)
    {
        return parent::fetchUserGroup($crew);
    }

    public function fetchUserGroupByObject($crew)
    {
        return parent::fetchUserGroupByObject($crew);
    }

    public function fetchUserGroupByList(array $crewList)
    {
        return parent::fetchUserGroupByList($crewList);
    }
}
