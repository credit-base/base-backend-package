<?php
namespace Base\Department\Model;

use Base\Department\Adapter\Department\IDepartmentAdapter;

class MockDepartment extends Department
{
    public function getRepository() : IDepartmentAdapter
    {
        return parent::getRepository();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }
    
    public function isNameExist() : bool
    {
        return parent::isNameExist();
    }
}
