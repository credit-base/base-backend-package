<?php
namespace Base\Department\CommandHandler\Department;

use Base\Department\Model\Department;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

class MockAddDepartmentCommandHandler extends AddDepartmentCommandHandler
{
    public function getDepartment() : Department
    {
        return parent::getDepartment();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function fetchUserGroup(int $id) : UserGroup
    {
        return parent::fetchUserGroup($id);
    }
}
