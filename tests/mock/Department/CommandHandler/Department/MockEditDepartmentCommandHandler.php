<?php
namespace Base\Department\CommandHandler\Department;

use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;

class MockEditDepartmentCommandHandler extends EditDepartmentCommandHandler
{
    public function getDepartmentRepository() : DepartmentRepository
    {
        return parent::getDepartmentRepository();
    }

    public function fetchDepartment(int $id) : Department
    {
        return parent::fetchDepartment($id);
    }
}
