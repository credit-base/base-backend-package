<?php
namespace Base\Department\Controller;

class MockDepartmentOperateController extends DepartmentOperateController
{
    public function validateAddScenario($name, $userGroupId) : bool
    {
        return $this->validateAddScenario($name, $userGroupId);
    }

    public function validateEditScenario($name) : bool
    {
        return $this->validateEditScenario($name);
    }
}
