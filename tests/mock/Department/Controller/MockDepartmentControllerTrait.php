<?php
namespace Base\Department\Controller;

use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Department\WidgetRule\DepartmentWidgetRule;
use Base\Department\Repository\DepartmentRepository;

class MockDepartmentControllerTrait extends Controller
{
    use DepartmentControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getDepartmentWidgetRulePublic() : DepartmentWidgetRule
    {
        return $this->getDepartmentWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : DepartmentRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
