<?php
namespace Base\Department\Controller;

use Base\Department\Adapter\Department\IDepartmentAdapter;
use Marmot\Interfaces\IView;

class MockDepartmentFetchController extends DepartmentFetchController
{
    public function getRepository() : IDepartmentAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
