<?php
namespace Base\ResourceCatalogData\Model;

use Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;

class MockGbSearchData extends GbSearchData
{
    public function getRepository() : IGbSearchDataAdapter
    {
        return parent::getRepository();
    }
}
