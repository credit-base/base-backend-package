<?php
namespace Base\ResourceCatalogData\Model;

use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

class MockSearchData extends SearchData
{
    public function setStatus(int $status) : void
    {
        unset($status);
    }

    public function getItemsData()
    {
    }
}
