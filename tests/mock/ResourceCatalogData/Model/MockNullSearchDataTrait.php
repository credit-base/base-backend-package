<?php
namespace Base\ResourceCatalogData\Model;

class MockNullSearchDataTrait
{
    use NullSearchDataTrait;

    public function publicResourceNotExist() : bool
    {
        return $this->resourceNotExist();
    }
}
