<?php
namespace Base\ResourceCatalogData\Model;

use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

class MockBjSearchData extends BjSearchData
{
    public function getRepository() : IBjSearchDataAdapter
    {
        return parent::getRepository();
    }
}
