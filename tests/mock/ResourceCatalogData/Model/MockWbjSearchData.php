<?php
namespace Base\ResourceCatalogData\Model;

use Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class MockWbjSearchData extends WbjSearchData
{
    public function getRepository() : IWbjSearchDataAdapter
    {
        return parent::getRepository();
    }
}
