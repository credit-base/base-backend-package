<?php
namespace Base\ResourceCatalogData\Repository;

use Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class MockWbjSearchDataSdkRepository extends WbjSearchDataSdkRepository
{
    public function getAdapter() : IWbjSearchDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IWbjSearchDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IWbjSearchDataAdapter
    {
        return parent::getMockAdapter();
    }
}
