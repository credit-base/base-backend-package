<?php
namespace Base\ResourceCatalogData\Repository;

use Base\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

class MockErrorDataSdkRepository extends ErrorDataSdkRepository
{
    public function getAdapter() : IErrorDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IErrorDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IErrorDataAdapter
    {
        return parent::getMockAdapter();
    }
}
