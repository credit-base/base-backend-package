<?php
namespace Base\ResourceCatalogData\Repository;

use Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;

class MockGbSearchDataSdkRepository extends GbSearchDataSdkRepository
{
    public function getAdapter() : IGbSearchDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IGbSearchDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IGbSearchDataAdapter
    {
        return parent::getMockAdapter();
    }
}
