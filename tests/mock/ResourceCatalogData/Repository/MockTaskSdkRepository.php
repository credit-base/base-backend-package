<?php
namespace Base\ResourceCatalogData\Repository;

use Base\ResourceCatalogData\Adapter\Task\ITaskAdapter;

class MockTaskSdkRepository extends TaskSdkRepository
{
    public function getAdapter() : ITaskAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : ITaskAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : ITaskAdapter
    {
        return parent::getMockAdapter();
    }
}
