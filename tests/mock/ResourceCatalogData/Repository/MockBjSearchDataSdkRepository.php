<?php
namespace Base\ResourceCatalogData\Repository;

use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

class MockBjSearchDataSdkRepository extends BjSearchDataSdkRepository
{
    public function getAdapter() : IBjSearchDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IBjSearchDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IBjSearchDataAdapter
    {
        return parent::getMockAdapter();
    }
}
