<?php
namespace Base\ResourceCatalogData\Translator;

use Base\Common\Translator\ISdkTranslator;

use Base\Template\Translator\TranslatorFactory;

class MockErrorDataSdkTranslator extends ErrorDataSdkTranslator
{
    public function getCrewSdkTranslator() : ISdkTranslator
    {
        return parent::getCrewSdkTranslator();
    }

    public function getTaskSdkTranslator() : ISdkTranslator
    {
        return parent::getTaskSdkTranslator();
    }

    public function getErrorItemsDataSdkTranslator() : ISdkTranslator
    {
        return parent::getErrorItemsDataSdkTranslator();
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return parent::getTranslatorFactory();
    }

    public function getTemplateSdkTranslator(int $category) : ISdkTranslator
    {
        return parent::getTemplateSdkTranslator($category);
    }
}
