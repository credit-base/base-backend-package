<?php
namespace Base\ResourceCatalogData\Translator;

use Base\Common\Translator\ISdkTranslator;

class MockTaskSdkTranslator extends TaskSdkTranslator
{
    public function getCrewSdkTranslator() : ISdkTranslator
    {
        return parent::getCrewSdkTranslator();
    }
}
