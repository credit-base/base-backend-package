<?php
namespace Base\ResourceCatalogData\Translator;

use Base\Common\Translator\ISdkTranslator;

class MockBjSearchDataSdkTranslator extends BjSearchDataSdkTranslator
{
    public function getCrewSdkTranslator() : ISdkTranslator
    {
        return parent::getCrewSdkTranslator();
    }

    public function getTaskSdkTranslator() : ISdkTranslator
    {
        return parent::getTaskSdkTranslator();
    }

    public function getBjItemsDataSdkTranslator() : ISdkTranslator
    {
        return parent::getBjItemsDataSdkTranslator();
    }

    public function getBjTemplateSdkTranslator() : ISdkTranslator
    {
        return parent::getBjTemplateSdkTranslator();
    }
}
