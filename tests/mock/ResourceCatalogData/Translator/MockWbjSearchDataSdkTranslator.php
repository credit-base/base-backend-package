<?php
namespace Base\ResourceCatalogData\Translator;

use Base\Common\Translator\ISdkTranslator;

class MockWbjSearchDataSdkTranslator extends WbjSearchDataSdkTranslator
{
    public function getCrewSdkTranslator() : ISdkTranslator
    {
        return parent::getCrewSdkTranslator();
    }

    public function getTaskSdkTranslator() : ISdkTranslator
    {
        return parent::getTaskSdkTranslator();
    }

    public function getWbjItemsDataSdkTranslator() : ISdkTranslator
    {
        return parent::getWbjItemsDataSdkTranslator();
    }

    public function getWbjTemplateSdkTranslator() : ISdkTranslator
    {
        return parent::getWbjTemplateSdkTranslator();
    }
}
