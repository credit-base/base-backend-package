<?php
namespace Base\ResourceCatalogData\Translator;

use Base\Common\Translator\ISdkTranslator;

class MockGbSearchDataSdkTranslator extends GbSearchDataSdkTranslator
{
    public function getCrewSdkTranslator() : ISdkTranslator
    {
        return parent::getCrewSdkTranslator();
    }

    public function getTaskSdkTranslator() : ISdkTranslator
    {
        return parent::getTaskSdkTranslator();
    }

    public function getGbItemsDataSdkTranslator() : ISdkTranslator
    {
        return parent::getGbItemsDataSdkTranslator();
    }

    public function getGbTemplateSdkTranslator() : ISdkTranslator
    {
        return parent::getGbTemplateSdkTranslator();
    }
}
