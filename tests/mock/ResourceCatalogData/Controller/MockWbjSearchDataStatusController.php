<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class MockWbjSearchDataStatusController extends WbjSearchDataStatusController
{
    public function getRepository() : IWbjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
