<?php
namespace Base\ResourceCatalogData\Controller;

use Base\ResourceCatalogData\Adapter\Task\ITaskAdapter;
use Marmot\Interfaces\IView;

class MockTaskFetchController extends TaskFetchController
{
    public function getRepository() : ITaskAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
