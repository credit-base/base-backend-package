<?php
namespace Base\ResourceCatalogData\Controller;

use Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use Marmot\Interfaces\IView;

class MockGbSearchDataFetchController extends GbSearchDataFetchController
{
    public function getRepository() : IGbSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
