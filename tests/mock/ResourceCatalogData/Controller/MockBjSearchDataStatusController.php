<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

class MockBjSearchDataStatusController extends BjSearchDataStatusController
{
    public function getRepository() : IBjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
