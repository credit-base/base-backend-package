<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;

class MockGbSearchDataStatusController extends GbSearchDataStatusController
{
    public function getRepository() : IGbSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
