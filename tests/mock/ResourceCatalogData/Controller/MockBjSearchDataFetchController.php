<?php
namespace Base\ResourceCatalogData\Controller;

use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use Marmot\Interfaces\IView;

class MockBjSearchDataFetchController extends BjSearchDataFetchController
{
    public function getRepository() : IBjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
