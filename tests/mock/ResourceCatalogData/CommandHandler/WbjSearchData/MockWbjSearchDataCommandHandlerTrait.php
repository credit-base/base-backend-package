<?php
namespace Base\ResourceCatalogData\CommandHandler\WbjSearchData;

use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\Repository\WbjSearchDataSdkRepository;

class MockWbjSearchDataCommandHandlerTrait
{
    use WbjSearchDataCommandHandlerTrait;

    public function publicGetRepository() : WbjSearchDataSdkRepository
    {
        return $this->getRepository();
    }

    public function publicFetchWbjSearchData(int $id) : WbjSearchData
    {
        return $this->fetchWbjSearchData($id);
    }
}
