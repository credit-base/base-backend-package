<?php
namespace Base\ResourceCatalogData\CommandHandler\GbSearchData;

use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Repository\GbSearchDataSdkRepository;

class MockGbSearchDataCommandHandlerTrait
{
    use GbSearchDataCommandHandlerTrait;

    public function publicGetRepository() : GbSearchDataSdkRepository
    {
        return $this->getRepository();
    }

    public function publicFetchGbSearchData(int $id) : GbSearchData
    {
        return $this->fetchGbSearchData($id);
    }
}
