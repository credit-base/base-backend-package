<?php
namespace Base\ResourceCatalogData\CommandHandler\BjSearchData;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Repository\BjSearchDataSdkRepository;

class MockBjSearchDataCommandHandlerTrait
{
    use BjSearchDataCommandHandlerTrait;

    public function publicGetRepository() : BjSearchDataSdkRepository
    {
        return $this->getRepository();
    }

    public function publicFetchBjSearchData(int $id) : BjSearchData
    {
        return $this->fetchBjSearchData($id);
    }
}
