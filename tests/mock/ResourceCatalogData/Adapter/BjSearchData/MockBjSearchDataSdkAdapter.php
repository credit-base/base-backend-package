<?php
namespace Base\ResourceCatalogData\Adapter\BjSearchData;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

use Base\ResourceCatalogData\Model\BjSearchData;

use Base\Crew\Repository\CrewRepository;

class MockBjSearchDataSdkAdapter extends BjSearchDataSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function fetchCrew($bjSearchData)
    {
        return parent::fetchCrew($bjSearchData);
    }

    public function fetchCrewByObject(BjSearchData $bjSearchData)
    {
        return parent::fetchCrewByObject($bjSearchData);
    }

    public function fetchCrewByList(array $bjSearchDataList)
    {
        return parent::fetchCrewByList($bjSearchDataList);
    }
}
