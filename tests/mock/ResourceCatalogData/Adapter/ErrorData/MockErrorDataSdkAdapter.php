<?php
namespace Base\ResourceCatalogData\Adapter\ErrorData;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

class MockErrorDataSdkAdapter extends ErrorDataSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }
}
