<?php
namespace Base\ResourceCatalogData\Adapter\WbjSearchData;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

use Base\ResourceCatalogData\Model\WbjSearchData;

use Base\Crew\Repository\CrewRepository;

class MockWbjSearchDataSdkAdapter extends WbjSearchDataSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function fetchCrew($wbjSearchData)
    {
        return parent::fetchCrew($wbjSearchData);
    }

    public function fetchCrewByObject(WbjSearchData $wbjSearchData)
    {
        return parent::fetchCrewByObject($wbjSearchData);
    }

    public function fetchCrewByList(array $wbjSearchDataList)
    {
        return parent::fetchCrewByList($wbjSearchDataList);
    }
}
