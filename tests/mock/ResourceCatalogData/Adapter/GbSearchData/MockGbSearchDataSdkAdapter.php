<?php
namespace Base\ResourceCatalogData\Adapter\GbSearchData;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

use Base\ResourceCatalogData\Model\GbSearchData;

use Base\Crew\Repository\CrewRepository;

class MockGbSearchDataSdkAdapter extends GbSearchDataSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function fetchCrew($gbSearchData)
    {
        return parent::fetchCrew($gbSearchData);
    }

    public function fetchCrewByObject(GbSearchData $gbSearchData)
    {
        return parent::fetchCrewByObject($gbSearchData);
    }

    public function fetchCrewByList(array $gbSearchDataList)
    {
        return parent::fetchCrewByList($gbSearchDataList);
    }
}
