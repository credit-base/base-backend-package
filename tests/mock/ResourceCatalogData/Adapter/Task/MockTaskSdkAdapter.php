<?php
namespace Base\ResourceCatalogData\Adapter\Task;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

class MockTaskSdkAdapter extends TaskSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }
}
