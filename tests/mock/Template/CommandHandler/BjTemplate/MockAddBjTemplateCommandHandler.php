<?php
namespace Base\Template\CommandHandler\BjTemplate;

use Base\Template\Model\BjTemplate;

class MockAddBjTemplateCommandHandler extends AddBjTemplateCommandHandler
{
    public function getBjTemplate() : BjTemplate
    {
        return parent::getBjTemplate();
    }
}
