<?php
namespace Base\Template\CommandHandler\BjTemplate;

use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;

class MockEditBjTemplateCommandHandler extends EditBjTemplateCommandHandler
{
    public function getBjTemplateSdkRepository() : IBjTemplateAdapter
    {
        return parent::getBjTemplateSdkRepository();
    }
}
