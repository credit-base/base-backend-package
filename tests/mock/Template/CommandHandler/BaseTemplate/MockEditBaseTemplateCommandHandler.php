<?php
namespace Base\Template\CommandHandler\BaseTemplate;

use Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class MockEditBaseTemplateCommandHandler extends EditBaseTemplateCommandHandler
{
    public function getBaseTemplateSdkRepository() : IBaseTemplateAdapter
    {
        return parent::getBaseTemplateSdkRepository();
    }
}
