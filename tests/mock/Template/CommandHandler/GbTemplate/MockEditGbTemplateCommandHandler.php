<?php
namespace Base\Template\CommandHandler\GbTemplate;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;

class MockEditGbTemplateCommandHandler extends EditGbTemplateCommandHandler
{
    public function getGbTemplateSdkRepository() : IGbTemplateAdapter
    {
        return parent::getGbTemplateSdkRepository();
    }
}
