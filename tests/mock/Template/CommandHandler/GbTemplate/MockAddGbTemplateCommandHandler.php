<?php
namespace Base\Template\CommandHandler\GbTemplate;

use Base\Template\Model\GbTemplate;

class MockAddGbTemplateCommandHandler extends AddGbTemplateCommandHandler
{
    public function getGbTemplate() : GbTemplate
    {
        return parent::getGbTemplate();
    }
}
