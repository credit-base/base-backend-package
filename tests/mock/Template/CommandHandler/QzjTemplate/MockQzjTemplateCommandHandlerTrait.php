<?php
namespace Base\Template\CommandHandler\QzjTemplate;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Template\Model\QzjTemplate;
use Base\Template\Repository\QzjTemplateSdkRepository;

class MockQzjTemplateCommandHandlerTrait
{
    use QzjTemplateCommandHandlerTrait;

    public function publicGetUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicFetchUserGroup(int $id) : UserGroup
    {
        return $this->fetchUserGroup($id);
    }

    public function publicGetQzjTemplateSdkRepository() : QzjTemplateSdkRepository
    {
        return $this->getQzjTemplateSdkRepository();
    }

    public function publicFetchQzjTemplate(int $id) : QzjTemplate
    {
        return $this->fetchQzjTemplate($id);
    }

    public function publicGetQzjTemplate() : QzjTemplate
    {
        return $this->getQzjTemplate();
    }
}
