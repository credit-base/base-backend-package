<?php
namespace Base\Template\CommandHandler\WbjTemplate;

use Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;

class MockEditWbjTemplateCommandHandler extends EditWbjTemplateCommandHandler
{
    public function getWbjTemplateSdkRepository() : IWbjTemplateAdapter
    {
        return parent::getWbjTemplateSdkRepository();
    }
}
