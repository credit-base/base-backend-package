<?php
namespace Base\Template\CommandHandler\WbjTemplate;

use Base\Template\Model\WbjTemplate;

class MockAddWbjTemplateCommandHandler extends AddWbjTemplateCommandHandler
{
    public function getWbjTemplate() : WbjTemplate
    {
        return parent::getWbjTemplate();
    }
}
