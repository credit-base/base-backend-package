<?php
namespace Base\Template\Model;

use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;

class MockBjTemplate extends BjTemplate
{
    public function getRepository() : IBjTemplateAdapter
    {
        return parent::getRepository();
    }
}
