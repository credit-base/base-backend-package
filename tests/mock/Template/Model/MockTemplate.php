<?php
namespace Base\Template\Model;

class MockTemplate extends Template
{
    public function getCategory() : int
    {
        return Template::CATEGORY['BJ'];
    }
}
