<?php
namespace Base\Template\Model;

use Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class MockBaseTemplate extends BaseTemplate
{
    public function getRepository() : IBaseTemplateAdapter
    {
        return parent::getRepository();
    }
}
