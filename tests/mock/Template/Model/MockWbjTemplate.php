<?php
namespace Base\Template\Model;

use Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;

class MockWbjTemplate extends WbjTemplate
{
    public function getRepository() : IWbjTemplateAdapter
    {
        return parent::getRepository();
    }
}
