<?php
namespace Base\Template\Model;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;

class MockGbTemplate extends GbTemplate
{
    public function getRepository() : IGbTemplateAdapter
    {
        return parent::getRepository();
    }
}
