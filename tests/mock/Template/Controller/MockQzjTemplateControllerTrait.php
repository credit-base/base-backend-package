<?php
namespace Base\Template\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Template\WidgetRule\TemplateWidgetRule;
use Base\Template\Repository\QzjTemplateSdkRepository;

class MockQzjTemplateControllerTrait extends Controller
{
    use QzjTemplateControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getTemplateWidgetRulePublic() : TemplateWidgetRule
    {
        return $this->getTemplateWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : QzjTemplateSdkRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    /**
     * @todo
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function validateOperateScenarioPublic(
        $name,
        $identify,
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $description,
        $items,
        $category,
        $sourceUnit
    ) : bool {
        return $this->validateOperateScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $category,
            $sourceUnit
        );
    }
}
