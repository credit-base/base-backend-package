<?php
namespace Base\Template\Controller;

use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Template\WidgetRule\TemplateWidgetRule;
use Base\Template\Repository\WbjTemplateSdkRepository;

class MockWbjTemplateControllerTrait extends Controller
{
    use WbjTemplateControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getTemplateWidgetRulePublic() : TemplateWidgetRule
    {
        return $this->getTemplateWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : WbjTemplateSdkRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
