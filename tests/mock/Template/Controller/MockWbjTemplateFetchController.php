<?php
namespace Base\Template\Controller;

use Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use Marmot\Interfaces\IView;

class MockWbjTemplateFetchController extends WbjTemplateFetchController
{
    public function getRepository() : IWbjTemplateAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
