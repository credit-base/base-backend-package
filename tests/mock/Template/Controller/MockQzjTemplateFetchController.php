<?php
namespace Base\Template\Controller;

use Marmot\Interfaces\IView;
use Base\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;

class MockQzjTemplateFetchController extends QzjTemplateFetchController
{
    public function getRepository() : IQzjTemplateAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
