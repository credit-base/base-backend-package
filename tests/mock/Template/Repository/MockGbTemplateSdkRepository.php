<?php
namespace Base\Template\Repository;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;

class MockGbTemplateSdkRepository extends GbTemplateSdkRepository
{
    public function getAdapter() : IGbTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IGbTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IGbTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}
