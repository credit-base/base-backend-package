<?php
namespace Base\Template\Repository;

use Base\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;

class MockQzjTemplateSdkRepository extends QzjTemplateSdkRepository
{
    public function getAdapter() : IQzjTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IQzjTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IQzjTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}
