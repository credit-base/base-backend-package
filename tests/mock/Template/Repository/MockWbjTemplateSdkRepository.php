<?php
namespace Base\Template\Repository;

use Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;

class MockWbjTemplateSdkRepository extends WbjTemplateSdkRepository
{
    public function getAdapter() : IWbjTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IWbjTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IWbjTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}
