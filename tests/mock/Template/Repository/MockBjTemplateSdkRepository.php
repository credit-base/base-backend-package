<?php
namespace Base\Template\Repository;

use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;

class MockBjTemplateSdkRepository extends BjTemplateSdkRepository
{
    public function getAdapter() : IBjTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IBjTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IBjTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}
