<?php
namespace Base\Template\Repository;

use Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class MockBaseTemplateSdkRepository extends BaseTemplateSdkRepository
{
    public function getAdapter() : IBaseTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IBaseTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IBaseTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}
