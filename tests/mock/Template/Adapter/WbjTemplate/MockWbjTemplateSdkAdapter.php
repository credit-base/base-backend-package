<?php
namespace Base\Template\Adapter\WbjTemplate;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

use Base\Template\Model\WbjTemplate;

use Base\UserGroup\Repository\UserGroupRepository;

class MockWbjTemplateSdkAdapter extends WbjTemplateSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function fetchSourceUnit($wbjTemplate)
    {
        return parent::fetchSourceUnit($wbjTemplate);
    }

    public function fetchSourceUnitByObject(WbjTemplate $wbjTemplate)
    {
        return parent::fetchSourceUnitByObject($wbjTemplate);
    }

    public function fetchSourceUnitByList(array $wbjTemplateList)
    {
        return parent::fetchSourceUnitByList($wbjTemplateList);
    }
}
