<?php
namespace Base\Template\Adapter\BjTemplate;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

use Base\Template\Model\BjTemplate;

use Base\UserGroup\Repository\UserGroupRepository;

class MockBjTemplateSdkAdapter extends BjTemplateSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function fetchSourceUnit($bjTemplate)
    {
        return parent::fetchSourceUnit($bjTemplate);
    }

    public function fetchSourceUnitByObject(BjTemplate $bjTemplate)
    {
        return parent::fetchSourceUnitByObject($bjTemplate);
    }

    public function fetchSourceUnitByList(array $bjTemplateList)
    {
        return parent::fetchSourceUnitByList($bjTemplateList);
    }
}
