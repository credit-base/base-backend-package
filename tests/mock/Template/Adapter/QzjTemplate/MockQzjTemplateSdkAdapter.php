<?php
namespace Base\Template\Adapter\QzjTemplate;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

use Base\Template\Model\QzjTemplate;

use Base\UserGroup\Repository\UserGroupRepository;

class MockQzjTemplateSdkAdapter extends QzjTemplateSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function fetchSourceUnit($qzjTemplate)
    {
        return parent::fetchSourceUnit($qzjTemplate);
    }

    public function fetchSourceUnitByObject(QzjTemplate $qzjTemplate)
    {
        return parent::fetchSourceUnitByObject($qzjTemplate);
    }

    public function fetchSourceUnitByList(array $qzjTemplateList)
    {
        return parent::fetchSourceUnitByList($qzjTemplateList);
    }
}
