<?php
namespace Base\Template\Adapter\GbTemplate;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

class MockGbTemplateSdkAdapter extends GbTemplateSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }
}
