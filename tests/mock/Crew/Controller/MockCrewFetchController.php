<?php
namespace Base\Crew\Controller;

use Marmot\Interfaces\IView;

use Base\Crew\Adapter\Crew\ICrewAdapter;

class MockCrewFetchController extends CrewFetchController
{
    public function getRepository() : ICrewAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
