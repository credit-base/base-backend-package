<?php
namespace Base\Crew\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Crew\Adapter\Crew\ICrewAdapter;

class MockCrewEnableController extends CrewEnableController
{
    public function getRepository() : ICrewAdapter
    {
        return parent::getRepository();
    }

    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
