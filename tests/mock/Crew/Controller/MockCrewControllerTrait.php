<?php
namespace Base\Crew\Controller;

use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\User\WidgetRule\UserWidgetRule;
use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Crew\WidgetRule\CrewWidgetRule;
use Base\Crew\Repository\CrewRepository;

class MockCrewControllerTrait extends Controller
{
    use CrewControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getCrewWidgetRulePublic() : CrewWidgetRule
    {
        return $this->getCrewWidgetRule();
    }

    public function getUserWidgetRulePublic() : UserWidgetRule
    {
        return $this->getUserWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : CrewRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
