<?php
namespace Base\Crew\Model;

use Base\Crew\Adapter\Crew\ICrewAdapter;

class MockCrew extends Crew
{
    public function getRepository() : ICrewAdapter
    {
        return parent::getRepository();
    }

    public function updatePassword(string $newPassword) : bool
    {
        return parent::updatePassword($newPassword);
    }

    public function verifyPassword(string $oldPassword) : bool
    {
        return parent::verifyPassword($oldPassword);
    }
}
