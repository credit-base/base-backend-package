<?php
namespace Base\Crew\Adapter\Crew;

use Marmot\Interfaces\INull;

use Base\Crew\Translator\CrewDbTranslator;
use Base\Crew\Adapter\Crew\Query\CrewRowCacheQuery;

use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use Base\Department\Adapter\Department\IDepartmentAdapter;

class MockCrewDbAdapter extends CrewDbAdapter
{
    public function getDbTranslator() : CrewDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : CrewRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getUserGroupRepository() : IUserGroupAdapter
    {
        return parent::getUserGroupRepository();
    }

    public function getDepartmentRepository() : IDepartmentAdapter
    {
        return parent::getDepartmentRepository();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }

    public function fetchUserGroup($crew)
    {
        return parent::fetchUserGroup($crew);
    }

    public function fetchUserGroupByObject($crew)
    {
        return parent::fetchUserGroupByObject($crew);
    }

    public function fetchUserGroupByList(array $crewList)
    {
        return parent::fetchUserGroupByList($crewList);
    }

    public function fetchDepartment($crew)
    {
        return parent::fetchDepartment($crew);
    }

    public function fetchDepartmentByObject($crew)
    {
        return parent::fetchDepartmentByObject($crew);
    }

    public function fetchDepartmentByList(array $crewList)
    {
        return parent::fetchDepartmentByList($crewList);
    }
}
