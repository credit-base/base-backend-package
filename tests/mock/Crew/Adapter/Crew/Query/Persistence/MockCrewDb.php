<?php
namespace Base\Crew\Adapter\Crew\Query\Persistence;

class MockCrewDb extends CrewDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
