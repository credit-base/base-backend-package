<?php
namespace Base\Crew\Adapter\Crew\Query\Persistence;

class MockCrewCache extends CrewCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
