<?php
namespace Base\Crew\CommandHandler\Crew;

use Base\Common\Model\IEnableAble;

class MockDisableCrewCommandHandler extends DisableCrewCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
