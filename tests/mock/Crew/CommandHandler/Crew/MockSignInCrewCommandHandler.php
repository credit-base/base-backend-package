<?php
namespace Base\Crew\CommandHandler\Crew;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Command\Crew\SignInCrewCommand;

class MockSignInCrewCommandHandler extends SignInCrewCommandHandler
{
    public function getRepository() : CrewRepository
    {
        return parent::getRepository();
    }

    public function searchCrews($userName) : array
    {
        return parent::searchCrews($userName);
    }

    public function isCrewExist(int $count) : bool
    {
        return parent::isCrewExist($count);
    }

    public function isCrewDisabled(Crew $crew) : bool
    {
        return parent::isCrewDisabled($crew);
    }

    public function crewPasswordCorrect(Crew $crew, SignInCrewCommand $command) : bool
    {
        return parent::crewPasswordCorrect($crew, $command);
    }
}
