<?php
namespace Base\Crew\CommandHandler\Crew;

use Base\Crew\Model\Crew;

class MockAddCrewCommandHandler extends AddCrewCommandHandler
{
    public function getCrew(int $category = 0) : Crew
    {
        return parent::getCrew($category);
    }
}
