<?php
namespace Base\Crew\CommandHandler\Crew;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;

class MockEditCrewCommandHandler extends EditCrewCommandHandler
{
    public function getRepository() : CrewRepository
    {
        return parent::getRepository();
    }

    public function fetchCrew(int $id) : Crew
    {
        return parent::fetchCrew($id);
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function fetchUserGroup(int $id) : UserGroup
    {
        return parent::fetchUserGroup($id);
    }

    public function getDepartmentRepository() : DepartmentRepository
    {
        return parent::getDepartmentRepository();
    }

    public function fetchDepartment(int $id) : Department
    {
        return parent::fetchDepartment($id);
    }
}
