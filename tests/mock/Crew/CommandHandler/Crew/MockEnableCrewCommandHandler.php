<?php
namespace Base\Crew\CommandHandler\Crew;

use Base\Common\Model\IEnableAble;

class MockEnableCrewCommandHandler extends EnableCrewCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
