<?php
namespace Base\Rule\CommandHandler\RuleService;

use Base\Common\Model\IApproveAble;
use Base\Common\Command\RejectCommand;

class MockRejectRuleServiceCommandHandler extends RejectRuleServiceCommandHandler
{
    public function executeAction(RejectCommand $command)
    {
        return parent::executeAction($command);
    }

    public function fetchIApplyObject($id) : IApproveAble
    {
        return parent::fetchIApplyObject($id);
    }
}
