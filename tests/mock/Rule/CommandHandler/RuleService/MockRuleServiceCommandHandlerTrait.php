<?php
namespace Base\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Repository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\Rule\Model\RuleService;
use Base\Rule\Model\ModelFactory;
use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\Repository\RepositoryFactory;
use Base\Rule\Repository\RuleServiceSdkRepository;
use Base\Rule\Translator\RuleServiceDbTranslator;
use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;

class MockRuleServiceCommandHandlerTrait
{
    use RuleServiceCommandHandlerTrait;

    public function getCrewRepositoryPublic() : CrewRepository
    {
        return $this->getCrewRepository();
    }

    public function fetchCrewPublic(int $id) : Crew
    {
        return $this->fetchCrew($id);
    }

    public function getRuleServiceSdkRepositoryPublic() : RuleServiceSdkRepository
    {
        return $this->getRuleServiceSdkRepository();
    }

    public function getRuleServicePublic() : RuleService
    {
        return $this->getRuleService();
    }

    public function fetchRuleServicePublic(int $id) : RuleService
    {
        return $this->fetchRuleService($id);
    }

    public function getUnAuditedRuleServiceSdkRepositoryPublic() : UnAuditedRuleServiceSdkRepository
    {
        return $this->getUnAuditedRuleServiceSdkRepository();
    }

    public function fetchUnAuditedRuleServicePublic(int $id) : UnAuditedRuleService
    {
        return $this->fetchUnAuditedRuleService($id);
    }

    public function getUnAuditedRuleServicePublic() : UnAuditedRuleService
    {
        return $this->getUnAuditedRuleService();
    }

    public function getRepositoryFactoryPublic() : RepositoryFactory
    {
        return $this->getRepositoryFactory();
    }

    public function getRepositoryPublic(int $category) : Repository
    {
        return $this->getRepository($category);
    }
}
