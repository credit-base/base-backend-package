<?php
namespace Base\Rule\Model;

class MockNullRuleService extends NullRuleService
{
    public function validate() : bool
    {
        return parent::validate();
    }
    
    public function isCrewNull() : bool
    {
        return parent::isCrewNull();
    }

    public function isSourceTemplateNull() : bool
    {
        return parent::isSourceTemplateNull();
    }

    public function isTransformationTemplateNull() : bool
    {
        return parent::isTransformationTemplateNull();
    }

    public function isRuleNotExist() : bool
    {
        return parent::isRuleNotExist();
    }
}
