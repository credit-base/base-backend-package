<?php
namespace Base\Rule\Model;

use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;

class MockUnAuditedRuleService extends UnAuditedRuleService
{
    public function getUnAuditedRuleServiceSdkRepository() : UnAuditedRuleServiceSdkRepository
    {
        return parent::getUnAuditedRuleServiceSdkRepository();
    }

    public function resubmitAction() : bool
    {
        return parent::resubmitAction();
    }

    public function approveAction() : bool
    {
        return parent::approveAction();
    }

    public function rejectAction() : bool
    {
        return parent::rejectAction();
    }
}
