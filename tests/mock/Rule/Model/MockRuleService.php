<?php
namespace Base\Rule\Model;

use Base\Rule\Adapter\RuleService\IRuleServiceAdapter;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
*/
class MockRuleService extends RuleService
{
    public function getRepository() : IRuleServiceAdapter
    {
        return parent::getRepository();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function validate() : bool
    {
        return parent::validate();
    }

    public function isCrewExist() : bool
    {
        return parent::isCrewExist();
    }

    public function isSourceTemplateExist() : bool
    {
        return parent::isSourceTemplateExist();
    }

    public function isTransformationTemplateExist() : bool
    {
        return parent::isTransformationTemplateExist();
    }

    public function isRuleNotExist() : bool
    {
        return parent::isRuleNotExist();
    }
}
