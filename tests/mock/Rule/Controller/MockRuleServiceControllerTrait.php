<?php
namespace Base\Rule\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Rule\WidgetRule\RuleServiceWidgetRule;
use Base\Rule\Repository\RuleServiceSdkRepository;
use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;

class MockRuleServiceControllerTrait extends Controller
{
    use RuleServiceControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getRuleServiceWidgetRulePublic() : RuleServiceWidgetRule
    {
        return $this->getRuleServiceWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : RuleServiceSdkRepository
    {
        return $this->getRepository();
    }

    public function getUnAuditedRuleServiceSdkRepositoryPublic() : UnAuditedRuleServiceSdkRepository
    {
        return $this->getUnAuditedRuleServiceSdkRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    public function validateCommonScenarioPublic($rules, $crew)
    {
        return $this->validateCommonScenario($rules, $crew);
    }
    
    public function validateRejectScenarioPublic(
        $rejectReason,
        $applyCrew
    ) {
        return $this->validateRejectScenario(
            $rejectReason,
            $applyCrew
        );
    }

    public function validateApproveScenarioPublic($applyCrew)
    {
        return $this->validateApproveScenario($applyCrew);
    }

    public function validateStatusScenarioPublic($status)
    {
        return $this->validateStatusScenario($status);
    }
    
    public function validateAddScenarioPublic(
        $transformationTemplate,
        $sourceTemplate,
        $transformationCategory,
        $sourceCategory
    ) {
        return $this->validateAddScenario(
            $transformationTemplate,
            $sourceTemplate,
            $transformationCategory,
            $sourceCategory
        );
    }
}
