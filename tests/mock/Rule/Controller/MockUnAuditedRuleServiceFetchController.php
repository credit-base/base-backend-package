<?php
namespace Base\Rule\Controller;

use Marmot\Interfaces\IView;

use Base\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;

class MockUnAuditedRuleServiceFetchController extends UnAuditedRuleServiceFetchController
{
    public function getRepository() : IUnAuditedRuleServiceAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
