<?php
namespace Base\Rule\Controller;

use Marmot\Interfaces\IView;

use Base\Rule\Adapter\RuleService\IRuleServiceAdapter;

class MockRuleServiceFetchController extends RuleServiceFetchController
{
    public function getRepository() : IRuleServiceAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
