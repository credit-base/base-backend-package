<?php
namespace Base\Rule\Repository;

use Base\Rule\Adapter\RuleService\IRuleServiceAdapter;

class MockRuleServiceSdkRepository extends RuleServiceSdkRepository
{
    public function getAdapter() : IRuleServiceAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IRuleServiceAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IRuleServiceAdapter
    {
        return parent::getMockAdapter();
    }
}
