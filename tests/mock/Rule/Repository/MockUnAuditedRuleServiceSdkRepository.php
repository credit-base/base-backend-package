<?php
namespace Base\Rule\Repository;

use Base\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;

class MockUnAuditedRuleServiceSdkRepository extends UnAuditedRuleServiceSdkRepository
{
    public function getAdapter() : IUnAuditedRuleServiceAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IUnAuditedRuleServiceAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IUnAuditedRuleServiceAdapter
    {
        return parent::getMockAdapter();
    }
}
