<?php
namespace Base\Rule\Translator;

use Base\Common\Translator\ISdkTranslator;

use Base\Template\Translator\TranslatorFactory;

class MockRuleServiceSdkTranslator extends RuleServiceSdkTranslator
{
    public function getCrewSdkTranslator() : ISdkTranslator
    {
        return parent::getCrewSdkTranslator();
    }

    public function getUserGroupSdkTranslator() : ISdkTranslator
    {
        return parent::getUserGroupSdkTranslator();
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return parent::getTranslatorFactory();
    }

    public function getTemplateSdkTranslator(int $category) : ISdkTranslator
    {
        return parent::getTemplateSdkTranslator($category);
    }
}
