<?php
namespace Base\Rule\Adapter\RuleService;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

use Base\Rule\Model\RuleService;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

class MockRuleServiceSdkAdapter extends RuleServiceSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function fetchCrew($ruleService)
    {
        return parent::fetchCrew($ruleService);
    }

    public function fetchCrewByObject(RuleService $ruleService)
    {
        return parent::fetchCrewByObject($ruleService);
    }

    public function fetchCrewByList(array $ruleServiceList)
    {
        return parent::fetchCrewByList($ruleServiceList);
    }

    public function fetchUserGroup($ruleService)
    {
        return parent::fetchUserGroup($ruleService);
    }

    public function fetchUserGroupByObject(RuleService $ruleService)
    {
        return parent::fetchUserGroupByObject($ruleService);
    }

    public function fetchUserGroupByList(array $ruleServiceList)
    {
        return parent::fetchUserGroupByList($ruleServiceList);
    }
}
