<?php
namespace Base\Rule\Adapter\UnAuditedRuleService;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

use Base\Rule\Model\UnAuditedRuleService;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

class MockUnAuditedRuleServiceSdkAdapter extends UnAuditedRuleServiceSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function fetchCrew($unAuditedRuleService)
    {
        return parent::fetchCrew($unAuditedRuleService);
    }

    public function fetchCrewByObject(UnAuditedRuleService $unAuditedRuleService)
    {
        return parent::fetchCrewByObject($unAuditedRuleService);
    }

    public function fetchCrewByList(array $unAuditedRuleServiceList)
    {
        return parent::fetchCrewByList($unAuditedRuleServiceList);
    }

    public function fetchApplyCrew($unAuditedRuleService)
    {
        return parent::fetchApplyCrew($unAuditedRuleService);
    }

    public function fetchApplyCrewByObject(UnAuditedRuleService $unAuditedRuleService)
    {
        return parent::fetchApplyCrewByObject($unAuditedRuleService);
    }

    public function fetchApplyCrewByList(array $unAuditedRuleServiceList)
    {
        return parent::fetchApplyCrewByList($unAuditedRuleServiceList);
    }

    public function fetchUserGroup($unAuditedRuleService)
    {
        return parent::fetchUserGroup($unAuditedRuleService);
    }

    public function fetchUserGroupByObject(UnAuditedRuleService $unAuditedRuleService)
    {
        return parent::fetchUserGroupByObject($unAuditedRuleService);
    }

    public function fetchUserGroupByList(array $unAuditedRuleServiceList)
    {
        return parent::fetchUserGroupByList($unAuditedRuleServiceList);
    }
}
