<?php
namespace Base\User\Model;

class MockUser extends User
{
    public function updatePassword(string $newPassword) : bool
    {
        unset($newPassword);
        return false;
    }

    public function verifyPassword(string $oldPassword) : bool
    {
        unset($oldPassword);
        return false;
    }
}
