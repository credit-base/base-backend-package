<?php
namespace Base\Enterprise\Repository;

use Base\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;

class MockEnterpriseRepository extends EnterpriseSdkRepository
{
    public function getAdapter() : IEnterpriseAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IEnterpriseAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IEnterpriseAdapter
    {
        return parent::getMockAdapter();
    }
}
