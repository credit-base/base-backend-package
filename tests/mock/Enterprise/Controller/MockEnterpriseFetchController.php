<?php
namespace Base\Enterprise\Controller;

use Marmot\Interfaces\IView;

use Base\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;

class MockEnterpriseFetchController extends EnterpriseFetchController
{
    public function getRepository() : IEnterpriseAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
