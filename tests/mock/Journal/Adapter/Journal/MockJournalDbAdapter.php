<?php
namespace Base\Journal\Adapter\Journal;

use Marmot\Interfaces\INull;

use Base\Journal\Translator\JournalDbTranslator;
use Base\Journal\Adapter\Journal\Query\JournalRowCacheQuery;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

class MockJournalDbAdapter extends JournalDbAdapter
{
    public function getDbTranslator() : JournalDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : JournalRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function fetchPublishUserGroup($journal)
    {
        return parent::fetchPublishUserGroup($journal);
    }

    public function fetchPublishUserGroupByObject($journal)
    {
        return parent::fetchPublishUserGroupByObject($journal);
    }

    public function fetchPublishUserGroupByList(array $journalList)
    {
        return parent::fetchPublishUserGroupByList($journalList);
    }

    public function fetchCrew($journal)
    {
        return parent::fetchCrew($journal);
    }

    public function fetchCrewByObject($journal)
    {
        return parent::fetchCrewByObject($journal);
    }

    public function fetchCrewByList(array $journalList)
    {
        return parent::fetchCrewByList($journalList);
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
