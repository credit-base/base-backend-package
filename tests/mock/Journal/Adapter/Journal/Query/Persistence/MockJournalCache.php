<?php
namespace Base\Journal\Adapter\Journal\Query\Persistence;

class MockJournalCache extends JournalCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
