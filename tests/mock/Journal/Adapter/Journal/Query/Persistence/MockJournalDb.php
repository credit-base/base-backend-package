<?php
namespace Base\Journal\Adapter\Journal\Query\Persistence;

class MockJournalDb extends JournalDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
