<?php
namespace Base\Journal\Model;

use Base\Journal\Adapter\Journal\IJournalAdapter;

class MockJournal extends Journal
{
    public function getRepository() : IJournalAdapter
    {
        return parent::getRepository();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function updateStatus(int $status) : bool
    {
        return parent::updateStatus($status);
    }

    public function enable() : bool
    {
        return parent::enable();
    }
}
