<?php
namespace Base\Journal\Controller;

use Base\Journal\Repository\UnAuditedJournalRepository;
use Marmot\Interfaces\IView;

class MockUnAuditedJournalFetchController extends UnAuditedJournalFetchController
{
    public function getRepository() : UnAuditedJournalRepository
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
