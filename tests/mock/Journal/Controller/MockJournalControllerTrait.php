<?php
namespace Base\Journal\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Journal\WidgetRule\JournalWidgetRule;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Repository\UnAuditedJournalRepository;

class MockJournalControllerTrait extends Controller
{
    use JournalControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getJournalWidgetRulePublic() : JournalWidgetRule
    {
        return $this->getJournalWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : JournalRepository
    {
        return $this->getRepository();
    }

    public function getUnAuditedJournalRepositoryPublic() : UnAuditedJournalRepository
    {
        return $this->getUnAuditedJournalRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    public function validateCommonScenarioPublic($crew)
    {
        return $this->validateCommonScenario($crew);
    }
    
    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    public function validateJournalScenarioPublic(
        $title,
        $source,
        $description,
        $cover,
        $attachment,
        $authImages,
        $year,
        $status,
        $crew
    ) {
        return $this->validateJournalScenario(
            $title,
            $source,
            $description,
            $cover,
            $attachment,
            $authImages,
            $year,
            $status,
            $crew
        );
    }
}
