<?php
namespace Base\Journal\Controller;

use Base\ApplyForm\Model\IApplyFormAble;

class MockJournalApproveController extends JournalApproveController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }
}
