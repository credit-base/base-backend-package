<?php
namespace Base\Journal\Controller;

use Base\Journal\Adapter\Journal\IJournalAdapter;
use Marmot\Interfaces\IView;

class MockJournalFetchController extends JournalFetchController
{
    public function getRepository() : IJournalAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
