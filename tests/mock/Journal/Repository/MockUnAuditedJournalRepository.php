<?php
namespace Base\Journal\Repository;

use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class MockUnAuditedJournalRepository extends UnAuditedJournalRepository
{
    public function getAdapter() : IApplyFormAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IApplyFormAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IApplyFormAdapter
    {
        return parent::getMockAdapter();
    }
}
