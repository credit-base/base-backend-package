<?php
namespace Base\Journal\Repository;

use Base\Journal\Adapter\Journal\IJournalAdapter;

class MockJournalRepository extends JournalRepository
{
    public function getAdapter() : IJournalAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IJournalAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IJournalAdapter
    {
        return parent::getMockAdapter();
    }
}
