<?php
namespace Base\Journal\CommandHandler\Journal;

use Base\Common\Model\IEnableAble;

use Base\Common\Command\DisableCommand;

class MockDisableJournalCommandHandler extends DisableJournalCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }

    public function executeAction(DisableCommand $command)
    {
        return parent::executeAction($command);
    }
}
