<?php
namespace Base\Journal\CommandHandler\Journal;

use Marmot\Interfaces\ICommand;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\Journal\Model\Journal;
use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Translator\JournalDbTranslator;
use Base\Journal\Repository\UnAuditedJournalRepository;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MockJournalCommandHandlerTrait
{
    use JournalCommandHandlerTrait;

    public function publicGetCrewRepository() : CrewRepository
    {
        return $this->getCrewRepository();
    }

    public function publicFetchCrew(int $id) : Crew
    {
        return $this->fetchCrew($id);
    }

    public function publicGetJournalRepository() : JournalRepository
    {
        return $this->getJournalRepository();
    }

    public function publicGetJournalDbTranslator() : JournalDbTranslator
    {
        return $this->getJournalDbTranslator();
    }

    public function publicFetchJournal(int $id) : Journal
    {
        return $this->fetchJournal($id);
    }

    public function publicGetUnAuditedJournalRepository() : UnAuditedJournalRepository
    {
        return $this->getUnAuditedJournalRepository();
    }

    public function publicFetchUnAuditedJournal(int $id) : UnAuditedJournal
    {
        return $this->fetchUnAuditedJournal($id);
    }

    public function publicGetJournal() : Journal
    {
        return $this->getJournal();
    }

    public function publicGetUnAuditedJournal() : UnAuditedJournal
    {
        return $this->getUnAuditedJournal();
    }

    public function publicExecuteAction(ICommand $command, UnAuditedJournal $unAuditedJournal) : UnAuditedJournal
    {
        return $this->executeAction($command, $unAuditedJournal);
    }

    public function publicJournalExecuteAction(ICommand $command, UnAuditedJournal $unAuditedJournal) : UnAuditedJournal
    {
        return $this->journalExecuteAction($command, $unAuditedJournal);
    }

    public function publicApplyInfo($unAuditedJournal) : array
    {
        return $this->applyInfo($unAuditedJournal);
    }
}
