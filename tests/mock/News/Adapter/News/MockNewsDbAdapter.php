<?php
namespace Base\News\Adapter\News;

use Marmot\Interfaces\INull;

use Marmot\Interfaces\ITranslator;
use Base\News\Adapter\News\Query\NewsRowCacheQuery;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

class MockNewsDbAdapter extends NewsDbAdapter
{
    public function getDbTranslator() : ITranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : NewsRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function fetchPublishUserGroup($news)
    {
        return parent::fetchPublishUserGroup($news);
    }

    public function fetchPublishUserGroupByObject($news)
    {
        return parent::fetchPublishUserGroupByObject($news);
    }

    public function fetchPublishUserGroupByList(array $newsList)
    {
        return parent::fetchPublishUserGroupByList($newsList);
    }

    public function fetchCrew($news)
    {
        return parent::fetchCrew($news);
    }

    public function fetchCrewByObject($news)
    {
        return parent::fetchCrewByObject($news);
    }

    public function fetchCrewByList(array $newsList)
    {
        return parent::fetchCrewByList($newsList);
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
