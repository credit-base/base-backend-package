<?php
namespace Base\News\Adapter\News\Query\Persistence;

class MockNewsDb extends NewsDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
