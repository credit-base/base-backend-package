<?php
namespace Base\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\News\Model\News;
use Base\News\Model\UnAuditedNews;
use Base\News\Repository\NewsRepository;
use Base\News\Translator\NewsDbTranslator;
use Base\News\Repository\UnAuditedNewsRepository;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MockNewsCommandHandlerTrait
{
    use NewsCommandHandlerTrait;

    public function publicGetCrewRepository() : CrewRepository
    {
        return $this->getCrewRepository();
    }

    public function publicFetchCrew(int $id) : Crew
    {
        return $this->fetchCrew($id);
    }

    public function publicGetNewsRepository() : NewsRepository
    {
        return $this->getNewsRepository();
    }

    public function publicGetNewsDbTranslator() : NewsDbTranslator
    {
        return $this->getNewsDbTranslator();
    }

    public function publicFetchNews(int $id) : News
    {
        return $this->fetchNews($id);
    }

    public function publicGetUnAuditedNewsRepository() : UnAuditedNewsRepository
    {
        return $this->getUnAuditedNewsRepository();
    }

    public function publicFetchUnAuditedNews(int $id) : UnAuditedNews
    {
        return $this->fetchUnAuditedNews($id);
    }

    public function publicGetNews() : News
    {
        return $this->getNews();
    }

    public function publicGetUnAuditedNews() : UnAuditedNews
    {
        return $this->getUnAuditedNews();
    }

    public function publicExecuteAction(ICommand $command, UnAuditedNews $unAuditedNews) : UnAuditedNews
    {
        return $this->executeAction($command, $unAuditedNews);
    }

    public function publicNewsExecuteAction(ICommand $command, UnAuditedNews $unAuditedNews) : UnAuditedNews
    {
        return $this->newsExecuteAction($command, $unAuditedNews);
    }

    public function publicApplyInfo($unAuditedNews) : array
    {
        return $this->applyInfo($unAuditedNews);
    }
}
