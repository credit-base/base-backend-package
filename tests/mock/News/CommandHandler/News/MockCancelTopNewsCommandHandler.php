<?php
namespace Base\News\CommandHandler\News;

use Base\Common\Model\ITopAble;

use Base\Common\Command\CancelTopCommand;

class MockCancelTopNewsCommandHandler extends CancelTopNewsCommandHandler
{
    public function fetchITopObject($id) : ITopAble
    {
        return parent::fetchITopObject($id);
    }

    public function executeAction(CancelTopCommand $command)
    {
        return parent::executeAction($command);
    }
}
