<?php
namespace Base\News\CommandHandler\News;

use Base\Common\Model\IEnableAble;

use Base\Common\Command\DisableCommand;

class MockDisableNewsCommandHandler extends DisableNewsCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }

    public function executeAction(DisableCommand $command)
    {
        return parent::executeAction($command);
    }
}
