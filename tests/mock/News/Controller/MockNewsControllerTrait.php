<?php
namespace Base\News\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\News\WidgetRule\NewsWidgetRule;
use Base\News\Repository\NewsRepository;
use Base\News\Repository\UnAuditedNewsRepository;

class MockNewsControllerTrait extends Controller
{
    use NewsControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getNewsWidgetRulePublic() : NewsWidgetRule
    {
        return $this->getNewsWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : NewsRepository
    {
        return $this->getRepository();
    }

    public function getUnAuditedNewsRepositoryPublic() : UnAuditedNewsRepository
    {
        return $this->getUnAuditedNewsRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    public function validateCommonScenarioPublic($crew)
    {
        return $this->validateCommonScenario($crew);
    }
    
    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    public function validateNewsScenarioPublic(
        $title,
        $source,
        $cover,
        $attachments,
        $content,
        $newsType,
        $dimension,
        $status,
        $stick,
        $bannerStatus,
        $bannerImage,
        $homePageShowStatus,
        $crew
    ) {
        return $this->validateNewsScenario(
            $title,
            $source,
            $cover,
            $attachments,
            $content,
            $newsType,
            $dimension,
            $status,
            $stick,
            $bannerStatus,
            $bannerImage,
            $homePageShowStatus,
            $crew
        );
    }

    public function validateMoveScenarioPublic($newsType, $crew)
    {
        return $this->validateMoveScenario($newsType, $crew);
    }
}
