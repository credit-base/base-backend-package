<?php
namespace Base\News\Controller;

use Marmot\Interfaces\IView;

class MockUnAuditedNewsFetchController extends UnAuditedNewsFetchController
{
    public function getRepository()
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
