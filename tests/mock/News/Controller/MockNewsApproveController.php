<?php
namespace Base\News\Controller;

use Base\ApplyForm\Model\IApplyFormAble;

class MockNewsApproveController extends NewsApproveController
{
    public function displaySuccess(IApplyFormAble $applyForm)
    {
        return parent::displaySuccess($applyForm);
    }
}
