<?php
namespace Base\News\Controller;

use Base\News\Adapter\News\INewsAdapter;
use Marmot\Interfaces\IView;

class MockNewsFetchController extends NewsFetchController
{
    public function getRepository() : INewsAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
