<?php
namespace Base\News\Model;

use Base\News\Adapter\News\INewsAdapter;

class MockNews extends News
{
    public function getRepository() : INewsAdapter
    {
        return parent::getRepository();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function updateStatus(int $status) : bool
    {
        return parent::updateStatus($status);
    }

    public function updateStick(int $stick) : bool
    {
        return parent::updateStick($stick);
    }

    public function enable() : bool
    {
        return parent::enable();
    }

    public function top() : bool
    {
        return parent::top();
    }
}
