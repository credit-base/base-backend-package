<?php
namespace Base\News\Repository;

use Base\News\Adapter\News\INewsAdapter;

class MockNewsRepository extends NewsRepository
{
    public function getAdapter() : INewsAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : INewsAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : INewsAdapter
    {
        return parent::getMockAdapter();
    }
}
