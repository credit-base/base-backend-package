<?php
namespace Base\News\Repository;

use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class MockUnAuditedNewsRepository extends UnAuditedNewsRepository
{
    public function getAdapter() : IApplyFormAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IApplyFormAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IApplyFormAdapter
    {
        return parent::getMockAdapter();
    }
}
