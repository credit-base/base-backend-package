<?php
namespace Base\UserGroup\Controller;

use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use Marmot\Interfaces\IView;

class MockUserGroupFetchController extends UserGroupFetchController
{
    public function getRepository() : IUserGroupAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
