<?php
namespace Base\UserGroup\Adapter\UserGroup\Query\Persistence;

class MockUserGroupCache extends UserGroupCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
