<?php
namespace Base\UserGroup\Adapter\UserGroup\Query\Persistence;

class MockUserGroupDb extends UserGroupDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
