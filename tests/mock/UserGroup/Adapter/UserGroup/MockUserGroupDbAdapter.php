<?php
namespace Base\UserGroup\Adapter\UserGroup;

use Marmot\Interfaces\INull;

use Base\UserGroup\Translator\UserGroupDbTranslator;
use Base\UserGroup\Adapter\UserGroup\Query\UserGroupRowCacheQuery;

class MockUserGroupDbAdapter extends UserGroupDbAdapter
{
    public function getDbTranslator() : UserGroupDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : UserGroupRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
