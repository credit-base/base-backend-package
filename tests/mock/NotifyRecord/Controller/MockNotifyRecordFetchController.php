<?php
namespace Base\NotifyRecord\Controller;

use Marmot\Interfaces\IView;

use Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter;

class MockNotifyRecordFetchController extends NotifyRecordFetchController
{
    public function getRepository() : INotifyRecordAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
