<?php
namespace Base\NotifyRecord\Adapter\NotifyRecord;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

class MockNotifyRecordSdkAdapter extends NotifyRecordSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }
}
