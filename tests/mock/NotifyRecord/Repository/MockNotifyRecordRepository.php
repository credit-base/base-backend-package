<?php
namespace Base\NotifyRecord\Repository;

use Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter;

class MockNotifyRecordRepository extends NotifyRecordSdkRepository
{
    public function getAdapter() : INotifyRecordAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : INotifyRecordAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : INotifyRecordAdapter
    {
        return parent::getMockAdapter();
    }
}
