<?php
namespace Base\Statistical\Controller;

use Base\Statistical\Repository\StatisticalRepository;

class MockStatisticalController extends StatisticalController
{
    public function getStatisticalRepository(string $type) : StatisticalRepository
    {
        return parent::getStatisticalRepository($type);
    }
}
