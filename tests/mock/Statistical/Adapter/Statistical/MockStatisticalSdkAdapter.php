<?php
namespace Base\Statistical\Adapter\Statistical;

use Base\Statistical\Model\Statistical;

use Base\Common\Translator\ISdkTranslator;
use BaseSdk\Statistical\Adapter\Statistical\IStatisticalAdapter;

class MockStatisticalSdkAdapter extends StatisticalSdkAdapter
{
    protected function getRestfulAdapter() : IStatisticalAdapter
    {
    }

    public function analyse(array $filter = array()) : Statistical
    {
        unset($filter);
        return new Statistical();
    }

    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }
}
