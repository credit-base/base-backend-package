<?php
namespace Base\Statistical\Adapter\Statistical;

use Base\Statistical\Translator\StaticsCreditPhotographyTranslator;

use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;
use Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence\CreditPhotographyDb;

class MockStaticsCreditPhotographyAdapter extends StaticsCreditPhotographyAdapter
{
    public function getStaticsCreditPhotographyTranslator() : StaticsCreditPhotographyTranslator
    {
        return parent::getStaticsCreditPhotographyTranslator();
    }

    public function getCreditPhotographyDb() : CreditPhotographyDb
    {
        return parent::getCreditPhotographyDb();
    }

    public function getCreditPhotographyDbTranslator() : CreditPhotographyDbTranslator
    {
        return parent::getCreditPhotographyDbTranslator();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatGroupBy(array $groupBy) : array
    {
        return parent::formatGroupBy($groupBy);
    }
}
