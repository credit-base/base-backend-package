<?php
namespace Base\Statistical\Adapter\Statistical;

use BaseSdk\Statistical\Adapter\Statistical\IStatisticalAdapter;

class MockEnterpriseRelationInformationCountSdkAdapter extends EnterpriseRelationInformationCountSdkAdapter
{
    public function getRestfulAdapter() : IStatisticalAdapter
    {
        return parent::getRestfulAdapter();
    }
}
