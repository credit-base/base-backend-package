<?php
namespace Base\Common\Repository;

use Marmot\Common\Model\IObject;
use Marmot\Framework\Classes\Repository;

class MockRepository extends Repository
{
    protected function getActualAdapter()
    {
    }

    protected function getMockAdapter()
    {
    }

    public function scenario($scenario = array())
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function add(IObject $object) : bool
    {
        unset($object);
        return true;
    }

    public function edit(IObject $object, array $keys = array()) : bool
    {
        unset($object);
        unset($keys);
        return true;
    }

    public function fetchOne($id)
    {
        unset($id);
        $object = $this->getMockBuilder(IObject::class)->getMock();

        return $object;
    }

    public function fetchList(array $ids) : array
    {
        unset($ids);
        return array();
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 0
    ) : array {
        unset($filter);
        unset($sort);
        unset($number);
        unset($size);

        return array(array(), 0);
    }
}
