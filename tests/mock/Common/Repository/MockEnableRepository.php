<?php
namespace Base\Common\Repository;

class MockEnableRepository
{
    public function edit($mockEnable, array $keys = array()) : bool
    {
        unset($mockEnable);
        unset($keys);
        return true;
    }
}
