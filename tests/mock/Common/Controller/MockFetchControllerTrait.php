<?php
namespace Base\Common\Controller;

use Marmot\Interfaces\IView;
use Base\Common\View\MockView;
use Marmot\Basecode\Classes\Request;

class MockFetchControllerTrait
{
    use FetchControllerTrait;

    public function generateView($data) : IView
    {
        return new MockView($data);
    }

    public function getResourceName() : string
    {
        return 'resource';
    }
    
    public function getRequest() : Request
    {
        return new Request();
    }
}
