<?php
namespace Base\Common\Adapter;

use Marmot\Interfaces\INull;
use Marmot\Common\Model\IObject;
use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Interfaces\IRowQuery;

class MockDbAdapter
{
    use DbAdapterTrait;

    protected function getDbTranslator() : ITranslator
    {
    }

    protected function getRowQuery() : IRowQuery
    {
    }

    protected function getNullObject() : INull
    {
    }

    public function insert(IObject $object) : bool
    {
        return $this->insertAction($object);
    }

    public function update(IObject $object, array $keys = array()) : bool
    {
        return $this->updateAction($object, $keys);
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList($id)
    {
        return $this->fetchListAction($id);
    }

    protected function formatFilter() : string
    {
        return '';
    }

    protected function formatSort() : string
    {
        return '';
    }
}
