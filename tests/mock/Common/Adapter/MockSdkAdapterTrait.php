<?php
namespace Base\Common\Adapter;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\IObject;
use Marmot\Framework\Classes\Repository;

use Base\Common\Translator\ISdkTranslator;

class MockSdkAdapterTrait
{
    use SdkAdapterTrait;

    protected function getTranslator() : ISdkTranslator
    {
    }

    protected function getRestfulRepository() : Repository
    {
    }

    protected function mapErrors() : void
    {
    }

    protected function getNullObject() : INull
    {
    }
    
    public function addActionPublic(IObject $object) : bool
    {
        return $this->addAction($object);
    }

    public function editActionPublic(IObject $object, array $keys = array()) : bool
    {
        return $this->editAction($object, $keys);
    }

    public function fetchOneActionPublic($id) : IObject
    {
        return $this->fetchOneAction($id);
    }

    public function fetchListActionPublic(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    public function filterActionPublic(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $offset, $size);
    }
}
