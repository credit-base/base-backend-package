<?php
namespace Base\Common\Adapter;

class MockCommonMapErrorsTraitObject
{
    use CommonMapErrorsTrait;

    protected function getMapErrors() : array
    {
        return [];
    }

    public function mapErrorsPublic() : void
    {
        $this->mapErrors();
    }
}
