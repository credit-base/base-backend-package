<?php
namespace Base\Common\Model;

class MockNullTopAbleTrait
{
    use NullTopAbleTrait;

    protected function resourceNotExist() : bool
    {
        return false;
    }

    public function publicUpdateStick(int $stick) : bool
    {
        return $this->updateStick($stick);
    }
}
