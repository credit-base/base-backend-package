<?php
namespace Base\Common\Model;

class MockNullApproveAbleTrait
{
    use NullApproveAbleTrait;

    protected function resourceNotExist() : bool
    {
        return false;
    }
}
