<?php
namespace Base\Common\Model;

use Base\News\Model\News;

class MockTopAbleTrait extends News implements ITopAble
{
    use TopAbleTrait;

    public function publicUpdateStick(int $stick) : bool
    {
        return $this->updateStick($stick);
    }
}
