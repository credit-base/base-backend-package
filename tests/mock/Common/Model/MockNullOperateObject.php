<?php
namespace Base\Common\Model;

class MockNullOperateObject
{
    use NullOperateTrait;

    public function publicResourceNotExist() : bool
    {
        return $this->resourceNotExist();
    }
}
