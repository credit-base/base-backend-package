<?php
namespace Base\Common\Model;

class MockNullResubmitAbleTrait
{
    use NullResubmitAbleTrait;

    protected function resourceNotExist() : bool
    {
        return false;
    }
}
