<?php
namespace Base\Common\Translator;

class MockSdkTranslator implements ISdkTranslator
{
    public function valueObjectToObject($valueObject = null, $object = null)
    {
        unset($valueObject);
        return $object;
    }

    public function valueObjectToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function objectToValueObject($object)
    {
        unset($object);
        return array();
    }
}
