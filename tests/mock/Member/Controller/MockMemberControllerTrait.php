<?php
namespace Base\Member\Controller;

use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\User\WidgetRule\UserWidgetRule;

use Base\Member\WidgetRule\MemberWidgetRule;
use Base\Member\Repository\MemberRepository;

class MockMemberControllerTrait extends Controller
{
    use MemberControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getMemberWidgetRulePublic() : MemberWidgetRule
    {
        return $this->getMemberWidgetRule();
    }

    public function getUserWidgetRulePublic() : UserWidgetRule
    {
        return $this->getUserWidgetRule();
    }

    public function getRepositoryPublic() : MemberRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    public function validateAddScenarioPublic(
        $userName,
        $realName,
        $cardId,
        $cellphone,
        $email,
        $contactAddress,
        $securityAnswer,
        $password,
        $securityQuestion
    ) : bool {
        return $this->validateAddScenario(
            $userName,
            $realName,
            $cardId,
            $cellphone,
            $email,
            $contactAddress,
            $securityAnswer,
            $password,
            $securityQuestion
        );
    }

    public function validateEditScenarioPublic($gender) : bool
    {
        return $this->validateEditScenario($gender);
    }

    public function validateSignInScenarioPublic(
        $userName,
        $password
    ) : bool {
        return $this->validateSignInScenario(
            $userName,
            $password
        );
    }

    public function validateResetPasswordScenarioPublic(
        $userName,
        $securityAnswer,
        $password
    ) : bool {
        return $this->validateResetPasswordScenario(
            $userName,
            $securityAnswer,
            $password
        );
    }

    public function validateUpdatePasswordScenarioPublic(
        $oldPassword,
        $password
    ) : bool {
        return $this->validateUpdatePasswordScenario(
            $oldPassword,
            $password
        );
    }

    public function validateValidateSecurityScenarioPublic(
        $securityAnswer
    ) : bool {
        return $this->validateValidateSecurityScenario(
            $securityAnswer
        );
    }
}
