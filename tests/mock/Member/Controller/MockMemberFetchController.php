<?php
namespace Base\Member\Controller;

use Base\Member\Adapter\Member\IMemberAdapter;
use Marmot\Interfaces\IView;

class MockMemberFetchController extends MemberFetchController
{
    public function getRepository() : IMemberAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
