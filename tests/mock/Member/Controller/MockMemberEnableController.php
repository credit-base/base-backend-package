<?php
namespace Base\Member\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Member\Adapter\Member\IMemberAdapter;

class MockMemberEnableController extends MemberEnableController
{
    public function getRepository() : IMemberAdapter
    {
        return parent::getRepository();
    }

    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}
