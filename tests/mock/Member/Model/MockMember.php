<?php
namespace Base\Member\Model;

use Base\Member\Adapter\Member\IMemberAdapter;

class MockMember extends Member
{
    public function getRepository() : IMemberAdapter
    {
        return parent::getRepository();
    }

    public function isCellphoneExist() : bool
    {
        return parent::isCellphoneExist();
    }

    public function isEmailExist() : bool
    {
        return parent::isEmailExist();
    }

    public function isUserNameExist() : bool
    {
        return parent::isUserNameExist();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function updatePassword(string $newPassword) : bool
    {
        return parent::updatePassword($newPassword);
    }

    public function verifyPassword(string $oldPassword) : bool
    {
        return parent::verifyPassword($oldPassword);
    }
}
