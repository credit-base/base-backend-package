<?php
namespace Base\Member\CommandHandler\Member;

use Base\Common\Model\IEnableAble;

class MockEnableMemberCommandHandler extends EnableMemberCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
