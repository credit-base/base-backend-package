<?php
namespace Base\Member\CommandHandler\Member;

use Base\Member\Model\Member;
use Base\Member\Command\Member\SignInMemberCommand;

class MockSignInMemberCommandHandler extends SignInMemberCommandHandler
{
    public function searchMembers($userName) : array
    {
        return parent::searchMembers($userName);
    }

    public function isMemberExist(int $count) : bool
    {
        return parent::isMemberExist($count);
    }

    public function isMemberDisabled(Member $member) : bool
    {
        return parent::isMemberDisabled($member);
    }

    public function memberPasswordCorrect(Member $member, SignInMemberCommand $command) : bool
    {
        return parent::memberPasswordCorrect($member, $command);
    }
}
