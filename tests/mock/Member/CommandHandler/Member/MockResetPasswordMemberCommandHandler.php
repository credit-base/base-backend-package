<?php
namespace Base\Member\CommandHandler\Member;

class MockResetPasswordMemberCommandHandler extends ResetPasswordMemberCommandHandler
{
    public function isMemberExist(int $count) : bool
    {
        return parent::isMemberExist($count);
    }

    public function searchMembers($userName) : array
    {
        return parent::searchMembers($userName);
    }
}
