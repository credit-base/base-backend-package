<?php
namespace Base\Member\CommandHandler\Member;

use Base\Common\Model\IEnableAble;

class MockDisableMemberCommandHandler extends DisableMemberCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
