<?php
namespace Base\Member\CommandHandler\Member;

use Base\Member\Model\Member;

class MockAddMemberCommandHandler extends AddMemberCommandHandler
{
    public function getMember() : Member
    {
        return parent::getMember();
    }
}
