<?php
namespace Base\Member\CommandHandler\Member;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;

class MockMemberCommandHandlerTrait
{
    use MemberCommandHandlerTrait;

    public function publicGetMemberRepository() : MemberRepository
    {
        return $this->getMemberRepository();
    }

    public function publicFetchMember(int $id) : Member
    {
        return $this->fetchMember($id);
    }
}
