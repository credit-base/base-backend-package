<?php
namespace Base\Member\Adapter\Member;

use Marmot\Interfaces\INull;

use Base\Member\Translator\MemberDbTranslator;
use Base\Member\Adapter\Member\Query\MemberRowCacheQuery;

class MockMemberDbAdapter extends MemberDbAdapter
{
    public function getDbTranslator() : MemberDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : MemberRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
