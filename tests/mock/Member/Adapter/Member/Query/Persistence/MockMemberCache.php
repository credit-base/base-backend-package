<?php
namespace Base\Member\Adapter\Member\Query\Persistence;

class MockMemberCache extends MemberCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
