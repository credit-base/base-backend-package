<?php
namespace Base\Member\Repository;

use Base\Member\Adapter\Member\IMemberAdapter;

class MockMemberRepository extends MemberRepository
{
    public function getAdapter() : IMemberAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IMemberAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IMemberAdapter
    {
        return parent::getMockAdapter();
    }
}
