<?php
namespace Base\WorkOrderTask\Repository;

use Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;

class MockParentTaskSdkRepository extends ParentTaskSdkRepository
{
    public function getAdapter() : IParentTaskAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IParentTaskAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IParentTaskAdapter
    {
        return parent::getMockAdapter();
    }
}
