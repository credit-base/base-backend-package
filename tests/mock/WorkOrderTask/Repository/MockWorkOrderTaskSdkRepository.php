<?php
namespace Base\WorkOrderTask\Repository;

use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockWorkOrderTaskSdkRepository extends WorkOrderTaskSdkRepository
{
    public function getAdapter() : IWorkOrderTaskAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IWorkOrderTaskAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IWorkOrderTaskAdapter
    {
        return parent::getMockAdapter();
    }
}
