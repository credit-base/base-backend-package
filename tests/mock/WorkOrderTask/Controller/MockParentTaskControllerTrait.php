<?php
namespace Base\WorkOrderTask\Controller;

use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use Base\WorkOrderTask\WidgetRule\ParentTaskWidgetRule;

class MockParentTaskControllerTrait extends Controller
{
    use ParentTaskControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getParentTaskWidgetRulePublic() : ParentTaskWidgetRule
    {
        return $this->getParentTaskWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : IParentTaskAdapter
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
