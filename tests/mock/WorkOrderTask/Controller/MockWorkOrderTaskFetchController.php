<?php
namespace Base\WorkOrderTask\Controller;

use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Marmot\Interfaces\IView;

class MockWorkOrderTaskFetchController extends WorkOrderTaskFetchController
{
    public function getRepository() : IWorkOrderTaskAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
