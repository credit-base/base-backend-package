<?php
namespace Base\WorkOrderTask\Controller;

use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;
use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockWorkOrderTaskControllerTrait extends Controller
{
    use WorkOrderTaskControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getWorkOrderTaskWidgetRulePublic() : WorkOrderTaskWidgetRule
    {
        return $this->getWorkOrderTaskWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : IWorkOrderTaskAdapter
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}
