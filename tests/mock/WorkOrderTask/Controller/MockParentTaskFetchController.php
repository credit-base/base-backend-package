<?php
namespace Base\WorkOrderTask\Controller;

use Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use Marmot\Interfaces\IView;

class MockParentTaskFetchController extends ParentTaskFetchController
{
    public function getRepository() : IParentTaskAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
