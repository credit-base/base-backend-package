<?php
namespace Base\WorkOrderTask\Model;

use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockWorkOrderTask extends WorkOrderTask
{
    public function getRepository() : IWorkOrderTaskAdapter
    {
        return parent::getRepository();
    }

    public function isDqr() : bool
    {
        return parent::isDqr();
    }

    public function isGjz() : bool
    {
        return parent::isGjz();
    }
}
