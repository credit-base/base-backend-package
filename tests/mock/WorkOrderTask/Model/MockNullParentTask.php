<?php
namespace Base\WorkOrderTask\Model;

class MockNullParentTask extends NullParentTask
{
    public function publicResourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
