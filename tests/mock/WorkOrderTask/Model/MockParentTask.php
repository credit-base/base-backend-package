<?php
namespace Base\WorkOrderTask\Model;

use Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;

class MockParentTask extends ParentTask
{
    public function getRepository() : IParentTaskAdapter
    {
        return parent::getRepository();
    }
}
