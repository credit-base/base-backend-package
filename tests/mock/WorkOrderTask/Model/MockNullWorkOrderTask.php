<?php
namespace Base\WorkOrderTask\Model;

class MockNullWorkOrderTask extends NullWorkOrderTask
{
    public function publicResourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
