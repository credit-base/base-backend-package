<?php
namespace Base\WorkOrderTask\Adapter\WorkOrderTask;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

use Base\WorkOrderTask\Model\WorkOrderTask;

use Base\UserGroup\Repository\UserGroupRepository;

class MockWorkOrderTaskSdkAdapter extends WorkOrderTaskSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function fetchAssignObject($workOrderTask)
    {
        return parent::fetchAssignObject($workOrderTask);
    }

    public function fetchAssignObjectByObject(WorkOrderTask $workOrderTask)
    {
        return parent::fetchAssignObjectByObject($workOrderTask);
    }

    public function fetchAssignObjectByList(array $workOrderTaskList)
    {
        return parent::fetchAssignObjectByList($workOrderTaskList);
    }
}
