<?php
namespace Base\WorkOrderTask\Adapter\ParentTask;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;

class MockParentTaskSdkAdapter extends ParentTaskSdkAdapter
{
    public function getTranslator() : ISdkTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }
}
