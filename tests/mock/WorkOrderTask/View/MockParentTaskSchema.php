<?php
namespace Base\WorkOrderTask\View;

use Base\WorkOrderTask\Model\ParentTask;

class MockParentTaskSchema extends ParentTaskSchema
{
    public function calRatio(ParentTask $parentTask) : int
    {
        return parent::calRatio($parentTask);
    }
}
