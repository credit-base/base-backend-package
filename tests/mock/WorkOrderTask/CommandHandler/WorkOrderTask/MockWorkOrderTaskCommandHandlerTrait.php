<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Base\WorkOrderTask\Model\WorkOrderTask;

class MockWorkOrderTaskCommandHandlerTrait
{
    use WorkOrderTaskCommandHandlerTrait;

    public function getWorkOrderTaskSdkRepositoryPublic() : IWorkOrderTaskAdapter
    {
        return $this->getWorkOrderTaskSdkRepository();
    }

    public function fetchWorkOrderTaskPublic($id) : WorkOrderTask
    {
        return $this->fetchWorkOrderTask($id);
    }
}
