<?php
namespace Base\WorkOrderTask\CommandHandler\ParentTask;

use Base\WorkOrderTask\Model\ParentTask;

class MockAddParentTaskCommandHandler extends AddParentTaskCommandHandler
{
    public function getParentTask() : ParentTask
    {
        return parent::getParentTask();
    }
}
