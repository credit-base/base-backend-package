<?php
namespace Base\WorkOrderTask\CommandHandler\ParentTask;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use Base\Template\Model\GbTemplate;

use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use Base\Template\Model\BjTemplate;

use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

class MockParentTaskCommandHandlerTrait
{
    use ParentTaskCommandHandlerTrait;

    public function getGbTemplateSdkRepositoryPublic() : IGbTemplateAdapter
    {
        return $this->getGbTemplateSdkRepository();
    }

    public function getBjTemplateSdkRepositoryPublic() : IBjTemplateAdapter
    {
        return $this->getBjTemplateSdkRepository();
    }

    public function getUserGroupRepositoryPublic() : IUserGroupAdapter
    {
        return $this->getUserGroupRepository();
    }

    public function fetchOneGbTemplatePublic($id) : GbTemplate
    {
        return $this->fetchOneGbTemplate($id);
    }

    public function fetchOneBjTemplatePublic($id) : BjTemplate
    {
        return $this->fetchOneBjTemplate($id);
    }

    public function fetchListUserGroupPublic(array $ids) : array
    {
        return $this->fetchListUserGroup($ids);
    }
}
