<?php
namespace Base\WorkOrderTask\CommandHandler\ParentTask;

use Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;

class MockRevokeParentTaskCommandHandler extends RevokeParentTaskCommandHandler
{
    public function getParentTaskSdkRepository() : IParentTaskAdapter
    {
        return parent::getParentTaskSdkRepository();
    }
}
