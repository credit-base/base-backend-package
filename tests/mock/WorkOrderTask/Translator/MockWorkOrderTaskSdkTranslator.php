<?php
namespace Base\WorkOrderTask\Translator;

use Base\Common\Translator\ISdkTranslator;

class MockWorkOrderTaskSdkTranslator extends WorkOrderTaskSdkTranslator
{
    public function getUserGroupSdkTranslator() : ISdkTranslator
    {
        return parent::getUserGroupSdkTranslator();
    }

    public function getTemplateTranslatorFactory() : TemplateTranslatorFactory
    {
        return parent::getTemplateTranslatorFactory();
    }

    public function getTemplateSdkTranslator(int $type) : ISdkTranslator
    {
        return parent::getTemplateSdkTranslator($type);
    }

    public function getParentTaskSdkTranslator() : ISdkTranslator
    {
        return parent::getParentTaskSdkTranslator();
    }
}
