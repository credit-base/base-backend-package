<?php
namespace Base\ApplyForm\Model;

class MockApplyTopAbleTrait
{
    use ApplyTopAbleTrait;

    public function setStick(int $stick) : void
    {
        $this->stick = $stick;
    }

    public function getStick() : int
    {
        return $this->stick;
    }

    public function apply() : bool
    {
        return false;
    }
}
