<?php
namespace Base\ApplyForm\Model;

class MockNullApplyTopAbleTrait
{
    use NullApplyTopAbleTrait;

    protected function resourceNotExist() : bool
    {
        return false;
    }
}
