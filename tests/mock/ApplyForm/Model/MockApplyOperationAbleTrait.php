<?php
namespace Base\ApplyForm\Model;

class MockApplyOperationAbleTrait
{
    use ApplyOperationAbleTrait;

    public function apply() : bool
    {
        return false;
    }
}
