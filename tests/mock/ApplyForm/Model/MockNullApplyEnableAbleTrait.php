<?php
namespace Base\ApplyForm\Model;

class MockNullApplyEnableAbleTrait
{
    use NullApplyEnableAbleTrait;

    protected function resourceNotExist() : bool
    {
        return false;
    }
}
