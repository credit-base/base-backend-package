<?php
namespace Base\ApplyForm\Model;

class MockNullApplyOperationAbleTrait
{
    use NullApplyOperationAbleTrait;

    protected function resourceNotExist() : bool
    {
        return false;
    }
}
