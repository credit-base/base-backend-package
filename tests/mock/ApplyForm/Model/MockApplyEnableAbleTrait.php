<?php
namespace Base\ApplyForm\Model;

class MockApplyEnableAbleTrait
{
    use ApplyEnableAbleTrait;

    private $status;

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function apply() : bool
    {
        return false;
    }
}
