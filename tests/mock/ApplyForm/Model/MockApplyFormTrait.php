<?php
namespace Base\ApplyForm\Model;

use Base\ApplyForm\Repository\ApplyFormRepository;

class MockApplyFormTrait implements IApplyFormAble
{
    use ApplyFormTrait;

    public function publicApplyFormRepository() : ApplyFormRepository
    {
        return $this->getApplyFormRepository();
    }

    public function publicApproveApplyInfo(int $type) : bool
    {
        return $this->approveApplyInfo($type);
    }

    public function publicUpdateApplyStatus(int $applyStatus) : bool
    {
        return $this->updateApplyStatus($applyStatus);
    }

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = $applyStatus;
        ;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }
}
