<?php
namespace Base\ApplyForm\Repository;

use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class MockApplyFormRepository extends ApplyFormRepository
{
    public function getAdapter() : IApplyFormAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IApplyFormAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IApplyFormAdapter
    {
        return parent::getMockAdapter();
    }
}
