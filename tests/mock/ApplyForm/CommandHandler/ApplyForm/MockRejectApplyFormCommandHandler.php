<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use Base\Common\Model\IApproveAble;

use Base\Common\Command\RejectCommand;

class MockRejectApplyFormCommandHandler extends RejectApplyFormCommandHandler
{
    public function fetchIApplyObject($id) : IApproveAble
    {
        return parent::fetchIApplyObject($id);
    }

    public function executeAction(RejectCommand $command)
    {
        return parent::executeAction($command);
    }
}
