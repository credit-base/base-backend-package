<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use Base\Common\Model\IApproveAble;

use Base\Common\Command\ApproveCommand;

class MockApproveApplyFormCommandHandler extends ApproveApplyFormCommandHandler
{
    public function fetchIApplyObject($id) : IApproveAble
    {
        return parent::fetchIApplyObject($id);
    }

    public function executeAction(ApproveCommand $command)
    {
        return parent::executeAction($command);
    }
}
