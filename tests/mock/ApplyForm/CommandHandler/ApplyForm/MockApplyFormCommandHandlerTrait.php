<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Repository\ApplyFormRepository;

class MockApplyFormCommandHandlerTrait
{
    use ApplyFormCommandHandlerTrait;

    public function publicGetCrewRepository() : CrewRepository
    {
        return $this->getCrewRepository();
    }

    public function publicFetchCrew(int $id) : Crew
    {
        return $this->fetchCrew($id);
    }

    public function publicGetRepository() : ApplyFormRepository
    {
        return $this->getRepository();
    }

    public function publicFetchApplyForm(int $id) : IApplyFormAble
    {
        return $this->fetchApplyForm($id);
    }
}
