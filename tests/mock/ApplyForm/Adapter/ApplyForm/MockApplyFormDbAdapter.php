<?php
namespace Base\ApplyForm\Adapter\ApplyForm;

use Marmot\Interfaces\INull;
use Marmot\Common\Model\IObject;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Repository\RepositoryFactory;
use Base\ApplyForm\Translator\ApplyFormDbTranslator;
use Base\ApplyForm\Adapter\ApplyForm\Query\ApplyFormRowCacheQuery;

use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class MockApplyFormDbAdapter extends ApplyFormDbAdapter
{
    public function getDbTranslator() : ApplyFormDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : ApplyFormRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getRepositoryFactory() : RepositoryFactory
    {
        return parent::getRepositoryFactory();
    }

    public function getTranslatorFactory()
    {
        return parent::getTranslatorFactory();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function addAction(IObject $object) : bool
    {
        return parent::addAction($object);
    }

    public function editAction(IObject $object, array $keys = array()) : bool
    {
        return parent::editAction($object, $keys);
    }

    public function fetchOneAction($id) : IObject
    {
        return parent::fetchOneAction($id);
    }

    public function fetchListAction(array $ids) : array
    {
        return parent::fetchListAction($ids);
    }
    
    public function fetchApplyUserGroup($applyForm)
    {
        return parent::fetchApplyUserGroup($applyForm);
    }

    public function fetchApplyUserGroupByObject($applyForm)
    {
        return parent::fetchApplyUserGroupByObject($applyForm);
    }

    public function fetchApplyUserGroupByList(array $applyFormList)
    {
        return parent::fetchApplyUserGroupByList($applyFormList);
    }

    public function fetchApplyCrew($applyForm)
    {
        return parent::fetchApplyCrew($applyForm);
    }

    public function fetchApplyCrewByObject($applyForm)
    {
        return parent::fetchApplyCrewByObject($applyForm);
    }

    public function fetchApplyCrewByList(array $applyFormList)
    {
        return parent::fetchApplyCrewByList($applyFormList);
    }

    public function fetchRelation($applyForm)
    {
        return parent::fetchRelation($applyForm);
    }

    public function fetchRelationByObject($applyForm)
    {
        return parent::fetchRelationByObject($applyForm);
    }

    public function fetchRelationByList(array $applyFormList)
    {
        return parent::fetchRelationByList($applyFormList);
    }

    public function applyObject() : IApplyFormAble
    {
        return parent::applyObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
