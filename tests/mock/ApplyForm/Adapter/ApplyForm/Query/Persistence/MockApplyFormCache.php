<?php
namespace Base\ApplyForm\Adapter\ApplyForm\Query\Persistence;

class MockApplyFormCache extends ApplyFormCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
