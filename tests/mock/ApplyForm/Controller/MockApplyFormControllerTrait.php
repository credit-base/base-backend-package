<?php
namespace Base\ApplyForm\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Repository\ApplyFormRepository;

class MockApplyFormControllerTrait extends Controller
{
    use ApplyFormControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getApplyFormRepositoryPublic() : ApplyFormRepository
    {
        return $this->getApplyFormRepository();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        unset($applyForm);
    }

    public function validateCommonScenarioPublic($applyCrewId)
    {
        return $this->validateCommonScenario($applyCrewId);
    }

    public function validateRejectScenarioPublic($rejectReason)
    {
        return $this->validateRejectScenario($rejectReason);
    }
}
