<?php
namespace Base\WebsiteCustomize\Repository;

use Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter;

class MockWebsiteCustomizeRepository extends WebsiteCustomizeRepository
{
    public function getAdapter() : IWebsiteCustomizeAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IWebsiteCustomizeAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IWebsiteCustomizeAdapter
    {
        return parent::getMockAdapter();
    }
}
