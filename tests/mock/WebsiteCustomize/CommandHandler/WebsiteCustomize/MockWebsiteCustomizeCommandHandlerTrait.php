<?php
namespace Base\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

class MockWebsiteCustomizeCommandHandlerTrait
{
    use WebsiteCustomizeCommandHandlerTrait;

    public function publicGetCrewRepository() : CrewRepository
    {
        return $this->getCrewRepository();
    }

    public function publicFetchCrew(int $id) : Crew
    {
        return $this->fetchCrew($id);
    }

    public function publicGetWebsiteCustomizeRepository() : WebsiteCustomizeRepository
    {
        return $this->getWebsiteCustomizeRepository();
    }

    public function publicFetchWebsiteCustomize(int $id) : WebsiteCustomize
    {
        return $this->fetchWebsiteCustomize($id);
    }

    public function publicGetWebsiteCustomize() : WebsiteCustomize
    {
        return $this->getWebsiteCustomize();
    }
}
