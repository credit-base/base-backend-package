<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize;

use Marmot\Interfaces\INull;

use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeRowCacheQuery;

use Base\Crew\Repository\CrewRepository;

class MockWebsiteCustomizeDbAdapter extends WebsiteCustomizeDbAdapter
{
    public function getDbTranslator() : WebsiteCustomizeDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : WebsiteCustomizeRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function fetchCrew($websiteCustomize)
    {
        return parent::fetchCrew($websiteCustomize);
    }

    public function fetchCrewByObject($websiteCustomize)
    {
        return parent::fetchCrewByObject($websiteCustomize);
    }

    public function fetchCrewByList(array $websiteCustomizeList)
    {
        return parent::fetchCrewByList($websiteCustomizeList);
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}
