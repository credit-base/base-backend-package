<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;

class MockWebsiteCustomizeHomePageFragmentCacheQuery extends WebsiteCustomizeHomePageFragmentCacheQuery
{
    public function getRepository() : WebsiteCustomizeRepository
    {
        return parent::getRepository();
    }

    public function getTranslator() : WebsiteCustomizeDbTranslator
    {
        return parent::getTranslator();
    }

    public function fetchCacheData()
    {
        return parent::fetchCacheData();
    }
}
