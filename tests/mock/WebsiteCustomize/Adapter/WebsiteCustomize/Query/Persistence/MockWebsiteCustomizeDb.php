<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\Persistence;

class MockWebsiteCustomizeDb extends WebsiteCustomizeDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}
