<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\Persistence;

class MockWebsiteCustomizeCache extends WebsiteCustomizeCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}
