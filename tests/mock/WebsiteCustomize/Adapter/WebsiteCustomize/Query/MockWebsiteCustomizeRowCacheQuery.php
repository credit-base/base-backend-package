<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use Marmot\Framework\Interfaces\DbLayer;
use Marmot\Interfaces\CacheLayer;

class MockWebsiteCustomizeRowCacheQuery extends WebsiteCustomizeRowCacheQuery
{
    public function getCacheLayer() : CacheLayer
    {
        return parent::getCacheLayer();
    }

    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}
