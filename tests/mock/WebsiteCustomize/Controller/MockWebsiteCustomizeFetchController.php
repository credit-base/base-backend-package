<?php
namespace Base\WebsiteCustomize\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Query\FragmentCacheQuery;

use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeFragmentCacheQueryFactory;

class MockWebsiteCustomizeFetchController extends WebsiteCustomizeFetchController
{
    public function getRepository() : IWebsiteCustomizeAdapter
    {
        return parent::getRepository();
    }

    public function getTranslator() : WebsiteCustomizeDbTranslator
    {
        return parent::getTranslator();
    }

    public function getFragmentCacheQueryFactory() : WebsiteCustomizeFragmentCacheQueryFactory
    {
        return parent::getFragmentCacheQueryFactory();
    }

    public function getFragmentCacheQuery(int $category) : FragmentCacheQuery
    {
        return parent::getFragmentCacheQuery($category);
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}
