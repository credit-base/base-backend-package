<?php
namespace Base\WebsiteCustomize\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\WebsiteCustomize\WidgetRule\WebsiteCustomizeWidgetRule;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

class MockWebsiteCustomizeControllerTrait extends Controller
{
    use WebsiteCustomizeControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getWebsiteCustomizeWidgetRulePublic() : WebsiteCustomizeWidgetRule
    {
        return $this->getWebsiteCustomizeWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : WebsiteCustomizeRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    public function validateAddScenarioPublic(
        $category,
        $content,
        $status,
        $crew
    ) {
        return $this->validateAddScenario(
            $category,
            $content,
            $status,
            $crew
        );
    }
}
