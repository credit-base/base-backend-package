<?php
namespace Base\WebsiteCustomize\WidgetRule;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class MockWebsiteCustomizeWidgetRule extends WebsiteCustomizeWidgetRule
{
    public function validateContentImage($type, $image, $pointer = 'image') : bool
    {
        return parent::validateContentImage($type, $image, $pointer);
    }

    public function validateContentStatus($status, $pointer = 'status') : bool
    {
        return parent::validateContentStatus($status, $pointer);
    }

    public function validateContentType($type, $pointer = 'type') : bool
    {
        return parent::validateContentType($type, $pointer);
    }

    public function validateContentName($type, $name, $pointer = 'name') : bool
    {
        return parent::validateContentName($type, $name, $pointer);
    }

    public function validateContentUrl($type, $url, $pointer = 'url') : bool
    {
        return parent::validateContentUrl($type, $url, $pointer);
    }

    public function validateHomePageContent(array $content) : bool
    {
        return parent::validateHomePageContent($content);
    }

    public function homePageContentKeysExist(array $content) : bool
    {
        return parent::homePageContentKeysExist($content);
    }

    public function homePageContentKeysRequired(array $content) : bool
    {
        return parent::homePageContentKeysRequired($content);
    }

    public function homePageFormat(array $content) : bool
    {
        return parent::homePageFormat($content);
    }

    public function validateHomePageContentFragment(string $key, $content) : bool
    {
        return parent::validateHomePageContentFragment($key, $content);
    }

    public function validateMemorialStatus($content) : bool
    {
        return parent::validateMemorialStatus($content);
    }

    public function validateTheme($content) : bool
    {
        return parent::validateTheme($content);
    }

    public function validateHeaderBarLeft($content) : bool
    {
        return parent::validateHeaderBarLeft($content);
    }

    public function validateHeaderBarRight($content) : bool
    {
        return parent::validateHeaderBarRight($content);
    }

    public function validateHeaderBg($content) : bool
    {
        return parent::validateHeaderBg($content);
    }

    public function validateLogo($content) : bool
    {
        return parent::validateLogo($content);
    }

    public function validateHeaderSearch($content) : bool
    {
        return parent::validateHeaderSearch($content);
    }

    public function validateNav($content) : bool
    {
        return parent::validateNav($content);
    }

    public function validateCenterDialog($content) : bool
    {
        return parent::validateCenterDialog($content);
    }

    public function validateAnimateWindow($content) : bool
    {
        return parent::validateAnimateWindow($content);
    }

    public function validateLeftFloatCard($content) : bool
    {
        return parent::validateLeftFloatCard($content);
    }

    public function validateFrameWindow($content) : bool
    {
        return parent::validateFrameWindow($content);
    }

    public function validateRightToolBar($content) : bool
    {
        return parent::validateRightToolBar($content);
    }

    public function validateSpecialColumn($content) : bool
    {
        return parent::validateSpecialColumn($content);
    }

    public function validateRelatedLinks($content) : bool
    {
        return parent::validateRelatedLinks($content);
    }

    public function validateFooterBanner($content) : bool
    {
        return parent::validateFooterBanner($content);
    }

    public function validateOrganizationGroup($content) : bool
    {
        return parent::validateOrganizationGroup($content);
    }

    public function validateSilhouette($content) : bool
    {
        return parent::validateSilhouette($content);
    }

    public function validatePartyAndGovernmentOrgans($content) : bool
    {
        return parent::validatePartyAndGovernmentOrgans($content);
    }

    public function validateGovernmentErrorCorrection($content) : bool
    {
        return parent::validateGovernmentErrorCorrection($content);
    }

    public function validateFooterNav($content) : bool
    {
        return parent::validateFooterNav($content);
    }

    public function validateFooterTwo($content) : bool
    {
        return parent::validateFooterTwo($content);
    }

    public function validateFooterThree($content) : bool
    {
        return parent::validateFooterThree($content);
    }
}
