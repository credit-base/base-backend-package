<?php
namespace Base\WebsiteCustomize\Model;

use Marmot\Framework\Query\FragmentCacheQuery;

use Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeFragmentCacheQueryFactory;

class MockWebsiteCustomize extends WebsiteCustomize
{
    public function getRepository() : IWebsiteCustomizeAdapter
    {
        return parent::getRepository();
    }

    public function getFragmentCacheQueryFactory() : WebsiteCustomizeFragmentCacheQueryFactory
    {
        return parent::getFragmentCacheQueryFactory();
    }

    public function getFragmentCacheQuery() : FragmentCacheQuery
    {
        return parent::getFragmentCacheQuery();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function addActionPublishedStatus() : bool
    {
        return parent::addActionPublishedStatus();
    }

    public function addActionUnPublishedStatus() : bool
    {
        return parent::addActionUnPublishedStatus();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function updatePublishWebsiteCustomize() : bool
    {
        return parent::updatePublishWebsiteCustomize();
    }

    public function fetchPublishWebsiteCustomize() : WebsiteCustomize
    {
        return parent::fetchPublishWebsiteCustomize();
    }

    public function updateStatus(int $status) : bool
    {
        return parent::updateStatus($status);
    }
}
