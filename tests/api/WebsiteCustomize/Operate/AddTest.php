<?php
namespace Base\WebsiteCustomize\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\WebsiteCustomize\SetDataApiTrait;
use Base\WebsiteCustomize\WebsiteCustomizeUtils;
use Base\WebsiteCustomize\Model\WebsiteCustomize;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

/**
 * 测试网站定制接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WebsiteCustomizeUtils, CrewSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_website_customize');
    }

    /**
     * 不存在网站定制数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew()
            ]
        );
    }

    public function testViewWebsiteCustomize()
    {
        $setWebsiteCustomize = $this->websiteCustomize()[0];
        $content = $this->content();

        $data = array(
            'data' => array(
                "type"=>"websiteCustomizes",
                "attributes"=>array(
                    "category"=>1,
                    "status"=> 0,
                    "content"=>$content
                ),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'websiteCustomizes',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $version = $contents['data']['attributes']['version'];
        $setWebsiteCustomize['version'] = $version;

        $this->websiteCustomizeCompareData($setWebsiteCustomize, $contents['data']);
    }
}
