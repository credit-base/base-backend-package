<?php
namespace Base\WebsiteCustomize\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\WebsiteCustomize\SetDataApiTrait;
use Base\WebsiteCustomize\Model\WebsiteCustomize;

/**
 * 测试网站定制接口发布
 */
class PublishTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_website_customize');
    }

    /**
     * 存在一条网站定制数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_website_customize' => $this->websiteCustomize()
            ]
        );
    }

    public function testViewWebsiteCustomize()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'websiteCustomizes/1/publish',
            ['headers' => Core::$container->get('client.headers')]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(WebsiteCustomize::STATUS['PUBLISHED'], $attributes['status']);
    }
}
