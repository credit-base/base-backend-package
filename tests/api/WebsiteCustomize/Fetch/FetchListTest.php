<?php
namespace Base\WebsiteCustomize\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\WebsiteCustomize\SetDataApiTrait;
use Base\WebsiteCustomize\WebsiteCustomizeUtils;
use Base\WebsiteCustomize\Model\WebsiteCustomize;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\Department\DepartmentUtils;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试网站定制接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WebsiteCustomizeUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    CrewSetDataApiTrait, CrewUtils, DepartmentUtils, DepartmentSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
        $this->clear('pcore_website_customize');
    }

    /**
     * 存在多条网站定制数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_department' => $this->department(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_website_customize' => $this->websiteCustomize()
            ]
        );
    }

    public function testViewWebsiteCustomize()
    {
        $setWebsiteCustomizeList = $this->websiteCustomize();
        $setCrewList = $this->crew();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'websiteCustomizes/1,2?include=crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $websiteCustomize) {
            $this->websiteCustomizeCompareData($setWebsiteCustomizeList[$key], $websiteCustomize);
        }

        $included = $contents['included'];

        foreach ($included as $key => $crew) {
            $this->crewCompareData($setCrewList[$key], $crew);
        }
    }
}
