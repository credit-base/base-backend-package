<?php
namespace Base\WebsiteCustomize\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\WebsiteCustomize\SetDataApiTrait;
use Base\WebsiteCustomize\WebsiteCustomizeUtils;
use Base\WebsiteCustomize\Model\WebsiteCustomize;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

/**
 * 测试网站定制接口获取首页定制数据
 */
class HomePageTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WebsiteCustomizeUtils, CrewSetDataApiTrait, CrewUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_website_customize');
    }

    /**
     * 存在一条网站定制数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_website_customize' => $this->websiteCustomize()
            ]
        );
    }

    public function testViewWebsiteCustomize()
    {
        $setWebsiteCustomize = $this->websiteCustomize()[1];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'websiteCustomizes/homePage',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->websiteCustomizeCompareData($setWebsiteCustomize, $contents['data']);
    }
}
