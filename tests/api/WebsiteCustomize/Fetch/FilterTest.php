<?php
namespace Base\WebsiteCustomize\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\WebsiteCustomize\SetDataApiTrait;
use Base\WebsiteCustomize\WebsiteCustomizeUtils;
use Base\WebsiteCustomize\Model\WebsiteCustomize;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Department\DepartmentUtils;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试网站定制接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WebsiteCustomizeUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    CrewSetDataApiTrait, CrewUtils, DepartmentUtils, DepartmentSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_website_customize');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
        $this->clear('pcore_crew');
    }

    /**
     * 存在多条网站定制数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_department' => $this->department(),
                'pcore_website_customize' => $this->websiteCustomize()
            ]
        );
    }

    public function testViewWebsiteCustomize()
    {
        $setWebsiteCustomizeList = $this->websiteCustomize();
        $setCrewList = $this->crew();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'websiteCustomizes?filter[category]='.WebsiteCustomize::CATEGORY['HOME_PAGE'].
            '&filter[crew]=1&filter[status]='.WebsiteCustomize::STATUS['UNPUBLISHED'].
            '&filter[version]=2109201155&include=crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $websiteCustomize) {
            $this->websiteCustomizeCompareData($setWebsiteCustomizeList[$key], $websiteCustomize);
        }

        foreach ($included as $key => $crew) {
            $this->crewCompareData($setCrewList[$key], $crew);
        }
    }
}
