<?php
namespace Base\WebsiteCustomize\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\WebsiteCustomize\SetDataApiTrait;
use Base\WebsiteCustomize\WebsiteCustomizeUtils;
use Base\WebsiteCustomize\Model\WebsiteCustomize;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\Department\DepartmentUtils;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

/**
 * 测试网站定制接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WebsiteCustomizeUtils, CrewSetDataApiTrait, CrewUtils,
    DepartmentUtils, DepartmentSetDataApiTrait, UserGroupUtils, UserGroupSetDataApiTrait;

    private $websiteCustomize;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->websiteCustomize);
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
        $this->clear('pcore_website_customize');
    }

    /**
     * 存在一条网站定制数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department(),
                'pcore_website_customize' => $this->websiteCustomize()
            ]
        );
    }

    public function testViewWebsiteCustomize()
    {
        $setWebsiteCustomize = $this->websiteCustomize()[0];
        $setCrew = $this->crew()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'websiteCustomizes/1?include=crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->websiteCustomizeCompareData($setWebsiteCustomize, $contents['data']);
        $this->crewCompareData($setCrew, $contents['included'][0]);
    }
}
