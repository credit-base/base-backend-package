<?php
namespace Base\WebsiteCustomize;

trait WebsiteCustomizeUtils
{
    private function websiteCustomizeCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['website_customize_id'], $responseDataArray['id']);
        $this->assertEquals('websiteCustomizes', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['category'], $attributes['category']);
        $this->assertEquals(json_decode($setDataArray['content'], true), $attributes['content']);
        $this->assertEquals($setDataArray['version'], $attributes['version']);
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsCrew = $responseDataArray['relationships']['crew']['data'];

        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals($setDataArray['crew_id'], $relationshipsCrew['id']);
    }
}
