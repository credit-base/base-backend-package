<?php
namespace Base\WebsiteCustomize;

use Marmot\Core;

use Base\WebsiteCustomize\Model\WebsiteCustomize;

trait SetDataApiTrait
{
    protected function websiteCustomize() : array
    {

        return
        [
            [
                'website_customize_id' => 1,
                'crew_id' => 1,
                'category' => WebsiteCustomize::CATEGORY['HOME_PAGE'],
                'content' => json_encode($this->content()),
                'version' => '2109201155',
                'status' => WebsiteCustomize::STATUS['UNPUBLISHED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'website_customize_id' => 2,
                'crew_id' => 2,
                'category' => WebsiteCustomize::CATEGORY['HOME_PAGE'],
                'content' => json_encode($this->content()),
                'version' => '2109201200',
                'status' => WebsiteCustomize::STATUS['PUBLISHED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    /**
     * @todo
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function content() : array
    {
        return array(
            'memorialStatus' => 1,
            'theme' =>  array(
                'color' => '#41245',
                'image' => array(
                    'name' => '图片名称',
                    'identify' => '图片地址.png'
                )
            ),
            'headerBarLeft' => array(
                array(
                    'name' => '访问量统计',
                    'status' => 0,
                    'url' => '',
                    'type' => 1
                ),
            ),
            'headerBarRight' => array(
                array(
                    'name' => '字体',
                    'status' => 0,
                    'url' => '',
                    'type' => 2
                ),
                array(
                    'name' => '无障碍阅读',
                    'status' => 0,
                    'url' => '',
                    'type' => 3
                ),
                array(
                    'name' => '登录',
                    'status' => 0,
                    'url' => '/signIn',
                    'type' => 4
                ),
                array(
                    'name' => '注册',
                    'status' => 0,
                    'url' => '/signUp',
                    'type' => 5
                ),
                array(
                    'name' => '用户中心',
                    'status' => 0,
                    'url' => '/members',
                    'type' => 6
                ),
                array(
                    'name' => '网站导航',
                    'status' => 0,
                    'url' => 'navigations/index',
                    'type' => 0
                ),
            ),
            'headerBg' => array(
                'name' => 'headerBg名称',
                'identify' => 'headerBg地址.png'
            ),
            'logo' => array(
                'name' => 'logo名称',
                'identify' => 'logo地址.png'
            ),
            'headerSearch' => array(
                'creditInformation' => array(
                    'status' => 0
                ),
                'personalCreditInformation' => array(
                    'status' => 0
                ),
                'unifiedSocialCreditCode' => array(
                    'status' => 0
                ),
                'legalPerson' => array(
                    'status' => 0
                ),
                'newsTitle' => array(
                    'status' => 0
                ),
            ),
            'nav' => array(
                array(
                    'name' => '首页',
                    'url' => '/',
                    'image' => array(
                        'name' => '首页',
                        'identify' => '首页.png'
                    ),
                    'status' => 0,
                    'nav' => 1
                ),
                array(
                    'name' => '组织架构',
                    'url' => '/organizations/index',
                    'image' => array(
                        'name' => '组织架构',
                        'identify' => '组织架构.png'
                    ),
                    'status' => 0,
                    'nav' => 2
                ),
            ),
            'centerDialog' => array(
                'status' =>0,
                'image' => array(
                    'name'=> '中间弹框位置',
                    'identify' => '中间弹框位置.jpg'
                ),
                'url'=>''
            ),
            'animateWindow' => array(
                array(
                    'status' =>0,
                    'image' => array(
                        'name'=> '飘窗位置标识',
                        'identify' => '飘窗位置标识.jpg'
                    ),
                    'url'=>''
                ),
            ),
            'leftFloatCard' => array(
                array(
                    'status' =>0,
                    'image' => array(
                        'name'=> '左侧浮动卡片位置标识',
                        'identify' => '左侧浮动卡片位置标识.png'
                    ),
                    'url'=>''
                ),
            ),
            'frameWindow' => array(
                'status' => 0,
                'url' => '/'
            ),
            'rightToolBar' => array(
                array(
                    'name' => 'App',
                    'category' => 1,
                    'status' => 0,
                    'images' => array(
                        array(
                            'title' => '安卓',
                            'image' => array(
                                'name' => '安卓二维码',
                                'identify' => '安卓二维码.jpg'
                            )
                        ),
                        array(
                            'title' => 'IOS',
                            'image' => array(
                                'name' => 'IOS二维码',
                                'identify' => 'IOS二维码.jpg'
                            )
                        ),
                    ),
                ),
                array(
                    'name' => '申诉',
                    'category' => 2,
                    'status' => 0,
                    'url'=> '/appeals'
                ),
                array(
                    'name' => '联系',
                    'category' => 3,
                    'status' => 0,
                    'description'=> '029-81563990'
                ),
            ),
            'specialColumn' => array(
                array(
                    'status' => 0,
                    'image' => array(
                        'name'=> '专题专栏',
                        'identify' => '专题专栏.jpg'
                    ),
                    'url'=>''
                ),
            ),
            'relatedLinks' => array(
                array(
                    'name' => '信用电子政务网站',
                    'status' => 0,
                    'items' => array(
                        array(
                            'name' => '国家发展和改革委',
                            'url' => 'https://www.ndrc.gov.cn/',
                            'status' => 0
                        )
                    )
                ),
                array(
                    'name' => '省区市信用网站',
                    'status' => 0,
                    'items' => array(
                        array(
                            'name' => '北京市',
                            'url' => 'http://jxj.beijing.gov.cn/creditbj/',
                            'status' => 0
                        )
                    )
                ),
            ),
            'footerBanner' => array(
                array(
                    'status' =>0,
                    'image' => array(
                        'name'=> '底部轮播',
                        'identify' => '底部轮播.jpg'
                    ),
                    'url'=>''
                ),
            ),
            'organizationGroup' => array(
                array(
                    'status' =>0,
                    'image' => array(
                        'name'=> '组织列表',
                        'identify' => '组织列表.jpg'
                    ),
                    'url'=>''
                ),
            ),
            'silhouette' => array(
                'image' => array(
                    'name' => '剪影',
                    'identify' => '剪影.png'
                ),
                'description' => '剪影简介',
                'status'=>0
            ),
            'partyAndGovernmentOrgans' => array(
                'status' => 0,
                'image' => array(
                    'name' => '党政机关',
                    'identify' => '党政机关.png'
                ),
                'url' => 'http://bszs.conac.cn/sitename'
            ),
            'governmentErrorCorrection' => array(
                'status' => 0,
                'image' => array(
                    'name' => '政府纠错',
                    'identify' => '政府纠错.png'
                ),
                'url' => 'http://www.gov.cn/c125533/jbxxk.htm'
            ),
            'footerNav' => array(
                array(
                    'name' => '帮助中心',
                    'url' => '/about',
                    'status' => 0
                ),
                array(
                    'name' => '技术支持',
                    'url' => '',
                    'status' => 0
                ),
                array(
                    'name' => '关于我们',
                    'url' => '/about',
                    'status' => 0
                )
            ),
            'footerTwo' => array(
                array(
                    'name' => '主办单位',
                    'description' => '发展和改革委员会',
                    'url' => '',
                    'status' => 0
                ),
                array(
                    'name' => '技术支持',
                    'description' => '北京企信云信息科技有限公司',
                    'url' => 'https://www.qixinyun.com/',
                    'status' => 0
                ),
            ),
            'footerThree' => array(
                array(
                    'name' => '备案号',
                    'description' => 'ICP备案170000-1',
                    'url' => 'https://beian.miit.gov.cn/#/Integrated/index',
                    'type' => 7,
                    'status' => 0
                ),
                array(
                    'image' => array(
                        'name' => '公网安备',
                        'identify' => '公网安备.png'
                    ),
                    'name' => '公网安备',
                    'description' => '公网安备',
                    'url' => '',
                    'type' => 8,
                    'status' => 0
                ),
                array(
                    'name' => '网站标识码',
                    'description' => '78907655671',
                    'url' => '',
                    'type' => 9,
                    'status' => 0
                ),
            )
        );
    }
}
