<?php
namespace Base\Interaction\UnAuditedComplaint\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Common\Model\IApproveAble;

use Base\Interaction\Model\UnAuditedComplaint;

use Base\Interaction\InteractionTrait;

/**
 * 测试信用投诉审核接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条信用投诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_apply_form' => $this->applyFormComplaint(),
                'pcore_reply' => $this->reply(),
                'pcore_member' => $this->member(),
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup()
            ]
        );
    }

    public function testViewUnAuditedComplaint()
    {
        $setUnAuditedComplaintList = $this->applyFormComplaint();
        foreach ($setUnAuditedComplaintList as $setUnAuditedComplaint) {
            $setUnAuditedComplaintList[$setUnAuditedComplaint['apply_form_id']] = $setUnAuditedComplaint;
        }

        $setCrewList = $this->crew();
        $setMemberList = $this->member();
        $setReplyList = $this->reply();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedComplaints?filter[applyUserGroup]=2&filter[title]=投诉&filter[relation]=2
            &filter[applyInfoType]='. UnAuditedComplaint::APPLY_COMPLAINT_TYPE.
            '&filter[applyInfoCategory]='. UnAuditedComplaint::APPLY_COMPLAINT_CATEGORY.'&
            filter[applyStatus]='. IApproveAble::APPLY_STATUS['REJECT'].'&
            filter[operationType]='. IApproveAble::OPERATION_TYPE['ACCEPT'].
            '&include=member,acceptUserGroup,reply,applyUserGroup,applyCrew,relation',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $unAuditedComplaint) {
            $this->unAuditedComplaintCompareData(
                $setUnAuditedComplaintList[$unAuditedComplaint['id']],
                $unAuditedComplaint
            );
        }

        $included = $contents['included'];

        $this->memberCompareData($setMemberList[1], $included[0]);
        $this->crewCompareData($setCrewList[0], $included[1]);
        $this->userGroupCompareData($setUserGroupList[1], $included[2]);
        $this->replyCompareData($setReplyList[1], $included[3]);
    }
}
