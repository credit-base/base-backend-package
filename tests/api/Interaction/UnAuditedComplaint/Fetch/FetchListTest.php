<?php
namespace Base\Interaction\UnAuditedComplaint\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\Model\UnAuditedComplaint;

use Base\Interaction\InteractionTrait;

/**
 * 测试信用投诉审核接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
        $this->clear('pcore_reply');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
    }

    /**
     * 存在多条信用投诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_reply' => $this->reply(),
                'pcore_member' => $this->member(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_apply_form' => $this->applyFormComplaint()
            ]
        );
    }

    public function testViewUnAuditedComplaint()
    {
        $setUnAuditedComplaintList = $this->applyFormComplaint();
        $setUserGroupList = $this->fetchUserGroupList();
        $setReplyList = $this->fetchReplyList();
        $setMemberList = $this->fetchMemberList();
        $setCrewList = $this->fetchCrewList();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedComplaints/1,2?include=member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $unAuditedComplaint) {
            $this->unAuditedComplaintCompareData($setUnAuditedComplaintList[$key], $unAuditedComplaint);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'userGroups') {
                $this->userGroupCompareData($setUserGroupList[$value['id']], $value);
            }
            if ($value['type'] == 'members') {
                $this->memberCompareData($setMemberList[$value['id']], $value);
            }
            if ($value['id'] != 0 && $value['type'] == 'crews') {
                $this->crewCompareData($setCrewList[$value['id']], $value);
            }
            if ($value['id'] != 0 && $value['type'] == 'replies') {
                $this->replyCompareData($setReplyList[$value['id']], $value);
            }
        }
    }
}
