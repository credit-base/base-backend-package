<?php
namespace Base\Interaction;

use Base\Interaction\SetDataApiTrait;
use Base\Interaction\InteractionUtils;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\Member\MemberUtils;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;

trait InteractionTrait
{
    use UserGroupSetDataApiTrait, UserGroupUtils, CrewSetDataApiTrait, CrewUtils, MemberSetDataApiTrait, MemberUtils,
    SetDataApiTrait, InteractionUtils;
    
    private function fetchUserGroupList() : array
    {
        $setUserGroupList = $this->userGroup();
        foreach ($setUserGroupList as $setUserGroup) {
            $setUserGroupList[$setUserGroup['user_group_id']] = $setUserGroup;
        }

        return $setUserGroupList;
    }

    private function fetchReplyList() : array
    {
        $setReplyList = $this->reply();
        foreach ($setReplyList as $setReply) {
            $setReplyList[$setReply['reply_id']] = $setReply;
        }

        return $setReplyList;
    }

    private function fetchMemberList() : array
    {
        $setMemberList = $this->member();
        foreach ($setMemberList as $setMember) {
            $setMemberList[$setMember['member_id']] = $setMember;
        }

        return $setMemberList;
    }

    private function fetchCrewList() : array
    {
        $setCrewList = $this->crew();
        foreach ($setCrewList as $setCrew) {
            $setCrewList[$setCrew['crew_id']] = $setCrew;
        }

        return $setCrewList;
    }
}
