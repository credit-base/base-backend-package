<?php
namespace Base\Interaction\UnAuditedAppeal\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Common\Model\IApproveAble;

use Base\Interaction\SetDataApiTrait;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

/**
 * 测试异议申诉接口审核通过
 */
class ApproveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CrewSetDataApiTrait, UserGroupSetDataApiTrait, MemberSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
        $this->clear('pcore_appeal');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在一条异议申诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_appeal' => $this->appeal(),
                'pcore_crew' => $this->crew(),
                'pcore_apply_form' => $this->applyFormAppeal(),
                'pcore_reply' => $this->reply(),
                'pcore_member' => $this->member(),
                'pcore_user_group' => $this->userGroup()
            ]
        );
    }

    public function testViewUnAuditedAppeal()
    {
        $data = array(
            'data' => array(
                "type"=>"unAuditedAppeal",
                "relationships"=>array(
                    "applyCrew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'unAuditedAppeals/1/approve',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IApproveAble::APPLY_STATUS['APPROVE'], $attributes['applyStatus']);

        $relationships = $contents['data']['relationships'];
        $relationshipsCrew = $relationships['applyCrew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals(2, $relationshipsCrew['id']);
    }
}
