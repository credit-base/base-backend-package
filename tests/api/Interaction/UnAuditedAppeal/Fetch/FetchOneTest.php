<?php
namespace Base\Interaction\UnAuditedAppeal\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\Model\UnAuditedAppeal;

use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

use Base\Interaction\InteractionTrait;

/**
 * 测试异议申诉审核接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, DepartmentSetDataApiTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
        $this->clear('pcore_reply');
        $this->clear('pcore_department');
    }

    /**
     * 存在一条异议申诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_reply' => $this->reply(),
                'pcore_member' => $this->member(),
                'pcore_apply_form' => $this->applyFormAppeal(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewUnAuditedAppeal()
    {
        $setUnAuditedAppeal = $this->applyFormAppeal()[1];
        $setUserGroup = $this->userGroup()[1];
        $setCrew = $this->crew()[0];
        $setMember = $this->member()[1];
        $setReply = $this->reply()[1];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedAppeals/2?include=member,acceptUserGroup,reply,applyUserGroup,applyCrew,relation',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->unAuditedAppealCompareData($setUnAuditedAppeal, $contents['data']);
        $this->memberCompareData($setMember, $contents['included'][0]);
        $this->crewCompareData($setCrew, $contents['included'][1]);
        $this->userGroupCompareData($setUserGroup, $contents['included'][2]);
        $this->replyCompareData($setReply, $contents['included'][3]);
    }
}
