<?php
namespace Base\Interaction\UnAuditedAppeal\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\Model\UnAuditedAppeal;

use Base\Common\Model\IApproveAble;

use Base\Interaction\InteractionTrait;

/**
 * 测试异议申诉审核接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
    }

    /**
     * 存在多条异议申诉审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_reply' => $this->reply(),
                'pcore_member' => $this->member(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_apply_form' => $this->applyFormAppeal()
            ]
        );
    }

    public function testViewUnAuditedAppeal()
    {
        $setUnAuditedAppealList = $this->applyFormAppeal();
        foreach ($setUnAuditedAppealList as $setUnAuditedAppeal) {
            $setUnAuditedAppealList[$setUnAuditedAppeal['apply_form_id']] = $setUnAuditedAppeal;
        }

        $setCrewList = $this->crew();
        $setUserGroupList = $this->userGroup();
        $setMemberList = $this->member();
        $setReplyList = $this->reply();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedAppeals?filter[applyUserGroup]=2&filter[title]=申诉&filter[relation]=2
            &filter[applyInfoType]='. UnAuditedAppeal::APPLY_APPEAL_TYPE.
            '&filter[applyInfoCategory]='. UnAuditedAppeal::APPLY_APPEAL_CATEGORY.'&
            filter[applyStatus]='. IApproveAble::APPLY_STATUS['REJECT'].'&
            filter[operationType]='. IApproveAble::OPERATION_TYPE['ACCEPT'].
            '&include=member,acceptUserGroup,reply,applyUserGroup,applyCrew,relation',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $unAuditedAppeal) {
            $this->unAuditedAppealCompareData($setUnAuditedAppealList[$unAuditedAppeal['id']], $unAuditedAppeal);
        }

        $included = $contents['included'];

        $this->memberCompareData($setMemberList[1], $included[0]);
        $this->crewCompareData($setCrewList[0], $included[1]);
        $this->userGroupCompareData($setUserGroupList[1], $included[2]);
        $this->replyCompareData($setReplyList[1], $included[3]);
    }
}
