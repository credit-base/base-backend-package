<?php
namespace Base\Interaction\Feedback\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\InteractionTrait;

/**
 * 测试问题反馈类接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
        $this->clear('pcore_feedback');
        $this->clear('pcore_reply');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条问题反馈类数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_member' => $this->member(),
                'pcore_reply' => $this->reply(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_feedback' => $this->feedback()
            ]
        );
    }

    public function testViewFeedback()
    {
        $setFeedbackList = [$this->feedback()[1],$this->feedback()[2]];
        $setUserGroupList = $this->fetchUserGroupList();
        $setReplyList = $this->fetchReplyList();
        $setMemberList = $this->fetchMemberList();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'feedbacks/2,3?include=acceptUserGroup,member,reply',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $feedback) {
            $this->feedbackCompareData($setFeedbackList[$key], $feedback);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'userGroups') {
                $this->userGroupCompareData($setUserGroupList[$value['id']], $value);
            }
            if ($value['type'] == 'members') {
                $this->memberCompareData($setMemberList[$value['id']], $value);
            }
            if ($value['type'] == 'replies') {
                $this->replyCompareData($setReplyList[$value['id']], $value);
            }
        }
    }
}
