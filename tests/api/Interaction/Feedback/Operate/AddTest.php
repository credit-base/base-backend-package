<?php
namespace Base\Interaction\Feedback\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\InteractionUtils;
use Base\Interaction\SetDataApiTrait;
use Base\Interaction\Model\IInteractionAble;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;

/**
 * 测试问题反馈接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, InteractionUtils, UserGroupSetDataApiTrait, MemberSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_feedback');
    }

    /**
     * 不存在问题反馈数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_member' => $this->member()
            ]
        );
    }

    public function testViewFeedback()
    {
        $setFeedback = $this->feedback()[0];
        $data = array(
            'data' => array(
                "type"=>"feedbacks",
                "attributes"=>array(
                    "title"=>"个人反馈",
                    "content"=>"个人反馈"
                ),
                "relationships"=>array(
                    "member"=>array(
                        "data"=>array(
                            array("type"=>"members","id"=>1)
                        )
                    ),
                    "acceptUserGroup"=>array(
                        "data"=>array(
                            array("type"=>"userGroups","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'feedbacks',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->feedbackCompareData($setFeedback, $contents['data']);
    }
}
