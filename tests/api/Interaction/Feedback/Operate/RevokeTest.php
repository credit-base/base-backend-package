<?php
namespace Base\Interaction\Feedback\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\SetDataApiTrait;
use Base\Interaction\Model\IInteractionAble;

/**
 * 测试问题反馈接口撤销
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_feedback');
    }

    /**
     * 存在一条问题反馈数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_feedback' => $this->feedback()
            ]
        );
    }

    public function testViewFeedback()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'feedbacks/1/revoke',
            [
                'headers'=>Core::$container->get('client.headers')
            ]
        );
        
        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['REVOKE'], $attributes['status']);
    }
}
