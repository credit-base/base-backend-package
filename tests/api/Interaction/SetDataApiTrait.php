<?php
namespace Base\Interaction;

use Marmot\Core;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\UnAuditedQA;
use Base\Interaction\Model\UnAuditedPraise;
use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Model\UnAuditedFeedback;
use Base\Interaction\Model\UnAuditedComplaint;

use Base\Common\Model\IApproveAble;

trait SetDataApiTrait
{
    protected function appeal() : array
    {
        return
        [
            [
                'appeal_id' => '1',
                'certificates' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg'))),
                'title' => '个人申诉',
                'content' => '个人申诉',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['PENDING'],
                'member_id' => 1,
                'reply_id' => 0,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '张文科',
                'identify' => '412825199009073428',
                'type' => IInteractionAble::TYPE['PERSONAL'],
                'contact' => '029-87654330',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ],
            [
                'appeal_id' => '2',
                'certificates' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg'))),
                'title' => '企业申诉',
                'content' => '企业申诉',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['COMPLETE'],
                'member_id' => 2,
                'reply_id' => 2,
                'accept_usergroup_id' => 2,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '西安传媒有限公司',
                'identify' => '912828A90875215901',
                'type' => IInteractionAble::TYPE['ENTERPRISE'],
                'contact' => '029-87654333',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ],
            [
                'appeal_id' => '3',
                'certificates' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg'))),
                'title' => '个人申诉',
                'content' => '个人申诉',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['ACCEPTING'],
                'member_id' => 1,
                'reply_id' => 1,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '张文科',
                'identify' => '412825199009073427',
                'type' => IInteractionAble::TYPE['PERSONAL'],
                'contact' => '029-87654230',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ]
        ];
    }

    protected function applyFormAppeal() : array
    {
        $applyInfoOne = $this->appeal()[0];
        $applyInfoTwo = $this->appeal()[1];

        return
        [
            [
                'apply_form_id' => 1,
                'title' => '个人申诉',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 0,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoOne))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedAppeal::APPLY_APPEAL_CATEGORY,
                'apply_info_type' => UnAuditedAppeal::APPLY_APPEAL_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'apply_form_id' => 2,
                'title' => '企业申诉',
                'relation_id' => 2,
                'apply_usergroup_id' => 2,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoTwo))),
                'reject_reason' => '数据不合理',
                'apply_info_category' => UnAuditedAppeal::APPLY_APPEAL_CATEGORY,
                'apply_info_type' => UnAuditedAppeal::APPLY_APPEAL_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['REJECT'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function complaint() : array
    {
        return
        [
            [
                'complaint_id' => '1',
                'title' => '个人投诉',
                'content' => '个人投诉',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['PENDING'],
                'member_id' => 1,
                'reply_id' => 0,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'subject' => '张文科',
                'name' => '张文科',
                'identify' => '412825199109073428',
                'type' => IInteractionAble::TYPE['PERSONAL'],
                'contact' => '029-87654330',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ],
            [
                'complaint_id' => '2',
                'title' => '企业投诉',
                'content' => '企业投诉',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['COMPLETE'],
                'member_id' => 2,
                'reply_id' => 2,
                'accept_usergroup_id' => 2,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'subject' => '西安传媒有限公司',
                'name' => '西安传媒有限公司',
                'identify' => '912728A90875215901',
                'type' => IInteractionAble::TYPE['ENTERPRISE'],
                'contact' => '029-87654333',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ],
            [
                'complaint_id' => '3',
                'title' => '个人投诉',
                'content' => '个人投诉',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['ACCEPTING'],
                'member_id' => 1,
                'reply_id' => 1,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'subject' => '张文科',
                'name' => '张文科',
                'identify' => '412825198009073427',
                'type' => IInteractionAble::TYPE['PERSONAL'],
                'contact' => '029-87654230',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ]
        ];
    }

    protected function applyFormComplaint() : array
    {
        $applyInfoOne = $this->complaint()[0];
        $applyInfoTwo = $this->complaint()[1];

        return
        [
            [
                'apply_form_id' => 1,
                'title' => '个人投诉',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 0,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoOne))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedComplaint::APPLY_COMPLAINT_CATEGORY,
                'apply_info_type' => UnAuditedComplaint::APPLY_COMPLAINT_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'apply_form_id' => 2,
                'title' => '企业投诉',
                'relation_id' => 2,
                'apply_usergroup_id' => 2,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoTwo))),
                'reject_reason' => '数据不合理',
                'apply_info_category' => UnAuditedComplaint::APPLY_COMPLAINT_CATEGORY,
                'apply_info_type' => UnAuditedComplaint::APPLY_COMPLAINT_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['REJECT'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
    
    protected function praise() : array
    {
        return
        [
            [
                'praise_id' => '1',
                'title' => '个人表扬',
                'content' => '个人表扬',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['PENDING'],
                'member_id' => 1,
                'reply_id' => 0,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '程峰',
                'subject' => '程峰',
                'identify' => '412825199109073428',
                'type' => IInteractionAble::TYPE['PERSONAL'],
                'contact' => '029-87654330',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ],
            [
                'praise_id' => '2',
                'title' => '企业表扬',
                'content' => '企业表扬',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['COMPLETE'],
                'member_id' => 2,
                'reply_id' => 2,
                'accept_usergroup_id' => 2,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '陕西传媒有限公司',
                'subject' => '陕西传媒有限公司',
                'identify' => '912728A90875215901',
                'type' => IInteractionAble::TYPE['ENTERPRISE'],
                'contact' => '029-87654333',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ],
            [
                'praise_id' => '3',
                'title' => '个人表扬',
                'content' => '个人表扬',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['ACCEPTING'],
                'member_id' => 1,
                'reply_id' => 1,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '程峰',
                'subject' => '程峰',
                'identify' => '412825198009073427',
                'type' => IInteractionAble::TYPE['PERSONAL'],
                'contact' => '029-87654230',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ],
            [
                'praise_id' => '4',
                'title' => '企业表扬',
                'content' => '企业表扬',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['COMPLETE'],
                'member_id' => 1,
                'reply_id' => 2,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['PUBLISH'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'name' => '陕西传媒责任有限公司',
                'subject' => '陕西传媒责任有限公司',
                'identify' => '912728A90875215901',
                'type' => IInteractionAble::TYPE['ENTERPRISE'],
                'contact' => '029-87654333',
                'images' => json_encode(array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg')))
            ],
        ];
    }

    protected function applyFormPraise() : array
    {
        $applyInfoOne = $this->praise()[0];
        $applyInfoTwo = $this->praise()[1];

        return
        [
            [
                'apply_form_id' => 1,
                'title' => '个人表扬',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 0,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoOne))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedPraise::APPLY_PRAISE_CATEGORY,
                'apply_info_type' => UnAuditedPraise::APPLY_PRAISE_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'apply_form_id' => 2,
                'title' => '企业表扬',
                'relation_id' => 2,
                'apply_usergroup_id' => 2,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoTwo))),
                'reject_reason' => '数据不合理',
                'apply_info_category' => UnAuditedPraise::APPLY_PRAISE_CATEGORY,
                'apply_info_type' => UnAuditedPraise::APPLY_PRAISE_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['REJECT'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
    
    protected function feedback() : array
    {
        return
        [
            [
                'feedback_id' => '1',
                'title' => '个人反馈',
                'content' => '个人反馈',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['PENDING'],
                'member_id' => 1,
                'reply_id' => 0,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'feedback_id' => '2',
                'title' => '企业反馈',
                'content' => '企业反馈',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['COMPLETE'],
                'member_id' => 2,
                'reply_id' => 2,
                'accept_usergroup_id' => 2,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'feedback_id' => '3',
                'title' => '个人反馈',
                'content' => '个人反馈',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['ACCEPTING'],
                'member_id' => 1,
                'reply_id' => 1,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function applyFormFeedback() : array
    {
        $applyInfoOne = $this->feedback()[0];
        $applyInfoTwo = $this->feedback()[1];

        return
        [
            [
                'apply_form_id' => 1,
                'title' => '个人反馈',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 0,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoOne))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedFeedback::APPLY_FEEDBACK_CATEGORY,
                'apply_info_type' => UnAuditedFeedback::APPLY_FEEDBACK_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'apply_form_id' => 2,
                'title' => '企业反馈',
                'relation_id' => 2,
                'apply_usergroup_id' => 2,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoTwo))),
                'reject_reason' => '数据不合理',
                'apply_info_category' => UnAuditedFeedback::APPLY_FEEDBACK_CATEGORY,
                'apply_info_type' => UnAuditedFeedback::APPLY_FEEDBACK_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['REJECT'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function qaInteraction() : array
    {
        return
        [
            [
                'qa_id' => '1',
                'title' => '个人问答',
                'content' => '个人问答',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['PENDING'],
                'member_id' => 1,
                'reply_id' => 0,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'qa_id' => '2',
                'title' => '企业问答',
                'content' => '企业问答',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['COMPLETE'],
                'member_id' => 2,
                'reply_id' => 2,
                'accept_usergroup_id' => 2,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'qa_id' => '3',
                'title' => '个人问答',
                'content' => '个人问答',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['ACCEPTING'],
                'member_id' => 1,
                'reply_id' => 1,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'qa_id' => '4',
                'title' => '企业问答',
                'content' => '企业问答',
                'accept_status' => IInteractionAble::ACCEPT_STATUS['COMPLETE'],
                'member_id' => 1,
                'reply_id' => 2,
                'accept_usergroup_id' => 1,
                'status' => IInteractionAble::INTERACTION_STATUS['PUBLISH'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
        ];
    }

    protected function applyFormQA() : array
    {
        $applyInfoOne = $this->qaInteraction()[0];
        $applyInfoTwo = $this->qaInteraction()[1];

        return
        [
            [
                'apply_form_id' => 1,
                'title' => '个人问答',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 0,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoOne))),
                'reject_reason' => '',
                'apply_info_category' => UnAuditedQA::APPLY_QA_CATEGORY,
                'apply_info_type' => UnAuditedQA::APPLY_QA_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'apply_form_id' => 2,
                'title' => '企业问答',
                'relation_id' => 2,
                'apply_usergroup_id' => 2,
                'apply_crew_id' => 1,
                'operation_type' => IApproveAble::OPERATION_TYPE['ACCEPT'],
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoTwo))),
                'reject_reason' => '数据不合理',
                'apply_info_category' => UnAuditedQA::APPLY_QA_CATEGORY,
                'apply_info_type' => UnAuditedQA::APPLY_QA_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['REJECT'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function reply() : array
    {
        return
        [
            [
                'reply_id' => 1,
                'content' => '回复内容',
                'images' => json_encode(array(array('name'=>'回复图片名称', 'identify'=>'回复图片地址.jpg'))),
                'accept_usergroup_id' => 1,
                'crew_id' => 1,
                'admissibility' => IInteractionAble::ADMISSIBILITY['ADMISSIBLE'],
                'status' => Reply::STATUS_NORMAL,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'reply_id' => 2,
                'content' => '回复内容',
                'images' => json_encode(array(array('name'=>'回复图片名称', 'identify'=>'回复图片地址.jpg'))),
                'accept_usergroup_id' => 2,
                'crew_id' => 2,
                'admissibility' => IInteractionAble::ADMISSIBILITY['INADMISSIBLE'],
                'status' => Reply::STATUS_NORMAL,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }
}
