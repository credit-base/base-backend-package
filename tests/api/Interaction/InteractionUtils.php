<?php
namespace Base\Interaction;

trait InteractionUtils
{
    private function interactionCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['title'], $attributes['title']);
        $this->assertEquals($setDataArray['content'], $attributes['content']);
        $this->assertEquals($setDataArray['accept_status'], $attributes['acceptStatus']);
        
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsMember = $responseDataArray['relationships']['member']['data'];
        $this->assertEquals('members', $relationshipsMember['type']);
        $this->assertEquals($setDataArray['member_id'], $relationshipsMember['id']);

        $relationshipsReply = $responseDataArray['relationships']['reply']['data'];
        $this->assertEquals('replies', $relationshipsReply['type']);
        $this->assertEquals($setDataArray['reply_id'], $relationshipsReply['id']);

        $relationshipsUserGroup = $responseDataArray['relationships']['acceptUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['accept_usergroup_id'], $relationshipsUserGroup['id']);
    }

    private function commentInteractionCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['name'], $attributes['name']);
        $this->assertEquals($setDataArray['identify'], $attributes['identify']);
        $this->assertEquals($setDataArray['contact'], $attributes['contact']);
        $this->assertEquals(json_decode($setDataArray['images'], true), $attributes['images']);
    }

    private function appealCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['appeal_id'], $responseDataArray['id']);
        $this->assertEquals('appeals', $responseDataArray['type']);

        $this->interactionCompareData($setDataArray, $responseDataArray);
        $this->commentInteractionCompareData($setDataArray, $responseDataArray);
        
        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['type'], $attributes['appealType']);
        $this->assertEquals(json_decode($setDataArray['certificates'], true), $attributes['certificates']);
    }

    private function complaintCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['complaint_id'], $responseDataArray['id']);
        $this->assertEquals('complaints', $responseDataArray['type']);

        $this->interactionCompareData($setDataArray, $responseDataArray);
        $this->commentInteractionCompareData($setDataArray, $responseDataArray);
        
        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['type'], $attributes['complaintType']);
        $this->assertEquals($setDataArray['subject'], $attributes['subject']);
    }

    private function praiseCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['praise_id'], $responseDataArray['id']);
        $this->assertEquals('praises', $responseDataArray['type']);

        $this->interactionCompareData($setDataArray, $responseDataArray);
        $this->commentInteractionCompareData($setDataArray, $responseDataArray);
        
        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['type'], $attributes['praiseType']);
        $this->assertEquals($setDataArray['subject'], $attributes['subject']);
    }

    private function feedbackCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['feedback_id'], $responseDataArray['id']);
        $this->assertEquals('feedbacks', $responseDataArray['type']);

        $this->interactionCompareData($setDataArray, $responseDataArray);
    }

    private function qaCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['qa_id'], $responseDataArray['id']);
        $this->assertEquals('qas', $responseDataArray['type']);

        $this->interactionCompareData($setDataArray, $responseDataArray);
    }

    private function unAuditedInteractionCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($applyInfo['title'], $attributes['title']);
        $this->assertEquals($applyInfo['content'], $attributes['content']);
        $this->assertEquals($applyInfo['accept_status'], $attributes['acceptStatus']);
        $this->assertEquals($applyInfo['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $this->assertEquals($setDataArray['apply_status'], $attributes['applyStatus']);
        $this->assertEquals($setDataArray['reject_reason'], $attributes['rejectReason']);
        $this->assertEquals($setDataArray['operation_type'], $attributes['operationType']);
        $this->assertEquals($setDataArray['apply_info_type'], $attributes['applyInfoType']);
        $this->assertEquals($setDataArray['apply_info_category'], $attributes['applyInfoCategory']);

        $relationshipsRelation = $responseDataArray['relationships']['relation']['data'];
        $this->assertEquals('members', $relationshipsRelation['type']);
        $this->assertEquals($setDataArray['relation_id'], $relationshipsRelation['id']);

        $relationshipsUserGroup = $responseDataArray['relationships']['acceptUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($applyInfo['accept_usergroup_id'], $relationshipsUserGroup['id']);

        $relationshipsMember = $responseDataArray['relationships']['member']['data'];
        $this->assertEquals('members', $relationshipsMember['type']);
        $this->assertEquals($applyInfo['member_id'], $relationshipsMember['id']);
        
        $relationshipsApplyCrew = $responseDataArray['relationships']['applyCrew']['data'];
        $this->assertEquals('crews', $relationshipsApplyCrew['type']);
        $this->assertEquals($setDataArray['apply_crew_id'], $relationshipsApplyCrew['id']);

        $relationshipsApplyUserGroup = $responseDataArray['relationships']['applyUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsApplyUserGroup['type']);
        $this->assertEquals($setDataArray['apply_usergroup_id'], $relationshipsApplyUserGroup['id']);
    }

    private function unAuditedCommentInteractionCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($applyInfo['name'], $attributes['name']);
        $this->assertEquals($applyInfo['identify'], $attributes['identify']);
        $this->assertEquals(json_decode($applyInfo['images'], true), $attributes['images']);
        $this->assertEquals($applyInfo['contact'], $attributes['contact']);
    }

    private function unAuditedAppealCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['apply_form_id'], $responseDataArray['id']);
        $this->assertEquals('unAuditedAppeals', $responseDataArray['type']);

        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals(json_decode($applyInfo['certificates'], true), $attributes['certificates']);
        $this->assertEquals($applyInfo['type'], $attributes['appealType']);
        $this->assertEquals($applyInfo['appeal_id'], $attributes['appealId']);

        $this->unAuditedInteractionCompareData($setDataArray, $responseDataArray);
        $this->unAuditedCommentInteractionCompareData($setDataArray, $responseDataArray);
    }

    private function unAuditedComplaintCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['apply_form_id'], $responseDataArray['id']);
        $this->assertEquals('unAuditedComplaints', $responseDataArray['type']);

        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($applyInfo['subject'], $attributes['subject']);
        $this->assertEquals($applyInfo['type'], $attributes['complaintType']);
        $this->assertEquals($applyInfo['complaint_id'], $attributes['complaintId']);

        $this->unAuditedInteractionCompareData($setDataArray, $responseDataArray);
        $this->unAuditedCommentInteractionCompareData($setDataArray, $responseDataArray);
    }

    private function unAuditedPraiseCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['apply_form_id'], $responseDataArray['id']);
        $this->assertEquals('unAuditedPraises', $responseDataArray['type']);

        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($applyInfo['subject'], $attributes['subject']);
        $this->assertEquals($applyInfo['type'], $attributes['praiseType']);
        $this->assertEquals($applyInfo['praise_id'], $attributes['praiseId']);

        $this->unAuditedInteractionCompareData($setDataArray, $responseDataArray);
        $this->unAuditedCommentInteractionCompareData($setDataArray, $responseDataArray);
    }

    private function unAuditedFeedbackCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['apply_form_id'], $responseDataArray['id']);
        $this->assertEquals('unAuditedFeedbacks', $responseDataArray['type']);

        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($applyInfo['feedback_id'], $attributes['feedbackId']);

        $this->unAuditedInteractionCompareData($setDataArray, $responseDataArray);
    }

    private function unAuditedQACompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['apply_form_id'], $responseDataArray['id']);
        $this->assertEquals('unAuditedQAs', $responseDataArray['type']);

        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($applyInfo['qa_id'], $attributes['qaId']);

        $this->unAuditedInteractionCompareData($setDataArray, $responseDataArray);
    }

    private function replyCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['reply_id'], $responseDataArray['id']);
        $this->assertEquals('replies', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['content'], $attributes['content']);
        $this->assertEquals(json_decode($setDataArray['images'], true), $attributes['images']);
        $this->assertEquals($setDataArray['admissibility'], $attributes['admissibility']);
        
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsCrew = $responseDataArray['relationships']['crew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals($setDataArray['crew_id'], $relationshipsCrew['id']);

        $relationshipsUserGroup = $responseDataArray['relationships']['acceptUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['accept_usergroup_id'], $relationshipsUserGroup['id']);
    }
}
