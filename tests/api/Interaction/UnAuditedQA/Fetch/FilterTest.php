<?php
namespace Base\Interaction\UnAuditedQA\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Common\Model\IApproveAble;

use Base\Interaction\Model\UnAuditedQA;

use Base\Interaction\InteractionTrait;

/**
 * 测试信用问答审核接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条信用问答审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_apply_form' => $this->applyFormQA(),
                'pcore_reply' => $this->reply(),
                'pcore_crew' => $this->crew(),
                'pcore_member' => $this->member(),
                'pcore_user_group' => $this->userGroup()
            ]
        );
    }

    public function testViewUnAuditedQA()
    {
        $setUnAuditedQAList = $this->applyFormQA();
        foreach ($setUnAuditedQAList as $setUnAuditedQA) {
            $setUnAuditedQAList[$setUnAuditedQA['apply_form_id']] = $setUnAuditedQA;
        }

        $setCrewList = $this->crew();
        $setMemberList = $this->member();
        $setReplyList = $this->reply();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedQas?filter[applyUserGroup]=2&filter[title]=问答&filter[relation]=2
            &filter[applyInfoType]='. UnAuditedQA::APPLY_QA_TYPE.
            '&filter[applyInfoCategory]='. UnAuditedQA::APPLY_QA_CATEGORY.'&
            filter[applyStatus]='. IApproveAble::APPLY_STATUS['REJECT'].'&
            filter[operationType]='. IApproveAble::OPERATION_TYPE['ACCEPT'].
            '&include=member,acceptUserGroup,reply,applyUserGroup,applyCrew,relation',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $unAuditedQA) {
            $this->unAuditedQACompareData($setUnAuditedQAList[$unAuditedQA['id']], $unAuditedQA);
        }

        $included = $contents['included'];

        $this->memberCompareData($setMemberList[1], $included[0]);
        $this->crewCompareData($setCrewList[0], $included[1]);
        $this->userGroupCompareData($setUserGroupList[1], $included[2]);
        $this->replyCompareData($setReplyList[1], $included[3]);
    }
}
