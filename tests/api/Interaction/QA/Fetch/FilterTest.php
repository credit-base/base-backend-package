<?php
namespace Base\Interaction\QA\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\Model\IInteractionAble;

use Base\Interaction\InteractionTrait;

/**
 * 测试信用问答类接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
        $this->clear('pcore_qa');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条信用问答类数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_qa' => $this->qaInteraction(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_member' => $this->member(),
                'pcore_reply' => $this->reply()
            ]
        );
    }

    public function testViewQA()
    {
        $setQA = $this->qaInteraction()[2];
        $setMemberList = $this->member();
        $setReplyList = $this->reply();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'qas?filter[member]=1&filter[acceptUserGroup]=1&
            filter[title]=问答&filter[status]='.IInteractionAble::INTERACTION_STATUS['NORMAL'].
            '&filter[acceptStatus]='.IInteractionAble::ACCEPT_STATUS['ACCEPTING'].
            '&include=acceptUserGroup,member,reply',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $qaInteraction) {
            $this->qaCompareData($setQA, $qaInteraction);
        }

        $included = $contents['included'];

        $this->userGroupCompareData($setUserGroupList[0], $included[0]);
        $this->memberCompareData($setMemberList[0], $included[1]);
        $this->replyCompareData($setReplyList[0], $included[2]);
    }
}
