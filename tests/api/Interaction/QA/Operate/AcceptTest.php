<?php
namespace Base\Interaction\QA\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\InteractionUtils;
use Base\Interaction\SetDataApiTrait;
use Base\Interaction\Model\IInteractionAble;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;

/**
 * 测试信用问答接口受理
 */
class AcceptTest extends TestCase
{
    use MemberSetDataApiTrait, TestCaseTrait, DbTrait, SetDataApiTrait, InteractionUtils, UserGroupSetDataApiTrait,
    CrewSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_qa');
        $this->clear('pcore_reply');
        $this->clear('pcore_apply_form');
    }

    /**
     * 不存在信用问答数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_qa' => $this->qaInteraction(),
                'pcore_member' => $this->member()
            ]
        );
    }

    public function testViewQA()
    {
        $setUnAuditedQA = $this->applyFormQA()[0];

        $data = array(
            'data' => array(
                "type"=>"qas",
                "relationships"=>array(
                    "reply"=>array(
                        "data"=>array(
                            array(
                                "type"=>"replies",
                                "attributes"=>array(
                                    "admissibility"=>IInteractionAble::ADMISSIBILITY['ADMISSIBLE'],
                                    "images"=>array(array('name'=>'回复图片名称', 'identify'=>'回复图片地址.jpg')),
                                    "content"=>"回复内容"
                                ),
                                "relationships"=>array(
                                    "crew"=>array(
                                        "data"=>array(
                                            array("type"=>"crews","id"=>1)
                                        )
                                    )
                                ),
                            )
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'qas/1/accept',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->UnAuditedQACompareData($setUnAuditedQA, $contents['data']);
    }
}
