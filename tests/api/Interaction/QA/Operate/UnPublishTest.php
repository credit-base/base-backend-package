<?php
namespace Base\Interaction\QA\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\SetDataApiTrait;
use Base\Interaction\Model\IInteractionAble;

/**
 * 测试信用问答接口取消公示
 */
class UnPublishTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qa');
    }

    /**
     * 存在一条信用问答数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_qa' => $this->qaInteraction()
            ]
        );
    }

    public function testViewQA()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'qas/4/unPublish',
            [
                'headers'=>Core::$container->get('client.headers')
            ]
        );
        
        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['NORMAL'], $attributes['status']);
    }
}
