<?php
namespace Base\Interaction\UnAuditedPraise\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Common\Model\IApproveAble;

use Base\Interaction\Model\UnAuditedPraise;

use Base\Interaction\InteractionTrait;

/**
 * 测试信用表扬审核接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
        $this->clear('pcore_apply_form');
    }

    /**
     * 存在多条信用表扬审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_apply_form' => $this->applyFormPraise(),
                'pcore_reply' => $this->reply(),
                'pcore_member' => $this->member(),
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup()
            ]
        );
    }

    public function testViewUnAuditedPraise()
    {
        $setUnAuditedPraiseList = $this->applyFormPraise();
        foreach ($setUnAuditedPraiseList as $setUnAuditedPraise) {
            $setUnAuditedPraiseList[$setUnAuditedPraise['apply_form_id']] = $setUnAuditedPraise;
        }

        $setCrewList = $this->crew();
        $setMemberList = $this->member();
        $setReplyList = $this->reply();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedPraises?filter[applyUserGroup]=2&filter[title]=表扬&filter[relation]=2
            &filter[applyInfoType]='. UnAuditedPraise::APPLY_PRAISE_TYPE.
            '&filter[applyInfoCategory]='. UnAuditedPraise::APPLY_PRAISE_CATEGORY.'&
            filter[applyStatus]='. IApproveAble::APPLY_STATUS['REJECT'].'&
            filter[operationType]='. IApproveAble::OPERATION_TYPE['ACCEPT'].
            '&include=member,acceptUserGroup,reply,applyUserGroup,applyCrew,relation',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $unAuditedPraise) {
            $this->unAuditedPraiseCompareData($setUnAuditedPraiseList[$unAuditedPraise['id']], $unAuditedPraise);
        }

        $included = $contents['included'];

        $this->memberCompareData($setMemberList[1], $included[0]);
        $this->crewCompareData($setCrewList[0], $included[1]);
        $this->userGroupCompareData($setUserGroupList[1], $included[2]);
        $this->replyCompareData($setReplyList[1], $included[3]);
    }
}
