<?php
namespace Base\Interaction\UnAuditedPraise\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Common\Model\IApproveAble;

use Base\Interaction\SetDataApiTrait;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

/**
 * 测试信用表扬接口重新编辑
 */
class EditTest extends TestCase
{
    use CrewSetDataApiTrait, TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_reply');
    }

    /**
     * 存在一条信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_reply' => $this->reply(),
                'pcore_apply_form' => $this->applyFormPraise()
            ]
        );
    }

    public function testViewUnAuditedPraise()
    {
        $data = array(
            'data' => array(
                "type"=>"unAuditedPraises",
                "relationships"=>array(
                    "reply"=>array(
                        "data"=>array(
                            array(
                                "type"=>"replies",
                                "attributes"=>array(
                                    "content"=>"信用表扬重新受理回复内容",
                                    "images"=>array(
                                        array('name' => '信用表扬重新受理回复图片', 'identify' => 'identify.jpg')
                                    ),
                                    "admissibility"=> 2
                                ),
                                "relationships"=>array(
                                    "crew"=>array(
                                        "data"=>array(
                                            array("type"=>"crews","id"=>1)
                                        )
                                    )
                                ),
                            )
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'unAuditedPraises/2/resubmit',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $attributes['applyStatus']);
    }
}
