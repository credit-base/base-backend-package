<?php
namespace Base\Interaction\Appeal\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\Model\IInteractionAble;

use Base\Interaction\InteractionTrait;

/**
 * 测试异议申诉类接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
        $this->clear('pcore_appeal');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条异议申诉类数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_appeal' => $this->appeal(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_member' => $this->member(),
                'pcore_reply' => $this->reply()
            ]
        );
    }

    public function testViewAppeal()
    {
        $setAppeal = $this->appeal()[2];
        $setReplyList = $this->reply();
        $setMemberList = $this->member();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'appeals?filter[member]=1&filter[acceptUserGroup]=1&
            filter[title]=申诉&filter[status]='.IInteractionAble::INTERACTION_STATUS['NORMAL'].
            '&filter[acceptStatus]='.IInteractionAble::ACCEPT_STATUS['ACCEPTING'].
            '&include=acceptUserGroup,member,reply',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $appeal) {
            $this->appealCompareData($setAppeal, $appeal);
        }

        $included = $contents['included'];

        $this->userGroupCompareData($setUserGroupList[0], $included[0]);
        $this->memberCompareData($setMemberList[0], $included[1]);
        $this->replyCompareData($setReplyList[0], $included[2]);
    }
}
