<?php
namespace Base\Interaction\Appeal\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

use Base\Interaction\InteractionTrait;

/**
 * 测试异议申诉类接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, DepartmentSetDataApiTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
        $this->clear('pcore_crew');
        $this->clear('pcore_appeal');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
    }

    /**
     * 存在一条异议申诉类数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_appeal' => $this->appeal(),
                'pcore_crew' => $this->crew(),
                'pcore_reply' => $this->reply(),
                'pcore_member' => $this->member(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewAppeal()
    {
        $setAppeal = $this->appeal()[2];
        $setUserGroup = $this->userGroup()[0];
        $setMember = $this->member()[0];
        $setReply = $this->reply()[0];
        $setCrew = $this->crew()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'appeals/3?include=acceptUserGroup,member,reply,reply.crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->appealCompareData($setAppeal, $contents['data']);
        $this->userGroupCompareData($setUserGroup, $contents['included'][0]);
        $this->memberCompareData($setMember, $contents['included'][1]);
        $this->crewCompareData($setCrew, $contents['included'][2]);
        $this->replyCompareData($setReply, $contents['included'][3]);
    }
}
