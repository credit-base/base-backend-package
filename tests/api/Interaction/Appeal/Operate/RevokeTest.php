<?php
namespace Base\Interaction\Appeal\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\SetDataApiTrait;
use Base\Interaction\Model\IInteractionAble;

/**
 * 测试异议申诉接口撤销
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_appeal');
    }

    /**
     * 存在一条异议申诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_appeal' => $this->appeal()
            ]
        );
    }

    public function testViewAppeal()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'appeals/1/revoke',
            [
                'headers'=>Core::$container->get('client.headers')
            ]
        );
        
        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['REVOKE'], $attributes['status']);
    }
}
