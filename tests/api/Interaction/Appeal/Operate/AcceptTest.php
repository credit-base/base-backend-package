<?php
namespace Base\Interaction\Appeal\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\SetDataApiTrait;
use Base\Interaction\InteractionUtils;
use Base\Interaction\Model\IInteractionAble;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

/**
 * 测试异议申诉接口受理
 */
class AcceptTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, InteractionUtils, UserGroupSetDataApiTrait,
    CrewSetDataApiTrait, MemberSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_appeal');
        $this->clear('pcore_reply');
        $this->clear('pcore_apply_form');
    }

    /**
     * 不存在异议申诉数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_appeal' => $this->appeal(),
                'pcore_member' => $this->member()
            ]
        );
    }

    public function testViewAppeal()
    {
        $setUnAuditedAppeal = $this->applyFormAppeal()[0];

        $data = array(
            'data' => array(
                "type"=>"appeals",
                "relationships"=>array(
                    "reply"=>array(
                        "data"=>array(
                            array(
                                "type"=>"replies",
                                "attributes"=>array(
                                    "content"=>"回复内容",
                                    "images"=>array(array('name'=>'回复图片名称', 'identify'=>'回复图片地址.jpg')),
                                    "admissibility"=>IInteractionAble::ADMISSIBILITY['ADMISSIBLE']
                                ),
                                "relationships"=>array(
                                    "crew"=>array(
                                        "data"=>array(
                                            array("type"=>"crews","id"=>1)
                                        )
                                    )
                                ),
                            )
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'appeals/1/accept',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->UnAuditedAppealCompareData($setUnAuditedAppeal, $contents['data']);
    }
}
