<?php
namespace Base\Interaction\Complaint\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

use Base\Interaction\InteractionTrait;

/**
 * 测试信用投诉类接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait, DepartmentSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_complaint');
        $this->clear('pcore_reply');
        $this->clear('pcore_department');
        $this->clear('pcore_member');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * 存在一条信用投诉类数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_reply' => $this->reply(),
                'pcore_complaint' => $this->complaint(),
                'pcore_member' => $this->member(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewComplaint()
    {
        $setComplaint = $this->complaint()[2];
        $setUserGroup = $this->userGroup()[0];
        $setCrew = $this->crew()[0];
        $setReply = $this->reply()[0];
        $setMember = $this->member()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'complaints/3?include=acceptUserGroup,member,reply,reply.crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->complaintCompareData($setComplaint, $contents['data']);
        $this->userGroupCompareData($setUserGroup, $contents['included'][0]);
        $this->memberCompareData($setMember, $contents['included'][1]);
        $this->crewCompareData($setCrew, $contents['included'][2]);
        $this->replyCompareData($setReply, $contents['included'][3]);
    }
}
