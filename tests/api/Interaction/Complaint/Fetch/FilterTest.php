<?php
namespace Base\Interaction\Complaint\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\Model\IInteractionAble;

use Base\Interaction\InteractionTrait;

/**
 * 测试信用投诉类接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, InteractionTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_complaint');
        $this->clear('pcore_member');
        $this->clear('pcore_reply');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条信用投诉类数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_complaint' => $this->complaint(),
                'pcore_member' => $this->member(),
                'pcore_reply' => $this->reply(),
                'pcore_user_group' => $this->userGroup()
            ]
        );
    }

    public function testViewComplaint()
    {
        $setComplaint = $this->complaint()[2];
        $setReplyList = $this->reply();
        $setMemberList = $this->member();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'complaints?filter[member]=1&filter[acceptUserGroup]=1&
            filter[title]=投诉&filter[status]='.IInteractionAble::INTERACTION_STATUS['NORMAL'].
            '&filter[acceptStatus]='.IInteractionAble::ACCEPT_STATUS['ACCEPTING'].
            '&include=acceptUserGroup,member,reply',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $complaint) {
            $this->complaintCompareData($setComplaint, $complaint);
        }

        $included = $contents['included'];

        $this->userGroupCompareData($setUserGroupList[0], $included[0]);
        $this->memberCompareData($setMemberList[0], $included[1]);
        $this->replyCompareData($setReplyList[0], $included[2]);
    }
}
