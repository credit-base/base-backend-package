<?php
namespace Base\Interaction\Praise\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\InteractionUtils;
use Base\Interaction\SetDataApiTrait;
use Base\Interaction\Model\IInteractionAble;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;

/**
 * 测试信用表扬接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, InteractionUtils, UserGroupSetDataApiTrait, MemberSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_user_group');
        $this->clear('pcore_member');
        $this->clear('pcore_praise');
    }

    /**
     * 不存在信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_member' => $this->member()
            ]
        );
    }

    public function testViewPraise()
    {
        $setPraise = $this->praise()[0];
        $data = array(
            'data' => array(
                "type"=>"praises",
                "attributes"=>array(
                    "title"=>"个人表扬",
                    "content"=>"个人表扬",
                    "name"=>"程峰",
                    "subject"=>"程峰",
                    "identify"=>"412825199109073428",
                    "type"=> IInteractionAble::TYPE['PERSONAL'],
                    "contact"=>"029-87654330",
                    "images"=>array(array('name'=>'图片名称', 'identify'=>'图片地址.jpg'))
                ),
                "relationships"=>array(
                    "member"=>array(
                        "data"=>array(
                            array("type"=>"members","id"=>1)
                        )
                    ),
                    "acceptUserGroup"=>array(
                        "data"=>array(
                            array("type"=>"userGroups","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'praises',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->praiseCompareData($setPraise, $contents['data']);
    }
}
