<?php
namespace Base\Interaction\Praise\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Interaction\SetDataApiTrait;
use Base\Interaction\Model\IInteractionAble;

/**
 * 测试信用表扬接口撤销
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_praise');
    }

    /**
     * 存在一条信用表扬数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_praise' => $this->praise()
            ]
        );
    }

    public function testViewPraise()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'praises/1/revoke',
            [
                'headers'=>Core::$container->get('client.headers')
            ]
        );
        
        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IInteractionAble::INTERACTION_STATUS['REVOKE'], $attributes['status']);
    }
}
