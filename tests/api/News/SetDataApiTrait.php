<?php
namespace Base\News;

use Marmot\Core;

use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\Model\UnAuditedNews;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

trait SetDataApiTrait
{
    protected function news() : array
    {
        return
        [
            [
                'news_id' => 1,
                'title' => '昨天天气阴',
                'source' => '昨天天气阴',
                'cover' => json_encode(array('name'=>'封面', 'identify'=>'封面.jpg')),
                'attachments' => json_encode(array(array('name'=>'附件', 'identify'=>'附件.pdf'))),
                'content' => '昨天天气阴',
                'description' => '昨天天气阴',
                'parent_category' => 0,
                'category' => 51,
                'type' => 151,
                'crew_id' => 1,
                'publish_usergroup_id' => 1,
                'stick' => ITopAble::STICK['DISABLED'],
                'status' => IEnableAble::STATUS['ENABLED'],
                'dimension' => News::DIMENSIONALITY['SOCIOLOGY'],
                'banner_status' => Banner::BANNER_STATUS['DISABLED'],
                'banner_image' => json_encode(array('name'=>'名称', 'identify'=>'地址.jpg')),
                'home_page_show_status' => News::HOME_PAGE_SHOW_STATUS['DISABLED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'news_id' => 2,
                'title' => '今天天气多云',
                'source' => '今天天气多云',
                'cover' => json_encode(array('name'=>'1', 'identify'=>'1.jpg')),
                'attachments' => json_encode(array(array('name'=>'1', 'identify'=>'1.pdf'))),
                'content' => '今天天气多云',
                'description' => '今天天气多云',
                'parent_category' => 0,
                'category' => 1,
                'type' => 1,
                'crew_id' => 2,
                'publish_usergroup_id' => 2,
                'stick' => ITopAble::STICK['ENABLED'],
                'status' => IEnableAble::STATUS['ENABLED'],
                'dimension' => News::DIMENSIONALITY['SOCIOLOGY'],
                'banner_status' => Banner::BANNER_STATUS['DISABLED'],
                'banner_image' => json_encode(array('name'=>'1', 'identify'=>'1.jpg')),
                'home_page_show_status' => News::HOME_PAGE_SHOW_STATUS['ENABLED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'news_id' => 3,
                'title' => '明天天气晴',
                'source' => '明天天气晴',
                'cover' => json_encode(array('name'=>'2', 'identify'=>'2.jpg')),
                'attachments' => json_encode(array(array('name'=>'2', 'identify'=>'2.pdf'))),
                'content' => '明天天气晴',
                'description' => '明天天气晴',
                'parent_category' => 0,
                'category' => 1,
                'type' => 1,
                'crew_id' => 1,
                'publish_usergroup_id' => 1,
                'stick' => ITopAble::STICK['ENABLED'],
                'status' => IEnableAble::STATUS['DISABLED'],
                'dimension' => News::DIMENSIONALITY['SOCIOLOGY'],
                'banner_status' => Banner::BANNER_STATUS['DISABLED'],
                'banner_image' => json_encode(array('name'=>'2', 'identify'=>'2.jpg')),
                'home_page_show_status' => News::HOME_PAGE_SHOW_STATUS['DISABLED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyForm(
        $operationType = IApproveAble::OPERATION_TYPE['ADD']
    ) : array {
        $news = $this->news()[0];
        if ($operationType == IApproveAble::OPERATION_TYPE['ADD']) {
            $news['news_id'] = 0;
        }
        $applyInfoOne = base64_encode(gzcompress(serialize($news)));
        $applyInfoTwo = base64_encode(gzcompress(serialize($this->news()[1])));
        $applyInfoThree = base64_encode(gzcompress(serialize($this->news()[2])));
        
        return
        [
            [
                'apply_form_id' => 1,
                'title' => '昨天天气阴',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 0,
                'operation_type' => $operationType,
                'apply_info' => $applyInfoOne,
                'reject_reason' => '',
                'apply_info_category' => UnAuditedNews::APPLY_NEWS_CATEGORY,
                'apply_info_type' => 151,
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'apply_form_id' => 2,
                'title' => '今天天气多云',
                'relation_id' => 2,
                'apply_usergroup_id' => 2,
                'apply_crew_id' => 2,
                'operation_type' => $operationType,
                'apply_info' => $applyInfoTwo,
                'reject_reason' => '驳回原因',
                'apply_info_category' => UnAuditedNews::APPLY_NEWS_CATEGORY,
                'apply_info_type' => 1,
                'apply_status' => IApproveAble::APPLY_STATUS['REJECT'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'apply_form_id' => 3,
                'title' => '明天天气晴',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 0,
                'operation_type' => $operationType,
                'apply_info' => $applyInfoThree,
                'reject_reason' => '',
                'apply_info_category' => UnAuditedNews::APPLY_NEWS_CATEGORY,
                'apply_info_type' => 1,
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
