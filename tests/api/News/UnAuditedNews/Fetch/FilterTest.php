<?php
namespace Base\News\UnAuditedNews\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\News\NewsUtils;
use Base\News\SetDataApiTrait;
use Base\News\Model\UnAuditedNews;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Common\Model\IApproveAble;

/**
 * 测试新闻审核接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, NewsUtils, CrewSetDataApiTrait, CrewUtils,
    UserGroupSetDataApiTrait, UserGroupUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条新闻审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_apply_form' => $this->applyForm()
            ]
        );
    }

    public function testViewUnAuditedNews()
    {
        $setUnAuditedNewsList = $this->applyForm();
        foreach ($setUnAuditedNewsList as $setUnAuditedNews) {
            $setUnAuditedNewsList[$setUnAuditedNews['apply_form_id']] = $setUnAuditedNews;
        }

        $setCrewList = $this->crew();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedNews?filter[applyUserGroup]=2&filter[title]=今天天气多云&filter[relation]=2
            &filter[applyInfoType]=1&filter[applyInfoCategory]='. UnAuditedNews::APPLY_NEWS_CATEGORY.'&
            filter[applyStatus]='. IApproveAble::APPLY_STATUS['REJECT'].'&
            filter[operationType]='. IApproveAble::OPERATION_TYPE['ADD'].
            '&include=crew,publishUserGroup,applyUserGroup,applyCrew,relation',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $unAuditedNews) {
            $this->unAuditedNewsCompareData($setUnAuditedNewsList[$unAuditedNews['id']], $unAuditedNews);
        }

        $included = $contents['included'];

        $this->userGroupCompareData($setUserGroupList[1], $included[1]);
        $this->crewCompareData($setCrewList[1], $included[0]);
    }
}
