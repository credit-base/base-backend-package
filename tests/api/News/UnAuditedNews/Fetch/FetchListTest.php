<?php
namespace Base\News\UnAuditedNews\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\News\SetDataApiTrait;
use Base\News\NewsUtils;
use Base\News\Model\UnAuditedNews;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

/**
 * 测试新闻审核接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, NewsUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    CrewSetDataApiTrait, CrewUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条新闻审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_apply_form' => $this->applyForm()
            ]
        );
    }

    public function testViewUnAuditedNews()
    {
        $setUnAuditedNewsList = $this->applyForm();
        $setUserGroupList = $this->userGroup();
        foreach ($setUserGroupList as $setUserGroup) {
            $setUserGroupList[$setUserGroup['user_group_id']] = $setUserGroup;
        }

        $setCrewList = $this->crew();
        foreach ($setCrewList as $setCrew) {
            $setCrewList[$setCrew['crew_id']] = $setCrew;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedNews/1,2?include=crew,publishUserGroup,applyUserGroup,applyCrew,relation',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $unAuditedNews) {
            $this->unAuditedNewsCompareData($setUnAuditedNewsList[$key], $unAuditedNews);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'userGroups') {
                $this->userGroupCompareData($setUserGroupList[$value['id']], $value);
            }
            if ($value['id'] != 0 && $value['type'] == 'crews') {
                $this->crewCompareData($setCrewList[$value['id']], $value);
            }
        }
    }
}
