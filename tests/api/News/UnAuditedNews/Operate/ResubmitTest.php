<?php
namespace Base\News\UnAuditedNews\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\News\SetDataApiTrait;
use Base\News\Model\UnAuditedNews;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

/**
 * 测试信用刊物接口重新编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, UserGroupSetDataApiTrait, CrewSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * 存在一条信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_apply_form' => $this->applyForm()
            ]
        );
    }

    public function testViewUnAuditedNews()
    {
        $data = array(
            'data' => array(
                "type"=>"unAuditedNews",
                "attributes"=>array(
                    "title"=>"新闻重新编辑标题",
                    "source"=>"新闻重新编辑来源",
                    "cover"=>array('name' => '重新编辑', 'identify' => '重新编辑.jpg'),
                    "attachments"=>array(array('name' => '重新编辑', 'identify' => '重新编辑.pdf')),
                    "content"=>"新闻重新编辑内容",
                    "newsType"=>151,
                    "dimension"=>1,
                    "status"=>0,
                    "stick"=>0,
                    "bannerStatus"=>0,
                    "bannerImage"=>array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
                    "homePageShowStatus"=>0,
                ),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'unAuditedNews/2/resubmit',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $attributes['applyStatus']);
        $this->assertEquals('新闻重新编辑标题', $attributes['title']);
        $this->assertEquals('新闻重新编辑来源', $attributes['source']);
        $this->assertEquals(array('name' => '重新编辑', 'identify' => '重新编辑.jpg'), $attributes['cover']);
        $this->assertEquals(
            array(array('name' => '重新编辑', 'identify' => '重新编辑.pdf')),
            $attributes['attachments']
        );
        $this->assertEquals(
            array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
            $attributes['bannerImage']
        );
        $this->assertEquals('新闻重新编辑内容', $attributes['content']);
        $this->assertEquals(151, $attributes['newsType']);
        $this->assertEquals(1, $attributes['dimension']);
        $this->assertEquals(0, $attributes['status']);
        $this->assertEquals(0, $attributes['stick']);
        $this->assertEquals(0, $attributes['bannerStatus']);
        $this->assertEquals(0, $attributes['homePageShowStatus']);

        $relationships = $contents['data']['relationships'];
        $relationshipsCrew = $relationships['crew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals(1, $relationshipsCrew['id']);

        $relationships = $contents['data']['relationships'];
        $relationshipsUserGroup = $relationships['publishUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals(1, $relationshipsUserGroup['id']);
    }
}
