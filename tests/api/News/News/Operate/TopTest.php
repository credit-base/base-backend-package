<?php
namespace Base\News\News\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\News\NewsUtils;
use Base\News\Model\News;
use Base\News\SetDataApiTrait;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IApproveAble;

/**
 * 测试新闻接口置顶
 */
class TopTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, NewsUtils, CrewSetDataApiTrait, UserGroupSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_news');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
    }

    /**
     * 不存在新闻数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_news' => $this->news(),
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup(),
            ]
        );
    }

    public function testViewNews()
    {
        $setNews = $this->applyForm(IApproveAble::OPERATION_TYPE['TOP'])[0];
        $setNews['apply_form_id'] = 1;

        $data = array(
            'data' => array(
                "type"=>"news",
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'news/1/top',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->unAuditedNewsCompareData($setNews, $contents['data']);
    }
}
