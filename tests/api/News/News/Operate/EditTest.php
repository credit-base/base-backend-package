<?php
namespace Base\News\News\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\News\NewsUtils;
use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\SetDataApiTrait;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

/**
 * 测试新闻接口编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, NewsUtils, UserGroupSetDataApiTrait, CrewSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_news');
    }

    /**
     * 不存在新闻数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_news' => $this->news(),
            ]
        );
    }

    public function testViewNews()
    {
        $setNews = $this->applyForm(IApproveAble::OPERATION_TYPE['EDIT'])[0];

        $data = array(
            'data' => array(
                "type"=>"news",
                "attributes"=>array(
                    "title"=>"昨天天气阴",
                    "source"=>"昨天天气阴",
                    "content"=>"昨天天气阴",
                    "cover"=>array('name' => '封面', 'identify' => '封面.jpg'),
                    "attachments"=>array(array('name'=>'附件', 'identify'=>'附件.pdf')),
                    "newsType"=>151,
                    "stick"=>ITopAble::STICK['DISABLED'],
                    "status"=>IEnableAble::STATUS['ENABLED'],
                    "dimension"=>News::DIMENSIONALITY['SOCIOLOGY'],
                    "bannerStatus"=>Banner::BANNER_STATUS['DISABLED'],
                    "bannerImage"=>array('name' => '名称', 'identify' => '地址.jpg'),
                    "homePageShowStatus"=>News::HOME_PAGE_SHOW_STATUS['DISABLED'],
                ),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'news/1',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->unAuditedNewsCompareData($setNews, $contents['data']);
    }
}
