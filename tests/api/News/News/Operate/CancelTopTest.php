<?php
namespace Base\News\News\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Common\Model\ITopAble;

use Base\News\Model\News;
use Base\News\SetDataApiTrait;

/**
 * 测试新闻接口取消置顶
 */
class CancelTopTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_news');
    }

    /**
     * 存在一条新闻数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_news' => $this->news()
            ]
        );
    }

    public function testViewNews()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);

        $data = array(
            'data' => array(
                "type"=>"news",
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );

        $response = $client->request(
            'PATCH',
            'news/2/cancelTop',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(ITopAble::STICK['DISABLED'], $attributes['stick']);
    }
}
