<?php
namespace Base\News\News\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\News\NewsUtils;
use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\SetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IEnableAble;

/**
 * 测试新闻类接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, NewsUtils, CrewSetDataApiTrait, CrewUtils,
    UserGroupSetDataApiTrait, UserGroupUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_news');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条新闻类数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_news' => $this->news()
            ]
        );
    }

    public function testViewNews()
    {
        $setNewsList = $this->news();
        $setCrewList = $this->crew();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'news?filter[publishUserGroup]=1&filter[parentCategory]=0&filter[category]=51&filter[newsType]=151&
            filter[title]=昨天天气晴&filter[status]='.IEnableAble::STATUS['ENABLED'].
            '&filter[dimension]='.News::DIMENSIONALITY['SOCIOLOGY'].'&filter[stick]='.ITopAble::STICK['DISABLED'].
            '&filter[bannerStatus]='.Banner::BANNER_STATUS['DISABLED'].
            '&filter[homePageShowStatus]='.News::HOME_PAGE_SHOW_STATUS['DISABLED'].'&include=publishUserGroup,crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $news) {
            $this->newsCompareData($setNewsList[$key], $news);
        }

        $included = $contents['included'];

        $this->userGroupCompareData($setUserGroupList[0], $included[0]);
        $this->crewCompareData($setCrewList[0], $included[1]);
    }
}
