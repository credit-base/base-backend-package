<?php
namespace Base\News\News\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\News\SetDataApiTrait;
use Base\News\NewsUtils;
use Base\News\Model\News;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

/**
 * 测试新闻类接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, NewsUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    CrewSetDataApiTrait, CrewUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_news');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条新闻类数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_news' => $this->news()
            ]
        );
    }

    public function testViewNews()
    {
        $setNewsList = $this->news();
        $setUserGroupList = $this->userGroup();
        foreach ($setUserGroupList as $setUserGroup) {
            $setUserGroupList[$setUserGroup['user_group_id']] = $setUserGroup;
        }

        $setCrewList = $this->crew();
        foreach ($setCrewList as $setCrew) {
            $setCrewList[$setCrew['crew_id']] = $setCrew;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'news/1,2?include=publishUserGroup,crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $news) {
            $this->newsCompareData($setNewsList[$key], $news);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'userGroups') {
                $this->userGroupCompareData($setUserGroupList[$value['id']], $value);
            }
            if ($value['type'] == 'crews') {
                $this->crewCompareData($setCrewList[$value['id']], $value);
            }
        }
    }
}
