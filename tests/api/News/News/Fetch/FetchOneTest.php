<?php
namespace Base\News\News\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\News\NewsUtils;
use Base\News\Model\News;
use Base\News\SetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试新闻类接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, NewsUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    CrewSetDataApiTrait, CrewUtils, DepartmentSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_news');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
    }

    /**
     * 存在一条新闻类数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_news' => $this->news(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewNews()
    {
        $setNews = $this->news()[0];
        $setUserGroup = $this->userGroup()[0];
        $setCrew = $this->crew()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'news/1?include=publishUserGroup,crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->newsCompareData($setNews, $contents['data']);
        $this->userGroupCompareData($setUserGroup, $contents['included'][0]);
        $this->crewCompareData($setCrew, $contents['included'][1]);
    }
}
