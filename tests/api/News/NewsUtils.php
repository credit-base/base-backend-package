<?php
namespace Base\News;

trait NewsUtils
{
    private function newsCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['news_id'], $responseDataArray['id']);
        $this->assertEquals('news', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['title'], $attributes['title']);
        $this->assertEquals($setDataArray['source'], $attributes['source']);
        $this->assertEquals(json_decode($setDataArray['cover'], true), $attributes['cover']);
        $this->assertEquals(json_decode($setDataArray['attachments'], true), $attributes['attachments']);
        $this->assertEquals($setDataArray['content'], $attributes['content']);
        $this->assertEquals($setDataArray['parent_category'], $attributes['parentCategory']);
        $this->assertEquals($setDataArray['category'], $attributes['category']);
        $this->assertEquals($setDataArray['type'], $attributes['newsType']);
        $this->assertEquals($setDataArray['dimension'], $attributes['dimension']);
        $this->assertEquals(json_decode($setDataArray['banner_image'], true), $attributes['bannerImage']);
        $this->assertEquals($setDataArray['banner_status'], $attributes['bannerStatus']);
        $this->assertEquals($setDataArray['home_page_show_status'], $attributes['homePageShowStatus']);
        $this->assertEquals($setDataArray['stick'], $attributes['stick']);

        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsCrew = $responseDataArray['relationships']['crew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals($setDataArray['crew_id'], $relationshipsCrew['id']);

        $relationshipsUserGroup = $responseDataArray['relationships']['publishUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['publish_usergroup_id'], $relationshipsUserGroup['id']);
    }

    private function unAuditedNewsCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['apply_form_id'], $responseDataArray['id']);
        $this->assertEquals('unAuditedNews', $responseDataArray['type']);

        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($applyInfo['title'], $attributes['title']);
        $this->assertEquals($applyInfo['source'], $attributes['source']);
        $this->assertEquals(json_decode($applyInfo['cover'], true), $attributes['cover']);
        $this->assertEquals(json_decode($applyInfo['attachments'], true), $attributes['attachments']);
        $this->assertEquals(json_decode($applyInfo['banner_image'], true), $attributes['bannerImage']);
        $this->assertEquals($applyInfo['content'], $attributes['content']);
        $this->assertEquals($applyInfo['parent_category'], $attributes['parentCategory']);
        $this->assertEquals($applyInfo['category'], $attributes['category']);
        $this->assertEquals($applyInfo['type'], $attributes['newsType']);
        $this->assertEquals($applyInfo['dimension'], $attributes['dimension']);
        $this->assertEquals($applyInfo['banner_status'], $attributes['bannerStatus']);
        $this->assertEquals($applyInfo['home_page_show_status'], $attributes['homePageShowStatus']);
        $this->assertEquals($applyInfo['stick'], $attributes['stick']);
        $this->assertEquals($applyInfo['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $this->assertEquals($setDataArray['apply_info_category'], $attributes['applyInfoCategory']);
        $this->assertEquals($setDataArray['apply_info_type'], $attributes['applyInfoType']);
        $this->assertEquals($setDataArray['apply_status'], $attributes['applyStatus']);
        $this->assertEquals($setDataArray['reject_reason'], $attributes['rejectReason']);
        $this->assertEquals($setDataArray['operation_type'], $attributes['operationType']);
        $this->assertEquals($applyInfo['news_id'], $attributes['newsId']);

        $relationshipsRelation = $responseDataArray['relationships']['relation']['data'];
        $this->assertEquals('crews', $relationshipsRelation['type']);
        $this->assertEquals($setDataArray['relation_id'], $relationshipsRelation['id']);

        $relationshipsUserGroup = $responseDataArray['relationships']['publishUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($applyInfo['publish_usergroup_id'], $relationshipsUserGroup['id']);

        $relationshipsCrew = $responseDataArray['relationships']['crew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals($applyInfo['crew_id'], $relationshipsCrew['id']);
        
        $relationshipsApplyCrew = $responseDataArray['relationships']['applyCrew']['data'];
        $this->assertEquals('crews', $relationshipsApplyCrew['type']);
        $this->assertEquals($setDataArray['apply_crew_id'], $relationshipsApplyCrew['id']);

        $relationshipsApplyUserGroup = $responseDataArray['relationships']['applyUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsApplyUserGroup['type']);
        $this->assertEquals($setDataArray['apply_usergroup_id'], $relationshipsApplyUserGroup['id']);
    }
}
