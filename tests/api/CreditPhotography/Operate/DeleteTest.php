<?php
namespace Base\CreditPhotography\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\CreditPhotography\SetDataApiTrait;
use Base\CreditPhotography\Model\CreditPhotography;

/**
 * 测试信用随手拍接口删除
 */
class DeleteTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_credit_photography');
    }

    /**
     * 存在一条信用随手拍数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_credit_photography' => $this->creditPhotography()
            ]
        );
    }

    public function testViewCreditPhotography()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'DELETE',
            'creditPhotography/2/delete',
            ['headers' => Core::$container->get('client.headers')]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(CreditPhotography::STATUS['DELETED'], $attributes['status']);
    }
}
