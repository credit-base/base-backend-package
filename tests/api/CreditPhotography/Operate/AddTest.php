<?php
namespace Base\CreditPhotography\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\CreditPhotography\SetDataApiTrait;
use Base\CreditPhotography\CreditPhotographyUtils;
use Base\CreditPhotography\Model\CreditPhotography;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;

/**
 * 测试信用随手拍接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CreditPhotographyUtils, CrewSetDataApiTrait, MemberSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_credit_photography');
    }

    /**
     * 不存在信用随手拍数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_member' => $this->member()
            ]
        );
    }

    public function testViewCreditPhotography()
    {
        $setCreditPhotography = $this->creditPhotography()[0];

        $data = array(
            'data' => array(
                "type"=>"creditPhotography",
                "attributes"=>array(
                    "description"=>"信用随手拍描述",
                    "attachments"=>array(array('name'=>'附件名称', 'identify'=>'附件地址.jpg'))
                ),
                "relationships"=>array(
                    "member"=>array(
                        "data"=>array(
                            array("type"=>"members","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'creditPhotography',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->creditPhotographyCompareData($setCreditPhotography, $contents['data']);
    }
}
