<?php
namespace Base\CreditPhotography\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Common\Model\IApproveAble;

use Base\CreditPhotography\SetDataApiTrait;
use Base\CreditPhotography\Model\CreditPhotography;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

/**
 * 测试信用随手拍接口审核驳回
 */
class RejectTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CrewSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_credit_photography');
    }

    /**
     * 存在一条信用随手拍数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_credit_photography' => $this->creditPhotography()
            ]
        );
    }

    public function testViewCreditPhotography()
    {
        $data = array(
            'data' => array(
                "type"=>"creditPhotography",
                "attributes"=>array(
                    "rejectReason"=>"审核驳回原因"
                ),
                "relationships"=>array(
                    "applyCrew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'creditPhotography/2/reject',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IApproveAble::APPLY_STATUS['REJECT'], $attributes['applyStatus']);
        $this->assertEquals('审核驳回原因', $attributes['rejectReason']);

        $relationships = $contents['data']['relationships'];
        $relationshipsCrew = $relationships['applyCrew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals(2, $relationshipsCrew['id']);
    }
}
