<?php
namespace Base\CreditPhotography;

use Marmot\Core;

use Base\Common\Model\IApproveAble;
use Base\CreditPhotography\Model\CreditPhotography;

trait SetDataApiTrait
{
    protected function creditPhotography() : array
    {
        return
        [
            [
                'credit_photography_id' => 1,
                'description' => '信用随手拍描述',
                'attachments' => json_encode(array(array('name'=>'附件名称', 'identify'=>'附件地址.jpg'))),
                'member_id' => 1,
                'real_name' => '张科',
                'status' => CreditPhotography::STATUS['NORMAL'],
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'apply_crew_id' => 0,
                'reject_reason' => '',
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'credit_photography_id' => 2,
                'description' => '信用随手拍',
                'attachments' => json_encode(array(array('name'=>'附件名称', 'identify'=>'附件地址.jpg'))),
                'member_id' => 2,
                'real_name' => '李煜',
                'status' => CreditPhotography::STATUS['NORMAL'],
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'apply_crew_id' => 2,
                'reject_reason' => '',
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
