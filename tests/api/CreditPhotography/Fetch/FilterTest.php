<?php
namespace Base\CreditPhotography\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\CreditPhotography\SetDataApiTrait;
use Base\CreditPhotography\CreditPhotographyUtils;
use Base\CreditPhotography\Model\CreditPhotography;

use Base\Common\Model\IApproveAble;

use Base\Department\DepartmentUtils;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

use Base\Member\MemberUtils;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Common\Model\IEnableAble;

/**
 * 测试信用刊物接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CreditPhotographyUtils, DepartmentSetDataApiTrait, DepartmentUtils,
    UserGroupSetDataApiTrait, UserGroupUtils, MemberSetDataApiTrait, MemberUtils, CrewSetDataApiTrait, CrewUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
        $this->clear('pcore_credit_photography');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
    }

    /**
     * 存在多条信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_member' => $this->member(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department(),
                'pcore_credit_photography' => $this->creditPhotography()
            ]
        );
    }

    public function testViewCreditPhotography()
    {
        $setCreditPhotographyList = $this->creditPhotography();
        foreach ($setCreditPhotographyList as $setCreditPhotography) {
            $setCreditPhotographyList[$setCreditPhotography['credit_photography_id']] = $setCreditPhotography;
        }

        $setCrewList = $this->crew();
        $setMemberList = $this->member();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'creditPhotography?filter[member]=2&filter[realName]=李煜&
            filter[applyStatus]='.IApproveAble::APPLY_STATUS['PENDING'].'&
            filter[status]='.CreditPhotography::STATUS['NORMAL'].'&include=member,applyCrew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $creditPhotography) {
            $this->creditPhotographyCompareData(
                $setCreditPhotographyList[$creditPhotography['id']],
                $creditPhotography
            );
        }

        $included = $contents['included'];

        $this->memberCompareData($setMemberList[1], $included[0]);
        $this->crewCompareData($setCrewList[1], $included[1]);
    }
}
