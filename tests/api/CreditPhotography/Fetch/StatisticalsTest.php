<?php
namespace Base\CreditPhotography\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\CreditPhotography\SetDataApiTrait;
use Base\CreditPhotography\CreditPhotographyUtils;
use Base\CreditPhotography\Model\CreditPhotography;

/**
 * 测试信用随手拍不同审核状态的数量统计
 */
class StatisticalsTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CreditPhotographyUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_credit_photography');
    }

    /**
     * 存在多条企业数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_credit_photography' => $this->creditPhotography()
            ]
        );
    }

    public function testViewCreditPhotography()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'statisticals/staticsCreditPhotography?filter[memberId]=1',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $this->assertEquals([], $contents['meta']);

        $data = $contents['data'];

        $this->assertEquals(0, $data['id']);
        $this->assertEquals('statisticals', $data['type']);

        $attributes = $data['attributes'];

        $this->assertEquals(1, $attributes['result']['pendingTotal']);
        $this->assertEquals(0, $attributes['result']['approveTotal']);
        $this->assertEquals(0, $attributes['result']['rejectTotal']);
    }
}
