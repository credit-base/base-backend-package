<?php
namespace Base\CreditPhotography\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\CreditPhotography\SetDataApiTrait;
use Base\CreditPhotography\CreditPhotographyUtils;
use Base\CreditPhotography\Model\CreditPhotography;

use Base\Member\MemberUtils;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Department\DepartmentUtils;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试信用刊物接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CreditPhotographyUtils, MemberSetDataApiTrait, MemberUtils,
    CrewSetDataApiTrait, CrewUtils, UserGroupSetDataApiTrait,
    UserGroupUtils, DepartmentSetDataApiTrait, DepartmentUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_credit_photography');
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
    }

    /**
     * 存在一条信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_credit_photography' => $this->creditPhotography(),
                'pcore_member' => $this->member(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department(),
            ]
        );
    }

    public function testViewCreditPhotography()
    {
        $setCreditPhotography = $this->creditPhotography()[1];
        $setMember = $this->member()[1];
        $setCrew = $this->crew()[1];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'creditPhotography/2?include=member,applyCrew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->creditPhotographyCompareData($setCreditPhotography, $contents['data']);
        $this->memberCompareData($setMember, $contents['included'][0]);
        $this->crewCompareData($setCrew, $contents['included'][1]);
    }
}
