<?php
namespace Base\CreditPhotography\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\CreditPhotography\SetDataApiTrait;
use Base\CreditPhotography\CreditPhotographyUtils;
use Base\CreditPhotography\Model\CreditPhotography;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Department\DepartmentUtils;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

use Base\Member\MemberUtils;
use Base\Member\SetDataApiTrait as MemberSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

/**
 * 测试信用刊物接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CreditPhotographyUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    DepartmentSetDataApiTrait, DepartmentUtils, MemberSetDataApiTrait, MemberUtils, CrewSetDataApiTrait, CrewUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_credit_photography');
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_member');
    }

    /**
     * 存在多条信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_member' => $this->member(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department(),
                'pcore_credit_photography' => $this->creditPhotography()
            ]
        );
    }

    public function testViewCreditPhotography()
    {
        $setCreditPhotographyList = $this->creditPhotography();
        $setCrewList = $this->crew();
        foreach ($setCrewList as $setCrew) {
            $setCrewList[$setCrew['crew_id']] = $setCrew;
        }

        $setMemberList = $this->member();
        foreach ($setMemberList as $setMember) {
            $setMemberList[$setMember['member_id']] = $setMember;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'creditPhotography/1,2?include=applyCrew,member',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $creditPhotography) {
            $this->creditPhotographyCompareData($setCreditPhotographyList[$key], $creditPhotography);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['id'] != 0) {
                if ($value['type'] == 'crews') {
                    $this->crewCompareData($setCrewList[$value['id']], $value);
                }
            }
            if ($value['type'] == 'members') {
                $this->memberCompareData($setMemberList[$value['id']], $value);
            }
        }
    }
}
