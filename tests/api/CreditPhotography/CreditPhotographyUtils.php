<?php
namespace Base\CreditPhotography;

trait CreditPhotographyUtils
{
    private function creditPhotographyCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['credit_photography_id'], $responseDataArray['id']);
        $this->assertEquals('creditPhotography', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['description'], $attributes['description']);
        $this->assertEquals(json_decode($setDataArray['attachments'], true), $attributes['attachments']);
        $this->assertEquals($setDataArray['apply_status'], $attributes['applyStatus']);
        $this->assertEquals($setDataArray['reject_reason'], $attributes['rejectReason']);
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsMember = $responseDataArray['relationships']['member']['data'];
        $this->assertEquals('members', $relationshipsMember['type']);
        $this->assertEquals($setDataArray['member_id'], $relationshipsMember['id']);

        $relationshipsCrew = $responseDataArray['relationships']['applyCrew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals($setDataArray['apply_crew_id'], $relationshipsCrew['id']);
    }
}
