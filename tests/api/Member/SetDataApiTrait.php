<?php
namespace Base\Member;

use Marmot\Core;

use Base\Member\Model\Member;

use Base\Common\Model\IEnableAble;

trait SetDataApiTrait
{
    protected function member() : array
    {
        return
        [
            [
                'member_id' => 1,
                'user_name' => '18800000000',
                'real_name' => '张科',
                'cellphone' => '18800000000',
                'email' => '997934308@qq.com',
                'cardid' => '610424198810202422',
                'gender' => Member::GENDER['NULL'],
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'contact_address' => '雁塔区长延堡街道',
                'security_question' => Member::SECURITY_QUESTION['QUESTION_FIVE'],
                'security_answer' => md5(md5('黑色')),
                'status' => IEnableAble::STATUS['ENABLED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'member_id' => 2,
                'user_name' => '18800000001',
                'real_name' => '李煜',
                'cellphone' => '18800000001',
                'email' => '997934307@qq.com',
                'cardid' => '610424198810212422',
                'gender' => Member::GENDER['MALE'],
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'contact_address' => '雁塔区长延堡街道',
                'security_question' => Member::SECURITY_QUESTION['QUESTION_TWO'],
                'security_answer' => md5(md5('夏目友人帐')),
                'status' => IEnableAble::STATUS['DISABLED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
