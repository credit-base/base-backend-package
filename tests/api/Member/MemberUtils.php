<?php
namespace Base\Member;

trait MemberUtils
{
    private function memberCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['member_id'], $responseDataArray['id']);
        $this->assertEquals('members', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['user_name'], $attributes['userName']);
        $this->assertEquals($setDataArray['real_name'], $attributes['realName']);
        $this->assertEquals($setDataArray['cellphone'], $attributes['cellphone']);
        $this->assertEquals($setDataArray['email'], $attributes['email']);
        $this->assertEquals($setDataArray['cardid'], $attributes['cardId']);
        $this->assertEquals($setDataArray['contact_address'], $attributes['contactAddress']);
        $this->assertEquals($setDataArray['gender'], $attributes['gender']);
        $this->assertEquals($setDataArray['security_question'], $attributes['securityQuestion']);
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);
    }
}
