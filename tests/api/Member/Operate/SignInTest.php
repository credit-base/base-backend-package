<?php
namespace Base\Member\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Member\MemberUtils;
use Base\Member\Model\Member;
use Base\Member\SetDataApiTrait;

/**
 * 测试前台用户接口登录
 */
class SignInTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, MemberUtils;

    private $member;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->member);
        $this->clear('pcore_member');
    }

    /**
     * 存在前台用户数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_member' => $this->member()
            ]
        );
    }

    public function testViewMember()
    {
        $setMember = $this->member()[0];

        $data = array(
            "data"=>array(
                "type"=>"members",
                "attributes"=>array(
                    "userName"=>'18800000000',
                    "password"=>'Admin123$'
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'members/signIn',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->memberCompareData($setMember, $contents['data']);
    }
}
