<?php
namespace Base\Member\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Member\MemberUtils;
use Base\Member\Model\Member;
use Base\Member\SetDataApiTrait;

/**
 * 测试前台用户接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, MemberUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * 不存在前台用户数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            []
        );
    }

    public function testViewMember()
    {
        $setMember = $this->member()[0];

        $data = array(
            "data"=>array(
                "type"=>"members",
                "attributes"=>array(
                    "userName"=>"18800000000",
                    "realName"=>"张科",
                    "cardId"=>"610424198810202422",
                    "cellphone"=>"18800000000",
                    "email"=>"997934308@qq.com",
                    "contactAddress"=>"雁塔区长延堡街道",
                    "securityQuestion"=>Member::SECURITY_QUESTION['QUESTION_FIVE'],
                    "securityAnswer"=>"黑色",
                    "password"=>"Admin123$"
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'members',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );
        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->memberCompareData($setMember, $contents['data']);
    }
}
