<?php
namespace Base\Member\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Member\MemberUtils;
use Base\Member\Model\Member;
use Base\Member\SetDataApiTrait;

/**
 * 测试前台用户接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, MemberUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * 存在一条前台用户数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_member' => $this->member()
            ]
        );
    }

    public function testViewMember()
    {
        $setMember = $this->member()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'members/1',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->memberCompareData($setMember, $contents['data']);
    }
}
