<?php
namespace Base\Member\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Member\MemberUtils;
use Base\Member\Model\Member;
use Base\Member\SetDataApiTrait;

use Base\Common\Model\IEnableAble;

/**
 * 测试前台用户接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, MemberUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_member');
    }

    /**
     * 存在多条前台用户数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_member' => $this->member()
            ]
        );
    }

    public function testViewMember()
    {
        $setMemberList = $this->member();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'members?filter[cellphone]=18800000000&filter[realName]=张科
            &filter[status]='.IEnableAble::STATUS['ENABLED'],
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];

        foreach ($data as $key => $member) {
            $this->memberCompareData($setMemberList[$key], $member);
        }
    }
}
