<?php
namespace Base\Department;

trait DepartmentUtils
{
    private function departmentCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['department_id'], $responseDataArray['id']);
        $this->assertEquals('departments', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['name'], $attributes['name']);
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsUserGroup = $responseDataArray['relationships']['userGroup']['data'];

        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['user_group_id'], $relationshipsUserGroup['id']);
    }
}
