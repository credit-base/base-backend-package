<?php
namespace Base\Department;

use Marmot\Core;

use Base\Department\Model\Department;

trait SetDataApiTrait
{
    protected function department() : array
    {
        return
        [
            [
                'department_id' => 1,
                'name' => '技术部',
                'user_group_id' => 1,
                'status' => Department::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ],
            [
                'department_id' => 2,
                'name' => '技术部',
                'user_group_id' => 2,
                'status' => Department::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ],
        ];
    }
}
