<?php
namespace Base\Department\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Department\SetDataApiTrait;
use Base\Department\DepartmentUtils;
use Base\Department\Model\Department;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

/**
 * 测试科室接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, DepartmentUtils, UserGroupSetDataApiTrait, UserGroupUtils;

    private $department;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->department);
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在一条科室数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewDepartment()
    {
        $setDepartment = $this->department()[0];
        $setUserGroup = $this->userGroup()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'departments/1?include=userGroup',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->departmentCompareData($setDepartment, $contents['data']);
        $this->userGroupCompareData($setUserGroup, $contents['included'][0]);
    }
}
