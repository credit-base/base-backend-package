<?php
namespace Base\Department\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Department\SetDataApiTrait;
use Base\Department\Model\Department;

/**
 * 测试科室接口编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
    }

    /**
     * 存在一条科室数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewDepartment()
    {
        $data = array(
            "data"=>array(
                "type"=>"departments",
                "attributes"=>array(
                    "name"=>'财金科'
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'departments/1',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals('财金科', $attributes['name']);
    }
}
