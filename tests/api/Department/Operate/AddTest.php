<?php
namespace Base\Department\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Department\SetDataApiTrait;
use Base\Department\DepartmentUtils;
use Base\Department\Model\Department;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

/**
 * 测试科室接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, DepartmentUtils, UserGroupSetDataApiTrait;

    private $department;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->department);
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
    }

    /**
     * 不存在科室数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup()
            ]
        );
    }

    public function testViewDepartment()
    {
        $setDepartment = $this->department()[0];

        $data = array(
            "data"=>array(
                "type"=>"departments",
                "attributes"=>array(
                    "name"=>'技术部'
                ),
                "relationships"=>array(
                    "userGroup"=>array(
                        "data"=>array(
                            array("type"=>"userGroups","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'departments',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->departmentCompareData($setDepartment, $contents['data']);
    }
}
