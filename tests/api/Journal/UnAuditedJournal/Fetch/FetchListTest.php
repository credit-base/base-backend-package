<?php
namespace Base\Journal\UnAuditedJournal\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Journal\SetDataApiTrait;
use Base\Journal\JournalUtils;
use Base\Journal\Model\UnAuditedJournal;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

/**
 * 测试信用刊物审核接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, JournalUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    CrewSetDataApiTrait, CrewUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条信用刊物审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_apply_form' => $this->applyForm()
            ]
        );
    }

    public function testViewUnAuditedJournal()
    {
        $setUnAuditedJournalList = $this->applyForm();
        $setUserGroupList = $this->userGroup();
        foreach ($setUserGroupList as $setUserGroup) {
            $setUserGroupList[$setUserGroup['user_group_id']] = $setUserGroup;
        }

        $setCrewList = $this->crew();
        foreach ($setCrewList as $setCrew) {
            $setCrewList[$setCrew['crew_id']] = $setCrew;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedJournals/1,2?include=crew,publishUserGroup,applyUserGroup,applyCrew,relation',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $unAuditedJournal) {
            $this->unAuditedJournalCompareData($setUnAuditedJournalList[$key], $unAuditedJournal);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'userGroups') {
                $this->userGroupCompareData($setUserGroupList[$value['id']], $value);
            }
            if ($value['id'] != 0 && $value['type'] == 'crews') {
                $this->crewCompareData($setCrewList[$value['id']], $value);
            }
        }
    }
}
