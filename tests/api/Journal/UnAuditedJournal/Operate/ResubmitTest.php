<?php
namespace Base\Journal\UnAuditedJournal\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Journal\SetDataApiTrait;
use Base\Journal\Model\UnAuditedJournal;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

/**
 * 测试信用刊物接口重新编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, UserGroupSetDataApiTrait, CrewSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form');
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
    }

    /**
     * 存在一条信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_apply_form' => $this->applyForm(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew()
            ]
        );
    }

    public function testViewUnAuditedJournal()
    {
        $data = array(
            'data' => array(
                "type"=>"unAuditedJournals",
                "attributes"=>array(
                    "title"=>"重新编辑标题",
                    "source"=>"来源",
                    "cover"=>array('name' => '重新编辑', 'identify' => '重新编辑.jpg'),
                    "attachment"=>array('name' => '重新编辑', 'identify' => '重新编辑.pdf'),
                    "authImages"=>array(array('name' => '重新编辑', 'identify' => '1.jpg')),
                    "description"=>"简介",
                    "year"=>2020,
                    "status"=>IEnableAble::STATUS['DISABLED']
                ),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'unAuditedJournals/2/resubmit',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $attributes['applyStatus']);
        $this->assertEquals('重新编辑标题', $attributes['title']);
        $this->assertEquals('来源', $attributes['source']);
        $this->assertEquals(array('name' => '重新编辑', 'identify' => '重新编辑.jpg'), $attributes['cover']);
        $this->assertEquals(array('name' => '重新编辑', 'identify' => '重新编辑.pdf'), $attributes['attachment']);
        $this->assertEquals(array(array('name' => '重新编辑', 'identify' => '1.jpg')), $attributes['authImages']);
        $this->assertEquals('简介', $attributes['description']);
        $this->assertEquals('2020', $attributes['year']);
        $this->assertEquals(IEnableAble::STATUS['DISABLED'], $attributes['status']);

        $relationships = $contents['data']['relationships'];
        $relationshipsUserGroup = $relationships['publishUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals(1, $relationshipsUserGroup['id']);

        $relationshipsCrew = $relationships['crew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals(1, $relationshipsCrew['id']);
    }
}
