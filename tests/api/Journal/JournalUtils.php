<?php
namespace Base\Journal;

trait JournalUtils
{
    private function journalCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['journal_id'], $responseDataArray['id']);
        $this->assertEquals('journals', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['title'], $attributes['title']);
        $this->assertEquals($setDataArray['source'], $attributes['source']);
        $this->assertEquals(json_decode($setDataArray['cover'], true), $attributes['cover']);
        $this->assertEquals(json_decode($setDataArray['attachment'], true), $attributes['attachment']);
        $this->assertEquals(json_decode($setDataArray['auth_images'], true), $attributes['authImages']);
        $this->assertEquals($setDataArray['description'], $attributes['description']);
        $this->assertEquals($setDataArray['year'], $attributes['year']);

        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsUserGroup = $responseDataArray['relationships']['publishUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['publish_usergroup_id'], $relationshipsUserGroup['id']);

        $relationshipsCrew = $responseDataArray['relationships']['crew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals($setDataArray['crew_id'], $relationshipsCrew['id']);
    }

    private function unAuditedJournalCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['apply_form_id'], $responseDataArray['id']);
        $this->assertEquals('unAuditedJournals', $responseDataArray['type']);

        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($applyInfo['title'], $attributes['title']);
        $this->assertEquals($applyInfo['source'], $attributes['source']);
        $this->assertEquals(json_decode($applyInfo['cover'], true), $attributes['cover']);
        $this->assertEquals(json_decode($applyInfo['attachment'], true), $attributes['attachment']);
        $this->assertEquals(json_decode($applyInfo['auth_images'], true), $attributes['authImages']);
        $this->assertEquals($applyInfo['description'], $attributes['description']);
        $this->assertEquals($applyInfo['year'], $attributes['year']);
        $this->assertEquals($applyInfo['status'], $attributes['status']);

        $this->assertEquals($setDataArray['apply_info_category'], $attributes['applyInfoCategory']);
        $this->assertEquals($setDataArray['apply_info_type'], $attributes['applyInfoType']);
        $this->assertEquals($setDataArray['apply_status'], $attributes['applyStatus']);
        $this->assertEquals($setDataArray['reject_reason'], $attributes['rejectReason']);
        $this->assertEquals($setDataArray['operation_type'], $attributes['operationType']);
        $this->assertEquals($applyInfo['journal_id'], $attributes['journalId']);

        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsRelation = $responseDataArray['relationships']['relation']['data'];
        $this->assertEquals('crews', $relationshipsRelation['type']);
        $this->assertEquals($setDataArray['relation_id'], $relationshipsRelation['id']);

        $relationshipsApplyCrew = $responseDataArray['relationships']['applyCrew']['data'];
        $this->assertEquals('crews', $relationshipsApplyCrew['type']);
        $this->assertEquals($setDataArray['apply_crew_id'], $relationshipsApplyCrew['id']);

        $relationshipsApplyUserGroup = $responseDataArray['relationships']['applyUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsApplyUserGroup['type']);
        $this->assertEquals($setDataArray['apply_usergroup_id'], $relationshipsApplyUserGroup['id']);

        $relationshipsUserGroup = $responseDataArray['relationships']['publishUserGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($applyInfo['publish_usergroup_id'], $relationshipsUserGroup['id']);

        $relationshipsCrew = $responseDataArray['relationships']['crew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals($applyInfo['crew_id'], $relationshipsCrew['id']);
    }
}
