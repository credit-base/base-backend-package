<?php
namespace Base\Journal;

use Marmot\Core;

use Base\Journal\Model\Journal;
use Base\Journal\Model\UnAuditedJournal;

use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

trait SetDataApiTrait
{
    protected function journal($status = IEnableAble::STATUS['ENABLED']) : array
    {
        return
        [
            [
                'journal_id' => 1,
                'title' => '信用刊物第一版',
                'source' => '信用刊物来源',
                'description' => '信用刊物简介',
                'cover' => json_encode(array('name'=>'封面名称', 'identify'=>'封面地址.jpg')),
                'attachment' => json_encode(array('name'=>'附件名称', 'identify'=>'附件地址.pdf')),
                'auth_images' => json_encode(array(array('name'=>'授权图片名称', 'identify'=>'授权图片地址.jpg'))),
                'year' => '2021',
                'crew_id' => 1,
                'publish_usergroup_id' => 1,
                'status' => $status,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'journal_id' => 2,
                'title' => '信用刊物第二版',
                'source' => '信用刊物来源',
                'description' => '信用刊物简介',
                'cover' => json_encode(array('name'=>'封面名称', 'identify'=>'封面地址.jpg')),
                'attachment' => json_encode(array('name'=>'附件名称', 'identify'=>'附件地址.pdf')),
                'auth_images' => json_encode(array(array('name'=>'授权图片名称', 'identify'=>'授权图片地址.jpg'))),
                'year' => '2021',
                'crew_id' => 2,
                'publish_usergroup_id' => 2,
                'status' => IEnableAble::STATUS['DISABLED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'journal_id' => 3,
                'title' => '信用刊物第三版',
                'source' => '信用刊物来源',
                'description' => '信用刊物简介',
                'cover' => json_encode(array('name'=>'封面名称', 'identify'=>'封面地址.jpg')),
                'attachment' => json_encode(array('name'=>'附件名称', 'identify'=>'附件地址.pdf')),
                'auth_images' => json_encode(array(array('name'=>'授权图片名称', 'identify'=>'授权图片地址.jpg'))),
                'year' => '2021',
                'crew_id' => 1,
                'publish_usergroup_id' => 1,
                'status' => IEnableAble::STATUS['DISABLED'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyForm(
        $operationType = IApproveAble::OPERATION_TYPE['ADD']
    ) : array {
        $journal = $this->journal()[0];
        if ($operationType == IApproveAble::OPERATION_TYPE['ADD']) {
            $journal['journal_id'] = 0;
        }
        $applyInfoOne = base64_encode(gzcompress(serialize($journal)));
        $applyInfoTwo = base64_encode(gzcompress(serialize($this->journal()[1])));
        $applyInfoThree = base64_encode(gzcompress(serialize($this->journal()[2])));
        
        return
        [
            [
                'apply_form_id' => 1,
                'title' => '信用刊物第一版',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 0,
                'operation_type' => $operationType,
                'apply_info' => $applyInfoOne,
                'reject_reason' => '',
                'apply_info_category' => UnAuditedJournal::APPLY_JOURNAL_CATEGORY,
                'apply_info_type' => UnAuditedJournal::APPLY_JOURNAL_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'apply_form_id' => 2,
                'title' => '信用刊物第二版',
                'relation_id' => 2,
                'apply_usergroup_id' => 2,
                'apply_crew_id' => 2,
                'operation_type' => $operationType,
                'apply_info' => $applyInfoTwo,
                'reject_reason' => '驳回原因',
                'apply_info_category' => UnAuditedJournal::APPLY_JOURNAL_CATEGORY,
                'apply_info_type' => UnAuditedJournal::APPLY_JOURNAL_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['REJECT'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'apply_form_id' => 3,
                'title' => '信用刊物第三版',
                'relation_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 0,
                'operation_type' => $operationType,
                'apply_info' => $applyInfoThree,
                'reject_reason' => '',
                'apply_info_category' => UnAuditedJournal::APPLY_JOURNAL_CATEGORY,
                'apply_info_type' => UnAuditedJournal::APPLY_JOURNAL_TYPE,
                'apply_status' => IApproveAble::APPLY_STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
