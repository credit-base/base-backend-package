<?php
namespace Base\Journal\Journal\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Journal\JournalUtils;
use Base\Journal\Model\Journal;
use Base\Journal\SetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试信用刊物接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, JournalUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    CrewSetDataApiTrait, CrewUtils, DepartmentSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_journal');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
    }

    /**
     * 存在一条信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_journal' => $this->journal(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewJournal()
    {
        $setJournal = $this->journal()[0];
        $setUserGroup = $this->userGroup()[0];
        $setCrew = $this->crew()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'journals/1?include=publishUserGroup,crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->journalCompareData($setJournal, $contents['data']);
        $this->userGroupCompareData($setUserGroup, $contents['included'][0]);
        $this->crewCompareData($setCrew, $contents['included'][1]);
    }
}
