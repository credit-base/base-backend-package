<?php
namespace Base\Journal\Journal\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Journal\JournalUtils;
use Base\Journal\Model\Journal;
use Base\Journal\SetDataApiTrait;

use Base\Crew\CrewUtils;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Common\Model\IEnableAble;

/**
 * 测试信用刊物接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, JournalUtils, CrewSetDataApiTrait, CrewUtils,
    UserGroupSetDataApiTrait, UserGroupUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_journal');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_journal' => $this->journal()
            ]
        );
    }

    public function testViewJournal()
    {
        $setJournalList = $this->journal();
        $setCrewList = $this->crew();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'journals?filter[publishUserGroup]=1&filter[title]=信用刊物第一版&filter[year]=2021&
            filter[status]='.IEnableAble::STATUS['ENABLED'].'&include=publishUserGroup,crew',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $journal) {
            $this->journalCompareData($setJournalList[$key], $journal);
        }

        $included = $contents['included'];

        $this->userGroupCompareData($setUserGroupList[0], $included[0]);
        $this->crewCompareData($setCrewList[0], $included[1]);
    }
}
