<?php
namespace Base\Journal\Journal\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Journal\JournalUtils;
use Base\Journal\Model\Journal;
use Base\Journal\SetDataApiTrait;

use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Common\Model\IEnableAble;
use Base\Common\Model\IApproveAble;

/**
 * 测试信用刊物接口启用
 */
class EnableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, JournalUtils, CrewSetDataApiTrait, UserGroupSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_journal');
        $this->clear('pcore_user_group');
    }

    /**
     * 不存在信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_journal' => $this->journal(),
                'pcore_user_group' => $this->userGroup(),
            ]
        );
    }

    public function testViewJournal()
    {
        $setJournal = $this->applyForm(IApproveAble::OPERATION_TYPE['ENABLE'])[2];
        $setJournal['apply_form_id'] = 1;

        $data = array(
            'data' => array(
                "type"=>"journals",
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'journals/3/enable',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->unAuditedJournalCompareData($setJournal, $contents['data']);
    }
}
