<?php
namespace Base\Journal\Journal\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Common\Model\IEnableAble;

use Base\Journal\Model\Journal;
use Base\Journal\SetDataApiTrait;

/**
 * 测试信用刊物接口禁用
 */
class DisableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_journal');
    }

    /**
     * 存在一条信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_journal' => $this->journal()
            ]
        );
    }

    public function testViewJournal()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);

        $data = array(
            'data' => array(
                "type"=>"journals",
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );

        $response = $client->request(
            'PATCH',
            'journals/1/disable',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IEnableAble::STATUS['DISABLED'], $attributes['status']);
    }
}
