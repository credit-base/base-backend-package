<?php
namespace Base\Journal\Journal\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Journal\JournalUtils;
use Base\Journal\Model\Journal;
use Base\Journal\SetDataApiTrait;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

/**
 * 测试信用刊物接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, JournalUtils, UserGroupSetDataApiTrait, CrewSetDataApiTrait;

    private $journal;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->journal);
        $this->clear('pcore_user_group');
        $this->clear('pcore_crew');
        $this->clear('pcore_apply_form');
    }

    /**
     * 不存在信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew()
            ]
        );
    }

    public function testViewJournal()
    {
        $setJournal = $this->applyForm()[0];

        $data = array(
            'data' => array(
                "type"=>"journals",
                "attributes"=>array(
                    "title"=>"信用刊物第一版",
                    "source"=>"信用刊物来源",
                    "description"=>"信用刊物简介",
                    "cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
                    "attachment"=>array('name' => '附件名称', 'identify' => '附件地址.pdf'),
                    "authImages"=>array(array('name'=>'授权图片名称', 'identify'=>'授权图片地址.jpg')),
                    "year"=>2021,
                    "status"=>0
                ),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'journals',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->unAuditedJournalCompareData($setJournal, $contents['data']);
    }
}
