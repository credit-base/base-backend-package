<?php
namespace Base\Journal\Journal\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Journal\JournalUtils;
use Base\Journal\Model\Journal;
use Base\Journal\SetDataApiTrait;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;
use Base\Crew\SetDataApiTrait as CrewSetDataApiTrait;

use Base\Common\Model\IApproveAble;

/**
 * 测试信用刊物接口编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, JournalUtils, UserGroupSetDataApiTrait, CrewSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_apply_form');
        $this->clear('pcore_journal');
    }

    /**
     * 不存在信用刊物数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_journal' => $this->journal(),
            ]
        );
    }

    public function testViewJournal()
    {
        $setJournal = $this->applyForm(IApproveAble::OPERATION_TYPE['EDIT'])[0];

        $data = array(
            'data' => array(
                "type"=>"journals",
                "attributes"=>array(
                    "title"=>"信用刊物第一版",
                    "source"=>"信用刊物来源",
                    "cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
                    "attachment"=>array('name' => '附件名称', 'identify' => '附件地址.pdf'),
                    "authImages"=>array(array('name'=>'授权图片名称', 'identify'=>'授权图片地址.jpg')),
                    "description"=>"信用刊物简介",
                    "year"=>2021,
                    "status"=>0
                ),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'journals/1',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->unAuditedJournalCompareData($setJournal, $contents['data']);
    }
}
