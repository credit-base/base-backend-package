<?php
namespace Base\Crew\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Crew\Model\Crew;
use Base\Crew\SetDataApiTrait;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试员工接口编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, UserGroupSetDataApiTrait, DepartmentSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
    }

    /**
     * 存在一条员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewCrew()
    {
        $data = array(
            "data"=>array(
                "type"=>"crews",
                "attributes"=>array(
                    "realName"=>'张科科',
                    "cardId"=>'610424198810202433',
                    "category"=>Crew::CATEGORY['OPERATOR'],
                    "purview"=>array(1,2,3,4,5)
                ),
                "relationships"=>array(
                    "userGroup"=>array(
                        "data"=>array(
                            array("type"=>"userGroups","id"=>2)
                        )
                    ),
                    "department"=>array(
                        "data"=>array(
                            array("type"=>"departments","id"=>2)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'crews/1',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals('张科科', $attributes['realName']);
        $this->assertEquals('610424198810202433', $attributes['cardId']);
        $this->assertEquals(Crew::CATEGORY['OPERATOR'], $attributes['category']);
        $this->assertEquals(array(1,2,3,4,5), $attributes['purview']);

        $relationships = $contents['data']['relationships'];
        $relationshipsUserGroup = $relationships['userGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals(2, $relationshipsUserGroup['id']);

        $relationshipsDepartment = $relationships['department']['data'];
        $this->assertEquals('departments', $relationshipsDepartment['type']);
        $this->assertEquals(2, $relationshipsDepartment['id']);
    }
}
