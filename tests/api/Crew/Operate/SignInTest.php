<?php
namespace Base\Crew\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Crew\SetDataApiTrait;
use Base\Crew\CrewUtils;
use Base\Crew\Model\Crew;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试员工接口登录
 */
class SignInTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CrewUtils, UserGroupSetDataApiTrait, DepartmentSetDataApiTrait;

    private $crew;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->crew);
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
    }

    /**
     * 存在员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_crew' => $this->crew(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewCrew()
    {
        $setCrew = $this->crew()[0];

        $data = array(
            "data"=>array(
                "type"=>"crews",
                "attributes"=>array(
                    "userName"=>'18800000000',
                    "password"=>'Admin123$'
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'crews/signIn',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->crewCompareData($setCrew, $contents['data']);
    }
}
