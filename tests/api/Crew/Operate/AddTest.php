<?php
namespace Base\Crew\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Crew\SetDataApiTrait;
use Base\Crew\CrewUtils;
use Base\Crew\Model\Crew;

use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试员工接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CrewUtils, UserGroupSetDataApiTrait, DepartmentSetDataApiTrait;

    private $crew;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->crew);
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
        $this->clear('pcore_crew');
    }

    /**
     * 不存在员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department()
            ]
        );
    }

    public function testViewCrew()
    {
        $setCrew = $this->crew()[0];

        $data = array(
            "data"=>array(
                "type"=>"crews",
                "attributes"=>array(
                    "realName"=>'张科',
                    "cardId"=>'610424198810202422',
                    "cellphone"=>'18800000000',
                    "password"=>'Admin123$',
                    "category"=>Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'],
                    "purview"=>array(1,2,3,4),
                ),
                "relationships"=>array(
                    "userGroup"=>array(
                        "data"=>array(
                            array("type"=>"userGroups","id"=>1)
                        )
                    ),
                    "department"=>array(
                        "data"=>array(
                            array("type"=>"departments","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'crews',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->crewCompareData($setCrew, $contents['data']);
    }
}
