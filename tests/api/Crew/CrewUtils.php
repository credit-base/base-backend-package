<?php
namespace Base\Crew;

trait CrewUtils
{
    private function crewCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['crew_id'], $responseDataArray['id']);
        $this->assertEquals('crews', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['real_name'], $attributes['realName']);
        $this->assertEquals($setDataArray['card_id'], $attributes['cardId']);
        $this->assertEquals($setDataArray['user_name'], $attributes['userName']);
        $this->assertEquals($setDataArray['cellphone'], $attributes['cellphone']);
        $this->assertEquals($setDataArray['category'], $attributes['category']);
        $this->assertEquals(json_decode($setDataArray['purview'], true), $attributes['purview']);
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsUserGroup = $responseDataArray['relationships']['userGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['user_group_id'], $relationshipsUserGroup['id']);

        $relationshipsDepartment = $responseDataArray['relationships']['department']['data'];
        $this->assertEquals('departments', $relationshipsDepartment['type']);
        $this->assertEquals($setDataArray['department_id'], $relationshipsDepartment['id']);
    }
}
