<?php
namespace Base\Crew\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Crew\SetDataApiTrait;
use Base\Crew\CrewUtils;
use Base\Crew\Model\Crew;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Department\DepartmentUtils;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试员工接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CrewUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    DepartmentSetDataApiTrait, DepartmentUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_department');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_department' => $this->department(),
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew()
            ]
        );
    }

    public function testViewCrew()
    {
        $setCrewList = $this->crew();
        $setUserGroupList = $this->userGroup();
        foreach ($setUserGroupList as $setUserGroup) {
            $setUserGroupList[$setUserGroup['user_group_id']] = $setUserGroup;
        }

        $setDepartmentList = $this->department();
        foreach ($setDepartmentList as $setDepartment) {
            $setDepartmentList[$setDepartment['department_id']] = $setDepartment;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'crews/1,2?include=userGroup,department',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $crew) {
            $this->crewCompareData($setCrewList[$key], $crew);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'userGroups') {
                $this->userGroupCompareData($setUserGroupList[$value['id']], $value);
            }
            if ($value['type'] == 'departments') {
                $this->departmentCompareData($setDepartmentList[$value['id']], $value);
            }
        }
    }
}
