<?php
namespace Base\Crew\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Crew\SetDataApiTrait;
use Base\Crew\CrewUtils;
use Base\Crew\Model\Crew;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Department\DepartmentUtils;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

/**
 * 测试员工接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CrewUtils, UserGroupSetDataApiTrait, UserGroupUtils,
    DepartmentSetDataApiTrait, DepartmentUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
        $this->clear('pcore_department');
    }

    /**
     * 存在一条员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_crew' => $this->crew(),
                'pcore_department' => $this->department(),
            ]
        );
    }

    public function testViewCrew()
    {
        $setCrew = $this->crew()[0];
        $setUserGroup = $this->userGroup()[0];
        $setDepartment = $this->department()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'crews/1?include=userGroup,department',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->crewCompareData($setCrew, $contents['data']);
        $this->userGroupCompareData($setUserGroup, $contents['included'][0]);
        $this->departmentCompareData($setDepartment, $contents['included'][1]);
    }
}
