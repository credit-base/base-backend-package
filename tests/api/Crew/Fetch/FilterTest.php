<?php
namespace Base\Crew\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\Crew\CrewUtils;
use Base\Crew\Model\Crew;
use Base\Crew\SetDataApiTrait;

use Base\Department\DepartmentUtils;
use Base\Department\SetDataApiTrait as DepartmentSetDataApiTrait;

use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\SetDataApiTrait as UserGroupSetDataApiTrait;

use Base\Common\Model\IEnableAble;

/**
 * 测试员工接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, CrewUtils, DepartmentSetDataApiTrait, DepartmentUtils,
    UserGroupSetDataApiTrait, UserGroupUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_department');
        $this->clear('pcore_crew');
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条员工数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
                'pcore_department' => $this->department(),
                'pcore_crew' => $this->crew()
            ]
        );
    }

    public function testViewCrew()
    {
        $setCrewList = $this->crew();
        $setDepartmentList = $this->department();
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'crews?filter[department]=1&filter[userGroup]=1&filter[cellphone]=18800000000&
            filter[realName]=张科&filter[category]='.Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'].'&
            filter[status]='.IEnableAble::STATUS['ENABLED'].'&include=userGroup,department',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $crew) {
            $this->crewCompareData($setCrewList[$key], $crew);
        }

        $included = $contents['included'];

        $this->userGroupCompareData($setUserGroupList[0], $included[0]);
        $this->departmentCompareData($setDepartmentList[0], $included[1]);
    }
}
