<?php
namespace Base\Crew;

use Marmot\Core;

use Base\Crew\Model\Crew;

use Base\Common\Model\IEnableAble;

trait SetDataApiTrait
{
    protected function crew() : array
    {
        return
        [
            [
                'crew_id' => 1,
                'user_name' => '18800000000',
                'cellphone' => '18800000000',
                'real_name' => '张科',
                'card_id' => '610424198810202422',
                'status' => IEnableAble::STATUS['ENABLED'],
                'category' => Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'],
                'purview' => json_encode(array(1,2,3,4)),
                'password' => 'b0e735ecd1370d24fd026ccabcd2d198',
                'salt' => 'W8i3',
                'user_group_id' => 1,
                'department_id' => 1,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'crew_id' => 2,
                'user_name' => '18800000001',
                'cellphone' => '18800000001',
                'real_name' => '李煜',
                'card_id' => '610424198810202423',
                'status' => IEnableAble::STATUS['DISABLED'],
                'category' => Crew::CATEGORY['SUPER_ADMINISTRATOR'],
                'purview' => json_encode(array(1,2,3)),
                'password' => 'b0e735ecd1370d24fd026ccabcd2d199',
                'salt' => 'W8i4',
                'user_group_id' => 2,
                'department_id' => 2,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}
