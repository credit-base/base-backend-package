<?php
namespace Base\UserGroup;

trait UserGroupUtils
{
    private function userGroupCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['user_group_id'], $responseDataArray['id']);
        $this->assertEquals('userGroups', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($setDataArray['name'], $attributes['name']);
        $this->assertEquals($setDataArray['short_name'], $attributes['shortName']);
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);
    }
}
