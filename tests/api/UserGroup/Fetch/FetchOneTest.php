<?php
namespace Base\UserGroup\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\UserGroup\SetDataApiTrait;
use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\Model\UserGroup;

/**
 * 测试用户组接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, UserGroupUtils;

    private $userGroup;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->userGroup);
        $this->clear('pcore_user_group');
    }

    /**
     * 存在一条委办局数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup(),
            ]
        );
    }

    public function testViewUserGroup()
    {
        $setUserGroup = $this->userGroup()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request('GET', 'userGroups/1', ['headers'=> Core::$container->get('client.headers')]);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->userGroupCompareData($setUserGroup, $contents['data']);
    }
}
