<?php
namespace Base\UserGroup\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use Base\UserGroup\SetDataApiTrait;
use Base\UserGroup\UserGroupUtils;
use Base\UserGroup\Model\UserGroup;

/**
 * 测试用户组接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, UserGroupUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_user_group');
    }

    /**
     * 存在多条委办局数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_user_group' => $this->userGroup()
            ]
        );
    }

    public function testViewUserGroup()
    {
        $setUserGroupList = $this->userGroup();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request('GET', 'userGroups/1,2', ['headers'=> Core::$container->get('client.headers')]);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $userGroup) {
            $this->userGroupCompareData($setUserGroupList[$key], $userGroup);
        }
    }
}
