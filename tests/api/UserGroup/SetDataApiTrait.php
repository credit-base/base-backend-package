<?php
namespace Base\UserGroup;

use Marmot\Core;

use Base\UserGroup\Model\UserGroup;

trait SetDataApiTrait
{
    protected function userGroup() : array
    {
        return
        [
            [
                'user_group_id' => 1,
                'name' => '发展和改革委员会',
                'short_name' => '发改委',
                'status' => UserGroup::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ],
            [
                'user_group_id' => 2,
                'name' => '税务局',
                'short_name' => '税务局',
                'status' => UserGroup::STATUS_NORMAL,
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }
}
