<?php
namespace Base\ApplyForm\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Repository\ApplyFormRepository;
use Base\ApplyForm\Command\ApplyForm\ApproveApplyFormCommand;
use Base\ApplyForm\Command\ApplyForm\RejectApplyFormCommand;
use Base\ApplyForm\CommandHandler\ApplyForm\ApplyFormCommandHandlerFactory;

use Base\Common\WidgetRule\CommonWidgetRule;

trait ApplyFormControllerTrait
{
    protected function getApplyFormRepository() : ApplyFormRepository
    {
        return new ApplyFormRepository();
    }

    abstract protected function displaySuccess(IApplyFormAble $applyForm);

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new ApplyFormCommandHandlerFactory());
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    /**
     * 申请信息审核通过功能,通过PATCH传参
     * @param int id 申请信息id
     *
     * @return jsonApi
     */
    public function approve(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $relationships = $data['relationships'];

        $applyCrewId = $relationships['applyCrew']['data'][0]['id'];

        if ($this->validateCommonScenario($applyCrewId)) {
            $command = new ApproveApplyFormCommand($applyCrewId, $id);

            if ($this->getCommandBus()->send($command)) {
                $applyForm = $this->getApplyFormRepository()->fetchOne($command->id);
                
                if ($applyForm instanceof IApplyFormAble) {
                    $this->displaySuccess($applyForm);
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 申请信息审核驳回功能,通过PATCH传参
     * @param int id 申请信息id
     *
     * @return jsonApi
     */
    public function reject(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $rejectReason = isset($attributes['rejectReason']) ? $attributes['rejectReason'] : array();
        $applyCrewId = $relationships['applyCrew']['data'][0]['id'];

        if ($this->validateCommonScenario($applyCrewId) && $this->validateRejectScenario($rejectReason)) {
            $command = new RejectApplyFormCommand(
                $applyCrewId,
                $rejectReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $applyForm = $this->getApplyFormRepository()->fetchOne($command->id);
                
                if ($applyForm instanceof IApplyFormAble) {
                    $this->displaySuccess($applyForm);
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateCommonScenario(
        $applyCrewId
    ) {
        return $this->getCommonWidgetRule()->formatNumeric($applyCrewId, 'applyCrewId');
    }

    protected function validateRejectScenario(
        $rejectReason
    ) {
        return $this->getCommonWidgetRule()->description($rejectReason, 'rejectReason');
    }
}
