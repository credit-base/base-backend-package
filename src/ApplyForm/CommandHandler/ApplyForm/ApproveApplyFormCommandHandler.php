<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use Base\Common\Model\IApproveAble;
use Base\Common\Command\ApproveCommand;
use Base\Common\CommandHandler\ApproveCommandHandler;

class ApproveApplyFormCommandHandler extends ApproveCommandHandler
{
    use ApplyFormCommandHandlerTrait;

    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchApplyForm($id);
    }

    protected function executeAction(ApproveCommand $command)
    {
        $this->approveAble = $this->fetchIApplyObject($command->id);
        $applyCrew = $this->fetchCrew($command->applyCrew);
        $this->approveAble->setApplyCrew($applyCrew);

        return $this->approveAble->approve();
    }
}
