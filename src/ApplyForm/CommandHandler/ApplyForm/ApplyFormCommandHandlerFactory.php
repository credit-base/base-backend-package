<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ApplyFormCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\ApplyForm\Command\ApplyForm\ApproveApplyFormCommand' =>
        'Base\ApplyForm\CommandHandler\ApplyForm\ApproveApplyFormCommandHandler',
        'Base\ApplyForm\Command\ApplyForm\RejectApplyFormCommand' =>
        'Base\ApplyForm\CommandHandler\ApplyForm\RejectApplyFormCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
