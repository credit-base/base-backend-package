<?php
namespace Base\ApplyForm\CommandHandler\ApplyForm;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

trait ApplyFormCommandHandlerTrait
{
    private $repository;

    private $crewRepository;
    
    public function __construct()
    {
        $this->repository = new ApplyFormRepository();
        $this->crewRepository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->crewRepository);
    }

    protected function getRepository() : ApplyFormRepository
    {
        return $this->repository;
    }
    
    protected function fetchApplyForm(int $id) : IApplyFormAble
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }
    
    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }
}
