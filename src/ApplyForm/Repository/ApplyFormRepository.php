<?php
namespace Base\ApplyForm\Repository;

use Marmot\Framework\Classes\Repository;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;
use Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter;

class ApplyFormRepository extends Repository implements IApplyFormAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new ApplyFormDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IApplyFormAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IApplyFormAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IApplyFormAdapter
    {
        return $this->adapter;
    }

    public function fetchOne($id) : IApplyFormAble
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(IApplyFormAble $applyForm) : bool
    {
        return $this->getAdapter()->add($applyForm);
    }

    public function edit(IApplyFormAble $applyForm, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($applyForm, $keys);
    }
}
