<?php
namespace Base\ApplyForm\Repository;

use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\Common\Repository\NullRepository;

class RepositoryFactory
{
    const MAPS = array(
        ApplyInfoCategory::APPLY_INFO_CATEGORY['NEWS'] =>
        'Base\Crew\Repository\CrewRepository',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['JOURNAL'] =>
        'Base\Crew\Repository\CrewRepository',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_COMPLAINT'] =>
        'Base\Member\Repository\MemberRepository',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_PRAISE'] =>
        'Base\Member\Repository\MemberRepository',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_APPEAL'] =>
        'Base\Member\Repository\MemberRepository',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_FEEDBACK'] =>
        'Base\Member\Repository\MemberRepository',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_QA'] =>
        'Base\Member\Repository\MemberRepository',
    );

    public function getRepository(int $category)
    {
        $repository = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($repository) ? new $repository : NullRepository::getInstance();
    }
}
