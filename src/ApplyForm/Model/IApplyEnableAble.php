<?php
namespace Base\ApplyForm\Model;

use Base\Common\Model\IEnableAble;

interface IApplyEnableAble extends IEnableAble
{
}
