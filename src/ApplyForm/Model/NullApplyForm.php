<?php
namespace Base\ApplyForm\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Common\Model\NullApproveAbleTrait;
use Base\Common\Model\NullResubmitAbleTrait;

class NullApplyForm implements INull, IApplyFormAble, IObject
{
    use NullApplyOperationAbleTrait,
        NullApplyEnableAbleTrait,
        NullApplyTopAbleTrait,
        NullApproveAbleTrait,
        NullResubmitAbleTrait,
        Object;

    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function setId($id)
    {
        unset($id);
        return $this->resourceNotExist();
    }

    public function getId()
    {
        return $this->resourceNotExist();
    }

    public function setStatus(int $status)
    {
        unset($status);
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
