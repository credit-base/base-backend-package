<?php
namespace Base\ApplyForm\Model;

trait NullApplyEnableAbleTrait
{
    public function enable() : bool
    {
        return $this->resourceNotExist();
    }

    public function disable() : bool
    {
        return $this->resourceNotExist();
    }

    abstract protected function resourceNotExist() : bool;
}
