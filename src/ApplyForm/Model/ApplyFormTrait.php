<?php
namespace Base\ApplyForm\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;

use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Common\Model\IApproveAble;
use Base\Common\Model\ApproveAbleTrait;
use Base\Common\Model\ResubmitAbleTrait;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

trait ApplyFormTrait
{
    use ApplyOperationAbleTrait, ApplyEnableAbleTrait, ApplyTopAbleTrait, ApproveAbleTrait, ResubmitAbleTrait;

    protected $applyId;

    protected $applyTitle;

    protected $relation;

    protected $operationType;

    protected $applyInfoCategory;
   
    protected $applyInfo;

    protected $applyCrew;

    protected $applyUserGroup;

    protected $repository;

    public function setApplyId($applyId) : void
    {
        $this->applyId = $applyId;
    }

    public function getApplyId()
    {
        return $this->applyId;
    }

    public function setApplyTitle(string $applyTitle) : void
    {
        $this->applyTitle = $applyTitle;
    }

    public function getApplyTitle() : string
    {
        return $this->applyTitle;
    }

    public function setRelation($relation) : void
    {
        $this->relation = $relation;
    }

    public function getRelation()
    {
        return $this->relation;
    }

    public function setOperationType(int $operationType) : void
    {
        $this->operationType = in_array($operationType, IApproveAble::OPERATION_TYPE)
        ? $operationType
        : IApproveAble::OPERATION_TYPE['NULL'];
    }

    public function getOperationType() : int
    {
        return $this->operationType;
    }

    public function setApplyInfoCategory(ApplyInfoCategory $applyInfoCategory) : void
    {
        $this->applyInfoCategory = $applyInfoCategory;
    }

    public function getApplyInfoCategory() : ApplyInfoCategory
    {
        return $this->applyInfoCategory;
    }

    public function setApplyInfo(array $applyInfo) : void
    {
        $this->applyInfo = $applyInfo;
    }

    public function getApplyInfo() : array
    {
        return $this->applyInfo;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setApplyUserGroup(UserGroup $applyUserGroup) : void
    {
        $this->applyUserGroup = $applyUserGroup;
    }

    public function getApplyUserGroup() : UserGroup
    {
        return $this->applyUserGroup;
    }

    protected function getApplyFormRepository() : ApplyFormRepository
    {
        return new ApplyFormRepository();
    }

    public function apply() : bool
    {
        return $this->getApplyFormRepository()->add($this);
    }

    public function resubmitAction() : bool
    {
        $this->setApplyStatus(IApproveAble::APPLY_STATUS['PENDING']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getApplyFormRepository()->edit($this, array(
            'applyTitle',
            'relation',
            'applyInfo',
            'applyStatus',
            'applyInfoType',
            'updateTime',
            'statusTime'
        ));
    }

    public function approveAction() : bool
    {
        return $this->updateApplyStatus(IApproveAble::APPLY_STATUS['APPROVE'])
            && $this->approveApplyInfo($this->getOperationType());
    }
    /**
     * @codeCoverageIgnore
     */
    protected function approveApplyInfo(int $type) : bool
    {
        switch ($type) {
            //添加
            case IApproveAble::OPERATION_TYPE['ADD']:
                return parent::add();
            //编辑
            case IApproveAble::OPERATION_TYPE['EDIT']:
                return parent::edit();
            //启用
            case IApproveAble::OPERATION_TYPE['ENABLE']:
                return parent::enable();
            //禁用
            case IApproveAble::OPERATION_TYPE['DISABLE']:
                return parent::disable();
            //置顶
            case IApproveAble::OPERATION_TYPE['TOP']:
                return parent::top();
            //取消置顶
            case IApproveAble::OPERATION_TYPE['CANCEL_TOP']:
                return parent::cancelTop();
            //移动
            case IApproveAble::OPERATION_TYPE['MOVE']:
                return parent::move();
            //受理
            case IApproveAble::OPERATION_TYPE['ACCEPT']:
                return parent::accept();
        }
    }
    
    public function rejectAction() : bool
    {
        return $this->updateApplyStatus(IApproveAble::APPLY_STATUS['REJECT']);
    }

    protected function updateApplyStatus(int $applyStatus) : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        $this->setApplyStatus($applyStatus);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getApplyFormRepository()->edit(
            $this,
            array('updateTime','applyStatus','statusTime', 'applyCrew', 'rejectReason')
        );
    }
}
