<?php
namespace Base\ApplyForm\Model;

use Base\Common\Model\IOperate;

interface IApplyOperationAble extends IOperate
{
}
