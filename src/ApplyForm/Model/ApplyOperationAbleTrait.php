<?php
namespace Base\ApplyForm\Model;

trait ApplyOperationAbleTrait
{
    public function add() : bool
    {
        return $this->apply();
    }

    public function edit() : bool
    {
        return $this->apply();
    }

    abstract protected function apply() : bool;
}
