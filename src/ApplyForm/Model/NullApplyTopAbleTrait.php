<?php
namespace Base\ApplyForm\Model;

trait NullApplyTopAbleTrait
{
    public function top() : bool
    {
        return $this->resourceNotExist();
    }

    public function cancelTop() : bool
    {
        return $this->resourceNotExist();
    }

    abstract protected function resourceNotExist() : bool;
}
