<?php
namespace Base\ApplyForm\Model;

use Base\Common\Model\IApproveAble;
use Base\Common\Model\IResubmitAble;

interface IApplyFormAble extends IApplyOperationAble, IApplyEnableAble, IApplyTopAble, IApproveAble, IResubmitAble
{
}
