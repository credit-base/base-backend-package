<?php
namespace Base\ApplyForm\Model;

use Marmot\Core;
use Base\Common\Model\TopAbleTrait;

trait ApplyTopAbleTrait
{
    use TopAbleTrait;
    
    public function top() : bool
    {
        if (!$this->isCancelTop()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'stick'));
            return false;
        }

        return $this->apply();
    }

    public function cancelTop() : bool
    {
        if (!$this->isTop()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'stick'));
            return false;
        }
        
        return $this->apply();
    }

    abstract protected function apply() : bool;
}
