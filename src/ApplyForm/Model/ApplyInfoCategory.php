<?php
namespace Base\ApplyForm\Model;

class ApplyInfoCategory
{
    /**
     * APPLY_INFO_CATEGORY 可申请的信息分类
     *
     * @var  APPLY_INFO_CATEGORY['NULL']
     * @var  APPLY_INFO_CATEGORY['NEWS'] 新闻
     * @var  APPLY_INFO_CATEGORY['JOURNAL'] 信用刊物
     * @var  APPLY_INFO_CATEGORY['INTERACTION_COMPLAINT'] 互动类-信用投诉
     * @var  APPLY_INFO_CATEGORY['INTERACTION_PRAISE'] 互动类-信用表扬
     * @var  APPLY_INFO_CATEGORY['INTERACTION_APPEAL'] 互动类-异议申诉
     * @var  APPLY_INFO_CATEGORY['INTERACTION_FEEDBACK'] 互动类-问题反馈
     * @var  APPLY_INFO_CATEGORY['INTERACTION_QA'] 互动类-信用问答
     *
     */
    const APPLY_INFO_CATEGORY = array(
        'NULL' => 0,
        'NEWS' => 1,
        'JOURNAL' => 2,
        'INTERACTION_COMPLAINT' => 3,
        'INTERACTION_PRAISE' => 4,
        'INTERACTION_APPEAL' => 5,
        'INTERACTION_FEEDBACK' => 6,
        'INTERACTION_QA' => 7,
    );
 
    /**
     * APPLY_INFO_TYPE 可申请的最小化信息分类
     *
     */
    const APPLY_INFO_TYPE = array(
        self::APPLY_INFO_CATEGORY['NEWS'] => array(NEWS_TYPE),
        self::APPLY_INFO_CATEGORY['JOURNAL'] => array('JOURNAL' => 1),
        self::APPLY_INFO_CATEGORY['INTERACTION_COMPLAINT'] => array('COMPLAINT' => 1),
        self::APPLY_INFO_CATEGORY['INTERACTION_PRAISE'] => array('PRAISE' => 1),
        self::APPLY_INFO_CATEGORY['INTERACTION_APPEAL'] => array('APPEAL' => 1),
        self::APPLY_INFO_CATEGORY['INTERACTION_FEEDBACK'] => array('FEEDBACK' => 1),
        self::APPLY_INFO_CATEGORY['INTERACTION_QA'] => array('QA' => 1),
    );

    /**
     * @var int $category 可以申请的信息分类
     */
    private $category;
    /**
     * @var int $type 最小化类型
     */
    private $type;

    public function __construct(int $category = 0, $type = 0)
    {
        $this->category = $category;
        $this->type = $type;
    }

    public function __destruct()
    {
        unset($this->category);
        unset($this->type);
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function getType() : int
    {
        return $this->type;
    }
}
