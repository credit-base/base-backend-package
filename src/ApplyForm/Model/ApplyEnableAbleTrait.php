<?php
namespace Base\ApplyForm\Model;

use Marmot\Core;
use Base\Common\Model\EnableAbleTrait;

trait ApplyEnableAbleTrait
{
    use EnableAbleTrait;
    
    public function enable() : bool
    {
        if (!$this->isDisabled()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        return $this->apply();
    }

    public function disable() : bool
    {
        if (!$this->isEnabled()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        return $this->apply();
    }

    abstract protected function apply() : bool;
}
