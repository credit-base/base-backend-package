<?php
namespace Base\ApplyForm\Command\ApplyForm;

use Base\Common\Command\ApproveCommand;

class ApproveApplyFormCommand extends ApproveCommand
{
    public $applyCrew;

    public function __construct(
        int $applyCrew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        $this->applyCrew = $applyCrew;
    }
}
