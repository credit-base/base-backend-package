<?php
namespace Base\ApplyForm\Command\ApplyForm;

use Marmot\Interfaces\ICommand;

use Base\Common\Model\IApproveAble;

class MoveApplyFormCommand implements ICommand
{
    public $crew;

    public $id;
    
    public $operationType;

    public function __construct(
        int $crew,
        int $id = 0,
        int $operationType = IApproveAble::OPERATION_TYPE['MOVE']
    ) {
        $this->crew = $crew;
        $this->id = $id;
        $this->operationType = $operationType;
    }
}
