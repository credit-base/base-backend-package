<?php
namespace Base\ApplyForm\Command\ApplyForm;

use Base\Common\Command\RejectCommand;

class RejectApplyFormCommand extends RejectCommand
{
    public $applyCrew;

    public function __construct(
        int $applyCrew,
        string $rejectReason,
        int $id = 0
    ) {
        parent::__construct(
            $rejectReason,
            $id
        );
        $this->applyCrew = $applyCrew;
    }
}
