<?php
namespace Base\ApplyForm\Adapter\ApplyForm;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\ApplyForm\Model\NullApplyForm;
use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Model\ApplyFormTrait;
use Base\ApplyForm\Model\ApplyInfoCategory;
use Base\ApplyForm\Translator\TranslatorFactory;
use Base\ApplyForm\Repository\RepositoryFactory;
use Base\ApplyForm\Translator\ApplyFormDbTranslator;
use Base\ApplyForm\Adapter\ApplyForm\Query\ApplyFormRowCacheQuery;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ApplyFormDbAdapter implements IApplyFormAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $repositoryFactory;

    private $translatorFactory;

    private $crewRepository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->dbTranslator = new ApplyFormDbTranslator();
        $this->rowCacheQuery = new ApplyFormRowCacheQuery();
        $this->repositoryFactory = new RepositoryFactory();
        $this->translatorFactory = new TranslatorFactory();
        $this->crewRepository = new CrewRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->repositoryFactory);
        unset($this->translatorFactory);
        unset($this->crewRepository);
        unset($this->userGroupRepository);
    }
    
    protected function getDbTranslator() : ApplyFormDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : ApplyFormRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getRepositoryFactory() : RepositoryFactory
    {
        return $this->repositoryFactory;
    }

    protected function getTranslatorFactory()
    {
        return $this->translatorFactory;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getNullObject() : INull
    {
        return NullApplyForm::getInstance();
    }

    public function add(IApplyFormAble $applyForm) : bool
    {
        return $this->addAction($applyForm);
    }

    protected function addAction(IObject $object) : bool
    {
        $info = array();
        $info = $this->getDbTranslator()->objectToArray($object);

        $id = $this->getRowQuery()->add($info);
        if (!$id) {
            return false;
        }

        $object->setApplyId($id);
        return true;
    }

    public function edit(IApplyFormAble $applyForm, array $keys = array()) : bool
    {
        return $this->editAction($applyForm, $keys);
    }

    protected function editAction(IObject $object, array $keys = array()) : bool
    {
        $info = array();
        $info = $this->getDbTranslator()->objectToArray($object, $keys);

        $rowQuery = $this->getRowQuery();
        $conditionArray[$rowQuery->getPrimaryKey()] = $object->getApplyId();

        return $rowQuery->update($info, $conditionArray);
    }

    public function fetchOne($id) : IApplyFormAble
    {
        $applyForm = $this->fetchOneAction($id);

        $this->fetchRelation($applyForm);
        $this->fetchApplyCrew($applyForm);
        $this->fetchApplyUserGroup($applyForm);

        return $applyForm;
    }

    protected function fetchOneAction($id) : IObject
    {
        $info = array();

        $info = $this->getRowQuery()->fetchOne($id);
        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return $this->getNullObject();
        }

        $applyInfo =unserialize(gzuncompress(base64_decode($info['apply_info'], true)));

        $info = array_merge($applyInfo, $info);

        $translator = $this->getTranslatorFactory()->getTranslator($info['apply_info_category']);

        $object = $translator->arrayToObject($info);
        $object = $translator->arrayToObjects($info, $object);

        return $object;
    }
    
    public function fetchList(array $ids) : array
    {
        $applyFormList = array();
        $applyFormList = $this->fetchListAction($ids);

        $this->fetchRelation($applyFormList);
        $this->fetchApplyCrew($applyFormList);
        $this->fetchApplyUserGroup($applyFormList);

        return $applyFormList;
    }

    protected function fetchListAction(array $ids) : array
    {
        $objectList = array();
        
        $objectInfoList = $this->getRowQuery()->fetchList($ids);
        if (empty($objectInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        foreach ($objectInfoList as $objectInfo) {
            $translator = $this->getTranslatorFactory()->getTranslator($objectInfo['apply_info_category']);

            $applyInfo =unserialize(gzuncompress(base64_decode($objectInfo['apply_info'], true)));

            $objectInfo = array_merge($applyInfo, $objectInfo);

            $object = $translator->arrayToObject($objectInfo);

            $object = $translator->arrayToObjects($objectInfo, $object);

            $objectList[$object->getApplyId()] = $object;
        }

        return $objectList;
    }

    protected function fetchApplyUserGroup($applyForm)
    {
        return is_array($applyForm) ?
        $this->fetchApplyUserGroupByList($applyForm) :
        $this->fetchApplyUserGroupByObject($applyForm);
    }

    protected function fetchApplyUserGroupByObject($applyForm)
    {
        if (!$applyForm instanceof INull) {
            $userGroupId = $applyForm->getApplyUserGroup()->getId();
            $userGroup = $this->getUserGroupRepository()->fetchOne($userGroupId);
            $applyForm->setApplyUserGroup($userGroup);
        }
    }

    protected function fetchApplyUserGroupByList(array $applyFormList)
    {
        if (!empty($applyFormList)) {
            $userGroupIds = array();
            foreach ($applyFormList as $applyForm) {
                $userGroupIds[] = $applyForm->getApplyUserGroup()->getId();
            }

            $userGroupList = $this->getUserGroupRepository()->fetchList($userGroupIds);

            if (!empty($userGroupList)) {
                foreach ($applyFormList as $applyForm) {
                    if (isset($userGroupList[$applyForm->getApplyUserGroup()->getId()])) {
                        $applyForm->setApplyUserGroup($userGroupList[$applyForm->getApplyUserGroup()->getId()]);
                    }
                }
            }
        }
    }

    protected function fetchApplyCrew($applyForm)
    {
        return is_array($applyForm) ?
        $this->fetchApplyCrewByList($applyForm) :
        $this->fetchApplyCrewByObject($applyForm);
    }

    protected function fetchApplyCrewByObject($applyForm)
    {
        if (!$applyForm instanceof INull) {
            $crewId = $applyForm->getApplyCrew()->getId();
            $crew = $this->getCrewRepository()->fetchOne($crewId);
            $applyForm->setApplyCrew($crew);
        }
    }

    protected function fetchApplyCrewByList(array $applyFormList)
    {
        if (!empty($applyFormList)) {
            $crewIds = array();
            foreach ($applyFormList as $applyForm) {
                $crewIds[] = $applyForm->getApplyCrew()->getId();
            }

            $crewList = $this->getCrewRepository()->fetchList($crewIds);
            if (!empty($crewList)) {
                foreach ($applyFormList as $applyForm) {
                    if (isset($crewList[$applyForm->getApplyCrew()->getId()])) {
                        $applyForm->setApplyCrew($crewList[$applyForm->getApplyCrew()->getId()]);
                    }
                }
            }
        }
    }
    
    protected function fetchRelation($applyForm)
    {
        return is_array($applyForm) ?
        $this->fetchRelationByList($applyForm) :
        $this->fetchRelationByObject($applyForm);
    }

    protected function fetchRelationByObject($applyForm)
    {
        if (!$applyForm instanceof INull) {
            $repository = $this->getRepositoryFactory()->getRepository(
                $applyForm->getApplyInfoCategory()->getCategory()
            );

            $relationId = $applyForm->getRelation()->getId();
            $relation = $repository->fetchOne($relationId);
            $applyForm->setRelation($relation);
        }
    }

    protected function fetchRelationByList(array $applyFormList)
    {
        if (!empty($applyFormList)) {
            $relationIds = array();
            foreach ($applyFormList as $applyForm) {
                $relationIds[$applyForm->getApplyInfoCategory()->getCategory()][] = array(
                    'relationId' => $applyForm->getRelation()->getId(),
                    'applyFormId' => $applyForm->getApplyId()
                );
            }

            $objectReferenceList = array();

            foreach ($relationIds as $category => $val) {
                $ids = array_unique(array_column($val, 'relationId'));
                $relationRepository = $this->getRepositoryFactory()->getRepository($category);
                $relationListTemp = $relationRepository->fetchList($ids);
                
                foreach ($val as $v) {
                    if (isset($relationListTemp[$v['relationId']])) {
                        $objectReferenceList[$v['applyFormId']] = $relationListTemp[$v['relationId']];
                    }
                }
            }

            if (!empty($objectReferenceList)) {
                foreach ($applyFormList as $applyForm) {
                    if (isset($objectReferenceList[$applyForm->getApplyId()])) {
                        $applyForm->setRelation($objectReferenceList[$applyForm->getApplyId()]);
                    }
                }
            }
        }
    }

    protected function applyObject() : IApplyFormAble
    {
        $applyForm = new class implements IApplyFormAble{
            use ApplyFormTrait, Object;
            public function __construct()
            {
                self::setApplyStatus(0);
                self::setUpdateTime(Core::$container->get('time'));
            }
        };

        return $applyForm;
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';
        
        if (!empty($filter)) {
            $applyForm = $this->applyObject();

            if (isset($filter['applyInfoCategory'])) {
                $applyForm->setApplyInfoCategory(
                    new ApplyInfoCategory($filter['applyInfoCategory'])
                );
                $info = $this->getDbTranslator()->objectToArray(
                    $applyForm,
                    array('applyInfoCategory')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['applyInfoType'])) {
                $info = $this->getDbTranslator()->objectToArray($applyForm, array('applyInfoType'));
                $condition .= $conjection.key($info).' IN ('.$filter['applyInfoType'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['relation'])) {
                $condition .= $conjection.'relation_id'.' = '.$filter['relation'];
                $conjection = ' AND ';
            }
            if (isset($filter['applyUserGroup'])) {
                $applyForm->setApplyUserGroup(new UserGroup($filter['applyUserGroup']));
                $info = $this->getDbTranslator()->objectToArray(
                    $applyForm,
                    array('applyUserGroup')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['title'])) {
                $applyForm->setApplyTitle($filter['title']);
                $info = $this->getDbTranslator()->objectToArray(
                    $applyForm,
                    array('applyTitle')
                );
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['applyStatus'])) {
                $applyForm->setApplyStatus(0);
                $info = $this->getDbTranslator()->objectToArray($applyForm, array('applyStatus'));
                $condition .= $conjection.key($info).' IN ('.$filter['applyStatus'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['operationType'])) {
                $applyForm->setOperationType($filter['operationType']);
                $info = $this->getDbTranslator()->objectToArray($applyForm, array('operationType'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $applyForm = $this->applyObject();

            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($applyForm, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }

            if (isset($sort['applyStatus'])) {
                $info = $this->getDbTranslator()->objectToArray($applyForm, array('applyStatus'));
                $condition .= $conjection.key($info).' '.($sort['applyStatus'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}
