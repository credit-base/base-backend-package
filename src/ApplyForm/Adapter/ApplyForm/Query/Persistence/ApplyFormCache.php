<?php
namespace Base\ApplyForm\Adapter\ApplyForm\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class ApplyFormCache extends Cache
{
    const KEY = 'apply_form';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
