<?php
namespace Base\ApplyForm\Adapter\ApplyForm\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ApplyFormDb extends Db
{
    const TABLE = 'apply_form';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
