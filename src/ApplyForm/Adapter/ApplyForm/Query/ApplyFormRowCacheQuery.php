<?php
namespace Base\ApplyForm\Adapter\ApplyForm\Query;

use Marmot\Framework\Query\RowCacheQuery;

class ApplyFormRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'apply_form_id',
            new Persistence\ApplyFormCache(),
            new Persistence\ApplyFormDb()
        );
    }
}
