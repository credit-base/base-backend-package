<?php
namespace Base\ApplyForm\Adapter\ApplyForm;

use Base\ApplyForm\Model\IApplyFormAble;

interface IApplyFormAdapter
{
    public function fetchOne($id) : IApplyFormAble;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(IApplyFormAble $applyForm) : bool;

    public function edit(IApplyFormAble $applyForm, array $keys = array()) : bool;
}
