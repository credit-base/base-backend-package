<?php
namespace Base\ApplyForm\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\NullTranslator;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

class TranslatorFactory
{
    const MAPS = array(
        ApplyInfoCategory::APPLY_INFO_CATEGORY['NEWS']=>
        'Base\News\Translator\UnAuditedNewsDbTranslator',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['JOURNAL']=>
        'Base\Journal\Translator\UnAuditedJournalDbTranslator',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_COMPLAINT']=>
        'Base\Interaction\Translator\UnAuditedComplaintDbTranslator',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_PRAISE']=>
        'Base\Interaction\Translator\UnAuditedPraiseDbTranslator',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_APPEAL']=>
        'Base\Interaction\Translator\UnAuditedAppealDbTranslator',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_FEEDBACK']=>
        'Base\Interaction\Translator\UnAuditedFeedbackDbTranslator',
        ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_QA']=>
        'Base\Interaction\Translator\UnAuditedQADbTranslator',
    );

    public function getTranslator(int $category) : ITranslator
    {
        $translator = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
