<?php
namespace Base\ApplyForm\Translator;

use Marmot\Interfaces\ITranslator;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Model\ApplyInfoCategory;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ApplyFormDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $applyForm = null)
    {
        if ($applyForm != null) {
            if (isset($expression['title'])) {
                $applyForm->setApplyTitle($expression['title']);
            }
            if (isset($expression['relation_id'])) {
                $applyForm->getRelation()->setId($expression['relation_id']);
            }
            if (isset($expression['operation_type'])) {
                $applyForm->setOperationType($expression['operation_type']);
            }
            if (isset($expression['apply_info_category']) && isset($expression['apply_info_type'])) {
                $applyForm->setApplyInfoCategory(
                    new ApplyInfoCategory(
                        $expression['apply_info_category'],
                        $expression['apply_info_type']
                    )
                );
            }
            if (isset($expression['apply_info'])) {
                $applyForm->setApplyInfo(unserialize(gzuncompress(base64_decode($expression['apply_info'], true))));
            }
            if (isset($expression['apply_crew_id'])) {
                $applyForm->getApplyCrew()->setId($expression['apply_crew_id']);
            }
            if (isset($expression['apply_usergroup_id'])) {
                $applyForm->getApplyUserGroup()->setId($expression['apply_usergroup_id']);
            }
            if (isset($expression['reject_reason'])) {
                $applyForm->setRejectReason($expression['reject_reason']);
            }
            if (isset($expression['apply_status'])) {
                $applyForm->setApplyStatus($expression['apply_status']);
            }
            if (isset($expression['create_time'])) {
                $applyForm->setCreateTime($expression['create_time']);
            }
            if (isset($expression['update_time'])) {
                $applyForm->setUpdateTime($expression['update_time']);
            }
            if (isset($expression['status_time'])) {
                $applyForm->setStatusTime($expression['status_time']);
            }
        }

        return $applyForm;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($applyForm, array $keys = array())
    {
        if (!$applyForm instanceof IApplyFormAble) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'applyId',
                'applyTitle',
                'relation',
                'operationType',
                'applyInfoCategory',
                'applyInfoType',
                'applyInfo',
                'applyCrew',
                'applyUserGroup',
                'rejectReason',
                'applyStatus',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('applyId', $keys)) {
            $expression['apply_form_id'] = $applyForm->getApplyId();
        }
        if (in_array('applyTitle', $keys)) {
            $expression['title'] = $applyForm->getApplyTitle();
        }
        if (in_array('relation', $keys)) {
            $expression['relation_id'] = $applyForm->getRelation()->getId();
        }
        if (in_array('operationType', $keys)) {
            $expression['operation_type'] = $applyForm->getOperationType();
        }
        if (in_array('applyInfoCategory', $keys)) {
            $expression['apply_info_category'] = $applyForm->getApplyInfoCategory()->getCategory();
        }
        if (in_array('applyInfoType', $keys)) {
            $expression['apply_info_type'] = $applyForm->getApplyInfoCategory()->getType();
        }
        if (in_array('applyInfo', $keys)) {
            $expression['apply_info'] = base64_encode(gzcompress(serialize($applyForm->getApplyInfo())));
        }
        if (in_array('applyCrew', $keys)) {
            $expression['apply_crew_id'] = $applyForm->getApplyCrew()->getId();
        }
        if (in_array('applyUserGroup', $keys)) {
            $expression['apply_usergroup_id'] = $applyForm->getApplyUserGroup()->getId();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['reject_reason'] = $applyForm->getRejectReason();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['apply_status'] = $applyForm->getApplyStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $applyForm->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $applyForm->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $applyForm->getStatusTime();
        }

        return $expression;
    }
}
