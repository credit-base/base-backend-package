<?php
namespace Base\Crew\Adapter\Crew;

use Marmot\Interfaces\INull;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Crew\Model\Crew;
use Base\Crew\Model\NullCrew;
use Base\Crew\Translator\CrewDbTranslator;
use Base\Crew\Adapter\Crew\Query\CrewRowCacheQuery;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;
use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Adapter\Department\IDepartmentAdapter;

class CrewDbAdapter implements ICrewAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $userGroupRepository;

    private $departmentRepository;

    public function __construct()
    {
        $this->dbTranslator = new CrewDbTranslator();
        $this->rowCacheQuery = new CrewRowCacheQuery();
        $this->userGroupRepository = new UserGroupRepository();
        $this->departmentRepository = new DepartmentRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->userGroupRepository);
        unset($this->departmentRepository);
    }
    
    protected function getDbTranslator() : CrewDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : CrewRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getUserGroupRepository() : IUserGroupAdapter
    {
        return $this->userGroupRepository;
    }

    protected function getDepartmentRepository() : IDepartmentAdapter
    {
        return $this->departmentRepository;
    }

    protected function getNullObject() : INull
    {
        return NullCrew::getInstance();
    }
    
    public function add(Crew $crew) : bool
    {
        return $this->addAction($crew);
    }

    public function edit(Crew $crew, array $keys = array()) : bool
    {
        return $this->editAction($crew, $keys);
    }

    public function fetchOne($id) : Crew
    {
        $crew = $this->fetchOneAction($id);

        $this->fetchUserGroup($crew);
        $this->fetchDepartment($crew);

        return $crew;
    }

    public function fetchList(array $ids) : array
    {
        $crewList = array();
        $crewList = $this->fetchListAction($ids);
        
        $this->fetchUserGroup($crewList);
        $this->fetchDepartment($crewList);

        return $crewList;
    }

    protected function fetchUserGroup($crew)
    {
        return is_array($crew) ?
        $this->fetchUserGroupByList($crew) :
        $this->fetchUserGroupByObject($crew);
    }

    protected function fetchUserGroupByObject($crew)
    {
        $userGroupId = $crew->getUserGroup()->getId();
        $userGroup = $this->getUserGroupRepository()->fetchOne($userGroupId);
        $crew->setUserGroup($userGroup);
    }

    protected function fetchUserGroupByList(array $crewList)
    {
        $userGroupIds = array();
        foreach ($crewList as $crew) {
            $userGroupIds[] = $crew->getUserGroup()->getId();
        }

        $userGroupList = $this->getUserGroupRepository()->fetchList($userGroupIds);
        if (!empty($userGroupList)) {
            foreach ($crewList as $crew) {
                if (isset($userGroupList[$crew->getUserGroup()->getId()])) {
                    $crew->setUserGroup($userGroupList[$crew->getUserGroup()->getId()]);
                }
            }
        }
    }

    protected function fetchDepartment($crew)
    {
        return is_array($crew) ?
        $this->fetchDepartmentByList($crew) :
        $this->fetchDepartmentByObject($crew);
    }

    protected function fetchDepartmentByObject($crew)
    {
        $departmentId = $crew->getDepartment()->getId();
        $department = $this->getDepartmentRepository()->fetchOne($departmentId);
        $crew->setDepartment($department);
    }

    protected function fetchDepartmentByList(array $crewList)
    {
        $departmentIds = array();
        foreach ($crewList as $crew) {
            $departmentIds[] = $crew->getDepartment()->getId();
        }

        $departmentList = $this->getDepartmentRepository()->fetchList($departmentIds);
        if (!empty($departmentList)) {
            foreach ($crewList as $crew) {
                if (isset($departmentList[$crew->getDepartment()->getId()])) {
                    $crew->setDepartment($departmentList[$crew->getDepartment()->getId()]);
                }
            }
        }
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $crew = new Crew();

            if (isset($filter['userGroup'])) {
                $crew->setUserGroup(new UserGroup($filter['userGroup']));
                $info = $this->getDbTranslator()->objectToArray($crew, array('userGroup'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['department'])) {
                $crew->setDepartment(new Department($filter['department']));
                $info = $this->getDbTranslator()->objectToArray($crew, array('department'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['cellphone'])) {
                $crew->setCellphone($filter['cellphone']);
                $info = $this->getDbTranslator()->objectToArray($crew, array('cellphone'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['userName'])) {
                $crew->setUserName($filter['userName']);
                $info = $this->getDbTranslator()->objectToArray($crew, array('userName'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['category'])) {
                $info = $this->getDbTranslator()->objectToArray($crew, array('category'));
                $condition .= $conjection.key($info).' IN ('.$filter['category'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['realName'])) {
                $crew->setRealName($filter['realName']);
                $info = $this->getDbTranslator()->objectToArray($crew, array('realName'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $crew->setStatus($filter['status']);
                $info = $this->getDbTranslator()->objectToArray($crew, array('status'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new Crew(), array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['status'])) {
                $info = $this->getDbTranslator()->objectToArray(new Crew(), array('status'));
                $condition .= $conjection.key($info).' '.($sort['status'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}
