<?php
namespace Base\Crew\Adapter\Crew;

use Base\Crew\Model\Crew;

interface ICrewAdapter
{
    public function fetchOne($id) : Crew;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Crew $crew) : bool;

    public function edit(Crew $crew, array $keys = array()) : bool;
}
