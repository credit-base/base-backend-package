<?php
namespace Base\Crew\Adapter\Crew;

use Base\Crew\Model\Crew;
use Base\Crew\Utils\MockFactory;

class CrewMockAdapter implements ICrewAdapter
{
    public function fetchOne($id) : Crew
    {
        return MockFactory::generateCrew($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $crewList = array();

        foreach ($ids as $id) {
            $crewList[$id] = MockFactory::generateCrew($id);
        }

        return $crewList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Crew $crew) : bool
    {
        unset($crew);
        return true;
    }

    public function edit(Crew $crew, array $keys = array()) : bool
    {
        unset($crew);
        unset($keys);
        return true;
    }
}
