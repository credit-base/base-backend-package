<?php
namespace Base\Crew\Adapter\Crew\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class CrewCache extends Cache
{
    const KEY = 'crew';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
