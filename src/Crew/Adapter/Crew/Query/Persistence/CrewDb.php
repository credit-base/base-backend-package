<?php
namespace Base\Crew\Adapter\Crew\Query\Persistence;

use Marmot\Framework\Classes\Db;

class CrewDb extends Db
{
    const TABLE = 'crew';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
