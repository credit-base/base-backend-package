<?php
namespace Base\Crew\Adapter\Crew\Query;

use Marmot\Framework\Query\RowCacheQuery;

class CrewRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'crew_id',
            new Persistence\CrewCache(),
            new Persistence\CrewDb()
        );
    }
}
