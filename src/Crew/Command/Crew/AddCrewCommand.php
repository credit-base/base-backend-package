<?php
namespace Base\Crew\Command\Crew;

use Base\Common\Command\AddCommand;

class AddCrewCommand extends AddCommand
{
    /**
     * @var int $category 用户类型
     */
    public $category;
    /**
     * @var string $realName 用户姓名
     */
    public $realName;
    /**
     * @var string $cellphone 手机号
     */
    public $cellphone;
    /**
     * @var string $cardId 身份证号
     */
    public $cardId;
    /**
     * @var string $password 密码
     */
    public $password;
    /**
     * @var array $purview 权限范围
     */
    public $purview;
    /**
     * @var int $userGroupId 所属委办局id
     */
    public $userGroupId;
    /**
     * @var int $departmentId 所属科室id
     */
    public $departmentId;

    public function __construct(
        int $category,
        string $realName,
        string $cellphone,
        string $cardId,
        string $password,
        array $purview,
        int $userGroupId = 0,
        int $departmentId = 0,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        
        $this->category = $category;
        $this->realName = $realName;
        $this->cellphone = $cellphone;
        $this->cardId = $cardId;
        $this->password = $password;
        $this->purview = $purview;
        $this->userGroupId = $userGroupId;
        $this->departmentId = $departmentId;
    }
}
