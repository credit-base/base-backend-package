<?php
namespace Base\Crew\Command\Crew;

use Marmot\Interfaces\ICommand;

class SignInCrewCommand implements ICommand
{
    /**
     * @var string $userName 账户
     */
    public $userName;
    /**
     * @var string $password 密码
     */
    public $password;
    /**
     * @var int $id id
     */
    public $id;

    public function __construct(
        string $userName,
        string $password,
        int $id = 0
    ) {
        $this->userName = $userName;
        $this->password = $password;
        $this->id = $id;
    }
}
