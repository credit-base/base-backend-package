<?php
namespace Base\Crew\Command\Crew;

use Base\Common\Command\EditCommand;

class EditCrewCommand extends EditCommand
{
    /**
     * @var int $category 用户类型
     */
    public $category;
    /**
     * @var string $realName 用户姓名
     */
    public $realName;
    /**
     * @var string $cardId 身份证号
     */
    public $cardId;
    /**
     * @var array $purview 权限范围
     */
    public $purview;
    /**
     * @var int $userGroupId 所属委办局id
     */
    public $userGroupId;
    /**
     * @var int $departmentId 所属科室id
     */
    public $departmentId;

    public function __construct(
        int $category,
        string $realName,
        string $cardId,
        array $purview,
        int $userGroupId,
        int $departmentId,
        int $id
    ) {
        parent::__construct(
            $id
        );
        
        $this->category = $category;
        $this->realName = $realName;
        $this->cardId = $cardId;
        $this->purview = $purview;
        $this->userGroupId = $userGroupId;
        $this->departmentId = $departmentId;
    }
}
