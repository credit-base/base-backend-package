<?php
namespace Base\Crew\WidgetRule;

use Respect\Validation\Validator as V;

use Marmot\Core;
use Base\Crew\Model\Crew;

class CrewWidgetRule
{
    public function category($category) : bool
    {
        if (!V::intVal()->validate($category) || !in_array($category, Crew::CATEGORY)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'category'));
            return false;
        }
        return true;
    }

    public function purview($purview) : bool
    {
        if (!is_array($purview) || empty($purview)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'purview'));
            return false;
        }

        foreach ($purview as $each) {
            if (!is_numeric($each)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'purview'));
                return false;
            }
        }

        return true;
    }
}
