<?php
namespace Base\Crew\CommandHandler\Crew;

use Base\Common\Model\IEnableAble;
use Base\Common\CommandHandler\DisableCommandHandler;

class DisableCrewCommandHandler extends DisableCommandHandler
{
    use CrewCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchCrew($id);
    }
}
