<?php
namespace Base\Crew\CommandHandler\Crew;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Crew\Model\Crew;
use Base\Crew\Command\Crew\AddCrewCommand;

class AddCrewCommandHandler implements ICommandHandler
{
    use CrewCommandHandlerTrait;
    
    protected function getCrew(int $category = 0) : Crew
    {
        return Crew::create($category);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddCrewCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->getCrew($command->category);

        $crew->setCategory($command->category);
        $crew->setRealName($command->realName);
        $crew->setCellphone($command->cellphone);
        $crew->setUserName($command->cellphone);
        $crew->setCardId($command->cardId);
        $crew->encryptPassword($command->password);
        $crew->setPurview($command->purview);
        $crew->setUserGroup($this->fetchUserGroup($command->userGroupId));
        $crew->setDepartment($this->fetchDepartment($command->departmentId));
    
        if ($crew->add()) {
            $command->id = $crew->getId();
            return true;
        }
        return false;
    }
}
