<?php
namespace Base\Crew\CommandHandler\Crew;

use Base\Common\Model\IEnableAble;
use Base\Common\CommandHandler\EnableCommandHandler;

class EnableCrewCommandHandler extends EnableCommandHandler
{
    use CrewCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchCrew($id);
    }
}
