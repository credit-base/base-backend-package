<?php
namespace Base\Crew\CommandHandler\Crew;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class CrewCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Crew\Command\Crew\AddCrewCommand' =>
        'Base\Crew\CommandHandler\Crew\AddCrewCommandHandler',
        'Base\Crew\Command\Crew\EditCrewCommand' =>
        'Base\Crew\CommandHandler\Crew\EditCrewCommandHandler',
        'Base\Crew\Command\Crew\SignInCrewCommand' =>
        'Base\Crew\CommandHandler\Crew\SignInCrewCommandHandler',
        'Base\Crew\Command\Crew\EnableCrewCommand' =>
        'Base\Crew\CommandHandler\Crew\EnableCrewCommandHandler',
        'Base\Crew\Command\Crew\DisableCrewCommand' =>
        'Base\Crew\CommandHandler\Crew\DisableCrewCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
