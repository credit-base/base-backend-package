<?php
namespace Base\Crew\CommandHandler\Crew;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;

trait CrewCommandHandlerTrait
{
    private $repository;

    private $userGroupRepository;

    private $departmentRepository;

    public function __construct()
    {
        $this->repository = new CrewRepository();
        $this->userGroupRepository = new UserGroupRepository();
        $this->departmentRepository = new DepartmentRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->userGroupRepository);
        unset($this->departmentRepository);
    }

    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function fetchUserGroup(int $id) : UserGroup
    {
        return $this->getUserGroupRepository()->fetchOne($id);
    }

    protected function getDepartmentRepository() : DepartmentRepository
    {
        return $this->departmentRepository;
    }

    protected function fetchDepartment(int $id) : Department
    {
        return $this->getDepartmentRepository()->fetchOne($id);
    }
}
