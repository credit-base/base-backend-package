<?php
namespace Base\Crew\CommandHandler\Crew;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Crew\Command\Crew\EditCrewCommand;

class EditCrewCommandHandler implements ICommandHandler
{
    use CrewCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof EditCrewCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->fetchCrew($command->id);

        $crew->setCategory($command->category);
        $crew->setRealName($command->realName);
        $crew->setCardId($command->cardId);
        $crew->setPurview($command->purview);
        $crew->setUserGroup($this->fetchUserGroup($command->userGroupId));
        $crew->setDepartment($this->fetchDepartment($command->departmentId));

        return $crew->edit();
    }
}
