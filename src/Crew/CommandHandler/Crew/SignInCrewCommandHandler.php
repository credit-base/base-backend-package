<?php
namespace Base\Crew\CommandHandler\Crew;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Crew\Model\Crew;
use Base\Crew\Command\Crew\SignInCrewCommand;

class SignInCrewCommandHandler implements ICommandHandler
{
    const SIGN_IN_SEARCH_COUNT = 1;

    use CrewCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof SignInCrewCommand)) {
            throw new \InvalidArgumentException;
        }

        list($crews, $count) = $this->searchCrews($command->userName);

        if ($this->isCrewExist($count)) {
            foreach ($crews as $crew) {
                $crew = $crew;
            }

            if ($this->isCrewDisabled($crew) && $this->crewPasswordCorrect($crew, $command)) {
                $command->id = $crew->getId();
                return true;
            }
        }

        return false;
    }

    protected function searchCrews($userName) : array
    {
        $filter = array();
        $filter['userName'] = $userName;

        list($crews, $count) = $this->getRepository()->filter($filter);

        return array($crews, $count);
    }

    protected function isCrewExist(int $count) : bool
    {
        if ($count != self::SIGN_IN_SEARCH_COUNT) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        return true;
    }

    protected function isCrewDisabled(Crew $crew) : bool
    {
        if ($crew->isDisabled()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'crewStatus'));
            return false;
        }

        return true;
    }

    protected function crewPasswordCorrect(Crew $crew, SignInCrewCommand $command) : bool
    {
        if (!empty($crew->getPassword())) {
            $password = $crew->getPassword();
            $crew->encryptPassword(
                $command->password,
                $crew->getSalt()
            );

            if ($password == $crew->getPassword()) {
                return true;
            }
        }
        
        Core::setLastError(PASSWORD_INCORRECT);
        return false;
    }
}
