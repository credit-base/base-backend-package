<?php
namespace Base\Crew\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Crew\Model\Crew;
use Base\Crew\Adapter\Crew\ICrewAdapter;
use Base\Crew\Adapter\Crew\CrewDbAdapter;
use Base\Crew\Adapter\Crew\CrewMockAdapter;

class CrewRepository extends Repository implements ICrewAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new CrewDbAdapter();
    }

    protected function getActualAdapter() : ICrewAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ICrewAdapter
    {
        return new CrewMockAdapter();
    }

    public function setAdapter(ICrewAdapter $adapter) : void
    {
        $this->adapter = $adapter;
    }

    public function fetchOne($id) : Crew
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Crew $crew) : bool
    {
        return $this->getAdapter()->add($crew);
    }

    public function edit(Crew $crew, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($crew, $keys);
    }
}
