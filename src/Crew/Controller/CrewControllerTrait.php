<?php
namespace Base\Crew\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\User\WidgetRule\UserWidgetRule;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Crew\WidgetRule\CrewWidgetRule;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\CommandHandler\Crew\CrewCommandHandlerFactory;

trait CrewControllerTrait
{
    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getUserWidgetRule() : UserWidgetRule
    {
        return new UserWidgetRule();
    }

    protected function getCrewWidgetRule() : CrewWidgetRule
    {
        return new CrewWidgetRule();
    }

    protected function getRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new CrewCommandHandlerFactory());
    }
}
