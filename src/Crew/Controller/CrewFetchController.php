<?php
namespace Base\Crew\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Crew\View\CrewView;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Adapter\Crew\ICrewAdapter;

class CrewFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CrewRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ICrewAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new CrewView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'crews';
    }
}
