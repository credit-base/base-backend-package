<?php
namespace Base\Crew\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Crew\Model\Crew;
use Base\Crew\View\CrewView;
use Base\Crew\Command\Crew\SignInCrewCommand;

class CrewController extends Controller
{
    use JsonApiTrait, CrewControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 员工登录功能,通过POST传参
     * 对应路由 /crews/signIn
     * @return jsonApi
     */
    public function signIn()
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];

        $userName = $attributes['userName'];
        $password = $attributes['password'];

        if ($this->validateSignInScenario(
            $userName,
            $password
        )) {
            $command = new SignInCrewCommand(
                $userName,
                $password
            );

            if ($this->getCommandBus()->send($command)) {
                $crew = $this->getRepository()->fetchOne($command->id);
                if ($crew instanceof Crew) {
                    $this->render(new CrewView($crew));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateSignInScenario(
        $userName,
        $password
    ) : bool {
        return $this->getUserWidgetRule()->cellphone($userName)
            && $this->getUserWidgetRule()->password($password, 'password');
    }
}
