<?php
namespace Base\Crew\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Common\Controller\Interfaces\IEnableAbleController;

use Base\Crew\Model\Crew;
use Base\Crew\View\CrewView;
use Base\Crew\Repository\CrewRepository;
use Base\Crew\Adapter\Crew\ICrewAdapter;
use Base\Crew\Command\Crew\EnableCrewCommand;
use Base\Crew\Command\Crew\DisableCrewCommand;
use Base\Crew\CommandHandler\Crew\CrewCommandHandlerFactory;

class CrewEnableController extends Controller implements IEnableAbleController
{
    use JsonApiTrait;

    private $repository;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new CrewRepository();
        $this->commandBus = new CommandBus(new CrewCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();

        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : ICrewAdapter
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * 对应路由 /crews/{id:\d+}/enable
     * 启用, 通过PATCH传参
     * @param int id 员工id
     * @return jsonApi
     */
    public function enable(int $id)
    {
        if (!empty($id)) {
            $command = new EnableCrewCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $crew  = $this->getRepository()->fetchOne($id);
                if ($crew instanceof Crew) {
                    $this->render(new CrewView($crew));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /crew/{id:\d+}/disable
     * 禁用, 通过PATCH传参
     * @param int id 员工id
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $command = new DisableCrewCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $crew  = $this->getRepository()->fetchOne($id);
                if ($crew instanceof Crew) {
                    $this->render(new CrewView($crew));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
