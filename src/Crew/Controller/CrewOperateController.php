<?php
namespace Base\Crew\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\Crew\Model\Crew;
use Base\Crew\View\CrewView;
use Base\Crew\Command\Crew\AddCrewCommand;
use Base\Crew\Command\Crew\EditCrewCommand;

class CrewOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, CrewControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 员工新增功能,通过POST传参
     * 对应路由 /crews
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $category = $attributes['category'];
        $realName = $attributes['realName'];
        $cellphone = $attributes['cellphone'];
        $cardId = $attributes['cardId'];
        $password = $attributes['password'];
        $purview = $attributes['purview'];

        $userGroupId = $relationships['userGroup']['data'][0]['id'];
        $departmentId = $relationships['department']['data'][0]['id'];

        if ($this->validateAddScenario(
            $category,
            $realName,
            $cellphone,
            $cardId,
            $password,
            $purview,
            $userGroupId,
            $departmentId
        )) {
            $command = new AddCrewCommand(
                $category,
                $realName,
                $cellphone,
                $cardId,
                $password,
                $purview,
                $userGroupId,
                $departmentId
            );

            if ($this->getCommandBus()->send($command)) {
                $crew = $this->getRepository()->fetchOne($command->id);
                if ($crew instanceof Crew) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new CrewView($crew));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateAddScenario(
        $category,
        $realName,
        $cellphone,
        $cardId,
        $password,
        $purview,
        $userGroupId,
        $departmentId
    ) : bool {
        if ($category == Crew::CATEGORY['PLATFORM_ADMINISTRATOR']) {
            return $this->validateAddCommonScenario(
                $category,
                $realName,
                $cellphone,
                $cardId,
                $password,
                $purview
            );
        }
        if ($category == Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'] || $category == Crew::CATEGORY['OPERATOR']) {
            return $this->validateAddCommonScenario($category, $realName, $cellphone, $cardId, $password, $purview)
                && $this->getCommonWidgetRule()->formatNumeric($userGroupId, 'userGroupId')
                && $this->getCommonWidgetRule()->formatNumeric($departmentId, 'departmentId');
        }
        return false;
    }

    protected function validateAddCommonScenario(
        $category,
        $realName,
        $cellphone,
        $cardId,
        $password,
        $purview
    ) : bool {
        return $this->getCrewWidgetRule()->category($category)
            && $this->getUserWidgetRule()->realName($realName)
            && $this->getUserWidgetRule()->cellphone($cellphone)
            && (empty($cardId) ? true : $this->getUserWidgetRule()->cardId($cardId))
            && $this->getUserWidgetRule()->password($password, 'password')
            && $this->getCrewWidgetRule()->purview($purview);
    }

    /**
     * 员工编辑功能,通过PATCH传参
     * 对应路由 /crews/{id:\d+}
     * @param int id 员工 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        
        $category = $attributes['category'];
        $realName = $attributes['realName'];
        $cardId = $attributes['cardId'];
        $purview = $attributes['purview'];

        $userGroupId = $relationships['userGroup']['data'][0]['id'];
        $departmentId = $relationships['department']['data'][0]['id'];

        if ($this->validateEditScenario(
            $category,
            $realName,
            $cardId,
            $purview,
            $userGroupId,
            $departmentId
        )) {
            $command = new EditCrewCommand(
                $category,
                $realName,
                $cardId,
                $purview,
                $userGroupId,
                $departmentId,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $crew = $this->getRepository()->fetchOne($id);
                if ($crew instanceof Crew) {
                    $this->render(new CrewView($crew));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateEditScenario(
        $category,
        $realName,
        $cardId,
        $purview,
        $userGroupId,
        $departmentId
    ) : bool {
        if ($category == Crew::CATEGORY['PLATFORM_ADMINISTRATOR']) {
            return $this->validateEditCommonScenario(
                $category,
                $realName,
                $cardId,
                $purview
            );
        }
        if ($category == Crew::CATEGORY['USER_GROUP_ADMINISTRATOR'] || $category == Crew::CATEGORY['OPERATOR']) {
            return $this->validateEditCommonScenario($category, $realName, $cardId, $purview)
                && $this->getCommonWidgetRule()->formatNumeric($userGroupId, 'userGroupId')
                && $this->getCommonWidgetRule()->formatNumeric($departmentId, 'departmentId');
        }
        return false;
    }

    protected function validateEditCommonScenario(
        $category,
        $realName,
        $cardId,
        $purview
    ) : bool {
        return $this->getCrewWidgetRule()->category($category)
            && $this->getUserWidgetRule()->realName($realName)
            && (empty($cardId) ? true : $this->getUserWidgetRule()->cardId($cardId))
            && $this->getCrewWidgetRule()->purview($purview);
    }
}
