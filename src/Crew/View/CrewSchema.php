<?php
namespace Base\Crew\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class CrewSchema extends SchemaProvider
{
    protected $resourceType = 'crews';

    public function getId($crew) : int
    {
        return $crew->getId();
    }

    public function getAttributes($crew) : array
    {
        return [
            'realName' => $crew->getRealName(),
            'cardId' => $crew->getCardId(),
            'userName' => $crew->getUserName(),
            'cellphone' => $crew->getCellphone(),
            'category' => $crew->getCategory(),
            'purview' => $crew->getPurview(),

            'status' => $crew->getStatus(),
            'createTime' => $crew->getCreateTime(),
            'updateTime' => $crew->getUpdateTime(),
            'statusTime' => $crew->getStatusTime(),
        ];
    }

    public function getRelationships($crew, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'userGroup' => [self::DATA => $crew->getUserGroup()],
            'department' => [self::DATA => $crew->getDepartment()]
        ];
    }
}
