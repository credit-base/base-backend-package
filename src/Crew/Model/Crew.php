<?php
namespace Base\Crew\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Common\Model\IOperate;
use Base\Common\Model\OperateTrait;

use Base\User\Model\User;
use Base\UserGroup\Model\UserGroup;
use Base\Department\Model\Department;

use Base\Crew\Adapter\Crew\ICrewAdapter;
use Base\Crew\Repository\CrewRepository;

class Crew extends User implements IOperate
{
    use OperateTrait;

    /**
     * @var CATEGORY['SUPER_ADMINISTRATOR']  超级管理员 1
     * @var CATEGORY['PLATFORM_ADMINISTRATOR']  平台管理员 2
     * @var CATEGORY['USER_GROUP_ADMINISTRATOR']  委办局管理员 3
     * @var CATEGORY['OPERATOR']  操作用户 4
     */
    const CATEGORY = array(
        'SUPER_ADMINISTRATOR' => 1,
        'PLATFORM_ADMINISTRATOR' => 2,
        'USER_GROUP_ADMINISTRATOR' => 3,
        'OPERATOR' => 4
    );

    const CATEGORY_MAPS = array(
        self::CATEGORY['SUPER_ADMINISTRATOR'] => 'Base\Crew\Model\SuperAdministrator',
        self::CATEGORY['PLATFORM_ADMINISTRATOR'] => 'Base\Crew\Model\PlatformAdministrator',
        self::CATEGORY['USER_GROUP_ADMINISTRATOR'] => 'Base\Crew\Model\UserGroupAdministrator',
        self::CATEGORY['OPERATOR'] => 'Base\Crew\Model\Operator',
    );

    const STATUS_NORMAL = 0;

    /**
     * @var string $realName 姓名
     */
    protected $realName;
    /**
     * @var UserGroup $userGroup 所属委办局
     */
    protected $userGroup;
    /**
     * @var Department $department 所属科室
     */
    protected $department;
    /**
     * @var string $cardId 身份证号码
     */
    protected $cardId;
    /**
     * @var int $category 员工类型
     */
    protected $category;
    /**
     * @var array $purview 权限范围
     */
    protected $purview;

    protected $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->realName = '';
        $this->userGroup = new UserGroup();
        $this->department = new Department();
        $this->cardId = '';
        $this->category = self::CATEGORY['OPERATOR'];
        $this->purview = array();
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->realName);
        unset($this->userGroup);
        unset($this->department);
        unset($this->cardId);
        unset($this->category);
        unset($this->purview);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setRealName(string $realName) : void
    {
        $this->realName = $realName;
    }

    public function getRealName() : string
    {
        return $this->realName;
    }
    
    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setDepartment(Department $department) : void
    {
        $this->department = $department;
    }

    public function getDepartment() : Department
    {
        return $this->department;
    }

    public function setCardId(string $cardId) : void
    {
        $this->cardId = $cardId;
    }

    public function getCardId() : string
    {
        return $this->cardId;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array($category, self::CATEGORY) ? $category : self::CATEGORY['OPERATOR'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setPurview(array $purview) : void
    {
        $this->purview = $purview;
    }

    public function getPurview() : array
    {
        return $this->purview;
    }

    protected function getRepository() : ICrewAdapter
    {
        return $this->repository;
    }

    protected function addAction() : bool
    {
        if (!$this->getDepartment() instanceof INull &&
            $this->getDepartment()->getUserGroup()->getId() != $this->getUserGroup()->getId()
        ) {
            Core::setLastError(DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP);
            return false;
        }

        if (!$this->getRepository()->add($this)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer' => 'cellphone'));
            return false;
        }
        return true;
    }

    protected function editAction() : bool
    {
        if (!$this->getDepartment() instanceof INull &&
            $this->getDepartment()->getUserGroup()->getId() != $this->getUserGroup()->getId()
        ) {
            Core::setLastError(DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP);
            return false;
        }
        
        if ($this->getCategory() == self::CATEGORY['PLATFORM_ADMINISTRATOR']) {
            $this->setUserGroup(new UserGroup(0));
            $this->setDepartment(new Department(0));
        }

        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'realName',
                'cardId',
                'userGroup',
                'department',
                'purview',
                'category',
                'updateTime'
            )
        );
    }

    public static function create(int $category = 0) : Crew
    {
        $crew = isset(self::CATEGORY_MAPS[$category]) ? self::CATEGORY_MAPS[$category] : '';

        return class_exists($crew) ? new $crew : NullCrew::getInstance();
    }

    protected function updatePassword(string $newPassword) : bool
    {
        unset($newPassword);
        return false;
    }

    protected function verifyPassword(string $oldPassword) : bool
    {
        unset($oldPassword);
        return false;
    }
}
