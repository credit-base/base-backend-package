<?php
namespace Base\Crew\Model;

use Marmot\Core;

use Base\UserGroup\Model\UserGroup;
use Base\Department\Model\Department;

class PlatformAdministrator extends Crew
{
    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setUserGroup(new UserGroup(0));
        $this->setDepartment(new Department(0));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'realName',
                'cardId',
                'updateTime',
                'purview',
                'userGroup',
                'department',
            )
        );
    }
}
