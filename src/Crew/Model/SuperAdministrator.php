<?php
namespace Base\Crew\Model;

use Marmot\Core;

class SuperAdministrator extends Crew
{
    protected function addAction() : bool
    {
        Core::setLastError(RESOURCE_CAN_NOT_MODIFY);
        return false;
    }

    protected function editAction() : bool
    {
        Core::setLastError(RESOURCE_CAN_NOT_MODIFY);
        return false;
    }
}
