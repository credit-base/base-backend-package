<?php
namespace Base\Crew\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;
use Base\Common\Model\NullEnableAbleTrait;

class NullCrew extends Crew implements INull
{
    use NullOperateTrait, NullEnableAbleTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
