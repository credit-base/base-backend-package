<?php
namespace Base\Crew\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Crew\Model\Crew;
use Base\Crew\Model\NullCrew;

use BaseSdk\Crew\Model\Crew as CrewSdk;
use BaseSdk\Crew\Model\NullCrew as NullCrewSdk;

class CrewSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($crewSdk = null, $crew = null)
    {
        return $this->translateToObject($crewSdk, $crew);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($crewSdk = null, $crew = null) : Crew
    {
        if (!$crewSdk instanceof CrewSdk || $crewSdk instanceof INull) {
            return NullCrew::getInstance();
        }

        if ($crew == null) {
            $crew = new Crew();
        }

        $crew->setId($crewSdk->getId());
        
        return $crew;
    }

    public function objectToValueObject($crew)
    {
        if (!$crew instanceof Crew) {
            return NullCrewSdk::getInstance();
        }

        $crewSdk = new CrewSdk(
            $crew->getId()
        );

        return $crewSdk;
    }
}
