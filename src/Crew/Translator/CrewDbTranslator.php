<?php
namespace Base\Crew\Translator;

use Marmot\Interfaces\ITranslator;

use Base\UserGroup\Model\UserGroup;
use Base\Department\Model\Department;

use Base\Crew\Model\Crew;
use Base\Crew\Model\NullCrew;

class CrewDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $crew = null) : Crew
    {
        if (!isset($expression['crew_id'])) {
            return NullCrew::getInstance();
        }

        if ($crew == null) {
            $crew = Crew::create($expression['category']);
        }
        $crew->setId($expression['crew_id']);
        $crew->setUserName($expression['user_name']);
        $crew->setCellphone($expression['cellphone']);
        $crew->setRealName($expression['real_name']);
        $crew->setPassword($expression['password']);
        $crew->setSalt($expression['salt']);
        $crew->setCardId($expression['card_id']);
        $crew->setCategory($expression['category']);
        $crew->setUserGroup(new UserGroup($expression['user_group_id']));
        $crew->setDepartment(new Department($expression['department_id']));
        if (isset($expression['purview'])) {
            $crew->setPurview(json_decode($expression['purview'], true));
        }

        $crew->setStatus($expression['status']);
        $crew->setCreateTime($expression['create_time']);
        $crew->setUpdateTime($expression['update_time']);
        $crew->setStatusTime($expression['status_time']);

        return $crew;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array()) : array
    {
        if (!$crew instanceof Crew) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'userName',
                'cellphone',
                'realName',
                'password',
                'salt',
                'cardId',
                'category',
                'userGroup',
                'department',
                'purview',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['crew_id'] = $crew->getId();
        }
        if (in_array('userName', $keys)) {
            $expression['user_name'] = $crew->getUserName();
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $crew->getCellphone();
        }
        if (in_array('realName', $keys)) {
            $expression['real_name'] = $crew->getRealName();
        }
        if (in_array('password', $keys)) {
            $expression['password'] = $crew->getPassword();
        }
        if (in_array('salt', $keys)) {
            $expression['salt'] = $crew->getSalt();
        }
        if (in_array('cardId', $keys)) {
            $expression['card_id'] = $crew->getCardId();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $crew->getCategory();
        }
        if (in_array('userGroup', $keys)) {
            $expression['user_group_id'] = $crew->getUserGroup()->getId();
        }
        if (in_array('department', $keys)) {
            $expression['department_id'] = $crew->getDepartment()->getId();
        }
        if (in_array('purview', $keys)) {
            $expression['purview'] = $crew->getPurview();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $crew->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $crew->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $crew->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $crew->getStatusTime();
        }

        return $expression;
    }
}
