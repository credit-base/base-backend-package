<?php
namespace Base\Rule\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Rule\Repository\RuleServiceSdkRepository;
use Base\Rule\Adapter\RuleService\IRuleServiceAdapter;

use Base\Common\Model\IOperate;
use Base\Common\Model\OperateTrait;

use Base\Crew\Model\Crew;

use Base\Template\Model\Template;

use Base\UserGroup\Model\UserGroup;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class RuleService implements IObject, IOperate
{
    const STATUS = array(
        'NORMAL' => 0, //正常
        'DELETED' => -2, //删除
    );

    const RULE_NAME = array(
        'TRANSFORMATION_RULE' => 'transformationRule', //转换规则
        'COMPLETION_RULE' => 'completionRule',//补全规则
        'COMPARISON_RULE' => 'comparisonRule',//比对规则
        'DE_DUPLICATION_RULE' => 'deDuplicationRule', //去重规则
        'FORMAT_VALIDATION_RULE' => 'formatValidationRule', //格式验证规则
    );
    
    const COMPLETION_BASE = array(
        'NAME' => 1, //企业名称
        'IDENTIFY' => 2, //统一社会信用代码/身份证号
    );
    
    const COMPARISON_BASE = array(
        'NAME' => 1, //企业名称
        'IDENTIFY' => 2, //统一社会信用代码/身份证号
    );

    const DE_DUPLICATION_RESULT = array(
        'DISCARD' => 1, //丢弃
        'COVER' => 2, //覆盖
    );

    use Object, OperateTrait;

    protected $id;
    /**
     * @var Template $transformationTemplate 目标资源目录
     */
    protected $transformationTemplate;

    /**
     * @var int $transformationCategory 目标资源目录类型
     */
    protected $transformationCategory;

    /**
     * @var Template $sourceTemplate 来源资源目录
     */
    protected $sourceTemplate;

    /**
     * @var int $sourceCategory 来源资源目录类型
     */
    protected $sourceCategory;

    /**
     * @var array $rules 规则集合
     */
    protected $rules;

    /**
     * @var int $version 版本号
     */
    protected $version;

    /**
     * @var int $dataTotal 已转换数据量
     */
    protected $dataTotal;

    /**
     * @var Crew $crew 发布人
     */
    protected $crew;

    /**
     * @var UserGroup $userGroup 来源委办局
     */
    protected $userGroup;

    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->sourceCategory = Template::CATEGORY['WBJ'];
        $this->sourceTemplate = new Template();
        $this->rules = array();
        $this->version = 0;
        $this->dataTotal = 0;
        $this->crew = new Crew();
        $this->userGroup = new UserGroup();
        $this->transformationCategory = Template::CATEGORY['BJ'];
        $this->transformationTemplate = new Template();
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new RuleServiceSdkRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->transformationCategory);
        unset($this->transformationTemplate);
        unset($this->sourceCategory);
        unset($this->sourceTemplate);
        unset($this->rules);
        unset($this->version);
        unset($this->dataTotal);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTransformationCategory(int $transformationCategory) : void
    {
        $this->transformationCategory = in_array($transformationCategory, Template::CATEGORY)
        ? $transformationCategory
        : Template::CATEGORY['BJ'];
    }

    public function getTransformationCategory() : int
    {
        return $this->transformationCategory;
    }

    public function setTransformationTemplate(Template $transformationTemplate)
    {
        $this->transformationTemplate = $transformationTemplate;
    }

    public function getTransformationTemplate() : Template
    {
        return $this->transformationTemplate;
    }

    public function setSourceCategory(int $sourceCategory) : void
    {
        $this->sourceCategory = in_array($sourceCategory, Template::CATEGORY)
        ? $sourceCategory
        : Template::CATEGORY['WBJ'];
    }

    public function getSourceCategory() : int
    {
        return $this->sourceCategory;
    }

    public function setSourceTemplate(Template $sourceTemplate)
    {
        $this->sourceTemplate = $sourceTemplate;
    }

    public function getSourceTemplate() : Template
    {
        return $this->sourceTemplate;
    }

    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    public function getRules() : array
    {
        return $this->rules;
    }

    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    public function getVersion() : int
    {
        return $this->version;
    }

    public function setDataTotal(int $dataTotal)
    {
        $this->dataTotal = $dataTotal;
    }

    public function getDataTotal() : int
    {
        return $this->dataTotal;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup)
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }

    protected function getRepository() : IRuleServiceAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        return $this->validate() && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->validate() && $this->getRepository()->edit(
            $this,
            array(
                'rules',
                'crew'
            )
        );
    }

    protected function validate() : bool
    {
        return $this->isCrewExist() &&
               $this->isSourceTemplateExist() &&
               $this->isTransformationTemplateExist() &&
               $this->isRuleNotExist();
    }

    //验证发布人不为空
    protected function isCrewExist() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crewId'));
            return false;
        }

        return true;
    }

    //验证来源资源目录不为空
    protected function isSourceTemplateExist() : bool
    {
        if ($this->getSourceTemplate() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'sourceTemplateId'));
            return false;
        }

        return true;
    }

    //验证目标资源目录不为空
    protected function isTransformationTemplateExist() : bool
    {
        if ($this->getTransformationTemplate() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'transformationTemplateId'));
            return false;
        }

        return true;
    }

    //验证规则是否存在
    protected function isRuleNotExist() : bool
    {
        $filter = array();
        $filter['transformationTemplate'] = $this->getTransformationTemplate()->getId();
        $filter['sourceTemplate'] = $this->getSourceTemplate()->getId();
        $filter['transformationCategory'] = $this->getTransformationCategory();
        $filter['sourceCategory'] = $this->getSourceCategory();
        $filter['status'] = self::STATUS['NORMAL'];
        if (!empty($this->getId())) {
            $filter['id'] = $this->getId();
        }

        list($ruleList, $count) = $this->getRepository()->filter($filter);
        unset($ruleList);
        
        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'rule'));
            return false;
        }

        return true;
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }

    public function delete() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->getRepository()->deletes($this, array('crew'));
    }
}
