<?php
namespace Base\Rule\Model;

use Marmot\Core;

use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;

use Base\Common\Model\IApproveAble;
use Base\Common\Model\IResubmitAble;
use Base\Common\Model\ApproveAbleTrait;
use Base\Common\Model\ResubmitAbleTrait;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

class UnAuditedRuleService extends RuleService implements IApproveAble, IResubmitAble
{
    use ApproveAbleTrait, ResubmitAbleTrait;

    protected $applyId;

    protected $publishCrew;

    protected $userGroup;

    protected $applyCrew;

    protected $applyUserGroup;

    protected $operationType;

    protected $relationId;

    protected $repository;
    
    public function __construct(int $applyId = 0)
    {
        parent::__construct();
        $this->applyId = !empty($applyId) ? $applyId : 0;
        $this->publishCrew = new Crew();
        $this->userGroup = new UserGroup();
        $this->applyCrew = new Crew();
        $this->applyUserGroup = new UserGroup();
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->relationId = 0;
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->rejectReason = '';
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->applyId);
        unset($this->publishCrew);
        unset($this->userGroup);
        unset($this->applyCrew);
        unset($this->applyUserGroup);
        unset($this->operationType);
        unset($this->relationId);
        unset($this->applyStatus);
        unset($this->rejectReason);
    }
    
    public function setApplyId($applyId) : void
    {
        $this->applyId = $applyId;
    }

    public function getApplyId()
    {
        return $this->applyId;
    }

    public function setPublishCrew(Crew $publishCrew) : void
    {
        $this->publishCrew = $publishCrew;
    }

    public function getPublishCrew() : Crew
    {
        return $this->publishCrew;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setApplyUserGroup(UserGroup $applyUserGroup) : void
    {
        $this->applyUserGroup = $applyUserGroup;
    }

    public function getApplyUserGroup() : UserGroup
    {
        return $this->applyUserGroup;
    }

    public function setOperationType(int $operationType) : void
    {
        $this->operationType = in_array($operationType, IApproveAble::OPERATION_TYPE)
        ? $operationType
        : IApproveAble::OPERATION_TYPE['NULL'];
    }

    public function getOperationType() : int
    {
        return $this->operationType;
    }

    public function setRelationId(int $relationId) : void
    {
        $this->relationId = $relationId;
    }

    public function getRelationId() : int
    {
        return $this->relationId;
    }

    protected function getUnAuditedRuleServiceSdkRepository() : UnAuditedRuleServiceSdkRepository
    {
        return new UnAuditedRuleServiceSdkRepository();
    }

    protected function resubmitAction() : bool
    {
        return $this->getUnAuditedRuleServiceSdkRepository()->resubmit(
            $this,
            array(
                'rules',
                'crew'
            )
        );
    }

    protected function approveAction() : bool
    {
        return $this->getUnAuditedRuleServiceSdkRepository()->approve($this, array('applyCrew'));
    }

    protected function rejectAction() : bool
    {
        return $this->getUnAuditedRuleServiceSdkRepository()->reject($this, array('applyCrew', 'rejectReason'));
    }

    public function revoke() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'applyStatus'));
            return false;
        }

        return $this->getUnAuditedRuleServiceSdkRepository()->revoke($this, array('crew'));
    }
}
