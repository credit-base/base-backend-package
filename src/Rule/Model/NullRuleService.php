<?php
namespace Base\Rule\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;

use Base\ResourceCatalogData\Model\ISearchDataAble;

class NullRuleService extends RuleService implements INull
{
    use NullOperateTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public function isNormal() : bool
    {
        return $this->resourceNotExist();
    }

    public function delete() : bool
    {
        return $this->resourceNotExist();
    }

    protected function validate() : bool
    {
        return $this->resourceNotExist();
    }

    protected function isCrewNull() : bool
    {
        return $this->resourceNotExist();
    }

    protected function isSourceTemplateNull() : bool
    {
        return $this->resourceNotExist();
    }

    protected function isTransformationTemplateNull() : bool
    {
        return $this->resourceNotExist();
    }

    protected function isRuleNotExist() : bool
    {
        return $this->resourceNotExist();
    }
}
