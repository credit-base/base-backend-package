<?php
namespace Base\Rule\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;
use Base\Common\Model\NullApproveAbleTrait;
use Base\Common\Model\NullResubmitAbleTrait;

class NullUnAuditedRuleService extends UnAuditedRuleService implements INull
{
    use NullOperateTrait,
        NullApproveAbleTrait,
        NullResubmitAbleTrait;

    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function revoke() : bool
    {
        return $this->resourceNotExist();
    }
}
