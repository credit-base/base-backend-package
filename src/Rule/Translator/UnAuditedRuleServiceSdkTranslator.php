<?php
namespace Base\Rule\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\Model\NullUnAuditedRuleService;

use BaseSdk\Rule\Model\UnAuditedRuleService as UnAuditedRuleServiceSdk;
use BaseSdk\Rule\Model\NullUnAuditedRuleService as NullUnAuditedRuleServiceSdk;

use Base\Crew\Translator\CrewSdkTranslator;
use Base\UserGroup\Translator\UserGroupSdkTranslator;

use Base\Template\Translator\TranslatorFactory;

class UnAuditedRuleServiceSdkTranslator extends RuleServiceSdkTranslator
{
    public function valueObjectToObject($unAuditedRuleServiceSdk = null, $unAuditedRuleService = null)
    {
        return $this->translateToObject($unAuditedRuleServiceSdk, $unAuditedRuleService);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($unAuditedRuleServiceSdk = null, $unAuditedRuleService = null)
    {
        if (!$unAuditedRuleServiceSdk instanceof UnAuditedRuleServiceSdk || $unAuditedRuleServiceSdk instanceof INull) {
            return NullUnAuditedRuleService::getInstance();
        }

        if ($unAuditedRuleService == null) {
            $unAuditedRuleService = new UnAuditedRuleService();
        }

        $applyCrew = $this->getCrewSdkTranslator()->valueObjectToObject($unAuditedRuleServiceSdk->getApplyCrew());

        $unAuditedRuleService = parent::translateToObject($unAuditedRuleServiceSdk, $unAuditedRuleService);
        $unAuditedRuleService->setApplyId($unAuditedRuleService->getId());
        $unAuditedRuleService->setApplyCrew($applyCrew);
        $unAuditedRuleService->setOperationType($unAuditedRuleServiceSdk->getOperationType());
        $unAuditedRuleService->setRelationId($unAuditedRuleServiceSdk->getRelationId());
        $unAuditedRuleService->setApplyStatus($unAuditedRuleServiceSdk->getApplyStatus());
        $unAuditedRuleService->setRejectReason($unAuditedRuleServiceSdk->getRejectReason());

        return $unAuditedRuleService;
    }

    public function objectToValueObject($unAuditedRuleService)
    {
        if (!$unAuditedRuleService instanceof UnAuditedRuleService) {
            return NullUnAuditedRuleServiceSdk::getInstance();
        }

        $ruleServiceSdk = parent::objectToValueObject($unAuditedRuleService);

        $applyCrewSdk = $this->getCrewSdkTranslator()->objectToValueObject($unAuditedRuleService->getApplyCrew());

        $unAuditedRuleServiceSdk = new UnAuditedRuleServiceSdk(
            $ruleServiceSdk->getId(),
            $ruleServiceSdk->getSourceCategory(),
            $ruleServiceSdk->getSourceTemplate(),
            $ruleServiceSdk->getTransformationCategory(),
            $ruleServiceSdk->getTransformationTemplate(),
            $ruleServiceSdk->getRules(),
            $ruleServiceSdk->getVersion(),
            $ruleServiceSdk->getDataTotal(),
            $ruleServiceSdk->getCrew(),
            $ruleServiceSdk->getUserGroup(),
            $applyCrewSdk,
            $unAuditedRuleService->getOperationType(),
            $unAuditedRuleService->getRelationId(),
            $unAuditedRuleService->getApplyStatus(),
            $unAuditedRuleService->getRejectReason(),
            $ruleServiceSdk->getStatus(),
            $ruleServiceSdk->getStatusTime(),
            $ruleServiceSdk->getCreateTime(),
            $ruleServiceSdk->getUpdateTime()
        );

        return $unAuditedRuleServiceSdk;
    }
}
