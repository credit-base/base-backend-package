<?php
namespace Base\Rule\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Rule\Model\RuleService;
use Base\Rule\Model\NullRuleService;

use BaseSdk\Rule\Model\RuleService as RuleServiceSdk;
use BaseSdk\Rule\Model\NullRuleService as NullRuleServiceSdk;

use Base\Crew\Translator\CrewSdkTranslator;
use Base\UserGroup\Translator\UserGroupSdkTranslator;

use Base\Template\Translator\TranslatorFactory;

class RuleServiceSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getCrewSdkTranslator() : ISdkTranslator
    {
        return new CrewSdkTranslator();
    }

    protected function getUserGroupSdkTranslator() : ISdkTranslator
    {
        return new UserGroupSdkTranslator();
    }

    protected function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    protected function getTemplateSdkTranslator(int $category) : ISdkTranslator
    {
        return $this->getTranslatorFactory()->getTranslator($category);
    }

    public function valueObjectToObject($ruleServiceSdk = null, $ruleService = null)
    {
        return $this->translateToObject($ruleServiceSdk, $ruleService);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($ruleServiceSdk = null, $ruleService = null)
    {
        if (!$ruleServiceSdk instanceof RuleServiceSdk || $ruleServiceSdk instanceof INull) {
            return NullRuleService::getInstance();
        }

        if ($ruleService == null) {
            $ruleService = new RuleService();
        }

        $crew = $this->getCrewSdkTranslator()->valueObjectToObject($ruleServiceSdk->getCrew());
        $userGroup = $this->getUserGroupSdkTranslator()->valueObjectToObject($ruleServiceSdk->getUserGroup());
        $sourceTemplate = $this->getTemplateSdkTranslator(
            $ruleServiceSdk->getSourceCategory()
        )->valueObjectToObject($ruleServiceSdk->getSourceTemplate());
        $transformationTemplate = $this->getTemplateSdkTranslator(
            $ruleServiceSdk->getTransformationCategory()
        )->valueObjectToObject($ruleServiceSdk->getTransformationTemplate());

        $ruleService->setId($ruleServiceSdk->getId());
        $ruleService->setSourceCategory($ruleServiceSdk->getSourceCategory());
        $ruleService->setSourceTemplate($sourceTemplate);
        $ruleService->setTransformationCategory($ruleServiceSdk->getTransformationCategory());
        $ruleService->setTransformationTemplate($transformationTemplate);
        $ruleService->setRules($ruleServiceSdk->getRules());
        $ruleService->setVersion($ruleServiceSdk->getVersion());
        $ruleService->setDataTotal($ruleServiceSdk->getDataTotal());
        $ruleService->setCrew($crew);
        $ruleService->setUserGroup($userGroup);
        $ruleService->setCreateTime($ruleServiceSdk->getCreateTime());
        $ruleService->setStatusTime($ruleServiceSdk->getStatusTime());
        $ruleService->setUpdateTime($ruleServiceSdk->getUpdateTime());
        $ruleService->setStatus($ruleServiceSdk->getStatus());

        return $ruleService;
    }

    public function objectToValueObject($ruleService)
    {
        if (!$ruleService instanceof RuleService) {
            return NullRuleServiceSdk::getInstance();
        }

        $crewSdk = $this->getCrewSdkTranslator()->objectToValueObject($ruleService->getCrew());

        $userGroupSdk = $this->getUserGroupSdkTranslator()->objectToValueObject($ruleService->getUserGroup());
        $sourceTemplateSdk = $this->getTemplateSdkTranslator(
            $ruleService->getSourceCategory()
        )->objectToValueObject($ruleService->getSourceTemplate());
        $transformationTemplateSdk = $this->getTemplateSdkTranslator(
            $ruleService->getTransformationCategory()
        )->objectToValueObject($ruleService->getTransformationTemplate());

        $ruleServiceSdk = new RuleServiceSdk(
            $ruleService->getId(),
            $ruleService->getSourceCategory(),
            $sourceTemplateSdk,
            $ruleService->getTransformationCategory(),
            $transformationTemplateSdk,
            $ruleService->getRules(),
            $ruleService->getVersion(),
            $ruleService->getDataTotal(),
            $crewSdk,
            $userGroupSdk,
            $ruleService->getStatus(),
            $ruleService->getStatusTime(),
            $ruleService->getCreateTime(),
            $ruleService->getUpdateTime()
        );

        return $ruleServiceSdk;
    }
}
