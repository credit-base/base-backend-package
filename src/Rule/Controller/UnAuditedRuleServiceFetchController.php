<?php
namespace Base\Rule\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\Rule\View\UnAuditedRuleServiceView;
use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;
use Base\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;

class UnAuditedRuleServiceFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditedRuleServiceSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IUnAuditedRuleServiceAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new UnAuditedRuleServiceView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'unAuditedRules';
    }
}
