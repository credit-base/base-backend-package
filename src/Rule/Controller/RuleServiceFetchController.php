<?php
namespace Base\Rule\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\Rule\View\RuleServiceView;
use Base\Rule\Repository\RuleServiceSdkRepository;
use Base\Rule\Adapter\RuleService\IRuleServiceAdapter;

class RuleServiceFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new RuleServiceSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IRuleServiceAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new RuleServiceView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'rules';
    }
}
