<?php
namespace Base\Rule\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Base\Common\Controller\Interfaces\IApproveAbleController;

use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\View\UnAuditedRuleServiceView;
use Base\Rule\Command\RuleService\RejectRuleServiceCommand;
use Base\Rule\Command\RuleService\ApproveRuleServiceCommand;

class RuleServiceApproveController extends Controller implements IApproveAbleController
{
    use JsonApiTrait, RuleServiceControllerTrait;

  /**
     * 申请信息审核通过功能,通过PATCH传参
     * 对应路由 /unAuditedRules/{id:\d+}/approve
     * @param int id 申请信息id
     * @return jsonApi
     */
    public function approve(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $relationships = $data['relationships'];

        $applyCrewId = $relationships['applyCrew']['data'][0]['id'];

        if ($this->validateApproveScenario($applyCrewId)) {
            $command = new ApproveRuleServiceCommand($applyCrewId, $id);

            if ($this->getCommandBus()->send($command)) {
                $unAuditedRuleService = $this->getUnAuditedRuleServiceSdkRepository()->fetchOne($command->id);
                
                if ($unAuditedRuleService instanceof UnAuditedRuleService) {
                    $this->render(new UnAuditedRuleServiceView($unAuditedRuleService));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 申请信息审核驳回功能,通过PATCH传参
     * 对应路由 /unAuditedRules/{id:\d+}/approve
     * @param int id 申请信息id
     *
     * @return jsonApi
     */
    public function reject(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $rejectReason = isset($attributes['rejectReason']) ? $attributes['rejectReason'] : array();
        $applyCrew = $relationships['applyCrew']['data'][0]['id'];

        if ($this->validateRejectScenario($rejectReason, $applyCrew)) {
            $command = new RejectRuleServiceCommand(
                $applyCrew,
                $rejectReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $unAuditedRuleService = $this->getUnAuditedRuleServiceSdkRepository()->fetchOne($command->id);
                
                if ($unAuditedRuleService instanceof UnAuditedRuleService) {
                    $this->render(new UnAuditedRuleServiceView($unAuditedRuleService));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
