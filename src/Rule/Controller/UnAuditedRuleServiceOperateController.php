<?php
namespace Base\Rule\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\View\UnAuditedRuleServiceView;

use Base\Rule\Command\RuleService\RevokeRuleServiceCommand;

class UnAuditedRuleServiceOperateController extends Controller
{
    use JsonApiTrait, RuleServiceControllerTrait;
    
    /**
     * 对应路由 /unAuditedRules/{id:\d+}/revoke
     * 撤销, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        if (!empty($id)) {
            $data = $this->getRequest()->patch('data');
            $relationships = $data['relationships'];

            $crew = $relationships['crew']['data'][0]['id'];

            if ($this->validateStatusScenario($crew)) {
                $command = new RevokeRuleServiceCommand($crew, $id);
    
                if ($this->getCommandBus()->send($command)) {
                    $ruleService  = $this->getUnAuditedRuleServiceSdkRepository()->fetchOne($id);
                    if ($ruleService instanceof UnAuditedRuleService) {
                        $this->render(new UnAuditedRuleServiceView($ruleService));
                        return true;
                    }
                }
            }
        }
        $this->displayError();
        return false;
    }
}
