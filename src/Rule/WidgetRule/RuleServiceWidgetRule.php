<?php
namespace Base\Rule\WidgetRule;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Base\Rule\Model\RuleService;

use Base\Template\Model\Template;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceWidgetRule
{
    const COMPLETION_RULE_MAX_COUNT = 2;
    const COMPARISON_RULE_MAX_COUNT = 2;

    const VALIDATE_RULES = [
        RuleService::RULE_NAME['TRANSFORMATION_RULE'] => 'validateTransformationRule',
        RuleService::RULE_NAME['COMPLETION_RULE'] => 'validateCompletionRules',
        RuleService::RULE_NAME['COMPARISON_RULE'] => 'validateComparisonRules',
        RuleService::RULE_NAME['DE_DUPLICATION_RULE'] => 'validateDeDuplicationRules',
    ];

    public function rules($rules) : bool
    {
        if (!V::arrayType()->validate($rules)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rules'));
            return false;
        }

        foreach ($rules as $key => $rule) {
            if (!$this->ruleName($key) || !$this->ruleFormat($rule) || !$this->validateRule($key, $rule)) {
                return false;
            }
        }

        return true;
    }

    //验证规则的键是否在范围内
    protected function ruleName(string $name) : bool
    {
        if (!in_array($name, RuleService::RULE_NAME)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rulesName'));
            return false;
        }

        return true;
    }

    protected function ruleFormat($rule) : bool
    {
        if (!V::arrayType()->validate($rule)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rules'));
            return false;
        }

        return true;
    }

    protected function validateRule(string $name, array $rule) : bool
    {
        $validateRule = isset(self::VALIDATE_RULES[$name]) ? self::VALIDATE_RULES[$name] : '';

        return method_exists($this, $validateRule) ? $this->$validateRule($rule) : false;
    }
    
    protected function validateTransformationRule(array $transformationRule) : bool
    {
        unset($transformationRule);
        return true;
    }

    protected function validateCompletionRules(array $completionRules) : bool
    {
        foreach ($completionRules as $completionRule) {
            if (count($completionRule) > self::COMPLETION_RULE_MAX_COUNT) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'completionRulesCount'));
                return false;
            }
            
            if (!V::arrayType()->validate($completionRule)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'completionRules'));
                return false;
            }

            foreach ($completionRule as $each) {
                if (!isset($each['id'])
                || !isset($each['base'])
                || !isset($each['item'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'completionRules'));
                    return false;
                }

                if (!is_numeric($each['id'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'completionRulesId'));
                    return false;
                }

                if (!is_array($each['base'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'completionRulesBase'));
                    return false;
                }

                if (!is_string($each['item'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'completionRulesItem'));
                    return false;
                }

                foreach ($each['base'] as $base) {
                    if (!in_array($base, RuleService::COMPLETION_BASE)) {
                        Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'completionRulesBase'));
                        return false;
                    }
                }
            }
        }

        return true;
    }

    protected function validateComparisonRules(array $comparisonRules) : bool
    {
        foreach ($comparisonRules as $comparisonRule) {
            if (count($comparisonRule) > self::COMPARISON_RULE_MAX_COUNT) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'comparisonRulesCount'));
                return false;
            }

            if (!V::arrayType()->validate($comparisonRule)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'comparisonRules'));
                return false;
            }

            foreach ($comparisonRule as $each) {
                if (!isset($each['id'])
                || !isset($each['base'])
                || !isset($each['item'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'comparisonRules'));
                    return false;
                }

                if (!is_numeric($each['id'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'comparisonRulesId'));
                    return false;
                }

                if (!is_array($each['base'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'comparisonRulesBase'));
                    return false;
                }

                if (!is_string($each['item'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'comparisonRulesItem'));
                    return false;
                }

                foreach ($each['base'] as $base) {
                    if (!in_array($base, RuleService::COMPLETION_BASE)) {
                        Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'comparisonRulesBase'));
                        return false;
                    }
                }
            }
        }

        return true;
    }

    protected function validateDeDuplicationRules(array $deDuplicationRule) : bool
    {
        if (!isset($deDuplicationRule['result'])
        || !isset($deDuplicationRule['items'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'deDuplicationRules'));
            return false;
        }

        if (!is_array($deDuplicationRule['items'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'deDuplicationRulesItems'));
            return false;
        }

        if (!in_array($deDuplicationRule['result'], RuleService::DE_DUPLICATION_RESULT)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'deDuplicationRulesResult'));
            return false;
        }

        return true;
    }

    public function category($category, $pointer = 'transformationCategory') : bool
    {
        if (!V::intVal()->validate($category) || !in_array($category, Template::CATEGORY)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }
        return true;
    }
}
