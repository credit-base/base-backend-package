<?php
namespace Base\Rule\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;
use Base\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceSdkAdapter;
use Base\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceMockAdapter;

class UnAuditedRuleServiceSdkRepository extends Repository implements IUnAuditedRuleServiceAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new UnAuditedRuleServiceSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IUnAuditedRuleServiceAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IUnAuditedRuleServiceAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IUnAuditedRuleServiceAdapter
    {
        return new UnAuditedRuleServiceMockAdapter();
    }

    public function fetchOne($id) : UnAuditedRuleService
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function resubmit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->resubmit($unAuditedRuleService, $keys);
    }
    
    public function approve(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->approve($unAuditedRuleService, $keys);
    }

    public function reject(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->reject($unAuditedRuleService, $keys);
    }

    public function revoke(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->revoke($unAuditedRuleService, $keys);
    }
}
