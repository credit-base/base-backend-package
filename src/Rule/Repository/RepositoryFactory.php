<?php
namespace Base\Rule\Repository;

use Base\Template\Model\Template;

use Base\Common\Repository\NullRepository;

class RepositoryFactory
{
    const MAPS = array(
        Template::CATEGORY['BJ'] =>
        'Base\Template\Repository\BjTemplateSdkRepository',
        Template::CATEGORY['GB'] =>
        'Base\Template\Repository\GbTemplateSdkRepository',
        Template::CATEGORY['BASE'] =>
        'Base\Template\Repository\BaseTemplateSdkRepository',
        Template::CATEGORY['WBJ'] =>
        'Base\Template\Repository\WbjTemplateSdkRepository',
    );

    public function getRepository(int $category)
    {
        $repository = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($repository) ? new $repository : NullRepository::getInstance();
    }
}
