<?php
namespace Base\Rule\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Rule\Model\RuleService;
use Base\Rule\Adapter\RuleService\IRuleServiceAdapter;
use Base\Rule\Adapter\RuleService\RuleServiceSdkAdapter;
use Base\Rule\Adapter\RuleService\RuleServiceMockAdapter;

class RuleServiceSdkRepository extends Repository implements IRuleServiceAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new RuleServiceSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IRuleServiceAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IRuleServiceAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IRuleServiceAdapter
    {
        return new RuleServiceMockAdapter();
    }

    public function fetchOne($id) : RuleService
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(RuleService $ruleService) : bool
    {
        return $this->getAdapter()->add($ruleService);
    }

    public function edit(RuleService $ruleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($ruleService, $keys);
    }

    public function deletes(RuleService $ruleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->deletes($ruleService, $keys);
    }
}
