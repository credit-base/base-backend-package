<?php
namespace Base\Rule\Adapter\RuleService;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\Rule\Model\RuleService;
use Base\Rule\Model\NullRuleService;
use Base\Rule\Translator\RuleServiceSdkTranslator;

use BaseSdk\Rule\Repository\RuleServiceRestfulRepository;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

class RuleServiceSdkAdapter implements IRuleServiceAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    private $crewRepository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->translator = new RuleServiceSdkTranslator();
        $this->repository = new RuleServiceRestfulRepository();
        $this->crewRepository = new CrewRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
        unset($this->crewRepository);
        unset($this->userGroupRepository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : RuleServiceRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullRuleService::getInstance();
    }

    public function fetchOne($id) : RuleService
    {
        $ruleService = $this->fetchOneAction($id);
        $this->fetchCrew($ruleService);
        $this->fetchUserGroup($ruleService);

        return $ruleService;
    }

    public function fetchList(array $ids) : array
    {
        $ruleServiceList = $this->fetchListAction($ids);
        $this->fetchCrew($ruleServiceList);
        $this->fetchUserGroup($ruleServiceList);

        return $ruleServiceList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        list($ruleServiceList, $count) = $this->filterAction($filter, $sort, $number, $size);
        $this->fetchUserGroup($ruleServiceList);
        $this->fetchCrew($ruleServiceList);
        
        return [$ruleServiceList, $count];
    }

    public function add(RuleService $ruleService) : bool
    {
        return $this->addAction($ruleService);
    }

    public function edit(RuleService $ruleService, array $keys = array()) : bool
    {
        return $this->editAction($ruleService, $keys);
    }

    public function deletes(RuleService $ruleService, array $keys = array()) : bool
    {
        $ruleServiceSdk = $this->getTranslator()->objectToValueObject($ruleService, $keys);

        $result = $this->getRestfulRepository()->deletes($ruleServiceSdk, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        $ruleService->setId($ruleServiceSdk->getId());
        return true;
    }

    protected function fetchUserGroup($ruleService)
    {
        return is_array($ruleService) ?
        $this->fetchUserGroupByList($ruleService) :
        $this->fetchUserGroupByObject($ruleService);
    }

    protected function fetchUserGroupByObject(RuleService $ruleService)
    {
        $userGroupId = $ruleService->getUserGroup()->getId();
        $userGroup = $this->getUserGroupRepository()->fetchOne($userGroupId);
        $ruleService->setUserGroup($userGroup);
    }

    protected function fetchUserGroupByList(array $ruleServiceList)
    {
        $userGroupIds = array();
        foreach ($ruleServiceList as $ruleService) {
            $userGroupIds[] = $ruleService->getUserGroup()->getId();
        }

        $userGroupList = $this->getUserGroupRepository()->fetchList($userGroupIds);
        if (!empty($userGroupList)) {
            foreach ($ruleServiceList as $ruleService) {
                $userGroupId = $ruleService->getUserGroup()->getId();
                if (isset($userGroupList[$userGroupId])
                    && ($userGroupList[$userGroupId]->getId() == $userGroupId)) {
                    $ruleService->setUserGroup($userGroupList[$userGroupId]);
                }
            }
        }
    }

    protected function fetchCrew($ruleService)
    {
        return is_array($ruleService) ?
        $this->fetchCrewByList($ruleService) :
        $this->fetchCrewByObject($ruleService);
    }

    protected function fetchCrewByObject(RuleService $ruleService)
    {
        $crewId = $ruleService->getCrew()->getId();
        $crew = $this->getCrewRepository()->fetchOne($crewId);
        $ruleService->setCrew($crew);
    }

    protected function fetchCrewByList(array $ruleServiceList)
    {
        $crewIds = array();
        foreach ($ruleServiceList as $ruleService) {
            $crewIds[] = $ruleService->getCrew()->getId();
        }

        $crewList = $this->getCrewRepository()->fetchList($crewIds);
        if (!empty($crewList)) {
            foreach ($ruleServiceList as $ruleService) {
                $crewId = $ruleService->getCrew()->getId();
                if (isset($crewList[$crewId])
                    && ($crewList[$crewId]->getId() == $crewId)) {
                    $ruleService->setCrew($crewList[$crewId]);
                }
            }
        }
    }
}
