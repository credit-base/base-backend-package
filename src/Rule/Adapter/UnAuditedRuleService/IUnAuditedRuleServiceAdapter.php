<?php
namespace Base\Rule\Adapter\UnAuditedRuleService;

use Base\Rule\Model\UnAuditedRuleService;

interface IUnAuditedRuleServiceAdapter
{
    public function fetchOne($id) : UnAuditedRuleService;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function resubmit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool;

    public function approve(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool;

    public function reject(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool;

    public function revoke(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool;
}
