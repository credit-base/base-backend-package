<?php
namespace Base\Rule\Adapter\UnAuditedRuleService;

use Base\Rule\Utils\MockFactory;
use Base\Rule\Model\UnAuditedRuleService;

class UnAuditedRuleServiceMockAdapter implements IUnAuditedRuleServiceAdapter
{
    public function fetchOne($id) : UnAuditedRuleService
    {
        return MockFactory::generateUnAuditedRuleService($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedRuleServiceList = array();

        foreach ($ids as $id) {
            $unAuditedRuleServiceList[$id] = MockFactory::generateUnAuditedRuleService($id);
        }

        return $unAuditedRuleServiceList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function resubmit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        unset($unAuditedRuleService);
        unset($keys);
        return true;
    }

    public function approve(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        unset($unAuditedRuleService);
        unset($keys);
        return true;
    }

    public function reject(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        unset($unAuditedRuleService);
        unset($keys);
        return true;
    }

    public function revoke(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        unset($unAuditedRuleService);
        unset($keys);
        return true;
    }
}
