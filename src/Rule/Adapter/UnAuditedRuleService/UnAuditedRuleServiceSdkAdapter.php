<?php
namespace Base\Rule\Adapter\UnAuditedRuleService;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\Model\NullUnAuditedRuleService;
use Base\Rule\Translator\UnAuditedRuleServiceSdkTranslator;

use BaseSdk\Rule\Repository\UnAuditedRuleServiceRestfulRepository;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

class UnAuditedRuleServiceSdkAdapter implements IUnAuditedRuleServiceAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    private $crewRepository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->translator = new UnAuditedRuleServiceSdkTranslator();
        $this->repository = new UnAuditedRuleServiceRestfulRepository();
        $this->crewRepository = new CrewRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
        unset($this->crewRepository);
        unset($this->userGroupRepository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : UnAuditedRuleServiceRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullUnAuditedRuleService::getInstance();
    }

    public function fetchOne($id) : UnAuditedRuleService
    {
        $unAuditedRuleService = $this->fetchOneAction($id);
        $this->fetchCrew($unAuditedRuleService);
        $this->fetchApplyCrew($unAuditedRuleService);
        $this->fetchUserGroup($unAuditedRuleService);

        return $unAuditedRuleService;
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedRuleServiceList = $this->fetchListAction($ids);
        $this->fetchCrew($unAuditedRuleServiceList);
        $this->fetchApplyCrew($unAuditedRuleServiceList);
        $this->fetchUserGroup($unAuditedRuleServiceList);

        return $unAuditedRuleServiceList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        list($unAuditedRuleServiceList, $count) = $this->filterAction($filter, $sort, $number, $size);
        $this->fetchUserGroup($unAuditedRuleServiceList);
        $this->fetchCrew($unAuditedRuleServiceList);
        $this->fetchApplyCrew($unAuditedRuleServiceList);
        
        return [$unAuditedRuleServiceList, $count];
    }

    public function resubmit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        $unAuditedRuleServiceSdk = $this->getTranslator()->objectToValueObject($unAuditedRuleService, $keys);

        $result = $this->getRestfulRepository()->resubmit($unAuditedRuleServiceSdk, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        $unAuditedRuleService->setId($unAuditedRuleServiceSdk->getId());
        return true;
    }

    public function approve(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        $unAuditedRuleServiceSdk = $this->getTranslator()->objectToValueObject($unAuditedRuleService, $keys);

        $result = $this->getRestfulRepository()->approve($unAuditedRuleServiceSdk, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        $unAuditedRuleService->setId($unAuditedRuleServiceSdk->getId());
        return true;
    }

    public function reject(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        $unAuditedRuleServiceSdk = $this->getTranslator()->objectToValueObject($unAuditedRuleService, $keys);

        $result = $this->getRestfulRepository()->reject($unAuditedRuleServiceSdk, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        $unAuditedRuleService->setId($unAuditedRuleServiceSdk->getId());
        return true;
    }

    public function revoke(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        $unAuditedRuleServiceSdk = $this->getTranslator()->objectToValueObject($unAuditedRuleService, $keys);

        $result = $this->getRestfulRepository()->revoke($unAuditedRuleServiceSdk, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        $unAuditedRuleService->setId($unAuditedRuleServiceSdk->getId());
        return true;
    }

    protected function fetchUserGroup($unAuditedRuleService)
    {
        return is_array($unAuditedRuleService) ?
        $this->fetchUserGroupByList($unAuditedRuleService) :
        $this->fetchUserGroupByObject($unAuditedRuleService);
    }

    protected function fetchUserGroupByObject(UnAuditedRuleService $unAuditedRuleService)
    {
        $userGroupId = $unAuditedRuleService->getUserGroup()->getId();
        $userGroup = $this->getUserGroupRepository()->fetchOne($userGroupId);
        $unAuditedRuleService->setUserGroup($userGroup);
    }

    protected function fetchUserGroupByList(array $unAuditedRuleServiceList)
    {
        $userGroupIds = array();
        foreach ($unAuditedRuleServiceList as $unAuditedRuleService) {
            $userGroupIds[] = $unAuditedRuleService->getUserGroup()->getId();
        }

        $userGroupList = $this->getUserGroupRepository()->fetchList($userGroupIds);
        if (!empty($userGroupList)) {
            foreach ($unAuditedRuleServiceList as $unAuditedRuleService) {
                $userGroupId = $unAuditedRuleService->getUserGroup()->getId();
                if (isset($userGroupList[$userGroupId])
                    && ($userGroupList[$userGroupId]->getId() == $userGroupId)) {
                    $unAuditedRuleService->setUserGroup($userGroupList[$userGroupId]);
                }
            }
        }
    }

    protected function fetchCrew($unAuditedRuleService)
    {
        return is_array($unAuditedRuleService) ?
        $this->fetchCrewByList($unAuditedRuleService) :
        $this->fetchCrewByObject($unAuditedRuleService);
    }

    protected function fetchCrewByObject(UnAuditedRuleService $unAuditedRuleService)
    {
        $crewId = $unAuditedRuleService->getCrew()->getId();
        $crew = $this->getCrewRepository()->fetchOne($crewId);
        $unAuditedRuleService->setCrew($crew);
        $unAuditedRuleService->setPublishCrew($crew);
    }

    protected function fetchCrewByList(array $unAuditedRuleServiceList)
    {
        $crewIds = array();
        foreach ($unAuditedRuleServiceList as $unAuditedRuleService) {
            $crewIds[] = $unAuditedRuleService->getCrew()->getId();
        }

        $crewList = $this->getCrewRepository()->fetchList($crewIds);
        if (!empty($crewList)) {
            foreach ($unAuditedRuleServiceList as $unAuditedRuleService) {
                $crewId = $unAuditedRuleService->getCrew()->getId();
                if (isset($crewList[$crewId])
                    && ($crewList[$crewId]->getId() == $crewId)) {
                    $unAuditedRuleService->setCrew($crewList[$crewId]);
                    $unAuditedRuleService->setPublishCrew($crewList[$crewId]);
                }
            }
        }
    }

    protected function fetchApplyCrew($unAuditedRuleService)
    {
        return is_array($unAuditedRuleService) ?
        $this->fetchApplyCrewByList($unAuditedRuleService) :
        $this->fetchApplyCrewByObject($unAuditedRuleService);
    }

    protected function fetchApplyCrewByObject(UnAuditedRuleService $unAuditedRuleService)
    {
        $applyCrewId = $unAuditedRuleService->getApplyCrew()->getId();
        $applyCrew = $this->getCrewRepository()->fetchOne($applyCrewId);
        $unAuditedRuleService->setApplyCrew($applyCrew);
        $unAuditedRuleService->setApplyUserGroup($applyCrew->getUserGroup());
    }

    protected function fetchApplyCrewByList(array $unAuditedRuleServiceList)
    {
        $applyCrewIds = array();
        foreach ($unAuditedRuleServiceList as $unAuditedRuleService) {
            $applyCrewIds[] = $unAuditedRuleService->getApplyCrew()->getId();
        }

        $applyCrewList = $this->getCrewRepository()->fetchList($applyCrewIds);
        if (!empty($applyCrewList)) {
            foreach ($unAuditedRuleServiceList as $unAuditedRuleService) {
                $applyCrewId = $unAuditedRuleService->getApplyCrew()->getId();
                if (isset($applyCrewList[$applyCrewId])
                    && ($applyCrewList[$applyCrewId]->getId() == $applyCrewId)) {
                    $applyCrew = $applyCrewList[$applyCrewId];
                    $unAuditedRuleService->setApplyCrew($applyCrew);
                    $unAuditedRuleService->setApplyUserGroup($applyCrew->getUserGroup());
                }
            }
        }
    }
}
