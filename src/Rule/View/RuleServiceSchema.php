<?php
namespace Base\Rule\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

use Base\Rule\Model\IRule;

class RuleServiceSchema extends SchemaProvider
{
    protected $resourceType = 'rules';

    public function getId($ruleService) : int
    {
        return $ruleService->getId();
    }

    public function getAttributes($ruleService) : array
    {
        return [
            'rules'  => $ruleService->getRules(),
            'version'  => $ruleService->getVersion(),
            'dataTotal' => $ruleService->getDataTotal(),
            'transformationCategory'  => $ruleService->getTransformationCategory(),
            'sourceCategory'  => $ruleService->getSourceCategory(),
            'status' => $ruleService->getStatus(),
            'createTime' => $ruleService->getCreateTime(),
            'updateTime' => $ruleService->getUpdateTime(),
            'statusTime' => $ruleService->getStatusTime(),
        ];
    }

    public function getRelationships($ruleService, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'transformationTemplate' => [self::DATA => $ruleService->getTransformationTemplate()],
            'sourceTemplate' => [self::DATA => $ruleService->getSourceTemplate()],
            'userGroup' => [self::DATA => $ruleService->getUserGroup()],
            'crew' => [self::DATA => $ruleService->getCrew()]
        ];
    }
}
