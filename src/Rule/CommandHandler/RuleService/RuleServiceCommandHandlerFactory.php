<?php
namespace Base\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class RuleServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Rule\Command\RuleService\AddRuleServiceCommand' =>
        'Base\Rule\CommandHandler\RuleService\AddRuleServiceCommandHandler',
        'Base\Rule\Command\RuleService\EditRuleServiceCommand' =>
        'Base\Rule\CommandHandler\RuleService\EditRuleServiceCommandHandler',
        'Base\Rule\Command\RuleService\DeleteRuleServiceCommand' =>
        'Base\Rule\CommandHandler\RuleService\DeleteRuleServiceCommandHandler',
        'Base\Rule\Command\RuleService\RevokeRuleServiceCommand' =>
        'Base\Rule\CommandHandler\RuleService\RevokeRuleServiceCommandHandler',
        'Base\Rule\Command\RuleService\ResubmitRuleServiceCommand' =>
        'Base\Rule\CommandHandler\RuleService\ResubmitRuleServiceCommandHandler',
        'Base\Rule\Command\RuleService\ApproveRuleServiceCommand' =>
        'Base\Rule\CommandHandler\RuleService\ApproveRuleServiceCommandHandler',
        'Base\Rule\Command\RuleService\RejectRuleServiceCommand' =>
        'Base\Rule\CommandHandler\RuleService\RejectRuleServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
