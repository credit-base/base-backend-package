<?php
namespace Base\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Rule\Command\RuleService\AddRuleServiceCommand;

class AddRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $sourceRepository = $this->getRepository($command->sourceCategory);
        $sourceTemplate = $sourceRepository->fetchOne($command->sourceTemplate);

        $transformationRepository = $this->getRepository($command->transformationCategory);
        $transformationTemplate = $transformationRepository->fetchOne($command->transformationTemplate);

        $crew = $this->fetchCrew($command->crew);

        $ruleService = $this->getRuleService();

        $ruleService->setTransformationTemplate($transformationTemplate);
        $ruleService->setSourceTemplate($sourceTemplate);
        $ruleService->setTransformationCategory($command->transformationCategory);
        $ruleService->setSourceCategory($command->sourceCategory);
        $ruleService->setCrew($crew);
        $ruleService->setRules($command->rules);

        if ($ruleService->add()) {
            $command->id = $ruleService->getId();
            return true;
        }
        
        return false;
    }
}
