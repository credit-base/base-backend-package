<?php
namespace Base\Rule\CommandHandler\RuleService;

use Base\Common\Model\IApproveAble;
use Base\Common\Command\RejectCommand;
use Base\Common\CommandHandler\RejectCommandHandler;

class RejectRuleServiceCommandHandler extends RejectCommandHandler
{
    use RuleServiceCommandHandlerTrait;

    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchUnAuditedRuleService($id);
    }

    protected function executeAction(RejectCommand $command)
    {
        $this->rejectAble = $this->fetchIApplyObject($command->id);

        $this->rejectAble->setRejectReason($command->rejectReason);
        $applyCrew = $this->fetchCrew($command->applyCrew);
        $this->rejectAble->setApplyCrew($applyCrew);
        
        return $this->rejectAble->reject();
    }
}
