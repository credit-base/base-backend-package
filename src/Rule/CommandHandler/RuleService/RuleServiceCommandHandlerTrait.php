<?php
namespace Base\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Repository;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\Rule\Model\RuleService;
use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\Repository\RepositoryFactory;
use Base\Rule\Repository\RuleServiceSdkRepository;
use Base\Rule\Repository\UnAuditedRuleServiceSdkRepository;

trait RuleServiceCommandHandlerTrait
{
    protected function getCrewRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    protected function getRuleServiceSdkRepository() : RuleServiceSdkRepository
    {
        return new RuleServiceSdkRepository();
    }

    protected function getRuleService() : RuleService
    {
        return new RuleService();
    }

    protected function fetchRuleService(int $id) : RuleService
    {
        return $this->getRuleServiceSdkRepository()->fetchOne($id);
    }

    protected function getUnAuditedRuleServiceSdkRepository() : UnAuditedRuleServiceSdkRepository
    {
        return new UnAuditedRuleServiceSdkRepository();
    }

    protected function fetchUnAuditedRuleService(int $id) : UnAuditedRuleService
    {
        return $this->getUnAuditedRuleServiceSdkRepository()->fetchOne($id);
    }
    
    protected function getUnAuditedRuleService() : UnAuditedRuleService
    {
        return new UnAuditedRuleService();
    }

    protected function getRepositoryFactory() : RepositoryFactory
    {
        return new RepositoryFactory();
    }

    protected function getRepository(int $category) : Repository
    {
        return $this->getRepositoryFactory()->getRepository($category);
    }
}
