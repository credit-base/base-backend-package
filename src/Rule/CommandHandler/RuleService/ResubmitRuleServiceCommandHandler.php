<?php
namespace Base\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Rule\Command\RuleService\ResubmitRuleServiceCommand;

class ResubmitRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->fetchCrew($command->crew);

        $unAuditedRuleService = $this->fetchUnAuditedRuleService($command->id);

        $unAuditedRuleService->setCrew($crew);
        $unAuditedRuleService->setRules($command->rules);

        return $unAuditedRuleService->resubmit();
    }
}
