<?php
namespace Base\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Common\Model\IApproveAble;
use Base\Common\Command\ApproveCommand;
use Base\Common\CommandHandler\ApproveCommandHandler;

use Base\Rule\Model\UnAuditedRuleService;
use Base\Rule\Command\RuleService\ApproveRuleServiceCommand;

class ApproveRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ApproveRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $applyCrew = $this->fetchCrew($command->applyCrew);

        $unAuditedRuleService = $this->fetchUnAuditedRuleService($command->id);
        $unAuditedRuleService->setApplyCrew($applyCrew);
        
        return $unAuditedRuleService->approve();
    }
}
