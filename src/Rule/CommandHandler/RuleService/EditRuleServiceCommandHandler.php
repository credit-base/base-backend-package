<?php
namespace Base\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Rule\Command\RuleService\EditRuleServiceCommand;

class EditRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->fetchCrew($command->crew);

        $ruleService = $this->fetchRuleService($command->id);

        $ruleService->setCrew($crew);
        $ruleService->setRules($command->rules);

        if ($ruleService->edit()) {
            $command->id = $ruleService->getId();
            return true;
        }

        return false;
    }
}
