<?php
namespace Base\Rule\Command\RuleService;

use Marmot\Interfaces\ICommand;

class AddRuleServiceCommand implements ICommand
{
    public $rules;

    public $transformationTemplate;

    public $sourceTemplate;

    public $transformationCategory;

    public $sourceCategory;

    public $crew;

    public $id;

    public function __construct(
        array $rules,
        int $transformationTemplate,
        int $sourceTemplate,
        int $transformationCategory,
        int $sourceCategory,
        int $crew,
        int $id = 0
    ) {
        $this->rules = $rules;
        $this->transformationTemplate = $transformationTemplate;
        $this->sourceTemplate = $sourceTemplate;
        $this->transformationCategory = $transformationCategory;
        $this->sourceCategory = $sourceCategory;
        $this->crew = $crew;
        $this->id = $id;
    }
}
