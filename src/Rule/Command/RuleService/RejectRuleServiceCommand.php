<?php
namespace Base\Rule\Command\RuleService;

use Base\Common\Command\RejectCommand;

class RejectRuleServiceCommand extends RejectCommand
{
    public $applyCrew;

    public function __construct(
        int $applyCrew,
        string $rejectReason,
        int $id = 0
    ) {
        parent::__construct(
            $rejectReason,
            $id
        );
        $this->applyCrew = $applyCrew;
    }
}
