<?php
namespace Base\Rule\Command\RuleService;

use Base\Common\Command\ApproveCommand;

class ApproveRuleServiceCommand extends ApproveCommand
{
    public $applyCrew;

    public function __construct(
        int $applyCrew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        $this->applyCrew = $applyCrew;
    }
}
