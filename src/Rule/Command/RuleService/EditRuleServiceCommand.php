<?php
namespace Base\Rule\Command\RuleService;

use Marmot\Interfaces\ICommand;

class EditRuleServiceCommand implements ICommand
{
    public $rules;
    
    public $crew;

    public $id;
    
    public function __construct(
        array $rules,
        int $crew,
        int $id
    ) {
        $this->rules = $rules;
        $this->crew = $crew;
        $this->id = $id;
    }
}
