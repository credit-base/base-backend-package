<?php
namespace Base\WebsiteCustomize\Translator;

use Marmot\Interfaces\ITranslator;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Model\NullWebsiteCustomize;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class WebsiteCustomizeDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $websiteCustomize = null) : WebsiteCustomize
    {
        if (!isset($expression['website_customize_id'])) {
            return NullWebsiteCustomize::getInstance();
        }

        if ($websiteCustomize == null) {
            $websiteCustomize = new WebsiteCustomize($expression['website_customize_id']);
        }

        if (isset($expression['category'])) {
            $websiteCustomize->setCategory($expression['category']);
        }
        if (isset($expression['content'])) {
            $content = array();
            if (is_string($expression['content'])) {
                $content = json_decode($expression['content'], true);
            }
            if (is_array($expression['content'])) {
                $content = $expression['content'];
            }
            $websiteCustomize->setContent($content);
        }
        if (isset($expression['crew_id'])) {
            $websiteCustomize->getCrew()->setId($expression['crew_id']);
        }
        if (isset($expression['version'])) {
            $websiteCustomize->setVersion($expression['version']);
        }

        $websiteCustomize->setStatus($expression['status']);
        $websiteCustomize->setCreateTime($expression['create_time']);
        $websiteCustomize->setUpdateTime($expression['update_time']);
        $websiteCustomize->setStatusTime($expression['status_time']);

        return $websiteCustomize;
    }

    public function objectToArray($websiteCustomize, array $keys = array()) : array
    {
        if (!$websiteCustomize instanceof WebsiteCustomize) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'category',
                'content',
                'version',
                'crew',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['website_customize_id'] = $websiteCustomize->getId();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $websiteCustomize->getCategory();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $websiteCustomize->getContent();
        }
        if (in_array('version', $keys)) {
            $expression['version'] = $websiteCustomize->getVersion();
        }
        if (in_array('crew', $keys)) {
            $expression['crew_id'] = $websiteCustomize->getCrew()->getId();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $websiteCustomize->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $websiteCustomize->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $websiteCustomize->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $websiteCustomize->getStatusTime();
        }
        
        return $expression;
    }
}
