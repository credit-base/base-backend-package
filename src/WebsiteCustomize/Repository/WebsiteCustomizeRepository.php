<?php
namespace Base\WebsiteCustomize\Repository;

use Marmot\Framework\Classes\Repository;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\WebsiteCustomizeDbAdapter;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\WebsiteCustomizeMockAdapter;

class WebsiteCustomizeRepository extends Repository implements IWebsiteCustomizeAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new WebsiteCustomizeDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IWebsiteCustomizeAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IWebsiteCustomizeAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IWebsiteCustomizeAdapter
    {
        return new WebsiteCustomizeMockAdapter();
    }

    public function fetchOne($id) : WebsiteCustomize
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(WebsiteCustomize $websiteCustomize) : bool
    {
        return $this->getAdapter()->add($websiteCustomize);
    }

    public function edit(WebsiteCustomize $websiteCustomize, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($websiteCustomize, $keys);
    }
}
