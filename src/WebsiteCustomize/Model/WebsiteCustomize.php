<?php
namespace Base\WebsiteCustomize\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Framework\Query\FragmentCacheQuery;

use Base\Common\Model\IOperate;
use Base\Common\Model\OperateTrait;

use Base\Crew\Model\Crew;

use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeFragmentCacheQueryFactory;

class WebsiteCustomize implements IObject, IOperate
{
    use Object, OperateTrait;

    const STATUS = array(
        'UNPUBLISHED' => 0, //未发布
        'PUBLISHED' => 2, //发布
    );

    const CATEGORY = array(
        'HOME_PAGE' => 1, //首页
    );

    const VERSION_MIN_RANDOM_NUMBER = 0000;
    const VERSION_MAX_RANDOM_NUMBER = 9999;

    const PUBLISHED_STATUS_COUNT = 1;

    protected $id;
    /**
     * @var int $category 定制类型
     */
    protected $category;

    /**
     * @var array $content 定制内容
     */
    protected $content;

    /**
     * @var int $version 定制版本号
     */
    protected $version;

    /**
     * @var Crew $crew 发布人
     */
    protected $crew;

    private $repository;

    private $fragmentCacheQueryFactory;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->category = self::CATEGORY['HOME_PAGE'];
        $this->content = array();
        $this->version = 0;
        $this->crew = new Crew();
        $this->status = self::STATUS['UNPUBLISHED'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new WebsiteCustomizeRepository();
        $this->fragmentCacheQueryFactory = new WebsiteCustomizeFragmentCacheQueryFactory();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->category);
        unset($this->content);
        unset($this->version);
        unset($this->crew);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
        unset($this->fragmentCacheQueryFactory);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array($category, self::CATEGORY) ? $category : self::CATEGORY['HOME_PAGE'];
    }
    
    public function getCategory() : int
    {
        return $this->category;
    }

    public function setContent(array $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : array
    {
        return $this->content;
    }

    public function setVersion(int $version) : void
    {
        $this->version = $version;
    }

    public function getVersion() : int
    {
        return $this->version;
    }

    public function generateVersion()
    {
        $version = date('ymd', $this->getCreateTime()).rand(
            self::VERSION_MIN_RANDOM_NUMBER,
            self::VERSION_MAX_RANDOM_NUMBER
        );

        $this->setVersion($version);
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['UNPUBLISHED'];
    }

    protected function getRepository() : IWebsiteCustomizeAdapter
    {
        return $this->repository;
    }

    protected function getFragmentCacheQueryFactory() : WebsiteCustomizeFragmentCacheQueryFactory
    {
        return $this->fragmentCacheQueryFactory;
    }
    
    protected function getFragmentCacheQuery() : FragmentCacheQuery
    {
        return $this->getFragmentCacheQueryFactory()->getCache($this->getCategory());
    }

    public function isUnPublished() : bool
    {
        return $this->getStatus() == self::STATUS['UNPUBLISHED'];
    }

    protected function addAction() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crewId'));
            return false;
        }

        return ($this->isUnPublished() ? $this->addActionUnPublishedStatus() : $this->addActionPublishedStatus());
    }

    protected function addActionPublishedStatus() : bool
    {
        if ($this->updatePublishWebsiteCustomize() && $this->getRepository()->add($this)) {
            $this->getFragmentCacheQuery()->clear();

            return true;
        }

        return false;
    }

    protected function addActionUnPublishedStatus() : bool
    {
        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return false;
    }

    public function publish() : bool
    {
        if (!$this->isUnPublished()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        if ($this->updatePublishWebsiteCustomize() && $this->updateStatus(self::STATUS['PUBLISHED'])) {
            $this->getFragmentCacheQuery()->clear();
            return true;
        }

        return false;
    }

    protected function updateStatus(int $status) : bool
    {
        $this->setStatus($status);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array('updateTime','status','statusTime')
        );
    }

    protected function updatePublishWebsiteCustomize() : bool
    {
        $websiteCustomize = $this->fetchPublishWebsiteCustomize();

        if ($websiteCustomize instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'websiteCustomizeId'));
            return false;
        }

        return $websiteCustomize->updateStatus(self::STATUS['UNPUBLISHED']);
    }

    protected function fetchPublishWebsiteCustomize() : WebsiteCustomize
    {
        $filter['category'] = $this->getCategory();
        $filter['status'] = self::STATUS['PUBLISHED'];

        list($list, $count) = $this->getRepository()->filter($filter);
        
        return ($count == self::PUBLISHED_STATUS_COUNT) ? current($list) : NullWebsiteCustomize::getInstance();
    }
}
