<?php
namespace Base\WebsiteCustomize\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;

class NullWebsiteCustomize extends WebsiteCustomize implements INull
{
    use NullOperateTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function isUnPublished() : bool
    {
        return $this->resourceNotExist();
    }

    public function addActionPublishedStatus() : bool
    {
        return $this->resourceNotExist();
    }

    public function addActionUnPublishedStatus() : bool
    {
        return $this->resourceNotExist();
    }

    public function publish() : bool
    {
        return $this->resourceNotExist();
    }

    public function updateStatus(int $status) : bool
    {
        unset($status);
        return $this->resourceNotExist();
    }

    public function updatePublishWebsiteCustomize() : bool
    {
        return $this->resourceNotExist();
    }
}
