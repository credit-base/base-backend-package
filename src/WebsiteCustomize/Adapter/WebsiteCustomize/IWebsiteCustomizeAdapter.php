<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize;

use Base\WebsiteCustomize\Model\WebsiteCustomize;

interface IWebsiteCustomizeAdapter
{
    public function fetchOne($id) : WebsiteCustomize;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(WebsiteCustomize $websiteCustomize) : bool;

    public function edit(WebsiteCustomize $websiteCustomize, array $keys = array()) : bool;
}
