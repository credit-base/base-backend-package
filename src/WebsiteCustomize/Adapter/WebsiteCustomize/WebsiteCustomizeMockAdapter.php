<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize;

use Base\WebsiteCustomize\Utils\MockFactory;
use Base\WebsiteCustomize\Model\WebsiteCustomize;

class WebsiteCustomizeMockAdapter implements IWebsiteCustomizeAdapter
{
    public function fetchOne($id) : WebsiteCustomize
    {
        return MockFactory::generateWebsiteCustomize($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $websiteCustomizeList = array();

        foreach ($ids as $id) {
            $websiteCustomizeList[$id] = MockFactory::generateWebsiteCustomize($id);
        }

        return $websiteCustomizeList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(WebsiteCustomize $websiteCustomize) : bool
    {
        unset($websiteCustomize);
        return true;
    }

    public function edit(WebsiteCustomize $websiteCustomize, array $keys = array()) : bool
    {
        unset($websiteCustomize);
        unset($keys);
        return true;
    }
}
