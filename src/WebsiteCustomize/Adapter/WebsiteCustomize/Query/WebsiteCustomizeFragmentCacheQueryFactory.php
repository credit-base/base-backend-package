<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use Base\WebsiteCustomize\Model\WebsiteCustomize;

class WebsiteCustomizeFragmentCacheQueryFactory
{
    const MAPS = array(
        WebsiteCustomize::CATEGORY['HOME_PAGE'] =>
        'Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeHomePageFragmentCacheQuery'
    );

    public function getCache($category)
    {
        $cache = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($cache) ? new $cache : NullWebsiteCustomizeHomeFragmentCacheQuery::getInstance();
    }
}
