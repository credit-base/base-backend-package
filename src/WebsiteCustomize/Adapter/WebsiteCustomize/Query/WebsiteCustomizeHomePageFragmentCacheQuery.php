<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use Marmot\Framework\Query\FragmentCacheQuery;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Model\NullWebsiteCustomize;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;

class WebsiteCustomizeHomePageFragmentCacheQuery extends FragmentCacheQuery
{
    private $repository;

    private $translator;

    public function __construct()
    {
        parent::__construct(
            'website_customize_home_page',
            new Persistence\WebsiteCustomizeCache()
        );

        $this->repository = new WebsiteCustomizeRepository();
        $this->translator = new WebsiteCustomizeDbTranslator();
    }

    protected function getRepository() : WebsiteCustomizeRepository
    {
        return $this->repository;
    }

    protected function getTranslator() : WebsiteCustomizeDbTranslator
    {
        return $this->translator;
    }

    protected function fetchCacheData()
    {
        $filter['category'] = WebsiteCustomize::CATEGORY['HOME_PAGE'];
        $filter['status'] = WebsiteCustomize::STATUS['PUBLISHED'];

        list($list, $count) = $this->getRepository()->filter($filter);

        $websiteCustomize = ($count == WebsiteCustomize::PUBLISHED_STATUS_COUNT) ?
                            current($list) :
                            NullWebsiteCustomize::getInstance();
        
        return $this->getTranslator()->objectToArray($websiteCustomize);
    }
}
