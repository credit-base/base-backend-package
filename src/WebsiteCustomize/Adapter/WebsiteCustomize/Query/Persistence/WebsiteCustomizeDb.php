<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\Persistence;

use Marmot\Framework\Classes\Db;

class WebsiteCustomizeDb extends Db
{
    const TABLE = 'website_customize';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
