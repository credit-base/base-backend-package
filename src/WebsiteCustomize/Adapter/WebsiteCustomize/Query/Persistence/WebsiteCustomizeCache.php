<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class WebsiteCustomizeCache extends Cache
{
    const KEY = 'website_customize';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
