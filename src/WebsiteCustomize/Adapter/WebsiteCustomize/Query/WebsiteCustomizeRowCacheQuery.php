<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use Marmot\Framework\Query\RowCacheQuery;

class WebsiteCustomizeRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'website_customize_id',
            new Persistence\WebsiteCustomizeCache(),
            new Persistence\WebsiteCustomizeDb()
        );
    }
}
