<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query;

use Marmot\Interfaces\INull;

class NullWebsiteCustomizeHomeFragmentCacheQuery extends WebsiteCustomizeHomePageFragmentCacheQuery implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public function fetchCacheData()
    {
        return array();
    }
}
