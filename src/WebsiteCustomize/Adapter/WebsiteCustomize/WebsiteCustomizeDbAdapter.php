<?php
namespace Base\WebsiteCustomize\Adapter\WebsiteCustomize;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Model\NullWebsiteCustomize;
use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeRowCacheQuery;

use Base\Crew\Repository\CrewRepository;

class WebsiteCustomizeDbAdapter implements IWebsiteCustomizeAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $crewRepository;

    public function __construct()
    {
        $this->dbTranslator = new WebsiteCustomizeDbTranslator();
        $this->rowCacheQuery = new WebsiteCustomizeRowCacheQuery();
        $this->crewRepository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->crewRepository);
    }
    
    protected function getDbTranslator() : WebsiteCustomizeDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : WebsiteCustomizeRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getNullObject() : INull
    {
        return NullWebsiteCustomize::getInstance();
    }

    public function add(WebsiteCustomize $websiteCustomize) : bool
    {
        return $this->addAction($websiteCustomize);
    }

    public function edit(WebsiteCustomize $websiteCustomize, array $keys = array()) : bool
    {
        return $this->editAction($websiteCustomize, $keys);
    }

    public function fetchOne($id) : WebsiteCustomize
    {
        $websiteCustomize = $this->fetchOneAction($id);

        $this->fetchCrew($websiteCustomize);

        return $websiteCustomize;
    }

    public function fetchList(array $ids) : array
    {
        $websiteCustomizeList = $this->fetchListAction($ids);
        $this->fetchCrew($websiteCustomizeList);

        return $websiteCustomizeList;
    }

    protected function fetchCrew($websiteCustomize)
    {
        return is_array($websiteCustomize) ?
        $this->fetchCrewByList($websiteCustomize) :
        $this->fetchCrewByObject($websiteCustomize);
    }

    protected function fetchCrewByObject($websiteCustomize)
    {
        if (!$websiteCustomize instanceof INull) {
            $crewId = $websiteCustomize->getCrew()->getId();
            $crew = $this->getCrewRepository()->fetchOne($crewId);
            $websiteCustomize->setCrew($crew);
        }
    }

    protected function fetchCrewByList(array $websiteCustomizeList)
    {
        if (!empty($websiteCustomizeList)) {
            $crewIds = array();
            foreach ($websiteCustomizeList as $websiteCustomize) {
                $crewIds[] = $websiteCustomize->getCrew()->getId();
            }

            $crewList = $this->getCrewRepository()->fetchList($crewIds);
            if (!empty($crewList)) {
                foreach ($websiteCustomizeList as $websiteCustomize) {
                    if (isset($crewList[$websiteCustomize->getCrew()->getId()])) {
                        $websiteCustomize->setCrew($crewList[$websiteCustomize->getCrew()->getId()]);
                    }
                }
            }
        }
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $websiteCustomize = new WebsiteCustomize();
            if (isset($filter['crew'])) {
                $websiteCustomize->getCrew()->setId($filter['crew']);
                $info = $this->getDbTranslator()->objectToArray($websiteCustomize, array('crew'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $websiteCustomize->setStatus($filter['status']);
                $info = $this->getDbTranslator()->objectToArray($websiteCustomize, array('status'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['category'])) {
                $websiteCustomize->setCategory($filter['category']);
                $info = $this->getDbTranslator()->objectToArray($websiteCustomize, array('category'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['version'])) {
                $websiteCustomize->setVersion($filter['version']);
                $info = $this->getDbTranslator()->objectToArray($websiteCustomize, array('version'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $websiteCustomize = new WebsiteCustomize();

            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($websiteCustomize, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['status'])) {
                $info = $this->getDbTranslator()->objectToArray($websiteCustomize, array('status'));
                $condition .= $conjection.key($info).' '.($sort['status'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray($websiteCustomize, array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }
        
        return $condition;
    }
}
