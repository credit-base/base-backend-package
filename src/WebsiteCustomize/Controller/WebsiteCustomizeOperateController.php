<?php
namespace Base\WebsiteCustomize\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\View\WebsiteCustomizeView;

use Base\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;
use Base\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;

class WebsiteCustomizeOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, WebsiteCustomizeControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 网站定制新增功能,通过POST传参
     * 对应路由 /websiteCustomizes
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $category = $attributes['category'];
        $content = $attributes['content'];
        $status = $attributes['status'];

        $crew = $relationships['crew']['data'][0]['id'];

        //验证
        if ($this->validateAddScenario(
            $category,
            $content,
            $status,
            $crew
        )) {
            //初始化命令
            $command = new AddWebsiteCustomizeCommand(
                $content,
                $category,
                $status,
                $crew
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $websiteCustomize = $this->getRepository()->fetchOne($command->id);
                if ($websiteCustomize instanceof WebsiteCustomize) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new WebsiteCustomizeView($websiteCustomize));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 网站定制发布功能,通过PATCH传参
     * 对应路由 /websiteCustomizes/{id:\d+}/publish
     * @param int id 网站定制 id
     * @return jsonApi
     */
    public function publish(int $id)
    {
        if (!empty($id)) {
            $command = new PublishWebsiteCustomizeCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $websiteCustomize  = $this->getRepository()->fetchOne($id);
                if ($websiteCustomize instanceof WebsiteCustomize) {
                    $this->render(new WebsiteCustomizeView($websiteCustomize));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        return false;
    }
}
