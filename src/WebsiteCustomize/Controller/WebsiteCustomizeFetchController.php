<?php
namespace Base\WebsiteCustomize\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Query\FragmentCacheQuery;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\View\WebsiteCustomizeView;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\Translator\WebsiteCustomizeDbTranslator;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter;
use Base\WebsiteCustomize\Adapter\WebsiteCustomize\Query\WebsiteCustomizeFragmentCacheQueryFactory;

class WebsiteCustomizeFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    private $fragmentCacheQueryFactory;

    private $translator;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WebsiteCustomizeRepository();
        $this->translator = new WebsiteCustomizeDbTranslator();
        $this->fragmentCacheQueryFactory = new WebsiteCustomizeFragmentCacheQueryFactory();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->translator);
        unset($this->fragmentCacheQueryFactory);
    }

    protected function getRepository() : IWebsiteCustomizeAdapter
    {
        return $this->repository;
    }

    protected function getTranslator() : WebsiteCustomizeDbTranslator
    {
        return $this->translator;
    }

    protected function getFragmentCacheQueryFactory() : WebsiteCustomizeFragmentCacheQueryFactory
    {
        return $this->fragmentCacheQueryFactory;
    }

    protected function getFragmentCacheQuery(int $category) : FragmentCacheQuery
    {
        return $this->getFragmentCacheQueryFactory()->getCache($category);
    }

    protected function generateView($data) : IView
    {
        return new WebsiteCustomizeView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'websiteCustomizes';
    }

    /**
     * 获取首页定制数据,通过GET传参
     * 对应路由 /websiteCustomizes/homePage
     * @return jsonApi
     */
    public function homePage()
    {
        $category = WebsiteCustomize::CATEGORY['HOME_PAGE'];

        $data = $this->getFragmentCacheQuery($category)->get();

        $websiteCustomize = $this->getTranslator()->arrayToObject($data);

        if (!$websiteCustomize instanceof INull) {
            $this->render(new WebsiteCustomizeView($websiteCustomize));
            return true;
        }
        
        Core::setLastError(RESOURCE_NOT_EXIST);
        $this->displayError();
        return false;
    }
}
