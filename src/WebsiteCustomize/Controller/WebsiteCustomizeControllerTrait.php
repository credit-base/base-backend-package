<?php
namespace Base\WebsiteCustomize\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\WebsiteCustomize\WidgetRule\WebsiteCustomizeWidgetRule;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\WebsiteCustomize\CommandHandler\WebsiteCustomize\WebsiteCustomizeCommandHandlerFactory;

trait WebsiteCustomizeControllerTrait
{
    protected function getWebsiteCustomizeWidgetRule() : WebsiteCustomizeWidgetRule
    {
        return new WebsiteCustomizeWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : WebsiteCustomizeRepository
    {
        return new WebsiteCustomizeRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new WebsiteCustomizeCommandHandlerFactory());
    }

    protected function validateAddScenario(
        $category,
        $content,
        $status,
        $crew
    ) {
        return $this->getWebsiteCustomizeWidgetRule()->category($category)
            && $this->getWebsiteCustomizeWidgetRule()->content($category, $content)
            && $this->getWebsiteCustomizeWidgetRule()->status($status)
            && $this->getCommonWidgetRule()->formatNumeric($crew, 'crewId');
    }
}
