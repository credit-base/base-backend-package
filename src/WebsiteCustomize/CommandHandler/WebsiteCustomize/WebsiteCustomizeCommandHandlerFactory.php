<?php
namespace Base\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class WebsiteCustomizeCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand' =>
        'Base\WebsiteCustomize\CommandHandler\WebsiteCustomize\AddWebsiteCustomizeCommandHandler',
        'Base\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand' =>
        'Base\WebsiteCustomize\CommandHandler\WebsiteCustomize\PublishWebsiteCustomizeCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
