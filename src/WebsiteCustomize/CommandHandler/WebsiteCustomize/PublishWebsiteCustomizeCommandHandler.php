<?php
namespace Base\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;

class PublishWebsiteCustomizeCommandHandler implements ICommandHandler
{
    use WebsiteCustomizeCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof PublishWebsiteCustomizeCommand)) {
            throw new \InvalidArgumentException;
        }

        $websiteCustomize = $this->fetchWebsiteCustomize($command->id);

        return $websiteCustomize->publish();
    }
}
