<?php
namespace Base\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;

class AddWebsiteCustomizeCommandHandler implements ICommandHandler
{
    use WebsiteCustomizeCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddWebsiteCustomizeCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->fetchCrew($command->crew);

        $websiteCustomize = $this->getWebsiteCustomize();
        $websiteCustomize->setCategory($command->category);
        $websiteCustomize->setContent($command->content);
        $websiteCustomize->setStatus($command->status);
        $websiteCustomize->setCrew($crew);
        $websiteCustomize->generateVersion();

        if ($websiteCustomize->add()) {
            $command->id = $websiteCustomize->getId();
            return true;
        }
        
        return false;
    }
}
