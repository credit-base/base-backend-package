<?php
namespace Base\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\WebsiteCustomize\Model\WebsiteCustomize;
use Base\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

trait WebsiteCustomizeCommandHandlerTrait
{
    protected function getCrewRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    protected function getWebsiteCustomize() : WebsiteCustomize
    {
        return new WebsiteCustomize();
    }

    protected function getWebsiteCustomizeRepository() : WebsiteCustomizeRepository
    {
        return new WebsiteCustomizeRepository();
    }

    protected function fetchWebsiteCustomize(int $id) : WebsiteCustomize
    {
        return $this->getWebsiteCustomizeRepository()->fetchOne($id);
    }
}
