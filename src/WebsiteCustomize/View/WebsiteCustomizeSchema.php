<?php
namespace Base\WebsiteCustomize\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class WebsiteCustomizeSchema extends SchemaProvider
{
    protected $resourceType = 'websiteCustomizes';

    public function getId($websiteCustomize) : int
    {
        return $websiteCustomize->getId();
    }

    public function getAttributes($websiteCustomize) : array
    {
        return [
            'category' => $websiteCustomize->getCategory(),
            'content'  => $websiteCustomize->getContent(),
            'version'  => $websiteCustomize->getVersion(),
            'status' => $websiteCustomize->getStatus(),
            'createTime' => $websiteCustomize->getCreateTime(),
            'updateTime' => $websiteCustomize->getUpdateTime(),
            'statusTime' => $websiteCustomize->getStatusTime(),
        ];
    }

    public function getRelationships($websiteCustomize, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'crew' => [self::DATA => $websiteCustomize->getCrew()]
        ];
    }
}
