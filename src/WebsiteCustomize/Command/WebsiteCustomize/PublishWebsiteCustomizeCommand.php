<?php
namespace Base\WebsiteCustomize\Command\WebsiteCustomize;

use Marmot\Interfaces\ICommand;

class PublishWebsiteCustomizeCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
