<?php
namespace Base\WebsiteCustomize\Command\WebsiteCustomize;

use Marmot\Interfaces\ICommand;

class AddWebsiteCustomizeCommand implements ICommand
{
    public $content;

    public $category;

    public $status;

    public $crew;

    public $id;
    
    public function __construct(
        array $content,
        int $category,
        int $status = 0,
        int $crew = 0,
        int $id = 0
    ) {
        $this->content = $content;
        $this->category = $category;
        $this->status = $status;
        $this->crew = $crew;
        $this->id = $id;
    }
}
