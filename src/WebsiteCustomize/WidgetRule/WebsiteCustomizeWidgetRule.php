<?php
namespace Base\WebsiteCustomize\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Base\Common\Model\IEnableAble;

use Base\WebsiteCustomize\Model\WebsiteCustomize;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class WebsiteCustomizeWidgetRule
{
    const VALIDATE_CONTENT = array(
        WebsiteCustomize::CATEGORY['HOME_PAGE'] => 'validateHomePageContent',
    );

    const HOME_PAGE_CONTENT_KEYS = array(
        'memorialStatus', 'theme', 'headerBarLeft', 'headerBarRight', 'headerBg', 'logo', 'headerSearch', 'nav',
        'centerDialog', 'animateWindow', 'leftFloatCard', 'frameWindow', 'rightToolBar', 'specialColumn',
        'relatedLinks', 'footerBanner', 'organizationGroup', 'silhouette', 'partyAndGovernmentOrgans',
        'governmentErrorCorrection', 'footerNav', 'footerTwo', 'footerThree'
    );

    const HOME_PAGE_CONTENT_REQUIRED_KEYS = array(
        'memorialStatus', 'theme', 'headerBarLeft', 'headerBarRight', 'headerBg', 'logo', 'headerSearch', 'nav',
        'rightToolBar', 'footerNav', 'footerThree'
    );

    const VALIDATE_HOME_PAGE_CONTENT_FRAGMENT = array(
        'memorialStatus' => 'validateMemorialStatus',
        'theme' => 'validateTheme',
        'headerBarLeft' => 'validateHeaderBarLeft',
        'headerBarRight' => 'validateHeaderBarRight',
        'headerBg' => 'validateHeaderBg',
        'logo' => 'validateLogo',
        'headerSearch' => 'validateHeaderSearch',
        'nav' => 'validateNav',
        'centerDialog' => 'validateCenterDialog',
        'animateWindow' => 'validateAnimateWindow',
        'leftFloatCard' => 'validateLeftFloatCard',
        'frameWindow' => 'validateFrameWindow',
        'rightToolBar' => 'validateRightToolBar',
        'specialColumn' => 'validateSpecialColumn',
        'relatedLinks' => 'validateRelatedLinks',
        'footerBanner' => 'validateFooterBanner',
        'organizationGroup' => 'validateOrganizationGroup',
        'silhouette' => 'validateSilhouette',
        'partyAndGovernmentOrgans' => 'validatePartyAndGovernmentOrgans',
        'governmentErrorCorrection' => 'validateGovernmentErrorCorrection',
        'footerNav' => 'validateFooterNav',
        'footerTwo' => 'validateFooterTwo',
        'footerThree' => 'validateFooterThree',
    );
    
    const CONTENT_IMAGE_TYPE = array(
        'FORMAT_ONE' => 1,
        'FORMAT_TWO' => 2
    );

    const CONTENT_IMAGE_VALIDATE = array(
        self::CONTENT_IMAGE_TYPE['FORMAT_ONE'] => 'validateImageFormatOneExtension',
        self::CONTENT_IMAGE_TYPE['FORMAT_TWO'] => 'validateImageFormatTwoExtension'
    );
    
    const CONTENT_NAME_TYPE = array(
        'FORMAT_ONE' => 1,
        'FORMAT_TWO' => 2,
        'FORMAT_THREE' => 3,
        'FORMAT_FOUR' => 4
    );

    const CONTENT_NAME_VALIDATE = array(
        self::CONTENT_NAME_TYPE['FORMAT_ONE'] => 'validateNameFormatOneExtension',
        self::CONTENT_NAME_TYPE['FORMAT_TWO'] => 'validateNameFormatTwoExtension',
        self::CONTENT_NAME_TYPE['FORMAT_THREE'] => 'validateNameFormatThreeExtension',
        self::CONTENT_NAME_TYPE['FORMAT_FOUR'] => 'validateNameFormatFourExtension'
    );
    
    const CONTENT_URL_TYPE = array(
        'FORMAT_ONE' => 1,
        'FORMAT_TWO' => 2
    );

    const CONTENT_URL_VALIDATE = array(
        self::CONTENT_URL_TYPE['FORMAT_ONE'] => 'validateUrlFormatOneExtension',
        self::CONTENT_URL_TYPE['FORMAT_TWO'] => 'validateUrlFormatTwoExtension'
    );

    //内容分类
    const TYPE = array(
        'NULL' => 0, //默认
        'PV' => 1, //访问量统计
        'FONT' => 2, //字体
        'ACCESSIBLE_READING' => 3, //无障碍阅读
        'SIGN_IN' => 4, //登录
        'SIGN_UP' => 5, //注册
        'USER_CENTER' => 6, //用户中心
        'FOR_THE_RECORD' => 7, //备案号
        'PUBLIC_NETWORK_SECURITY' => 8, //公网安备
        'WEBSITE_IDENTIFY' => 9, //网站标识码
    );
    
    //头部搜索栏数组键名
    const HEADER_SEARCH_KEYS = array(
        'creditInformation', 'personalCreditInformation', 'unifiedSocialCreditCode', 'legalPerson', 'newsTitle'
    );

    //内容图片验证
    protected function validateContentImage($type, $image, $pointer = 'image') : bool
    {
        if (!V::arrayType()->validate($image)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (!isset($image['identify']) || !isset($image['name'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        $validateImage = isset(self::CONTENT_IMAGE_VALIDATE[$type]) ? self::CONTENT_IMAGE_VALIDATE[$type] : '';

        if (!method_exists($this, $validateImage)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }
        
        return $this->$validateImage($image['identify'], $pointer);
    }

    private function validateImageFormatOneExtension($image, $pointer = 'image') : bool
    {
        if (!V::extension('png')->validate($image)
            && !V::extension('jpg')->validate($image)
            && !V::extension('jpeg')->validate($image)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    private function validateImageFormatTwoExtension($image, $pointer = 'image') : bool
    {
        if (!V::extension('png')->validate($image)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    //内容状态验证
    protected function validateContentStatus($status, $pointer = 'status') : bool
    {
        if (!V::numeric()->validate($status) || !in_array($status, IEnableAble::STATUS)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    //内容类型验证
    protected function validateContentType($type, $pointer = 'type') : bool
    {
        if (!V::numeric()->validate($type) || !in_array($type, self::TYPE)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    //内容名称验证
    protected function validateContentName($type, $name, $pointer = 'name') : bool
    {
        $validateName = isset(self::CONTENT_NAME_VALIDATE[$type]) ? self::CONTENT_NAME_VALIDATE[$type] : '';

        if (!method_exists($this, $validateName)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }
        
        return $this->$validateName($name, $pointer);
    }

    const FORMAT_ONE_NAME_MIN_LENGTH = 2;
    const FORMAT_ONE_NAME_MAX_LENGTH = 10;

    private function validateNameFormatOneExtension($name, $pointer = 'name') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_ONE_NAME_MIN_LENGTH,
            self::FORMAT_ONE_NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    const FORMAT_TWO_NAME_MIN_LENGTH = 2;
    const FORMAT_TWO_NAME_MAX_LENGTH = 3;

    private function validateNameFormatTwoExtension($name, $pointer = 'name') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_TWO_NAME_MIN_LENGTH,
            self::FORMAT_TWO_NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    const FORMAT_THREE_NAME_MIN_LENGTH = 2;
    const FORMAT_THREE_NAME_MAX_LENGTH = 8;

    private function validateNameFormatThreeExtension($name, $pointer = 'name') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_THREE_NAME_MIN_LENGTH,
            self::FORMAT_THREE_NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    const FORMAT_FOUR_NAME_MIN_LENGTH = 2;
    const FORMAT_FOUR_NAME_MAX_LENGTH = 20;

    private function validateNameFormatFourExtension($name, $pointer = 'name') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_FOUR_NAME_MIN_LENGTH,
            self::FORMAT_FOUR_NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }
    
    //内容url验证
    protected function validateContentUrl($type, $url, $pointer = 'url') : bool
    {
        $validateUrl = isset(self::CONTENT_URL_VALIDATE[$type]) ? self::CONTENT_URL_VALIDATE[$type] : '';

        if (!method_exists($this, $validateUrl)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }
        
        return $this->$validateUrl($url, $pointer);
    }

    const FORMAT_ONE_URL_MIN_LENGTH = 0;
    const FORMAT_ONE_URL_MAX_LENGTH = 200;

    private function validateUrlFormatOneExtension($url, $pointer = 'url') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_ONE_URL_MIN_LENGTH,
            self::FORMAT_ONE_URL_MAX_LENGTH
        )->validate($url)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    const FORMAT_TWO_URL_MIN_LENGTH = 1;
    const FORMAT_TWO_URL_MAX_LENGTH = 200;

    private function validateUrlFormatTwoExtension($url, $pointer = 'url') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_TWO_URL_MIN_LENGTH,
            self::FORMAT_TWO_URL_MAX_LENGTH
        )->validate($url)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    //背景置灰
    const MEMORIAL_STATUS = array(
        'NO' => 0, //否 默认
        'YES' => 1 // 是
    );

    public function content($category, $content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'content'));
            return false;
        }

        $validateContent = isset(self::VALIDATE_CONTENT[$category]) ? self::VALIDATE_CONTENT[$category] : '';

        if (!method_exists($this, $validateContent)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'content'));
            return false;
        }

        return $this->$validateContent($content);
    }

    protected function validateHomePageContent(array $content) : bool
    {
        return $this->homePageContentKeysExist($content)
            && $this->homePageContentKeysRequired($content)
            && $this->homePageFormat($content);
    }

    //验证数组的键名存在
    protected function homePageContentKeysExist(array $content) : bool
    {
        $homePageContentKeys = array_keys($content);
        foreach ($homePageContentKeys as $value) {
            if (!in_array($value, self::HOME_PAGE_CONTENT_KEYS)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'content-keys'));
                return false;
            }
        }

        return true;
    }

    //验证数组中必填项存在
    protected function homePageContentKeysRequired(array $content) : bool
    {
        $homePageContentKeys = array_keys($content);
        $requiredKeys = self::HOME_PAGE_CONTENT_REQUIRED_KEYS;

        foreach ($requiredKeys as $value) {
            if (!in_array($value, $homePageContentKeys)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'content-requiredKeys'));
                return false;
            }
        }

        return true;
    }

    //验证数组格式
    protected function homePageFormat(array $content) : bool
    {
        foreach ($content as $key => $value) {
            if (!$this->validateHomePageContentFragment($key, $value)) {
                return false;
            }
        }

        return true;
    }

    //数组内容验证
    protected function validateHomePageContentFragment(string $key, $content) : bool
    {
        $validateContent = isset(
            self::VALIDATE_HOME_PAGE_CONTENT_FRAGMENT[$key]
        ) ? self::VALIDATE_HOME_PAGE_CONTENT_FRAGMENT[$key] : '';

        if (!method_exists($this, $validateContent)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'content'));
            return false;
        }

        return $this->$validateContent($content);
    }

    //验证背景置灰状态
    protected function validateMemorialStatus($content) : bool
    {
        if (!V::numeric()->validate($content) || !in_array($content, self::MEMORIAL_STATUS)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'memorialStatus'));
            return false;
        }

        return true;
    }

    //验证页面主题标识
    protected function validateTheme($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'theme'));
            return false;
        }

        if (!isset($content['color']) || !isset($content['image'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'theme'));
            return false;
        }

        if (!is_string($content['color'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'theme-color'));
            return false;
        }

        return $this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
            $content['image'],
            'theme-image'
        );
    }

    //头部左侧位置标识页面主题标识
    protected function validateHeaderBarLeft($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'headerBarLeft'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['name']) || !isset($value['status']) || !isset($value['url']) || !isset($value['type'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'headerBarLeft'));
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name'],
                'headerBarLeft-name'
            )
            ) {
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'headerBarLeft-status')) {
                return false;
            }

            if (!$this->validateContentType($value['type'], 'headerBarLeft-type')) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'headerBarLeft-url'
            )) {
                return false;
            }

            if ($value['type'] == self::TYPE['NULL']) {
                if (empty($value['url'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'headerBarLeft-url'));
                    return false;
                }
            }
        }

        return true;
    }

    //头部右侧位置标识页面主题标识
    protected function validateHeaderBarRight($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'headerBarRight'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['name']) || !isset($value['status']) || !isset($value['url']) || !isset($value['type'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'headerBarRight'));
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name'],
                'headerBarRight-name'
            )
            ) {
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'headerBarRight-status')) {
                return false;
            }

            if (!$this->validateContentType($value['type'], 'headerBarRight-type')) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'headerBarRight-url'
            )) {
                return false;
            }

            if ($value['type'] == self::TYPE['NULL']) {
                if (empty($value['url'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'headerBarRight-url'));
                    return false;
                }
            }
        }

        return true;
    }
    //网站header背景
    protected function validateHeaderBg($content) : bool
    {
        return $this->validateContentImage(self::CONTENT_IMAGE_TYPE['FORMAT_TWO'], $content, 'headerBg');
    }
    
    //网站logo
    protected function validateLogo($content) : bool
    {
        return $this->validateContentImage(self::CONTENT_IMAGE_TYPE['FORMAT_TWO'], $content, 'logo');
    }

    //头部搜索栏位置标识
    protected function validateHeaderSearch($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'headerSearch'));
            return false;
        }

        foreach ($content as $key => $value) {
            if (!in_array($key, self::HEADER_SEARCH_KEYS)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'headerSearch'));
                return false;
            }

            if (!isset($value['status']) || !$this->validateContentStatus($value['status'], 'headerSearch-status')) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'headerSearch-status'));
                return false;
            }
        }
        
        return true;
    }

    //头部导航位置标识
    const NAV_COUNT = 16;

    protected function validateNav($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'nav'));
            return false;
        }

        if (count($content) > self::NAV_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'nav-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['name']) ||
                !isset($value['status']) ||
                !isset($value['url']) ||
                !isset($value['nav']) ||
                !isset($value['image'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'nav'));
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_THREE'],
                $value['name'],
                'nav-name'
            )
            ) {
                return false;
            }

            if (!is_numeric($value['nav'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'nav'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'nav-status')) {
                return false;
            }

            if (!$this->validateContentUrl(self::CONTENT_URL_TYPE['FORMAT_TWO'], $value['url'], 'nav-url')) {
                return false;
            }

            if (!empty($value['image'])) {
                if (!$this->validateContentImage(
                    self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
                    $value['image'],
                    'nav-image'
                )) {
                    return false;
                }
            }
        }
        
        return true;
    }

    //中间弹框位置标识
    protected function validateCenterDialog($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'centerDialog'));
            return false;
        }

        if (!isset($content['status']) || !isset($content['image']) || !isset($content['url'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'centerDialog'));
            return false;
        }

        if (!$this->validateContentStatus($content['status'], 'centerDialog-status')) {
            return false;
        }

        if (!$this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
            $content['image'],
            'centerDialog-image'
        )) {
            return false;
        }
        
        if (!$this->validateContentUrl(
            self::CONTENT_URL_TYPE['FORMAT_ONE'],
            $content['url'],
            'centerDialog-url'
        )) {
            return false;
        }

        return true;
    }

    //飘窗位置标识
    const ANIMATE_WINDOW_COUNT = 2;
    
    protected function validateAnimateWindow($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'animateWindow'));
            return false;
        }

        if (count($content) > self::ANIMATE_WINDOW_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'animateWindow-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['image']) || !isset($value['url'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'animateWindow'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'animateWindow-status')) {
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image'],
                'animateWindow-image'
            )) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'animateWindow-url'
            )) {
                return false;
            }
        }

        return true;
    }

    //左侧浮动卡片位置标识
    const LEFT_FLOAT_CARD_COUNT = 5;
    
    protected function validateLeftFloatCard($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'leftFloatCard'));
            return false;
        }

        if (count($content) > self::LEFT_FLOAT_CARD_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'leftFloatCard-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['image']) || !isset($value['url'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'leftFloatCard'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'leftFloatCard-status')) {
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
                $value['image'],
                'leftFloatCard-image'
            )) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'leftFloatCard-url'
            )) {
                return false;
            }
        }

        return true;
    }

    //外链窗口
    protected function validateFrameWindow($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'frameWindow'));
            return false;
        }

        if (!isset($content['status']) || !isset($content['url'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'frameWindow'));
            return false;
        }

        if (!$this->validateContentStatus($content['status'], 'frameWindow-status')) {
            return false;
        }

        return $this->validateContentUrl(
            self::CONTENT_URL_TYPE['FORMAT_TWO'],
            $content['url'],
            'frameWindow-url'
        );
    }

    //右侧工具栏分类
    const RIGHT_TOOL_BAR_CATEGORY = array(
        'IMAGE' => 1, //图片
        'LINK' => 2, //链接
        'WRITTEN_WORDS' => 3 //文字
    );
    
    const RIGHT_TOOL_BAR_CATEGORY_VALIDATE = array(
        self::RIGHT_TOOL_BAR_CATEGORY['IMAGE'] => 'validateRightToolBarImage',
        self::RIGHT_TOOL_BAR_CATEGORY['LINK'] => 'validateRightToolBarLink',
        self::RIGHT_TOOL_BAR_CATEGORY['WRITTEN_WORDS'] => 'validateRightToolBarWrittenWords'
    );

    const RIGHT_TOOL_BAR_COUNT = 8;
    //右侧工具栏
    protected function validateRightToolBar($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rightToolBar'));
            return false;
        }

        if (count($content) > self::RIGHT_TOOL_BAR_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rightToolBar-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['name']) || !isset($value['category'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rightToolBar'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'rightToolBar-status')) {
                return false;
            }

            if (!$this->validateContentCategory($value['category'])) {
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_TWO'],
                $value['name'],
                'rightToolBar-name'
            )) {
                return false;
            }

            $validate = isset(
                self::RIGHT_TOOL_BAR_CATEGORY_VALIDATE[$value['category']]
            ) ? self::RIGHT_TOOL_BAR_CATEGORY_VALIDATE[$value['category']] : '';

            if (!method_exists($this, $validate) || !$this->$validate($value)) {
                return false;
            }
        }

        return true;
    }

    //内容右侧工具栏分类验证
    private function validateContentCategory($category) : bool
    {
        if (!V::numeric()->validate($category) || !in_array($category, self::RIGHT_TOOL_BAR_CATEGORY)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'rightToolBar-category'));
            return false;
        }

        return true;
    }

    const RIGHT_TOOL_BAR_IMAGE_COUNT = 2;

    private function validateRightToolBarImage($content) : bool
    {
        if (!isset($content['images'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rightToolBar-images'));
            return false;
        }

        $images = $content['images'];

        if (!V::arrayType()->validate($images)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rightToolBar-images'));
            return false;
        }

        if (count($images) > self::RIGHT_TOOL_BAR_IMAGE_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rightToolBar-images-count'));
            return false;
        }

        foreach ($images as $value) {
            if (!isset($value['title']) || !isset($value['image'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'rightToolBar-images'));
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['title'],
                'rightToolBar-images-title'
            )) {
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image'],
                'rightToolBar-images-image'
            )) {
                return false;
            }
        }

        return true;
    }

    private function validateRightToolBarLink($content) : bool
    {
        return isset($content['url'])
        && $this->validateContentUrl(self::CONTENT_URL_TYPE['FORMAT_TWO'], $content['url'], 'rightToolBar-url');
    }

    private function validateRightToolBarWrittenWords($content) : bool
    {
        return isset($content['description'])
        && $this->validateRightToolBarDescription($content['description']);
    }

    const RIGHT_TOOL_BAR_DESCRIPTION_MIN_LENGTH = 0;
    const RIGHT_TOOL_BAR_DESCRIPTION_MAX_LENGTH = 200;

    private function validateRightToolBarDescription($description) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::RIGHT_TOOL_BAR_DESCRIPTION_MIN_LENGTH,
            self::RIGHT_TOOL_BAR_DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'rightToolBar-description'));
            return false;
        }

        return true;
    }

    //专题专栏
    const SPECIAL_COLUMN_COUNT = 2;
    protected function validateSpecialColumn($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'specialColumn'));
            return false;
        }

        if (count($content) > self::SPECIAL_COLUMN_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'specialColumn-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['image']) || !isset($value['url'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'specialColumn'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'specialColumn-status')) {
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image'],
                'specialColumn-image'
            )) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'specialColumn-url'
            )) {
                return false;
            }
        }

        return true;
    }
    
    //相关链接
    const RELATED_LINKS_COUNT = 4;
    protected function validateRelatedLinks($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'relatedLinks'));
            return false;
        }

        if (count($content) > self::RELATED_LINKS_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'relatedLinks-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['name']) || !isset($value['items'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'relatedLinks'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'relatedLinks-status')) {
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                $value['name'],
                'relatedLinks-name'
            )) {
                return false;
            }

            if (!V::arrayType()->validate($value['items'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'relatedLinks-items'));
                return false;
            }

            foreach ($value['items'] as $item) {
                if (!isset($item['status']) || !isset($item['name']) || !isset($item['url'])) {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'relatedLinks-items'));
                    return false;
                }

                if (!$this->validateContentStatus($item['status'], 'relatedLinks-items-status')) {
                    return false;
                }

                if (!$this->validateContentName(
                    self::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                    $item['name'],
                    'relatedLinks-items-name'
                )) {
                    return false;
                }

                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_TWO'],
                    $item['url'],
                    'relatedLinks-items-url'
                )) {
                    return false;
                }
            }
        }

        return true;
    }
    
    //底部轮播
    const FOOTER_BANNER_COUNT = 10;
    protected function validateFooterBanner($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'footerBanner'));
            return false;
        }

        if (count($content) > self::FOOTER_BANNER_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'footerBanner-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['image']) || !isset($value['url'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'footerBanner'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'footerBanner-status')) {
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image'],
                'footerBanner-image'
            )) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'footerBanner-url'
            )) {
                return false;
            }
        }

        return true;
    }

    //组织列表
    const ORGANIZATION_GROUP_COUNT = 7;
    protected function validateOrganizationGroup($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'organizationGroup'));
            return false;
        }

        if (count($content) > self::ORGANIZATION_GROUP_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'organizationGroup-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['image']) || !isset($value['url'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'organizationGroup'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'organizationGroup-status')) {
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image'],
                'organizationGroup-image'
            )) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'organizationGroup-url'
            )) {
                return false;
            }
        }

        return true;
    }

    //剪影
    protected function validateSilhouette($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'silhouette'));
            return false;
        }

        if (!isset($content['status']) || !isset($content['image']) || !isset($content['description'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'silhouette'));
            return false;
        }

        if (!$this->validateContentStatus($content['status'], 'silhouette-status')) {
            return false;
        }

        if (!$this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
            $content['image'],
            'silhouette-image'
        )) {
            return false;
        }

        if (!empty($content['description'])) {
            return $this->validateSilhouetteDescription($content['description']);
        }

        return true;
    }

    const SILHOUETTE_DESCRIPTION_MIN_LENGTH = 2;
    const SILHOUETTE_DESCRIPTION_MAX_LENGTH = 200;

    private function validateSilhouetteDescription($description) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::SILHOUETTE_DESCRIPTION_MIN_LENGTH,
            self::SILHOUETTE_DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'silhouette-description'));
            return false;
        }

        return true;
    }

    //党政机关
    protected function validatePartyAndGovernmentOrgans($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'partyAndGovernmentOrgans'));
            return false;
        }

        if (!isset($content['status']) || !isset($content['image']) || !isset($content['url'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'partyAndGovernmentOrgans'));
            return false;
        }

        if (!$this->validateContentStatus($content['status'], 'partyAndGovernmentOrgans-status')) {
            return false;
        }

        if (!$this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
            $content['image'],
            'partyAndGovernmentOrgans-image'
        )) {
            return false;
        }

        if (!$this->validateContentUrl(
            self::CONTENT_URL_TYPE['FORMAT_ONE'],
            $content['url'],
            'partyAndGovernmentOrgans-url'
        )) {
            return false;
        }

        return true;
    }

    //政府纠错
    protected function validateGovernmentErrorCorrection($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'governmentErrorCorrection'));
            return false;
        }

        if (!isset($content['status']) || !isset($content['image']) || !isset($content['url'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'governmentErrorCorrection'));
            return false;
        }

        if (!$this->validateContentStatus($content['status'], 'governmentErrorCorrection-status')) {
            return false;
        }

        if (!$this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
            $content['image'],
            'governmentErrorCorrection-image'
        )) {
            return false;
        }

        if (!$this->validateContentUrl(
            self::CONTENT_URL_TYPE['FORMAT_ONE'],
            $content['url'],
            'governmentErrorCorrection-url'
        )) {
            return false;
        }

        return true;
    }

    //页脚导航
    protected function validateFooterNav($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'footerNav'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['name']) || !isset($value['url'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'footerNav'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'footerNav-status')) {
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name'],
                'footerNav-name'
            )) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'footerNav-url'
            )) {
                return false;
            }
        }

        return true;
    }

    //页脚第二行内容呈现
    protected function validateFooterTwo($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'footerTwo'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) ||
                !isset($value['name']) ||
                !isset($value['url']) ||
                !isset($value['description'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'footerTwo'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'footerTwo-status')) {
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name'],
                'footerTwo-name'
            )) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'footerTwo-url'
            )) {
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                $value['description'],
                'footerTwo-description'
            )) {
                return false;
            }
        }

        return true;
    }

    //页脚第三行内容呈现
    protected function validateFooterThree($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'footerThree'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) ||
                !isset($value['name']) ||
                !isset($value['url']) ||
                !isset($value['type']) ||
                !isset($value['description'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'footerThree'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'footerThree-status')) {
                return false;
            }

            if (!$this->validateContentType($value['type'], 'footerThree-type')) {
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name'],
                'footerThree-name'
            )) {
                return false;
            }

            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $value['url'],
                'footerThree-url'
            )) {
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                $value['description'],
                'footerThree-description'
            )) {
                return false;
            }

            if ($value['type'] == self::TYPE['PUBLIC_NETWORK_SECURITY']) {
                if (isset($value['image']) && !empty($value['image'])) {
                    if (!$this->validateContentImage(
                        self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
                        $value['image'],
                        'footerThree-image'
                    )) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function category($category) : bool
    {
        if (!V::numeric()->validate($category) || !in_array($category, WebsiteCustomize::CATEGORY)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'category'));
            return false;
        }

        return true;
    }

    public function status($status) : bool
    {
        if (!V::numeric()->validate($status) || !in_array($status, WebsiteCustomize::STATUS)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'status'));
            return false;
        }

        return true;
    }
}
