<?php
namespace Base\News\Adapter\News;

use Base\News\Utils\MockFactory;
use Base\News\Model\UnAuditedNews;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class UnAuditedNewsMockAdapter implements IApplyFormAdapter
{
    public function add(IApplyFormAble $unAuditedNews, array $keys = array()) : bool
    {
        unset($unAuditedNews);

        return true;
    }

    public function edit(IApplyFormAble $unAuditedNews, array $keys = array()) : bool
    {
        unset($unAuditedNews);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : IApplyFormAble
    {
        return MockFactory::generateUnAuditedNews($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedNewsList = array();

        foreach ($ids as $id) {
            $unAuditedNewsList[] = MockFactory::generateUnAuditedNews($id);
        }

        return $unAuditedNewsList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
