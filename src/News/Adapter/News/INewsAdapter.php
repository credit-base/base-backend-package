<?php
namespace Base\News\Adapter\News;

use Base\News\Model\News;

interface INewsAdapter
{
    public function fetchOne($id) : News;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(News $news) : bool;

    public function edit(News $news, array $keys = array()) : bool;
}
