<?php
namespace Base\News\Adapter\News;

use Marmot\Interfaces\INull;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\News\Model\News;
use Base\News\Model\NullNews;
use Base\News\Translator\NewsDbTranslator;
use Base\News\Adapter\News\Query\NewsRowCacheQuery;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class NewsDbAdapter implements INewsAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $crewRepository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->dbTranslator = new NewsDbTranslator();
        $this->rowCacheQuery = new NewsRowCacheQuery();
        $this->crewRepository = new CrewRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->crewRepository);
        unset($this->userGroupRepository);
    }
    
    protected function getDbTranslator() : ITranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : NewsRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getNullObject() : INull
    {
        return NullNews::getInstance();
    }

    public function add(News $news) : bool
    {
        return $this->addAction($news);
    }

    public function edit(News $news, array $keys = array()) : bool
    {
        return $this->editAction($news, $keys);
    }

    public function fetchOne($id) : News
    {
        $news = $this->fetchOneAction($id);
        $this->fetchCrew($news);
        $this->fetchPublishUserGroup($news);

        return $news;
    }

    public function fetchList(array $ids) : array
    {
        $newsList = $this->fetchListAction($ids);
        $this->fetchCrew($newsList);
        $this->fetchPublishUserGroup($newsList);

        return $newsList;
    }

    protected function fetchPublishUserGroup($news)
    {
        return is_array($news) ?
        $this->fetchPublishUserGroupByList($news) :
        $this->fetchPublishUserGroupByObject($news);
    }

    protected function fetchPublishUserGroupByObject($news)
    {
        if (!$news instanceof INull) {
            $userGroupId = $news->getPublishUserGroup()->getId();
            $userGroup = $this->getUserGroupRepository()->fetchOne($userGroupId);
            $news->setPublishUserGroup($userGroup);
        }
    }

    protected function fetchPublishUserGroupByList(array $newsList)
    {
        if (!empty($newsList)) {
            $userGroupIds = array();
            foreach ($newsList as $news) {
                $userGroupIds[] = $news->getPublishUserGroup()->getId();
            }

            $userGroupList = $this->getUserGroupRepository()->fetchList($userGroupIds);
            if (!empty($userGroupList)) {
                foreach ($newsList as $news) {
                    if (isset($userGroupList[$news->getPublishUserGroup()->getId()])) {
                        $news->setPublishUserGroup($userGroupList[$news->getPublishUserGroup()->getId()]);
                    }
                }
            }
        }
    }

    protected function fetchCrew($news)
    {
        return is_array($news) ?
        $this->fetchCrewByList($news) :
        $this->fetchCrewByObject($news);
    }

    protected function fetchCrewByObject($news)
    {
        if (!$news instanceof INull) {
            $crewId = $news->getCrew()->getId();
            $crew = $this->getCrewRepository()->fetchOne($crewId);
            $news->setCrew($crew);
        }
    }

    protected function fetchCrewByList(array $newsList)
    {
        if (!empty($newsList)) {
            $crewIds = array();
            foreach ($newsList as $news) {
                $crewIds[] = $news->getCrew()->getId();
            }

            $crewList = $this->getCrewRepository()->fetchList($crewIds);
            if (!empty($crewList)) {
                foreach ($newsList as $news) {
                    if (isset($crewList[$news->getCrew()->getId()])) {
                        $news->setCrew($crewList[$news->getCrew()->getId()]);
                    }
                }
            }
        }
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $news = new News();
            if (isset($filter['publishUserGroup'])) {
                $news->getPublishUserGroup()->setId($filter['publishUserGroup']);
                $info = $this->getDbTranslator()->objectToArray($news, array('publishUserGroup'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['parentCategory'])) {
                $info = $this->getDbTranslator()->objectToArray($news, array('parentCategory'));
                $condition .= $conjection.key($info).' IN ('.$filter['parentCategory'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['category'])) {
                $info = $this->getDbTranslator()->objectToArray($news, array('category'));
                $condition .= $conjection.key($info).' IN ('.$filter['category'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['newsType'])) {
                $info = $this->getDbTranslator()->objectToArray($news, array('type'));
                $condition .= $conjection.key($info).' IN ('.$filter['newsType'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['dimension'])) {
                $info = $this->getDbTranslator()->objectToArray($news, array('dimension'));
                $condition .= $conjection.key($info).' IN ('.$filter['dimension'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['title'])) {
                $news->setTitle($filter['title']);
                $info = $this->getDbTranslator()->objectToArray($news, array('title'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $news->setStatus($filter['status']);
                $info = $this->getDbTranslator()->objectToArray($news, array('status'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['stick'])) {
                $news->setStick($filter['stick']);
                $info = $this->getDbTranslator()->objectToArray($news, array('stick'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['bannerStatus'])) {
                $info = $this->getDbTranslator()->objectToArray($news, array('bannerStatus'));
                $condition .= $conjection.key($info).' = '.$filter['bannerStatus'];
                $conjection = ' AND ';
            }
            if (isset($filter['homePageShowStatus'])) {
                $news->setHomePageShowStatus($filter['homePageShowStatus']);
                $info = $this->getDbTranslator()->objectToArray($news, array('homePageShowStatus'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $news = new News();

            foreach ($sort as $key => $val) {
                if ($key == 'updateTime') {
                    $info = $this->getDbTranslator()->objectToArray($news, array('updateTime'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'status') {
                    $info = $this->getDbTranslator()->objectToArray($news, array('status'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'stick') {
                    $info = $this->getDbTranslator()->objectToArray($news, array('stick'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'bannerStatus') {
                    $info = $this->getDbTranslator()->objectToArray($news, array('bannerStatus'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'homePageShowStatus') {
                    $info = $this->getDbTranslator()->objectToArray($news, array('homePageShowStatus'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
            }
        }
        
        return $condition;
    }
}
