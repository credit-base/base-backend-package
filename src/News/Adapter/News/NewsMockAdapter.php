<?php
namespace Base\News\Adapter\News;

use Base\News\Model\News;
use Base\News\Utils\MockFactory;

class NewsMockAdapter implements INewsAdapter
{
    public function fetchOne($id) : News
    {
        return MockFactory::generateNews($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $newsList = array();

        foreach ($ids as $id) {
            $newsList[$id] = MockFactory::generateNews($id);
        }

        return $newsList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(News $news) : bool
    {
        unset($news);
        return true;
    }

    public function edit(News $news, array $keys = array()) : bool
    {
        unset($news);
        unset($keys);
        return true;
    }
}
