<?php
namespace Base\News\Adapter\News\Query\Persistence;

use Marmot\Framework\Classes\Db;

class NewsDb extends Db
{
    const TABLE = 'news';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
