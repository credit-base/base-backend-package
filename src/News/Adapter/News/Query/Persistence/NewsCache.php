<?php
namespace Base\News\Adapter\News\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class NewsCache extends Cache
{
    const KEY = 'news';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
