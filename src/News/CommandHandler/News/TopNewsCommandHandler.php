<?php
namespace Base\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\News\Command\News\TopNewsCommand;

class TopNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof TopNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedNews = $this->getUnAuditedNews();

        $unAuditedNews = $this->newsExecuteAction($command, $unAuditedNews);
        $applyInfo = $this->applyInfo($unAuditedNews);
        $unAuditedNews->setApplyInfo($applyInfo);

        if ($unAuditedNews->top()) {
            $command->id = $unAuditedNews->getApplyId();
            return true;
        }
        
        return false;
    }
}
