<?php
namespace Base\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\News\Command\News\ResubmitNewsCommand;

class ResubmitNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedNews = $this->fetchUnAuditedNews($command->id);

        $unAuditedNews = $this->executeAction($command, $unAuditedNews);
        $applyInfo = $this->applyInfo($unAuditedNews);
        $unAuditedNews->setApplyInfo($applyInfo);

        return $unAuditedNews->resubmit();
    }
}
