<?php
namespace Base\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\Model\UnAuditedNews;
use Base\News\Repository\NewsRepository;
use Base\News\Translator\NewsDbTranslator;
use Base\News\Repository\UnAuditedNewsRepository;

use Base\ApplyForm\Model\ApplyInfoCategory;

trait NewsCommandHandlerTrait
{
    protected function getCrewRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    protected function getNewsRepository() : NewsRepository
    {
        return new NewsRepository();
    }

    protected function getNewsDbTranslator() : NewsDbTranslator
    {
        return new NewsDbTranslator();
    }

    protected function fetchNews(int $id) : News
    {
        return $this->getNewsRepository()->fetchOne($id);
    }

    protected function getUnAuditedNewsRepository() : UnAuditedNewsRepository
    {
        return new UnAuditedNewsRepository();
    }

    protected function fetchUnAuditedNews(int $id) : UnAuditedNews
    {
        return $this->getUnAuditedNewsRepository()->fetchOne($id);
    }
    
    protected function getNews() : News
    {
        return new News();
    }
    
    protected function getUnAuditedNews() : UnAuditedNews
    {
        return new UnAuditedNews();
    }

    protected function executeAction(ICommand $command, UnAuditedNews $unAuditedNews) : UnAuditedNews
    {
        $crew = $this->fetchCrew($command->crew);

        $unAuditedNews->setApplyTitle($command->title);
        $unAuditedNews->setApplyInfoCategory(
            new ApplyInfoCategory(UnAuditedNews::APPLY_NEWS_CATEGORY, $command->newsType)
        );
        $unAuditedNews->setApplyUserGroup($crew->getUserGroup());
        $unAuditedNews->setRelation($crew);

        $unAuditedNews->setTitle($command->title);
        $unAuditedNews->setSource($command->source);
        $unAuditedNews->setCategory($command->newsType);
        $unAuditedNews->setContent($command->content);
        $unAuditedNews->subDescription($command->content);
        $unAuditedNews->setCover($command->cover);
        $unAuditedNews->setAttachments($command->attachments);
        $unAuditedNews->setPublishUserGroup($crew->getUserGroup());
        $unAuditedNews->setCrew($crew);
        $unAuditedNews->setDimension($command->dimension);
        $unAuditedNews->setStatus($command->status);
        $unAuditedNews->setStick($command->stick);
        $unAuditedNews->setBanner(
            new Banner($command->bannerImage, $command->bannerStatus)
        );
        $unAuditedNews->setHomePageShowStatus($command->homePageShowStatus);

        return $unAuditedNews;
    }

    protected function newsExecuteAction(ICommand $command, UnAuditedNews $unAuditedNews) : UnAuditedNews
    {
        $news = $this->fetchNews($command->newsId);
        $crew = $this->fetchCrew($command->crew);

        $unAuditedNews->setOperationType($command->operationType);
        $unAuditedNews->setApplyTitle($news->getTitle());
        $unAuditedNews->setApplyInfoCategory(
            new ApplyInfoCategory(UnAuditedNews::APPLY_NEWS_CATEGORY, $news->getNewsCategory()->getType())
        );
        $unAuditedNews->setApplyUserGroup($crew->getUserGroup());
        $unAuditedNews->setRelation($crew);

        $unAuditedNews->setId($news->getId());
        $unAuditedNews->setTitle($news->getTitle());
        $unAuditedNews->setSource($news->getSource());
        $unAuditedNews->setNewsCategory($news->getNewsCategory());
        $unAuditedNews->setContent($news->getContent());
        $unAuditedNews->setDescription($news->getDescription());
        $unAuditedNews->setCover($news->getCover());
        $unAuditedNews->setAttachments($news->getAttachments());
        $unAuditedNews->setPublishUserGroup($crew->getUserGroup());
        $unAuditedNews->setStatus($news->getStatus());
        $unAuditedNews->setStick($news->getStick());
        $unAuditedNews->setDimension($news->getDimension());
        $unAuditedNews->setBanner(
            new Banner($news->getBanner()->getImage(), $news->getBanner()->getStatus())
        );
        $unAuditedNews->setHomePageShowStatus($news->getHomePageShowStatus());
        $unAuditedNews->setCrew($crew);

        return $unAuditedNews;
    }

    protected function applyInfo($unAuditedNews) : array
    {
        $applyInfo = $this->getNewsDbTranslator()->objectToArray($unAuditedNews);

        return $applyInfo;
    }
}
