<?php
namespace Base\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\News\Command\News\AddNewsCommand;

class AddNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedNews = $this->getUnAuditedNews();

        $unAuditedNews->setOperationType($command->operationType);
        $unAuditedNews = $this->executeAction($command, $unAuditedNews);
        $applyInfo = $this->applyInfo($unAuditedNews);
        $unAuditedNews->setApplyInfo($applyInfo);

        if ($unAuditedNews->add()) {
            $command->id = $unAuditedNews->getApplyId();
            return true;
        }
        
        return false;
    }
}
