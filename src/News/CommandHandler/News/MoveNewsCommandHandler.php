<?php
namespace Base\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\News\Command\News\MoveNewsCommand;

use Base\News\Model\UnAuditedNews;

use Base\ApplyForm\Model\ApplyInfoCategory;

class MoveNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof MoveNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedNews = $this->getUnAuditedNews();

        $unAuditedNews = $this->newsExecuteAction($command, $unAuditedNews);
        $unAuditedNews->setCategory($command->newsType);
        $unAuditedNews->setApplyInfoCategory(
            new ApplyInfoCategory(UnAuditedNews::APPLY_NEWS_CATEGORY, $command->newsType)
        );
        $applyInfo = $this->applyInfo($unAuditedNews);
        $unAuditedNews->setApplyInfo($applyInfo);

        if ($unAuditedNews->move()) {
            $command->id = $unAuditedNews->getApplyId();
            return true;
        }

        return false;
    }
}
