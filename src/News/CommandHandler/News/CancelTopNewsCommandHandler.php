<?php
namespace Base\News\CommandHandler\News;

use Base\Common\Model\ITopAble;
use Base\Common\Command\CancelTopCommand;
use Base\Common\CommandHandler\CancelTopCommandHandler;

class CancelTopNewsCommandHandler extends CancelTopCommandHandler
{
    use NewsCommandHandlerTrait;
    
    protected function fetchITopObject($id) : ITopAble
    {
        return $this->fetchNews($id);
    }

    protected function executeAction(CancelTopCommand $command)
    {
        $crew = $this->fetchCrew($command->crew);
        
        $this->cancelTopAble = $this->fetchITopObject($command->id);
        $this->cancelTopAble->setCrew($crew);
        $this->cancelTopAble->setPublishUserGroup($crew->getUserGroup());

        return $this->cancelTopAble->cancelTop();
    }
}
