<?php
namespace Base\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class NewsCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\News\Command\News\AddNewsCommand' =>
        'Base\News\CommandHandler\News\AddNewsCommandHandler',
        'Base\News\Command\News\EditNewsCommand' =>
        'Base\News\CommandHandler\News\EditNewsCommandHandler',
        'Base\News\Command\News\MoveNewsCommand' =>
        'Base\News\CommandHandler\News\MoveNewsCommandHandler',
        'Base\News\Command\News\TopNewsCommand' =>
        'Base\News\CommandHandler\News\TopNewsCommandHandler',
        'Base\News\Command\News\CancelTopNewsCommand' =>
        'Base\News\CommandHandler\News\CancelTopNewsCommandHandler',
        'Base\News\Command\News\EnableNewsCommand' =>
        'Base\News\CommandHandler\News\EnableNewsCommandHandler',
        'Base\News\Command\News\DisableNewsCommand' =>
        'Base\News\CommandHandler\News\DisableNewsCommandHandler',
        'Base\News\Command\News\ResubmitNewsCommand' =>
        'Base\News\CommandHandler\News\ResubmitNewsCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
