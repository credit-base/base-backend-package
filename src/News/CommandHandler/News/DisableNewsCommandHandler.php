<?php
namespace Base\News\CommandHandler\News;

use Base\Common\Model\IEnableAble;
use Base\Common\Command\DisableCommand;
use Base\Common\CommandHandler\DisableCommandHandler;

class DisableNewsCommandHandler extends DisableCommandHandler
{
    use NewsCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchNews($id);
    }

    protected function executeAction(DisableCommand $command)
    {
        $crew = $this->fetchCrew($command->crew);
        
        $this->disableAble = $this->fetchIEnableObject($command->id);
        $this->disableAble->setCrew($crew);
        $this->disableAble->setPublishUserGroup($crew->getUserGroup());

        return $this->disableAble->disable();
    }
}
