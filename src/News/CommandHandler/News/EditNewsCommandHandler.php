<?php
namespace Base\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\News\Command\News\EditNewsCommand;

class EditNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedNews = $this->getUnAuditedNews();

        $unAuditedNews->setOperationType($command->operationType);
        $unAuditedNews = $this->executeAction($command, $unAuditedNews);
        $unAuditedNews->setId($command->newsId);
        $applyInfo = $this->applyInfo($unAuditedNews);
        $unAuditedNews->setApplyInfo($applyInfo);

        if ($unAuditedNews->edit()) {
            $command->id = $unAuditedNews->getApplyId();
            return true;
        }

        return false;
    }
}
