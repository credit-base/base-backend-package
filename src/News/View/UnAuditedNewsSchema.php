<?php
namespace Base\News\View;

class UnAuditedNewsSchema extends NewsSchema
{
    protected $resourceType = 'unAuditedNews';

    public function getId($unAuditNews) : int
    {
        return $unAuditNews->getApplyId();
    }

    public function getAttributes($unAuditNews) : array
    {
        $attributes = parent::getAttributes($unAuditNews);
        $applyAttributes = array(
            'applyInfoCategory' => $unAuditNews->getApplyInfoCategory()->getCategory(),
            'applyInfoType' => $unAuditNews->getApplyInfoCategory()->getType(),
            'applyStatus'  => $unAuditNews->getApplyStatus(),
            'rejectReason' => $unAuditNews->getRejectReason(),
            'operationType'  => $unAuditNews->getOperationType(),
            'newsId'  => $unAuditNews->getId()
        );
        
        return array_merge($applyAttributes, $attributes);
    }

    public function getRelationships($unAuditNews, $isPrimary, array $includeList)
    {
        $relationships = parent::getRelationships($unAuditNews, $isPrimary, $includeList);
        $applyRelationships = [
            'relation' => [self::DATA => $unAuditNews->getRelation()],
            'applyCrew' => [self::DATA => $unAuditNews->getApplyCrew()],
            'applyUserGroup' => [self::DATA => $unAuditNews->getApplyUserGroup()],
        ];

        return array_merge($applyRelationships, $relationships);
    }
}
