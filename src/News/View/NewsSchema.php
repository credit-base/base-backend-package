<?php
namespace Base\News\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class NewsSchema extends SchemaProvider
{
    protected $resourceType = 'news';

    public function getId($news) : int
    {
        return $news->getId();
    }

    public function getAttributes($news) : array
    {
        return [
            'title'  => $news->getTitle(),
            'source'  => $news->getSource(),
            'cover'  => $news->getCover(),
            'attachments'  => $news->getAttachments(),
            'content'  => $news->getContent(),
            'description' => $news->getDescription(),
            'parentCategory'  => $news->getNewsCategory()->getParentCategory(),
            'category'  => $news->getNewsCategory()->getCategory(),
            'newsType'  => $news->getNewsCategory()->getType(),
            'dimension' => $news->getDimension(),
            'bannerImage'  => $news->getBanner()->getImage(),
            'bannerStatus'  => $news->getBanner()->getStatus(),
            'homePageShowStatus' => $news->getHomePageShowStatus(),
            'stick' => $news->getStick(),
            'status' => $news->getStatus(),
            'createTime' => $news->getCreateTime(),
            'updateTime' => $news->getUpdateTime(),
            'statusTime' => $news->getStatusTime(),
        ];
    }

    public function getRelationships($news, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'publishUserGroup' => [self::DATA => $news->getPublishUserGroup()],
            'crew' => [self::DATA => $news->getCrew()]
        ];
    }
}
