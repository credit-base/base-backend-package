<?php
namespace Base\News\Translator;

use Marmot\Interfaces\ITranslator;

use Base\News\Model\News;
use Base\News\Model\NullNews;

class NewsDbTranslator implements ITranslator
{
    use NewsTranslatorTrait;
    
    public function arrayToObject(array $expression, $news = null) : News
    {
        if (!isset($expression['news_id'])) {
            return NullNews::getInstance();
        }

        if ($news == null) {
            $news = new News($expression['news_id']);
        }

        $news->setId($expression['news_id']);

        $this->arrayToObjectUtils($news, $expression);

        return $news;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($news, array $keys = array()) : array
    {
        if (!$news instanceof News) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'description',
                'crew',
                'dimension',
                'publishUserGroup',
                'parentCategory',
                'category',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'type',
                'status',
                'stick',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['news_id'] = $news->getId();
        }

        $expressionInfo = $this->objectToArrayUtils($news, $keys);
        
        return array_merge($expression, $expressionInfo);
    }
}
