<?php
namespace Base\News\Translator;

use Base\News\Model\UnAuditedNews;

use Base\ApplyForm\Translator\ApplyFormDbTranslator;

class UnAuditedNewsDbTranslator extends ApplyFormDbTranslator
{
    use NewsTranslatorTrait;

    public function arrayToObject(array $expression, $news = null)
    {
        if ($news == null) {
            $news = new UnAuditedNews();
        }

        $news->setId($expression['apply_form_id']);
        $news->setApplyId($expression['apply_form_id']);

        parent::arrayToObject($expression, $news);

        return $news;
    }

    public function arrayToObjects(array $expression, $news = null)
    {
        if (isset($expression['news_id'])) {
            $news->setId($expression['news_id']);
        }
        
        $this->arrayToObjectUtils($news, $expression);

        return $news;
    }
}
