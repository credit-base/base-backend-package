<?php
namespace Base\News\Translator;

use Base\News\Model\News;
use Base\News\Model\Banner;
use Base\News\Model\NewsCategory;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait NewsTranslatorTrait
{
    public function arrayToObjectUtils(News $news, array $expression)
    {
        if (isset($expression['title'])) {
            $news->setTitle($expression['title']);
        }
        if (isset($expression['source'])) {
            $news->setSource($expression['source']);
        }
        if (isset($expression['cover'])) {
            $cover = array();
            if (is_string($expression['cover'])) {
                $cover = json_decode($expression['cover'], true);
            }
            if (is_array($expression['cover'])) {
                $cover = $expression['cover'];
            }
            $news->setCover($cover);
        }
        if (isset($expression['attachments'])) {
            $attachments = array();
            if (is_string($expression['attachments'])) {
                $attachments = json_decode($expression['attachments'], true);
            }
            if (is_array($expression['attachments'])) {
                $attachments = $expression['attachments'];
            }
            $news->setAttachments($attachments);
        }
        if (isset($expression['content'])) {
            $news->setContent($expression['content']);
        }
        if (isset($expression['description'])) {
            $news->setDescription($expression['description']);
        }

        $parentCategory = isset($expression['parent_category']) ? $expression['parent_category'] : 0;
        $category = isset($expression['category']) ? $expression['category'] : 0;
        $type = isset($expression['type']) ? $expression['type'] : 0;
        $news->setNewsCategory(
            new NewsCategory(
                $parentCategory,
                $category,
                $type
            )
        );

        $bannerImage = array();
        
        if (isset($expression['banner_image'])) {
            $bannerImage = array();
            if (is_string($expression['banner_image'])) {
                $bannerImage = json_decode($expression['banner_image'], true);
            }
            if (is_array($expression['banner_image'])) {
                $bannerImage = $expression['banner_image'];
            }
        }
        $bannerStatus = isset($expression['banner_status']) ? $expression['banner_status'] : 0;
        $news->setBanner(
            new Banner(
                $bannerImage,
                $bannerStatus
            )
        );

        if (isset($expression['crew_id'])) {
            $news->getCrew()->setId($expression['crew_id']);
        }
        if (isset($expression['publish_usergroup_id'])) {
            $news->getPublishUserGroup()->setId($expression['publish_usergroup_id']);
        }
        if (isset($expression['dimension'])) {
            $news->setDimension($expression['dimension']);
        }
        if (isset($expression['home_page_show_status'])) {
            $news->setHomePageShowStatus($expression['home_page_show_status']);
        }
        if (isset($expression['stick'])) {
            $news->setStick($expression['stick']);
        }
        if (isset($expression['status'])) {
            $news->setStatus($expression['status']);
        }

        $news->setCreateTime($expression['create_time']);
        $news->setUpdateTime($expression['update_time']);
        $news->setStatusTime($expression['status_time']);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArrayUtils(News $news, array $keys)
    {
        $expression = array();

        if (in_array('title', $keys)) {
            $expression['title'] = $news->getTitle();
        }
        if (in_array('source', $keys)) {
            $expression['source'] = $news->getSource();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $news->getCover();
        }
        if (in_array('attachments', $keys)) {
            $expression['attachments'] = $news->getAttachments();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $news->getContent();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $news->getDescription();
        }
        if (in_array('parentCategory', $keys)) {
            $expression['parent_category'] = $news->getNewsCategory()->getParentCategory();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $news->getNewsCategory()->getCategory();
        }
        if (in_array('type', $keys)) {
            $expression['type'] = $news->getNewsCategory()->getType();
        }
        if (in_array('crew', $keys)) {
            $expression['crew_id'] = $news->getCrew()->getId();
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = $news->getDimension();
        }
        if (in_array('publishUserGroup', $keys)) {
            $expression['publish_usergroup_id'] = $news->getPublishUserGroup()->getId();
        }
        if (in_array('bannerImage', $keys)) {
            $expression['banner_image'] = $news->getBanner()->getImage();
        }
        if (in_array('bannerStatus', $keys)) {
            $expression['banner_status'] = $news->getBanner()->getStatus();
        }
        if (in_array('homePageShowStatus', $keys)) {
            $expression['home_page_show_status'] = $news->getHomePageShowStatus();
        }
        if (in_array('stick', $keys)) {
            $expression['stick'] = $news->getStick();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $news->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $news->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $news->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $news->getStatusTime();
        }
        
        return $expression;
    }
}
