<?php
namespace Base\News\Model;

class NewsCategory
{
    /**
     * @var int $parentCategory 父级分类
     */
    private $parentCategory;
    /**
     * @var int $category 分类
     */
    private $category;
    /**
     * @var int $type 类型
     */
    private $type;

    public function __construct(
        int $parentCategory = 0,
        int $category = 0,
        int $type = 0
    ) {
        $this->parentCategory = $parentCategory;
        $this->category = $category;
        $this->type = $type;
    }

    public function __destruct()
    {
        unset($this->parentCategory);
        unset($this->category);
        unset($this->type);
    }

    public function getParentCategory() : int
    {
        return $this->parentCategory;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function getType() : int
    {
        return $this->type;
    }
}
