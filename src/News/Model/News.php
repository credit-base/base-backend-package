<?php
namespace Base\News\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Framework\Classes\Filter;

use Base\News\Adapter\News\INewsAdapter;
use Base\News\Repository\NewsRepository;

use Base\Common\Model\ITopAble;
use Base\Common\Model\IOperate;
use Base\Common\Model\IEnableAble;
use Base\Common\Model\TopAbleTrait;
use Base\Common\Model\OperateTrait;
use Base\Common\Model\EnableAbleTrait;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

class News implements IObject, IEnableAble, ITopAble, IOperate
{
    use Object, EnableAbleTrait, TopAbleTrait, OperateTrait;
    
    /**
     * 数据共享维度
     * @var  DIMENSIONALITY['SOCIOLOGY'] 社会公开
     * @var  DIMENSIONALITY['GOVERNMENT_AFFAIRS'] 政务共享
     */
    const DIMENSIONALITY = array(
        'SOCIOLOGY' => 1 ,
        'GOVERNMENT_AFFAIRS' => 2
    );

    /**
     * 是否首页展示
     * @var  HOME_PAGE_SHOW_STATUS['DISABLED'] | 不展示
     * @var  HOME_PAGE_SHOW_STATUS['ENABLED']  | 展示
     */
    const HOME_PAGE_SHOW_STATUS = array(
        'DISABLED' => 0,
        'ENABLED' => 2
    );

    protected $id;
    /**
     * @var string $title 标题
     */
    protected $title;
    /**
     * @var string $source 来源
     */
    protected $source;
    /**
     * @var array $cover 封面
     */
    protected $cover;
    /**
     * @var array $attachments 附件
     */
    protected $attachments;
    /**
     * @var string $content 内容
     */
    protected $content;
    /**
     * @var string $description 描述
     */
    protected $description;
    /**
     * @var NewsCategory $newsCategory 新闻分类
     */
    protected $newsCategory;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var int $dimension 数据共享维度
     */
    protected $dimension;
    /**
     * @var UserGroup $publishUserGroup 发布委办局
     */
    protected $publishUserGroup;
    /**
     * @var Banner $banner 轮播图信息
     */
    protected $banner;
    /**
     * @var int $homePageShowStatus 首页展示状态
     */
    protected $homePageShowStatus;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->title = '';
        $this->source = '';
        $this->cover = array();
        $this->attachments = array();
        $this->content = '';
        $this->description = '';
        $this->newsCategory = new NewsCategory();
        $this->crew = new Crew();
        $this->publishUserGroup = new UserGroup();
        $this->dimension = self::DIMENSIONALITY['SOCIOLOGY'];
        $this->banner = new Banner();
        $this->homePageShowStatus = self::HOME_PAGE_SHOW_STATUS['DISABLED'];
        $this->stick = ITopAble::STICK['DISABLED'];
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new NewsRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->source);
        unset($this->cover);
        unset($this->attachments);
        unset($this->content);
        unset($this->description);
        unset($this->newsCategory);
        unset($this->crew);
        unset($this->publishUserGroup);
        unset($this->banner);
        unset($this->homePageShowStatus);
        unset($this->dimension);
        unset($this->stick);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setSource(string $source)
    {
        $this->source = $source;
    }

    public function getSource() : string
    {
        return $this->source;
    }

    public function setCover(array $cover)
    {
        $this->cover = $cover;
    }

    public function getCover() : array
    {
        return $this->cover;
    }

    public function setAttachments(array $attachments)
    {
        $this->attachments = $attachments;
    }

    public function getAttachments() : array
    {
        return $this->attachments;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function subDescription(string $content)
    {
        $start = 0;
        $length = 200;

        $content = Filter::dhtmlspecialchars($content);
        $content = str_replace("&nbsp;", "", $content);
        $content = Filter::stripslashesPlus($content);
        $content = strip_tags($content);

        if (mb_strlen($content, 'utf-8') > $length) {
            $content = mb_substr($content, $start, $length, 'utf-8');
        }
        $this->setDescription($content);
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setNewsCategory(NewsCategory $newsCategory) : void
    {
        $this->newsCategory = $newsCategory;
    }

    public function getNewsCategory() : NewsCategory
    {
        return $this->newsCategory;
    }

    public function setCategory(int $type) : void
    {
        $newsCategory = new NewsCategory();

        if (array_key_exists($type, NEWS_TYPE_MAPPING)) {
            $newsCategory = new NewsCategory(
                NEWS_TYPE_MAPPING[$type][0],
                NEWS_TYPE_MAPPING[$type][1],
                $type
            );
        }
        $this->setNewsCategory($newsCategory);
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setPublishUserGroup(UserGroup $publishUserGroup)
    {
        $this->publishUserGroup = $publishUserGroup;
    }

    public function getPublishUserGroup() : UserGroup
    {
        return $this->publishUserGroup;
    }

    public function setBanner(Banner $banner) : void
    {
        $this->banner = $banner;
    }

    public function getBanner() : Banner
    {
        return $this->banner;
    }

    public function setHomePageShowStatus(int $homePageShowStatus) : void
    {
        $this->homePageShowStatus = in_array(
            $homePageShowStatus,
            self::HOME_PAGE_SHOW_STATUS
        ) ? $homePageShowStatus : self::HOME_PAGE_SHOW_STATUS['DISABLED'];
    }

    public function getHomePageShowStatus() : int
    {
        return $this->homePageShowStatus;
    }

    public function setDimension(int $dimension) : void
    {
        $this->dimension = in_array(
            $dimension,
            self::DIMENSIONALITY
        ) ? $dimension : self::DIMENSIONALITY['SOCIOLOGY'];
    }

    public function getDimension() : int
    {
        return $this->dimension;
    }

    protected function getRepository() : INewsAdapter
    {
        return $this->repository;
    }

    protected function addAction() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crew'));
            return false;
        }

        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crew'));
            return false;
        }
        
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'title',
                'content',
                'source',
                'description',
                'attachments',
                'cover',
                'dimension',
                'parentCategory',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'status',
                'stick',
                'category',
                'type',
                'crew',
                'publishUserGroup',
                'updateTime'
            )
        );
    }

    public function enable() : bool
    {
        //return $this->updateStatus(IEnableAble::STATUS['ENABLED']);

        $this->setStatus(IEnableAble::STATUS['ENABLED']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'statusTime',
                    'status',
                    'updateTime',
                    'publishUserGroup',
                    'crew'
                ));
    }

    public function top() : bool
    {
        $this->setStick(ITopAble::STICK['ENABLED']);
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'stick',
                    'updateTime',
                    'publishUserGroup',
                    'crew'
                ));
        //return $this->updateStick(ITopAble::STICK['ENABLED']);
    }

    public function move() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'parentCategory',
                    'category',
                    'type',
                    'crew',
                    'publishUserGroup',
                    'updateTime'
                ));
    }

    protected function updateStatus(int $status) : bool
    {
        $this->setStatus($status);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'statusTime',
                    'status',
                    'updateTime',
                    'publishUserGroup',
                    'crew'
                ));
    }

    protected function updateStick(int $stick) : bool
    {
        $this->setStick($stick);
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'stick',
                    'updateTime',
                    'publishUserGroup',
                    'crew'
                ));
    }
}
