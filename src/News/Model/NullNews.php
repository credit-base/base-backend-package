<?php
namespace Base\News\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;
use Base\Common\Model\NullTopAbleTrait;
use Base\Common\Model\NullEnableAbleTrait;

class NullNews extends News implements INull
{
    use NullOperateTrait, NullEnableAbleTrait, NullTopAbleTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
