<?php
namespace Base\News\Model;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Model\ApplyFormTrait;
use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

class UnAuditedNews extends News implements IApplyFormAble
{
    use ApplyFormTrait;
    
    const APPLY_NEWS_CATEGORY = ApplyInfoCategory::APPLY_INFO_CATEGORY['NEWS'];

    public function __construct(int $applyId = 0)
    {
        parent::__construct();
        $this->applyId = !empty($applyId) ? $applyId : 0;
        $this->applyTitle = parent::getTitle();
        $this->relation = new Crew();
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->applyInfoCategory  = new ApplyInfoCategory(
            self::APPLY_NEWS_CATEGORY,
            parent::getNewsCategory()->getType()
        );
        $this->applyInfo = array();
        $this->applyCrew = new Crew();
        $this->applyUserGroup = new UserGroup();
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->rejectReason = '';
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->applyId);
        unset($this->applyTitle);
        unset($this->relation);
        unset($this->operationType);
        unset($this->applyInfoCategory);
        unset($this->applyInfo);
        unset($this->applyCrew);
        unset($this->applyUserGroup);
        unset($this->applyStatus);
        unset($this->rejectReason);
    }

    public function move() : bool
    {
        return $this->apply();
    }
}
