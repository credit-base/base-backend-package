<?php
namespace Base\News\Model;

class Banner
{
    /**
     * 是否设为轮播
     * @var  BANNER_STATUS['DISABLED'] | 不轮播
     * @var  BANNER_STATUS['ENABLED']  | 轮播
     */
    const BANNER_STATUS = array(
        'DISABLED' => 0,
        'ENABLED' => 2
    );
    /**
     * @var array $image 轮播图
     */
    private $image;

    /**
     * @var int $status 轮播图状态
     */
    private $status;

    public function __construct(
        array $image = array(),
        int $status = self::BANNER_STATUS['DISABLED']
    ) {
        $this->image = $image;
        $this->status = $status;
    }

    public function __destruct()
    {
        unset($this->image);
        unset($this->status);
    }

    public function getImage() : array
    {
        return $this->image;
    }

    public function getStatus() : int
    {
        return $this->status;
    }
}
