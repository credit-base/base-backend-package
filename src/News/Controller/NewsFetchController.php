<?php
namespace Base\News\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\News\View\NewsView;
use Base\News\Adapter\News\INewsAdapter;
use Base\News\Repository\NewsRepository;

class NewsFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new NewsRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : INewsAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new NewsView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'news';
    }
}
