<?php
namespace Base\News\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\News\Model\UnAuditedNews;
use Base\News\View\UnAuditedNewsView;
use Base\News\Repository\UnAuditedNewsRepository;

class UnAuditedNewsFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditedNewsRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new UnAuditedNewsView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'unAuditedNews';
    }

    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();
        $filter['applyInfoCategory'] = UnAuditedNews::APPLY_NEWS_CATEGORY;

        list($objectList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = $this->generateView($objectList);
            $view->pagination(
                $this->getResourceName(),
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
