<?php
namespace Base\News\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Common\Controller\Interfaces\ITopAbleController;

use Base\News\Model\News;
use Base\News\View\NewsView;
use Base\News\Model\UnAuditedNews;
use Base\News\View\UnAuditedNewsView;

use Base\News\Command\News\TopNewsCommand;
use Base\News\Command\News\CancelTopNewsCommand;

class NewsTopController extends Controller implements ITopAbleController
{
    use JsonApiTrait, NewsControllerTrait;

    /**
     * 对应路由 /news/{id:\d+}/top
     * 新闻类置顶功能,通过PATCH传参
     * @param $id 新闻id
     * @param jsonApi array("data"=>array(
     *                               "type"=>"news",
     *                               "relationships"=>array(
     *                                   "crew"=>array(
     *                                       "data"=>array(
     *                                           array("type"=>"crews","id"=>发布人id)
     *                                       )
     *                                   )
     *                               )
     *                           )
     *                   )
     * @return jsonApi
     */
    public function top(int $id)
    {
        if (!empty($id)) {
            $data = $this->getRequest()->patch('data');
            $relationships = $data['relationships'];

            $crew = $relationships['crew']['data'][0]['id'];

            if ($this->validateCommonScenario($crew)) {
                $command = new TopNewsCommand($id, $crew);

                if ($this->getCommandBus()->send($command)) {
                    $news  = $this->getUnAuditedNewsRepository()->fetchOne($command->id);
                    if ($news instanceof UnAuditedNews) {
                        $this->render(new UnAuditedNewsView($news));
                        return true;
                    }
                }
            }
        }
        
        $this->displayError();
        return false;
    }
    
    /**
     * 对应路由 /news/{id:\d+}/cancelTop
     * 新闻类取消置顶功能,通过PATCH传参
     * @param $id 新闻id
     * @param jsonApi array("data"=>array(
     *                               "type"=>"news",
     *                               "relationships"=>array(
     *                                   "crew"=>array(
     *                                       "data"=>array(
     *                                           array("type"=>"crews","id"=>发布人id)
     *                                       )
     *                                   )
     *                               )
     *                           )
     *                   )
     * @return jsonApi
     */
    public function cancelTop(int $id)
    {
        if (!empty($id)) {
            $data = $this->getRequest()->patch('data');
            $relationships = $data['relationships'];

            $crew = $relationships['crew']['data'][0]['id'];

            if ($this->validateCommonScenario($crew)) {
                $command = new CancelTopNewsCommand($crew, $id);
    
                if ($this->getCommandBus()->send($command)) {
                    $news  = $this->getRepository()->fetchOne($id);
                    
                    if ($news instanceof News) {
                        $this->render(new NewsView($news));
                        return true;
                    }
                }
            }
        }

        $this->displayError();
        return false;
    }
}
