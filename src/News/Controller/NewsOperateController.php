<?php
namespace Base\News\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\News\Model\UnAuditedNews;
use Base\News\View\UnAuditedNewsView;
use Base\News\Command\News\AddNewsCommand;
use Base\News\Command\News\EditNewsCommand;
use Base\News\Command\News\MoveNewsCommand;

class NewsOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, NewsControllerTrait;
    
    /**
     * 新闻类新增功能,通过POST传参
     * 对应路由 /news
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $cover = $attributes['cover'];
        $attachments = $attributes['attachments'];
        $content = $attributes['content'];
        $newsType = $attributes['newsType'];
        $dimension = $attributes['dimension'];
        $status = $attributes['status'];
        $stick = $attributes['stick'];
        $bannerStatus = $attributes['bannerStatus'];
        $bannerImage = $attributes['bannerImage'];
        $homePageShowStatus = $attributes['homePageShowStatus'];

        $crew = $relationships['crew']['data'][0]['id'];

        //验证
        if ($this->validateNewsScenario(
            $title,
            $source,
            $cover,
            $attachments,
            $content,
            $newsType,
            $dimension,
            $status,
            $stick,
            $bannerStatus,
            $bannerImage,
            $homePageShowStatus,
            $crew
        )) {
            //初始化命令
            $command = new AddNewsCommand(
                $title,
                $source,
                $cover,
                $attachments,
                $content,
                $newsType,
                $dimension,
                $status,
                $stick,
                $bannerStatus,
                $bannerImage,
                $homePageShowStatus,
                $crew
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $news = $this->getUnAuditedNewsRepository()->fetchOne($command->id);
                if ($news instanceof UnAuditedNews) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new UnAuditedNewsView($news));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 新闻编辑功能,通过PATCH传参
     * 对应路由 /news/{id:\d+}
     * @param int id 新闻 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $cover = $attributes['cover'];
        $attachments = $attributes['attachments'];
        $content = $attributes['content'];
        $newsType = $attributes['newsType'];
        $dimension = $attributes['dimension'];
        $stick = $attributes['stick'];
        $status = $attributes['status'];
        $bannerStatus = $attributes['bannerStatus'];
        $bannerImage = $attributes['bannerImage'];
        $homePageShowStatus = $attributes['homePageShowStatus'];

        $crew = $relationships['crew']['data'][0]['id'];
        
        if ($this->validateNewsScenario(
            $title,
            $source,
            $cover,
            $attachments,
            $content,
            $newsType,
            $dimension,
            $status,
            $stick,
            $bannerStatus,
            $bannerImage,
            $homePageShowStatus,
            $crew
        )) {
            $command = new EditNewsCommand(
                $title,
                $source,
                $cover,
                $attachments,
                $content,
                $newsType,
                $dimension,
                $status,
                $stick,
                $bannerStatus,
                $bannerImage,
                $homePageShowStatus,
                $id,
                $crew
            );

            if ($this->getCommandBus()->send($command)) {
                $news = $this->getUnAuditedNewsRepository()->fetchOne($command->id);
                if ($news instanceof UnAuditedNews) {
                    $this->render(new UnAuditedNewsView($news));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 新闻移动功能,通过PATCH传参
     * 对应路由 /news/{id:\d+}/move/{newsType:\d+}
     * @param int id 新闻 id
     * @param int newsType 新闻分类 newsType
     * @return jsonApi
     */
    public function move(int $id, int $newsType)
    {
        $data = $this->getRequest()->patch('data');
        $relationships = $data['relationships'];

        $crew = $relationships['crew']['data'][0]['id'];
        
        if ($this->validateMoveScenario(
            $newsType,
            $crew
        )) {
            $command = new MoveNewsCommand(
                $newsType,
                $id,
                $crew
            );

            if ($this->getCommandBus()->send($command)) {
                $news = $this->getUnAuditedNewsRepository()->fetchOne($command->id);
                if ($news instanceof UnAuditedNews) {
                    $this->render(new UnAuditedNewsView($news));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
