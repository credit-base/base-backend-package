<?php
namespace Base\News\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Common\Controller\Interfaces\IEnableAbleController;

use Base\News\Model\News;
use Base\News\View\NewsView;
use Base\News\Model\UnAuditedNews;
use Base\News\View\UnAuditedNewsView;
use Base\News\Command\News\EnableNewsCommand;
use Base\News\Command\News\DisableNewsCommand;

class NewsEnableController extends Controller implements IEnableAbleController
{
    use JsonApiTrait, NewsControllerTrait;
    
    /**
     * 对应路由 /news/{id:\d+}/enable
     * 新闻类启用功能,通过PATCH传参
     * @param $id 新闻id
     * @param jsonApi array("data"=>array(
     *                               "type"=>"news",
     *                               "relationships"=>array(
     *                                   "crew"=>array(
     *                                       "data"=>array(
     *                                           array("type"=>"crews","id"=>发布人id)
     *                                       )
     *                                   )
     *                               )
     *                           )
     *                   )
     * @return jsonApi
     */
    public function enable(int $id)
    {
        if (!empty($id)) {
            $data = $this->getRequest()->patch('data');
            $relationships = $data['relationships'];

            $crew = $relationships['crew']['data'][0]['id'];
            
            if ($this->validateCommonScenario($crew)) {
                $command = new EnableNewsCommand($id, $crew);

                if ($this->getCommandBus()->send($command)) {
                    $news  = $this->getUnAuditedNewsRepository()->fetchOne($command->id);
                    if ($news instanceof UnAuditedNews) {
                        $this->render(new UnAuditedNewsView($news));
                        return true;
                    }
                }
            }
        }
        
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /news/{id:\d+}/disable
     * 新闻类禁用功能,通过PATCH传参
     * @param $id 新闻id
     * @param jsonApi array("data"=>array(
     *                               "type"=>"news",
     *                               "relationships"=>array(
     *                                   "crew"=>array(
     *                                       "data"=>array(
     *                                           array("type"=>"crews","id"=>发布人id)
     *                                       )
     *                                   )
     *                               )
     *                           )
     *                   )
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $data = $this->getRequest()->patch('data');
            $relationships = $data['relationships'];

            $crew = $relationships['crew']['data'][0]['id'];

            if ($this->validateCommonScenario($crew)) {
                $command = new DisableNewsCommand($crew, $id);

                if ($this->getCommandBus()->send($command)) {
                    $news  = $this->getRepository()->fetchOne($id);
                    if ($news instanceof News) {
                        $this->render(new NewsView($news));
                        return true;
                    }
                }
            }
        }
        
        $this->displayError();
        return false;
    }
}
