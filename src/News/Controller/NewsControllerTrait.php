<?php
namespace Base\News\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\News\Model\Banner;
use Base\News\WidgetRule\NewsWidgetRule;
use Base\News\Repository\NewsRepository;
use Base\News\Repository\UnAuditedNewsRepository;
use Base\News\CommandHandler\News\NewsCommandHandlerFactory;

trait NewsControllerTrait
{
    protected function getNewsWidgetRule() : NewsWidgetRule
    {
        return new NewsWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : NewsRepository
    {
        return new NewsRepository();
    }

    protected function getUnAuditedNewsRepository() : UnAuditedNewsRepository
    {
        return new UnAuditedNewsRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new NewsCommandHandlerFactory());
    }

    protected function validateCommonScenario(
        $crew
    ) {
        return $this->getCommonWidgetRule()->formatNumeric($crew, 'crewId');
    }
    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    protected function validateNewsScenario(
        $title,
        $source,
        $cover,
        $attachments,
        $content,
        $newsType,
        $dimension,
        $status,
        $stick,
        $bannerStatus,
        $bannerImage,
        $homePageShowStatus,
        $crew
    ) {
        return $this->getCommonWidgetRule()->title($title)
            && $this->getCommonWidgetRule()->source($source)
            && $this->getCommonWidgetRule()->image($cover, 'cover')
            && (empty($attachments) ? true : $this->getCommonWidgetRule()->attachments($attachments))
            && $this->getCommonWidgetRule()->formatString($content, 'content')
            && $this->getNewsWidgetRule()->newsType($newsType)
            && $this->getNewsWidgetRule()->dimension($dimension)
            && $this->getCommonWidgetRule()->status($status)
            && $this->getCommonWidgetRule()->stick($stick)
            && $this->getNewsWidgetRule()->bannerStatus($bannerStatus)
            && ($bannerStatus != Banner::BANNER_STATUS['ENABLED'] ? true : $this->getCommonWidgetRule()->image(
                $bannerImage,
                'bannerImage'
            ))
            && $this->getNewsWidgetRule()->homePageShowStatus($homePageShowStatus)
            && $this->getCommonWidgetRule()->formatNumeric($crew, 'crewId');
    }

    protected function validateMoveScenario(
        $newsType,
        $crew
    ) {
        return $this->getNewsWidgetRule()->newsType($newsType)
            && $this->getCommonWidgetRule()->formatNumeric($crew, 'crewId');
    }
}
