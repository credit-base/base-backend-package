<?php
namespace Base\News\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Common\Controller\Interfaces\IResubmitAbleController;

use Base\News\Model\UnAuditedNews;
use Base\News\View\UnAuditedNewsView;
use Base\News\Command\News\ResubmitNewsCommand;

class NewsResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, NewsControllerTrait;

    /**
     * 对应路由 /unAuditedNews/{id:\d+}/resubmit
     * 重新提交, 通过PATCH传参
     * @param int id
     * @param jsonApi
     * @return jsonApi
     */
    public function resubmit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        
        $title = $attributes['title'];
        $source = $attributes['source'];
        $cover = $attributes['cover'];
        $attachments = $attributes['attachments'];
        $content = $attributes['content'];
        $newsType = $attributes['newsType'];
        $dimension = $attributes['dimension'];
        $status = $attributes['status'];
        $stick = $attributes['stick'];
        $bannerStatus = $attributes['bannerStatus'];
        $bannerImage = $attributes['bannerImage'];
        $homePageShowStatus = $attributes['homePageShowStatus'];

        $crew = $relationships['crew']['data'][0]['id'];
        
        if ($this->validateNewsScenario(
            $title,
            $source,
            $cover,
            $attachments,
            $content,
            $newsType,
            $dimension,
            $status,
            $stick,
            $bannerStatus,
            $bannerImage,
            $homePageShowStatus,
            $crew
        )) {
            $command = new ResubmitNewsCommand(
                $title,
                $source,
                $cover,
                $attachments,
                $content,
                $newsType,
                $dimension,
                $status,
                $stick,
                $bannerStatus,
                $bannerImage,
                $homePageShowStatus,
                $crew,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $unAuditedNews = $this->getUnAuditedNewsRepository()->fetchOne($id);
                if ($unAuditedNews instanceof UnAuditedNews) {
                    $this->render(new UnAuditedNewsView($unAuditedNews));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
