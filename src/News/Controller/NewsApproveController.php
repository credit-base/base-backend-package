<?php
namespace Base\News\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Common\Controller\Interfaces\IApproveAbleController;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Controller\ApplyFormControllerTrait;

use Base\News\View\UnAuditedNewsView;

class NewsApproveController extends Controller implements IApproveAbleController
{
    use JsonApiTrait, ApplyFormControllerTrait;

    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedNewsView($applyForm));
    }
}
