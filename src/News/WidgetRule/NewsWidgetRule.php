<?php
namespace Base\News\WidgetRule;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Base\News\Model\News;
use Base\News\Model\Banner;

class NewsWidgetRule
{
    public function newsType($newsType) : bool
    {
        if (!V::numeric()->positive()->validate($newsType) || !in_array($newsType, NEWS_TYPE)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'newsType'));
            return false;
        }

        return true;
    }

    public function dimension($dimension) : bool
    {
        if (!V::numeric()->positive()->validate($dimension) || !in_array($dimension, News::DIMENSIONALITY)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'dimension'));
            return false;
        }
        
        return true;
    }

    public function homePageShowStatus($homePageShowStatus) : bool
    {
        if (!V::numeric()->validate($homePageShowStatus)
        || !in_array($homePageShowStatus, News::HOME_PAGE_SHOW_STATUS)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'homePageShowStatus'));
            return false;
        }
        
        return true;
    }

    public function bannerStatus($bannerStatus) : bool
    {
        if (!V::numeric()->validate($bannerStatus) || !in_array($bannerStatus, Banner::BANNER_STATUS)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'bannerStatus'));
            return false;
        }
        
        return true;
    }
}
