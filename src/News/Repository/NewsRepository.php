<?php
namespace Base\News\Repository;

use Marmot\Framework\Classes\Repository;

use Base\News\Model\News;
use Base\News\Adapter\News\INewsAdapter;
use Base\News\Adapter\News\NewsDbAdapter;
use Base\News\Adapter\News\NewsMockAdapter;

class NewsRepository extends Repository implements INewsAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new NewsDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(INewsAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : INewsAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : INewsAdapter
    {
        return new NewsMockAdapter();
    }

    public function fetchOne($id) : News
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(News $news) : bool
    {
        return $this->getAdapter()->add($news);
    }

    public function edit(News $news, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($news, $keys);
    }
}
