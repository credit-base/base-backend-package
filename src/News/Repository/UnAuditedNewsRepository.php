<?php
namespace Base\News\Repository;

use Base\ApplyForm\Repository\ApplyFormRepository;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

use Base\News\Adapter\News\UnAuditedNewsDbAdapter;
use Base\News\Adapter\News\UnAuditedNewsMockAdapter;

class UnAuditedNewsRepository extends ApplyFormRepository
{
    public function __construct()
    {
        $this->adapter = new UnAuditedNewsDbAdapter();
    }

    protected function getActualAdapter() : IApplyFormAdapter
    {
        return $this->adapter;
    }
    
    protected function getMockAdapter() : IApplyFormAdapter
    {
        return new UnAuditedNewsMockAdapter();
    }
}
