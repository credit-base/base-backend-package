<?php
namespace Base\News\Command\News;

use Base\ApplyForm\Command\ApplyForm\TopApplyFormCommand;

class TopNewsCommand extends TopApplyFormCommand
{
    public $newsId;

    public function __construct(
        int $newsId,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $crew,
            $id
        );
        
        $this->newsId = $newsId;
    }
}
