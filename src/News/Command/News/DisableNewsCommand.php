<?php
namespace Base\News\Command\News;

use Base\Common\Command\DisableCommand;

class DisableNewsCommand extends DisableCommand
{
    public $crew;

    public function __construct(
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        
        $this->crew = $crew;
    }
}
