<?php
namespace Base\News\Command\News;

use Base\ApplyForm\Command\ApplyForm\MoveApplyFormCommand;

class MoveNewsCommand extends MoveApplyFormCommand
{
    public $newsType;

    public $newsId;

    public function __construct(
        int $newsType,
        int $newsId,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $crew,
            $id
        );
        
        $this->newsType = $newsType;
        $this->newsId = $newsId;
    }
}
