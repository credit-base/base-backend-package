<?php
namespace Base\News\Command\News;

trait NewsCommandTrait
{
    public $title;

    public $source;

    public $cover;

    public $attachments;

    public $content;

    public $newsType;

    public $dimension;

    public $status;

    public $stick;

    public $bannerStatus;

    public $bannerImage;

    public $homePageShowStatus;
}
