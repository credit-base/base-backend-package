<?php
namespace Base\News\Command\News;

use Base\Common\Command\ResubmitCommand;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ResubmitNewsCommand extends ResubmitCommand
{
    use NewsCommandTrait;

    public $crew;

    public function __construct(
        string $title,
        string $source,
        array $cover,
        array $attachments,
        string $content,
        int $newsType,
        int $dimension,
        int $status,
        int $stick,
        int $bannerStatus,
        array $bannerImage,
        int $homePageShowStatus,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        
        $this->title = $title;
        $this->source = $source;
        $this->cover = $cover;
        $this->attachments = $attachments;
        $this->content = $content;
        $this->newsType = $newsType;
        $this->dimension = $dimension;
        $this->status = $status;
        $this->stick = $stick;
        $this->bannerStatus = $bannerStatus;
        $this->bannerImage = $bannerImage;
        $this->homePageShowStatus = $homePageShowStatus;
        $this->crew = $crew;
    }
}
