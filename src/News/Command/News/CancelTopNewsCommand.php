<?php
namespace Base\News\Command\News;

use Base\Common\Command\CancelTopCommand;

class CancelTopNewsCommand extends CancelTopCommand
{
    public $crew;

    public function __construct(
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        
        $this->crew = $crew;
    }
}
