<?php
namespace Base\News\Command\News;

use Base\ApplyForm\Command\ApplyForm\EnableApplyFormCommand;

class EnableNewsCommand extends EnableApplyFormCommand
{
    public $newsId;

    public function __construct(
        int $newsId,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $crew,
            $id
        );
        
        $this->newsId = $newsId;
    }
}
