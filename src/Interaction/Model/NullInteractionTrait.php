<?php
namespace Base\Interaction\Model;

use Base\Common\Model\NullOperateTrait;

trait NullInteractionTrait
{
    use NullOperateTrait;
    
    public function isMemberNull() : bool
    {
        return $this->resourceNotExist();
    }

    public function isAcceptUserGroupNull() : bool
    {
        return $this->resourceNotExist();
    }

    public function validate() : bool
    {
        return $this->resourceNotExist();
    }

    public function updateAcceptStatus(int $acceptStatus) : bool
    {
        unset($acceptStatus);
        return $this->resourceNotExist();
    }

    public function updateStatus(int $status) : bool
    {
        unset($status);
        return $this->resourceNotExist();
    }

    public function accept() : bool
    {
        return $this->resourceNotExist();
    }

    public function isNormal() : bool
    {
        return $this->resourceNotExist();
    }

    public function isPublish() : bool
    {
        return $this->resourceNotExist();
    }

    public function isAcceptStatusComplete() : bool
    {
        return $this->resourceNotExist();
    }

    public function revoke() : bool
    {
        return $this->resourceNotExist();
    }

    public function publish() : bool
    {
        return $this->resourceNotExist();
    }

    public function unPublish() : bool
    {
        return $this->resourceNotExist();
    }
}
