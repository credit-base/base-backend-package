<?php
namespace Base\Interaction\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;

class NullReply extends Reply implements INull
{
    use NullOperateTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function isCrewNull() : bool
    {
        return $this->resourceNotExist();
    }

    public function isAcceptUserGroupNull() : bool
    {
        return $this->resourceNotExist();
    }
}
