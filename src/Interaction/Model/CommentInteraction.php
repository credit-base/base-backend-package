<?php
namespace Base\Interaction\Model;

abstract class CommentInteraction extends Interaction
{
    /**
     * @var string $name 姓名/企业名称
     */
    protected $name;
    /**
     * @var string $identify 统一社会信用代码/反馈人身份证号
     */
    protected $identify;
    /**
     * @var int $type 类型
     */
    protected $type;
    /**
     * @var string $contact 联系方式
     */
    protected $contact;
    /**
     * @var array $images 图片
     */
    protected $images;

    public function __construct()
    {
        parent::__construct();
        $this->name = '';
        $this->identify = '';
        $this->type = IInteractionAble::TYPE['PERSONAL'];
        $this->contact = '';
        $this->images = array();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->name);
        unset($this->identify);
        unset($this->type);
        unset($this->contact);
        unset($this->images);
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setIdentify(string $identify) : void
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function setType(int $type) : void
    {
        $this->type = in_array($type, IInteractionAble::TYPE) ? $type : IInteractionAble::TYPE['PERSONAL'];
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function setContact(string $contact) : void
    {
        $this->contact = $contact;
    }

    public function getContact() : string
    {
        return $this->contact;
    }

    public function setImages(array $images) : void
    {
        $this->images = $images;
    }

    public function getImages() : array
    {
        return $this->images;
    }
}
