<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\Complaint\IComplaintAdapter;
use Base\Interaction\Repository\Complaint\ComplaintRepository;

class Complaint extends CommentInteraction
{
    /**
     * @var string $subject 被投诉主体
     */
    protected $subject;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->subject = '';
        $this->repository = new ComplaintRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->subject);
        unset($this->repository);
    }

    public function setSubject(string $subject) : void
    {
        $this->subject = $subject;
    }

    public function getSubject() : string
    {
        return $this->subject;
    }

    protected function getRepository() : IComplaintAdapter
    {
        return $this->repository;
    }
}
