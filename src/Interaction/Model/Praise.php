<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\Praise\IPraiseAdapter;
use Base\Interaction\Repository\Praise\PraiseRepository;

class Praise extends CommentInteraction
{
    /**
     * @var string $subject 被表扬主体
     */
    protected $subject;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->subject = '';
        $this->repository = new PraiseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->subject);
        unset($this->repository);
    }

    public function setSubject(string $subject) : void
    {
        $this->subject = $subject;
    }

    public function getSubject() : string
    {
        return $this->subject;
    }

    protected function getRepository() : IPraiseAdapter
    {
        return $this->repository;
    }
}
