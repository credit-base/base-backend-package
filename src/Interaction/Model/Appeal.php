<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\Appeal\IAppealAdapter;
use Base\Interaction\Repository\Appeal\AppealRepository;

class Appeal extends CommentInteraction
{
    /**
     * @var array $certificates 上传身份证/营业执照
     */
    protected $certificates;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->certificates = array();
        $this->repository = new AppealRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->certificates);
        unset($this->repository);
    }

    public function setCertificates(array $certificates) : void
    {
        $this->certificates = $certificates;
    }

    public function getCertificates() : array
    {
        return $this->certificates;
    }

    protected function getRepository() : IAppealAdapter
    {
        return $this->repository;
    }
}
