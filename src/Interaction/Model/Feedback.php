<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\Feedback\IFeedbackAdapter;
use Base\Interaction\Repository\Feedback\FeedbackRepository;

class Feedback extends Interaction
{
    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new FeedbackRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IFeedbackAdapter
    {
        return $this->repository;
    }
}
