<?php
namespace Base\Interaction\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Base\Common\Model\IOperate;
use Marmot\Common\Model\IObject;
use Base\Common\Model\OperateTrait;

use Base\Member\Model\Member;
use Base\UserGroup\Model\UserGroup;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
abstract class Interaction implements IObject, IInteractionAble, IOperate
{
    use Object, OperateTrait;

    /**
     * @var $id
     */
    protected $id;
    /**
     * @var string $title 标题
     */
    protected $title;
    /**
     * @var string $content 内容
     */
    protected $content;
    /**
     * @var Member $member 前台用户
     */
    protected $member;
    /**
     * @var UserGroup $acceptUserGroup 受理委办局
     */
    protected $acceptUserGroup;
    /**
     * @var int $acceptStatus 受理状态
     */
    protected $acceptStatus;
    /**
     * @var Reply $reply 回复信息
     */
    protected $reply;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->title = '';
        $this->content = '';
        $this->member = new Member();
        $this->acceptUserGroup = new UserGroup();
        $this->acceptStatus = IInteractionAble::ACCEPT_STATUS['PENDING'];
        $this->reply = new Reply();
        $this->status = IInteractionAble::INTERACTION_STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->content);
        unset($this->member);
        unset($this->acceptUserGroup);
        unset($this->acceptStatus);
        unset($this->reply);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    /**
     * @marmot-set-id
     */
    public function setId($id) : void
    {
        $this->id = $id;
    }
    /**
     * @marmot-get-id
     */
    public function getId()
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setAcceptUserGroup(UserGroup $acceptUserGroup) : void
    {
        $this->acceptUserGroup = $acceptUserGroup;
    }

    public function getAcceptUserGroup() : UserGroup
    {
        return $this->acceptUserGroup;
    }

    public function setAcceptStatus(int $acceptStatus) : void
    {
        $this->acceptStatus = in_array($acceptStatus, IInteractionAble::ACCEPT_STATUS) ?
                            $acceptStatus :
                            IInteractionAble::ACCEPT_STATUS['PENDING'];
    }

    public function getAcceptStatus() : int
    {
        return $this->acceptStatus;
    }

    public function setReply(Reply $reply) : void
    {
        $this->reply = $reply;
    }

    public function getReply() : Reply
    {
        return $this->reply;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, IInteractionAble::INTERACTION_STATUS) ?
                        $status :
                        IInteractionAble::INTERACTION_STATUS['NORMAL'];
    }

    abstract protected function getRepository();

    protected function isMemberNull() : bool
    {
        if ($this->getMember() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'memberId'));
            return false;
        }
        
        return true;
    }

    protected function isAcceptUserGroupNull() : bool
    {
        if ($this->getAcceptUserGroup() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'acceptUserGroupId'));
            return false;
        }
        
        return true;
    }

    protected function validate() : bool
    {
        if (!$this->isMemberNull() || !$this->isAcceptUserGroupNull()) {
            return false;
        }

        return true;
    }

    protected function addAction() : bool
    {
        return $this->validate() && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return false;
    }

    public function updateAcceptStatus(int $acceptStatus) : bool
    {
        $this->setAcceptStatus($acceptStatus);
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'acceptStatus',
                    'updateTime',
                    'reply'
                ));
    }

    protected function updateStatus(int $status) : bool
    {
        $this->setStatus($status);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'statusTime',
                    'status',
                    'updateTime'
                ));
    }

    public function accept() : bool
    {
        return $this->updateAcceptStatus(IInteractionAble::ACCEPT_STATUS['COMPLETE']);
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == IInteractionAble::INTERACTION_STATUS['NORMAL'];
    }

    public function isPublish() : bool
    {
        return $this->getStatus() == IInteractionAble::INTERACTION_STATUS['PUBLISH'];
    }
    
    public function isAcceptStatusPending() : bool
    {
        return $this->getAcceptStatus() == IInteractionAble::ACCEPT_STATUS['PENDING'];
    }

    public function isAcceptStatusComplete() : bool
    {
        return $this->getAcceptStatus() == IInteractionAble::ACCEPT_STATUS['COMPLETE'];
    }

    public function revoke() : bool
    {
        if (!$this->isNormal() || !$this->isAcceptStatusPending()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->updateStatus(IInteractionAble::INTERACTION_STATUS['REVOKE']);
    }

    public function publish() : bool
    {
        if (!$this->isAcceptStatusComplete()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->updateStatus(IInteractionAble::INTERACTION_STATUS['PUBLISH']);
    }

    public function unPublish() : bool
    {
        if (!$this->isPublish()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->updateStatus(IInteractionAble::INTERACTION_STATUS['NORMAL']);
    }
}
