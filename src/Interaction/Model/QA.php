<?php
namespace Base\Interaction\Model;

use Base\Interaction\Adapter\QA\IQAAdapter;
use Base\Interaction\Repository\QA\QARepository;

class QA extends Interaction
{
    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new QARepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IQAAdapter
    {
        return $this->repository;
    }
}
