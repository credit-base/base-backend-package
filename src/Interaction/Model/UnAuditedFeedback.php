<?php
namespace Base\Interaction\Model;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Model\ApplyFormTrait;
use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\Crew\Model\Crew;
use Base\Member\Model\Member;

use Base\Interaction\Translator\FeedbackDbTranslator;

class UnAuditedFeedback extends Feedback implements IApplyFormAble
{
    use ApplyFormTrait;
    
    const APPLY_FEEDBACK_CATEGORY = ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_FEEDBACK'];
    const APPLY_FEEDBACK_TYPE = ApplyInfoCategory::APPLY_INFO_TYPE[self::APPLY_FEEDBACK_CATEGORY]['FEEDBACK'];

    public $translator;

    public function __construct(int $applyId = 0)
    {
        parent::__construct();
        $this->applyId = !empty($applyId) ? $applyId : 0;
        $this->applyTitle = parent::getTitle();
        $this->relation = new Member();
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->applyInfoCategory  = new ApplyInfoCategory(
            self::APPLY_FEEDBACK_CATEGORY,
            self::APPLY_FEEDBACK_TYPE
        );
        $this->applyInfo = array();
        $this->applyCrew = new Crew();
        $this->applyUserGroup = parent::getAcceptUserGroup();
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->rejectReason = '';
        $this->translator = new FeedbackDbTranslator();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->applyId);
        unset($this->applyTitle);
        unset($this->relation);
        unset($this->operationType);
        unset($this->applyInfoCategory);
        unset($this->applyInfo);
        unset($this->applyUserGroup);
        unset($this->applyCrew);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->translator);
    }

    protected function getFeedbackDbTranslator() : FeedbackDbTranslator
    {
        return $this->translator;
    }

    public function add() : bool
    {
        $applyInfo = $this->applyInfo();

        $this->setApplyInfo($applyInfo);

        return $this->apply();
    }

    protected function applyInfo() : array
    {
        $applyInfo = $this->getFeedbackDbTranslator()->objectToArray($this);

        return $applyInfo;
    }
}
