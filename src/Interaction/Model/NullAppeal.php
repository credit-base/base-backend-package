<?php
namespace Base\Interaction\Model;

use Marmot\Interfaces\INull;

class NullAppeal extends Appeal implements INull
{
    use NullInteractionTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
