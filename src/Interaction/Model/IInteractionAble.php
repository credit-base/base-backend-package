<?php
namespace Base\Interaction\Model;

interface IInteractionAble
{
    /**
     * 受理状态
     * @var ACCEPT_STATUS['PENDING']  待受理 0
     * @var ACCEPT_STATUS['ACCEPTING']  受理中 1
     * @var ACCEPT_STATUS['COMPLETE']  受理完成 2
     */
    const ACCEPT_STATUS = array(
        'PENDING' => 0,
        'ACCEPTING' => 1,
        'COMPLETE' => 2
    );

    /**
     * 受理情况
     * @var ADMISSIBILITY['ADMISSIBLE']  予以受理 1
     * @var ADMISSIBILITY['INADMISSIBLE']  不予受理 2
     */
    const ADMISSIBILITY = array(
        'ADMISSIBLE' => 1,
        'INADMISSIBLE' => 2
    );

    /**
     * 类型
     * @var TYPE['PERSONAL']  个人 1
     * @var TYPE['ENTERPRISE']  企业 2
     * @var TYPE['GOVERNMENT']  政府 3
     */
    const TYPE = array(
        'PERSONAL' => 1,
        'ENTERPRISE' => 2,
        'GOVERNMENT' => 3
    );

    /**
     * 状态
     * @var INTERACTION_STATUS['NORMAL']  正常 0
     * @var INTERACTION_STATUS['PUBLISH']  撤销 -2
     * @var INTERACTION_STATUS['REVOKE']  公示 2
     */
    const INTERACTION_STATUS = array(
        'NORMAL' => 0,
        'REVOKE' => -2,
        'PUBLISH' => 2
    );
}
