<?php
namespace Base\Interaction\Model;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Model\ApplyFormTrait;
use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\Crew\Model\Crew;
use Base\Member\Model\Member;

use Base\Interaction\Translator\QADbTranslator;

class UnAuditedQA extends QA implements IApplyFormAble
{
    use ApplyFormTrait;
    
    const APPLY_QA_CATEGORY = ApplyInfoCategory::APPLY_INFO_CATEGORY['INTERACTION_QA'];
    const APPLY_QA_TYPE = ApplyInfoCategory::APPLY_INFO_TYPE[self::APPLY_QA_CATEGORY]['QA'];

    public $translator;

    public function __construct(int $applyId = 0)
    {
        parent::__construct();
        $this->applyId = !empty($applyId) ? $applyId : 0;
        $this->applyTitle = parent::getTitle();
        $this->relation = new Member();
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->applyInfoCategory  = new ApplyInfoCategory(
            self::APPLY_QA_CATEGORY,
            self::APPLY_QA_TYPE
        );
        $this->applyInfo = array();
        $this->applyCrew = new Crew();
        $this->applyUserGroup = parent::getAcceptUserGroup();
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->rejectReason = '';
        $this->translator = new QADbTranslator();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->applyId);
        unset($this->applyTitle);
        unset($this->relation);
        unset($this->operationType);
        unset($this->applyInfoCategory);
        unset($this->applyInfo);
        unset($this->applyUserGroup);
        unset($this->applyCrew);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->translator);
    }

    protected function getQADbTranslator() : QADbTranslator
    {
        return $this->translator;
    }

    public function add() : bool
    {
        $applyInfo = $this->applyInfo();

        $this->setApplyInfo($applyInfo);

        return $this->apply();
    }

    protected function applyInfo() : array
    {
        $applyInfo = $this->getQADbTranslator()->objectToArray($this);

        return $applyInfo;
    }
}
