<?php
namespace Base\Interaction\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Marmot\Common\Model\Object;
use Base\Common\Model\IOperate;
use Marmot\Common\Model\IObject;
use Base\Common\Model\OperateTrait;

use Base\Crew\Model\Crew;
use Base\UserGroup\Model\UserGroup;

use Base\Interaction\Adapter\Reply\IReplyAdapter;
use Base\Interaction\Repository\Reply\ReplyRepository;

class Reply implements IObject, IOperate
{
    use Object, OperateTrait;

    const STATUS_NORMAL = 0;

    /**
     * @var int $id
     */
    protected $id;
    /**
     * @var string $content 回复内容
     */
    protected $content;
    /**
     * @var array $images 回复图片
     */
    protected $images;
    /**
     * @var int $admissibility 受理情况
     */
    protected $admissibility;
    /**
     * @var Crew $crew 受理人
     */
    protected $crew;
    /**
     * @var UserGroup $acceptUserGroup 受理委办局
     */
    protected $acceptUserGroup;

    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->content = '';
        $this->images = array();
        $this->admissibility = IInteractionAble::ADMISSIBILITY['ADMISSIBLE'];
        $this->crew = new Crew();
        $this->acceptUserGroup = new UserGroup();
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new ReplyRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->content);
        unset($this->images);
        unset($this->admissibility);
        unset($this->crew);
        unset($this->acceptUserGroup);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function setImages(array $images) : void
    {
        $this->images = $images;
    }

    public function getImages() : array
    {
        return $this->images;
    }

    public function setAdmissibility(int $admissibility) : void
    {
        $this->admissibility = in_array($admissibility, IInteractionAble::ADMISSIBILITY) ?
                                $admissibility :
                                IInteractionAble::ADMISSIBILITY['ADMISSIBLE'];
    }

    public function getAdmissibility() : int
    {
        return $this->admissibility;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setAcceptUserGroup(UserGroup $acceptUserGroup) : void
    {
        $this->acceptUserGroup = $acceptUserGroup;
    }

    public function getAcceptUserGroup() : UserGroup
    {
        return $this->acceptUserGroup;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : IReplyAdapter
    {
        return $this->repository;
    }

    protected function isCrewNull() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crewId'));
            return false;
        }
        
        return true;
    }

    protected function isAcceptUserGroupNull() : bool
    {
        if ($this->getAcceptUserGroup() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'acceptUserGroupId'));
            return false;
        }
        
        return true;
    }

    protected function addAction() : bool
    {
        return $this->isCrewNull() && $this->isAcceptUserGroupNull() && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->isCrewNull() && $this->getRepository()->edit(
            $this,
            array(
                'crew',
                'images',
                'content',
                'admissibility',
                'updateTime'
            )
        );
    }
}
