<?php
namespace Base\Interaction\View\Reply;

use Neomerx\JsonApi\Schema\SchemaProvider;

class ReplySchema extends SchemaProvider
{
    protected $resourceType = 'replies';

    public function getId($reply) : int
    {
        return $reply->getId();
    }

    public function getAttributes($reply) : array
    {
        return [
            'content'  => $reply->getContent(),
            'images' => $reply->getImages(),
            'admissibility' => $reply->getAdmissibility(),
            'status' => $reply->getStatus(),
            'createTime' => $reply->getCreateTime(),
            'updateTime' => $reply->getUpdateTime(),
            'statusTime' => $reply->getStatusTime(),
        ];
    }

    public function getRelationships($reply, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'acceptUserGroup' => [self::DATA => $reply->getAcceptUserGroup()],
            'crew' => [self::DATA => $reply->getCrew()]
        ];
    }
}
