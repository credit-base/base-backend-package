<?php
namespace Base\Interaction\View\Feedback;

use Neomerx\JsonApi\Schema\SchemaProvider;

class UnAuditedFeedbackSchema extends FeedbackSchema
{
    protected $resourceType = 'unAuditedFeedbacks';

    public function getId($unAuditFeedback) : int
    {
        return $unAuditFeedback->getApplyId();
    }

    public function getAttributes($unAuditFeedback) : array
    {
        $attributes = parent::getAttributes($unAuditFeedback);
        $applyAttributes = array(
            'applyInfoCategory' => $unAuditFeedback->getApplyInfoCategory()->getCategory(),
            'applyInfoType' => $unAuditFeedback->getApplyInfoCategory()->getType(),
            'applyStatus'  => $unAuditFeedback->getApplyStatus(),
            'rejectReason' => $unAuditFeedback->getRejectReason(),
            'operationType'  => $unAuditFeedback->getOperationType(),
            'feedbackId'  => $unAuditFeedback->getId()
        );
        
        return array_merge($applyAttributes, $attributes);
    }

    public function getRelationships($unAuditFeedback, $isPrimary, array $includeList)
    {
        $relationships = parent::getRelationships($unAuditFeedback, $isPrimary, $includeList);
        $applyRelationships = [
            'relation' => [self::DATA => $unAuditFeedback->getRelation()],
            'applyCrew' => [self::DATA => $unAuditFeedback->getApplyCrew()],
            'applyUserGroup' => [self::DATA => $unAuditFeedback->getApplyUserGroup()],
        ];

        return array_merge($applyRelationships, $relationships);
    }
}
