<?php
namespace Base\Interaction\View\Feedback;

use Neomerx\JsonApi\Schema\SchemaProvider;

class FeedbackSchema extends SchemaProvider
{
    protected $resourceType = 'feedbacks';

    public function getId($feedback) : int
    {
        return $feedback->getId();
    }

    public function getAttributes($feedback) : array
    {
        return [
            'title'  => $feedback->getTitle(),
            'content'  => $feedback->getContent(),
            'acceptStatus' => $feedback->getAcceptStatus(),
            'status' => $feedback->getStatus(),
            'createTime' => $feedback->getCreateTime(),
            'updateTime' => $feedback->getUpdateTime(),
            'statusTime' => $feedback->getStatusTime(),
        ];
    }

    public function getRelationships($feedback, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'acceptUserGroup' => [self::DATA => $feedback->getAcceptUserGroup()],
            'member' => [self::DATA => $feedback->getMember()],
            'reply' => [self::DATA => $feedback->getReply()]
        ];
    }
}
