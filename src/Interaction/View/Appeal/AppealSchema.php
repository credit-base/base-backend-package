<?php
namespace Base\Interaction\View\Appeal;

use Neomerx\JsonApi\Schema\SchemaProvider;

class AppealSchema extends SchemaProvider
{
    protected $resourceType = 'appeals';

    public function getId($appeal) : int
    {
        return $appeal->getId();
    }

    public function getAttributes($appeal) : array
    {
        return [
            'title'  => $appeal->getTitle(),
            'content'  => $appeal->getContent(),
            'name' => $appeal->getName(),
            'identify'  => $appeal->getIdentify(),
            'appealType'  => $appeal->getType(),
            'contact'  => $appeal->getContact(),
            'images' => $appeal->getImages(),
            'acceptStatus' => $appeal->getAcceptStatus(),
            'certificates' => $appeal->getCertificates(),
            'status' => $appeal->getStatus(),
            'createTime' => $appeal->getCreateTime(),
            'updateTime' => $appeal->getUpdateTime(),
            'statusTime' => $appeal->getStatusTime(),
        ];
    }

    public function getRelationships($appeal, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'acceptUserGroup' => [self::DATA => $appeal->getAcceptUserGroup()],
            'member' => [self::DATA => $appeal->getMember()],
            'reply' => [self::DATA => $appeal->getReply()]
        ];
    }
}
