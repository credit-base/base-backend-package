<?php
namespace Base\Interaction\View\Appeal;

use Neomerx\JsonApi\Schema\SchemaProvider;

class UnAuditedAppealSchema extends AppealSchema
{
    protected $resourceType = 'unAuditedAppeals';

    public function getId($unAuditAppeal) : int
    {
        return $unAuditAppeal->getApplyId();
    }

    public function getAttributes($unAuditAppeal) : array
    {
        $attributes = parent::getAttributes($unAuditAppeal);
        $applyAttributes = array(
            'applyInfoCategory' => $unAuditAppeal->getApplyInfoCategory()->getCategory(),
            'applyInfoType' => $unAuditAppeal->getApplyInfoCategory()->getType(),
            'applyStatus'  => $unAuditAppeal->getApplyStatus(),
            'rejectReason' => $unAuditAppeal->getRejectReason(),
            'operationType'  => $unAuditAppeal->getOperationType(),
            'appealId'  => $unAuditAppeal->getId()
        );
        
        return array_merge($applyAttributes, $attributes);
    }

    public function getRelationships($unAuditAppeal, $isPrimary, array $includeList)
    {
        $relationships = parent::getRelationships($unAuditAppeal, $isPrimary, $includeList);
        $applyRelationships = [
            'relation' => [self::DATA => $unAuditAppeal->getRelation()],
            'applyCrew' => [self::DATA => $unAuditAppeal->getApplyCrew()],
            'applyUserGroup' => [self::DATA => $unAuditAppeal->getApplyUserGroup()],
        ];

        return array_merge($applyRelationships, $relationships);
    }
}
