<?php
namespace Base\Interaction\View\Complaint;

use Neomerx\JsonApi\Schema\SchemaProvider;

class ComplaintSchema extends SchemaProvider
{
    protected $resourceType = 'complaints';

    public function getId($complaint) : int
    {
        return $complaint->getId();
    }

    public function getAttributes($complaint) : array
    {
        return [
            'title'  => $complaint->getTitle(),
            'content'  => $complaint->getContent(),
            'name' => $complaint->getName(),
            'identify'  => $complaint->getIdentify(),
            'complaintType'  => $complaint->getType(),
            'contact'  => $complaint->getContact(),
            'images' => $complaint->getImages(),
            'acceptStatus' => $complaint->getAcceptStatus(),
            'subject' => $complaint->getSubject(),
            'status' => $complaint->getStatus(),
            'createTime' => $complaint->getCreateTime(),
            'updateTime' => $complaint->getUpdateTime(),
            'statusTime' => $complaint->getStatusTime(),
        ];
    }

    public function getRelationships($complaint, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'acceptUserGroup' => [self::DATA => $complaint->getAcceptUserGroup()],
            'member' => [self::DATA => $complaint->getMember()],
            'reply' => [self::DATA => $complaint->getReply()]
        ];
    }
}
