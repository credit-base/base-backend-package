<?php
namespace Base\Interaction\View\Complaint;

use Neomerx\JsonApi\Schema\SchemaProvider;

class UnAuditedComplaintSchema extends ComplaintSchema
{
    protected $resourceType = 'unAuditedComplaints';

    public function getId($unAuditComplaint) : int
    {
        return $unAuditComplaint->getApplyId();
    }

    public function getAttributes($unAuditComplaint) : array
    {
        $attributes = parent::getAttributes($unAuditComplaint);
        $applyAttributes = array(
            'applyInfoCategory' => $unAuditComplaint->getApplyInfoCategory()->getCategory(),
            'applyInfoType' => $unAuditComplaint->getApplyInfoCategory()->getType(),
            'applyStatus'  => $unAuditComplaint->getApplyStatus(),
            'rejectReason' => $unAuditComplaint->getRejectReason(),
            'operationType'  => $unAuditComplaint->getOperationType(),
            'complaintId'  => $unAuditComplaint->getId()
        );
        
        return array_merge($applyAttributes, $attributes);
    }

    public function getRelationships($unAuditComplaint, $isPrimary, array $includeList)
    {
        $relationships = parent::getRelationships($unAuditComplaint, $isPrimary, $includeList);
        $applyRelationships = [
            'relation' => [self::DATA => $unAuditComplaint->getRelation()],
            'applyCrew' => [self::DATA => $unAuditComplaint->getApplyCrew()],
            'applyUserGroup' => [self::DATA => $unAuditComplaint->getApplyUserGroup()],
        ];

        return array_merge($applyRelationships, $relationships);
    }
}
