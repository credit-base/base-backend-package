<?php
namespace Base\Interaction\View\Praise;

use Neomerx\JsonApi\Schema\SchemaProvider;

class PraiseSchema extends SchemaProvider
{
    protected $resourceType = 'praises';

    public function getId($praise) : int
    {
        return $praise->getId();
    }

    public function getAttributes($praise) : array
    {
        return [
            'title'  => $praise->getTitle(),
            'content'  => $praise->getContent(),
            'name' => $praise->getName(),
            'identify'  => $praise->getIdentify(),
            'praiseType'  => $praise->getType(),
            'contact'  => $praise->getContact(),
            'images' => $praise->getImages(),
            'acceptStatus' => $praise->getAcceptStatus(),
            'subject' => $praise->getSubject(),
            'status' => $praise->getStatus(),
            'createTime' => $praise->getCreateTime(),
            'updateTime' => $praise->getUpdateTime(),
            'statusTime' => $praise->getStatusTime(),
        ];
    }

    public function getRelationships($praise, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'acceptUserGroup' => [self::DATA => $praise->getAcceptUserGroup()],
            'member' => [self::DATA => $praise->getMember()],
            'reply' => [self::DATA => $praise->getReply()]
        ];
    }
}
