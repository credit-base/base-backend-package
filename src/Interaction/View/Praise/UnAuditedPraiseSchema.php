<?php
namespace Base\Interaction\View\Praise;

use Neomerx\JsonApi\Schema\SchemaProvider;

class UnAuditedPraiseSchema extends PraiseSchema
{
    protected $resourceType = 'unAuditedPraises';

    public function getId($unAuditPraise) : int
    {
        return $unAuditPraise->getApplyId();
    }

    public function getAttributes($unAuditPraise) : array
    {
        $attributes = parent::getAttributes($unAuditPraise);
        $applyAttributes = array(
            'applyInfoCategory' => $unAuditPraise->getApplyInfoCategory()->getCategory(),
            'applyInfoType' => $unAuditPraise->getApplyInfoCategory()->getType(),
            'applyStatus'  => $unAuditPraise->getApplyStatus(),
            'rejectReason' => $unAuditPraise->getRejectReason(),
            'operationType'  => $unAuditPraise->getOperationType(),
            'praiseId'  => $unAuditPraise->getId()
        );
        
        return array_merge($applyAttributes, $attributes);
    }

    public function getRelationships($unAuditPraise, $isPrimary, array $includeList)
    {
        $relationships = parent::getRelationships($unAuditPraise, $isPrimary, $includeList);
        $applyRelationships = [
            'relation' => [self::DATA => $unAuditPraise->getRelation()],
            'applyCrew' => [self::DATA => $unAuditPraise->getApplyCrew()],
            'applyUserGroup' => [self::DATA => $unAuditPraise->getApplyUserGroup()],
        ];

        return array_merge($applyRelationships, $relationships);
    }
}
