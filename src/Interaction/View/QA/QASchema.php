<?php
namespace Base\Interaction\View\QA;

use Neomerx\JsonApi\Schema\SchemaProvider;

class QASchema extends SchemaProvider
{
    protected $resourceType = 'qas';

    public function getId($qaInteraction) : int
    {
        return $qaInteraction->getId();
    }

    public function getAttributes($qaInteraction) : array
    {
        return [
            'title'  => $qaInteraction->getTitle(),
            'content'  => $qaInteraction->getContent(),
            'acceptStatus' => $qaInteraction->getAcceptStatus(),
            'status' => $qaInteraction->getStatus(),
            'createTime' => $qaInteraction->getCreateTime(),
            'updateTime' => $qaInteraction->getUpdateTime(),
            'statusTime' => $qaInteraction->getStatusTime(),
        ];
    }

    public function getRelationships($qaInteraction, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'acceptUserGroup' => [self::DATA => $qaInteraction->getAcceptUserGroup()],
            'member' => [self::DATA => $qaInteraction->getMember()],
            'reply' => [self::DATA => $qaInteraction->getReply()]
        ];
    }
}
