<?php
namespace Base\Interaction\View\QA;

use Neomerx\JsonApi\Schema\SchemaProvider;

class UnAuditedQASchema extends QASchema
{
    protected $resourceType = 'unAuditedQAs';

    public function getId($unAuditQA) : int
    {
        return $unAuditQA->getApplyId();
    }

    public function getAttributes($unAuditQA) : array
    {
        $attributes = parent::getAttributes($unAuditQA);
        $applyAttributes = array(
            'applyInfoCategory' => $unAuditQA->getApplyInfoCategory()->getCategory(),
            'applyInfoType' => $unAuditQA->getApplyInfoCategory()->getType(),
            'applyStatus'  => $unAuditQA->getApplyStatus(),
            'rejectReason' => $unAuditQA->getRejectReason(),
            'operationType'  => $unAuditQA->getOperationType(),
            'qaId'  => $unAuditQA->getId()
        );
        
        return array_merge($applyAttributes, $attributes);
    }

    public function getRelationships($unAuditQA, $isPrimary, array $includeList)
    {
        $relationships = parent::getRelationships($unAuditQA, $isPrimary, $includeList);
        $applyRelationships = [
            'relation' => [self::DATA => $unAuditQA->getRelation()],
            'applyCrew' => [self::DATA => $unAuditQA->getApplyCrew()],
            'applyUserGroup' => [self::DATA => $unAuditQA->getApplyUserGroup()],
        ];

        return array_merge($applyRelationships, $relationships);
    }
}
