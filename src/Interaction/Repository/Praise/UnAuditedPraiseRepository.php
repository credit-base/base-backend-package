<?php
namespace Base\Interaction\Repository\Praise;

use Base\ApplyForm\Repository\ApplyFormRepository;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

use Base\Interaction\Adapter\Praise\UnAuditedPraiseDbAdapter;
use Base\Interaction\Adapter\Praise\UnAuditedPraiseMockAdapter;

class UnAuditedPraiseRepository extends ApplyFormRepository
{
    public function __construct()
    {
        $this->adapter = new UnAuditedPraiseDbAdapter();
    }

    protected function getActualAdapter() : IApplyFormAdapter
    {
        return $this->adapter;
    }
    
    protected function getMockAdapter() : IApplyFormAdapter
    {
        return new UnAuditedPraiseMockAdapter();
    }
}
