<?php
namespace Base\Interaction\Repository\Praise;

use Marmot\Framework\Classes\Repository;

use Base\Interaction\Model\Praise;
use Base\Interaction\Adapter\Praise\IPraiseAdapter;
use Base\Interaction\Adapter\Praise\PraiseDbAdapter;
use Base\Interaction\Adapter\Praise\PraiseMockAdapter;

class PraiseRepository extends Repository implements IPraiseAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new PraiseDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IPraiseAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IPraiseAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IPraiseAdapter
    {
        return new PraiseMockAdapter();
    }

    public function fetchOne($id) : Praise
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Praise $praise) : bool
    {
        return $this->getAdapter()->add($praise);
    }

    public function edit(Praise $praise, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($praise, $keys);
    }
}
