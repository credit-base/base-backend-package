<?php
namespace Base\Interaction\Repository\Feedback;

use Marmot\Framework\Classes\Repository;

use Base\Interaction\Model\Feedback;
use Base\Interaction\Adapter\Feedback\IFeedbackAdapter;
use Base\Interaction\Adapter\Feedback\FeedbackDbAdapter;
use Base\Interaction\Adapter\Feedback\FeedbackMockAdapter;

class FeedbackRepository extends Repository implements IFeedbackAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new FeedbackDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IFeedbackAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IFeedbackAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IFeedbackAdapter
    {
        return new FeedbackMockAdapter();
    }

    public function fetchOne($id) : Feedback
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Feedback $feedback) : bool
    {
        return $this->getAdapter()->add($feedback);
    }

    public function edit(Feedback $feedback, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($feedback, $keys);
    }
}
