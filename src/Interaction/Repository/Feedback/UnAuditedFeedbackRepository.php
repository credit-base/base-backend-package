<?php
namespace Base\Interaction\Repository\Feedback;

use Base\ApplyForm\Repository\ApplyFormRepository;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

use Base\Interaction\Adapter\Feedback\UnAuditedFeedbackDbAdapter;
use Base\Interaction\Adapter\Feedback\UnAuditedFeedbackMockAdapter;

class UnAuditedFeedbackRepository extends ApplyFormRepository
{
    public function __construct()
    {
        $this->adapter = new UnAuditedFeedbackDbAdapter();
    }

    protected function getActualAdapter() : IApplyFormAdapter
    {
        return $this->adapter;
    }
    
    protected function getMockAdapter() : IApplyFormAdapter
    {
        return new UnAuditedFeedbackMockAdapter();
    }
}
