<?php
namespace Base\Interaction\Repository\Complaint;

use Marmot\Framework\Classes\Repository;

use Base\Interaction\Model\Complaint;
use Base\Interaction\Adapter\Complaint\IComplaintAdapter;
use Base\Interaction\Adapter\Complaint\ComplaintDbAdapter;
use Base\Interaction\Adapter\Complaint\ComplaintMockAdapter;

class ComplaintRepository extends Repository implements IComplaintAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new ComplaintDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IComplaintAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IComplaintAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IComplaintAdapter
    {
        return new ComplaintMockAdapter();
    }

    public function fetchOne($id) : Complaint
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Complaint $complaint) : bool
    {
        return $this->getAdapter()->add($complaint);
    }

    public function edit(Complaint $complaint, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($complaint, $keys);
    }
}
