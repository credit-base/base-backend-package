<?php
namespace Base\Interaction\Repository\Complaint;

use Base\ApplyForm\Repository\ApplyFormRepository;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

use Base\Interaction\Adapter\Complaint\UnAuditedComplaintDbAdapter;
use Base\Interaction\Adapter\Complaint\UnAuditedComplaintMockAdapter;

class UnAuditedComplaintRepository extends ApplyFormRepository
{
    public function __construct()
    {
        $this->adapter = new UnAuditedComplaintDbAdapter();
    }

    protected function getActualAdapter() : IApplyFormAdapter
    {
        return $this->adapter;
    }
    
    protected function getMockAdapter() : IApplyFormAdapter
    {
        return new UnAuditedComplaintMockAdapter();
    }
}
