<?php
namespace Base\Interaction\Repository\Reply;

use Marmot\Framework\Classes\Repository;

use Base\Interaction\Model\Reply;
use Base\Interaction\Adapter\Reply\IReplyAdapter;
use Base\Interaction\Adapter\Reply\ReplyDbAdapter;
use Base\Interaction\Adapter\Reply\ReplyMockAdapter;

class ReplyRepository extends Repository implements IReplyAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new ReplyDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IReplyAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IReplyAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IReplyAdapter
    {
        return new ReplyMockAdapter();
    }

    public function fetchOne($id) : Reply
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Reply $reply) : bool
    {
        return $this->getAdapter()->add($reply);
    }

    public function edit(Reply $reply, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($reply, $keys);
    }
}
