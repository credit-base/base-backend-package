<?php
namespace Base\Interaction\Repository\Appeal;

use Marmot\Framework\Classes\Repository;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Adapter\Appeal\IAppealAdapter;
use Base\Interaction\Adapter\Appeal\AppealDbAdapter;
use Base\Interaction\Adapter\Appeal\AppealMockAdapter;

class AppealRepository extends Repository implements IAppealAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new AppealDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IAppealAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IAppealAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IAppealAdapter
    {
        return new AppealMockAdapter();
    }

    public function fetchOne($id) : Appeal
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Appeal $appeal) : bool
    {
        return $this->getAdapter()->add($appeal);
    }

    public function edit(Appeal $appeal, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($appeal, $keys);
    }
}
