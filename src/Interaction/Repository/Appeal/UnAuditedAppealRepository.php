<?php
namespace Base\Interaction\Repository\Appeal;

use Base\ApplyForm\Repository\ApplyFormRepository;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

use Base\Interaction\Adapter\Appeal\UnAuditedAppealDbAdapter;
use Base\Interaction\Adapter\Appeal\UnAuditedAppealMockAdapter;

class UnAuditedAppealRepository extends ApplyFormRepository
{
    public function __construct()
    {
        $this->adapter = new UnAuditedAppealDbAdapter();
    }

    protected function getActualAdapter() : IApplyFormAdapter
    {
        return $this->adapter;
    }
    
    protected function getMockAdapter() : IApplyFormAdapter
    {
        return new UnAuditedAppealMockAdapter();
    }
}
