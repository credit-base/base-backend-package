<?php
namespace Base\Interaction\Repository\QA;

use Base\ApplyForm\Repository\ApplyFormRepository;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

use Base\Interaction\Adapter\QA\UnAuditedQADbAdapter;
use Base\Interaction\Adapter\QA\UnAuditedQAMockAdapter;

class UnAuditedQARepository extends ApplyFormRepository
{
    public function __construct()
    {
        $this->adapter = new UnAuditedQADbAdapter();
    }

    protected function getActualAdapter() : IApplyFormAdapter
    {
        return $this->adapter;
    }
    
    protected function getMockAdapter() : IApplyFormAdapter
    {
        return new UnAuditedQAMockAdapter();
    }
}
