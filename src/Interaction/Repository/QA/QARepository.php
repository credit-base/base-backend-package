<?php
namespace Base\Interaction\Repository\QA;

use Marmot\Framework\Classes\Repository;

use Base\Interaction\Model\QA;
use Base\Interaction\Adapter\QA\IQAAdapter;
use Base\Interaction\Adapter\QA\QADbAdapter;
use Base\Interaction\Adapter\QA\QAMockAdapter;

class QARepository extends Repository implements IQAAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new QADbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IQAAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IQAAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IQAAdapter
    {
        return new QAMockAdapter();
    }

    public function fetchOne($id) : QA
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(QA $qaInteraction) : bool
    {
        return $this->getAdapter()->add($qaInteraction);
    }

    public function edit(QA $qaInteraction, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($qaInteraction, $keys);
    }
}
