<?php
namespace Base\Interaction\Service;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\Reply;

class ResubmitAcceptInteractionService
{
    private $unAuditedInteraction;

    private $reply;

    public function __construct(IApplyFormAble $unAuditedInteraction, Reply $reply)
    {
        $this->unAuditedInteraction = $unAuditedInteraction;
        $this->reply = $reply;
    }

    public function __destruct()
    {
        unset($this->unAuditedInteraction);
        unset($this->reply);
    }

    protected function getUnAuditedInteraction() : IApplyFormAble
    {
        return $this->unAuditedInteraction;
    }

    protected function getReply() : Reply
    {
        return $this->reply;
    }

    public function resubmitAccept() : bool
    {
        $reply = $this->getReply();
        $unAuditedInteraction = $this->getUnAuditedInteraction();

        return $unAuditedInteraction->resubmit() && $reply->edit();
    }
}
