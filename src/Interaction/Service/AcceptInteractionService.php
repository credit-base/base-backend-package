<?php
namespace Base\Interaction\Service;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\IInteractionAble;

class AcceptInteractionService
{
    private $unAuditedInteraction;

    private $interaction;

    private $reply;

    public function __construct(IApplyFormAble $unAuditedInteraction, IInteractionAble $interaction, Reply $reply)
    {
        $this->unAuditedInteraction = $unAuditedInteraction;
        $this->interaction = $interaction;
        $this->reply = $reply;
    }

    public function __destruct()
    {
        unset($this->unAuditedInteraction);
        unset($this->interaction);
        unset($this->reply);
    }

    protected function getUnAuditedInteraction() : IApplyFormAble
    {
        return $this->unAuditedInteraction;
    }

    protected function getInteraction() : IInteractionAble
    {
        return $this->interaction;
    }

    protected function getReply() : Reply
    {
        return $this->reply;
    }

    public function accept() : bool
    {
        $unAuditedInteraction = $this->getUnAuditedInteraction();
        $interaction = $this->getInteraction();
        $reply = $this->getReply();

        if ($reply->add()) {
            $unAuditedInteraction->getReply()->setId($reply->getId());
            $interaction->getReply()->setId($reply->getId());

            return $unAuditedInteraction->add()
                && $interaction->updateAcceptStatus(IInteractionAble::ACCEPT_STATUS['ACCEPTING']);
        }

        return false;
    }
}
