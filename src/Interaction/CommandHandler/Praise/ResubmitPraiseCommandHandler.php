<?php
namespace Base\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Praise\ResubmitPraiseCommand;

class ResubmitPraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedPraise = $this->fetchUnAuditedPraise($command->id);
        $unAuditedPraise = $this->interactionResubmitExecuteAction($command, $unAuditedPraise);
        $service = $this->getResubmitAcceptInteractionService($unAuditedPraise, $unAuditedPraise->getReply());

        return $service->resubmitAccept();
    }
}
