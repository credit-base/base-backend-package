<?php
namespace Base\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Praise\AcceptPraiseCommand;

class AcceptPraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AcceptPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->fetchPraise($command->id);
        $unAuditedPraise = $this->commentInteractionAcceptExecuteAction($command, $praise);
        $unAuditedPraise->setSubject($praise->getSubject());

        $service = $this->getAcceptInteractionService(
            $unAuditedPraise,
            $praise,
            $unAuditedPraise->getReply()
        );

        if ($service->accept()) {
            $command->id = $unAuditedPraise->getApplyId();
            return true;
        }

        return false;
    }
}
