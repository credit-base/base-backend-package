<?php
namespace Base\Interaction\CommandHandler\Praise;

use Base\Interaction\Model\Praise;
use Base\Interaction\Model\UnAuditedPraise;
use Base\Interaction\Repository\Praise\PraiseRepository;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;
use Base\Interaction\CommandHandler\InteractionCommandHandlerTrait;

trait PraiseCommandHandlerTrait
{
    use InteractionCommandHandlerTrait;

    protected function getPraise() : Praise
    {
        return new Praise();
    }
    
    protected function getUnAuditedInteraction() : UnAuditedPraise
    {
        return new UnAuditedPraise();
    }

    protected function getPraiseRepository() : PraiseRepository
    {
        return new PraiseRepository();
    }

    protected function fetchPraise(int $id) : Praise
    {
        return $this->getPraiseRepository()->fetchOne($id);
    }

    protected function getUnAuditedPraiseRepository() : UnAuditedPraiseRepository
    {
        return new UnAuditedPraiseRepository();
    }

    protected function fetchUnAuditedPraise(int $id) : UnAuditedPraise
    {
        return $this->getUnAuditedPraiseRepository()->fetchOne($id);
    }
}
