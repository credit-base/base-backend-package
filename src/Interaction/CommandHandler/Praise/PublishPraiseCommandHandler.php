<?php
namespace Base\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Praise\PublishPraiseCommand;

class PublishPraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof PublishPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->fetchPraise($command->id);

        return $praise->publish();
    }
}
