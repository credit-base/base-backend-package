<?php
namespace Base\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Praise\AddPraiseCommand;

class AddPraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->getPraise();
        $praise = $this->commentInteractionAddExecuteAction($command, $praise);
        $praise->setSubject($command->subject);

        if ($praise->add()) {
            $command->id = $praise->getId();
            return true;
        }
        
        return false;
    }
}
