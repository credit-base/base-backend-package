<?php
namespace Base\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Praise\RevokePraiseCommand;

class RevokePraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokePraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->fetchPraise($command->id);

        return $praise->revoke();
    }
}
