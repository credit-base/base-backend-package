<?php
namespace Base\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Praise\UnPublishPraiseCommand;

class UnPublishPraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof UnPublishPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->fetchPraise($command->id);

        return $praise->unPublish();
    }
}
