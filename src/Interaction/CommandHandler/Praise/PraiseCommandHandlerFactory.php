<?php
namespace Base\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class PraiseCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Interaction\Command\Praise\AddPraiseCommand' =>
        'Base\Interaction\CommandHandler\Praise\AddPraiseCommandHandler',
        'Base\Interaction\Command\Praise\AcceptPraiseCommand' =>
        'Base\Interaction\CommandHandler\Praise\AcceptPraiseCommandHandler',
        'Base\Interaction\Command\Praise\RevokePraiseCommand' =>
        'Base\Interaction\CommandHandler\Praise\RevokePraiseCommandHandler',
        'Base\Interaction\Command\Praise\PublishPraiseCommand' =>
        'Base\Interaction\CommandHandler\Praise\PublishPraiseCommandHandler',
        'Base\Interaction\Command\Praise\UnPublishPraiseCommand' =>
        'Base\Interaction\CommandHandler\Praise\UnPublishPraiseCommandHandler',
        'Base\Interaction\Command\Praise\ResubmitPraiseCommand' =>
        'Base\Interaction\CommandHandler\Praise\ResubmitPraiseCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
