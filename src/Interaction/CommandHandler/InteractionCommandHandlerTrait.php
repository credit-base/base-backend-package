<?php
namespace Base\Interaction\CommandHandler;

use Marmot\Interfaces\ICommand;
use Base\ApplyForm\Model\IApplyFormAble;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Translator\ReplyDbTranslator;
use Base\Interaction\Repository\Reply\ReplyRepository;
use Base\Interaction\Service\AcceptInteractionService;
use Base\Interaction\Service\ResubmitAcceptInteractionService;

trait InteractionCommandHandlerTrait
{
    abstract protected function getUnAuditedInteraction() : IApplyFormAble;

    protected function getMemberRepository() : MemberRepository
    {
        return new MemberRepository();
    }

    protected function fetchMember(int $id) : Member
    {
        return $this->getMemberRepository()->fetchOne($id);
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return new UserGroupRepository();
    }

    protected function fetchUserGroup(int $id) : UserGroup
    {
        return $this->getUserGroupRepository()->fetchOne($id);
    }

    protected function getReplyRepository() : ReplyRepository
    {
        return new ReplyRepository();
    }

    protected function fetchReply(int $id) : Reply
    {
        return $this->getReplyRepository()->fetchOne($id);
    }

    protected function getCrewRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    protected function getReply() : Reply
    {
        return new Reply();
    }

    protected function getAcceptInteractionService(
        IApplyFormAble $unAuditedInteraction,
        IInteractionAble $interaction,
        Reply $reply
    ) : AcceptInteractionService {
        return new AcceptInteractionService($unAuditedInteraction, $interaction, $reply);
    }

    protected function getResubmitAcceptInteractionService(
        IApplyFormAble $unAuditedInteraction,
        Reply $reply
    ) : ResubmitAcceptInteractionService {
        return new ResubmitAcceptInteractionService($unAuditedInteraction, $reply);
    }

    protected function interactionAddExecuteAction(ICommand $command, IInteractionAble $interaction) : IInteractionAble
    {
        $member = $this->fetchMember($command->member);
        $acceptUserGroup = $this->fetchUserGroup($command->acceptUserGroup);

        $interaction->setTitle($command->title);
        $interaction->setContent($command->content);
        $interaction->setMember($member);
        $interaction->setAcceptUserGroup($acceptUserGroup);

        return $interaction;
    }

    protected function commentInteractionAddExecuteAction(
        ICommand $command,
        IInteractionAble $interaction
    ) : IInteractionAble {
        $interaction = $this->interactionAddExecuteAction($command, $interaction);
        $interaction->setName($command->name);
        $interaction->setIdentify($command->identify);
        $interaction->setType($command->type);
        $interaction->setContact($command->contact);
        $interaction->setImages($command->images);

        return $interaction;
    }

    protected function interactionAcceptExecuteAction(
        ICommand $command,
        IInteractionAble $interaction
    ) : IApplyFormAble {
        $crew = $this->fetchCrew($command->crew);
        $reply = $this->getReply();
        $reply->setContent($command->content);
        $reply->setImages($command->images);
        $reply->setAdmissibility($command->admissibility);
        $reply->setAcceptUserGroup($interaction->getAcceptUserGroup());
        $reply->setCrew($crew);

        $unAuditedInteraction = $this->getUnAuditedInteraction();
        $unAuditedInteraction->setOperationType($command->operationType);
        $unAuditedInteraction->setApplyTitle($interaction->getTitle());
        $unAuditedInteraction->setApplyUserGroup($interaction->getAcceptUserGroup());
        $unAuditedInteraction->setRelation($interaction->getMember());

        $unAuditedInteraction->setId($interaction->getId());
        $unAuditedInteraction->setTitle($interaction->getTitle());
        $unAuditedInteraction->setContent($interaction->getContent());
        $unAuditedInteraction->setMember($interaction->getMember());
        $unAuditedInteraction->setAcceptUserGroup($interaction->getAcceptUserGroup());
        $unAuditedInteraction->setReply($reply);

        return $unAuditedInteraction;
    }

    protected function commentInteractionAcceptExecuteAction(
        ICommand $command,
        IInteractionAble $interaction
    ) : IApplyFormAble {
        $unAuditedInteraction = $this->interactionAcceptExecuteAction($command, $interaction);

        $unAuditedInteraction->setName($interaction->getName());
        $unAuditedInteraction->setIdentify($interaction->getIdentify());
        $unAuditedInteraction->setType($interaction->getType());
        $unAuditedInteraction->setContact($interaction->getContact());
        $unAuditedInteraction->setImages($interaction->getImages());

        return $unAuditedInteraction;
    }

    protected function interactionResubmitExecuteAction(
        ICommand $command,
        IApplyFormAble $unAuditedInteraction
    ) : IApplyFormAble {
        $crew = $this->fetchCrew($command->crew);
        $reply = $unAuditedInteraction->getReply();
        $reply->setContent($command->content);
        $reply->setImages($command->images);
        $reply->setAdmissibility($command->admissibility);
        $reply->setAcceptUserGroup($unAuditedInteraction->getAcceptUserGroup());
        $reply->setCrew($crew);

        return $unAuditedInteraction;
    }
}
