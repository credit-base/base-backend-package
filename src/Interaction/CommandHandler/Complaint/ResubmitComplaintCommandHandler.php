<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Complaint\ResubmitComplaintCommand;

class ResubmitComplaintCommandHandler implements ICommandHandler
{
    use ComplaintCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitComplaintCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedComplaint = $this->fetchUnAuditedComplaint($command->id);
        $unAuditedComplaint = $this->interactionResubmitExecuteAction($command, $unAuditedComplaint);
        $service = $this->getResubmitAcceptInteractionService($unAuditedComplaint, $unAuditedComplaint->getReply());

        return $service->resubmitAccept();
    }
}
