<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ComplaintCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Interaction\Command\Complaint\AddComplaintCommand' =>
        'Base\Interaction\CommandHandler\Complaint\AddComplaintCommandHandler',
        'Base\Interaction\Command\Complaint\AcceptComplaintCommand' =>
        'Base\Interaction\CommandHandler\Complaint\AcceptComplaintCommandHandler',
        'Base\Interaction\Command\Complaint\RevokeComplaintCommand' =>
        'Base\Interaction\CommandHandler\Complaint\RevokeComplaintCommandHandler',
        'Base\Interaction\Command\Complaint\ResubmitComplaintCommand' =>
        'Base\Interaction\CommandHandler\Complaint\ResubmitComplaintCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
