<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Complaint\AddComplaintCommand;

class AddComplaintCommandHandler implements ICommandHandler
{
    use ComplaintCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddComplaintCommand)) {
            throw new \InvalidArgumentException;
        }

        $complaint = $this->getComplaint();
        $complaint = $this->commentInteractionAddExecuteAction($command, $complaint);
        $complaint->setSubject($command->subject);

        if ($complaint->add()) {
            $command->id = $complaint->getId();
            return true;
        }
        
        return false;
    }
}
