<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Complaint\RevokeComplaintCommand;

class RevokeComplaintCommandHandler implements ICommandHandler
{
    use ComplaintCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeComplaintCommand)) {
            throw new \InvalidArgumentException;
        }

        $complaint = $this->fetchComplaint($command->id);

        return $complaint->revoke();
    }
}
