<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Base\Interaction\Model\Complaint;
use Base\Interaction\Model\UnAuditedComplaint;
use Base\Interaction\Repository\Complaint\ComplaintRepository;
use Base\Interaction\CommandHandler\InteractionCommandHandlerTrait;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;

trait ComplaintCommandHandlerTrait
{
    use InteractionCommandHandlerTrait;

    protected function getComplaint() : Complaint
    {
        return new Complaint();
    }
    
    protected function getUnAuditedInteraction() : UnAuditedComplaint
    {
        return new UnAuditedComplaint();
    }

    protected function getComplaintRepository() : ComplaintRepository
    {
        return new ComplaintRepository();
    }

    protected function fetchComplaint(int $id) : Complaint
    {
        return $this->getComplaintRepository()->fetchOne($id);
    }

    protected function getUnAuditedComplaintRepository() : UnAuditedComplaintRepository
    {
        return new UnAuditedComplaintRepository();
    }

    protected function fetchUnAuditedComplaint(int $id) : UnAuditedComplaint
    {
        return $this->getUnAuditedComplaintRepository()->fetchOne($id);
    }
}
