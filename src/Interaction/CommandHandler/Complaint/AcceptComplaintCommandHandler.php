<?php
namespace Base\Interaction\CommandHandler\Complaint;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Complaint\AcceptComplaintCommand;

class AcceptComplaintCommandHandler implements ICommandHandler
{
    use ComplaintCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AcceptComplaintCommand)) {
            throw new \InvalidArgumentException;
        }

        $complaint = $this->fetchComplaint($command->id);

        $unAuditedComplaint = $this->commentInteractionAcceptExecuteAction($command, $complaint);
        $unAuditedComplaint->setSubject($complaint->getSubject());

        $service = $this->getAcceptInteractionService(
            $unAuditedComplaint,
            $complaint,
            $unAuditedComplaint->getReply()
        );

        if ($service->accept()) {
            $command->id = $unAuditedComplaint->getApplyId();
            return true;
        }

        return false;
    }
}
