<?php
namespace Base\Interaction\CommandHandler\QA;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\QA\PublishQACommand;

class PublishQACommandHandler implements ICommandHandler
{
    use QACommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof PublishQACommand)) {
            throw new \InvalidArgumentException;
        }

        $qaInteraction = $this->fetchQA($command->id);

        return $qaInteraction->publish();
    }
}
