<?php
namespace Base\Interaction\CommandHandler\QA;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\QA\AddQACommand;

class AddQACommandHandler implements ICommandHandler
{
    use QACommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddQACommand)) {
            throw new \InvalidArgumentException;
        }

        $qaInteraction = $this->getQA();
        $qaInteraction = $this->interactionAddExecuteAction($command, $qaInteraction);

        if ($qaInteraction->add()) {
            $command->id = $qaInteraction->getId();
            return true;
        }
        
        return false;
    }
}
