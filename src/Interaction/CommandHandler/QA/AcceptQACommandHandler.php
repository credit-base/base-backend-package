<?php
namespace Base\Interaction\CommandHandler\QA;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\QA\AcceptQACommand;

class AcceptQACommandHandler implements ICommandHandler
{
    use QACommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AcceptQACommand)) {
            throw new \InvalidArgumentException;
        }

        $qaInteraction = $this->fetchQA($command->id);
        $unAuditedQA = $this->interactionAcceptExecuteAction($command, $qaInteraction);

        $service = $this->getAcceptInteractionService(
            $unAuditedQA,
            $qaInteraction,
            $unAuditedQA->getReply()
        );

        if ($service->accept()) {
            $command->id = $unAuditedQA->getApplyId();
            return true;
        }

        return false;
    }
}
