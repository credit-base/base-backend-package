<?php
namespace Base\Interaction\CommandHandler\QA;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\QA\ResubmitQACommand;

class ResubmitQACommandHandler implements ICommandHandler
{
    use QACommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitQACommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedQA = $this->fetchUnAuditedQA($command->id);
        $unAuditedQA = $this->interactionResubmitExecuteAction($command, $unAuditedQA);
        $service = $this->getResubmitAcceptInteractionService($unAuditedQA, $unAuditedQA->getReply());

        return $service->resubmitAccept();
    }
}
