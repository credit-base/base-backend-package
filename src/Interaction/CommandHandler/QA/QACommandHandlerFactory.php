<?php
namespace Base\Interaction\CommandHandler\QA;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class QACommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Interaction\Command\QA\AddQACommand' =>
        'Base\Interaction\CommandHandler\QA\AddQACommandHandler',
        'Base\Interaction\Command\QA\AcceptQACommand' =>
        'Base\Interaction\CommandHandler\QA\AcceptQACommandHandler',
        'Base\Interaction\Command\QA\RevokeQACommand' =>
        'Base\Interaction\CommandHandler\QA\RevokeQACommandHandler',
        'Base\Interaction\Command\QA\PublishQACommand' =>
        'Base\Interaction\CommandHandler\QA\PublishQACommandHandler',
        'Base\Interaction\Command\QA\UnPublishQACommand' =>
        'Base\Interaction\CommandHandler\QA\UnPublishQACommandHandler',
        'Base\Interaction\Command\QA\ResubmitQACommand' =>
        'Base\Interaction\CommandHandler\QA\ResubmitQACommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
