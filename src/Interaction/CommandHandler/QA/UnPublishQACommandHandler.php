<?php
namespace Base\Interaction\CommandHandler\QA;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\QA\UnPublishQACommand;

class UnPublishQACommandHandler implements ICommandHandler
{
    use QACommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof UnPublishQACommand)) {
            throw new \InvalidArgumentException;
        }

        $qaInteraction = $this->fetchQA($command->id);

        return $qaInteraction->unPublish();
    }
}
