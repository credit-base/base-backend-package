<?php
namespace Base\Interaction\CommandHandler\QA;

use Base\Interaction\Model\QA;
use Base\Interaction\Model\UnAuditedQA;
use Base\Interaction\Repository\QA\QARepository;
use Base\Interaction\Repository\QA\UnAuditedQARepository;
use Base\Interaction\CommandHandler\InteractionCommandHandlerTrait;

trait QACommandHandlerTrait
{
    use InteractionCommandHandlerTrait;

    protected function getQA() : QA
    {
        return new QA();
    }
    
    protected function getUnAuditedInteraction() : UnAuditedQA
    {
        return new UnAuditedQA();
    }

    protected function getQARepository() : QARepository
    {
        return new QARepository();
    }

    protected function fetchQA(int $id) : QA
    {
        return $this->getQARepository()->fetchOne($id);
    }

    protected function getUnAuditedQARepository() : UnAuditedQARepository
    {
        return new UnAuditedQARepository();
    }

    protected function fetchUnAuditedQA(int $id) : UnAuditedQA
    {
        return $this->getUnAuditedQARepository()->fetchOne($id);
    }
}
