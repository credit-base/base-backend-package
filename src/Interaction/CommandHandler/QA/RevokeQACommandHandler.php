<?php
namespace Base\Interaction\CommandHandler\QA;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\QA\RevokeQACommand;

class RevokeQACommandHandler implements ICommandHandler
{
    use QACommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeQACommand)) {
            throw new \InvalidArgumentException;
        }

        $qaInteraction = $this->fetchQA($command->id);

        return $qaInteraction->revoke();
    }
}
