<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Feedback\RevokeFeedbackCommand;

class RevokeFeedbackCommandHandler implements ICommandHandler
{
    use FeedbackCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeFeedbackCommand)) {
            throw new \InvalidArgumentException;
        }

        $feedback = $this->fetchFeedback($command->id);

        return $feedback->revoke();
    }
}
