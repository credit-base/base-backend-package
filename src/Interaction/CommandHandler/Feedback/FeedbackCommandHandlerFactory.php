<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class FeedbackCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Interaction\Command\Feedback\AddFeedbackCommand' =>
        'Base\Interaction\CommandHandler\Feedback\AddFeedbackCommandHandler',
        'Base\Interaction\Command\Feedback\AcceptFeedbackCommand' =>
        'Base\Interaction\CommandHandler\Feedback\AcceptFeedbackCommandHandler',
        'Base\Interaction\Command\Feedback\RevokeFeedbackCommand' =>
        'Base\Interaction\CommandHandler\Feedback\RevokeFeedbackCommandHandler',
        'Base\Interaction\Command\Feedback\ResubmitFeedbackCommand' =>
        'Base\Interaction\CommandHandler\Feedback\ResubmitFeedbackCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
