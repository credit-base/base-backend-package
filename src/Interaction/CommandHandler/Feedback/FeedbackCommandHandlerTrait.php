<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Base\Interaction\Model\Feedback;
use Base\Interaction\Model\UnAuditedFeedback;
use Base\Interaction\Repository\Feedback\FeedbackRepository;
use Base\Interaction\CommandHandler\InteractionCommandHandlerTrait;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

trait FeedbackCommandHandlerTrait
{
    use InteractionCommandHandlerTrait;

    protected function getFeedback() : Feedback
    {
        return new Feedback();
    }
    
    protected function getUnAuditedInteraction() : UnAuditedFeedback
    {
        return new UnAuditedFeedback();
    }

    protected function getFeedbackRepository() : FeedbackRepository
    {
        return new FeedbackRepository();
    }

    protected function fetchFeedback(int $id) : Feedback
    {
        return $this->getFeedbackRepository()->fetchOne($id);
    }

    protected function getUnAuditedFeedbackRepository() : UnAuditedFeedbackRepository
    {
        return new UnAuditedFeedbackRepository();
    }

    protected function fetchUnAuditedFeedback(int $id) : UnAuditedFeedback
    {
        return $this->getUnAuditedFeedbackRepository()->fetchOne($id);
    }
}
