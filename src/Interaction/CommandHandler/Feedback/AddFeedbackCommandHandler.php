<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Feedback\AddFeedbackCommand;

class AddFeedbackCommandHandler implements ICommandHandler
{
    use FeedbackCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddFeedbackCommand)) {
            throw new \InvalidArgumentException;
        }

        $feedback = $this->getFeedback();
        $feedback = $this->interactionAddExecuteAction($command, $feedback);

        if ($feedback->add()) {
            $command->id = $feedback->getId();
            return true;
        }
        
        return false;
    }
}
