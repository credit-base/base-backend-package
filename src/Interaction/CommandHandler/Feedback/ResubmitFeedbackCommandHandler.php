<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Feedback\ResubmitFeedbackCommand;

class ResubmitFeedbackCommandHandler implements ICommandHandler
{
    use FeedbackCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitFeedbackCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedFeedback = $this->fetchUnAuditedFeedback($command->id);
        $unAuditedFeedback = $this->interactionResubmitExecuteAction($command, $unAuditedFeedback);
        $service = $this->getResubmitAcceptInteractionService($unAuditedFeedback, $unAuditedFeedback->getReply());

        return $service->resubmitAccept();
    }
}
