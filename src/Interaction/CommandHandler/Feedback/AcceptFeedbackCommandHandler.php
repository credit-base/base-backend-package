<?php
namespace Base\Interaction\CommandHandler\Feedback;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Feedback\AcceptFeedbackCommand;

class AcceptFeedbackCommandHandler implements ICommandHandler
{
    use FeedbackCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AcceptFeedbackCommand)) {
            throw new \InvalidArgumentException;
        }

        $feedback = $this->fetchFeedback($command->id);

        $unAuditedFeedback = $this->interactionAcceptExecuteAction($command, $feedback);

        $service = $this->getAcceptInteractionService(
            $unAuditedFeedback,
            $feedback,
            $unAuditedFeedback->getReply()
        );

        if ($service->accept()) {
            $command->id = $unAuditedFeedback->getApplyId();
            return true;
        }

        return false;
    }
}
