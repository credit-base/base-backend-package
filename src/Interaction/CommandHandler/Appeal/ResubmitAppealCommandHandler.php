<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Appeal\ResubmitAppealCommand;

class ResubmitAppealCommandHandler implements ICommandHandler
{
    use AppealCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitAppealCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedAppeal = $this->fetchUnAuditedAppeal($command->id);
        $unAuditedAppeal = $this->interactionResubmitExecuteAction($command, $unAuditedAppeal);
        $service = $this->getResubmitAcceptInteractionService($unAuditedAppeal, $unAuditedAppeal->getReply());

        return $service->resubmitAccept();
    }
}
