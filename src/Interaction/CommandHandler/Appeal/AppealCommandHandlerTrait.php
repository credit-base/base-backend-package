<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\Repository\Appeal\AppealRepository;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;
use Base\Interaction\CommandHandler\InteractionCommandHandlerTrait;

trait AppealCommandHandlerTrait
{
    use InteractionCommandHandlerTrait;

    protected function getAppeal() : Appeal
    {
        return new Appeal();
    }
    
    protected function getUnAuditedInteraction() : UnAuditedAppeal
    {
        return new UnAuditedAppeal();
    }

    protected function getAppealRepository() : AppealRepository
    {
        return new AppealRepository();
    }

    protected function fetchAppeal(int $id) : Appeal
    {
        return $this->getAppealRepository()->fetchOne($id);
    }

    protected function getUnAuditedAppealRepository() : UnAuditedAppealRepository
    {
        return new UnAuditedAppealRepository();
    }

    protected function fetchUnAuditedAppeal(int $id) : UnAuditedAppeal
    {
        return $this->getUnAuditedAppealRepository()->fetchOne($id);
    }
}
