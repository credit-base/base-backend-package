<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Appeal\AcceptAppealCommand;

class AcceptAppealCommandHandler implements ICommandHandler
{
    use AppealCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AcceptAppealCommand)) {
            throw new \InvalidArgumentException;
        }

        $appeal = $this->fetchAppeal($command->id);

        $unAuditedAppeal = $this->commentInteractionAcceptExecuteAction($command, $appeal);
        $unAuditedAppeal->setCertificates($appeal->getCertificates());

        $service = $this->getAcceptInteractionService(
            $unAuditedAppeal,
            $appeal,
            $unAuditedAppeal->getReply()
        );

        if ($service->accept()) {
            $command->id = $unAuditedAppeal->getApplyId();
            return true;
        }

        return false;
    }
}
