<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Appeal\AddAppealCommand;

class AddAppealCommandHandler implements ICommandHandler
{
    use AppealCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddAppealCommand)) {
            throw new \InvalidArgumentException;
        }

        $appeal = $this->getAppeal();
        $appeal = $this->commentInteractionAddExecuteAction($command, $appeal);
        $appeal->setCertificates($command->certificates);

        if ($appeal->add()) {
            $command->id = $appeal->getId();
            return true;
        }
        
        return false;
    }
}
