<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Interaction\Command\Appeal\RevokeAppealCommand;

class RevokeAppealCommandHandler implements ICommandHandler
{
    use AppealCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeAppealCommand)) {
            throw new \InvalidArgumentException;
        }

        $appeal = $this->fetchAppeal($command->id);

        return $appeal->revoke();
    }
}
