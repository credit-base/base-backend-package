<?php
namespace Base\Interaction\CommandHandler\Appeal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class AppealCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Interaction\Command\Appeal\AddAppealCommand' =>
        'Base\Interaction\CommandHandler\Appeal\AddAppealCommandHandler',
        'Base\Interaction\Command\Appeal\AcceptAppealCommand' =>
        'Base\Interaction\CommandHandler\Appeal\AcceptAppealCommandHandler',
        'Base\Interaction\Command\Appeal\RevokeAppealCommand' =>
        'Base\Interaction\CommandHandler\Appeal\RevokeAppealCommandHandler',
        'Base\Interaction\Command\Appeal\ResubmitAppealCommand' =>
        'Base\Interaction\CommandHandler\Appeal\ResubmitAppealCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
