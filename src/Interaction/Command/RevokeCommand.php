<?php
namespace Base\Interaction\Command;

use Marmot\Interfaces\ICommand;

class RevokeCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
