<?php
namespace Base\Interaction\Command;

use Marmot\Interfaces\ICommand;

class ReplyCommand implements ICommand
{
    public $content;

    public $images;

    public $admissibility;

    public $crew;

    public $id;

    public function __construct(
        string $content,
        array $images,
        int $admissibility,
        int $crew,
        int $id = 0
    ) {
        $this->content = $content;
        $this->images = $images;
        $this->admissibility = $admissibility;
        $this->crew = $crew;
        $this->id = $id;
    }
}
