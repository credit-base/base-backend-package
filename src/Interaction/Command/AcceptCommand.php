<?php
namespace Base\Interaction\Command;

use Base\Common\Model\IApproveAble;

class AcceptCommand extends ReplyCommand
{
    public $operationType;

    public function __construct(
        string $content,
        array $images,
        int $admissibility,
        int $crew,
        int $id = 0,
        int $operationType = IApproveAble::OPERATION_TYPE['ACCEPT']
    ) {
        parent::__construct(
            $content,
            $images,
            $admissibility,
            $crew,
            $id
        );

        $this->operationType = $operationType;
    }
}
