<?php
namespace Base\Interaction\Command;

/**
 * @todo
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 */
class CommentInteractionCommand extends InteractionCommand
{
    public $name;

    public $identify;

    public $contact;

    public $images;

    public $type;

    public function __construct(
        string $name,
        string $identify,
        string $contact,
        array $images,
        int $type,
        string $title,
        string $content,
        int $acceptUserGroup,
        int $member,
        int $id = 0
    ) {
        parent::__construct(
            $title,
            $content,
            $acceptUserGroup,
            $member,
            $id
        );

        $this->name = $name;
        $this->identify = $identify;
        $this->contact = $contact;
        $this->images = $images;
        $this->type = $type;
    }
}
