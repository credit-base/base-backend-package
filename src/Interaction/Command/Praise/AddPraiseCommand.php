<?php
namespace Base\Interaction\Command\Praise;

use Base\Interaction\Command\CommentInteractionCommand;

/**
 * @todo
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 */
class AddPraiseCommand extends CommentInteractionCommand
{
    public $subject;

    public function __construct(
        string $subject,
        string $name,
        string $identify,
        string $contact,
        array $images,
        int $type,
        string $title,
        string $content,
        int $acceptUserGroup,
        int $member,
        int $id = 0
    ) {
        parent::__construct(
            $name,
            $identify,
            $contact,
            $images,
            $type,
            $title,
            $content,
            $acceptUserGroup,
            $member,
            $id
        );

        $this->subject = $subject;
    }
}
