<?php
namespace Base\Interaction\Command\Appeal;

use Base\Interaction\Command\CommentInteractionCommand;

/**
 * @todo
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 */
class AddAppealCommand extends CommentInteractionCommand
{
    public $certificates;

    public function __construct(
        array $certificates,
        string $name,
        string $identify,
        string $contact,
        array $images,
        int $type,
        string $title,
        string $content,
        int $acceptUserGroup,
        int $member,
        int $id = 0
    ) {
        parent::__construct(
            $name,
            $identify,
            $contact,
            $images,
            $type,
            $title,
            $content,
            $acceptUserGroup,
            $member,
            $id
        );

        $this->certificates = $certificates;
    }
}
