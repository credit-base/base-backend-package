<?php
namespace Base\Interaction\Command;

use Marmot\Interfaces\ICommand;

class InteractionCommand implements ICommand
{
    public $title;

    public $content;

    public $acceptUserGroup;

    public $member;

    public $id;

    public function __construct(
        string $title,
        string $content,
        int $acceptUserGroup,
        int $member,
        int $id = 0
    ) {
        $this->title = $title;
        $this->content = $content;
        $this->acceptUserGroup = $acceptUserGroup;
        $this->member = $member;
        $this->id = $id;
    }
}
