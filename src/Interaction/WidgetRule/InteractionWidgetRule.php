<?php
namespace Base\Interaction\WidgetRule;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Base\Interaction\Model\IInteractionAble;

class InteractionWidgetRule
{
    const TITLE_MIN_LENGTH = 1;
    const TITLE_MAX_LENGTH = 50;

    public function title($title) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::TITLE_MIN_LENGTH,
            self::TITLE_MAX_LENGTH
        )->validate($title)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'title'));
            return false;
        }

        return true;
    }

    const CONTENT_MIN_LENGTH = 1;
    const CONTENT_MAX_LENGTH = 2000;

    public function content($content, $pointer = 'content') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::CONTENT_MIN_LENGTH,
            self::CONTENT_MAX_LENGTH
        )->validate($content)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    const NAME_MIN_LENGTH = 2;
    const NAME_MAX_LENGTH = 50;

    public function name($name) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'name'));
            return false;
        }

        return true;
    }

    public function identify($identify) : bool
    {
        $reg = '/[a-zA-Z0-9]/';
        if (!V::alnum()->noWhitespace()->length(13, 18)->validate($identify) ||
        !preg_match($reg, $identify)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'identify'));
            return false;
        }

        return true;
    }

    public function type($type) : bool
    {
        if (!V::numeric()->positive()->validate($type) || !in_array($type, IInteractionAble::TYPE)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'type'));
            return false;
        }
        
        return true;
    }

    const CONTACT_MIN_LENGTH = 1;
    const CONTACT_MAX_LENGTH = 26;

    public function contact($contact) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::CONTACT_MIN_LENGTH,
            self::CONTACT_MAX_LENGTH
        )->validate($contact)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'contact'));
            return false;
        }

        return true;
    }

    const IMAGES_MAX_COUNT = 9;
    public function images($images, $pointer = 'images') : bool
    {
        if (!V::arrayType()->validate($images)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (count($images) > self::IMAGES_MAX_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer.'Count'));
            return false;
        }

        foreach ($images as $image) {
            if (!$this->validateImageExtension($image['identify'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
                return false;
            }
        }
        return true;
    }

    private function validateImageExtension($image) : bool
    {
        if (!V::extension('png')->validate($image)
            && !V::extension('jpg')->validate($image)
            && !V::extension('jpeg')->validate($image)) {
            return false;
        }

        return true;
    }

    const SUBJECT_MIN_LENGTH = 2;
    const SUBJECT_MAX_LENGTH = 50;

    public function subject($subject) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::SUBJECT_MIN_LENGTH,
            self::SUBJECT_MAX_LENGTH
        )->validate($subject)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'subject'));
            return false;
        }

        return true;
    }

    public function admissibility($admissibility) : bool
    {
        if (!V::numeric()->positive()->validate($admissibility) ||
            !in_array($admissibility, IInteractionAble::ADMISSIBILITY)
        ) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'admissibility'));
            return false;
        }
        
        return true;
    }
}
