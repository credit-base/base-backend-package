<?php
namespace Base\Interaction\Controller\Praise;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\Common\Controller\Interfaces\IResubmitAbleController;

use Base\Interaction\View\Praise\UnAuditedPraiseView;
use Base\Interaction\Command\Praise\ResubmitPraiseCommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class PraiseResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, PraiseControllerTrait, InteractionAcceptControllerTrait;

    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedPraiseView($applyForm));
    }

    /**
     * 对应路由 /unAuditedPraise/{id:\d+}/resubmit
     * 重新受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new ResubmitPraiseCommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }
}
