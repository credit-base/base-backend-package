<?php
namespace Base\Interaction\Controller\Praise;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Interaction\View\Praise\PraiseView;
use Base\Interaction\Adapter\Praise\IPraiseAdapter;
use Base\Interaction\Repository\Praise\PraiseRepository;

class PraiseFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new PraiseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IPraiseAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new PraiseView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'praises';
    }
}
