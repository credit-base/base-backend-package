<?php
namespace Base\Interaction\Controller\Praise;

use Marmot\Framework\Classes\CommandBus;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Repository\Praise\PraiseRepository;
use Base\Interaction\Controller\InteractionControllerTrait;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;
use Base\Interaction\CommandHandler\Praise\PraiseCommandHandlerFactory;

trait PraiseControllerTrait
{
    use InteractionControllerTrait;

    protected function getRepository() : PraiseRepository
    {
        return new PraiseRepository();
    }

    protected function getUnAuditedInteractionRepository() : ApplyFormRepository
    {
        return new UnAuditedPraiseRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new PraiseCommandHandlerFactory());
    }
}
