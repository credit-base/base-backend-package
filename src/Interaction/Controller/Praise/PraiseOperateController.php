<?php
namespace Base\Interaction\Controller\Praise;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\Praise;
use Base\Interaction\View\Praise\PraiseView;
use Base\Interaction\View\Praise\UnAuditedPraiseView;
use Base\Interaction\Command\Praise\AddPraiseCommand;
use Base\Interaction\Command\Praise\RevokePraiseCommand;
use Base\Interaction\Command\Praise\AcceptPraiseCommand;
use Base\Interaction\Command\Praise\PublishPraiseCommand;
use Base\Interaction\Command\Praise\UnPublishPraiseCommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class PraiseOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, PraiseControllerTrait, InteractionAcceptControllerTrait;
    
    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedPraiseView($applyForm));
    }

    /**
     * 对应路由 /praises/{id:\d+}/accept
     * 受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new AcceptPraiseCommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }

    /**
     * 信用表扬新增功能,通过POST传参
     * 对应路由 /praises
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $name = $attributes['name'];
        $type = $attributes['type'];
        $title = $attributes['title'];
        $images = $attributes['images'];
        $subject = $attributes['subject'];
        $contact = $attributes['contact'];
        $content = $attributes['content'];
        $identify = $attributes['identify'];

        $member = $relationships['member']['data'][0]['id'];
        $acceptUserGroup = $relationships['acceptUserGroup']['data'][0]['id'];

        //验证
        if ($this->validateCommonScenario($title, $content, $member, $acceptUserGroup) &&
            $this->validateCommentCommonScenario($name, $identify, $type, $contact, $images) &&
            $this->validatePraiseAddScenario($subject)
        ) {
            //初始化命令
            $command = new AddPraiseCommand(
                $subject,
                $name,
                $identify,
                $contact,
                $images,
                $type,
                $title,
                $content,
                $acceptUserGroup,
                $member
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $praise = $this->getRepository()->fetchOne($command->id);
                if ($praise instanceof Praise) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new PraiseView($praise));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        return false;
    }

    /**
     * 信用表扬撤销功能,通过PATCH传参
     * 对应路由 /praises/{id:\d+}/revoke
     * @param int id 信用表扬 id
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        if (!empty($id)) {
            $command = new RevokePraiseCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $praise  = $this->getRepository()->fetchOne($id);
                if ($praise instanceof Praise) {
                    $this->render(new PraiseView($praise));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 信用表扬公示功能,通过PATCH传参
     * 对应路由 /praises/{id:\d+}/publish
     * @param int id 信用表扬 id
     * @return jsonApi
     */
    public function publish(int $id)
    {
        if (!empty($id)) {
            $command = new PublishPraiseCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $praise  = $this->getRepository()->fetchOne($id);
                if ($praise instanceof Praise) {
                    $this->render(new PraiseView($praise));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 信用表扬取消公示功能,通过PATCH传参
     * 对应路由 /praises/{id:\d+}/unPublish
     * @param int id 信用表扬 id
     * @return jsonApi
     */
    public function unPublish(int $id)
    {
        if (!empty($id)) {
            $command = new UnPublishPraiseCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $praise  = $this->getRepository()->fetchOne($id);
                if ($praise instanceof Praise) {
                    $this->render(new PraiseView($praise));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
