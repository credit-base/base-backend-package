<?php
namespace Base\Interaction\Controller\Praise;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Interaction\Model\UnAuditedPraise;
use Base\Interaction\View\Praise\UnAuditedPraiseView;
use Base\Interaction\Repository\Praise\UnAuditedPraiseRepository;

class UnAuditedPraiseFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditedPraiseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditedPraiseRepository
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new UnAuditedPraiseView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'unAuditedPraises';
    }

    /**
     * 根据检索条件查询数据,通过GET传参
     * 对应路由 /unAuditedPraises
     * @return jsonApi
     */
    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();
        $filter['applyInfoCategory'] = UnAuditedPraise::APPLY_PRAISE_CATEGORY;
        $filter['applyInfoType'] = UnAuditedPraise::APPLY_PRAISE_TYPE;

        list($objectList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = $this->generateView($objectList);
            $view->pagination(
                $this->getResourceName(),
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
