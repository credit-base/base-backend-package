<?php
namespace Base\Interaction\Controller;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Interaction\WidgetRule\InteractionWidgetRule;

trait InteractionControllerTrait
{
    protected function getInteractionWidgetRule() : InteractionWidgetRule
    {
        return new InteractionWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function validateCommonScenario(
        $title,
        $content,
        $member,
        $acceptUserGroup
    ) : bool {
        return $this->getInteractionWidgetRule()->title($title)
            && $this->getInteractionWidgetRule()->content($content, 'content')
            && $this->getCommonWidgetRule()->formatNumeric($member, 'memberId')
            && $this->getCommonWidgetRule()->formatNumeric($acceptUserGroup, 'acceptUserGroupId');
    }

    protected function validateCommentCommonScenario(
        $name,
        $identify,
        $type,
        $contact,
        $images
    ) : bool {
        return $this->getInteractionWidgetRule()->name($name)
            && $this->getInteractionWidgetRule()->identify($identify)
            && $this->getInteractionWidgetRule()->type($type)
            && $this->getInteractionWidgetRule()->contact($contact)
            && $this->getInteractionWidgetRule()->images($images, 'images');
    }

    protected function validateComplaintAddScenario(
        $subject
    ) : bool {
        return $this->getInteractionWidgetRule()->subject($subject);
    }

    protected function validatePraiseAddScenario(
        $subject
    ) : bool {
        return $this->getInteractionWidgetRule()->subject($subject);
    }

    protected function validateAppealAddScenario(
        $certificates
    ) : bool {
        return $this->getInteractionWidgetRule()->images($certificates, 'certificates');
    }

    protected function validateAcceptScenario(
        $replyContent,
        $replyImages,
        $admissibility,
        $crew
    ) : bool {
        return $this->getInteractionWidgetRule()->content($replyContent, 'replyContent')
            && $this->getInteractionWidgetRule()->images($replyImages, 'replyImages')
            && $this->getInteractionWidgetRule()->admissibility($admissibility)
            && $this->getCommonWidgetRule()->formatNumeric($crew, 'replyCrew');
    }
}
