<?php
namespace Base\Interaction\Controller\Appeal;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Interaction\View\Appeal\AppealView;
use Base\Interaction\Adapter\Appeal\IAppealAdapter;
use Base\Interaction\Repository\Appeal\AppealRepository;

class AppealFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new AppealRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IAppealAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new AppealView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'appeals';
    }
}
