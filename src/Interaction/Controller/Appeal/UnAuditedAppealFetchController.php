<?php
namespace Base\Interaction\Controller\Appeal;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Interaction\Model\UnAuditedAppeal;
use Base\Interaction\View\Appeal\UnAuditedAppealView;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;

class UnAuditedAppealFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditedAppealRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditedAppealRepository
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new UnAuditedAppealView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'unAuditedAppeals';
    }

    /**
     * 根据检索条件查询数据,通过GET传参
     * 对应路由 /unAuditedAppeals
     * @return jsonApi
     */
    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();
        $filter['applyInfoCategory'] = UnAuditedAppeal::APPLY_APPEAL_CATEGORY;
        $filter['applyInfoType'] = UnAuditedAppeal::APPLY_APPEAL_TYPE;

        list($objectList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = $this->generateView($objectList);
            $view->pagination(
                $this->getResourceName(),
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
