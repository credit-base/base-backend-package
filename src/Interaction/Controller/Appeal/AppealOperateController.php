<?php
namespace Base\Interaction\Controller\Appeal;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\Appeal;
use Base\Interaction\View\Appeal\AppealView;
use Base\Interaction\View\Appeal\UnAuditedAppealView;
use Base\Interaction\Command\Appeal\AddAppealCommand;
use Base\Interaction\Command\Appeal\RevokeAppealCommand;
use Base\Interaction\Command\Appeal\AcceptAppealCommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class AppealOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, AppealControllerTrait, InteractionAcceptControllerTrait;
    
    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedAppealView($applyForm));
    }

    /**
     * 对应路由 /appeals/{id:\d+}/accept
     * 受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new AcceptAppealCommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }

    /**
     * 异议申诉新增功能,通过POST传参
     * 对应路由 /appeals
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $content = $attributes['content'];
        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $certificates = $attributes['certificates'];
        $type = $attributes['type'];
        $contact = $attributes['contact'];
        $images = $attributes['images'];

        $member = $relationships['member']['data'][0]['id'];
        $acceptUserGroup = $relationships['acceptUserGroup']['data'][0]['id'];

        //验证
        if ($this->validateCommonScenario($title, $content, $member, $acceptUserGroup) &&
            $this->validateCommentCommonScenario($name, $identify, $type, $contact, $images) &&
            $this->validateAppealAddScenario($certificates)
        ) {
            //初始化命令
            $command = new AddAppealCommand(
                $certificates,
                $name,
                $identify,
                $contact,
                $images,
                $type,
                $title,
                $content,
                $acceptUserGroup,
                $member
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $appeal = $this->getRepository()->fetchOne($command->id);
                if ($appeal instanceof Appeal) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new AppealView($appeal));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        return false;
    }

    /**
     * 异议申诉撤销功能,通过PATCH传参
     * 对应路由 /appeals/{id:\d+}/revoke
     * @param int id 异议申诉 id
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        if (!empty($id)) {
            $command = new RevokeAppealCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $appeal  = $this->getRepository()->fetchOne($id);
                if ($appeal instanceof Appeal) {
                    $this->render(new AppealView($appeal));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
