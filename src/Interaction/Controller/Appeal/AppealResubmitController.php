<?php
namespace Base\Interaction\Controller\Appeal;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\Common\Controller\Interfaces\IResubmitAbleController;

use Base\Interaction\View\Appeal\UnAuditedAppealView;
use Base\Interaction\Command\Appeal\ResubmitAppealCommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class AppealResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, AppealControllerTrait, InteractionAcceptControllerTrait;

    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedAppealView($applyForm));
    }

    /**
     * 对应路由 /unAuditedAppeal/{id:\d+}/resubmit
     * 重新受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new ResubmitAppealCommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }
}
