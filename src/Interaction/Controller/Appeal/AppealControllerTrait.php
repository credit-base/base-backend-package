<?php
namespace Base\Interaction\Controller\Appeal;

use Marmot\Framework\Classes\CommandBus;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Repository\Appeal\AppealRepository;
use Base\Interaction\Controller\InteractionControllerTrait;
use Base\Interaction\Repository\Appeal\UnAuditedAppealRepository;
use Base\Interaction\CommandHandler\Appeal\AppealCommandHandlerFactory;

trait AppealControllerTrait
{
    use InteractionControllerTrait;

    protected function getRepository() : AppealRepository
    {
        return new AppealRepository();
    }

    protected function getUnAuditedInteractionRepository() : ApplyFormRepository
    {
        return new UnAuditedAppealRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new AppealCommandHandlerFactory());
    }
}
