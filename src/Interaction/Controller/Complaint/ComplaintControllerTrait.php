<?php
namespace Base\Interaction\Controller\Complaint;

use Marmot\Framework\Classes\CommandBus;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Controller\InteractionControllerTrait;
use Base\Interaction\Repository\Complaint\ComplaintRepository;
use Base\Interaction\Repository\Complaint\UnAuditedComplaintRepository;
use Base\Interaction\CommandHandler\Complaint\ComplaintCommandHandlerFactory;

trait ComplaintControllerTrait
{
    use InteractionControllerTrait;

    protected function getRepository() : ComplaintRepository
    {
        return new ComplaintRepository();
    }

    protected function getUnAuditedInteractionRepository() : ApplyFormRepository
    {
        return new UnAuditedComplaintRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new ComplaintCommandHandlerFactory());
    }
}
