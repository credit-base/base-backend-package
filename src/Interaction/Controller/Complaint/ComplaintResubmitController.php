<?php
namespace Base\Interaction\Controller\Complaint;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\Common\Controller\Interfaces\IResubmitAbleController;

use Base\Interaction\View\Complaint\UnAuditedComplaintView;
use Base\Interaction\Command\Complaint\ResubmitComplaintCommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class ComplaintResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, ComplaintControllerTrait, InteractionAcceptControllerTrait;

    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedComplaintView($applyForm));
    }

    /**
     * 对应路由 /unAuditedComplaint/{id:\d+}/resubmit
     * 重新受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new ResubmitComplaintCommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }
}
