<?php
namespace Base\Interaction\Controller\Complaint;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\Complaint;
use Base\Interaction\View\Complaint\ComplaintView;
use Base\Interaction\View\Complaint\UnAuditedComplaintView;
use Base\Interaction\Command\Complaint\AddComplaintCommand;
use Base\Interaction\Command\Complaint\RevokeComplaintCommand;
use Base\Interaction\Command\Complaint\AcceptComplaintCommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class ComplaintOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, ComplaintControllerTrait, InteractionAcceptControllerTrait;
    
    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedComplaintView($applyForm));
    }

    /**
     * 对应路由 /complaints/{id:\d+}/accept
     * 受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new AcceptComplaintCommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }

    /**
     * 信用投诉新增功能,通过POST传参
     * 对应路由 /complaints
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $content = $attributes['content'];
        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $subject = $attributes['subject'];
        $type = $attributes['type'];
        $contact = $attributes['contact'];
        $images = $attributes['images'];

        $member = $relationships['member']['data'][0]['id'];
        $acceptUserGroup = $relationships['acceptUserGroup']['data'][0]['id'];

        //验证
        if ($this->validateCommonScenario($title, $content, $member, $acceptUserGroup) &&
            $this->validateCommentCommonScenario($name, $identify, $type, $contact, $images) &&
            $this->validateComplaintAddScenario($subject)
        ) {
            //初始化命令
            $command = new AddComplaintCommand(
                $subject,
                $name,
                $identify,
                $contact,
                $images,
                $type,
                $title,
                $content,
                $acceptUserGroup,
                $member
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $complaint = $this->getRepository()->fetchOne($command->id);
                if ($complaint instanceof Complaint) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new ComplaintView($complaint));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        return false;
    }

    /**
     * 信用投诉撤销功能,通过PATCH传参
     * 对应路由 /complaints/{id:\d+}/revoke
     * @param int id 信用投诉 id
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        if (!empty($id)) {
            $command = new RevokeComplaintCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $complaint  = $this->getRepository()->fetchOne($id);
                if ($complaint instanceof Complaint) {
                    $this->render(new ComplaintView($complaint));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
