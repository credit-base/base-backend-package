<?php
namespace Base\Interaction\Controller\Complaint;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Interaction\View\Complaint\ComplaintView;
use Base\Interaction\Adapter\Complaint\IComplaintAdapter;
use Base\Interaction\Repository\Complaint\ComplaintRepository;

class ComplaintFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ComplaintRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IComplaintAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new ComplaintView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'complaints';
    }
}
