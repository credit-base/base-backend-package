<?php
namespace Base\Interaction\Controller;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\CommandBus;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Repository\ApplyFormRepository;

trait InteractionAcceptControllerTrait
{
    abstract protected function getCommandBus() : CommandBus;
    abstract protected function getUnAuditedInteractionRepository() : ApplyFormRepository;
    abstract protected function displaySuccess(IApplyFormAble $applyForm);
    abstract protected function getAcceptCommand(array $data) : ICommand;
    abstract protected function validateAcceptScenario(
        $replyContent,
        $replyImages,
        $admissibility,
        $crew
    ) : bool;

    public function accept(int $id)
    {
        return $this->acceptInteraction($id);
    }

    public function resubmit(int $id)
    {
        return $this->acceptInteraction($id);
    }

    protected function acceptInteraction(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $relationships = $data['relationships'];

        $replyAttributes = $relationships['reply']['data'][0]['attributes'];
        $replyRelationships = $relationships['reply']['data'][0]['relationships'];

        $acceptData = array();
        $acceptData['replyContent'] = $replyAttributes['content'];
        $acceptData['replyImages'] = $replyAttributes['images'];
        $acceptData['admissibility'] = $replyAttributes['admissibility'];
        $acceptData['crew'] = $replyRelationships['crew']['data'][0]['id'];
        $acceptData['id'] = $id;

        if ($this->validateAcceptScenario(
            $acceptData['replyContent'],
            $acceptData['replyImages'],
            $acceptData['admissibility'],
            $acceptData['crew']
        )) {
            $command = $this->getAcceptCommand($acceptData);

            if ($this->getCommandBus()->send($command)) {
                $interaction = $this->getUnAuditedInteractionRepository()->fetchOne($command->id);

                if ($interaction instanceof IApplyFormAble) {
                    $this->displaySuccess($interaction);
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
