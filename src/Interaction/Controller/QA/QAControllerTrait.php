<?php
namespace Base\Interaction\Controller\QA;

use Marmot\Framework\Classes\CommandBus;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Repository\QA\QARepository;
use Base\Interaction\Repository\QA\UnAuditedQARepository;
use Base\Interaction\Controller\InteractionControllerTrait;
use Base\Interaction\CommandHandler\QA\QACommandHandlerFactory;

trait QAControllerTrait
{
    use InteractionControllerTrait;

    protected function getRepository() : QARepository
    {
        return new QARepository();
    }

    protected function getUnAuditedInteractionRepository() : ApplyFormRepository
    {
        return new UnAuditedQARepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new QACommandHandlerFactory());
    }
}
