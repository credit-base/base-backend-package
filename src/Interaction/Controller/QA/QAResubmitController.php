<?php
namespace Base\Interaction\Controller\QA;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\Common\Controller\Interfaces\IResubmitAbleController;

use Base\Interaction\View\QA\UnAuditedQAView;
use Base\Interaction\Command\QA\ResubmitQACommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class QAResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, QAControllerTrait, InteractionAcceptControllerTrait;

    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedQAView($applyForm));
    }

    /**
     * 对应路由 /unAuditedQA/{id:\d+}/resubmit
     * 重新受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new ResubmitQACommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }
}
