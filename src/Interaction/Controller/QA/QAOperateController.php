<?php
namespace Base\Interaction\Controller\QA;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\QA;
use Base\Interaction\View\QA\QAView;
use Base\Interaction\View\QA\UnAuditedQAView;
use Base\Interaction\Command\QA\AddQACommand;
use Base\Interaction\Command\QA\RevokeQACommand;
use Base\Interaction\Command\QA\AcceptQACommand;
use Base\Interaction\Command\QA\PublishQACommand;
use Base\Interaction\Command\QA\UnPublishQACommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class QAOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, QAControllerTrait, InteractionAcceptControllerTrait;
    
    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedQAView($applyForm));
    }

    /**
     * 对应路由 /qas/{id:\d+}/accept
     * 受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new AcceptQACommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }

    /**
     * 信用问答新增功能,通过POST传参
     * 对应路由 /qas
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $content = $attributes['content'];

        $member = $relationships['member']['data'][0]['id'];
        $acceptUserGroup = $relationships['acceptUserGroup']['data'][0]['id'];

        //验证
        if ($this->validateCommonScenario($title, $content, $member, $acceptUserGroup)) {
            //初始化命令
            $command = new AddQACommand(
                $title,
                $content,
                $acceptUserGroup,
                $member
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $qaInteraction = $this->getRepository()->fetchOne($command->id);
                if ($qaInteraction instanceof QA) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new QAView($qaInteraction));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        return false;
    }

    /**
     * 信用问答撤销功能,通过PATCH传参
     * 对应路由 /qas/{id:\d+}/revoke
     * @param int id 信用问答 id
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        if (!empty($id)) {
            $command = new RevokeQACommand($id);

            if ($this->getCommandBus()->send($command)) {
                $qaInteraction  = $this->getRepository()->fetchOne($id);
                if ($qaInteraction instanceof QA) {
                    $this->render(new QAView($qaInteraction));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 信用问答公示功能,通过PATCH传参
     * 对应路由 /qas/{id:\d+}/publish
     * @param int id 信用问答 id
     * @return jsonApi
     */
    public function publish(int $id)
    {
        if (!empty($id)) {
            $command = new PublishQACommand($id);

            if ($this->getCommandBus()->send($command)) {
                $qaInteraction  = $this->getRepository()->fetchOne($id);
                if ($qaInteraction instanceof QA) {
                    $this->render(new QAView($qaInteraction));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 信用问答取消公示功能,通过PATCH传参
     * 对应路由 /qas/{id:\d+}/unPublish
     * @param int id 信用问答 id
     * @return jsonApi
     */
    public function unPublish(int $id)
    {
        if (!empty($id)) {
            $command = new UnPublishQACommand($id);

            if ($this->getCommandBus()->send($command)) {
                $qaInteraction  = $this->getRepository()->fetchOne($id);
                if ($qaInteraction instanceof QA) {
                    $this->render(new QAView($qaInteraction));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
