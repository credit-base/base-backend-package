<?php
namespace Base\Interaction\Controller\QA;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Interaction\View\QA\QAView;
use Base\Interaction\Adapter\QA\IQAAdapter;
use Base\Interaction\Repository\QA\QARepository;

class QAFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new QARepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IQAAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new QAView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'qas';
    }
}
