<?php
namespace Base\Interaction\Controller\QA;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Interaction\Model\UnAuditedQA;
use Base\Interaction\View\QA\UnAuditedQAView;
use Base\Interaction\Repository\QA\UnAuditedQARepository;

class UnAuditedQAFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditedQARepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditedQARepository
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new UnAuditedQAView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'unAuditedQAs';
    }

    /**
     * 根据检索条件查询数据,通过GET传参
     * 对应路由 /unAuditedQAs
     * @return jsonApi
     */
    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();
        $filter['applyInfoCategory'] = UnAuditedQA::APPLY_QA_CATEGORY;
        $filter['applyInfoType'] = UnAuditedQA::APPLY_QA_TYPE;

        list($objectList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = $this->generateView($objectList);
            $view->pagination(
                $this->getResourceName(),
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
