<?php
namespace Base\Interaction\Controller\Feedback;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Interaction\Model\UnAuditedFeedback;
use Base\Interaction\View\Feedback\UnAuditedFeedbackView;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;

class UnAuditedFeedbackFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditedFeedbackRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditedFeedbackRepository
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new UnAuditedFeedbackView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'unAuditedFeedbacks';
    }

    /**
     * 根据检索条件查询数据,通过GET传参
     * 对应路由 /unAuditedFeedbacks
     * @return jsonApi
     */
    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();
        $filter['applyInfoCategory'] = UnAuditedFeedback::APPLY_FEEDBACK_CATEGORY;
        $filter['applyInfoType'] = UnAuditedFeedback::APPLY_FEEDBACK_TYPE;

        list($objectList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = $this->generateView($objectList);
            $view->pagination(
                $this->getResourceName(),
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
