<?php
namespace Base\Interaction\Controller\Feedback;

use Marmot\Framework\Classes\CommandBus;
use Base\ApplyForm\Repository\ApplyFormRepository;

use Base\Interaction\Controller\InteractionControllerTrait;
use Base\Interaction\Repository\Feedback\FeedbackRepository;
use Base\Interaction\Repository\Feedback\UnAuditedFeedbackRepository;
use Base\Interaction\CommandHandler\Feedback\FeedbackCommandHandlerFactory;

trait FeedbackControllerTrait
{
    use InteractionControllerTrait;

    protected function getRepository() : FeedbackRepository
    {
        return new FeedbackRepository();
    }

    protected function getUnAuditedInteractionRepository() : ApplyFormRepository
    {
        return new UnAuditedFeedbackRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new FeedbackCommandHandlerFactory());
    }
}
