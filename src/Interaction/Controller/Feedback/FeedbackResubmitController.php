<?php
namespace Base\Interaction\Controller\Feedback;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\Common\Controller\Interfaces\IResubmitAbleController;

use Base\Interaction\View\Feedback\UnAuditedFeedbackView;
use Base\Interaction\Command\Feedback\ResubmitFeedbackCommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class FeedbackResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, FeedbackControllerTrait, InteractionAcceptControllerTrait;

    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedFeedbackView($applyForm));
    }

    /**
     * 对应路由 /unAuditedFeedback/{id:\d+}/resubmit
     * 重新受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new ResubmitFeedbackCommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }
}
