<?php
namespace Base\Interaction\Controller\Feedback;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Interaction\View\Feedback\FeedbackView;
use Base\Interaction\Adapter\Feedback\IFeedbackAdapter;
use Base\Interaction\Repository\Feedback\FeedbackRepository;

class FeedbackFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new FeedbackRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IFeedbackAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new FeedbackView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'feedbacks';
    }
}
