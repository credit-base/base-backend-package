<?php
namespace Base\Interaction\Controller\Feedback;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\ApplyForm\Model\IApplyFormAble;

use Base\Interaction\Model\Feedback;
use Base\Interaction\View\Feedback\FeedbackView;
use Base\Interaction\View\Feedback\UnAuditedFeedbackView;
use Base\Interaction\Command\Feedback\AddFeedbackCommand;
use Base\Interaction\Command\Feedback\RevokeFeedbackCommand;
use Base\Interaction\Command\Feedback\AcceptFeedbackCommand;
use Base\Interaction\Controller\InteractionAcceptControllerTrait;

class FeedbackOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, FeedbackControllerTrait, InteractionAcceptControllerTrait;
    
    protected function displaySuccess(IApplyFormAble $applyForm)
    {
        $this->render(new UnAuditedFeedbackView($applyForm));
    }

    /**
     * 对应路由 /feedbacks/{id:\d+}/accept
     * 受理, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    protected function getAcceptCommand(array $data) : ICommand
    {
        return new AcceptFeedbackCommand(
            $data['replyContent'],
            $data['replyImages'],
            $data['admissibility'],
            $data['crew'],
            $data['id']
        );
    }

    /**
     * 问题反馈新增功能,通过POST传参
     * 对应路由 /feedbacks
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $content = $attributes['content'];

        $member = $relationships['member']['data'][0]['id'];
        $acceptUserGroup = $relationships['acceptUserGroup']['data'][0]['id'];

        //验证
        if ($this->validateCommonScenario($title, $content, $member, $acceptUserGroup)) {
            //初始化命令
            $command = new AddFeedbackCommand(
                $title,
                $content,
                $acceptUserGroup,
                $member
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $feedback = $this->getRepository()->fetchOne($command->id);
                if ($feedback instanceof Feedback) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new FeedbackView($feedback));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        return false;
    }

    /**
     * 问题反馈撤销功能,通过PATCH传参
     * 对应路由 /feedbacks/{id:\d+}/revoke
     * @param int id 问题反馈 id
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        if (!empty($id)) {
            $command = new RevokeFeedbackCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $feedback  = $this->getRepository()->fetchOne($id);
                if ($feedback instanceof Feedback) {
                    $this->render(new FeedbackView($feedback));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
