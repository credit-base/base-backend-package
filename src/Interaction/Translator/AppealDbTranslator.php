<?php
namespace Base\Interaction\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Model\NullAppeal;

class AppealDbTranslator implements ITranslator
{
    use InteractionTranslatorTrait;
    
    public function arrayToObject(array $expression, $appeal = null) : Appeal
    {
        if (!isset($expression['appeal_id'])) {
            return NullAppeal::getInstance();
        }

        if ($appeal == null) {
            $appeal = new Appeal($expression['appeal_id']);
        }

        $appeal->setId($expression['appeal_id']);

        $this->arrayToObjectCommentInteractionUtils($appeal, $expression);

        if (isset($expression['certificates'])) {
            $certificates = array();
            if (is_string($expression['certificates'])) {
                $certificates = json_decode($expression['certificates'], true);
            }
            if (is_array($expression['certificates'])) {
                $certificates = $expression['certificates'];
            }
            $appeal->setCertificates($certificates);
        }

        return $appeal;
    }

    public function objectToArray($appeal, array $keys = array()) : array
    {
        if (!$appeal instanceof Appeal) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'content',
                'acceptStatus',
                'name',
                'identify',
                'type',
                'contact',
                'images',
                'certificates',
                'member',
                'reply',
                'acceptUserGroup',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['appeal_id'] = $appeal->getId();
        }
        if (in_array('certificates', $keys)) {
            $expression['certificates'] = $appeal->getCertificates();
        }

        $expressionInfo = $this->objectToArrayCommentInteractionUtils($appeal, $keys);
        
        return array_merge($expression, $expressionInfo);
    }
}
