<?php
namespace Base\Interaction\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Interaction\Model\QA;
use Base\Interaction\Model\NullQA;

class QADbTranslator implements ITranslator
{
    use InteractionTranslatorTrait;
    
    public function arrayToObject(array $expression, $qaInteraction = null) : QA
    {
        if (!isset($expression['qa_id'])) {
            return NullQA::getInstance();
        }

        if ($qaInteraction == null) {
            $qaInteraction = new QA($expression['qa_id']);
        }

        $qaInteraction->setId($expression['qa_id']);

        $this->arrayToObjectInteractionUtils($qaInteraction, $expression);

        return $qaInteraction;
    }

    public function objectToArray($qaInteraction, array $keys = array()) : array
    {
        if (!$qaInteraction instanceof QA) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'content',
                'acceptStatus',
                'member',
                'reply',
                'acceptUserGroup',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['qa_id'] = $qaInteraction->getId();
        }

        $expressionInfo = $this->objectToArrayInteractionUtils($qaInteraction, $keys);
        
        return array_merge($expression, $expressionInfo);
    }
}
