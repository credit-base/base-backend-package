<?php
namespace Base\Interaction\Translator;

use Base\Interaction\Model\UnAuditedQA;

use Base\ApplyForm\Translator\ApplyFormDbTranslator;

class UnAuditedQADbTranslator extends ApplyFormDbTranslator
{
    use InteractionTranslatorTrait;

    public function arrayToObject(array $expression, $qaInteraction = null)
    {

        if ($qaInteraction == null) {
            $qaInteraction = new UnAuditedQA();
        }

        $qaInteraction->setId($expression['apply_form_id']);
        $qaInteraction->setApplyId($expression['apply_form_id']);

        parent::arrayToObject($expression, $qaInteraction);

        return $qaInteraction;
    }

    public function arrayToObjects(array $expression, $qaInteraction = null)
    {
        if (isset($expression['qa_id'])) {
            $qaInteraction->setId($expression['qa_id']);
        }
        
        $this->arrayToObjectInteractionUtils($qaInteraction, $expression);

        return $qaInteraction;
    }
}
