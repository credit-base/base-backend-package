<?php
namespace Base\Interaction\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Interaction\Model\Feedback;
use Base\Interaction\Model\NullFeedback;

class FeedbackDbTranslator implements ITranslator
{
    use InteractionTranslatorTrait;
    
    public function arrayToObject(array $expression, $feedback = null) : Feedback
    {
        if (!isset($expression['feedback_id'])) {
            return NullFeedback::getInstance();
        }

        if ($feedback == null) {
            $feedback = new Feedback($expression['feedback_id']);
        }

        $feedback->setId($expression['feedback_id']);

        $this->arrayToObjectInteractionUtils($feedback, $expression);

        return $feedback;
    }

    public function objectToArray($feedback, array $keys = array()) : array
    {
        if (!$feedback instanceof Feedback) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'content',
                'acceptStatus',
                'member',
                'reply',
                'acceptUserGroup',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['feedback_id'] = $feedback->getId();
        }

        $expressionInfo = $this->objectToArrayInteractionUtils($feedback, $keys);
        
        return array_merge($expression, $expressionInfo);
    }
}
