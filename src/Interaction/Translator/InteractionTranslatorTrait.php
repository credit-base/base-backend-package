<?php
namespace Base\Interaction\Translator;

use Base\Interaction\Model\IInteractionAble;

trait InteractionTranslatorTrait
{
    public function arrayToObjectInteractionUtils(IInteractionAble $interaction, array $expression)
    {
        if (isset($expression['title'])) {
            $interaction->setTitle($expression['title']);
        }
        if (isset($expression['content'])) {
            $interaction->setContent($expression['content']);
        }
        if (isset($expression['member_id'])) {
            $interaction->getMember()->setId($expression['member_id']);
        }
        if (isset($expression['reply_id'])) {
            $interaction->getReply()->setId($expression['reply_id']);
        }
        if (isset($expression['accept_usergroup_id'])) {
            $interaction->getAcceptUserGroup()->setId($expression['accept_usergroup_id']);
        }
        if (isset($expression['status'])) {
            $interaction->setStatus($expression['status']);
        }
        if (isset($expression['accept_status'])) {
            $interaction->setAcceptStatus($expression['accept_status']);
        }

        $interaction->setCreateTime($expression['create_time']);
        $interaction->setUpdateTime($expression['update_time']);
        $interaction->setStatusTime($expression['status_time']);
    }

    public function arrayToObjectCommentInteractionUtils(IInteractionAble $interaction, array $expression)
    {
        $this->arrayToObjectInteractionUtils($interaction, $expression);

        if (isset($expression['name'])) {
            $interaction->setName($expression['name']);
        }
        if (isset($expression['identify'])) {
            $interaction->setIdentify($expression['identify']);
        }
        if (isset($expression['type'])) {
            $interaction->setType($expression['type']);
        }
        if (isset($expression['contact'])) {
            $interaction->setContact($expression['contact']);
        }
        if (isset($expression['images'])) {
            $images = array();
            if (is_string($expression['images'])) {
                $images = json_decode($expression['images'], true);
            }
            if (is_array($expression['images'])) {
                $images = $expression['images'];
            }
            $interaction->setImages($images);
        }
    }
    /**
     * @todo
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArrayInteractionUtils(IInteractionAble $interaction, array $keys)
    {
        $expression = array();

        if (in_array('title', $keys)) {
            $expression['title'] = $interaction->getTitle();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $interaction->getContent();
        }
        if (in_array('acceptStatus', $keys)) {
            $expression['accept_status'] = $interaction->getAcceptStatus();
        }
        if (in_array('member', $keys)) {
            $expression['member_id'] = $interaction->getMember()->getId();
        }
        if (in_array('reply', $keys)) {
            $expression['reply_id'] = $interaction->getReply()->getId();
        }
        if (in_array('acceptUserGroup', $keys)) {
            $expression['accept_usergroup_id'] = $interaction->getAcceptUserGroup()->getId();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $interaction->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $interaction->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $interaction->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $interaction->getStatusTime();
        }
        
        return $expression;
    }

    public function objectToArrayCommentInteractionUtils(IInteractionAble $interaction, array $keys)
    {
        $expression = $this->objectToArrayInteractionUtils($interaction, $keys);

        if (in_array('name', $keys)) {
            $expression['name'] = $interaction->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $interaction->getIdentify();
        }
        if (in_array('type', $keys)) {
            $expression['type'] = $interaction->getType();
        }
        if (in_array('contact', $keys)) {
            $expression['contact'] = $interaction->getContact();
        }
        if (in_array('images', $keys)) {
            $expression['images'] = $interaction->getImages();
        }
        
        return $expression;
    }
}
