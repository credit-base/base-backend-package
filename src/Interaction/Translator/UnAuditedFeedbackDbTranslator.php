<?php
namespace Base\Interaction\Translator;

use Base\Interaction\Model\UnAuditedFeedback;

use Base\ApplyForm\Translator\ApplyFormDbTranslator;

class UnAuditedFeedbackDbTranslator extends ApplyFormDbTranslator
{
    use InteractionTranslatorTrait;

    public function arrayToObject(array $expression, $feedback = null)
    {

        if ($feedback == null) {
            $feedback = new UnAuditedFeedback();
        }

        $feedback->setId($expression['apply_form_id']);
        $feedback->setApplyId($expression['apply_form_id']);

        parent::arrayToObject($expression, $feedback);

        return $feedback;
    }

    public function arrayToObjects(array $expression, $feedback = null)
    {
        if (isset($expression['feedback_id'])) {
            $feedback->setId($expression['feedback_id']);
        }
        
        $this->arrayToObjectInteractionUtils($feedback, $expression);

        return $feedback;
    }
}
