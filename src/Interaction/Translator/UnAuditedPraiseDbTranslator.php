<?php
namespace Base\Interaction\Translator;

use Base\Interaction\Model\UnAuditedPraise;

use Base\ApplyForm\Translator\ApplyFormDbTranslator;

class UnAuditedPraiseDbTranslator extends ApplyFormDbTranslator
{
    use InteractionTranslatorTrait;

    public function arrayToObject(array $expression, $praise = null)
    {

        if ($praise == null) {
            $praise = new UnAuditedPraise();
        }

        $praise->setId($expression['apply_form_id']);
        $praise->setApplyId($expression['apply_form_id']);

        parent::arrayToObject($expression, $praise);

        return $praise;
    }

    public function arrayToObjects(array $expression, $praise = null)
    {
        if (isset($expression['praise_id'])) {
            $praise->setId($expression['praise_id']);
        }
        
        $this->arrayToObjectCommentInteractionUtils($praise, $expression);

        if (isset($expression['subject'])) {
            $praise->setSubject($expression['subject']);
        }

        return $praise;
    }
}
