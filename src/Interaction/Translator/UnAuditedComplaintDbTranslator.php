<?php
namespace Base\Interaction\Translator;

use Base\Interaction\Model\UnAuditedComplaint;

use Base\ApplyForm\Translator\ApplyFormDbTranslator;

class UnAuditedComplaintDbTranslator extends ApplyFormDbTranslator
{
    use InteractionTranslatorTrait;

    public function arrayToObject(array $expression, $complaint = null)
    {

        if ($complaint == null) {
            $complaint = new UnAuditedComplaint();
        }

        $complaint->setId($expression['apply_form_id']);
        $complaint->setApplyId($expression['apply_form_id']);

        parent::arrayToObject($expression, $complaint);

        return $complaint;
    }

    public function arrayToObjects(array $expression, $complaint = null)
    {
        if (isset($expression['complaint_id'])) {
            $complaint->setId($expression['complaint_id']);
        }
        
        $this->arrayToObjectCommentInteractionUtils($complaint, $expression);

        if (isset($expression['subject'])) {
            $complaint->setSubject($expression['subject']);
        }

        return $complaint;
    }
}
