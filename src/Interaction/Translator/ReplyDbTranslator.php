<?php
namespace Base\Interaction\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\NullReply;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ReplyDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $reply = null) : Reply
    {
        if (!isset($expression['reply_id'])) {
            return NullReply::getInstance();
        }

        if ($reply == null) {
            $reply = new Reply($expression['reply_id']);
        }

        if (isset($expression['content'])) {
            $reply->setContent($expression['content']);
        }
        if (isset($expression['images'])) {
            $images = array();
            if (is_string($expression['images'])) {
                $images = json_decode($expression['images'], true);
            }
            if (is_array($expression['images'])) {
                $images = $expression['images'];
            }
            $reply->setImages($images);
        }
        if (isset($expression['admissibility'])) {
            $reply->setAdmissibility($expression['admissibility']);
        }
        if (isset($expression['crew_id'])) {
            $reply->getCrew()->setId($expression['crew_id']);
        }
        if (isset($expression['accept_usergroup_id'])) {
            $reply->getAcceptUserGroup()->setId($expression['accept_usergroup_id']);
        }

        $reply->setStatus($expression['status']);
        $reply->setCreateTime($expression['create_time']);
        $reply->setUpdateTime($expression['update_time']);
        $reply->setStatusTime($expression['status_time']);

        return $reply;
    }

    public function objectToArray($reply, array $keys = array()) : array
    {
        if (!$reply instanceof Reply) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'content',
                'images',
                'crew',
                'acceptUserGroup',
                'admissibility',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['reply_id'] = $reply->getId();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $reply->getContent();
        }
        if (in_array('images', $keys)) {
            $expression['images'] = $reply->getImages();
        }
        if (in_array('crew', $keys)) {
            $expression['crew_id'] = $reply->getCrew()->getId();
        }
        if (in_array('acceptUserGroup', $keys)) {
            $expression['accept_usergroup_id'] = $reply->getAcceptUserGroup()->getId();
        }
        if (in_array('admissibility', $keys)) {
            $expression['admissibility'] = $reply->getAdmissibility();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $reply->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $reply->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $reply->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $reply->getStatusTime();
        }
        
        return $expression;
    }
}
