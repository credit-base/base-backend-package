<?php
namespace Base\Interaction\Translator;

use Base\Interaction\Model\UnAuditedAppeal;

use Base\ApplyForm\Translator\ApplyFormDbTranslator;

class UnAuditedAppealDbTranslator extends ApplyFormDbTranslator
{
    use InteractionTranslatorTrait;

    public function arrayToObject(array $expression, $appeal = null)
    {

        if ($appeal == null) {
            $appeal = new UnAuditedAppeal();
        }

        $appeal->setId($expression['apply_form_id']);
        $appeal->setApplyId($expression['apply_form_id']);

        parent::arrayToObject($expression, $appeal);

        return $appeal;
    }

    public function arrayToObjects(array $expression, $appeal = null)
    {
        if (isset($expression['appeal_id'])) {
            $appeal->setId($expression['appeal_id']);
        }
        
        $this->arrayToObjectCommentInteractionUtils($appeal, $expression);

        if (isset($expression['certificates'])) {
            $certificates = array();
            if (is_string($expression['certificates'])) {
                $certificates = json_decode($expression['certificates'], true);
            }
            if (is_array($expression['certificates'])) {
                $certificates = $expression['certificates'];
            }
            $appeal->setCertificates($certificates);
        }

        return $appeal;
    }
}
