<?php
namespace Base\Interaction\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Interaction\Model\Complaint;
use Base\Interaction\Model\NullComplaint;

class ComplaintDbTranslator implements ITranslator
{
    use InteractionTranslatorTrait;
    
    public function arrayToObject(array $expression, $complaint = null) : Complaint
    {
        if (!isset($expression['complaint_id'])) {
            return NullComplaint::getInstance();
        }

        if ($complaint == null) {
            $complaint = new Complaint($expression['complaint_id']);
        }

        $complaint->setId($expression['complaint_id']);

        $this->arrayToObjectCommentInteractionUtils($complaint, $expression);

        if (isset($expression['subject'])) {
            $complaint->setSubject($expression['subject']);
        }

        return $complaint;
    }

    public function objectToArray($complaint, array $keys = array()) : array
    {
        if (!$complaint instanceof Complaint) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'content',
                'acceptStatus',
                'name',
                'identify',
                'type',
                'contact',
                'images',
                'subject',
                'member',
                'reply',
                'acceptUserGroup',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['complaint_id'] = $complaint->getId();
        }
        if (in_array('subject', $keys)) {
            $expression['subject'] = $complaint->getSubject();
        }

        $expressionInfo = $this->objectToArrayCommentInteractionUtils($complaint, $keys);
        
        return array_merge($expression, $expressionInfo);
    }
}
