<?php
namespace Base\Interaction\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Interaction\Model\Praise;
use Base\Interaction\Model\NullPraise;

class PraiseDbTranslator implements ITranslator
{
    use InteractionTranslatorTrait;
    
    public function arrayToObject(array $expression, $praise = null) : Praise
    {
        if (!isset($expression['praise_id'])) {
            return NullPraise::getInstance();
        }

        if ($praise == null) {
            $praise = new Praise($expression['praise_id']);
        }

        $praise->setId($expression['praise_id']);

        $this->arrayToObjectCommentInteractionUtils($praise, $expression);

        if (isset($expression['subject'])) {
            $praise->setSubject($expression['subject']);
        }

        return $praise;
    }

    public function objectToArray($praise, array $keys = array()) : array
    {
        if (!$praise instanceof Praise) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'content',
                'acceptStatus',
                'name',
                'identify',
                'type',
                'contact',
                'images',
                'subject',
                'member',
                'reply',
                'acceptUserGroup',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['praise_id'] = $praise->getId();
        }
        if (in_array('subject', $keys)) {
            $expression['subject'] = $praise->getSubject();
        }

        $expressionInfo = $this->objectToArrayCommentInteractionUtils($praise, $keys);
        
        return array_merge($expression, $expressionInfo);
    }
}
