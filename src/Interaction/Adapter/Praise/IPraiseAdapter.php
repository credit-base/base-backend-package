<?php
namespace Base\Interaction\Adapter\Praise;

use Base\Interaction\Model\Praise;

interface IPraiseAdapter
{
    public function fetchOne($id) : Praise;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Praise $praise) : bool;

    public function edit(Praise $praise, array $keys = array()) : bool;
}
