<?php
namespace Base\Interaction\Adapter\Praise;

use Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter;

use Base\Interaction\Adapter\UnAuditedInteractionDbAdapterTrait;

class UnAuditedPraiseDbAdapter extends ApplyFormDbAdapter
{
    use UnAuditedInteractionDbAdapterTrait;
}
