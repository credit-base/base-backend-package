<?php
namespace Base\Interaction\Adapter\Praise\Query\Persistence;

use Marmot\Framework\Classes\Db;

class PraiseDb extends Db
{
    const TABLE = 'praise';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
