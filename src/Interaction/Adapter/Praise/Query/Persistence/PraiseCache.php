<?php
namespace Base\Interaction\Adapter\Praise\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class PraiseCache extends Cache
{
    const KEY = 'praise';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
