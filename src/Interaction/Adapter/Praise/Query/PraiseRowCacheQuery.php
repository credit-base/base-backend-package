<?php
namespace Base\Interaction\Adapter\Praise\Query;

use Marmot\Framework\Query\RowCacheQuery;

class PraiseRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'praise_id',
            new Persistence\PraiseCache(),
            new Persistence\PraiseDb()
        );
    }
}
