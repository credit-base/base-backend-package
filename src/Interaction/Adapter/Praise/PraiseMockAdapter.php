<?php
namespace Base\Interaction\Adapter\Praise;

use Base\Interaction\Model\Praise;
use Base\Interaction\Utils\PraiseMockFactory;

class PraiseMockAdapter implements IPraiseAdapter
{
    public function fetchOne($id) : Praise
    {
        return PraiseMockFactory::generatePraise($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $praiseList = array();

        foreach ($ids as $id) {
            $praiseList[$id] = PraiseMockFactory::generatePraise($id);
        }

        return $praiseList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Praise $praise) : bool
    {
        unset($praise);
        return true;
    }

    public function edit(Praise $praise, array $keys = array()) : bool
    {
        unset($praise);
        unset($keys);
        return true;
    }
}
