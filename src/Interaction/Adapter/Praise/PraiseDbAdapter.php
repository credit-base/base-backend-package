<?php
namespace Base\Interaction\Adapter\Praise;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Interaction\Model\Praise;
use Base\Interaction\Model\NullPraise;
use Base\Interaction\Translator\PraiseDbTranslator;
use Base\Interaction\Adapter\InteractionDbAdapterTrait;
use Base\Interaction\Adapter\Praise\Query\PraiseRowCacheQuery;

class PraiseDbAdapter implements IPraiseAdapter
{
    use DbAdapterTrait, InteractionDbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new PraiseDbTranslator();
        $this->rowCacheQuery = new PraiseRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : PraiseDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : PraiseRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullPraise::getInstance();
    }

    public function add(Praise $praise) : bool
    {
        return $this->addAction($praise);
    }

    public function edit(Praise $praise, array $keys = array()) : bool
    {
        return $this->editAction($praise, $keys);
    }

    public function fetchOne($id) : Praise
    {
        $praise = $this->fetchOneAction($id);
        $this->fetchMember($praise);
        $this->fetchAcceptUserGroup($praise);
        $this->fetchReply($praise);
        
        return $praise;
    }

    public function fetchList(array $ids) : array
    {
        $praiseList = $this->fetchListAction($ids);
        $this->fetchMember($praiseList);
        $this->fetchAcceptUserGroup($praiseList);
        $this->fetchReply($praiseList);

        return $praiseList;
    }

    protected function formatFilter(array $filter) : string
    {
        $praise = new Praise();

        return $this->interactionFormatFilter($filter, $praise);
    }

    protected function formatSort(array $sort) : string
    {
        $praise = new Praise();

        return $this->interactionFormatSort($sort, $praise);
    }
}
