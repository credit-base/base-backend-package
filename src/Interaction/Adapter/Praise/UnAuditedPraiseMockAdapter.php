<?php
namespace Base\Interaction\Adapter\Praise;

use Base\Interaction\Utils\PraiseMockFactory;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class UnAuditedPraiseMockAdapter implements IApplyFormAdapter
{
    public function add(IApplyFormAble $unAuditedPraise) : bool
    {
        unset($unAuditedPraise);

        return true;
    }

    public function edit(IApplyFormAble $unAuditedPraise, array $keys = array()) : bool
    {
        unset($unAuditedPraise);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : IApplyFormAble
    {
        return PraiseMockFactory::generateUnAuditedPraise($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedPraiseList = array();

        foreach ($ids as $id) {
            $unAuditedPraiseList[] = PraiseMockFactory::generateUnAuditedPraise($id);
        }

        return $unAuditedPraiseList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
