<?php
namespace Base\Interaction\Adapter\Appeal;

use Base\Interaction\Utils\AppealMockFactory;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class UnAuditedAppealMockAdapter implements IApplyFormAdapter
{
    public function add(IApplyFormAble $unAuditedAppeal) : bool
    {
        unset($unAuditedAppeal);

        return true;
    }

    public function edit(IApplyFormAble $unAuditedAppeal, array $keys = array()) : bool
    {
        unset($unAuditedAppeal);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : IApplyFormAble
    {
        return AppealMockFactory::generateUnAuditedAppeal($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedAppealList = array();

        foreach ($ids as $id) {
            $unAuditedAppealList[] = AppealMockFactory::generateUnAuditedAppeal($id);
        }

        return $unAuditedAppealList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
