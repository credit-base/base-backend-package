<?php
namespace Base\Interaction\Adapter\Appeal;

use Base\Interaction\Model\Appeal;

interface IAppealAdapter
{
    public function fetchOne($id) : Appeal;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Appeal $appeal) : bool;

    public function edit(Appeal $appeal, array $keys = array()) : bool;
}
