<?php
namespace Base\Interaction\Adapter\Appeal\Query\Persistence;

use Marmot\Framework\Classes\Db;

class AppealDb extends Db
{
    const TABLE = 'appeal';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
