<?php
namespace Base\Interaction\Adapter\Appeal\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class AppealCache extends Cache
{
    const KEY = 'appeal';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
