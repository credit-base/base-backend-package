<?php
namespace Base\Interaction\Adapter\Appeal\Query;

use Marmot\Framework\Query\RowCacheQuery;

class AppealRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'appeal_id',
            new Persistence\AppealCache(),
            new Persistence\AppealDb()
        );
    }
}
