<?php
namespace Base\Interaction\Adapter\Appeal;

use Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter;

use Base\Interaction\Adapter\UnAuditedInteractionDbAdapterTrait;

class UnAuditedAppealDbAdapter extends ApplyFormDbAdapter
{
    use UnAuditedInteractionDbAdapterTrait;
}
