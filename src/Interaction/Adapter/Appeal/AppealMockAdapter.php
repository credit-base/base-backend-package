<?php
namespace Base\Interaction\Adapter\Appeal;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Utils\AppealMockFactory;

class AppealMockAdapter implements IAppealAdapter
{
    public function fetchOne($id) : Appeal
    {
        return AppealMockFactory::generateAppeal($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $appealList = array();

        foreach ($ids as $id) {
            $appealList[$id] = AppealMockFactory::generateAppeal($id);
        }

        return $appealList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Appeal $appeal) : bool
    {
        unset($appeal);
        return true;
    }

    public function edit(Appeal $appeal, array $keys = array()) : bool
    {
        unset($appeal);
        unset($keys);
        return true;
    }
}
