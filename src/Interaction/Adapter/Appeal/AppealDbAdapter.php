<?php
namespace Base\Interaction\Adapter\Appeal;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Interaction\Model\Appeal;
use Base\Interaction\Model\NullAppeal;
use Base\Interaction\Translator\AppealDbTranslator;
use Base\Interaction\Adapter\InteractionDbAdapterTrait;
use Base\Interaction\Adapter\Appeal\Query\AppealRowCacheQuery;

class AppealDbAdapter implements IAppealAdapter
{
    use DbAdapterTrait, InteractionDbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new AppealDbTranslator();
        $this->rowCacheQuery = new AppealRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : AppealDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : AppealRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullAppeal::getInstance();
    }

    public function add(Appeal $appeal) : bool
    {
        return $this->addAction($appeal);
    }

    public function edit(Appeal $appeal, array $keys = array()) : bool
    {
        return $this->editAction($appeal, $keys);
    }

    public function fetchOne($id) : Appeal
    {
        $appeal = $this->fetchOneAction($id);
        $this->fetchMember($appeal);
        $this->fetchAcceptUserGroup($appeal);
        $this->fetchReply($appeal);
        
        return $appeal;
    }

    public function fetchList(array $ids) : array
    {
        $appealList = $this->fetchListAction($ids);
        $this->fetchMember($appealList);
        $this->fetchAcceptUserGroup($appealList);
        $this->fetchReply($appealList);

        return $appealList;
    }

    protected function formatFilter(array $filter) : string
    {
        $appeal = new Appeal();

        return $this->interactionFormatFilter($filter, $appeal);
    }

    protected function formatSort(array $sort) : string
    {
        $appeal = new Appeal();

        return $this->interactionFormatSort($sort, $appeal);
    }
}
