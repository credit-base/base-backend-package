<?php
namespace Base\Interaction\Adapter;

use Marmot\Interfaces\ITranslator;

use Base\Member\Repository\MemberRepository;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Interaction\Model\IInteractionAble;
use Base\Interaction\Repository\Reply\ReplyRepository;

trait InteractionDbAdapterTrait
{
    abstract protected function getDbTranslator() : ITranslator;
    
    protected function getUserGroupRepository() : UserGroupRepository
    {
        return new UserGroupRepository();
    }

    protected function getMemberRepository() : MemberRepository
    {
        return new MemberRepository();
    }

    protected function getReplyRepository() : ReplyRepository
    {
        return new ReplyRepository();
    }

    protected function fetchAcceptUserGroup($interaction)
    {
        return is_array($interaction) ?
        $this->fetchAcceptUserGroupByList($interaction) :
        $this->fetchAcceptUserGroupByObject($interaction);
    }

    protected function fetchAcceptUserGroupByObject(IInteractionAble $interaction)
    {
        $acceptUserGroupId = $interaction->getAcceptUserGroup()->getId();
        $acceptUserGroup = $this->getUserGroupRepository()->fetchOne($acceptUserGroupId);
        $interaction->setAcceptUserGroup($acceptUserGroup);
    }

    protected function fetchAcceptUserGroupByList(array $interactionList)
    {
        $acceptUserGroupIds = array();
        foreach ($interactionList as $interaction) {
            $acceptUserGroupIds[] = $interaction->getAcceptUserGroup()->getId();
        }

        $acceptUserGroupList = $this->getUserGroupRepository()->fetchList($acceptUserGroupIds);
        if (!empty($acceptUserGroupList)) {
            foreach ($interactionList as $interaction) {
                $acceptUserGroupId = $interaction->getAcceptUserGroup()->getId();
                if (isset($acceptUserGroupList[$acceptUserGroupId])
                    && ($acceptUserGroupList[$acceptUserGroupId]->getId() == $acceptUserGroupId)) {
                    $interaction->setAcceptUserGroup($acceptUserGroupList[$acceptUserGroupId]);
                }
            }
        }
    }

    protected function fetchMember($interaction)
    {
        return is_array($interaction) ?
        $this->fetchMemberByList($interaction) :
        $this->fetchMemberByObject($interaction);
    }

    protected function fetchMemberByObject(IInteractionAble $interaction)
    {
        $memberId = $interaction->getMember()->getId();
        $member = $this->getMemberRepository()->fetchOne($memberId);
        $interaction->setMember($member);
    }

    protected function fetchMemberByList(array $interactionList)
    {
        $memberIds = array();
        foreach ($interactionList as $interaction) {
            $memberIds[] = $interaction->getMember()->getId();
        }

        $memberList = $this->getMemberRepository()->fetchList($memberIds);
        if (!empty($memberList)) {
            foreach ($interactionList as $interaction) {
                $memberId = $interaction->getMember()->getId();
                if (isset($memberList[$memberId])
                    && ($memberList[$memberId]->getId() == $memberId)) {
                    $interaction->setMember($memberList[$memberId]);
                }
            }
        }
    }
    
    protected function fetchReply($interaction)
    {
        return is_array($interaction) ?
        $this->fetchReplyByList($interaction) :
        $this->fetchReplyByObject($interaction);
    }

    protected function fetchReplyByObject(IInteractionAble $interaction)
    {
        $replyId = $interaction->getReply()->getId();
        $reply = $this->getReplyRepository()->fetchOne($replyId);
        $interaction->setReply($reply);
    }

    protected function fetchReplyByList(array $interactionList)
    {
        $replyIds = array();
        foreach ($interactionList as $interaction) {
            $replyIds[] = $interaction->getReply()->getId();
        }

        $replyList = $this->getReplyRepository()->fetchList($replyIds);
        if (!empty($replyList)) {
            foreach ($interactionList as $interaction) {
                $replyId = $interaction->getReply()->getId();
                if (isset($replyList[$replyId])
                    && ($replyList[$replyId]->getId() == $replyId)) {
                    $interaction->setReply($replyList[$replyId]);
                }
            }
        }
    }

    protected function interactionFormatFilter(array $filter, IInteractionAble $interaction) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            if (isset($filter['title'])) {
                $interaction->setTitle($filter['title']);
                $info = $this->getDbTranslator()->objectToArray($interaction, array('title'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $info = $this->getDbTranslator()->objectToArray($interaction, array('status'));
                $condition .= $conjection.key($info).' IN ('.$filter['status'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['acceptStatus'])) {
                $info = $this->getDbTranslator()->objectToArray($interaction, array('acceptStatus'));
                $condition .= $conjection.key($info).' IN ('.$filter['acceptStatus'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['acceptUserGroup'])) {
                $interaction->getAcceptUserGroup()->setId($filter['acceptUserGroup']);
                $info = $this->getDbTranslator()->objectToArray($interaction, array('acceptUserGroup'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['member'])) {
                $interaction->getMember()->setId($filter['member']);
                $info = $this->getDbTranslator()->objectToArray($interaction, array('member'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function interactionFormatSort(array $sort, IInteractionAble $interaction) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['acceptStatus'])) {
                $info = $this->getDbTranslator()->objectToArray($interaction, array('acceptStatus'));
                $condition .= $conjection.key($info).' '.($sort['acceptStatus'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($interaction, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['status'])) {
                $info = $this->getDbTranslator()->objectToArray($interaction, array('status'));
                $condition .= $conjection.key($info).' '.($sort['status'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}
