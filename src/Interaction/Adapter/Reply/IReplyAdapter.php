<?php
namespace Base\Interaction\Adapter\Reply;

use Base\Interaction\Model\Reply;

interface IReplyAdapter
{
    public function fetchOne($id) : Reply;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Reply $reply) : bool;

    public function edit(Reply $reply, array $keys = array()) : bool;
}
