<?php
namespace Base\Interaction\Adapter\Reply\Query;

use Marmot\Framework\Query\RowCacheQuery;

class ReplyRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'reply_id',
            new Persistence\ReplyCache(),
            new Persistence\ReplyDb()
        );
    }
}
