<?php
namespace Base\Interaction\Adapter\Reply\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ReplyDb extends Db
{
    const TABLE = 'reply';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
