<?php
namespace Base\Interaction\Adapter\Reply\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class ReplyCache extends Cache
{
    const KEY = 'reply';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
