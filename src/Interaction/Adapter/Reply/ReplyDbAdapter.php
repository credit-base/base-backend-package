<?php
namespace Base\Interaction\Adapter\Reply;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Interaction\Model\Reply;
use Base\Interaction\Model\NullReply;
use Base\Interaction\Translator\ReplyDbTranslator;
use Base\Interaction\Adapter\Reply\Query\ReplyRowCacheQuery;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

class ReplyDbAdapter implements IReplyAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $crewRepository;

    private $acceptUserGroupRepository;

    public function __construct()
    {
        $this->dbTranslator = new ReplyDbTranslator();
        $this->rowCacheQuery = new ReplyRowCacheQuery();
        $this->crewRepository = new CrewRepository();
        $this->acceptUserGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->crewRepository);
        unset($this->acceptUserGroupRepository);
    }
    
    protected function getDbTranslator() : ReplyDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : ReplyRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->acceptUserGroupRepository;
    }

    protected function getNullObject() : INull
    {
        return NullReply::getInstance();
    }

    public function add(Reply $reply) : bool
    {
        return $this->addAction($reply);
    }

    public function edit(Reply $reply, array $keys = array()) : bool
    {
        return $this->editAction($reply, $keys);
    }

    public function fetchOne($id) : Reply
    {
        $reply = $this->fetchOneAction($id);
        $this->fetchCrew($reply);
        $this->fetchUserGroup($reply);

        return $reply;
    }

    public function fetchList(array $ids) : array
    {
        $replyList = $this->fetchListAction($ids);
        $this->fetchCrew($replyList);
        $this->fetchUserGroup($replyList);

        return $replyList;
    }

    protected function fetchUserGroup($reply)
    {
        return is_array($reply) ?
        $this->fetchUserGroupByList($reply) :
        $this->fetchUserGroupByObject($reply);
    }

    protected function fetchUserGroupByObject(Reply $reply)
    {
        if (!$reply instanceof INull) {
            $acceptUserGroupId = $reply->getAcceptUserGroup()->getId();
            $acceptUserGroup = $this->getUserGroupRepository()->fetchOne($acceptUserGroupId);
            $reply->setAcceptUserGroup($acceptUserGroup);
        }
    }

    protected function fetchUserGroupByList(array $replyList)
    {
        if (!empty($replyList)) {
            $acceptUserGroupIds = array();
            foreach ($replyList as $reply) {
                $acceptUserGroupIds[] = $reply->getAcceptUserGroup()->getId();
            }

            $acceptUserGroupList = $this->getUserGroupRepository()->fetchList($acceptUserGroupIds);
            if (!empty($acceptUserGroupList)) {
                foreach ($replyList as $reply) {
                    if (isset($acceptUserGroupList[$reply->getAcceptUserGroup()->getId()])) {
                        $reply->setAcceptUserGroup($acceptUserGroupList[$reply->getAcceptUserGroup()->getId()]);
                    }
                }
            }
        }
    }

    protected function fetchCrew($reply)
    {
        return is_array($reply) ?
        $this->fetchCrewByList($reply) :
        $this->fetchCrewByObject($reply);
    }

    protected function fetchCrewByObject(Reply $reply)
    {
        if (!$reply instanceof INull) {
            $crewId = $reply->getCrew()->getId();
            $crew = $this->getCrewRepository()->fetchOne($crewId);
            $reply->setCrew($crew);
        }
    }

    protected function fetchCrewByList(array $replyList)
    {
        if (!empty($replyList)) {
            $crewIds = array();
            foreach ($replyList as $reply) {
                $crewIds[] = $reply->getCrew()->getId();
            }

            $crewList = $this->getCrewRepository()->fetchList($crewIds);
            if (!empty($crewList)) {
                foreach ($replyList as $reply) {
                    if (isset($crewList[$reply->getCrew()->getId()])) {
                        $reply->setCrew($crewList[$reply->getCrew()->getId()]);
                    }
                }
            }
        }
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        unset($filter);
        unset($conjection);

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        unset($sort);
        unset($conjection);

        return $condition;
    }
}
