<?php
namespace Base\Interaction\Adapter\Reply;

use Base\Interaction\Model\Reply;
use Base\Interaction\Utils\ReplyMockFactory;

class ReplyMockAdapter implements IReplyAdapter
{
    public function fetchOne($id) : Reply
    {
        return ReplyMockFactory::generateReply($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $replyList = array();

        foreach ($ids as $id) {
            $replyList[$id] = ReplyMockFactory::generateReply($id);
        }

        return $replyList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Reply $reply) : bool
    {
        unset($reply);
        return true;
    }

    public function edit(Reply $reply, array $keys = array()) : bool
    {
        unset($reply);
        unset($keys);
        return true;
    }
}
