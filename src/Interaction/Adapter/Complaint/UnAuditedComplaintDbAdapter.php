<?php
namespace Base\Interaction\Adapter\Complaint;

use Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter;

use Base\Interaction\Adapter\UnAuditedInteractionDbAdapterTrait;

class UnAuditedComplaintDbAdapter extends ApplyFormDbAdapter
{
    use UnAuditedInteractionDbAdapterTrait;
}
