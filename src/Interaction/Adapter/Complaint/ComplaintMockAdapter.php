<?php
namespace Base\Interaction\Adapter\Complaint;

use Base\Interaction\Model\Complaint;
use Base\Interaction\Utils\ComplaintMockFactory;

class ComplaintMockAdapter implements IComplaintAdapter
{
    public function fetchOne($id) : Complaint
    {
        return ComplaintMockFactory::generateComplaint($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $complaintList = array();

        foreach ($ids as $id) {
            $complaintList[$id] = ComplaintMockFactory::generateComplaint($id);
        }

        return $complaintList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Complaint $complaint) : bool
    {
        unset($complaint);
        return true;
    }

    public function edit(Complaint $complaint, array $keys = array()) : bool
    {
        unset($complaint);
        unset($keys);
        return true;
    }
}
