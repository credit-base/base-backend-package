<?php
namespace Base\Interaction\Adapter\Complaint;

use Base\Interaction\Model\Complaint;

interface IComplaintAdapter
{
    public function fetchOne($id) : Complaint;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Complaint $complaint) : bool;

    public function edit(Complaint $complaint, array $keys = array()) : bool;
}
