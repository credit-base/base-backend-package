<?php
namespace Base\Interaction\Adapter\Complaint;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Interaction\Model\Complaint;
use Base\Interaction\Model\NullComplaint;
use Base\Interaction\Translator\ComplaintDbTranslator;
use Base\Interaction\Adapter\InteractionDbAdapterTrait;
use Base\Interaction\Adapter\Complaint\Query\ComplaintRowCacheQuery;

class ComplaintDbAdapter implements IComplaintAdapter
{
    use DbAdapterTrait, InteractionDbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new ComplaintDbTranslator();
        $this->rowCacheQuery = new ComplaintRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : ComplaintDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : ComplaintRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullComplaint::getInstance();
    }

    public function add(Complaint $complaint) : bool
    {
        return $this->addAction($complaint);
    }

    public function edit(Complaint $complaint, array $keys = array()) : bool
    {
        return $this->editAction($complaint, $keys);
    }

    public function fetchOne($id) : Complaint
    {
        $complaint = $this->fetchOneAction($id);
        $this->fetchMember($complaint);
        $this->fetchAcceptUserGroup($complaint);
        $this->fetchReply($complaint);
        
        return $complaint;
    }

    public function fetchList(array $ids) : array
    {
        $complaintList = $this->fetchListAction($ids);
        $this->fetchMember($complaintList);
        $this->fetchAcceptUserGroup($complaintList);
        $this->fetchReply($complaintList);

        return $complaintList;
    }

    protected function formatFilter(array $filter) : string
    {
        $complaint = new Complaint();

        return $this->interactionFormatFilter($filter, $complaint);
    }

    protected function formatSort(array $sort) : string
    {
        $complaint = new Complaint();

        return $this->interactionFormatSort($sort, $complaint);
    }
}
