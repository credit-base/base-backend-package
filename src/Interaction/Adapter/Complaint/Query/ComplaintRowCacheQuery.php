<?php
namespace Base\Interaction\Adapter\Complaint\Query;

use Marmot\Framework\Query\RowCacheQuery;

class ComplaintRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'complaint_id',
            new Persistence\ComplaintCache(),
            new Persistence\ComplaintDb()
        );
    }
}
