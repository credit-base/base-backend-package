<?php
namespace Base\Interaction\Adapter\Complaint\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ComplaintDb extends Db
{
    const TABLE = 'complaint';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
