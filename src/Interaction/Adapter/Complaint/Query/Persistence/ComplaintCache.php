<?php
namespace Base\Interaction\Adapter\Complaint\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class ComplaintCache extends Cache
{
    const KEY = 'complaint';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
