<?php
namespace Base\Interaction\Adapter\Complaint;

use Base\Interaction\Utils\ComplaintMockFactory;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class UnAuditedComplaintMockAdapter implements IApplyFormAdapter
{
    public function add(IApplyFormAble $unAuditedComplaint) : bool
    {
        unset($unAuditedComplaint);

        return true;
    }

    public function edit(IApplyFormAble $unAuditedComplaint, array $keys = array()) : bool
    {
        unset($unAuditedComplaint);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : IApplyFormAble
    {
        return ComplaintMockFactory::generateUnAuditedComplaint($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedComplaintList = array();

        foreach ($ids as $id) {
            $unAuditedComplaintList[] = ComplaintMockFactory::generateUnAuditedComplaint($id);
        }

        return $unAuditedComplaintList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
