<?php
namespace Base\Interaction\Adapter\Feedback;

use Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter;

use Base\Interaction\Adapter\UnAuditedInteractionDbAdapterTrait;

class UnAuditedFeedbackDbAdapter extends ApplyFormDbAdapter
{
    use UnAuditedInteractionDbAdapterTrait;
}
