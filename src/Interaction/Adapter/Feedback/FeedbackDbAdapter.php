<?php
namespace Base\Interaction\Adapter\Feedback;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Interaction\Model\Feedback;
use Base\Interaction\Model\NullFeedback;
use Base\Interaction\Translator\FeedbackDbTranslator;
use Base\Interaction\Adapter\InteractionDbAdapterTrait;
use Base\Interaction\Adapter\Feedback\Query\FeedbackRowCacheQuery;

class FeedbackDbAdapter implements IFeedbackAdapter
{
    use DbAdapterTrait, InteractionDbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new FeedbackDbTranslator();
        $this->rowCacheQuery = new FeedbackRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : FeedbackDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : FeedbackRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullFeedback::getInstance();
    }

    public function add(Feedback $feedback) : bool
    {
        return $this->addAction($feedback);
    }

    public function edit(Feedback $feedback, array $keys = array()) : bool
    {
        return $this->editAction($feedback, $keys);
    }

    public function fetchOne($id) : Feedback
    {
        $feedback = $this->fetchOneAction($id);
        $this->fetchMember($feedback);
        $this->fetchAcceptUserGroup($feedback);
        $this->fetchReply($feedback);
        
        return $feedback;
    }

    public function fetchList(array $ids) : array
    {
        $feedbackList = $this->fetchListAction($ids);
        $this->fetchMember($feedbackList);
        $this->fetchAcceptUserGroup($feedbackList);
        $this->fetchReply($feedbackList);

        return $feedbackList;
    }

    protected function formatFilter(array $filter) : string
    {
        $feedback = new Feedback();

        return $this->interactionFormatFilter($filter, $feedback);
    }

    protected function formatSort(array $sort) : string
    {
        $feedback = new Feedback();

        return $this->interactionFormatSort($sort, $feedback);
    }
}
