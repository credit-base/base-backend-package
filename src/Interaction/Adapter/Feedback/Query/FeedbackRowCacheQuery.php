<?php
namespace Base\Interaction\Adapter\Feedback\Query;

use Marmot\Framework\Query\RowCacheQuery;

class FeedbackRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'feedback_id',
            new Persistence\FeedbackCache(),
            new Persistence\FeedbackDb()
        );
    }
}
