<?php
namespace Base\Interaction\Adapter\Feedback\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class FeedbackCache extends Cache
{
    const KEY = 'feedback';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
