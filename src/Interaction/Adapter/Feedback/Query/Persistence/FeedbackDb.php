<?php
namespace Base\Interaction\Adapter\Feedback\Query\Persistence;

use Marmot\Framework\Classes\Db;

class FeedbackDb extends Db
{
    const TABLE = 'feedback';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
