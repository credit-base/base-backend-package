<?php
namespace Base\Interaction\Adapter\Feedback;

use Base\Interaction\Model\Feedback;
use Base\Interaction\Utils\FeedbackMockFactory;

class FeedbackMockAdapter implements IFeedbackAdapter
{
    public function fetchOne($id) : Feedback
    {
        return FeedbackMockFactory::generateFeedback($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $feedbackList = array();

        foreach ($ids as $id) {
            $feedbackList[$id] = FeedbackMockFactory::generateFeedback($id);
        }

        return $feedbackList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Feedback $feedback) : bool
    {
        unset($feedback);
        return true;
    }

    public function edit(Feedback $feedback, array $keys = array()) : bool
    {
        unset($feedback);
        unset($keys);
        return true;
    }
}
