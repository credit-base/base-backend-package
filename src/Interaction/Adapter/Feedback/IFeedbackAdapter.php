<?php
namespace Base\Interaction\Adapter\Feedback;

use Base\Interaction\Model\Feedback;

interface IFeedbackAdapter
{
    public function fetchOne($id) : Feedback;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Feedback $feedback) : bool;

    public function edit(Feedback $feedback, array $keys = array()) : bool;
}
