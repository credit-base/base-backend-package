<?php
namespace Base\Interaction\Adapter\Feedback;

use Base\Interaction\Utils\FeedbackMockFactory;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class UnAuditedFeedbackMockAdapter implements IApplyFormAdapter
{
    public function add(IApplyFormAble $unAuditedFeedback) : bool
    {
        unset($unAuditedFeedback);

        return true;
    }

    public function edit(IApplyFormAble $unAuditedFeedback, array $keys = array()) : bool
    {
        unset($unAuditedFeedback);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : IApplyFormAble
    {
        return FeedbackMockFactory::generateUnAuditedFeedback($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedFeedbackList = array();

        foreach ($ids as $id) {
            $unAuditedFeedbackList[] = FeedbackMockFactory::generateUnAuditedFeedback($id);
        }

        return $unAuditedFeedbackList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
