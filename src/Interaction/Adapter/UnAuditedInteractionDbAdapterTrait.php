<?php
namespace Base\Interaction\Adapter;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\Interaction\Repository\Reply\ReplyRepository;

trait UnAuditedInteractionDbAdapterTrait
{
    public function fetchOne($id) : IApplyFormAble
    {
        $applyForm = $this->fetchOneAction($id);
        $this->fetchRelation($applyForm);
        $this->fetchApplyCrew($applyForm);
        $this->fetchApplyUserGroup($applyForm);
        $this->fetchReply($applyForm);

        return $applyForm;
    }

    public function fetchList(array $ids) : array
    {
        $applyFormList = array();
        $applyFormList = $this->fetchListAction($ids);
        $this->fetchRelation($applyFormList);
        $this->fetchApplyCrew($applyFormList);
        $this->fetchApplyUserGroup($applyFormList);
        $this->fetchReply($applyFormList);

        return $applyFormList;
    }

    protected function getReplyRepository() : ReplyRepository
    {
        return new ReplyRepository();
    }
    
    protected function fetchReply($unAuditedInteraction)
    {
        return is_array($unAuditedInteraction) ?
        $this->fetchReplyByList($unAuditedInteraction) :
        $this->fetchReplyByObject($unAuditedInteraction);
    }

    protected function fetchReplyByObject(IApplyFormAble $unAuditedInteraction)
    {
        $replyId = $unAuditedInteraction->getReply()->getId();
        $reply = $this->getReplyRepository()->fetchOne($replyId);
        $unAuditedInteraction->setReply($reply);
    }

    protected function fetchReplyByList(array $unAuditedInteractionList)
    {
        $replyIds = array();
        foreach ($unAuditedInteractionList as $unAuditedInteraction) {
            $replyIds[] = $unAuditedInteraction->getReply()->getId();
        }

        $replyList = $this->getReplyRepository()->fetchList($replyIds);
        if (!empty($replyList)) {
            foreach ($unAuditedInteractionList as $unAuditedInteraction) {
                $replyId = $unAuditedInteraction->getReply()->getId();
                if (isset($replyList[$replyId])
                    && ($replyList[$replyId]->getId() == $replyId)) {
                    $unAuditedInteraction->setReply($replyList[$replyId]);
                }
            }
        }
    }
}
