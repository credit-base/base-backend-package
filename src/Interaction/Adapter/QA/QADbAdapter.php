<?php
namespace Base\Interaction\Adapter\QA;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Interaction\Model\QA;
use Base\Interaction\Model\NullQA;
use Base\Interaction\Translator\QADbTranslator;
use Base\Interaction\Adapter\InteractionDbAdapterTrait;
use Base\Interaction\Adapter\QA\Query\QARowCacheQuery;

class QADbAdapter implements IQAAdapter
{
    use DbAdapterTrait, InteractionDbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new QADbTranslator();
        $this->rowCacheQuery = new QARowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : QADbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : QARowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullQA::getInstance();
    }

    public function add(QA $qaInteraction) : bool
    {
        return $this->addAction($qaInteraction);
    }

    public function edit(QA $qaInteraction, array $keys = array()) : bool
    {
        return $this->editAction($qaInteraction, $keys);
    }

    public function fetchOne($id) : QA
    {
        $qaInteraction = $this->fetchOneAction($id);
        $this->fetchMember($qaInteraction);
        $this->fetchAcceptUserGroup($qaInteraction);
        $this->fetchReply($qaInteraction);
        
        return $qaInteraction;
    }

    public function fetchList(array $ids) : array
    {
        $qaInteractionList = $this->fetchListAction($ids);
        $this->fetchMember($qaInteractionList);
        $this->fetchAcceptUserGroup($qaInteractionList);
        $this->fetchReply($qaInteractionList);

        return $qaInteractionList;
    }

    protected function formatFilter(array $filter) : string
    {
        $qaInteraction = new QA();

        return $this->interactionFormatFilter($filter, $qaInteraction);
    }

    protected function formatSort(array $sort) : string
    {
        $qaInteraction = new QA();

        return $this->interactionFormatSort($sort, $qaInteraction);
    }
}
