<?php
namespace Base\Interaction\Adapter\QA;

use Base\Interaction\Utils\QAMockFactory;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class UnAuditedQAMockAdapter implements IApplyFormAdapter
{
    public function add(IApplyFormAble $unAuditedQA) : bool
    {
        unset($unAuditedQA);

        return true;
    }

    public function edit(IApplyFormAble $unAuditedQA, array $keys = array()) : bool
    {
        unset($unAuditedQA);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : IApplyFormAble
    {
        return QAMockFactory::generateUnAuditedQA($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedQAList = array();

        foreach ($ids as $id) {
            $unAuditedQAList[] = QAMockFactory::generateUnAuditedQA($id);
        }

        return $unAuditedQAList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
