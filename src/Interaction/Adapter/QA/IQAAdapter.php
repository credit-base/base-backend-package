<?php
namespace Base\Interaction\Adapter\QA;

use Base\Interaction\Model\QA;

interface IQAAdapter
{
    public function fetchOne($id) : QA;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(QA $qaInteraction) : bool;

    public function edit(QA $qaInteraction, array $keys = array()) : bool;
}
