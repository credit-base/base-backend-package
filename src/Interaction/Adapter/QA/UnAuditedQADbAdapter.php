<?php
namespace Base\Interaction\Adapter\QA;

use Base\ApplyForm\Adapter\ApplyForm\ApplyFormDbAdapter;

use Base\Interaction\Adapter\UnAuditedInteractionDbAdapterTrait;

class UnAuditedQADbAdapter extends ApplyFormDbAdapter
{
    use UnAuditedInteractionDbAdapterTrait;
}
