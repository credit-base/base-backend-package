<?php
namespace Base\Interaction\Adapter\QA;

use Base\Interaction\Model\QA;
use Base\Interaction\Utils\QAMockFactory;

class QAMockAdapter implements IQAAdapter
{
    public function fetchOne($id) : QA
    {
        return QAMockFactory::generateQA($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $qaInteractionList = array();

        foreach ($ids as $id) {
            $qaInteractionList[$id] = QAMockFactory::generateQA($id);
        }

        return $qaInteractionList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(QA $qaInteraction) : bool
    {
        unset($qaInteraction);
        return true;
    }

    public function edit(QA $qaInteraction, array $keys = array()) : bool
    {
        unset($qaInteraction);
        unset($keys);
        return true;
    }
}
