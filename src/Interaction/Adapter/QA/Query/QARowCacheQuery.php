<?php
namespace Base\Interaction\Adapter\QA\Query;

use Marmot\Framework\Query\RowCacheQuery;

class QARowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'qa_id',
            new Persistence\QACache(),
            new Persistence\QADb()
        );
    }
}
