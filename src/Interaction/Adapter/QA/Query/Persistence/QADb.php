<?php
namespace Base\Interaction\Adapter\QA\Query\Persistence;

use Marmot\Framework\Classes\Db;

class QADb extends Db
{
    const TABLE = 'qa';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
