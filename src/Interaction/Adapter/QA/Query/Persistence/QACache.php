<?php
namespace Base\Interaction\Adapter\QA\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class QACache extends Cache
{
    const KEY = 'qa';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
