<?php
namespace Base\NotifyRecord\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\NotifyRecord\Model\NotifyRecord;
use Base\NotifyRecord\Model\NullNotifyRecord;

use BaseSdk\NotifyRecord\Model\NotifyRecord as NotifyRecordSdk;
use BaseSdk\NotifyRecord\Model\NullNotifyRecord as NullNotifyRecordSdk;

class NotifyRecordSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($notifyRecordSdk = null, $notifyRecord = null)
    {
        return $this->translateToObject($notifyRecordSdk, $notifyRecord);
    }

    protected function translateToObject($notifyRecordSdk = null, $notifyRecord = null) : NotifyRecord
    {
        if (!$notifyRecordSdk instanceof NotifyRecordSdk || $notifyRecordSdk instanceof INull) {
            return NullNotifyRecord::getInstance();
        }

        if ($notifyRecord == null) {
            $notifyRecord = new NotifyRecord();
        }

        $notifyRecord->setId($notifyRecordSdk->getId());
        $notifyRecord->setName($notifyRecordSdk->getName());
        $notifyRecord->setHash($notifyRecordSdk->getHash());
        $notifyRecord->setCreateTime($notifyRecordSdk->getCreateTime());
        $notifyRecord->setStatusTime($notifyRecordSdk->getStatusTime());
        $notifyRecord->setUpdateTime($notifyRecordSdk->getUpdateTime());
        $notifyRecord->setStatus($notifyRecordSdk->getStatus());
        
        return $notifyRecord;
    }

    public function objectToValueObject($notifyRecord)
    {
        unset($notifyRecord);

        return NullNotifyRecordSdk::getInstance();
    }
}
