<?php
namespace Base\NotifyRecord\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\NotifyRecord\Model\NotifyRecord;
use Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter;
use Base\NotifyRecord\Adapter\NotifyRecord\NotifyRecordSdkAdapter;
use Base\NotifyRecord\Adapter\NotifyRecord\NotifyRecordMockAdapter;

class NotifyRecordSdkRepository extends Repository implements INotifyRecordAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new NotifyRecordSdkAdapter();
    }

    public function setAdapter(INotifyRecordAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter() : INotifyRecordAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : INotifyRecordAdapter
    {
        return new NotifyRecordMockAdapter();
    }

    public function fetchOne($id) : NotifyRecord
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $number, $size);
    }
}
