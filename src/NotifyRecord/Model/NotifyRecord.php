<?php
namespace Base\NotifyRecord\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

/**
 * notify 触发记录表，用于记录 notify 的触发记录信息
 * @todo 设置错误原因
 */
class NotifyRecord implements IObject
{
    const STATUS = [
        'PENDING' => 0,
        'SUCCESS'=>2
    ];

    const ALLOW_FILE_EXTS = array(
        'xls', 'xlsx'
    );

    use Object;

    private $id;
    /**
     * @param string $name 文件名字
     */
    private $name;
    /**
     * @param string $hash 文件hash
     */
    private $hash;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->hash = '';
        $this->status = self::STATUS['PENDING'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->hash);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setHash(string $hash) : void
    {
        $this->hash = $hash;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array(
            $status,
            self::STATUS
        ) ? $status : self::STATUS['PENDING'];
    }
}
