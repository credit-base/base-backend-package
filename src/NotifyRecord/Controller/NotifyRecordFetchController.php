<?php
namespace Base\NotifyRecord\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\NotifyRecord\View\NotifyRecordView;
use Base\NotifyRecord\Repository\NotifyRecordSdkRepository;
use Base\NotifyRecord\Adapter\NotifyRecord\INotifyRecordAdapter;

class NotifyRecordFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new NotifyRecordSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : INotifyRecordAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new NotifyRecordView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'notifyRecords';
    }
}
