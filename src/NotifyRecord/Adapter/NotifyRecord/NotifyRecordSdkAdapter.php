<?php
namespace Base\NotifyRecord\Adapter\NotifyRecord;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\NotifyRecord\Model\NotifyRecord;
use Base\NotifyRecord\Model\NullNotifyRecord;
use Base\NotifyRecord\Translator\NotifyRecordSdkTranslator;

use BaseSdk\NotifyRecord\Repository\NotifyRecordRestfulRepository;

class NotifyRecordSdkAdapter implements INotifyRecordAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    public function __construct()
    {
        $this->translator = new NotifyRecordSdkTranslator();
        $this->repository = new NotifyRecordRestfulRepository();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }
    
    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : NotifyRecordRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullNotifyRecord::getInstance();
    }

    public function fetchOne($id) : NotifyRecord
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $number, $size);
    }
}
