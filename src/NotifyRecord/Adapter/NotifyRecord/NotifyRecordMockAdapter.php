<?php
namespace Base\NotifyRecord\Adapter\NotifyRecord;

use Base\NotifyRecord\Utils\MockFactory;
use Base\NotifyRecord\Model\NotifyRecord;

class NotifyRecordMockAdapter implements INotifyRecordAdapter
{
    public function fetchOne($id) : NotifyRecord
    {
        return MockFactory::generateNotifyRecord($id);
    }

    public function fetchList(array $ids) : array
    {
        $notifyRecordList = array();

        foreach ($ids as $id) {
            $notifyRecordList[$id] = MockFactory::generateNotifyRecord($id);
        }

        return $notifyRecordList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($number);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }
}
