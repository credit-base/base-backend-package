<?php
namespace Base\NotifyRecord\Adapter\NotifyRecord;

use Base\NotifyRecord\Model\NotifyRecord;

interface INotifyRecordAdapter
{
    public function fetchOne($id) : NotifyRecord;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 0
    ) : array;
}
