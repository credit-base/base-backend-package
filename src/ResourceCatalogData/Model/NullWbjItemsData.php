<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Interfaces\INull;

class NullWbjItemsData extends WbjItemsData implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
