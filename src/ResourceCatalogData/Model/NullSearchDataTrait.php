<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;

trait NullSearchDataTrait
{
    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
