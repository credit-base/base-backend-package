<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Crew\Model\Crew;
use Base\Template\Model\Template;
use Base\UserGroup\Model\UserGroup;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
abstract class SearchData implements IObject, ISearchDataAble
{
    const EXPIRATION_DATE_DEFAULT = '4102329600'; //20991231

    use Object;

    /**
     * @var $id
     */
    protected $id;
    /**
     * @var string $name 主体名称
     */
    protected $name;
    /**
     * @var string $identify 主体标识
     */
    protected $identify;
    /**
     * @var int $infoClassify 信息分类
     */
    protected $infoClassify;
    /**
     * @var int $infoCategory 信息类别
     */
    protected $infoCategory;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var UserGroup $sourceUnit 来源单位
     */
    protected $sourceUnit;
    /**
     * @var int $subjectCategory 主体类别
     */
    protected $subjectCategory;
    /**
     * @var int $dimension 公开范围
     */
    protected $dimension;
    /**
     * @var int $expirationDate 有效期限
     */
    protected $expirationDate;
    /**
     * @var string $hash 摘要hash
     */
    protected $hash;
    /**
     * @var Template $template 资源目录
     */
    protected $template;
    /**
     * @var ItemsData $itemsData 资源目录数据
     */
    protected $itemsData;
    /**
     * @var Task $task 任务
     */
    protected $task;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->identify = '';
        $this->infoClassify = 0;
        $this->infoCategory = 0;
        $this->crew = new Crew();
        $this->sourceUnit = new UserGroup();
        $this->subjectCategory = 0;
        $this->dimension = 0;
        $this->expirationDate = self::EXPIRATION_DATE_DEFAULT;
        $this->hash = '';
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->task = new Task();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->identify);
        unset($this->infoClassify);
        unset($this->infoCategory);
        unset($this->crew);
        unset($this->sourceUnit);
        unset($this->subjectCategory);
        unset($this->dimension);
        unset($this->expirationDate);
        unset($this->hash);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->task);
    }

    /**
     * @marmot-set-id
     */
    public function setId($id) : void
    {
        $this->id = $id;
    }
    /**
     * @marmot-get-id
     */
    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setIdentify(string $identify) : void
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function setInfoClassify(int $infoClassify) : void
    {
        $this->infoClassify = $infoClassify;
    }

    public function getInfoClassify() : int
    {
        return $this->infoClassify;
    }

    public function setInfoCategory(int $infoCategory) : void
    {
        $this->infoCategory = $infoCategory;
    }

    public function getInfoCategory() : int
    {
        return $this->infoCategory;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }

    public function setSubjectCategory(int $subjectCategory) : void
    {
        $this->subjectCategory = $subjectCategory;
    }

    public function getSubjectCategory() : int
    {
        return $this->subjectCategory;
    }

    public function setDimension(int $dimension) : void
    {
        $this->dimension = $dimension;
    }

    public function getDimension() : int
    {
        return $this->dimension;
    }

    public function setExpirationDate(int $expirationDate) : void
    {
        $this->expirationDate = $expirationDate;
    }

    public function getExpirationDate() : int
    {
        return $this->expirationDate;
    }

    public function setHash(string $hash) : void
    {
        $this->hash = $hash;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function setTask(Task $task) : void
    {
        $this->task = $task;
    }

    public function getTask() : Task
    {
        return $this->task;
    }

    public function setTemplate(Template $template) : void
    {
        $this->template = $template;
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }
    
    abstract public function setStatus(int $status) : void;

    abstract public function getItemsData();
}
