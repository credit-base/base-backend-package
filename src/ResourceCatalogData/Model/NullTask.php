<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Interfaces\INull;

class NullTask extends Task implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
