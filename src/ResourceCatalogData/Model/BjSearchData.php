<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;

use Base\Template\Model\BjTemplate;

use Base\ResourceCatalogData\Repository\BjSearchDataSdkRepository;
use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

class BjSearchData extends SearchData
{
    const STATUS = array(
        'CONFIRM' => 0, //待确认
        'ENABLED' => 2, //已确认
        'DISABLED' => -2, //屏蔽
        'DELETED' => -4, //封存
    );

    const FRONT_END_PROCESSOR_STATUS = array(
        'NOT_IMPORT' => 0, //未导入前置机
        'IMPORT' => 2, //已导入前置机
    );

    /**
     * @var string $description 待确认规则描述
     */
    protected $description;
    /**
     * @var int $frontEndProcessorStatus 前置机状态
     */
    protected $frontEndProcessorStatus;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->status = self::STATUS['CONFIRM'];
        $this->description = '';
        $this->frontEndProcessorStatus = self::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'];
        $this->template = new BjTemplate();
        $this->itemsData = new BjItemsData();
        $this->repository = new BjSearchDataSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->status);
        unset($this->description);
        unset($this->frontEndProcessorStatus);
        unset($this->template);
        unset($this->itemsData);
        unset($this->repository);
    }

    public function setItemsData(BjItemsData $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : BjItemsData
    {
        return $this->itemsData;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['CONFIRM'];
    }

    public function setFrontEndProcessorStatus(int $frontEndProcessorStatus) : void
    {
        $this->frontEndProcessorStatus = in_array(
            $frontEndProcessorStatus,
            self::FRONT_END_PROCESSOR_STATUS
        ) ? $frontEndProcessorStatus : self::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'];
    }
    
    public function getFrontEndProcessorStatus() : int
    {
        return $this->frontEndProcessorStatus;
    }

    protected function getRepository() : IBjSearchDataAdapter
    {
        return $this->repository;
    }

    public function confirm() : bool
    {
        if (!$this->isConfirm()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->getRepository()->confirm($this);
    }

    public function isConfirm() : bool
    {
        return $this->getStatus() == self::STATUS['CONFIRM'];
    }

    public function disable() : bool
    {
        if (!$this->isEnabled()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->getRepository()->disable($this);
    }

    public function isEnabled() : bool
    {
        return $this->getStatus() == self::STATUS['ENABLED'];
    }

    public function delete() : bool
    {
        if (!$this->isConfirm()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->getRepository()->deletes($this);
    }
}
