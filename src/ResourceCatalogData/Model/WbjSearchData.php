<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Core;

use Base\Template\Model\WbjTemplate;

use Base\ResourceCatalogData\Repository\WbjSearchDataSdkRepository;
use Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class WbjSearchData extends SearchData
{
    const STATUS = array(
        'ENABLED' => 0 ,
        'DISABLED' => -2
    );

    protected $repository;

    protected $wbjItemsDataRepository;

    public function __construct()
    {
        parent::__construct();
        $this->status = self::STATUS['ENABLED'];
        $this->template = new WbjTemplate();
        $this->itemsData = new WbjItemsData();
        $this->repository = new WbjSearchDataSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->status);
        unset($this->template);
        unset($this->itemsData);
        unset($this->repository);
    }

    public function setItemsData(WbjItemsData $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : WbjItemsData
    {
        return $this->itemsData;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['ENABLED'];
    }

    protected function getRepository() : IWbjSearchDataAdapter
    {
        return $this->repository;
    }

    public function disable() : bool
    {
        if (!$this->isEnabled()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->getRepository()->disable($this);
    }

    public function isEnabled() : bool
    {
        return $this->getStatus() == self::STATUS['ENABLED'];
    }
}
