<?php
namespace Base\ResourceCatalogData\Model;

use Marmot\Interfaces\INull;

class NullGbSearchData extends GbSearchData implements INull
{
    use NullSearchDataTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function confirm() : bool
    {
        return $this->resourceNotExist();
    }

    public function disable() : bool
    {
        return $this->resourceNotExist();
    }
    
    public function delete() : bool
    {
        return $this->resourceNotExist();
    }

    public function isEnabled() : bool
    {
        return $this->resourceNotExist();
    }

    public function isConfirm() : bool
    {
        return $this->resourceNotExist();
    }
}
