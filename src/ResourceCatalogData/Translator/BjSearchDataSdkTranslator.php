<?php
namespace Base\ResourceCatalogData\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Model\NullBjSearchData;

use BaseSdk\ResourceCatalogData\Model\BjSearchData as BjSearchDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullBjSearchData as NullBjSearchDataSdk;

use Base\Crew\Translator\CrewSdkTranslator;

use Base\Template\Translator\BjTemplateSdkTranslator;

class BjSearchDataSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getCrewSdkTranslator() : ISdkTranslator
    {
        return new CrewSdkTranslator();
    }

    protected function getTaskSdkTranslator() : ISdkTranslator
    {
        return new TaskSdkTranslator();
    }

    protected function getBjItemsDataSdkTranslator() : ISdkTranslator
    {
        return new BjItemsDataSdkTranslator();
    }

    protected function getBjTemplateSdkTranslator() : ISdkTranslator
    {
        return new BjTemplateSdkTranslator();
    }
    
    public function valueObjectToObject($bjSearchDataSdk = null, $bjSearchData = null)
    {
        return $this->translateToObject($bjSearchDataSdk, $bjSearchData);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($bjSearchDataSdk = null, $bjSearchData = null) : BjSearchData
    {
        if (!$bjSearchDataSdk instanceof BjSearchDataSdk || $bjSearchDataSdk instanceof INull) {
            return NullBjSearchData::getInstance();
        }

        if ($bjSearchData == null) {
            $bjSearchData = new BjSearchData();
        }

        $crew = $this->getCrewSdkTranslator()->valueObjectToObject($bjSearchDataSdk->getCrew());
        $bjTemplate = $this->getBjTemplateSdkTranslator()->valueObjectToObject($bjSearchDataSdk->getTemplate());
        $bjItemsData = $this->getBjItemsDataSdkTranslator()->valueObjectToObject($bjSearchDataSdk->getItemsData());
        $task = $this->getTaskSdkTranslator()->valueObjectToObject($bjSearchDataSdk->getTask());

        $bjSearchData->setId($bjSearchDataSdk->getId());
        $bjSearchData->setName($bjSearchDataSdk->getName());
        $bjSearchData->setIdentify($bjSearchDataSdk->getIdentify());
        $bjSearchData->setInfoClassify($bjSearchDataSdk->getInfoClassify());
        $bjSearchData->setInfoCategory($bjSearchDataSdk->getInfoCategory());
        $bjSearchData->setCrew($crew);
        $bjSearchData->setSubjectCategory($bjSearchDataSdk->getSubjectCategory());
        $bjSearchData->setDimension($bjSearchDataSdk->getDimension());
        $bjSearchData->setExpirationDate($bjSearchDataSdk->getExpirationDate());
        $bjSearchData->setHash($bjSearchDataSdk->getHash());
        $bjSearchData->setTemplate($bjTemplate);
        $bjSearchData->setItemsData($bjItemsData);
        $bjSearchData->setTask($task);
        $bjSearchData->setDescription($bjSearchDataSdk->getDescription());
        $bjSearchData->setFrontEndProcessorStatus($bjSearchDataSdk->getFrontEndProcessorStatus());
        $bjSearchData->setCreateTime($bjSearchDataSdk->getCreateTime());
        $bjSearchData->setStatusTime($bjSearchDataSdk->getStatusTime());
        $bjSearchData->setUpdateTime($bjSearchDataSdk->getUpdateTime());
        $bjSearchData->setStatus($bjSearchDataSdk->getStatus());

        return $bjSearchData;
    }

    public function objectToValueObject($bjSearchData)
    {
        if (!$bjSearchData instanceof BjSearchData) {
            return NullBjSearchDataSdk::getInstance();
        }

        $crewSdk = $this->getCrewSdkTranslator()->objectToValueObject($bjSearchData->getCrew());
        $bjTemplateSdk = $this->getBjTemplateSdkTranslator()->objectToValueObject($bjSearchData->getTemplate());
        $bjItemsDataSdk = $this->getBjItemsDataSdkTranslator()->objectToValueObject($bjSearchData->getItemsData());
        $taskSdk = $this->getTaskSdkTranslator()->objectToValueObject($bjSearchData->getTask());

        $bjSearchDataSdk = new BjSearchDataSdk(
            $bjSearchData->getId(),
            $bjSearchData->getName(),
            $bjSearchData->getIdentify(),
            $bjSearchData->getInfoClassify(),
            $bjSearchData->getInfoCategory(),
            $crewSdk,
            $bjSearchData->getSubjectCategory(),
            $bjSearchData->getDimension(),
            $bjSearchData->getExpirationDate(),
            $bjSearchData->getHash(),
            $bjTemplateSdk,
            $bjItemsDataSdk,
            $taskSdk,
            $bjSearchData->getDescription(),
            $bjSearchData->getFrontEndProcessorStatus(),
            $bjSearchData->getStatus(),
            $bjSearchData->getStatusTime(),
            $bjSearchData->getCreateTime(),
            $bjSearchData->getUpdateTime()
        );

        return $bjSearchDataSdk;
    }
}
