<?php
namespace Base\ResourceCatalogData\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\ResourceCatalogData\Model\BjItemsData;
use Base\ResourceCatalogData\Model\NullBjItemsData;

use BaseSdk\ResourceCatalogData\Model\BjItemsData as BjItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullBjItemsData as NullBjItemsDataSdk;

class BjItemsDataSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($bjItemsDataSdk = null, $bjItemsData = null)
    {
        return $this->translateToObject($bjItemsDataSdk, $bjItemsData);
    }

    protected function translateToObject($bjItemsDataSdk = null, $bjItemsData = null) : BjItemsData
    {
        if (!$bjItemsDataSdk instanceof BjItemsDataSdk || $bjItemsDataSdk instanceof INull) {
            return NullBjItemsData::getInstance();
        }

        if ($bjItemsData == null) {
            $bjItemsData = new BjItemsData();
        }

        $bjItemsData->setId($bjItemsDataSdk->getId());
        $bjItemsData->setData($bjItemsDataSdk->getData());
        $bjItemsData->setStatus($bjItemsDataSdk->getStatus());
        $bjItemsData->setCreateTime($bjItemsDataSdk->getCreateTime());
        $bjItemsData->setStatusTime($bjItemsDataSdk->getStatusTime());
        $bjItemsData->setUpdateTime($bjItemsDataSdk->getUpdateTime());
        
        return $bjItemsData;
    }

    public function objectToValueObject($bjItemsData)
    {
        unset($bjItemsData);

        return NullBjItemsDataSdk::getInstance();
    }
}
