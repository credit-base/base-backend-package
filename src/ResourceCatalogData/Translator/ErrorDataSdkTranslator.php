<?php
namespace Base\ResourceCatalogData\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\ResourceCatalogData\Model\ErrorData;
use Base\ResourceCatalogData\Model\NullErrorData;

use BaseSdk\ResourceCatalogData\Model\ErrorData as ErrorDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullErrorData as NullErrorDataSdk;

use Base\Crew\Translator\CrewSdkTranslator;

use Base\Template\Translator\TranslatorFactory;

class ErrorDataSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getCrewSdkTranslator() : ISdkTranslator
    {
        return new CrewSdkTranslator();
    }

    protected function getTaskSdkTranslator() : ISdkTranslator
    {
        return new TaskSdkTranslator();
    }

    protected function getErrorItemsDataSdkTranslator() : ISdkTranslator
    {
        return new ErrorItemsDataSdkTranslator();
    }

    protected function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    protected function getTemplateSdkTranslator(int $category) : ISdkTranslator
    {
        return $this->getTranslatorFactory()->getTranslator($category);
    }

    public function valueObjectToObject($errorDataSdk = null, $errorData = null)
    {
        return $this->translateToObject($errorDataSdk, $errorData);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($errorDataSdk = null, $errorData = null) : ErrorData
    {
        if (!$errorDataSdk instanceof ErrorDataSdk || $errorDataSdk instanceof INull) {
            return NullErrorData::getInstance();
        }

        if ($errorData == null) {
            $errorData = new ErrorData();
        }

        $crew = $this->getCrewSdkTranslator()->valueObjectToObject($errorDataSdk->getCrew());
        $template = $this->getTemplateSdkTranslator($errorDataSdk->getCategory())->valueObjectToObject(
            $errorDataSdk->getTemplate()
        );
        $errorItemsData = $this->getErrorItemsDataSdkTranslator()->valueObjectToObject($errorDataSdk->getItemsData());
        $task = $this->getTaskSdkTranslator()->valueObjectToObject($errorDataSdk->getTask());

        $errorData->setId($errorDataSdk->getId());
        $errorData->setName($errorDataSdk->getName());
        $errorData->setIdentify($errorDataSdk->getIdentify());
        $errorData->setInfoClassify($errorDataSdk->getInfoClassify());
        $errorData->setInfoCategory($errorDataSdk->getInfoCategory());
        $errorData->setCrew($crew);
        $errorData->setSubjectCategory($errorDataSdk->getSubjectCategory());
        $errorData->setDimension($errorDataSdk->getDimension());
        $errorData->setExpirationDate($errorDataSdk->getExpirationDate());
        $errorData->setHash($errorDataSdk->getHash());
        $errorData->setTemplate($template);
        $errorData->setItemsData($errorItemsData);
        $errorData->setTask($task);
        $errorData->setCategory($errorDataSdk->getCategory());
        $errorData->setErrorType($errorDataSdk->getErrorType());
        $errorData->setErrorReason($errorDataSdk->getErrorReason());
        $errorData->setCreateTime($errorDataSdk->getCreateTime());
        $errorData->setStatusTime($errorDataSdk->getStatusTime());
        $errorData->setUpdateTime($errorDataSdk->getUpdateTime());
        $errorData->setStatus($errorDataSdk->getStatus());

        return $errorData;
    }

    public function objectToValueObject($errorData)
    {
        if (!$errorData instanceof ErrorData) {
            return NullErrorDataSdk::getInstance();
        }

        $crewSdk = $this->getCrewSdkTranslator()->objectToValueObject($errorData->getCrew());
        $templateSdk = $this->getTemplateSdkTranslator($errorData->getCategory())->objectToValueObject(
            $errorData->getTemplate()
        );
        $errorItemsDataSdk = $this->getErrorItemsDataSdkTranslator()->objectToValueObject($errorData->getItemsData());
        $taskSdk = $this->getTaskSdkTranslator()->objectToValueObject($errorData->getTask());

        $errorDataSdk = new ErrorDataSdk(
            $errorData->getId(),
            $errorData->getName(),
            $errorData->getIdentify(),
            $errorData->getInfoClassify(),
            $errorData->getInfoCategory(),
            $crewSdk,
            $errorData->getSubjectCategory(),
            $errorData->getDimension(),
            $errorData->getExpirationDate(),
            $errorData->getHash(),
            $templateSdk,
            $errorItemsDataSdk,
            $taskSdk,
            $errorData->getCategory(),
            $errorData->getErrorType(),
            $errorData->getErrorReason(),
            $errorData->getStatus(),
            $errorData->getStatusTime(),
            $errorData->getCreateTime(),
            $errorData->getUpdateTime()
        );

        return $errorDataSdk;
    }
}
