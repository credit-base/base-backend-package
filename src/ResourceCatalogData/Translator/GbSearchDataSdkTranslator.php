<?php
namespace Base\ResourceCatalogData\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Model\NullGbSearchData;

use BaseSdk\ResourceCatalogData\Model\GbSearchData as GbSearchDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullGbSearchData as NullGbSearchDataSdk;

use Base\Crew\Translator\CrewSdkTranslator;

use Base\Template\Translator\GbTemplateSdkTranslator;

class GbSearchDataSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getCrewSdkTranslator() : ISdkTranslator
    {
        return new CrewSdkTranslator();
    }

    protected function getTaskSdkTranslator() : ISdkTranslator
    {
        return new TaskSdkTranslator();
    }

    protected function getGbItemsDataSdkTranslator() : ISdkTranslator
    {
        return new GbItemsDataSdkTranslator();
    }

    protected function getGbTemplateSdkTranslator() : ISdkTranslator
    {
        return new GbTemplateSdkTranslator();
    }
    

    public function valueObjectToObject($gbSearchDataSdk = null, $gbSearchData = null)
    {
        return $this->translateToObject($gbSearchDataSdk, $gbSearchData);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($gbSearchDataSdk = null, $gbSearchData = null) : GbSearchData
    {
        if (!$gbSearchDataSdk instanceof GbSearchDataSdk || $gbSearchDataSdk instanceof INull) {
            return NullGbSearchData::getInstance();
        }

        if ($gbSearchData == null) {
            $gbSearchData = new GbSearchData();
        }

        $crew = $this->getCrewSdkTranslator()->valueObjectToObject($gbSearchDataSdk->getCrew());
        $gbTemplate = $this->getGbTemplateSdkTranslator()->valueObjectToObject($gbSearchDataSdk->getTemplate());
        $gbItemsData = $this->getGbItemsDataSdkTranslator()->valueObjectToObject($gbSearchDataSdk->getItemsData());
        $task = $this->getTaskSdkTranslator()->valueObjectToObject($gbSearchDataSdk->getTask());

        $gbSearchData->setId($gbSearchDataSdk->getId());
        $gbSearchData->setName($gbSearchDataSdk->getName());
        $gbSearchData->setIdentify($gbSearchDataSdk->getIdentify());
        $gbSearchData->setInfoClassify($gbSearchDataSdk->getInfoClassify());
        $gbSearchData->setInfoCategory($gbSearchDataSdk->getInfoCategory());
        $gbSearchData->setCrew($crew);
        $gbSearchData->setSubjectCategory($gbSearchDataSdk->getSubjectCategory());
        $gbSearchData->setDimension($gbSearchDataSdk->getDimension());
        $gbSearchData->setExpirationDate($gbSearchDataSdk->getExpirationDate());
        $gbSearchData->setHash($gbSearchDataSdk->getHash());
        $gbSearchData->setTemplate($gbTemplate);
        $gbSearchData->setItemsData($gbItemsData);
        $gbSearchData->setTask($task);
        $gbSearchData->setDescription($gbSearchDataSdk->getDescription());
        $gbSearchData->setFrontEndProcessorStatus($gbSearchDataSdk->getFrontEndProcessorStatus());
        $gbSearchData->setCreateTime($gbSearchDataSdk->getCreateTime());
        $gbSearchData->setStatusTime($gbSearchDataSdk->getStatusTime());
        $gbSearchData->setUpdateTime($gbSearchDataSdk->getUpdateTime());
        $gbSearchData->setStatus($gbSearchDataSdk->getStatus());

        return $gbSearchData;
    }

    public function objectToValueObject($gbSearchData)
    {
        if (!$gbSearchData instanceof GbSearchData) {
            return NullGbSearchDataSdk::getInstance();
        }

        $crewSdk = $this->getCrewSdkTranslator()->objectToValueObject($gbSearchData->getCrew());
        $gbTemplateSdk = $this->getGbTemplateSdkTranslator()->objectToValueObject($gbSearchData->getTemplate());
        $gbItemsDataSdk = $this->getGbItemsDataSdkTranslator()->objectToValueObject($gbSearchData->getItemsData());
        $taskSdk = $this->getTaskSdkTranslator()->objectToValueObject($gbSearchData->getTask());

        $gbSearchDataSdk = new GbSearchDataSdk(
            $gbSearchData->getId(),
            $gbSearchData->getName(),
            $gbSearchData->getIdentify(),
            $gbSearchData->getInfoClassify(),
            $gbSearchData->getInfoCategory(),
            $crewSdk,
            $gbSearchData->getSubjectCategory(),
            $gbSearchData->getDimension(),
            $gbSearchData->getExpirationDate(),
            $gbSearchData->getHash(),
            $gbTemplateSdk,
            $gbItemsDataSdk,
            $taskSdk,
            $gbSearchData->getDescription(),
            $gbSearchData->getFrontEndProcessorStatus(),
            $gbSearchData->getStatus(),
            $gbSearchData->getStatusTime(),
            $gbSearchData->getCreateTime(),
            $gbSearchData->getUpdateTime()
        );

        return $gbSearchDataSdk;
    }
}
