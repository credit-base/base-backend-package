<?php
namespace Base\ResourceCatalogData\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\ResourceCatalogData\Model\GbItemsData;
use Base\ResourceCatalogData\Model\NullGbItemsData;

use BaseSdk\ResourceCatalogData\Model\GbItemsData as GbItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullGbItemsData as NullGbItemsDataSdk;

class GbItemsDataSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($gbItemsDataSdk = null, $gbItemsData = null)
    {
        return $this->translateToObject($gbItemsDataSdk, $gbItemsData);
    }

    protected function translateToObject($gbItemsDataSdk = null, $gbItemsData = null) : GbItemsData
    {
        if (!$gbItemsDataSdk instanceof GbItemsDataSdk || $gbItemsDataSdk instanceof INull) {
            return NullGbItemsData::getInstance();
        }

        if ($gbItemsData == null) {
            $gbItemsData = new GbItemsData();
        }

        $gbItemsData->setId($gbItemsDataSdk->getId());
        $gbItemsData->setData($gbItemsDataSdk->getData());
        $gbItemsData->setStatus($gbItemsDataSdk->getStatus());
        $gbItemsData->setCreateTime($gbItemsDataSdk->getCreateTime());
        $gbItemsData->setStatusTime($gbItemsDataSdk->getStatusTime());
        $gbItemsData->setUpdateTime($gbItemsDataSdk->getUpdateTime());
        
        return $gbItemsData;
    }

    public function objectToValueObject($gbItemsData)
    {
        unset($gbItemsData);

        return NullGbItemsDataSdk::getInstance();
    }
}
