<?php
namespace Base\ResourceCatalogData\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\Model\NullWbjSearchData;

use BaseSdk\ResourceCatalogData\Model\WbjSearchData as WbjSearchDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullWbjSearchData as NullWbjSearchDataSdk;

use Base\Crew\Translator\CrewSdkTranslator;

use Base\Template\Translator\WbjTemplateSdkTranslator;

class WbjSearchDataSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getCrewSdkTranslator() : ISdkTranslator
    {
        return new CrewSdkTranslator();
    }

    protected function getTaskSdkTranslator() : ISdkTranslator
    {
        return new TaskSdkTranslator();
    }

    protected function getWbjItemsDataSdkTranslator() : ISdkTranslator
    {
        return new WbjItemsDataSdkTranslator();
    }

    protected function getWbjTemplateSdkTranslator() : ISdkTranslator
    {
        return new WbjTemplateSdkTranslator();
    }
    

    public function valueObjectToObject($wbjSearchDataSdk = null, $wbjSearchData = null)
    {
        return $this->translateToObject($wbjSearchDataSdk, $wbjSearchData);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($wbjSearchDataSdk = null, $wbjSearchData = null) : WbjSearchData
    {
        if (!$wbjSearchDataSdk instanceof WbjSearchDataSdk || $wbjSearchDataSdk instanceof INull) {
            return NullWbjSearchData::getInstance();
        }

        if ($wbjSearchData == null) {
            $wbjSearchData = new WbjSearchData();
        }

        $crew = $this->getCrewSdkTranslator()->valueObjectToObject($wbjSearchDataSdk->getCrew());
        $wbjTemplate = $this->getWbjTemplateSdkTranslator()->valueObjectToObject($wbjSearchDataSdk->getTemplate());
        $wbjItemsData = $this->getWbjItemsDataSdkTranslator()->valueObjectToObject($wbjSearchDataSdk->getItemsData());
        $task = $this->getTaskSdkTranslator()->valueObjectToObject($wbjSearchDataSdk->getTask());

        $wbjSearchData->setId($wbjSearchDataSdk->getId());
        $wbjSearchData->setName($wbjSearchDataSdk->getName());
        $wbjSearchData->setIdentify($wbjSearchDataSdk->getIdentify());
        $wbjSearchData->setInfoClassify($wbjSearchDataSdk->getInfoClassify());
        $wbjSearchData->setInfoCategory($wbjSearchDataSdk->getInfoCategory());
        $wbjSearchData->setCrew($crew);
        $wbjSearchData->setSubjectCategory($wbjSearchDataSdk->getSubjectCategory());
        $wbjSearchData->setDimension($wbjSearchDataSdk->getDimension());
        $wbjSearchData->setExpirationDate($wbjSearchDataSdk->getExpirationDate());
        $wbjSearchData->setHash($wbjSearchDataSdk->getHash());
        $wbjSearchData->setTemplate($wbjTemplate);
        $wbjSearchData->setItemsData($wbjItemsData);
        $wbjSearchData->setTask($task);
        $wbjSearchData->setCreateTime($wbjSearchDataSdk->getCreateTime());
        $wbjSearchData->setStatusTime($wbjSearchDataSdk->getStatusTime());
        $wbjSearchData->setUpdateTime($wbjSearchDataSdk->getUpdateTime());
        $wbjSearchData->setStatus($wbjSearchDataSdk->getStatus());

        return $wbjSearchData;
    }

    public function objectToValueObject($wbjSearchData)
    {
        if (!$wbjSearchData instanceof WbjSearchData) {
            return NullWbjSearchDataSdk::getInstance();
        }

        $crewSdk = $this->getCrewSdkTranslator()->objectToValueObject($wbjSearchData->getCrew());
        $wbjTemplateSdk = $this->getWbjTemplateSdkTranslator()->objectToValueObject($wbjSearchData->getTemplate());
        $wbjItemsDataSdk = $this->getWbjItemsDataSdkTranslator()->objectToValueObject($wbjSearchData->getItemsData());
        $taskSdk = $this->getTaskSdkTranslator()->objectToValueObject($wbjSearchData->getTask());

        $wbjSearchDataSdk = new WbjSearchDataSdk(
            $wbjSearchData->getId(),
            $wbjSearchData->getName(),
            $wbjSearchData->getIdentify(),
            $wbjSearchData->getInfoClassify(),
            $wbjSearchData->getInfoCategory(),
            $crewSdk,
            $wbjSearchData->getSubjectCategory(),
            $wbjSearchData->getDimension(),
            $wbjSearchData->getExpirationDate(),
            $wbjSearchData->getHash(),
            $wbjTemplateSdk,
            $wbjItemsDataSdk,
            $taskSdk,
            $wbjSearchData->getStatus(),
            $wbjSearchData->getStatusTime(),
            $wbjSearchData->getCreateTime(),
            $wbjSearchData->getUpdateTime()
        );

        return $wbjSearchDataSdk;
    }
}
