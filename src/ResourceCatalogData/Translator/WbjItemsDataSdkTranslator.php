<?php
namespace Base\ResourceCatalogData\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\ResourceCatalogData\Model\WbjItemsData;
use Base\ResourceCatalogData\Model\NullWbjItemsData;

use BaseSdk\ResourceCatalogData\Model\WbjItemsData as WbjItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullWbjItemsData as NullWbjItemsDataSdk;

class WbjItemsDataSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($wbjItemsDataSdk = null, $wbjItemsData = null)
    {
        return $this->translateToObject($wbjItemsDataSdk, $wbjItemsData);
    }

    protected function translateToObject($wbjItemsDataSdk = null, $wbjItemsData = null) : WbjItemsData
    {
        if (!$wbjItemsDataSdk instanceof WbjItemsDataSdk || $wbjItemsDataSdk instanceof INull) {
            return NullWbjItemsData::getInstance();
        }

        if ($wbjItemsData == null) {
            $wbjItemsData = new WbjItemsData();
        }

        $wbjItemsData->setId($wbjItemsDataSdk->getId());
        $wbjItemsData->setData($wbjItemsDataSdk->getData());
        $wbjItemsData->setStatus($wbjItemsDataSdk->getStatus());
        $wbjItemsData->setCreateTime($wbjItemsDataSdk->getCreateTime());
        $wbjItemsData->setStatusTime($wbjItemsDataSdk->getStatusTime());
        $wbjItemsData->setUpdateTime($wbjItemsDataSdk->getUpdateTime());
        
        return $wbjItemsData;
    }

    public function objectToValueObject($wbjItemsData)
    {
        unset($wbjItemsData);

        return NullWbjItemsDataSdk::getInstance();
    }
}
