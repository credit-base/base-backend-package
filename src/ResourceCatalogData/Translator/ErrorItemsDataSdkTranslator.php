<?php
namespace Base\ResourceCatalogData\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\ResourceCatalogData\Model\ErrorItemsData;
use Base\ResourceCatalogData\Model\NullErrorItemsData;

use BaseSdk\ResourceCatalogData\Model\ErrorItemsData as ErrorItemsDataSdk;
use BaseSdk\ResourceCatalogData\Model\NullErrorItemsData as NullErrorItemsDataSdk;

class ErrorItemsDataSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($errorItemsDataSdk = null, $errorItemsData = null)
    {
        return $this->translateToObject($errorItemsDataSdk, $errorItemsData);
    }

    protected function translateToObject($errorItemsDataSdk = null, $errorItemsData = null) : ErrorItemsData
    {
        if (!$errorItemsDataSdk instanceof ErrorItemsDataSdk || $errorItemsDataSdk instanceof INull) {
            return NullErrorItemsData::getInstance();
        }

        if ($errorItemsData == null) {
            $errorItemsData = new ErrorItemsData();
        }

        $errorItemsData->setId($errorItemsDataSdk->getId());
        $errorItemsData->setData($errorItemsDataSdk->getData());
        $errorItemsData->setStatus($errorItemsDataSdk->getStatus());
        $errorItemsData->setCreateTime($errorItemsDataSdk->getCreateTime());
        $errorItemsData->setStatusTime($errorItemsDataSdk->getStatusTime());
        $errorItemsData->setUpdateTime($errorItemsDataSdk->getUpdateTime());
        
        return $errorItemsData;
    }

    public function objectToValueObject($errorItemsData)
    {
        unset($errorItemsData);

        return NullErrorItemsDataSdk::getInstance();
    }
}
