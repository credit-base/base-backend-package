<?php
namespace Base\ResourceCatalogData\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\ResourceCatalogData\Model\Task;
use Base\ResourceCatalogData\Model\NullTask;

use BaseSdk\ResourceCatalogData\Model\Task as TaskSdk;
use BaseSdk\ResourceCatalogData\Model\NullTask as NullTaskSdk;

use Base\Crew\Translator\CrewSdkTranslator;

class TaskSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getCrewSdkTranslator() : ISdkTranslator
    {
        return new CrewSdkTranslator();
    }

    public function valueObjectToObject($taskSdk = null, $task = null)
    {
        return $this->translateToObject($taskSdk, $task);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($taskSdk = null, $task = null) : Task
    {
        if (!$taskSdk instanceof TaskSdk || $taskSdk instanceof INull) {
            return NullTask::getInstance();
        }

        if ($task == null) {
            $task = new Task();
        }

        $crew = $this->getCrewSdkTranslator()->valueObjectToObject($taskSdk->getCrew());

        $task->setId($taskSdk->getId());
        $task->setCrew($crew);
        $task->setPid($taskSdk->getPid());
        $task->setTotal($taskSdk->getTotal());
        $task->setSuccessNumber($taskSdk->getSuccessNumber());
        $task->setFailureNumber($taskSdk->getFailureNumber());
        $task->setSourceCategory($taskSdk->getSourceCategory());
        $task->setSourceTemplate($taskSdk->getSourceTemplate());
        $task->setTargetCategory($taskSdk->getTargetCategory());
        $task->setTargetTemplate($taskSdk->getTargetTemplate());
        $task->setTargetRule($taskSdk->getTargetRule());
        $task->setScheduleTask($taskSdk->getScheduleTask());
        $task->setErrorNumber($taskSdk->getErrorNumber());
        $task->setFileName($taskSdk->getFileName());
        $task->setCreateTime($taskSdk->getCreateTime());
        $task->setStatusTime($taskSdk->getStatusTime());
        $task->setUpdateTime($taskSdk->getUpdateTime());
        $task->setStatus($taskSdk->getStatus());

        return $task;
    }

    public function objectToValueObject($task)
    {
        if (!$task instanceof Task) {
            return NullTaskSdk::getInstance();
        }

        $crewSdk = $this->getCrewSdkTranslator()->objectToValueObject($task->getCrew());

        $taskSdk = new TaskSdk(
            $task->getId(),
            $crewSdk,
            $task->getPid(),
            $task->getTotal(),
            $task->getSuccessNumber(),
            $task->getFailureNumber(),
            $task->getSourceCategory(),
            $task->getSourceTemplate(),
            $task->getTargetCategory(),
            $task->getTargetTemplate(),
            $task->getTargetRule(),
            $task->getScheduleTask(),
            $task->getErrorNumber(),
            $task->getFileName(),
            $task->getStatus(),
            $task->getStatusTime(),
            $task->getCreateTime(),
            $task->getUpdateTime()
        );

        return $taskSdk;
    }
}
