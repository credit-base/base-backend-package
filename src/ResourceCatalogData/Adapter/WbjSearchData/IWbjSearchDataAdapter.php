<?php
namespace Base\ResourceCatalogData\Adapter\WbjSearchData;

use Base\ResourceCatalogData\Model\WbjSearchData;

interface IWbjSearchDataAdapter
{
    public function fetchOne($id) : WbjSearchData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 0
    ) : array;

    public function disable(WbjSearchData $wbjSearchData) : bool;
}
