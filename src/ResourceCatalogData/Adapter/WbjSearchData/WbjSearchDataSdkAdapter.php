<?php
namespace Base\ResourceCatalogData\Adapter\WbjSearchData;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\Model\NullWbjSearchData;
use Base\ResourceCatalogData\Translator\WbjSearchDataSdkTranslator;

use BaseSdk\ResourceCatalogData\Repository\WbjSearchDataRestfulRepository;

use Base\Crew\Repository\CrewRepository;

class WbjSearchDataSdkAdapter implements IWbjSearchDataAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    private $crewRepository;

    public function __construct()
    {
        $this->translator = new WbjSearchDataSdkTranslator();
        $this->repository = new WbjSearchDataRestfulRepository();
        $this->crewRepository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
        unset($this->crewRepository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : WbjSearchDataRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullWbjSearchData::getInstance();
    }

    public function fetchOne($id) : WbjSearchData
    {
        $wbjSearchData = $this->fetchOneAction($id);
        $this->fetchCrew($wbjSearchData);

        return $wbjSearchData;
    }

    public function fetchList(array $ids) : array
    {
        $wbjSearchDataList = $this->fetchListAction($ids);
        $this->fetchCrew($wbjSearchDataList);

        return $wbjSearchDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        list($wbjSearchDataList, $count) = $this->filterAction($filter, $sort, $number, $size);
        $this->fetchCrew($wbjSearchDataList);
        
        return [$wbjSearchDataList, $count];
    }

    public function disable(WbjSearchData $wbjSearchData) : bool
    {
        $wbjSearchDataSdk = $this->getTranslator()->objectToValueObject($wbjSearchData);

        $result = $this->getRestfulRepository()->disable($wbjSearchDataSdk);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        return true;
    }

    protected function fetchCrew($wbjSearchData)
    {
        return is_array($wbjSearchData) ?
        $this->fetchCrewByList($wbjSearchData) :
        $this->fetchCrewByObject($wbjSearchData);
    }

    protected function fetchCrewByObject(WbjSearchData $wbjSearchData)
    {
        $crewId = $wbjSearchData->getCrew()->getId();
        $crew = $this->getCrewRepository()->fetchOne($crewId);
        $wbjSearchData->setCrew($crew);
        $wbjSearchData->setSourceUnit($crew->getUserGroup());
    }

    protected function fetchCrewByList(array $wbjSearchDataList)
    {
        $crewIds = array();
        foreach ($wbjSearchDataList as $wbjSearchData) {
            $crewIds[] = $wbjSearchData->getCrew()->getId();
        }

        $crewList = $this->getCrewRepository()->fetchList($crewIds);
        if (!empty($crewList)) {
            foreach ($wbjSearchDataList as $wbjSearchData) {
                $crewId = $wbjSearchData->getCrew()->getId();
                if (isset($crewList[$crewId])
                    && ($crewList[$crewId]->getId() == $crewId)) {
                    $crew = $crewList[$crewId];
                    $wbjSearchData->setCrew($crew);
                    $wbjSearchData->setSourceUnit($crew->getUserGroup());
                }
            }
        }
    }
}
