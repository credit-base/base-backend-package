<?php
namespace Base\ResourceCatalogData\Adapter\GbSearchData;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Model\NullGbSearchData;
use Base\ResourceCatalogData\Translator\GbSearchDataSdkTranslator;

use BaseSdk\ResourceCatalogData\Repository\GbSearchDataRestfulRepository;

use Base\Crew\Repository\CrewRepository;

class GbSearchDataSdkAdapter implements IGbSearchDataAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    private $crewRepository;

    public function __construct()
    {
        $this->translator = new GbSearchDataSdkTranslator();
        $this->repository = new GbSearchDataRestfulRepository();
        $this->crewRepository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
        unset($this->crewRepository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : GbSearchDataRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullGbSearchData::getInstance();
    }

    public function fetchOne($id) : GbSearchData
    {
        $gbSearchData = $this->fetchOneAction($id);
        $this->fetchCrew($gbSearchData);

        return $gbSearchData;
    }

    public function fetchList(array $ids) : array
    {
        $gbSearchDataList = $this->fetchListAction($ids);
        $this->fetchCrew($gbSearchDataList);

        return $gbSearchDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        list($gbSearchDataList, $count) = $this->filterAction($filter, $sort, $number, $size);
        $this->fetchCrew($gbSearchDataList);
        
        return [$gbSearchDataList, $count];
    }

    public function confirm(GbSearchData $gbSearchData) : bool
    {
        $gbSearchDataSdk = $this->getTranslator()->objectToValueObject($gbSearchData);

        $result = $this->getRestfulRepository()->confirm($gbSearchDataSdk);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        return true;
    }

    public function deletes(GbSearchData $gbSearchData) : bool
    {
        $gbSearchDataSdk = $this->getTranslator()->objectToValueObject($gbSearchData);

        $result = $this->getRestfulRepository()->deletes($gbSearchDataSdk);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        return true;
    }

    public function disable(GbSearchData $gbSearchData) : bool
    {
        $gbSearchDataSdk = $this->getTranslator()->objectToValueObject($gbSearchData);

        $result = $this->getRestfulRepository()->disable($gbSearchDataSdk);

        if (!$result) {
            $this->mapErrors();
            return false;
        }
        
        return true;
    }

    protected function fetchCrew($gbSearchData)
    {
        return is_array($gbSearchData) ?
        $this->fetchCrewByList($gbSearchData) :
        $this->fetchCrewByObject($gbSearchData);
    }

    protected function fetchCrewByObject(GbSearchData $gbSearchData)
    {
        $crewId = $gbSearchData->getCrew()->getId();
        $crew = $this->getCrewRepository()->fetchOne($crewId);
        $gbSearchData->setCrew($crew);
        $gbSearchData->setSourceUnit($crew->getUserGroup());
    }

    protected function fetchCrewByList(array $gbSearchDataList)
    {
        $crewIds = array();
        foreach ($gbSearchDataList as $gbSearchData) {
            $crewIds[] = $gbSearchData->getCrew()->getId();
        }

        $crewList = $this->getCrewRepository()->fetchList($crewIds);
        if (!empty($crewList)) {
            foreach ($gbSearchDataList as $gbSearchData) {
                $crewId = $gbSearchData->getCrew()->getId();
                if (isset($crewList[$crewId])
                    && ($crewList[$crewId]->getId() == $crewId)) {
                    $crew = $crewList[$crewId];
                    $gbSearchData->setCrew($crew);
                    $gbSearchData->setSourceUnit($crew->getUserGroup());
                }
            }
        }
    }
}
