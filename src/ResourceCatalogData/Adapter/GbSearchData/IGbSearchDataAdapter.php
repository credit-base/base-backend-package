<?php
namespace Base\ResourceCatalogData\Adapter\GbSearchData;

use Base\ResourceCatalogData\Model\GbSearchData;

interface IGbSearchDataAdapter
{
    public function fetchOne($id) : GbSearchData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 0
    ) : array;

    public function confirm(GbSearchData $gbSearchData) : bool;

    public function deletes(GbSearchData $gbSearchData) : bool;

    public function disable(GbSearchData $gbSearchData) : bool;
}
