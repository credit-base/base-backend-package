<?php
namespace Base\ResourceCatalogData\Adapter\ErrorData;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\ResourceCatalogData\Model\ErrorData;
use Base\ResourceCatalogData\Model\NullErrorData;
use Base\ResourceCatalogData\Translator\ErrorDataSdkTranslator;

use BaseSdk\ResourceCatalogData\Repository\ErrorDataRestfulRepository;

class ErrorDataSdkAdapter implements IErrorDataAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    public function __construct()
    {
        $this->translator = new ErrorDataSdkTranslator();
        $this->repository = new ErrorDataRestfulRepository();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : ErrorDataRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullErrorData::getInstance();
    }

    public function fetchOne($id) : ErrorData
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $number, $size);
    }
}
