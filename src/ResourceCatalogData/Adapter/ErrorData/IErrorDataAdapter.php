<?php
namespace Base\ResourceCatalogData\Adapter\ErrorData;

use Base\ResourceCatalogData\Model\ErrorData;

interface IErrorDataAdapter
{
    public function fetchOne($id) : ErrorData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 0
    ) : array;
}
