<?php
namespace Base\ResourceCatalogData\Adapter\BjSearchData;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Model\NullBjSearchData;
use Base\ResourceCatalogData\Translator\BjSearchDataSdkTranslator;

use BaseSdk\ResourceCatalogData\Repository\BjSearchDataRestfulRepository;

use Base\Crew\Repository\CrewRepository;

class BjSearchDataSdkAdapter implements IBjSearchDataAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    private $crewRepository;

    public function __construct()
    {
        $this->translator = new BjSearchDataSdkTranslator();
        $this->repository = new BjSearchDataRestfulRepository();
        $this->crewRepository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
        unset($this->crewRepository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : BjSearchDataRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullBjSearchData::getInstance();
    }

    public function fetchOne($id) : BjSearchData
    {
        $bjSearchData = $this->fetchOneAction($id);
        $this->fetchCrew($bjSearchData);

        return $bjSearchData;
    }

    public function fetchList(array $ids) : array
    {
        $bjSearchDataList = $this->fetchListAction($ids);
        $this->fetchCrew($bjSearchDataList);

        return $bjSearchDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        list($bjSearchDataList, $count) = $this->filterAction($filter, $sort, $number, $size);
        $this->fetchCrew($bjSearchDataList);
        
        return [$bjSearchDataList, $count];
    }

    public function confirm(BjSearchData $bjSearchData) : bool
    {
        $bjSearchDataSdk = $this->getTranslator()->objectToValueObject($bjSearchData);

        $result = $this->getRestfulRepository()->confirm($bjSearchDataSdk);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        return true;
    }

    public function deletes(BjSearchData $bjSearchData) : bool
    {
        $bjSearchDataSdk = $this->getTranslator()->objectToValueObject($bjSearchData);

        $result = $this->getRestfulRepository()->deletes($bjSearchDataSdk);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        return true;
    }

    public function disable(BjSearchData $bjSearchData) : bool
    {
        $bjSearchDataSdk = $this->getTranslator()->objectToValueObject($bjSearchData);

        $result = $this->getRestfulRepository()->disable($bjSearchDataSdk);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        return true;
    }

    protected function fetchCrew($bjSearchData)
    {
        return is_array($bjSearchData) ?
        $this->fetchCrewByList($bjSearchData) :
        $this->fetchCrewByObject($bjSearchData);
    }

    protected function fetchCrewByObject(BjSearchData $bjSearchData)
    {
        $crewId = $bjSearchData->getCrew()->getId();
        $crew = $this->getCrewRepository()->fetchOne($crewId);
        $bjSearchData->setCrew($crew);
        $bjSearchData->setSourceUnit($crew->getUserGroup());
    }

    protected function fetchCrewByList(array $bjSearchDataList)
    {
        $crewIds = array();
        foreach ($bjSearchDataList as $bjSearchData) {
            $crewIds[] = $bjSearchData->getCrew()->getId();
        }

        $crewList = $this->getCrewRepository()->fetchList($crewIds);
        if (!empty($crewList)) {
            foreach ($bjSearchDataList as $bjSearchData) {
                $crewId = $bjSearchData->getCrew()->getId();
                if (isset($crewList[$crewId])
                    && ($crewList[$crewId]->getId() == $crewId)) {
                    $crew = $crewList[$crewId];
                    $bjSearchData->setCrew($crew);
                    $bjSearchData->setSourceUnit($crew->getUserGroup());
                }
            }
        }
    }
}
