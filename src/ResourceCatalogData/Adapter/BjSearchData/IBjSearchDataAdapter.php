<?php
namespace Base\ResourceCatalogData\Adapter\BjSearchData;

use Base\ResourceCatalogData\Model\BjSearchData;

interface IBjSearchDataAdapter
{
    public function fetchOne($id) : BjSearchData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 0
    ) : array;

    public function confirm(BjSearchData $bjSearchData) : bool;

    public function deletes(BjSearchData $bjSearchData) : bool;

    public function disable(BjSearchData $bjSearchData) : bool;
}
