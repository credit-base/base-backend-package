<?php
namespace Base\ResourceCatalogData\Adapter\BjSearchData;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Utils\BjSearchDataMockFactory;

class BjSearchDataMockAdapter implements IBjSearchDataAdapter
{
    public function fetchOne($id) : BjSearchData
    {
        return BjSearchDataMockFactory::generateBjSearchData($id);
    }

    public function fetchList(array $ids) : array
    {
        $bjSearchDataList = array();

        foreach ($ids as $id) {
            $bjSearchDataList[$id] = BjSearchDataMockFactory::generateBjSearchData($id);
        }

        return $bjSearchDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($number);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function confirm(BjSearchData $bjSearchData) : bool
    {
        unset($bjSearchData);
        return true;
    }

    public function deletes(BjSearchData $bjSearchData) : bool
    {
        unset($bjSearchData);
        return true;
    }

    public function disable(BjSearchData $bjSearchData) : bool
    {
        unset($bjSearchData);
        return true;
    }
}
