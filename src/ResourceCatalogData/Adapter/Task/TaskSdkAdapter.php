<?php
namespace Base\ResourceCatalogData\Adapter\Task;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\ResourceCatalogData\Model\Task;
use Base\ResourceCatalogData\Model\NullTask;
use Base\ResourceCatalogData\Translator\TaskSdkTranslator;

use BaseSdk\ResourceCatalogData\Repository\TaskRestfulRepository;

class TaskSdkAdapter implements ITaskAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    public function __construct()
    {
        $this->translator = new TaskSdkTranslator();
        $this->repository = new TaskRestfulRepository();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : TaskRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullTask::getInstance();
    }

    public function fetchOne($id) : Task
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $number, $size);
    }
}
