<?php
namespace Base\ResourceCatalogData\Adapter\Task;

use Base\ResourceCatalogData\Model\Task;

interface ITaskAdapter
{
    public function fetchOne($id) : Task;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 0
    ) : array;
}
