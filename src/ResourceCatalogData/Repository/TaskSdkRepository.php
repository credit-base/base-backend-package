<?php
namespace Base\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use Base\ResourceCatalogData\Model\Task;
use Base\ResourceCatalogData\Adapter\Task\ITaskAdapter;
use Base\ResourceCatalogData\Adapter\Task\TaskSdkAdapter;
use Base\ResourceCatalogData\Adapter\Task\TaskMockAdapter;

class TaskSdkRepository extends Repository implements ITaskAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new TaskSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(ITaskAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : ITaskAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ITaskAdapter
    {
        return new TaskMockAdapter();
    }

    public function fetchOne($id) : Task
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $number, $size);
    }
}
