<?php
namespace Base\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;
use Base\ResourceCatalogData\Adapter\WbjSearchData\WbjSearchDataSdkAdapter;
use Base\ResourceCatalogData\Adapter\WbjSearchData\WbjSearchDataMockAdapter;

class WbjSearchDataSdkRepository extends Repository implements IWbjSearchDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new WbjSearchDataSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IWbjSearchDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IWbjSearchDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IWbjSearchDataAdapter
    {
        return new WbjSearchDataMockAdapter();
    }

    public function fetchOne($id) : WbjSearchData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $number, $size);
    }

    public function disable(WbjSearchData $wbjSearchData) : bool
    {
        return $this->getAdapter()->disable($wbjSearchData);
    }
}
