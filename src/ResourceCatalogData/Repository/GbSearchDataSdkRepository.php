<?php
namespace Base\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use Base\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataSdkAdapter;
use Base\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataMockAdapter;

class GbSearchDataSdkRepository extends Repository implements IGbSearchDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new GbSearchDataSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IGbSearchDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IGbSearchDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IGbSearchDataAdapter
    {
        return new GbSearchDataMockAdapter();
    }

    public function fetchOne($id) : GbSearchData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $number, $size);
    }

    public function confirm(GbSearchData $gbSearchData) : bool
    {
        return $this->getAdapter()->confirm($gbSearchData);
    }

    public function deletes(GbSearchData $gbSearchData) : bool
    {
        return $this->getAdapter()->deletes($gbSearchData);
    }

    public function disable(GbSearchData $gbSearchData) : bool
    {
        return $this->getAdapter()->disable($gbSearchData);
    }
}
