<?php
namespace Base\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use Base\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataSdkAdapter;
use Base\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataMockAdapter;

class BjSearchDataSdkRepository extends Repository implements IBjSearchDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new BjSearchDataSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IBjSearchDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IBjSearchDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IBjSearchDataAdapter
    {
        return new BjSearchDataMockAdapter();
    }

    public function fetchOne($id) : BjSearchData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $number, $size);
    }

    public function confirm(BjSearchData $bjSearchData) : bool
    {
        return $this->getAdapter()->confirm($bjSearchData);
    }

    public function deletes(BjSearchData $bjSearchData) : bool
    {
        return $this->getAdapter()->deletes($bjSearchData);
    }

    public function disable(BjSearchData $bjSearchData) : bool
    {
        return $this->getAdapter()->disable($bjSearchData);
    }
}
