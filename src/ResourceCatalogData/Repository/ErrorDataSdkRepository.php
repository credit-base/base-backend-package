<?php
namespace Base\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use Base\ResourceCatalogData\Model\ErrorData;
use Base\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;
use Base\ResourceCatalogData\Adapter\ErrorData\ErrorDataSdkAdapter;
use Base\ResourceCatalogData\Adapter\ErrorData\ErrorDataMockAdapter;

class ErrorDataSdkRepository extends Repository implements IErrorDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new ErrorDataSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IErrorDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IErrorDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IErrorDataAdapter
    {
        return new ErrorDataMockAdapter();
    }

    public function fetchOne($id) : ErrorData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $number, $size);
    }
}
