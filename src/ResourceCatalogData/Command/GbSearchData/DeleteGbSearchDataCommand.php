<?php
namespace Base\ResourceCatalogData\Command\GbSearchData;

use Marmot\Interfaces\ICommand;

class DeleteGbSearchDataCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
