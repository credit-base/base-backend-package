<?php
namespace Base\ResourceCatalogData\Command\GbSearchData;

use Marmot\Interfaces\ICommand;

class DisableGbSearchDataCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
