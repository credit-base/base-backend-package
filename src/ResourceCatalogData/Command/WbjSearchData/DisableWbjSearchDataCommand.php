<?php
namespace Base\ResourceCatalogData\Command\WbjSearchData;

use Marmot\Interfaces\ICommand;

class DisableWbjSearchDataCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
