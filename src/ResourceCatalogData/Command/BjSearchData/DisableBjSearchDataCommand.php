<?php
namespace Base\ResourceCatalogData\Command\BjSearchData;

use Marmot\Interfaces\ICommand;

class DisableBjSearchDataCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
