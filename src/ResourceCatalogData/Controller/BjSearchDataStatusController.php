<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\View\BjSearchDataView;
use Base\ResourceCatalogData\Repository\BjSearchDataSdkRepository;
use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use Base\ResourceCatalogData\Command\BjSearchData\ConfirmBjSearchDataCommand;
use Base\ResourceCatalogData\Command\BjSearchData\DeleteBjSearchDataCommand;
use Base\ResourceCatalogData\Command\BjSearchData\DisableBjSearchDataCommand;
use Base\ResourceCatalogData\CommandHandler\BjSearchData\BjSearchDataCommandHandlerFactory;

class BjSearchDataStatusController extends Controller
{
    use JsonApiTrait;

    private $repository;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new BjSearchDataSdkRepository();
        $this->commandBus = new CommandBus(new BjSearchDataCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();

        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : IBjSearchDataAdapter
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * 对应路由 /bjSearchData/{id:\d+}/confirm
     * 确认, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function confirm(int $id)
    {
        if (!empty($id)) {
            $command = new ConfirmBjSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $bjSearchData  = $this->getRepository()->fetchOne($id);
                if ($bjSearchData instanceof BjSearchData) {
                    $this->render(new BjSearchDataView($bjSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /bjSearchData/{id:\d+}/disable
     * 屏蔽, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $command = new DisableBjSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $bjSearchData  = $this->getRepository()->fetchOne($id);
                if ($bjSearchData instanceof BjSearchData) {
                    $this->render(new BjSearchDataView($bjSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /bjSearchData/{id:\d+}/delete
     * 封存, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function delete(int $id)
    {
        if (!empty($id)) {
            $command = new DeleteBjSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $bjSearchData  = $this->getRepository()->fetchOne($id);
                if ($bjSearchData instanceof BjSearchData) {
                    $this->render(new BjSearchDataView($bjSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
