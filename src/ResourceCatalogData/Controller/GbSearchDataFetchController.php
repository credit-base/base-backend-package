<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\ResourceCatalogData\View\GbSearchDataView;
use Base\ResourceCatalogData\Repository\GbSearchDataSdkRepository;
use Base\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;

class GbSearchDataFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new GbSearchDataSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IGbSearchDataAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new GbSearchDataView($data);
    }

    protected function getResourceName() : string
    {
        return 'gbSearchData';
    }
}
