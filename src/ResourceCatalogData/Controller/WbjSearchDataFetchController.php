<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\ResourceCatalogData\View\WbjSearchDataView;
use Base\ResourceCatalogData\Repository\WbjSearchDataSdkRepository;
use Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class WbjSearchDataFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WbjSearchDataSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IWbjSearchDataAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new WbjSearchDataView($data);
    }

    protected function getResourceName() : string
    {
        return 'wbjSearchData';
    }
}
