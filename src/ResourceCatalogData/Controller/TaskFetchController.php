<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\ResourceCatalogData\View\TaskView;
use Base\ResourceCatalogData\Repository\TaskSdkRepository;
use Base\ResourceCatalogData\Adapter\Task\ITaskAdapter;

class TaskFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new TaskSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ITaskAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new TaskView($data);
    }

    protected function getResourceName() : string
    {
        return 'tasks';
    }
}
