<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\ResourceCatalogData\View\ErrorDataView;
use Base\ResourceCatalogData\Repository\ErrorDataSdkRepository;
use Base\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

class ErrorDataFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ErrorDataSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IErrorDataAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new ErrorDataView($data);
    }

    protected function getResourceName() : string
    {
        return 'errorDatas';
    }
}
