<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\View\WbjSearchDataView;
use Base\ResourceCatalogData\Repository\WbjSearchDataSdkRepository;
use Base\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;
use Base\ResourceCatalogData\Command\WbjSearchData\DisableWbjSearchDataCommand;
use Base\ResourceCatalogData\CommandHandler\WbjSearchData\WbjSearchDataCommandHandlerFactory;

class WbjSearchDataStatusController extends Controller
{
    use JsonApiTrait;

    private $repository;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new WbjSearchDataSdkRepository();
        $this->commandBus = new CommandBus(new WbjSearchDataCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();

        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : IWbjSearchDataAdapter
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    

    /**
     * 对应路由 /wbjSearchData/{id:\d+}/disable
     * 屏蔽, 通过PATCH传参
     * @param int id 委办局资源目录数据id
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $command = new DisableWbjSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $wbjSearchData  = $this->getRepository()->fetchOne($id);
                if ($wbjSearchData instanceof WbjSearchData) {
                    $this->render(new WbjSearchDataView($wbjSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
