<?php
namespace Base\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\ResourceCatalogData\View\BjSearchDataView;
use Base\ResourceCatalogData\Repository\BjSearchDataSdkRepository;
use Base\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

class BjSearchDataFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BjSearchDataSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IBjSearchDataAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new BjSearchDataView($data);
    }

    protected function getResourceName() : string
    {
        return 'bjSearchData';
    }
}
