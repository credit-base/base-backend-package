<?php
namespace Base\ResourceCatalogData\CommandHandler\GbSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class GbSearchDataCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\ResourceCatalogData\Command\GbSearchData\ConfirmGbSearchDataCommand'=>
        'Base\ResourceCatalogData\CommandHandler\GbSearchData\ConfirmGbSearchDataCommandHandler',
        'Base\ResourceCatalogData\Command\GbSearchData\DisableGbSearchDataCommand'=>
        'Base\ResourceCatalogData\CommandHandler\GbSearchData\DisableGbSearchDataCommandHandler',
        'Base\ResourceCatalogData\Command\GbSearchData\DeleteGbSearchDataCommand'=>
        'Base\ResourceCatalogData\CommandHandler\GbSearchData\DeleteGbSearchDataCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
