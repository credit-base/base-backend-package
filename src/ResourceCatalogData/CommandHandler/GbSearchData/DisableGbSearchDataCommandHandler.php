<?php
namespace Base\ResourceCatalogData\CommandHandler\GbSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\ResourceCatalogData\Command\GbSearchData\DisableGbSearchDataCommand;

class DisableGbSearchDataCommandHandler implements ICommandHandler
{
    use GbSearchDataCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof DisableGbSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }

        $gbSearchData = $this->fetchGbSearchData($command->id);

        return $gbSearchData->disable();
    }
}
