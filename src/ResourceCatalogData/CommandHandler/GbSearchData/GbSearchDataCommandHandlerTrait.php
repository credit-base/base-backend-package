<?php
namespace Base\ResourceCatalogData\CommandHandler\GbSearchData;

use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Repository\GbSearchDataSdkRepository;

trait GbSearchDataCommandHandlerTrait
{
    private $repository;

    public function __construct()
    {
        $this->repository = new GbSearchDataSdkRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : GbSearchDataSdkRepository
    {
        return $this->repository;
    }

    protected function fetchGbSearchData(int $id) : GbSearchData
    {
        return $this->getRepository()->fetchOne($id);
    }
}
