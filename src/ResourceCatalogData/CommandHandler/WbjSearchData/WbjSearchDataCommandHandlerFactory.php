<?php
namespace Base\ResourceCatalogData\CommandHandler\WbjSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class WbjSearchDataCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\ResourceCatalogData\Command\WbjSearchData\DisableWbjSearchDataCommand'=>
        'Base\ResourceCatalogData\CommandHandler\WbjSearchData\DisableWbjSearchDataCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
