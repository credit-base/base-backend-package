<?php
namespace Base\ResourceCatalogData\CommandHandler\WbjSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\ResourceCatalogData\Command\WbjSearchData\DisableWbjSearchDataCommand;

class DisableWbjSearchDataCommandHandler implements ICommandHandler
{
    use WbjSearchDataCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof DisableWbjSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }

        $wbjSearchData = $this->fetchWbjSearchData($command->id);

        return $wbjSearchData->disable();
    }
}
