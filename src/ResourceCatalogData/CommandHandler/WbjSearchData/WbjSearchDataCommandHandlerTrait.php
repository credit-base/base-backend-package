<?php
namespace Base\ResourceCatalogData\CommandHandler\WbjSearchData;

use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\Repository\WbjSearchDataSdkRepository;

trait WbjSearchDataCommandHandlerTrait
{
    private $repository;

    public function __construct()
    {
        $this->repository = new WbjSearchDataSdkRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : WbjSearchDataSdkRepository
    {
        return $this->repository;
    }

    protected function fetchWbjSearchData(int $id) : WbjSearchData
    {
        return $this->getRepository()->fetchOne($id);
    }
}
