<?php
namespace Base\ResourceCatalogData\CommandHandler\BjSearchData;

use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Repository\BjSearchDataSdkRepository;

trait BjSearchDataCommandHandlerTrait
{
    private $repository;

    public function __construct()
    {
        $this->repository = new BjSearchDataSdkRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : BjSearchDataSdkRepository
    {
        return $this->repository;
    }

    protected function fetchBjSearchData(int $id) : BjSearchData
    {
        return $this->getRepository()->fetchOne($id);
    }
}
