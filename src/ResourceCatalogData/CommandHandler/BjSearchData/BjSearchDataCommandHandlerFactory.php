<?php
namespace Base\ResourceCatalogData\CommandHandler\BjSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class BjSearchDataCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\ResourceCatalogData\Command\BjSearchData\ConfirmBjSearchDataCommand'=>
        'Base\ResourceCatalogData\CommandHandler\BjSearchData\ConfirmBjSearchDataCommandHandler',
        'Base\ResourceCatalogData\Command\BjSearchData\DisableBjSearchDataCommand'=>
        'Base\ResourceCatalogData\CommandHandler\BjSearchData\DisableBjSearchDataCommandHandler',
        'Base\ResourceCatalogData\Command\BjSearchData\DeleteBjSearchDataCommand'=>
        'Base\ResourceCatalogData\CommandHandler\BjSearchData\DeleteBjSearchDataCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
