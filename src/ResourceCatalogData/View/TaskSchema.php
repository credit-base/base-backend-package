<?php
namespace Base\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

use Base\ResourceCatalogData\Model\Task;

class TaskSchema extends SchemaProvider
{
    protected $resourceType = 'tasks';

    public function getId($task) : int
    {
        return $task->getId();
    }

    public function getAttributes($task) : array
    {
        $fileName = $task->getFileName();
        $errorFileName = Task::ERROR_FILE_NAME_PREFIX.'_'.$task->getSourceCategory().
        '_'.$task->getTargetTemplate().'_'.$task->getTargetCategory().'_'.$fileName;
        $fileName = $task->isFailureFileDownloadStatus() ? $errorFileName : $fileName;

        return [
            'pid' => $task->getPid(),
            'total' => $task->getTotal(),
            'successNumber' => $task->getSuccessNumber(),
            'failureNumber' => $task->getFailureNumber(),
            'sourceCategory' => $task->getSourceCategory(),
            'sourceTemplate' => $task->getSourceTemplate(),
            'targetCategory' => $task->getTargetCategory(),
            'targetTemplate' => $task->getTargetTemplate(),
            'targetRule' => $task->getTargetRule(),
            'scheduleTask' => $task->getScheduleTask(),
            'errorNumber' => $task->getErrorNumber(),
            'fileName' => $fileName,
            
            'status' => $task->getStatus(),
            'createTime' => $task->getCreateTime(),
            'updateTime' => $task->getUpdateTime(),
            'statusTime' => $task->getStatusTime(),
        ];
    }

    public function getRelationships($task, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'crew' => [self::DATA => $task->getCrew()],
            'userGroup' => [self::DATA => $task->getUserGroup()]
        ];
    }
}
