<?php
namespace Base\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class BjSearchDataSchema extends SchemaProvider
{
    protected $resourceType = 'bjSearchData';

    public function getId($bjSearchData) : int
    {
        return $bjSearchData->getId();
    }

    public function getAttributes($bjSearchData) : array
    {
        return [
            'infoClassify' => $bjSearchData->getInfoClassify(),
            'infoCategory' => $bjSearchData->getInfoCategory(),
            'subjectCategory' => $bjSearchData->getSubjectCategory(),
            'dimension' => $bjSearchData->getDimension(),
            'name' => $bjSearchData->getName(),
            'identify' => $bjSearchData->getIdentify(),
            'expirationDate' => $bjSearchData->getExpirationDate(),
            'description' => $bjSearchData->getDescription(),
            'status' => $bjSearchData->getStatus(),
            'frontEndProcessorStatus' => $bjSearchData->getFrontEndProcessorStatus(),
            'createTime' => $bjSearchData->getCreateTime(),
            'updateTime' => $bjSearchData->getUpdateTime(),
            'statusTime' => $bjSearchData->getStatusTime(),
        ];
    }

    public function getRelationships($bjSearchData, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'crew' => [self::DATA => $bjSearchData->getCrew()],
            'sourceUnit' => [self::DATA => $bjSearchData->getSourceUnit()],
            'itemsData' => [self::DATA => $bjSearchData->getItemsData()],
            'template' => [self::DATA => $bjSearchData->getTemplate()]
        ];
    }
}
