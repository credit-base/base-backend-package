<?php
namespace Base\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class WbjSearchDataSchema extends SchemaProvider
{
    protected $resourceType = 'wbjSearchData';

    public function getId($wbjSearchData) : int
    {
        return $wbjSearchData->getId();
    }

    public function getAttributes($wbjSearchData) : array
    {
        return [
            'infoClassify' => $wbjSearchData->getInfoClassify(),
            'infoCategory' => $wbjSearchData->getInfoCategory(),
            'subjectCategory' => $wbjSearchData->getSubjectCategory(),
            'dimension' => $wbjSearchData->getDimension(),
            'name' => $wbjSearchData->getName(),
            'identify' => $wbjSearchData->getIdentify(),
            'expirationDate' => $wbjSearchData->getExpirationDate(),
            'status' => $wbjSearchData->getStatus(),
            'createTime' => $wbjSearchData->getCreateTime(),
            'updateTime' => $wbjSearchData->getUpdateTime(),
            'statusTime' => $wbjSearchData->getStatusTime(),
        ];
    }

    public function getRelationships($wbjSearchData, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'crew' => [self::DATA => $wbjSearchData->getCrew()],
            'sourceUnit' => [self::DATA => $wbjSearchData->getSourceUnit()],
            'itemsData' => [self::DATA => $wbjSearchData->getItemsData()],
            'template' => [self::DATA => $wbjSearchData->getTemplate()]
        ];
    }
}
