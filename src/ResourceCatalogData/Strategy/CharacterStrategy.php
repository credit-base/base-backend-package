<?php
namespace Base\ResourceCatalogData\Strategy;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Base\ResourceCatalogData\Model\ISearchDataAble;

class CharacterStrategy implements IDataStrategy
{
    const TEXT_MIN_LENGTH = 5;
    
    public function validate($value, $rule = array()) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(null, $rule['length'])->validate($value)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'data'));
            return false;
        }

        return true;
    }

    public function mock($rule = array())
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed(mt_rand());

        $data = $faker->text($rule['length']+self::TEXT_MIN_LENGTH);

        if ($rule['identify'] == 'ZTMC') {
            $data = $faker->company();

            if (isset($rule['subjectCategory']) &&
                in_array(ISearchDataAble::SUBJECT_CATEGORY['ZRR'], $rule['subjectCategory'])) {
                $data = $faker->name();
            }
        }

        if ($rule['identify'] == 'TYSHXYDM' || $rule['identify'] == 'ZJHM') {
            $data = $faker->creditCardNumber();
        }

        return $data;
    }
}
