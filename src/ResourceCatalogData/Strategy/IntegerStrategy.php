<?php
namespace Base\ResourceCatalogData\Strategy;

use Marmot\Core;

use Respect\Validation\Validator as V;

class IntegerStrategy implements IDataStrategy
{
    public function validate($value, $rule = array()) : bool
    {
        if (!is_numeric($value) || !V::charset('UTF-8')->length(null, $rule['length'])->validate($value)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'data'));
            return false;
        }

        return true;
    }

    public function mock($rule = array())
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed(mt_rand());
        return $faker->randomNumber(rand(1, 9));
    }
}
