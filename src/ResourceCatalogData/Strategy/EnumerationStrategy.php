<?php
namespace Base\ResourceCatalogData\Strategy;

use Marmot\Core;

use Respect\Validation\Validator as V;

class EnumerationStrategy implements IDataStrategy
{
    public function validate($value, $rule = array()) : bool
    {
        if (!V::stringType()->validate($value) || !in_array($value, $rule['options'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'data'));
            return false;
        }

        return true;
    }

    public function mock($rule = array())
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed(mt_rand());
        return $faker->randomElement($rule['options']);
    }
}
