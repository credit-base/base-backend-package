<?php
namespace Base\ResourceCatalogData\Strategy;

interface IDataStrategy
{
    public function validate($value, $rule = array()) : bool;

    public function mock($rule = array());
}
