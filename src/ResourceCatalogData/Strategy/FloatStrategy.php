<?php
namespace Base\ResourceCatalogData\Strategy;

use Marmot\Core;

use Respect\Validation\Validator as V;

class FloatStrategy implements IDataStrategy
{
    const DEFAULT_LENGTH = 24;

    public function validate($value, $rule = array()) : bool
    {
        unset($rule);
        $reg = '/^(([1-9][0-9]{0,24})|([0]\.\d{1,6}|[1-9][0-9]{0,24}\.\d{1,6}))$/';
        
        if (!V::floatVal()->validate($value) || !preg_match($reg, $value)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'data'));
            return false;
        }

        return true;
    }

    public function mock($rule = array())
    {
        unset($rule);
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed(mt_rand());
        return $faker->randomFloat(24);
    }
}
