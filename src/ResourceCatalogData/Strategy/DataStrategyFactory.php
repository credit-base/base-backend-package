<?php
namespace Base\ResourceCatalogData\Strategy;

use Base\Template\Model\Template;

class DataStrategyFactory
{
    const MAPS = array(
        Template::TYPE['ZFX']=>'\Base\ResourceCatalogData\Strategy\CharacterStrategy',
        Template::TYPE['RQX']=>'\Base\ResourceCatalogData\Strategy\DateStrategy',
        Template::TYPE['ZSX']=>'\Base\ResourceCatalogData\Strategy\IntegerStrategy',
        Template::TYPE['FDX']=>'\Base\ResourceCatalogData\Strategy\FloatStrategy',
        Template::TYPE['MJX']=>'\Base\ResourceCatalogData\Strategy\EnumerationStrategy',
        Template::TYPE['JHX']=>'\Base\ResourceCatalogData\Strategy\CollectionStrategy'
    );

    public function getStrategy(int $type) : IDataStrategy
    {
        $strategy = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';
        return class_exists($strategy) ? new $strategy : new NullDataStrategy();
    }
}
