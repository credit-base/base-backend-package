<?php
namespace Base\Common\CommandHandler;

use Base\Common\Model\IApproveAble;
use Base\Common\Command\RejectCommand;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

abstract class RejectCommandHandler implements ICommandHandler
{
    abstract protected function fetchIApplyObject($id) : IApproveAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(RejectCommand $command)
    {
        $this->rejectAble = $this->fetchIApplyObject($command->id);

        $this->rejectAble->setRejectReason($command->rejectReason);
        
        return $this->rejectAble->reject();
    }
}
