<?php
namespace Base\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Common\Model\ITopAble;
use Base\Common\Command\CancelTopCommand;

abstract class CancelTopCommandHandler implements ICommandHandler
{
    abstract protected function fetchITopObject($id) : ITopAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(CancelTopCommand $command)
    {
        $this->cancelTopAble = $this->fetchITopObject($command->id);
        return $this->cancelTopAble->cancelTop();
    }
}
