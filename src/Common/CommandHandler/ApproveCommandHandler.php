<?php
namespace Base\Common\CommandHandler;

use Base\Common\Model\IApproveAble;
use Base\Common\Command\ApproveCommand;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

abstract class ApproveCommandHandler implements ICommandHandler
{
    abstract protected function fetchIApplyObject($id) : IApproveAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ApproveCommand $command)
    {
        $this->approveAble = $this->fetchIApplyObject($command->id);

        return $this->approveAble->approve();
    }
}
