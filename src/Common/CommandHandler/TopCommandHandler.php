<?php
namespace Base\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Common\Model\ITopAble;
use Base\Common\Command\TopCommand;

abstract class TopCommandHandler implements ICommandHandler
{
    abstract protected function fetchITopObject($id) : ITopAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(TopCommand $command)
    {
        $this->topAble = $this->fetchITopObject($command->id);

        return $this->topAble->top();
    }
}
