<?php
namespace Base\Common\Model;

use Marmot\Core;

trait TopAbleTrait
{
    protected $stick;
    
    public function setStick(int $stick) : void
    {
        $this->stick = in_array($stick, ITopAble::STICK) ? $stick : ITopAble::STICK['DISABLED'];
    }

    public function getStick() : int
    {
        return $this->stick;
    }
    
    public function top() : bool
    {
        if (!$this->isCancelTop()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'stick'));
            return false;
        }
        
        return $this->updateStick(ITopAble::STICK['ENABLED']);
    }

    public function cancelTop() : bool
    {
        if (!$this->isTop()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'stick'));
            return false;
        }
        
        return $this->updateStick(ITopAble::STICK['DISABLED']);
    }

    public function isTop() : bool
    {
        return $this->getStick() == ITopAble::STICK['ENABLED'];
    }

    public function isCancelTop() : bool
    {
        return $this->getStick() == ITopAble::STICK['DISABLED'];
    }
    
    protected function updateStick(int $stick) : bool
    {
        $this->setStick($stick);
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'stick',
                    'updateTime'
                ));
    }
}
