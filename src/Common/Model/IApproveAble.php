<?php
namespace Base\Common\Model;

interface IApproveAble
{
    const APPLY_STATUS = array(
        'PENDING' => 0,
        'APPROVE' => 2,
        'REJECT' => -2,
        'REVOKE' => -4
    );

    const OPERATION_TYPE = array(
        'NULL' => 0,
        'ADD' => 1,
        'EDIT' => 2,
        'ENABLE' => 3,
        'DISABLE' => 4,
        'TOP' => 5,
        'CANCEL_TOP' => 6,
        'MOVE' => 7,
        'ACCEPT' => 8
    );

    public function approve() : bool;

    public function reject() : bool;
}
