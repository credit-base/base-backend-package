<?php
namespace Base\Common\Model;

interface IOperate
{
    public function add() : bool;
    
    public function edit() : bool;
}
