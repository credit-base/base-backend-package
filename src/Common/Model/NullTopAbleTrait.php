<?php
namespace Base\Common\Model;

trait NullTopAbleTrait
{
    public function top() : bool
    {
        return $this->resourceNotExist();
    }

    public function cancelTop() : bool
    {
        return $this->resourceNotExist();
    }

    protected function updateStick(int $stick) : bool
    {
        unset($stick);
        return $this->resourceNotExist();
    }

    public function isTop() : bool
    {
        return $this->resourceNotExist();
    }

    public function isCancelTop() : bool
    {
        return $this->resourceNotExist();
    }

    abstract protected function resourceNotExist() : bool;
}
