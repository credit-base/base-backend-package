<?php
namespace Base\Common\Model;

interface IResubmitAble
{
    public function resubmit() : bool;
}
