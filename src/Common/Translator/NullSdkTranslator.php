<?php
namespace Base\Common\Translator;

use Marmot\Interfaces\INull;
use Marmot\Core;

class NullSdkTranslator implements ISdkTranslator, INull
{
    protected static $instance;
   
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    private function translatorNotExist() : bool
    {
        Core::setLastError(TRANSLATOR_NOT_EXIST);
        return false;
    }

    public function valueObjectToObject($valueObject = null, $object = null)
    {
        unset($valueObject);
        unset($object);

        return $this->translatorNotExist();
    }

    public function valueObjectToObjects(array $expression)
    {
        unset($expression);

        return $this->translatorNotExist();
    }

    public function objectToValueObject($object)
    {
        unset($object);

        return $this->translatorNotExist();
    }
}
