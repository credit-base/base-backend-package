<?php
namespace Base\Common\Translator;

interface ISdkTranslator
{
    public function valueObjectToObject($valueObject = null, $object = null);

    public function valueObjectToObjects(array $expression);

    public function objectToValueObject($object);
}
