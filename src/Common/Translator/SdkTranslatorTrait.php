<?php
namespace Base\Common\Translator;

trait SdkTranslatorTrait
{
    public function valueObjectToObjects(array $expression) : array
    {
        if (empty($expression)) {
            return array(array(), 0);
        }

        $valueObjectList = isset($expression[0]) ? $expression[0] : array();
        $count = isset($expression[1]) ? $expression[1] : count($valueObjectList);
        $results = array();

        foreach ($valueObjectList as $valueObject) {
            $results[$valueObject->getId()] = $this->translateToObject($valueObject);
        }

        return [$results, $count];
    }
}
