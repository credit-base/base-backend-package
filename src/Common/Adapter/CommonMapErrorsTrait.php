<?php
namespace Base\Common\Adapter;

use Marmot\Core;

trait CommonMapErrorsTrait
{
    abstract protected function getMapErrors() : array;

    public function commonMapErrors()
    {
        return [
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR,
            102 => RESOURCE_CAN_NOT_MODIFY,
            103 => RESOURCE_ALREADY_EXIST,
            104 => PARAMETER_ERROR
        ];
    }

    protected function mapErrors() : void
    {
        $id = Core::getLastError()->getId();
        $pointer = isset(Core::getLastError()->getSource()['pointer']) ?
                    Core::getLastError()->getSource()['pointer'] :
                    '';
        $source = array('pointer'=>$pointer);

        $mapErrors = $this->getMapErrors();
    

        if (isset($mapErrors[$id])) {
            is_array($mapErrors[$id])
            ? Core::setLastError($mapErrors[$id][$pointer], $source)
            : Core::setLastError($mapErrors[$id], $source);
        }
    }
}
