<?php
namespace Base\Common\Adapter;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\IObject;
use Marmot\Framework\Classes\Repository;

use Base\Common\Translator\ISdkTranslator;

trait SdkAdapterTrait
{
    abstract protected function getTranslator() : ISdkTranslator;

    abstract protected function getRestfulRepository() : Repository;

    abstract protected function mapErrors() : void;

    abstract protected function getNullObject() : INull;

    protected function addAction(IObject $object) : bool
    {
        $valueObject = $this->getTranslator()->objectToValueObject($object);

        $result = $this->getRestfulRepository()->add($valueObject);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        $object->setId($result);
        return true;
    }

    protected function editAction(IObject $object, array $keys = array()) : bool
    {
        $valueObject = $this->getTranslator()->objectToValueObject($object);

        $result = $this->getRestfulRepository()->edit($valueObject, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }

        return true;
    }

    protected function fetchOneAction($id) : IObject
    {
        $valueObject = $this->getRestfulRepository()->scenario()->fetchOne($id);

        if ($valueObject instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return $this->getNullObject();
        }

        $object = $this->getTranslator()->valueObjectToObject($valueObject);

        return $object;
    }

    protected function fetchListAction(array $ids) : array
    {
        $objectList = array();
        
        $valueObjectList = $this->getRestfulRepository()->scenario()->fetchList($ids);

        if (empty($valueObjectList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getTranslator();
        foreach ($valueObjectList as $valueObject) {
            $object = $translator->valueObjectToObject($valueObject);
            $objectList[$object->getId()] = $object;
        }

        return $objectList;
    }

    protected function filterAction(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        $objectList = array();
        
        list($valueObjectList, $count) = $this->getRestfulRepository()->scenario()->filter(
            $filter,
            $sort,
            $number,
            $size
        );

        if (empty($valueObjectList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array(array(), 0);
        }

        $translator = $this->getTranslator();
        foreach ($valueObjectList as $valueObject) {
            $object = $translator->valueObjectToObject($valueObject);
            $objectList[$object->getId()] = $object;
        }

        return [$objectList, $count];
    }
}
