<?php
namespace Base\Common\Controller\Factory;

use Base\Common\Controller\NullResubmitController;
use Base\Common\Controller\Interfaces\IResubmitAbleController;

class ResubmitControllerFactory
{
    const MAPS = array(
        'unAuditedNews'=>'\Base\News\Controller\NewsResubmitController',
        'unAuditedJournals'=>'\Base\Journal\Controller\JournalResubmitController',
        'unAuditedRules'=>'\Base\Rule\Controller\RuleServiceResubmitController',
        'unAuditedComplaints'=>'\Base\Interaction\Controller\Complaint\ComplaintResubmitController',
        'unAuditedPraises'=>'\Base\Interaction\Controller\Praise\PraiseResubmitController',
        'unAuditedAppeals'=>'\Base\Interaction\Controller\Appeal\AppealResubmitController',
        'unAuditedFeedbacks'=>'\Base\Interaction\Controller\Feedback\FeedbackResubmitController',
        'unAuditedQas'=>'\Base\Interaction\Controller\QA\QAResubmitController',
    );

    public static function getController(string $resource) : IResubmitAbleController
    {
        $resubmitController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($resubmitController) ? new $resubmitController : NullResubmitController::getInstance();
    }
}
