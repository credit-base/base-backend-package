<?php
namespace Base\Common\Controller\Factory;

use Base\Common\Controller\NullEnableController;
use Base\Common\Controller\Interfaces\IEnableAbleController;

class EnableControllerFactory
{
    const MAPS = array(
        'crews'=>'Base\Crew\Controller\CrewEnableController',
        'news'=>'\Base\News\Controller\NewsEnableController',
        'members'=>'Base\Member\Controller\MemberEnableController',
        'journals'=>'\Base\Journal\Controller\JournalEnableController',
    );

    public static function getController(string $resource) : IEnableAbleController
    {
        $enableController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';
        
        return class_exists($enableController) ? new $enableController : NullEnableController::getInstance();
    }
}
