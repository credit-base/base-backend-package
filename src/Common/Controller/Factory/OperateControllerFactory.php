<?php
namespace Base\Common\Controller\Factory;

use Base\Common\Controller\NullOperateController;
use Marmot\Framework\Common\Controller\IOperateController;

class OperateControllerFactory
{
    const MAPS = array(
        'departments'=>'\Base\Department\Controller\DepartmentOperateController',
        'crews'=>'\Base\Crew\Controller\CrewOperateController',
        'gbTemplates'=>'\Base\Template\Controller\GbTemplateOperateController',
        'bjTemplates'=>'\Base\Template\Controller\BjTemplateOperateController',
        'baseTemplates'=>'Base\Template\Controller\BaseTemplateOperateController',
        'templates'=>'\Base\Template\Controller\WbjTemplateOperateController',
        'qzjTemplates'=>'Base\Template\Controller\QzjTemplateOperateController',
        'news'=>'\Base\News\Controller\NewsOperateController',
        'members'=>'\Base\Member\Controller\MemberOperateController',
        'journals'=>'\Base\Journal\Controller\JournalOperateController',
        'creditPhotography'=>'\Base\CreditPhotography\Controller\CreditPhotographyOperateController',
        'rules'=>'\Base\Rule\Controller\RuleServiceOperateController',
        'websiteCustomizes'=>'Base\WebsiteCustomize\Controller\WebsiteCustomizeOperateController',
        'complaints'=>'\Base\Interaction\Controller\Complaint\ComplaintOperateController',
        'praises'=>'\Base\Interaction\Controller\Praise\PraiseOperateController',
        'appeals'=>'\Base\Interaction\Controller\Appeal\AppealOperateController',
        'feedbacks'=>'\Base\Interaction\Controller\Feedback\FeedbackOperateController',
        'qas'=>'\Base\Interaction\Controller\QA\QAOperateController',
    );

    public static function getController(string $resource) : IOperateController
    {
        $controller = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($controller) ? new $controller : NullOperateController::getInstance();
    }
}
