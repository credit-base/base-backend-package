<?php
namespace Base\Common\Controller\Factory;

use Base\Common\Controller\NullApproveController;
use Base\Common\Controller\Interfaces\IApproveAbleController;

class ApproveControllerFactory
{
    const MAPS = array(
        'unAuditedNews'=>'\Base\News\Controller\NewsApproveController',
        'unAuditedJournals'=>'\Base\Journal\Controller\JournalApproveController',
        'creditPhotography'=>'\Base\CreditPhotography\Controller\CreditPhotographyApproveController',
        'unAuditedRules'=>'\Base\Rule\Controller\RuleServiceApproveController',
        'unAuditedComplaints'=>'\Base\Interaction\Controller\Complaint\ComplaintApproveController',
        'unAuditedPraises'=>'\Base\Interaction\Controller\Praise\PraiseApproveController',
        'unAuditedAppeals'=>'\Base\Interaction\Controller\Appeal\AppealApproveController',
        'unAuditedFeedbacks'=>'\Base\Interaction\Controller\Feedback\FeedbackApproveController',
        'unAuditedQas'=>'\Base\Interaction\Controller\QA\QAApproveController',
    );

    public static function getController(string $resource) : IApproveAbleController
    {
        $approveController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';
        return class_exists($approveController) ? new $approveController : NullApproveController::getInstance();
    }
}
