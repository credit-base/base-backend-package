<?php
namespace Base\Common\Controller\Factory;

use Base\Common\Controller\NullFetchController;
use Marmot\Framework\Common\Controller\IFetchController;

class FetchControllerFactory
{
    const MAPS = array(
        'crews'=>'Base\Crew\Controller\CrewFetchController',
        'userGroups'=>'Base\UserGroup\Controller\UserGroupFetchController',
        'departments'=>'Base\Department\Controller\DepartmentFetchController',
        'gbTemplates'=>'Base\Template\Controller\GbTemplateFetchController',
        'bjTemplates'=>'Base\Template\Controller\BjTemplateFetchController',
        'baseTemplates'=>'Base\Template\Controller\BaseTemplateFetchController',
        'templates'=>'Base\Template\Controller\WbjTemplateFetchController',
        'qzjTemplates'=>'Base\Template\Controller\QzjTemplateFetchController',
        'parentTasks'=>'Base\WorkOrderTask\Controller\ParentTaskFetchController',
        'workOrderTasks'=>'Base\WorkOrderTask\Controller\WorkOrderTaskFetchController',
        'wbjSearchData'=>'Base\ResourceCatalogData\Controller\WbjSearchDataFetchController',
        'bjSearchData'=>'Base\ResourceCatalogData\Controller\BjSearchDataFetchController',
        'gbSearchData'=>'Base\ResourceCatalogData\Controller\GbSearchDataFetchController',
        'news'=>'\Base\News\Controller\NewsFetchController',
        'unAuditedNews'=>'\Base\News\Controller\UnAuditedNewsFetchController',
        'members'=>'Base\Member\Controller\MemberFetchController',
        'journals'=>'\Base\Journal\Controller\JournalFetchController',
        'unAuditedJournals'=>'\Base\Journal\Controller\UnAuditedJournalFetchController',
        'creditPhotography'=>'\Base\CreditPhotography\Controller\CreditPhotographyFetchController',
        'rules'=>'\Base\Rule\Controller\RuleServiceFetchController',
        'unAuditedRules'=>'\Base\Rule\Controller\UnAuditedRuleServiceFetchController',
        'tasks'=>'Base\ResourceCatalogData\Controller\TaskFetchController',
        'errorDatas'=>'Base\ResourceCatalogData\Controller\ErrorDataFetchController',
        'notifyRecords'=>'Base\NotifyRecord\Controller\NotifyRecordFetchController',
        'websiteCustomizes'=>'Base\WebsiteCustomize\Controller\WebsiteCustomizeFetchController',
        'enterprises'=>'Base\Enterprise\Controller\EnterpriseFetchController',
        'complaints'=>'\Base\Interaction\Controller\Complaint\ComplaintFetchController',
        'praises'=>'\Base\Interaction\Controller\Praise\PraiseFetchController',
        'appeals'=>'\Base\Interaction\Controller\Appeal\AppealFetchController',
        'feedbacks'=>'\Base\Interaction\Controller\Feedback\FeedbackFetchController',
        'qas'=>'\Base\Interaction\Controller\QA\QAFetchController',
        'unAuditedComplaints'=>'\Base\Interaction\Controller\Complaint\UnAuditedComplaintFetchController',
        'unAuditedPraises'=>'\Base\Interaction\Controller\Praise\UnAuditedPraiseFetchController',
        'unAuditedAppeals'=>'\Base\Interaction\Controller\Appeal\UnAuditedAppealFetchController',
        'unAuditedFeedbacks'=>'\Base\Interaction\Controller\Feedback\UnAuditedFeedbackFetchController',
        'unAuditedQas'=>'\Base\Interaction\Controller\QA\UnAuditedQAFetchController',
    );

    public static function getController(string $resource) : IFetchController
    {
        $controller = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($controller) ? new $controller : NullFetchController::getInstance();
    }
}
