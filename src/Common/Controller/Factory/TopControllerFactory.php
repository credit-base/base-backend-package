<?php
namespace Base\Common\Controller\Factory;

use Base\Common\Controller\NullTopController;
use Base\Common\Controller\Interfaces\ITopAbleController;

class TopControllerFactory
{
    const MAPS = array(
        'news'=>'\Base\News\Controller\NewsTopController',
    );

    public static function getController(string $resource) : ITopAbleController
    {
        $topController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';
        
        return class_exists($topController) ? new $topController : NullTopController::getInstance();
    }
}
