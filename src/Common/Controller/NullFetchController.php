<?php
namespace Base\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Controller\IFetchController;

class NullFetchController implements IFetchController, INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function fetchList(string $ids)
    {
        unset($ids);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
    
    public function fetchOne(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
    }

    public function filter()
    {
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
