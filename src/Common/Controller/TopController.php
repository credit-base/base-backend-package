<?php
namespace Base\Common\Controller;

use Marmot\Framework\Classes\Controller;

use Base\Common\Controller\Factory\TopControllerFactory;
use Base\Common\Controller\Interfaces\ITopAbleController;

class TopController extends Controller
{
    protected function getController(string $resource) : ITopAbleController
    {
        return TopControllerFactory::getController($resource);
    }

    public function top(string $resource, int $id)
    {
        $controller = $this->getController($resource);
        
        return $controller->top($id);
    }
    
    public function cancelTop(string $resource, int $id)
    {
        $controller = $this->getController($resource);
        
        return $controller->cancelTop($id);
    }
}
