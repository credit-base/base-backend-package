<?php
namespace Base\Common\Controller;

use Marmot\Framework\Classes\Controller;

use Base\Common\Controller\Factory\ApproveControllerFactory;
use Base\Common\Controller\Interfaces\IApproveAbleController;

class ApproveController extends Controller
{
    protected function getApproveController(string $resource) : IApproveAbleController
    {
        return ApproveControllerFactory::getController($resource);
    }

    public function index(string $resource, $id, string $status)
    {
        $approveController = $this->getApproveController($resource);
        
        return $approveController->$status($id);
    }
}
