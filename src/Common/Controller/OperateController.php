<?php
namespace Base\Common\Controller;

use Marmot\Framework\Classes\Controller;

use Base\Common\Controller\Factory\OperateControllerFactory;
use Marmot\Framework\Common\Controller\IOperateController;

class OperateController extends Controller
{
    protected function getController(string $resource) : IOperateController
    {
        return OperateControllerFactory::getController($resource);
    }

    public function add(string $resource)
    {
        $controller = $this->getController($resource);
        return $controller->add();
    }
    
    public function edit(string $resource, int $id)
    {
        $controller = $this->getController($resource);
        return $controller->edit($id);
    }
}
