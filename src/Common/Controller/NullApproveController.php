<?php
namespace Base\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Common\Controller\Interfaces\IApproveAbleController;

class NullApproveController implements IApproveAbleController, INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function approve(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
    }

    public function reject(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
