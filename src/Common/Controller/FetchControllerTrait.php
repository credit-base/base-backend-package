<?php
namespace Base\Common\Controller;

use Marmot\Basecode\Classes\Request;
use Marmot\Framework\Common\Controller\FetchControllerTrait as BaseFetchControllerTrait;

trait FetchControllerTrait
{
    use BaseFetchControllerTrait;

    abstract protected function getRequest() : Request;

    public function filter()
    {
        $filter = $this->getRequest()->get('filter', array());
        $sort = $this->getRequest()->get('sort', '');
        $page = $this->getRequest()->get('page', array());
        $size = isset($page['size']) ? $page['size'] : 20;
        $number = isset($page['number']) ? $page['number'] : 1;

        list($objectList, $count) = $this->getRepository()->filter(
            $filter,
            [$sort],
            $number,
            $size
        );

        if ($count > 0) {
            $view = $this->generateView($objectList);
            $view->pagination(
                $this->getResourceName(),
                $this->getRequest()->get(),
                $count,
                $size,
                $number
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
