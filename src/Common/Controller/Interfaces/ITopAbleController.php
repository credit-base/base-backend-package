<?php
namespace Base\Common\Controller\Interfaces;

interface ITopAbleController
{
    public function top(int $id);
    public function cancelTop(int $id);
}
