<?php
namespace Base\Common\Controller\Interfaces;

interface IOperateController
{
    public function add();

    public function edit(int $id);
}
