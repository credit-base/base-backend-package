<?php
namespace Base\Common\Controller\Interfaces;

interface IResubmitAbleController
{
    public function resubmit(int $id);
}
