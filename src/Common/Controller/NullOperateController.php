<?php
namespace Base\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Controller\IOperateController;

class NullOperateController implements IOperateController, INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function add()
    {
        Core::setLastError(ROUTE_NOT_EXIST);
    }

    public function edit(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
