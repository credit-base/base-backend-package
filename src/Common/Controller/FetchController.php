<?php
namespace Base\Common\Controller;

use Marmot\Framework\Classes\Controller;

use Base\Common\Controller\Factory\FetchControllerFactory;
use Marmot\Framework\Common\Controller\IFetchController;

class FetchController extends Controller
{
    protected function getController(string $resource) : IFetchController
    {
        return FetchControllerFactory::getController($resource);
    }
    
    public function fetchOne(string $resource, int $id)
    {
        $controller = $this->getController($resource);
        return $controller->fetchOne($id);
    }
    
    public function fetchList(string $resource, string $ids)
    {
        $controller = $this->getController($resource);
        return $controller->fetchList($ids);
    }

    public function filter(string $resource)
    {
        $controller = $this->getController($resource);
        return $controller->filter();
    }
}
