<?php
namespace Base\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Common\Controller\Interfaces\ITopAbleController;

class NullTopController implements ITopAbleController, INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function top(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
    
    public function cancelTop(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
