<?php
namespace Base\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Common\Controller\Interfaces\IResubmitAbleController;

class NullResubmitController implements IResubmitAbleController, INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function resubmit(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
