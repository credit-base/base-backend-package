<?php
namespace Base\Common\Command;

use Marmot\Interfaces\ICommand;

abstract class CancelTopCommand implements ICommand
{
    public $id;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
    }
}
