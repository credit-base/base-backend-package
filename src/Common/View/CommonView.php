<?php
namespace Base\Common\View;

use Marmot\Interfaces\IView;

/**
 * @codeCoverageIgnore
 * @SuppressWarnings(PHPMD)
 */
abstract class CommonView implements IView
{
    use JsonApiView;

    private $rules;

    private $data;
    
    private $encodingParameters;

    public function __construct($data, $encodingParameters = null)
    {
        $this->data = $data;
        $this->encodingParameters = $encodingParameters;

        $this->rules = array(
            \Base\UserGroup\Model\UserGroup::class => \Base\UserGroup\View\UserGroupSchema::class,
            \Base\UserGroup\Model\NullUserGroup::class => \Base\UserGroup\View\UserGroupSchema::class,
            
            \Base\Department\Model\Department::class => \Base\Department\View\DepartmentSchema::class,
            \Base\Department\Model\NullDepartment::class => \Base\Department\View\DepartmentSchema::class,
            
            \Base\Crew\Model\Crew::class => \Base\Crew\View\CrewSchema::class,
            \Base\Crew\Model\NullCrew::class => \Base\Crew\View\CrewSchema::class,
            \Base\Crew\Model\Operator::class => \Base\Crew\View\CrewSchema::class,
            \Base\Crew\Model\PlatformAdministrator::class => \Base\Crew\View\CrewSchema::class,
            \Base\Crew\Model\SuperAdministrator::class => \Base\Crew\View\CrewSchema::class,
            \Base\Crew\Model\UserGroupAdministrator::class => \Base\Crew\View\CrewSchema::class,
            
            \Base\Template\Model\GbTemplate::class => \Base\Template\View\GbTemplateSchema::class,
            \Base\Template\Model\NullGbTemplate::class => \Base\Template\View\GbTemplateSchema::class,
            
            \Base\Template\Model\BjTemplate::class => \Base\Template\View\BjTemplateSchema::class,
            \Base\Template\Model\NullBjTemplate::class => \Base\Template\View\BjTemplateSchema::class,

            \Base\Template\Model\BaseTemplate::class => \Base\Template\View\BaseTemplateSchema::class,
            \Base\Template\Model\NullBaseTemplate::class => \Base\Template\View\BaseTemplateSchema::class,
            
            \Base\Template\Model\WbjTemplate::class => \Base\Template\View\WbjTemplateSchema::class,
            \Base\Template\Model\NullWbjTemplate::class => \Base\Template\View\WbjTemplateSchema::class,

            // \Base\Template\Model\Template::class => \Base\Template\View\WbjTemplateSchema::class,
            // \Base\Template\Model\NullTemplate::class => \Base\Template\View\WbjTemplateSchema::class,

            \Base\Template\Model\QzjTemplate::class => \Base\Template\View\QzjTemplateSchema::class,
            \Base\Template\Model\NullQzjTemplate::class => \Base\Template\View\QzjTemplateSchema::class,
            
            \Base\WorkOrderTask\Model\ParentTask::class => \Base\WorkOrderTask\View\ParentTaskSchema::class,
            \Base\WorkOrderTask\Model\NullParentTask::class => \Base\WorkOrderTask\View\ParentTaskSchema::class,
            \Base\WorkOrderTask\Model\WorkOrderTask::class => \Base\WorkOrderTask\View\WorkOrderTaskSchema::class,
            \Base\WorkOrderTask\Model\NullWorkOrderTask::class => \Base\WorkOrderTask\View\WorkOrderTaskSchema::class,

            \Base\ResourceCatalogData\Model\YsSearchData::class =>
            \Base\ResourceCatalogData\View\YsSearchDataSchema::class,
            \Base\ResourceCatalogData\Model\NullYsSearchData::class =>
            \Base\ResourceCatalogData\View\YsSearchDataSchema::class,

            \Base\ResourceCatalogData\Model\WbjSearchData::class =>
            \Base\ResourceCatalogData\View\WbjSearchDataSchema::class,
            \Base\ResourceCatalogData\Model\NullWbjSearchData::class =>
            \Base\ResourceCatalogData\View\WbjSearchDataSchema::class,

            \Base\ResourceCatalogData\Model\BjSearchData::class =>
            \Base\ResourceCatalogData\View\BjSearchDataSchema::class,
            \Base\ResourceCatalogData\Model\NullBjSearchData::class =>
            \Base\ResourceCatalogData\View\BjSearchDataSchema::class,

            \Base\ResourceCatalogData\Model\GbSearchData::class =>
            \Base\ResourceCatalogData\View\GbSearchDataSchema::class,
            \Base\ResourceCatalogData\Model\NullGbSearchData::class =>
            \Base\ResourceCatalogData\View\GbSearchDataSchema::class,

            \Base\ResourceCatalogData\Model\YsItemsData::class =>
            \Base\ResourceCatalogData\View\YsItemsDataSchema::class,
            \Base\ResourceCatalogData\Model\NullYsItemsData::class =>
            \Base\ResourceCatalogData\View\YsItemsDataSchema::class,

            \Base\ResourceCatalogData\Model\WbjItemsData::class =>
            \Base\ResourceCatalogData\View\WbjItemsDataSchema::class,
            \Base\ResourceCatalogData\Model\NullWbjItemsData::class =>
            \Base\ResourceCatalogData\View\WbjItemsDataSchema::class,

            \Base\ResourceCatalogData\Model\BjItemsData::class =>
            \Base\ResourceCatalogData\View\BjItemsDataSchema::class,
            \Base\ResourceCatalogData\Model\NullBjItemsData::class =>
            \Base\ResourceCatalogData\View\BjItemsDataSchema::class,

            \Base\ResourceCatalogData\Model\GbItemsData::class =>
            \Base\ResourceCatalogData\View\GbItemsDataSchema::class,
            \Base\ResourceCatalogData\Model\NullGbItemsData::class =>
            \Base\ResourceCatalogData\View\GbItemsDataSchema::class,

            \Base\News\Model\News::class =>
            \Base\News\View\NewsSchema::class,
            \Base\News\Model\NullNews::class =>
            \Base\News\View\NewsSchema::class,
            \Base\News\Model\UnAuditedNews::class =>
            \Base\News\View\UnAuditedNewsSchema::class,

            \Base\Member\Model\Member::class => \Base\Member\View\MemberSchema::class,
            \Base\Member\Model\NullMember::class => \Base\Member\View\MemberSchema::class,

            \Base\Journal\Model\Journal::class =>
            \Base\Journal\View\JournalSchema::class,
            \Base\Journal\Model\NullJournal::class =>
            \Base\Journal\View\JournalSchema::class,
            \Base\Journal\Model\UnAuditedJournal::class =>
            \Base\Journal\View\UnAuditedJournalSchema::class,

            \Base\CreditPhotography\Model\CreditPhotography::class =>
            \Base\CreditPhotography\View\CreditPhotographySchema::class,
            \Base\CreditPhotography\Model\NullCreditPhotography::class =>
            \Base\CreditPhotography\View\CreditPhotographySchema::class,

            \Base\Statistical\Model\Statistical::class =>
            \Base\Statistical\View\StatisticalSchema::class,
            \Base\Statistical\Model\NullStatistical::class =>
            \Base\Statistical\View\StatisticalSchema::class,

            \Base\Rule\Model\RuleService::class =>
            \Base\Rule\View\RuleServiceSchema::class,
            \Base\Rule\Model\NullRuleService::class =>
            \Base\Rule\View\RuleServiceSchema::class,
            \Base\Rule\Model\UnAuditedRuleService::class =>
            \Base\Rule\View\UnAuditedRuleServiceSchema::class,
            \Base\Rule\Model\NullUnAuditedRuleService::class =>
            \Base\Rule\View\UnAuditedRuleServiceSchema::class,

            \Base\ResourceCatalogData\Model\Task::class =>
            \Base\ResourceCatalogData\View\TaskSchema::class,
            \Base\ResourceCatalogData\Model\NullTask::class =>
            \Base\ResourceCatalogData\View\TaskSchema::class,

            \Base\ResourceCatalogData\Model\ErrorData::class =>
            \Base\ResourceCatalogData\View\ErrorDataSchema::class,
            \Base\ResourceCatalogData\Model\NullErrorData::class =>
            \Base\ResourceCatalogData\View\ErrorDataSchema::class,

            \Base\WebsiteCustomize\Model\WebsiteCustomize::class =>
            \Base\WebsiteCustomize\View\WebsiteCustomizeSchema::class,
            \Base\WebsiteCustomize\Model\NullWebsiteCustomize::class =>
            \Base\WebsiteCustomize\View\WebsiteCustomizeSchema::class,

            \Base\Enterprise\Model\Enterprise::class =>
            \Base\Enterprise\View\EnterpriseSchema::class,
            \Base\Enterprise\Model\NullEnterprise::class =>
            \Base\Enterprise\View\EnterpriseSchema::class,

            \Base\Interaction\Model\Complaint::class =>
            \Base\Interaction\View\Complaint\ComplaintSchema::class,
            \Base\Interaction\Model\NullComplaint::class =>
            \Base\Interaction\View\Complaint\ComplaintSchema::class,
            \Base\Interaction\Model\UnAuditedComplaint::class =>
            \Base\Interaction\View\Complaint\UnAuditedComplaintSchema::class,

            \Base\Interaction\Model\Praise::class =>
            \Base\Interaction\View\Praise\PraiseSchema::class,
            \Base\Interaction\Model\NullPraise::class =>
            \Base\Interaction\View\Praise\PraiseSchema::class,
            \Base\Interaction\Model\UnAuditedPraise::class =>
            \Base\Interaction\View\Praise\UnAuditedPraiseSchema::class,

            \Base\Interaction\Model\Appeal::class =>
            \Base\Interaction\View\Appeal\AppealSchema::class,
            \Base\Interaction\Model\NullAppeal::class =>
            \Base\Interaction\View\Appeal\AppealSchema::class,
            \Base\Interaction\Model\UnAuditedAppeal::class =>
            \Base\Interaction\View\Appeal\UnAuditedAppealSchema::class,

            \Base\Interaction\Model\Feedback::class =>
            \Base\Interaction\View\Feedback\FeedbackSchema::class,
            \Base\Interaction\Model\NullFeedback::class =>
            \Base\Interaction\View\Feedback\FeedbackSchema::class,
            \Base\Interaction\Model\UnAuditedFeedback::class =>
            \Base\Interaction\View\Feedback\UnAuditedFeedbackSchema::class,

            \Base\Interaction\Model\QA::class =>
            \Base\Interaction\View\QA\QASchema::class,
            \Base\Interaction\Model\NullQA::class =>
            \Base\Interaction\View\QA\QASchema::class,
            \Base\Interaction\Model\UnAuditedQA::class =>
            \Base\Interaction\View\QA\UnAuditedQASchema::class,

            \Base\Interaction\Model\Reply::class =>
            \Base\Interaction\View\Reply\ReplySchema::class,
            \Base\Interaction\Model\NullReply::class =>
            \Base\Interaction\View\Reply\ReplySchema::class,

            \Base\NotifyRecord\Model\NotifyRecord::class => \Base\NotifyRecord\View\NotifyRecordSchema::class,
            \Base\NotifyRecord\Model\NullNotifyRecord::class => \Base\NotifyRecord\View\NotifyRecordSchema::class,
        );
    }
    
    public function display()
    {
        return $this->jsonApiFormat($this->data, $this->rules, $this->encodingParameters);
    }
}
