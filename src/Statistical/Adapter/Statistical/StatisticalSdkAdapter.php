<?php
namespace Base\Statistical\Adapter\Statistical;

use Base\Common\Translator\ISdkTranslator;

use Base\Statistical\Translator\StatisticalSdkTranslator;
use Base\Statistical\Adapter\Statistical\IStatisticalAdapter;

use BaseSdk\Statistical\Adapter\Statistical\IStatisticalAdapter as IStatisticalAdapterSdk;

abstract class StatisticalSdkAdapter implements IStatisticalAdapter
{
    private $translator;

    public function __construct()
    {
        $this->translator = new StatisticalSdkTranslator();
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    abstract protected function getRestfulAdapter() : IStatisticalAdapterSdk;
}
