<?php
namespace Base\Statistical\Adapter\Statistical;

use Base\Statistical\Model\Statistical;

interface IStatisticalAdapter
{
    public function analyse(array $filter = array()) : Statistical;
}
