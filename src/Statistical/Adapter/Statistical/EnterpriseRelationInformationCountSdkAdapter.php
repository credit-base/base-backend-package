<?php
namespace Base\Statistical\Adapter\Statistical;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Base\Statistical\Model\Statistical;
use Base\Statistical\Model\NullStatistical;

use BaseSdk\Statistical\Adapter\Statistical\IStatisticalAdapter;
use BaseSdk\Statistical\Adapter\Statistical\EnterpriseRelationInformationCountRestfulAdapter;

class EnterpriseRelationInformationCountSdkAdapter extends StatisticalSdkAdapter
{
    protected function getRestfulAdapter() : IStatisticalAdapter
    {
        return new EnterpriseRelationInformationCountRestfulAdapter();
    }

    public function analyse(array $filter = array()) : Statistical
    {
        $statisticalSdk = $this->getRestfulAdapter()->analyse($filter);

        if ($statisticalSdk instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullStatistical::getInstance();
        }

        $statistical = $this->getTranslator()->valueObjectToObject($statisticalSdk);

        return $statistical;
    }
}
