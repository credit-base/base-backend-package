<?php
namespace Base\Statistical\Adapter\Statistical;

use Marmot\Interfaces\INull;

use Base\Statistical\Model\Statistical;
use Base\Statistical\Model\NullStatistical;

class NullStatisticalAdapter implements INull, IStatisticalAdapter
{
    public function analyse(array $filter = array()) : Statistical
    {
        return NullStatistical::getInstance();
    }
}
