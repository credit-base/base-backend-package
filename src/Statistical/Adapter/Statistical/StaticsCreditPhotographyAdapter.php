<?php
namespace Base\Statistical\Adapter\Statistical;

use Marmot\Core;

use Base\Statistical\Model\Statistical;
use Base\Statistical\Model\NullStatistical;
use Base\Statistical\Translator\StaticsCreditPhotographyTranslator;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;
use Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence\CreditPhotographyDb;

class StaticsCreditPhotographyAdapter implements IStatisticalAdapter
{
    private $staticsCreditPhotographyTranslator;

    private $creditPhotographyDb;

    private $creditPhotographyTranslator;

    public function __construct()
    {
        $this->staticsCreditPhotographyTranslator = new StaticsCreditPhotographyTranslator();
        $this->creditPhotographyDb = new CreditPhotographyDb();
        $this->creditPhotographyTranslator = new CreditPhotographyDbTranslator();
    }

    public function __destruct()
    {
        unset($this->staticsCreditPhotographyTranslator);
        unset($this->creditPhotographyDb);
        unset($this->creditPhotographyTranslator);
    }
    
    protected function getStaticsCreditPhotographyTranslator() : StaticsCreditPhotographyTranslator
    {
        return $this->staticsCreditPhotographyTranslator;
    }
    
    protected function getCreditPhotographyDb() : CreditPhotographyDb
    {
        return $this->creditPhotographyDb;
    }

    protected function getCreditPhotographyDbTranslator() : CreditPhotographyDbTranslator
    {
        return $this->creditPhotographyTranslator;
    }

    public function analyse(array $filter = array()) : Statistical
    {
        $info = array();
        $groupBy['applyStatus'] = '';
        $filter['status'] = CreditPhotography::STATUS['NORMAL'];

        $condition = $this->formatFilter($filter);
        list($conditionGroupBy, $groupByKey) = $this->formatGroupBy($groupBy);

        $condition .= $conditionGroupBy;

        $info = $this->getCreditPhotographyDb()->select($condition, 'count(*),'.$groupByKey);

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullStatistical::getInstance();
        }

        return $this->getStaticsCreditPhotographyTranslator()->arrayToObject($info);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $creditPhotography = new CreditPhotography();

            if (isset($filter['memberId'])) {
                $info = $this->getCreditPhotographyDbTranslator()->objectToArray(
                    $creditPhotography,
                    array('member')
                );
                $condition .= $conjection.key($info).' IN ('.$filter['memberId'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $info = $this->getCreditPhotographyDbTranslator()->objectToArray(
                    $creditPhotography,
                    array('status')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatGroupBy(array $groupBy) : array
    {
        $condition = $groupByKey = '';
        $conjection = ' GROUP BY ';

        if (isset($groupBy['applyStatus'])) {
            $info = $this->getCreditPhotographyDbTranslator()->objectToArray(
                new CreditPhotography(),
                array('applyStatus')
            );
            $condition .= $conjection.key($info);
            $groupByKey = key($info);
        }

        return [$condition, $groupByKey];
    }
}
