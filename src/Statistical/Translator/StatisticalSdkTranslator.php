<?php
namespace Base\Statistical\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Statistical\Model\Statistical;
use Base\Statistical\Model\NullStatistical;

use BaseSdk\Statistical\Model\Statistical as StatisticalSdk;
use BaseSdk\Statistical\Model\NullStatistical as NullStatisticalSdk;

class StatisticalSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;
    
    public function valueObjectToObject($statisticalSdk = null, $statistical = null)
    {
        return $this->translateToObject($statisticalSdk, $statistical);
    }

    protected function translateToObject($statisticalSdk = null, $statistical = null) : Statistical
    {
        if (!$statisticalSdk instanceof StatisticalSdk || $statisticalSdk instanceof INull) {
            return NullStatistical::getInstance();
        }

        if ($statistical == null) {
            $statistical = new Statistical();
        }

        $statistical->setId($statisticalSdk->getId());
        $statistical->setResult($statisticalSdk->getResult());
        
        return $statistical;
    }

    public function objectToValueObject($statistical)
    {
        unset($statistical);

        return NullStatisticalSdk::getInstance();
    }
}
