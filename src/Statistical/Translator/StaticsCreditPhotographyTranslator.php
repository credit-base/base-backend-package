<?php
namespace Base\Statistical\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Common\Model\IApproveAble;

use Base\Statistical\Model\Statistical;

class StaticsCreditPhotographyTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {
        if ($statistical == null) {
            $statistical = new Statistical();
        }

        $count = array();
        foreach ($expression as $key => $val) {
            unset($key);
            $count[$val['apply_status']] = $val['count(*)'];
        }

        $result = array(
            'pendingTotal' =>
                isset($count[IApproveAble::APPLY_STATUS['PENDING']]) ?
                $count[IApproveAble::APPLY_STATUS['PENDING']] :
                0,
            'approveTotal' =>
                isset($count[IApproveAble::APPLY_STATUS['APPROVE']]) ?
                $count[IApproveAble::APPLY_STATUS['APPROVE']] :
                0,
            'rejectTotal' =>
                isset($count[IApproveAble::APPLY_STATUS['REJECT']]) ? $count[IApproveAble::APPLY_STATUS['REJECT']] : 0,
        );
        
        $statistical->setResult($result);

        return $statistical;
    }

    public function objectToArray($statistical, array $keys = array())
    {
        unset($statistical);
        unset($keys);

        return array();
    }
}
