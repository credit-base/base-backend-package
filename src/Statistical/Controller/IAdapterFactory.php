<?php
namespace Base\Statistical\Controller;

use Base\Statistical\Adapter\Statistical\IStatisticalAdapter;
use Base\Statistical\Adapter\Statistical\NullStatisticalAdapter;

class IAdapterFactory
{
    const MAPS = array(
        'staticsCreditPhotography'=>
        'Base\Statistical\Adapter\Statistical\StaticsCreditPhotographyAdapter',
        'enterpriseRelationInformationCount'=>
        'Base\Statistical\Adapter\Statistical\EnterpriseRelationInformationCountSdkAdapter',
    );

    public function getAdapter(string $type) : IStatisticalAdapter
    {
        $adapter = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($adapter) ? new $adapter : new NullStatisticalAdapter();
    }
}
