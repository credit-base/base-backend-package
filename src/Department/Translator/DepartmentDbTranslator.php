<?php
namespace Base\Department\Translator;

use Marmot\Interfaces\ITranslator;

use Base\UserGroup\Model\UserGroup;

use Base\Department\Model\Department;
use Base\Department\Model\NullDepartment;

class DepartmentDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $department = null) : Department
    {
        if (!isset($expression['department_id'])) {
            return NullDepartment::getInstance();
        }

        if ($department == null) {
            $department = new Department($expression['department_id']);
        }
        $department->setName($expression['name']);
        $department->setUserGroup(new UserGroup($expression['user_group_id']));

        $department->setStatus($expression['status']);
        $department->setCreateTime($expression['create_time']);
        $department->setUpdateTime($expression['update_time']);
        $department->setStatusTime($expression['status_time']);

        return $department;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($department, array $keys = array()) : array
    {
        if (!$department instanceof Department) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'userGroup',
                'purview',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['department_id'] = $department->getId();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $department->getName();
        }
        if (in_array('userGroup', $keys)) {
            $expression['user_group_id'] = $department->getUserGroup()->getId();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $department->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $department->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $department->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $department->getStatusTime();
        }

        return $expression;
    }
}
