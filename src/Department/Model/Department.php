<?php
namespace Base\Department\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Common\Model\IOperate;
use Base\Common\Model\OperateTrait;

use Base\UserGroup\Model\UserGroup;

use Base\Department\Repository\DepartmentRepository;
use Base\Department\Adapter\Department\IDepartmentAdapter;

class Department implements IObject, IOperate
{
    use Object, OperateTrait;

    const STATUS_NORMAL = 0;

    /**
     * @var int $id
     */
    protected $id;
    /**
     * @var string $name 科室名称
     */
    protected $name;
    /**
     * @var UserGroup $userGroup 所属委办局
     */
    protected $userGroup;

    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->userGroup = new UserGroup();
        $this->status = STATUS_NORMAL;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new DepartmentRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->userGroup);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : IDepartmentAdapter
    {
        return $this->repository;
    }

    protected function addAction() : bool
    {
        if ($this->getUserGroup() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'userGroupId'));
            return false;
        }

        return $this->isNameExist() && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->isNameExist() && $this->getRepository()->edit(
            $this,
            array(
                'name',
                'updateTime'
            )
        );
    }
    
    protected function isNameExist() : bool
    {
        $filter = array();

        $filter['unique'] = $this->getName();
        $filter['userGroup'] = $this->getUserGroup()->getId();
        
        if (!empty($this->getId())) {
            $filter['id'] = $this->getId();
        }

        list($departmentList, $count) = $this->getRepository()->filter($filter);
        unset($departmentList);
        
        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'departmentName'));
            return false;
        }

        return true;
    }
}
