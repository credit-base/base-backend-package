<?php
namespace Base\Department\Command\Department;

use Base\Common\Command\AddCommand;

class AddDepartmentCommand extends AddCommand
{
    /**
     * @var string $name 科室名称
     */
    public $name;
    /**
     * @var int $userGroupId 所属委办局id
     */
    public $userGroupId;

    public $id;

    public function __construct(
        string $name,
        int $userGroupId,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        
        $this->name = $name;
        $this->userGroupId = $userGroupId;
    }
}
