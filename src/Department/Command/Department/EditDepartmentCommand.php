<?php
namespace Base\Department\Command\Department;

use Base\Common\Command\EditCommand;

class EditDepartmentCommand extends EditCommand
{
    /**
     * @var string $name 科室名称
     */
    public $name;

    public $id;

    public function __construct(
        string $name,
        int $id
    ) {
        parent::__construct(
            $id
        );
        
        $this->name = $name;
    }
}
