<?php
namespace Base\Department\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Department\Model\Department;
use Base\Department\Adapter\Department\IDepartmentAdapter;
use Base\Department\Adapter\Department\DepartmentDbAdapter;
use Base\Department\Adapter\Department\DepartmentMockAdapter;

class DepartmentRepository extends Repository implements IDepartmentAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new DepartmentDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IDepartmentAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IDepartmentAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IDepartmentAdapter
    {
        return new DepartmentMockAdapter();
    }

    public function fetchOne($id) : Department
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Department $department) : bool
    {
        return $this->getAdapter()->add($department);
    }

    public function edit(Department $department, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($department, $keys);
    }
}
