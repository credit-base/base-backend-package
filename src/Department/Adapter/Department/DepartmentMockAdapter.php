<?php
namespace Base\Department\Adapter\Department;

use Base\Department\Model\Department;
use Base\Department\Utils\MockFactory;

class DepartmentMockAdapter implements IDepartmentAdapter
{
    public function fetchOne($id) : Department
    {
        return MockFactory::generateDepartment($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $departmentList = array();

        foreach ($ids as $id) {
            $departmentList[$id] = MockFactory::generateDepartment($id);
        }

        return $departmentList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Department $department) : bool
    {
        unset($department);
        return true;
    }

    public function edit(Department $department, array $keys = array()) : bool
    {
        unset($department);
        unset($keys);
        return true;
    }
}
