<?php
namespace Base\Department\Adapter\Department;

use Base\Department\Model\Department;

interface IDepartmentAdapter
{
    public function fetchOne($id) : Department;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Department $department) : bool;

    public function edit(Department $department, array $keys = array()) : bool;
}
