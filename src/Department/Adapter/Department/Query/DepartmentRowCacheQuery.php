<?php
namespace Base\Department\Adapter\Department\Query;

use Marmot\Framework\Query\RowCacheQuery;

class DepartmentRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'department_id',
            new Persistence\DepartmentCache(),
            new Persistence\DepartmentDb()
        );
    }
}
