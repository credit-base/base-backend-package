<?php
namespace Base\Department\Adapter\Department\Query\Persistence;

use Marmot\Framework\Classes\Db;

class DepartmentDb extends Db
{
    const TABLE = 'department';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
