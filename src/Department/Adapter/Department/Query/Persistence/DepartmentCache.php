<?php
namespace Base\Department\Adapter\Department\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class DepartmentCache extends Cache
{
    const KEY = 'department';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
