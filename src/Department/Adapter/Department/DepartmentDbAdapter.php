<?php
namespace Base\Department\Adapter\Department;

use Marmot\Interfaces\INull;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Department\Model\Department;
use Base\Department\Model\NullDepartment;
use Base\Department\Translator\DepartmentDbTranslator;
use Base\Department\Adapter\Department\Query\DepartmentRowCacheQuery;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;
use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

class DepartmentDbAdapter implements IDepartmentAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $userGroupRepository;

    public function __construct()
    {
        $this->dbTranslator = new DepartmentDbTranslator();
        $this->rowCacheQuery = new DepartmentRowCacheQuery();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->userGroupRepository);
    }
    
    protected function getDbTranslator() : DepartmentDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : DepartmentRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getUserGroupRepository() : IUserGroupAdapter
    {
        return $this->userGroupRepository;
    }

    protected function getNullObject() : INull
    {
        return NullDepartment::getInstance();
    }
    
    public function add(Department $department) : bool
    {
        return $this->addAction($department);
    }

    public function edit(Department $department, array $keys = array()) : bool
    {
        return $this->editAction($department, $keys);
    }

    public function fetchOne($id) : Department
    {
        $department = $this->fetchOneAction($id);
        
        $this->fetchUserGroup($department);

        return $department;
    }

    public function fetchList(array $ids) : array
    {
        $departmentList = array();
        $departmentList = $this->fetchListAction($ids);

        $this->fetchUserGroup($departmentList);

        return $departmentList;
    }

    protected function fetchUserGroup($department)
    {
        return is_array($department) ?
        $this->fetchUserGroupByList($department) :
        $this->fetchUserGroupByObject($department);
    }

    protected function fetchUserGroupByObject($department)
    {
        if (!$department instanceof INull) {
            $userGroupId = $department->getUserGroup()->getId();
            $userGroup = $this->getUserGroupRepository()->fetchOne($userGroupId);
            $department->setUserGroup($userGroup);
        }
    }

    protected function fetchUserGroupByList(array $departmentList)
    {
        if (!empty($departmentList)) {
            $userGroupIds = array();
            foreach ($departmentList as $department) {
                $userGroupIds[] = $department->getUserGroup()->getId();
            }

            $userGroupList = $this->getUserGroupRepository()->fetchList($userGroupIds);
            if (!empty($userGroupList)) {
                foreach ($departmentList as $department) {
                    if (isset($userGroupList[$department->getUserGroup()->getId()])) {
                        $department->setUserGroup($userGroupList[$department->getUserGroup()->getId()]);
                    }
                }
            }
        }
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $department = new Department();

            if (isset($filter['name'])) {
                $department->setName($filter['name']);
                $info = $this->getDbTranslator()->objectToArray($department, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['userGroup'])) {
                $department->setUserGroup(new UserGroup($filter['userGroup']));
                $info = $this->getDbTranslator()->objectToArray($department, array('userGroup'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['id'])) {
                $department->setId($filter['id']);
                $info = $this->getDbTranslator()->objectToArray($department, array('id'));
                $condition .= $conjection.key($info).' <> '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['unique'])) {
                $department->setName($filter['unique']);
                $info = $this->getDbTranslator()->objectToArray($department, array('name'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new Department(), array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}
