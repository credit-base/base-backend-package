<?php
namespace Base\Department\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class DepartmentSchema extends SchemaProvider
{
    protected $resourceType = 'departments';

    public function getId($department) : int
    {
        return $department->getId();
    }

    public function getAttributes($department) : array
    {
        return [
            'name' => $department->getName(),

            'status' => $department->getStatus(),
            'createTime' => $department->getCreateTime(),
            'updateTime' => $department->getUpdateTime(),
            'statusTime' => $department->getStatusTime(),
        ];
    }

    public function getRelationships($department, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'userGroup' => [self::DATA => $department->getUserGroup()]
        ];
    }
}
