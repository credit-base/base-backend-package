<?php
namespace Base\Department\CommandHandler\Department;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class DepartmentCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Department\Command\Department\AddDepartmentCommand' =>
        'Base\Department\CommandHandler\Department\AddDepartmentCommandHandler',
        'Base\Department\Command\Department\EditDepartmentCommand' =>
        'Base\Department\CommandHandler\Department\EditDepartmentCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
