<?php
namespace Base\Department\CommandHandler\Department;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Department\Model\Department;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Command\Department\EditDepartmentCommand;

class EditDepartmentCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new DepartmentRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getDepartmentRepository() : DepartmentRepository
    {
        return $this->repository;
    }

    protected function fetchDepartment(int $id) : Department
    {
        return $this->getDepartmentRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditDepartmentCommand)) {
            throw new \InvalidArgumentException;
        }

        $department = $this->fetchDepartment($command->id);
        $department->setName($command->name);

        return $department->edit();
    }
}
