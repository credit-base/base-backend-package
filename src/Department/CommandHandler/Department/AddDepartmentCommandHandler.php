<?php
namespace Base\Department\CommandHandler\Department;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Department\Model\Department;
use Base\Department\Command\Department\AddDepartmentCommand;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

class AddDepartmentCommandHandler implements ICommandHandler
{
    private $department;

    private $repository;

    public function __construct()
    {
        $this->department = new Department();
        $this->repository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->department);
        unset($this->repository);
    }

    protected function getDepartment() : Department
    {
        return $this->department;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->repository;
    }

    protected function fetchUserGroup(int $id) : UserGroup
    {
        return $this->getUserGroupRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddDepartmentCommand)) {
            throw new \InvalidArgumentException;
        }

        $userGroup = $this->fetchUserGroup($command->userGroupId);

        $department = $this->getDepartment();
        $department->setName($command->name);
        $department->setUserGroup($userGroup);

        if ($department->add()) {
            $command->id = $department->getId();
            return true;
        }
        return false;
    }
}
