<?php
namespace Base\Department\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\Department\Model\Department;
use Base\Department\View\DepartmentView;

use Base\Department\Command\Department\AddDepartmentCommand;
use Base\Department\Command\Department\EditDepartmentCommand;

class DepartmentOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, DepartmentControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 科室新增功能,通过POST传参
     * 对应路由 /departments
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $name = $attributes['name'];
        $userGroupId = $relationships['userGroup']['data'][0]['id'];

        //验证
        if ($this->validateAddScenario(
            $name,
            $userGroupId
        )) {
            //初始化命令
            $command = new AddDepartmentCommand(
                $name,
                $userGroupId
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $department = $this->getRepository()->fetchOne($command->id);
                if ($department instanceof Department) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new DepartmentView($department));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateAddScenario(
        $name,
        $userGroupId
    ) : bool {
        return $this->getCommonWidgetRule()->name($name)
        && $this->getCommonWidgetRule()->formatNumeric($userGroupId, 'userGroupId');
    }

    /**
     * 科室编辑功能,通过PATCH传参
     * 对应路由 /departments/{id:\d+}
     * @param int id 科室 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];

        $name = $attributes['name'];

        if ($this->validateEditScenario($name)) {
            $command = new EditDepartmentCommand(
                $name,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $organization = $this->getRepository()->fetchOne($id);
                if ($organization instanceof Department) {
                    $this->render(new DepartmentView($organization));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateEditScenario(
        $name
    ) : bool {
        return $this->getCommonWidgetRule()->name($name);
    }
}
