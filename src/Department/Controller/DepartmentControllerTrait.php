<?php
namespace Base\Department\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Department\Repository\DepartmentRepository;
use Base\Department\CommandHandler\Department\DepartmentCommandHandlerFactory;

trait DepartmentControllerTrait
{
    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : DepartmentRepository
    {
        return new DepartmentRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new DepartmentCommandHandlerFactory());
    }
}
