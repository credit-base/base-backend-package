<?php
namespace Base\Department\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Department\View\DepartmentView;
use Base\Department\Repository\DepartmentRepository;
use Base\Department\Adapter\Department\IDepartmentAdapter;

class DepartmentFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new DepartmentRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IDepartmentAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new DepartmentView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'departments';
    }
}
