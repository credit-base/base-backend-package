<?php
namespace Base\Member\Adapter\Member;

use Base\Member\Model\Member;
use Base\Member\Utils\MockFactory;

class MemberMockAdapter implements IMemberAdapter
{
    public function fetchOne($id) : Member
    {
        return MockFactory::generateMember($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $memberList = array();

        foreach ($ids as $id) {
            $memberList[$id] = MockFactory::generateMember($id);
        }

        return $memberList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Member $member) : bool
    {
        unset($member);
        return true;
    }

    public function edit(Member $member, array $keys = array()) : bool
    {
        unset($member);
        unset($keys);
        return true;
    }
}
