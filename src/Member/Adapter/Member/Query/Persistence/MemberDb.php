<?php
namespace Base\Member\Adapter\Member\Query\Persistence;

use Marmot\Framework\Classes\Db;

class MemberDb extends Db
{
    const TABLE = 'member';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
