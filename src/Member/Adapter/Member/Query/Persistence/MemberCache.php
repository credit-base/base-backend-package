<?php
namespace Base\Member\Adapter\Member\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class MemberCache extends Cache
{
    const KEY = 'member';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
