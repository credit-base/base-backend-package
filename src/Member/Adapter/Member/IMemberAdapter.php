<?php
namespace Base\Member\Adapter\Member;

use Base\Member\Model\Member;

interface IMemberAdapter
{
    public function fetchOne($id) : Member;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Member $member) : bool;

    public function edit(Member $member, array $keys = array()) : bool;
}
