<?php
namespace Base\Member\Adapter\Member;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Member\Model\Member;
use Base\Member\Model\NullMember;
use Base\Member\Translator\MemberDbTranslator;
use Base\Member\Adapter\Member\Query\MemberRowCacheQuery;

class MemberDbAdapter implements IMemberAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new MemberDbTranslator();
        $this->rowCacheQuery = new MemberRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : MemberDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : MemberRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullMember::getInstance();
    }

    public function add(Member $member) : bool
    {
        return $this->addAction($member);
    }

    public function edit(Member $member, array $keys = array()) : bool
    {
        return $this->editAction($member, $keys);
    }

    public function fetchOne($id) : Member
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $member = new Member();

            if (isset($filter['cellphone'])) {
                $member->setCellphone($filter['cellphone']);
                $info = $this->getDbTranslator()->objectToArray($member, array('cellphone'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['email'])) {
                $member->setEmail($filter['email']);
                $info = $this->getDbTranslator()->objectToArray($member, array('email'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['userName'])) {
                $member->setUserName($filter['userName']);
                $info = $this->getDbTranslator()->objectToArray($member, array('userName'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['realName'])) {
                $member->setRealName($filter['realName']);
                $info = $this->getDbTranslator()->objectToArray($member, array('realName'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $member->setStatus($filter['status']);
                $info = $this->getDbTranslator()->objectToArray($member, array('status'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $member = new Member();

            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($member, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['status'])) {
                $info = $this->getDbTranslator()->objectToArray($member, array('status'));
                $condition .= $conjection.key($info).' '.($sort['status'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}
