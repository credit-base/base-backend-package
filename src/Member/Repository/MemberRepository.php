<?php
namespace Base\Member\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Member\Model\Member;
use Base\Member\Adapter\Member\IMemberAdapter;
use Base\Member\Adapter\Member\MemberDbAdapter;
use Base\Member\Adapter\Member\MemberMockAdapter;

class MemberRepository extends Repository implements IMemberAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new MemberDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IMemberAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IMemberAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IMemberAdapter
    {
        return new MemberMockAdapter();
    }

    public function fetchOne($id) : Member
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Member $member) : bool
    {
        return $this->getAdapter()->add($member);
    }

    public function edit(Member $member, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($member, $keys);
    }
}
