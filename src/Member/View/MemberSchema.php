<?php
namespace Base\Member\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class MemberSchema extends SchemaProvider
{
    protected $resourceType = 'members';

    public function getId($member) : int
    {
        return $member->getId();
    }

    public function getAttributes($member) : array
    {
        return [
            'userName' => $member->getUserName(),
            'realName' => $member->getRealName(),
            'cellphone' => $member->getCellphone(),
            'email' => $member->getEmail(),
            'cardId' => $member->getCardId(),
            'contactAddress' => $member->getContactAddress(),
            'securityQuestion' => $member->getSecurityQuestion(),
            'gender' => $member->getGender(),
            'status' => $member->getStatus(),
            'createTime' => $member->getCreateTime(),
            'updateTime' => $member->getUpdateTime(),
            'statusTime' => $member->getStatusTime(),
        ];
    }
}
