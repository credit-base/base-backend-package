<?php
namespace Base\Member\Command\Member;

use Marmot\Interfaces\ICommand;

class SignInMemberCommand implements ICommand
{
    /**
     * @var string $userName 用户名
     */
    public $userName;
    /**
     * @var string $password 密码
     */
    public $password;
    /**
     * @var int $id id
     */
    public $id;

    public function __construct(
        string $userName,
        string $password,
        int $id = 0
    ) {
        $this->userName = $userName;
        $this->password = $password;
        $this->id = $id;
    }
}
