<?php
namespace Base\Member\Command\Member;

use Marmot\Interfaces\ICommand;

class ResetPasswordMemberCommand implements ICommand
{
    public $userName;
    
    public $securityAnswer;
    
    public $password;
    
    public $id;
    
    public function __construct(
        string $userName,
        string $securityAnswer,
        string $password,
        int $id = 0
    ) {
        $this->userName = $userName;
        $this->securityAnswer = $securityAnswer;
        $this->password = $password;
        $this->id = $id;
    }
}
