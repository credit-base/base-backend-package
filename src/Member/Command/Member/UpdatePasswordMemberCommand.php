<?php
namespace Base\Member\Command\Member;

use Marmot\Interfaces\ICommand;

class UpdatePasswordMemberCommand implements ICommand
{
    public $password;
    
    public $oldPassword;
    
    public $id;
    
    public function __construct(
        string $password,
        string $oldPassword,
        int $id
    ) {
        $this->password = $password;
        $this->oldPassword = $oldPassword;
        $this->id = $id;
    }
}
