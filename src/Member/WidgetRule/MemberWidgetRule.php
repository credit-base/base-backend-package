<?php
namespace Base\Member\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Base\Member\Model\Member;

class MemberWidgetRule
{
    const USER_NAME_MIN_LENGTH = 2;
    const USER_NAME_MAX_LENGTH = 20;

    public function userName($userName) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::USER_NAME_MIN_LENGTH,
            self::USER_NAME_MAX_LENGTH
        )->validate($userName)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'userName'));
            return false;
        }

        return true;
    }

    public function email($email) : bool
    {
        if (!V::email()->validate($email)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'email'));
            return false;
        }

        return true;
    }

    const CONTACT_ADDRESS_MIN_LENGTH = 1;
    const CONTACT_ADDRESS_MAX_LENGTH = 255;

    public function contactAddress($contactAddress) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::CONTACT_ADDRESS_MIN_LENGTH,
            self::CONTACT_ADDRESS_MAX_LENGTH
        )->validate($contactAddress)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'contactAddress'));
            return false;
        }

        return true;
    }

    const SECURITY_ANSWER_MIN_LENGTH = 1;
    const SECURITY_ANSWER_MAX_LENGTH = 30;

    public function securityAnswer($securityAnswer) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::SECURITY_ANSWER_MIN_LENGTH,
            self::SECURITY_ANSWER_MAX_LENGTH
        )->validate($securityAnswer)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'securityAnswer'));
            return false;
        }

        return true;
    }

    public function securityQuestion($securityQuestion) : bool
    {
        if (!V::numeric()->positive()->validate($securityQuestion)
        || !in_array($securityQuestion, Member::SECURITY_QUESTION)
        ) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'securityQuestion'));
            return false;
        }

        return true;
    }

    public function gender($gender) : bool
    {
        if (!V::numeric()->positive()->validate($gender)
        || !in_array($gender, Member::GENDER)
        ) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'gender'));
            return false;
        }

        return true;
    }
}
