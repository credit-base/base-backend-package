<?php
namespace Base\Member\CommandHandler\Member;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;

trait MemberCommandHandlerTrait
{
    private $repository;

    public function __construct()
    {
        $this->repository = new MemberRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->repository;
    }

    protected function fetchMember(int $id) : Member
    {
        return $this->getMemberRepository()->fetchOne($id);
    }
}
