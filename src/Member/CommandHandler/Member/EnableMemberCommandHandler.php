<?php
namespace Base\Member\CommandHandler\Member;

use Base\Common\Model\IEnableAble;
use Base\Common\CommandHandler\EnableCommandHandler;

class EnableMemberCommandHandler extends EnableCommandHandler
{
    use MemberCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchMember($id);
    }
}
