<?php
namespace Base\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Member\Model\Member;
use Base\Member\Command\Member\AddMemberCommand;

class AddMemberCommandHandler implements ICommandHandler
{
    protected function getMember() : Member
    {
        return new Member();
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->getMember();
        $member->setUserName($command->userName);
        $member->setRealName($command->realName);
        $member->setCellphone($command->cellphone);
        $member->setEmail($command->email);
        $member->setCardId($command->cardId);
        $member->setContactAddress($command->contactAddress);
        $member->setSecurityQuestion($command->securityQuestion);
        $member->encryptSecurityAnswer($command->securityAnswer);
        $member->encryptPassword($command->password);
        
        if ($member->add()) {
            $command->id = $member->getId();
            return true;
        }
        return false;
    }
}
