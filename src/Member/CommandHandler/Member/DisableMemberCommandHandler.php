<?php
namespace Base\Member\CommandHandler\Member;

use Base\Common\Model\IEnableAble;
use Base\Common\CommandHandler\DisableCommandHandler;

class DisableMemberCommandHandler extends DisableCommandHandler
{
    use MemberCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchMember($id);
    }
}
