<?php
namespace Base\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Member\Command\Member\UpdatePasswordMemberCommand;

class UpdatePasswordMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof UpdatePasswordMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->fetchMember($command->id);

        return $member->changePassword($command->oldPassword, $command->password);
    }
}
