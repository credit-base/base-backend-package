<?php
namespace Base\Member\CommandHandler\Member;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Member\Command\Member\ResetPasswordMemberCommand;

class ResetPasswordMemberCommandHandler implements ICommandHandler
{
    const REST_PASSWORD_SEARCH_COUNT = 1;

    use MemberCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResetPasswordMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        list($members, $count) = $this->searchMembers($command->userName);

        if ($this->isMemberExist($count)) {
            foreach ($members as $member) {
                $member = $member;
            }

            if (!$member->validateSecurity($command->securityAnswer)) {
                Core::setLastError(PARAMETER_ERROR, array('pointer' => 'securityAnswer'));
                return false;
            }

            if ($member->resetPassword($command->password)) {
                $command->id = $member->getId();
                return true;
            }
        }

        return false;
    }

    protected function searchMembers($userName) : array
    {
        $filter = array();
        $filter['userName'] = $userName;

        list($members, $count) = $this->getMemberRepository()->filter($filter);

        return array($members, $count);
    }

    protected function isMemberExist(int $count) : bool
    {
        if ($count != self::REST_PASSWORD_SEARCH_COUNT) {
            Core::setLastError(RESOURCE_NOT_EXIST, array('pointer' => 'userName'));
            return false;
        }

        return true;
    }
}
