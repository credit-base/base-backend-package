<?php
namespace Base\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Member\Command\Member\ValidateSecurityMemberCommand;

class ValidateSecurityMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ValidateSecurityMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->fetchMember($command->id);

        return $member->validateSecurity($command->securityAnswer);
    }
}
