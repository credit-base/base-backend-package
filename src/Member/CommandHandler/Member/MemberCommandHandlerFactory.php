<?php
namespace Base\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class MemberCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Member\Command\Member\AddMemberCommand' =>
        'Base\Member\CommandHandler\Member\AddMemberCommandHandler',
        'Base\Member\Command\Member\EditMemberCommand' =>
        'Base\Member\CommandHandler\Member\EditMemberCommandHandler',
        'Base\Member\Command\Member\EnableMemberCommand' =>
        'Base\Member\CommandHandler\Member\EnableMemberCommandHandler',
        'Base\Member\Command\Member\DisableMemberCommand' =>
        'Base\Member\CommandHandler\Member\DisableMemberCommandHandler',
        'Base\Member\Command\Member\SignInMemberCommand' =>
        'Base\Member\CommandHandler\Member\SignInMemberCommandHandler',
        'Base\Member\Command\Member\ResetPasswordMemberCommand' =>
        'Base\Member\CommandHandler\Member\ResetPasswordMemberCommandHandler',
        'Base\Member\Command\Member\UpdatePasswordMemberCommand' =>
        'Base\Member\CommandHandler\Member\UpdatePasswordMemberCommandHandler',
        'Base\Member\Command\Member\ValidateSecurityMemberCommand' =>
        'Base\Member\CommandHandler\Member\ValidateSecurityMemberCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
