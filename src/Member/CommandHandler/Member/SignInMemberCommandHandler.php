<?php
namespace Base\Member\CommandHandler\Member;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Member\Model\Member;
use Base\Member\Command\Member\SignInMemberCommand;

class SignInMemberCommandHandler implements ICommandHandler
{
    const SIGN_IN_SEARCH_COUNT = 1;

    use MemberCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof SignInMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        list($members, $count) = $this->searchMembers($command->userName);

        if ($this->isMemberExist($count)) {
            foreach ($members as $member) {
                $member = $member;
            }

            if ($this->isMemberDisabled($member) && $this->memberPasswordCorrect($member, $command)) {
                $command->id = $member->getId();
                return true;
            }
        }

        return false;
    }

    protected function searchMembers($userName) : array
    {
        $filter = array();
        $filter['userName'] = $userName;

        list($members, $count) = $this->getMemberRepository()->filter($filter);

        return array($members, $count);
    }

    protected function isMemberExist(int $count) : bool
    {
        if ($count != self::SIGN_IN_SEARCH_COUNT) {
            Core::setLastError(RESOURCE_NOT_EXIST, array('pointer' => 'userName'));
            return false;
        }

        return true;
    }

    protected function isMemberDisabled(Member $member) : bool
    {
        if ($member->isDisabled()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'signInStatus'));
            return false;
        }

        return true;
    }

    protected function memberPasswordCorrect(Member $member, SignInMemberCommand $command) : bool
    {
        if (!empty($member->getPassword())) {
            $password = $member->getPassword();
            $member->encryptPassword(
                $command->password,
                $member->getSalt()
            );

            if ($password == $member->getPassword()) {
                return true;
            }
        }
        
        Core::setLastError(PASSWORD_INCORRECT, array('pointer' => 'password'));
        return false;
    }
}
