<?php
namespace Base\Member\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;
use Base\Common\Model\NullEnableAbleTrait;

class NullMember extends Member implements INull
{
    use NullOperateTrait, NullEnableAbleTrait;
    
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
