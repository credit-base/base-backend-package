<?php
namespace Base\Member\Model;

use Marmot\Core;

use Base\Common\Model\IOperate;
use Base\Common\Model\IEnableAble;
use Base\Common\Model\OperateTrait;

use Base\Member\Repository\MemberRepository;
use Base\Member\Adapter\Member\IMemberAdapter;

use Base\User\Model\User;

class Member extends User implements IOperate
{
    use OperateTrait;

    /**
     * @var SECURITY_QUESTION['NULL']  未知 0
     * @var SECURITY_QUESTION['QUESTION_ONE']  你父亲或母亲的姓名 1
     * @var SECURITY_QUESTION['QUESTION_TWO']  你喜欢看的电影 2
     * @var SECURITY_QUESTION['QUESTION_THREE']  你最好朋友的名字 3
     * @var SECURITY_QUESTION['QUESTION_FOUR']  你毕业于哪个高中 4
     * @var SECURITY_QUESTION['QUESTION_FIVE']  你最喜欢的颜色 5
     */
    const SECURITY_QUESTION = array(
        'NULL' => 0,
        'QUESTION_ONE' => 1,
        'QUESTION_TWO' => 2,
        'QUESTION_THREE' => 3,
        'QUESTION_FOUR' => 4,
        'QUESTION_FIVE' => 5
    );
    
    /**
     * @var GENDER['NULL']  未知 0
     * @var GENDER['MALE']  男 1
     * @var GENDER['FEMALE']  女 2
     */
    const GENDER = array(
        'NULL' => 0, //未知
        'MALE' => 1, //男
        'FEMALE' => 2 //女
    );

    /**
     * @var string $realName 姓名
     */
    protected $realName;
    /**
     * @var string $email 邮箱
     */
    protected $email;
    /**
     * @var string $cardId 邮箱
     */
    protected $cardId;
    /**
     * @var string $contactAddress 联系地址
     */
    protected $contactAddress;
    /**
     * @var int $securityQuestion 密保问题
     */
    protected $securityQuestion;
    /**
     * @var string $securityAnswer 密保答案
     */
    protected $securityAnswer;
    /**
     * @var int $gender 性别
     */
    protected $gender;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->realName = '';
        $this->email = '';
        $this->cardId = '';
        $this->contactAddress = '';
        $this->securityQuestion = self::SECURITY_QUESTION['NULL'];
        $this->securityAnswer = '';
        $this->gender = self::GENDER['NULL'];
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new MemberRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->realName);
        unset($this->email);
        unset($this->cardId);
        unset($this->contactAddress);
        unset($this->securityQuestion);
        unset($this->securityAnswer);
        unset($this->gender);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setRealName(string $realName) : void
    {
        $this->realName = $realName;
    }

    public function getRealName() : string
    {
        return $this->realName;
    }

    public function setEmail(string $email) : void
    {
        $this->email = $email;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function setCardId(string $cardId) : void
    {
        $this->cardId = $cardId;
    }

    public function getCardId() : string
    {
        return $this->cardId;
    }

    public function setContactAddress(string $contactAddress) : void
    {
        $this->contactAddress = $contactAddress;
    }

    public function getContactAddress() : string
    {
        return $this->contactAddress;
    }

    public function setSecurityQuestion(int $securityQuestion) : void
    {
        $this->securityQuestion = in_array(
            $securityQuestion,
            self::SECURITY_QUESTION
        ) ? $securityQuestion : self::SECURITY_QUESTION['NULL'];
    }

    public function getSecurityQuestion() : int
    {
        return $this->securityQuestion;
    }

    public function encryptSecurityAnswer(string $securityAnswer) : void
    {
        $this->securityAnswer = md5(md5($securityAnswer));
    }

    public function setSecurityAnswer(string $securityAnswer) : void
    {
        $this->securityAnswer = $securityAnswer;
    }

    public function getSecurityAnswer() : string
    {
        return $this->securityAnswer;
    }

    public function setGender(int $gender) : void
    {
        $this->gender = in_array($gender, self::GENDER) ? $gender : self::GENDER['NULL'];
    }

    public function getGender() : int
    {
        return $this->gender;
    }

    protected function getRepository() : IMemberAdapter
    {
        return $this->repository;
    }
    
    protected function isCellphoneExist() : bool
    {
        $filter = array();

        $filter['cellphone'] = $this->getCellphone();

        list($memberList, $count) = $this->getRepository()->filter($filter);
        unset($memberList);
        
        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'cellphone'));
            return false;
        }

        return true;
    }

    protected function isEmailExist() : bool
    {
        $filter = array();

        $filter['email'] = $this->getEmail();

        list($memberList, $count) = $this->getRepository()->filter($filter);
        unset($memberList);
        
        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'email'));
            return false;
        }

        return true;
    }

    protected function isUserNameExist() : bool
    {
        $filter = array();

        $filter['userName'] = $this->getUserName();

        list($memberList, $count) = $this->getRepository()->filter($filter);
        unset($memberList);
        
        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'userName'));
            return false;
        }

        return true;
    }

    protected function addAction() : bool
    {
        return $this->isUserNameExist()
            && $this->isCellphoneExist()
            && $this->isEmailExist()
            && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'gender',
                'updateTime'
            )
        );
    }

    protected function updatePassword(string $newPassword) : bool
    {
        $this->encryptPassword($newPassword);
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit($this, array(
                    'updateTime',
                    'password',
                    'salt',
                ));
    }

    protected function verifyPassword(string $oldPassword) : bool
    {
        //检查旧密码是否正确
        $oldEncryptedPassword = $this->getPassword();
        $this->encryptPassword($oldPassword, $this->getSalt());
        if ($oldEncryptedPassword != $this->getPassword()) {
            Core::setLastError(PASSWORD_INCORRECT, array('pointer'=>'oldPassword'));
            return false;
        }

        return true;
    }

    public function validateSecurity(string $securityAnswer) : bool
    {
        //检查密码答案是否正确
        $oldEncryptedSecurityAnswer = $this->getSecurityAnswer();
        $this->encryptSecurityAnswer($securityAnswer);
        if ($oldEncryptedSecurityAnswer != $this->getSecurityAnswer()) {
            Core::setLastError(PARAMETER_ERROR, array('pointer'=>'securityAnswer'));
            return false;
        }

        return true;
    }
}
