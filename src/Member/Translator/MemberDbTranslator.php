<?php
namespace Base\Member\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Member\Model\Member;
use Base\Member\Model\NullMember;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class MemberDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $member = null) : Member
    {
        if (!isset($expression['member_id'])) {
            return NullMember::getInstance();
        }

        if ($member == null) {
            $member = new Member($expression['member_id']);
        }
        if (isset($expression['user_name'])) {
            $member->setUserName($expression['user_name']);
        }
        if (isset($expression['real_name'])) {
            $member->setRealName($expression['real_name']);
        }
        if (isset($expression['cellphone'])) {
            $member->setCellphone($expression['cellphone']);
        }
        if (isset($expression['email'])) {
            $member->setEmail($expression['email']);
        }
        if (isset($expression['cardid'])) {
            $member->setCardId($expression['cardid']);
        }
        if (isset($expression['contact_address'])) {
            $member->setContactAddress($expression['contact_address']);
        }
        if (isset($expression['security_question'])) {
            $member->setSecurityQuestion($expression['security_question']);
        }
        if (isset($expression['security_answer'])) {
            $member->setSecurityAnswer($expression['security_answer']);
        }
        if (isset($expression['gender'])) {
            $member->setGender($expression['gender']);
        }
        if (isset($expression['password'])) {
            $member->setPassword($expression['password']);
        }
        if (isset($expression['salt'])) {
            $member->setSalt($expression['salt']);
        }
        $member->setStatus($expression['status']);
        $member->setCreateTime($expression['create_time']);
        $member->setUpdateTime($expression['update_time']);
        $member->setStatusTime($expression['status_time']);

        return $member;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($member, array $keys = array()) : array
    {
        if (!$member instanceof Member) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'userName',
                'realName',
                'cellphone',
                'email',
                'cardId',
                'contactAddress',
                'securityQuestion',
                'securityAnswer',
                'gender',
                'password',
                'salt',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['member_id'] = $member->getId();
        }
        if (in_array('userName', $keys)) {
            $expression['user_name'] = $member->getUserName();
        }
        if (in_array('realName', $keys)) {
            $expression['real_name'] = $member->getRealName();
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $member->getCellphone();
        }
        if (in_array('email', $keys)) {
            $expression['email'] = $member->getEmail();
        }
        if (in_array('cardId', $keys)) {
            $expression['cardid'] = $member->getCardId();
        }
        if (in_array('contactAddress', $keys)) {
            $expression['contact_address'] = $member->getContactAddress();
        }
        if (in_array('securityQuestion', $keys)) {
            $expression['security_question'] = $member->getSecurityQuestion();
        }
        if (in_array('securityAnswer', $keys)) {
            $expression['security_answer'] = $member->getSecurityAnswer();
        }
        if (in_array('gender', $keys)) {
            $expression['gender'] = $member->getGender();
        }
        if (in_array('password', $keys)) {
            $expression['password'] = $member->getPassword();
        }
        if (in_array('salt', $keys)) {
            $expression['salt'] = $member->getSalt();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $member->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $member->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $member->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $member->getStatusTime();
        }
        
        return $expression;
    }
}
