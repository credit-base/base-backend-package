<?php
namespace Base\Member\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Common\Controller\Interfaces\IEnableAbleController;

use Base\Member\Model\Member;
use Base\Member\View\MemberView;
use Base\Member\Repository\MemberRepository;
use Base\Member\Adapter\Member\IMemberAdapter;
use Base\Member\Command\Member\EnableMemberCommand;
use Base\Member\Command\Member\DisableMemberCommand;
use Base\Member\CommandHandler\Member\MemberCommandHandlerFactory;

class MemberEnableController extends Controller implements IEnableAbleController
{
    use JsonApiTrait;

    private $repository;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new MemberRepository();
        $this->commandBus = new CommandBus(new MemberCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();

        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : IMemberAdapter
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * 对应路由 /members/{id:\d+}/enable
     * 启用, 通过PATCH传参
     * @param int id 前台用户id
     * @return jsonApi
     */
    public function enable(int $id)
    {
        if (!empty($id)) {
            $command = new EnableMemberCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $member  = $this->getRepository()->fetchOne($id);
                if ($member instanceof Member) {
                    $this->render(new MemberView($member));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /member/{id:\d+}/disable
     * 禁用, 通过PATCH传参
     * @param int id 前台用户id
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $command = new DisableMemberCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $member  = $this->getRepository()->fetchOne($id);
                if ($member instanceof Member) {
                    $this->render(new MemberView($member));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
