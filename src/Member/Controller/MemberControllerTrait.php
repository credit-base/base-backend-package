<?php
namespace Base\Member\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\User\WidgetRule\UserWidgetRule;

use Base\Member\WidgetRule\MemberWidgetRule;
use Base\Member\Repository\MemberRepository;
use Base\Member\CommandHandler\Member\MemberCommandHandlerFactory;

trait MemberControllerTrait
{
    protected function getMemberWidgetRule() : MemberWidgetRule
    {
        return new MemberWidgetRule();
    }

    protected function getUserWidgetRule() : UserWidgetRule
    {
        return new UserWidgetRule();
    }

    protected function getRepository() : MemberRepository
    {
        return new MemberRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new MemberCommandHandlerFactory());
    }

    protected function validateAddScenario(
        $userName,
        $realName,
        $cardId,
        $cellphone,
        $email,
        $contactAddress,
        $securityAnswer,
        $password,
        $securityQuestion
    ) : bool {
        $userWidgetRule = $this->getUserWidgetRule();
        $memberWidgetRule = $this->getMemberWidgetRule();
        return $memberWidgetRule->userName($userName)
            && $userWidgetRule->realName($realName)
            && $userWidgetRule->cardId($cardId)
            && $userWidgetRule->cellphone($cellphone)
            && $memberWidgetRule->email($email)
            && $memberWidgetRule->contactAddress($contactAddress)
            && $memberWidgetRule->securityAnswer($securityAnswer)
            && $userWidgetRule->password($password, 'password')
            && $memberWidgetRule->securityQuestion($securityQuestion);
    }

    protected function validateEditScenario(
        $gender
    ) : bool {
        return empty($gender) ? true : $this->getMemberWidgetRule()->gender($gender);
    }

    protected function validateSignInScenario(
        $userName,
        $password
    ) : bool {
        return $this->getMemberWidgetRule()->userName($userName)
            && $this->getUserWidgetRule()->password($password, 'password');
    }

    protected function validateResetPasswordScenario(
        $userName,
        $securityAnswer,
        $password
    ) : bool {
        $userWidgetRule = $this->getUserWidgetRule();
        $memberWidgetRule = $this->getMemberWidgetRule();
        return $memberWidgetRule->userName($userName)
            && $memberWidgetRule->securityAnswer($securityAnswer)
            && $userWidgetRule->password($password, 'password');
    }

    protected function validateUpdatePasswordScenario(
        $oldPassword,
        $password
    ) : bool {
        $userWidgetRule = $this->getUserWidgetRule();
        return $userWidgetRule->password($oldPassword, 'oldPassword')
            && $userWidgetRule->password($password, 'password');
    }

    protected function validateValidateSecurityScenario(
        $securityAnswer
    ) : bool {
        return $this->getMemberWidgetRule()->securityAnswer($securityAnswer);
    }
}
