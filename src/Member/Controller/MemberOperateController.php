<?php
namespace Base\Member\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\Member\Model\Member;
use Base\Member\View\MemberView;
use Base\Member\Command\Member\AddMemberCommand;
use Base\Member\Command\Member\EditMemberCommand;
use Base\Member\Command\Member\SignInMemberCommand;

class MemberOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, MemberControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 前台用户新增功能,通过POST传参
     * 对应路由 /members
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];

        $userName = $attributes['userName'];
        $realName = $attributes['realName'];
        $cardId = $attributes['cardId'];
        $cellphone = $attributes['cellphone'];
        $email = $attributes['email'];
        $contactAddress = $attributes['contactAddress'];
        $securityAnswer = $attributes['securityAnswer'];
        $securityQuestion = $attributes['securityQuestion'];
        $password = $attributes['password'];
        //验证
        if ($this->validateAddScenario(
            $userName,
            $realName,
            $cardId,
            $cellphone,
            $email,
            $contactAddress,
            $securityAnswer,
            $password,
            $securityQuestion
        )) {
            //初始化命令
            $command = new AddMemberCommand(
                $userName,
                $realName,
                $cardId,
                $cellphone,
                $email,
                $contactAddress,
                $securityAnswer,
                $password,
                $securityQuestion
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $member = $this->getRepository()->fetchOne($command->id);
                if ($member instanceof Member) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new MemberView($member));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 前台用户编辑功能,通过PATCH传参
     * 对应路由 /members/{id:\d+}
     * @param int id 前台用户 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $gender = $attributes['gender'];
        
        if ($this->validateEditScenario($gender)) {
            $command = new EditMemberCommand(
                $gender,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $member = $this->getRepository()->fetchOne($id);
                if ($member instanceof Member) {
                    $this->render(new MemberView($member));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 前台用户登录功能,通过POST传参
     * 对应路由 /members/signIn
     * @return jsonApi
     */
    public function signIn()
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];

        $userName = $attributes['userName'];
        $password = $attributes['password'];

        if ($this->validateSignInScenario(
            $userName,
            $password
        )) {
            $command = new SignInMemberCommand(
                $userName,
                $password
            );

            if ($this->getCommandBus()->send($command)) {
                $member = $this->getRepository()->fetchOne($command->id);
                if ($member instanceof Member) {
                    $this->render(new MemberView($member));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
