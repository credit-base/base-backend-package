<?php
namespace Base\Member\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Member\Model\Member;
use Base\Member\View\MemberView;
use Base\Member\Command\Member\ResetPasswordMemberCommand;
use Base\Member\Command\Member\UpdatePasswordMemberCommand;
use Base\Member\Command\Member\ValidateSecurityMemberCommand;

class MemberPasswordController extends Controller
{
    use JsonApiTrait, MemberControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 对应路由 /members/resetPassword
     * 忘记密码, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function resetPassword()
    {
        $data = $this->getRequest()->patch('data');

        $attributes = $data['attributes'];
        
        $userName = $attributes['userName'];
        $securityAnswer = $attributes['securityAnswer'];
        $password = $attributes['password'];

        if ($this->validateResetPasswordScenario(
            $userName,
            $securityAnswer,
            $password
        )) {
            $command = new ResetPasswordMemberCommand(
                $userName,
                $securityAnswer,
                $password
            );

            if ($this->getCommandBus()->send($command)) {
                $member = $this->getRepository()->fetchOne($command->id);
                if ($member instanceof Member) {
                    $this->render(new MemberView($member));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /members/{id:\d+}/updatePassword
     * 修改密码,通过PATCH传参,json
     * @param string id 用户id
     * @param jsonApi
     * @return jsonApi
     */
    public function updatePassword(int $id)
    {
        $data = $this->getRequest()->patch("data");
        $attributes = $data['attributes'];

        $oldPassword = $attributes['oldPassword'];
        $password = $attributes['password'];

        if ($this->validateUpdatePasswordScenario(
            $oldPassword,
            $password
        )) {
            $command = new UpdatePasswordMemberCommand(
                $password,
                $oldPassword,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $member = $this->getRepository()->fetchOne($command->id);
                if ($member instanceof Member) {
                    $this->render(new MemberView($member));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
    
    /**
     * 对应路由 /members/{id:\d+}/validateSecurity
     * 验证密保答案,通过PATCH传参,json
     * @param string id 用户id
     * @param jsonApi
     * @return jsonApi
     */
    public function validateSecurity(int $id)
    {
        $data = $this->getRequest()->patch("data");
        $attributes = $data['attributes'];

        $securityAnswer = $attributes['securityAnswer'];

        if ($this->validateValidateSecurityScenario(
            $securityAnswer
        )) {
            $command = new ValidateSecurityMemberCommand(
                $securityAnswer,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $member = $this->getRepository()->fetchOne($command->id);
                if ($member instanceof Member) {
                    $this->render(new MemberView($member));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
