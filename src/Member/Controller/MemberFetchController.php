<?php
namespace Base\Member\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Member\View\MemberView;
use Base\Member\Repository\MemberRepository;
use Base\Member\Adapter\Member\IMemberAdapter;

class MemberFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new MemberRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IMemberAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new MemberView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'members';
    }
}
