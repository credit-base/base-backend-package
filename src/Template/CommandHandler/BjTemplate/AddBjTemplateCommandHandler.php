<?php
namespace Base\Template\CommandHandler\BjTemplate;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Base\Template\Command\BjTemplate\AddBjTemplateCommand;
use Base\Template\Model\BjTemplate;
use Base\Template\Model\GbTemplate;
use Base\UserGroup\Model\UserGroup;

class AddBjTemplateCommandHandler implements ICommandHandler
{

    protected function getBjTemplate() : BjTemplate
    {
        return new BjTemplate();
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddBjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $bjTemplate = $this->getBjTemplate();

        $bjTemplate->setSourceUnit(new UserGroup($command->sourceUnit));
        $bjTemplate->getGbTemplate()->setId($command->gbTemplate);
        $bjTemplate->setName($command->name);
        $bjTemplate->setIdentify($command->identify);
        $bjTemplate->setSubjectCategory($command->subjectCategory);
        $bjTemplate->setDimension($command->dimension);
        $bjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $bjTemplate->setInfoClassify($command->infoClassify);
        $bjTemplate->setInfoCategory($command->infoCategory);
        $bjTemplate->setDescription($command->description);
        $bjTemplate->setItems($command->items);

        if ($bjTemplate->add()) {
            $command->id = $bjTemplate->getId();
            return true;
        }
        return false;
    }
}
