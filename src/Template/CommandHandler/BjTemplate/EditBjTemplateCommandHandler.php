<?php
namespace Base\Template\CommandHandler\BjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Template\Model\GbTemplate;
use Base\Template\Repository\BjTemplateSdkRepository;
use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use Base\Template\Command\BjTemplate\EditBjTemplateCommand;

use Base\UserGroup\Model\UserGroup;

class EditBjTemplateCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new BjTemplateSdkRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getBjTemplateSdkRepository() : IBjTemplateAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditBjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getBjTemplateSdkRepository();
        $bjTemplate = $repository->fetchOne($command->id);

        $bjTemplate->setName($command->name);
        $bjTemplate->setIdentify($command->identify);
        $bjTemplate->setSubjectCategory($command->subjectCategory);
        $bjTemplate->setDimension($command->dimension);
        $bjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $bjTemplate->setInfoClassify($command->infoClassify);
        $bjTemplate->setInfoCategory($command->infoCategory);
        $bjTemplate->setDescription($command->description);
        $bjTemplate->setItems($command->items);
        $bjTemplate->setSourceUnit(new UserGroup($command->sourceUnit));
        $bjTemplate->getGbTemplate()->setId($command->gbTemplate);

        return $bjTemplate->edit();
    }
}
