<?php
namespace Base\Template\CommandHandler\GbTemplate;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Base\Template\Command\GbTemplate\EditGbTemplateCommand;
use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use Base\Template\Repository\GbTemplateSdkRepository;

class EditGbTemplateCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new GbTemplateSdkRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getGbTemplateSdkRepository() : IGbTemplateAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditGbTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getGbTemplateSdkRepository();
        $gbTemplate = $repository->fetchOne($command->id);

        $gbTemplate->setName($command->name);
        $gbTemplate->setIdentify($command->identify);
        $gbTemplate->setSubjectCategory($command->subjectCategory);
        $gbTemplate->setDimension($command->dimension);
        $gbTemplate->setExchangeFrequency($command->exchangeFrequency);
        $gbTemplate->setInfoClassify($command->infoClassify);
        $gbTemplate->setInfoCategory($command->infoCategory);
        $gbTemplate->setDescription($command->description);
        $gbTemplate->setItems($command->items);

        return $gbTemplate->edit();
    }
}
