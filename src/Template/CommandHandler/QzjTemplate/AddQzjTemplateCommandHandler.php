<?php
namespace Base\Template\CommandHandler\QzjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Template\Command\QzjTemplate\AddQzjTemplateCommand;

class AddQzjTemplateCommandHandler implements ICommandHandler
{
    use QzjTemplateCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddQzjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $sourceUnit = $this->fetchUserGroup($command->sourceUnit);

        $qzjTemplate = $this->getQzjTemplate();

        $qzjTemplate->setSourceUnit($sourceUnit);
        $qzjTemplate->setName($command->name);
        $qzjTemplate->setIdentify($command->identify);
        $qzjTemplate->setCategory($command->category);
        $qzjTemplate->setSubjectCategory($command->subjectCategory);
        $qzjTemplate->setDimension($command->dimension);
        $qzjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $qzjTemplate->setInfoClassify($command->infoClassify);
        $qzjTemplate->setInfoCategory($command->infoCategory);
        $qzjTemplate->setDescription($command->description);
        $qzjTemplate->setItems($command->items);

        if ($qzjTemplate->add()) {
            $command->id = $qzjTemplate->getId();
            return true;
        }
        return false;
    }
}
