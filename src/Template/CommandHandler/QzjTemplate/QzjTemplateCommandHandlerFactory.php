<?php
namespace Base\Template\CommandHandler\QzjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class QzjTemplateCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Template\Command\QzjTemplate\AddQzjTemplateCommand' =>
        'Base\Template\CommandHandler\QzjTemplate\AddQzjTemplateCommandHandler',
        'Base\Template\Command\QzjTemplate\EditQzjTemplateCommand' =>
        'Base\Template\CommandHandler\QzjTemplate\EditQzjTemplateCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
