<?php
namespace Base\Template\CommandHandler\QzjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Template\Repository\QzjTemplateRepository;
use Base\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;
use Base\Template\Command\QzjTemplate\EditQzjTemplateCommand;

class EditQzjTemplateCommandHandler implements ICommandHandler
{
    use QzjTemplateCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof EditQzjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $sourceUnit = $this->fetchUserGroup($command->sourceUnit);

        $qzjTemplate = $this->fetchQzjTemplate($command->id);

        $qzjTemplate->setName($command->name);
        $qzjTemplate->setIdentify($command->identify);
        $qzjTemplate->setCategory($command->category);
        $qzjTemplate->setSourceUnit($sourceUnit);
        $qzjTemplate->setSubjectCategory($command->subjectCategory);
        $qzjTemplate->setDimension($command->dimension);
        $qzjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $qzjTemplate->setInfoClassify($command->infoClassify);
        $qzjTemplate->setInfoCategory($command->infoCategory);
        $qzjTemplate->setDescription($command->description);
        $qzjTemplate->setItems($command->items);

        return $qzjTemplate->edit();
    }
}
