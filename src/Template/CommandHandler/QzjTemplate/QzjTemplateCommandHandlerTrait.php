<?php
namespace Base\Template\CommandHandler\QzjTemplate;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Repository\UserGroupRepository;

use Base\Template\Model\QzjTemplate;
use Base\Template\Repository\QzjTemplateSdkRepository;

trait QzjTemplateCommandHandlerTrait
{
    protected function getUserGroupRepository() : UserGroupRepository
    {
        return new UserGroupRepository();
    }

    protected function fetchUserGroup(int $id) : UserGroup
    {
        return $this->getUserGroupRepository()->fetchOne($id);
    }

    protected function getQzjTemplate() : QzjTemplate
    {
        return new QzjTemplate();
    }

    protected function getQzjTemplateSdkRepository() : QzjTemplateSdkRepository
    {
        return new QzjTemplateSdkRepository();
    }

    protected function fetchQzjTemplate(int $id) : QzjTemplate
    {
        return $this->getQzjTemplateSdkRepository()->fetchOne($id);
    }
}
