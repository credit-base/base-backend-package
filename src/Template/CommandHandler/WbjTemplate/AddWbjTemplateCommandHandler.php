<?php
namespace Base\Template\CommandHandler\WbjTemplate;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Base\Template\Command\WbjTemplate\AddWbjTemplateCommand;
use Base\Template\Model\WbjTemplate;
use Base\UserGroup\Model\UserGroup;

class AddWbjTemplateCommandHandler implements ICommandHandler
{

    protected function getWbjTemplate() : WbjTemplate
    {
        return new WbjTemplate();
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddWbjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $wbjTemplate = $this->getWbjTemplate();

        $wbjTemplate->setSourceUnit(new UserGroup($command->sourceUnit));
        $wbjTemplate->setName($command->name);
        $wbjTemplate->setIdentify($command->identify);
        $wbjTemplate->setSubjectCategory($command->subjectCategory);
        $wbjTemplate->setDimension($command->dimension);
        $wbjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $wbjTemplate->setInfoClassify($command->infoClassify);
        $wbjTemplate->setInfoCategory($command->infoCategory);
        $wbjTemplate->setDescription($command->description);
        $wbjTemplate->setItems($command->items);

        if ($wbjTemplate->add()) {
            $command->id = $wbjTemplate->getId();
            return true;
        }
        return false;
    }
}
