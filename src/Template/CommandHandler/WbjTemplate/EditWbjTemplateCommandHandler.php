<?php
namespace Base\Template\CommandHandler\WbjTemplate;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Base\Template\Command\WbjTemplate\EditWbjTemplateCommand;
use Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use Base\Template\Repository\WbjTemplateSdkRepository;

use Base\UserGroup\Model\UserGroup;

class EditWbjTemplateCommandHandler implements ICommandHandler
{

    private $repository;

    public function __construct()
    {
        $this->repository = new WbjTemplateSdkRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getWbjTemplateSdkRepository() : IWbjTemplateAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditWbjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getWbjTemplateSdkRepository();
        $wbjTemplate = $repository->fetchOne($command->id);

        $wbjTemplate->setName($command->name);
        $wbjTemplate->setIdentify($command->identify);
        $wbjTemplate->setSubjectCategory($command->subjectCategory);
        $wbjTemplate->setDimension($command->dimension);
        $wbjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $wbjTemplate->setInfoClassify($command->infoClassify);
        $wbjTemplate->setInfoCategory($command->infoCategory);
        $wbjTemplate->setDescription($command->description);
        $wbjTemplate->setItems($command->items);
        $wbjTemplate->setSourceUnit(new UserGroup($command->sourceUnit));

        return $wbjTemplate->edit();
    }
}
