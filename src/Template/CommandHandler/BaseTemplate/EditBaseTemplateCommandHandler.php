<?php
namespace Base\Template\CommandHandler\BaseTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Template\Repository\BaseTemplateSdkRepository;
use Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;
use Base\Template\Command\BaseTemplate\EditBaseTemplateCommand;

class EditBaseTemplateCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new BaseTemplateSdkRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getBaseTemplateSdkRepository() : IBaseTemplateAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditBaseTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getBaseTemplateSdkRepository();
        $baseTemplate = $repository->fetchOne($command->id);

        $baseTemplate->setSubjectCategory($command->subjectCategory);
        $baseTemplate->setDimension($command->dimension);
        $baseTemplate->setExchangeFrequency($command->exchangeFrequency);
        $baseTemplate->setInfoClassify($command->infoClassify);
        $baseTemplate->setInfoCategory($command->infoCategory);
        $baseTemplate->setItems($command->items);

        return $baseTemplate->edit();
    }
}
