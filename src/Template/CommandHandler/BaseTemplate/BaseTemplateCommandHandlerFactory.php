<?php
namespace Base\Template\CommandHandler\BaseTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class BaseTemplateCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Template\Command\BaseTemplate\EditBaseTemplateCommand' =>
        'Base\Template\CommandHandler\BaseTemplate\EditBaseTemplateCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
