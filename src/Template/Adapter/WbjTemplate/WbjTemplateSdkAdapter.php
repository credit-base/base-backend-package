<?php
namespace Base\Template\Adapter\WbjTemplate;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\Template\Model\WbjTemplate;
use Base\Template\Model\NullWbjTemplate;
use Base\Template\Translator\WbjTemplateSdkTranslator;

use BaseSdk\Template\Repository\WbjTemplateRestfulRepository;

use Base\UserGroup\Repository\UserGroupRepository;

class WbjTemplateSdkAdapter implements IWbjTemplateAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->translator = new WbjTemplateSdkTranslator();
        $this->repository = new WbjTemplateRestfulRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
        unset($this->userGroupRepository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : WbjTemplateRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullWbjTemplate::getInstance();
    }

    public function fetchOne($id) : WbjTemplate
    {
        $wbjTemplate = $this->fetchOneAction($id);
        $this->fetchSourceUnit($wbjTemplate);

        return $wbjTemplate;
    }

    public function fetchList(array $ids) : array
    {
        $wbjTemplateList = $this->fetchListAction($ids);
        $this->fetchSourceUnit($wbjTemplateList);

        return $wbjTemplateList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        list($wbjTemplateList, $count) = $this->filterAction($filter, $sort, $number, $size);
        $this->fetchSourceUnit($wbjTemplateList);
        
        return [$wbjTemplateList, $count];
    }

    public function add(WbjTemplate $wbjTemplate) : bool
    {
        return $this->addAction($wbjTemplate);
    }

    public function edit(WbjTemplate $wbjTemplate, array $keys = array()) : bool
    {
        return $this->editAction($wbjTemplate, $keys);
    }

    protected function fetchSourceUnit($wbjTemplate)
    {
        return is_array($wbjTemplate) ?
        $this->fetchSourceUnitByList($wbjTemplate) :
        $this->fetchSourceUnitByObject($wbjTemplate);
    }

    protected function fetchSourceUnitByObject(WbjTemplate $wbjTemplate)
    {
        $sourceUnitId = $wbjTemplate->getSourceUnit()->getId();
        $sourceUnit = $this->getUserGroupRepository()->fetchOne($sourceUnitId);
        $wbjTemplate->setSourceUnit($sourceUnit);
    }

    protected function fetchSourceUnitByList(array $wbjTemplateList)
    {
        $sourceUnitIds = array();
        foreach ($wbjTemplateList as $wbjTemplate) {
            $sourceUnitIds[] = $wbjTemplate->getSourceUnit()->getId();
        }

        $sourceUnitList = $this->getUserGroupRepository()->fetchList($sourceUnitIds);
        if (!empty($sourceUnitList)) {
            foreach ($wbjTemplateList as $wbjTemplate) {
                $sourceUnitId = $wbjTemplate->getSourceUnit()->getId();
                if (isset($sourceUnitList[$sourceUnitId])
                    && ($sourceUnitList[$sourceUnitId]->getId() == $sourceUnitId)) {
                    $wbjTemplate->setSourceUnit($sourceUnitList[$sourceUnitId]);
                }
            }
        }
    }
}
