<?php
namespace Base\Template\Adapter\BaseTemplate;

use Base\Template\Model\BaseTemplate;

interface IBaseTemplateAdapter
{
    public function fetchOne($id) : BaseTemplate;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool;
}
