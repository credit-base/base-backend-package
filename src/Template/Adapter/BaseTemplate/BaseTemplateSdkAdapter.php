<?php
namespace Base\Template\Adapter\BaseTemplate;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\Template\Model\BaseTemplate;
use Base\Template\Model\NullBaseTemplate;
use Base\Template\Translator\BaseTemplateSdkTranslator;

use BaseSdk\Template\Repository\BaseTemplateRestfulRepository;

class BaseTemplateSdkAdapter implements IBaseTemplateAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    public function __construct()
    {
        $this->translator = new BaseTemplateSdkTranslator();
        $this->repository = new BaseTemplateRestfulRepository();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : BaseTemplateRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullBaseTemplate::getInstance();
    }

    public function fetchOne($id) : BaseTemplate
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $number, $size);
    }

    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool
    {
        return $this->editAction($baseTemplate, $keys);
    }
}
