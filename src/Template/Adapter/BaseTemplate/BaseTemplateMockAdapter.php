<?php
namespace Base\Template\Adapter\BaseTemplate;

use Base\Template\Model\BaseTemplate;
use Base\Template\Utils\BaseTemplateMockFactory;

class BaseTemplateMockAdapter implements IBaseTemplateAdapter
{
    public function fetchOne($id) : BaseTemplate
    {
        return BaseTemplateMockFactory::generateBaseTemplate($id);
    }

    public function fetchList(array $ids) : array
    {
        $baseTemplateList = array();

        foreach ($ids as $id) {
            $baseTemplateList[$id] = BaseTemplateMockFactory::generateBaseTemplate($id);
        }

        return $baseTemplateList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool
    {
        unset($baseTemplate);
        unset($keys);
        return true;
    }
}
