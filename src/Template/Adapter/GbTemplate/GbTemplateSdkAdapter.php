<?php
namespace Base\Template\Adapter\GbTemplate;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\Template\Model\GbTemplate;
use Base\Template\Model\NullGbTemplate;
use Base\Template\Translator\GbTemplateSdkTranslator;

use BaseSdk\Template\Repository\GbTemplateRestfulRepository;

class GbTemplateSdkAdapter implements IGbTemplateAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    public function __construct()
    {
        $this->translator = new GbTemplateSdkTranslator();
        $this->repository = new GbTemplateRestfulRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : GbTemplateRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullGbTemplate::getInstance();
    }

    public function fetchOne($id) : GbTemplate
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $number, $size);
    }

    public function add(GbTemplate $gbTemplate) : bool
    {
        return $this->addAction($gbTemplate);
    }

    public function edit(GbTemplate $gbTemplate, array $keys = array()) : bool
    {
        return $this->editAction($gbTemplate, $keys);
    }
}
