<?php
namespace Base\Template\Adapter\GbTemplate;

use Base\Template\Model\GbTemplate;

interface IGbTemplateAdapter
{
    public function fetchOne($id) : GbTemplate;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(GbTemplate $gbTemplate) : bool;

    public function edit(GbTemplate $gbTemplate, array $keys = array()) : bool;
}
