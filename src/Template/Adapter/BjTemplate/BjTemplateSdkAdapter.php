<?php
namespace Base\Template\Adapter\BjTemplate;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\Template\Model\BjTemplate;
use Base\Template\Model\NullBjTemplate;
use Base\Template\Translator\BjTemplateSdkTranslator;

use BaseSdk\Template\Repository\BjTemplateRestfulRepository;

use Base\UserGroup\Repository\UserGroupRepository;

class BjTemplateSdkAdapter implements IBjTemplateAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->translator = new BjTemplateSdkTranslator();
        $this->repository = new BjTemplateRestfulRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
        unset($this->userGroupRepository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : BjTemplateRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullBjTemplate::getInstance();
    }

    public function fetchOne($id) : BjTemplate
    {
        $bjTemplate = $this->fetchOneAction($id);
        $this->fetchSourceUnit($bjTemplate);

        return $bjTemplate;
    }

    public function fetchList(array $ids) : array
    {
        $bjTemplateList = $this->fetchListAction($ids);
        $this->fetchSourceUnit($bjTemplateList);

        return $bjTemplateList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        list($bjTemplateList, $count) = $this->filterAction($filter, $sort, $number, $size);
        $this->fetchSourceUnit($bjTemplateList);
        
        return [$bjTemplateList, $count];
    }

    public function add(BjTemplate $bjTemplate) : bool
    {
        return $this->addAction($bjTemplate);
    }

    public function edit(BjTemplate $bjTemplate, array $keys = array()) : bool
    {
        return $this->editAction($bjTemplate, $keys);
    }

    protected function fetchSourceUnit($bjTemplate)
    {
        return is_array($bjTemplate) ?
        $this->fetchSourceUnitByList($bjTemplate) :
        $this->fetchSourceUnitByObject($bjTemplate);
    }

    protected function fetchSourceUnitByObject(BjTemplate $bjTemplate)
    {
        $sourceUnitId = $bjTemplate->getSourceUnit()->getId();
        $sourceUnit = $this->getUserGroupRepository()->fetchOne($sourceUnitId);
        $bjTemplate->setSourceUnit($sourceUnit);
    }

    protected function fetchSourceUnitByList(array $bjTemplateList)
    {
        $sourceUnitIds = array();
        foreach ($bjTemplateList as $bjTemplate) {
            $sourceUnitIds[] = $bjTemplate->getSourceUnit()->getId();
        }

        $sourceUnitList = $this->getUserGroupRepository()->fetchList($sourceUnitIds);
        if (!empty($sourceUnitList)) {
            foreach ($bjTemplateList as $bjTemplate) {
                $sourceUnitId = $bjTemplate->getSourceUnit()->getId();
                if (isset($sourceUnitList[$sourceUnitId])
                    && ($sourceUnitList[$sourceUnitId]->getId() == $sourceUnitId)) {
                    $bjTemplate->setSourceUnit($sourceUnitList[$sourceUnitId]);
                }
            }
        }
    }
}
