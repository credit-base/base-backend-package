<?php
namespace Base\Template\Adapter\QzjTemplate;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\Template\Model\QzjTemplate;
use Base\Template\Model\NullQzjTemplate;
use Base\Template\Translator\QzjTemplateSdkTranslator;

use BaseSdk\Template\Repository\QzjTemplateRestfulRepository;

use Base\UserGroup\Repository\UserGroupRepository;

class QzjTemplateSdkAdapter implements IQzjTemplateAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->translator = new QzjTemplateSdkTranslator();
        $this->repository = new QzjTemplateRestfulRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
        unset($this->userGroupRepository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : QzjTemplateRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullQzjTemplate::getInstance();
    }

    public function fetchOne($id) : QzjTemplate
    {
        $qzjTemplate = $this->fetchOneAction($id);
        $this->fetchSourceUnit($qzjTemplate);

        return $qzjTemplate;
    }

    public function fetchList(array $ids) : array
    {
        $qzjTemplateList = $this->fetchListAction($ids);
        $this->fetchSourceUnit($qzjTemplateList);

        return $qzjTemplateList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        list($qzjTemplateList, $count) = $this->filterAction($filter, $sort, $number, $size);
        $this->fetchSourceUnit($qzjTemplateList);
        
        return [$qzjTemplateList, $count];
    }

    public function add(QzjTemplate $qzjTemplate) : bool
    {
        return $this->addAction($qzjTemplate);
    }

    public function edit(QzjTemplate $qzjTemplate, array $keys = array()) : bool
    {
        return $this->editAction($qzjTemplate, $keys);
    }

    protected function fetchSourceUnit($qzjTemplate)
    {
        return is_array($qzjTemplate) ?
        $this->fetchSourceUnitByList($qzjTemplate) :
        $this->fetchSourceUnitByObject($qzjTemplate);
    }

    protected function fetchSourceUnitByObject(QzjTemplate $qzjTemplate)
    {
        $sourceUnitId = $qzjTemplate->getSourceUnit()->getId();
        $sourceUnit = $this->getUserGroupRepository()->fetchOne($sourceUnitId);
        $qzjTemplate->setSourceUnit($sourceUnit);
    }

    protected function fetchSourceUnitByList(array $qzjTemplateList)
    {
        $sourceUnitIds = array();
        foreach ($qzjTemplateList as $qzjTemplate) {
            $sourceUnitIds[] = $qzjTemplate->getSourceUnit()->getId();
        }

        $sourceUnitList = $this->getUserGroupRepository()->fetchList($sourceUnitIds);
        if (!empty($sourceUnitList)) {
            foreach ($qzjTemplateList as $qzjTemplate) {
                $sourceUnitId = $qzjTemplate->getSourceUnit()->getId();
                if (isset($sourceUnitList[$sourceUnitId])
                    && ($sourceUnitList[$sourceUnitId]->getId() == $sourceUnitId)) {
                    $qzjTemplate->setSourceUnit($sourceUnitList[$sourceUnitId]);
                }
            }
        }
    }
}
