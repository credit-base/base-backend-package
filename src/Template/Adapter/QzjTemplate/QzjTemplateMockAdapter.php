<?php
namespace Base\Template\Adapter\QzjTemplate;

use Base\Template\Model\QzjTemplate;
use Base\Template\Utils\QzjTemplateMockFactory;

class QzjTemplateMockAdapter implements IQzjTemplateAdapter
{
    public function fetchOne($id) : QzjTemplate
    {
        return QzjTemplateMockFactory::generateQzjTemplate($id);
    }

    public function fetchList(array $ids) : array
    {
        $qzjTemplateList = array();

        foreach ($ids as $id) {
            $qzjTemplateList[$id] = QzjTemplateMockFactory::generateQzjTemplate($id);
        }

        return $qzjTemplateList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(QzjTemplate $qzjTemplate) : bool
    {
        unset($qzjTemplate);
        return true;
    }

    public function edit(QzjTemplate $qzjTemplate, array $keys = array()) : bool
    {
        unset($qzjTemplate);
        unset($keys);
        return true;
    }
}
