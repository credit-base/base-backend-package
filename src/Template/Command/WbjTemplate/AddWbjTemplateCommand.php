<?php
namespace Base\Template\Command\WbjTemplate;

use Base\Template\Command\Template\AddTemplateCommand;

class AddWbjTemplateCommand extends AddTemplateCommand
{
    /**
     * @var int $sourceUnit 来源单位
     */
    public $sourceUnit;
    
    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    public function __construct(
        string $name,
        string $identify,
        array $subjectCategory,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        string $description,
        array $items,
        int $sourceUnit,
        int $id = 0
    ) {
        parent::__construct(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $id
        );
        $this->sourceUnit = $sourceUnit;
    }
}
