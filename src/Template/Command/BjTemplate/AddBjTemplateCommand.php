<?php
namespace Base\Template\Command\BjTemplate;

use Base\Template\Command\Template\AddTemplateCommand;

class AddBjTemplateCommand extends AddTemplateCommand
{
    /**
     * @var int $sourceUnit 来源单位
     */
    public $sourceUnit;
    /**
     * @var int $gbTemplate 国标目录
     */
    public $gbTemplate;
    
    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    public function __construct(
        string $name,
        string $identify,
        array $subjectCategory,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        string $description,
        array $items,
        int $sourceUnit,
        int $gbTemplate,
        int $id = 0
    ) {
        parent::__construct(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $id
        );
        $this->sourceUnit = $sourceUnit;
        $this->gbTemplate = $gbTemplate;
    }
}
