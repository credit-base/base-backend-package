<?php
namespace Base\Template\Command\BaseTemplate;

use Marmot\Interfaces\ICommand;

class EditBaseTemplateCommand implements ICommand
{
    public $subjectCategory;

    public $dimension;

    public $exchangeFrequency;

    public $infoClassify;

    public $infoCategory;

    public $items;

    public $id;

    public function __construct(
        array $subjectCategory,
        array $items,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        int $id
    ) {
        $this->subjectCategory = $subjectCategory;
        $this->items = $items;
        $this->dimension = $dimension;
        $this->exchangeFrequency = $exchangeFrequency;
        $this->infoClassify = $infoClassify;
        $this->infoCategory = $infoCategory;
        $this->id = $id;
    }
}
