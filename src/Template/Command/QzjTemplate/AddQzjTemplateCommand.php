<?php
namespace Base\Template\Command\QzjTemplate;

use Base\Template\Command\Template\AddTemplateCommand;

class AddQzjTemplateCommand extends AddTemplateCommand
{
    /**
     * @var int $sourceUnit 来源单位
     */
    public $sourceUnit;
    
    /**
     * @var int $category 目录类别
     */
    public $category;
    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    public function __construct(
        string $name,
        string $identify,
        array $subjectCategory,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        string $description,
        array $items,
        int $sourceUnit,
        int $category,
        int $id = 0
    ) {
        parent::__construct(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $id
        );
        $this->sourceUnit = $sourceUnit;
        $this->category = $category;
    }
}
