<?php
namespace Base\Template\Model;

use Marmot\Core;

use Base\Common\Model\IOperate;
use Base\Common\Model\OperateTrait;

use Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use Base\Template\Repository\WbjTemplateSdkRepository;

use Base\UserGroup\Model\UserGroup;

class WbjTemplate extends Template implements IOperate
{
    use OperateTrait;

    /**
     * @var int $sourceUnit 来源单位
     */
    protected $sourceUnit;
    
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->sourceUnit = new UserGroup();
        $this->repository = new WbjTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
        unset($this->repository);
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }

    public function getCategory() : int
    {
        return Template::CATEGORY['WBJ'];
    }

    protected function getRepository() : IWbjTemplateAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->getRepository()->edit(
            $this,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit'
            )
        );
    }
}
