<?php
namespace Base\Template\Model;

use Marmot\Core;

use Base\Common\Model\IOperate;
use Base\Common\Model\OperateTrait;

use Base\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;
use Base\Template\Repository\QzjTemplateSdkRepository;

use Base\UserGroup\Model\UserGroup;

class QzjTemplate extends Template implements IOperate
{
    use OperateTrait;

    /**
     * 目录类别
     * @var CATEGORY['QZJ_WBJ']  前置机委办局 20
     * @var CATEGORY['QZJ_BJ']  前置机本级 21
     * @var CATEGORY['QZJ_GB']  前置机国标 22
     */
    const QZJ_TEMPLATE_CATEGORY = array(
        'QZJ_WBJ' => 20,
        'QZJ_BJ' => 21,
        'QZJ_GB' => 22,
    );

    /**
     * @var int $sourceUnit 来源单位
     */
    protected $sourceUnit;

    /**
     * @var int $category 目录类别
     */
    protected $category;
    
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->sourceUnit = new UserGroup();
        $this->category = 0;
        $this->repository = new QzjTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
        unset($this->category);
        unset($this->repository);
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }

    public function setCategory(int $category) : void
    {
        $this->category = $category;
    }
    
    public function getCategory() : int
    {
        return $this->category;
    }

    protected function getRepository() : IQzjTemplateAdapter
    {
        return $this->repository;
    }
    
    protected function isTemplateExist() : bool
    {
        $filter = array();

        $filter['category'] = $this->getCategory();
        $filter['identify'] = $this->getIdentify();
        $filter['sourceUnit'] = $this->getSourceUnit()->getId();

        if (!empty($this->getId())) {
            $filter['id'] = $this->getId();
        }

        list($list, $count) = $this->getRepository()->filter($filter);
        unset($list);
        
        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'qzjTemplate'));
            return false;
        }

        return true;
    }

    protected function addAction() : bool
    {
        return $this->isTemplateExist() && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->isTemplateExist() && $this->getRepository()->edit(
            $this,
            array(
                'name',
                'identify',
                'category',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit'
            )
        );
    }
}
