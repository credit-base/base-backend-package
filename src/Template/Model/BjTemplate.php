<?php
namespace Base\Template\Model;

use Marmot\Core;

use Base\Common\Model\IOperate;
use Base\Common\Model\OperateTrait;

use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use Base\Template\Repository\BjTemplateSdkRepository;

use Base\UserGroup\Model\UserGroup;

class BjTemplate extends Template implements IOperate
{
    use OperateTrait;

    /**
     * @var int $sourceUnit 来源单位
     */
    protected $sourceUnit;
    /**
     * @var int $gbTemplate 国标目录
     */
    protected $gbTemplate;
    
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->sourceUnit = new UserGroup();
        $this->gbTemplate = new GbTemplate();
        $this->repository = new BjTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
        unset($this->gbTemplate);
        unset($this->repository);
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }

    public function setGbTemplate(GbTemplate $gbTemplate) : void
    {
        $this->gbTemplate = $gbTemplate;
    }

    public function getGbTemplate() : gbTemplate
    {
        return $this->gbTemplate;
    }

    public function getCategory() : int
    {
        return Template::CATEGORY['BJ'];
    }

    protected function getRepository() : IBjTemplateAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->getRepository()->edit(
            $this,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'gbTemplate'
            )
        );
    }
}
