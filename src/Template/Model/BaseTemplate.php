<?php
namespace Base\Template\Model;

use Marmot\Core;

use Base\Common\Model\IOperate;
use Base\Common\Model\OperateTrait;

use Base\Template\Repository\BaseTemplateSdkRepository;
use Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class BaseTemplate extends Template implements IOperate
{
    use OperateTrait;
    
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new BaseTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    public function getCategory() : int
    {
        return Template::CATEGORY['BASE'];
    }

    protected function getRepository() : IBaseTemplateAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        return false;
    }

    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'subjectCategory',
                'dimension',
                'infoClassify',
                'infoCategory',
                'items',
                'updateTime'
            )
        );
    }
}
