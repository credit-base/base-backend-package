<?php
namespace Base\Template\Model;

use Marmot\Core;

use Base\Common\Model\IOperate;
use Base\Common\Model\OperateTrait;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use Base\Template\Repository\GbTemplateSdkRepository;

class GbTemplate extends Template implements IOperate
{
    use OperateTrait;
    
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new GbTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    public function getCategory() : int
    {
        return Template::CATEGORY['GB'];
    }

    protected function getRepository() : IGbTemplateAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->getRepository()->edit(
            $this,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            )
        );
    }
}
