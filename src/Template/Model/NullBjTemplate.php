<?php
namespace Base\Template\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;

class NullBjTemplate extends BjTemplate implements INull
{
    use NullOperateTrait;
    
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
