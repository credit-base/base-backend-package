<?php
namespace Base\Template\WidgetRule;

use Respect\Validation\Validator as V;

use Marmot\Core;
use Base\Template\Model\Template;
use Base\Template\Model\QzjTemplate;

/**
 * @SuppressWarnings(PHPMD)
 */
class TemplateWidgetRule
{
    const NAME_MIN_LENGTH = 1;
    const NAME_MAX_LENGTH = 100;
    public function name($name) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'name'));
            return false;
        }
        return true;
    }

    public function identify($identify) : bool
    {
        $reg = '/^[A-Z][A-Z_]{0,98}[A-Z]$/';
        if (!preg_match($reg, $identify)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'identify'));
            return false;
        }
        return true;
    }

    public function subjectCategory($subjectCategory) : bool
    {
        if (empty($subjectCategory) || !is_array($subjectCategory)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'subjectCategory'));
            return false;
        }
        if (array_diff($subjectCategory, Template::SUBJECT_CATEGORY)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'subjectCategory'));
            return false;
        }
        return true;
    }

    public function dimension($dimension) : bool
    {
        if (!V::intVal()->validate($dimension) || !in_array($dimension, Template::DIMENSION)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'dimension'));
            return false;
        }
        return true;
    }

    public function infoCategory($infoCategory) : bool
    {
        if (!empty($infoCategory)) {
            if (!V::intVal()->validate($infoCategory)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'infoCategory'));
                return false;
            }
        }
        return true;
    }

    const DESCRIPTION_MIN_LENGTH = 1;
    const DESCRIPTION_MAX_LENGTH = 2000;
    public function description($description) : bool
    {
        if (!empty($description)) {
            if (!V::charset('UTF-8')->stringType()->length(
                self::DESCRIPTION_MIN_LENGTH,
                self::DESCRIPTION_MAX_LENGTH
            )->validate($description)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'description'));
                return false;
            }
        }
        return true;
    }

    const ITEMS_KEYS = array(
        'name',
        'identify',
        'type',
        'length',
        'options',
        'dimension',
        'isNecessary',
        'isMasked',
        'maskRule',
        'remarks'
    );

    public function items($subjectCategory, $items) : bool
    {
        if (empty($items) || !is_array($items)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'items'));
            return false;
        }

        foreach ($items as $each) {
            if (!empty(array_diff(self::ITEMS_KEYS, array_keys($each)))) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'items'));
                return false;
            }
            if (!$this->name($each['name'])) {
                return false;
            }
            if (!$this->identify($each['identify'])) {
                return false;
            }
            if (!$this->type($each['type'])) {
                return false;
            }
            if (!$this->length($each['type'], $each['length'])) {
                return false;
            }
            if (!$this->options($each['type'], $each['options'])) {
                return false;
            }
            if (!$this->dimension($each['dimension'])) {
                return false;
            }
            if (!$this->isNecessary($each['isNecessary'])) {
                return false;
            }
            if (!$this->isMasked($each['isMasked'])) {
                return false;
            }
            if (!$this->maskRule($each['isMasked'], $each['maskRule'])) {
                return false;
            }
            if (!$this->remarks($each['remarks'])) {
                return false;
            }
            $names[] = $each['name'];
            $identifies[] = $each['identify'];
        }
        
        if (!$this->nameUnique($names)) {
            return false;
        }
        if (!$this->identifyUnique($identifies)) {
            return false;
        }
        if (!$this->identifyDefine($subjectCategory, $identifies)) {
            return false;
        }
        return true;
    }

    protected function nameUnique($names) : bool
    {
        if (count(array_unique($names)) != count($names)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'name'));
            return false;
        }
        return true;
    }

    protected function identifyUnique($identifies) : bool
    {
        if (count(array_unique($identifies)) != count($identifies)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'identify'));
            return false;
        }
        return true;
    }

    const IDENTIFY_DEFINE = array(
        'ZTMC' => 'ZTMC',
        'ZJHM' => 'ZJHM',
        'TYSHXYDM' => 'TYSHXYDM',
    );
    const FR = array(
        Template::SUBJECT_CATEGORY['FRJFFRZZ'],
        Template::SUBJECT_CATEGORY['GTGSH']
    );
    protected function identifyDefine($subjectCategory, $identifies) : bool
    {
        if (!in_array(self::IDENTIFY_DEFINE['ZTMC'], $identifies)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'ZTMC'));
            return false;
        }
        if (!empty($subjectCategory)) {
            foreach ($subjectCategory as $value) {
                if ($value == Template::SUBJECT_CATEGORY['ZRR']) {
                    if (!in_array(self::IDENTIFY_DEFINE['ZJHM'], $identifies)) {
                        Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'ZJHM'));
                        return false;
                    }
                }
                if (in_array($value, self::FR)) {
                    if (!in_array(self::IDENTIFY_DEFINE['TYSHXYDM'], $identifies)) {
                        Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'TYSHXYDM'));
                        return false;
                    }
                }
            }
        }
        if (empty($subjectCategory)) {
            if (!in_array(self::IDENTIFY_DEFINE['ZJHM'], $identifies)
                && !in_array(self::IDENTIFY_DEFINE['TYSHXYDM'], $identifies)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'identify'));
                return false;
            }
        }
        return true;
    }

    protected function type($type) : bool
    {
        if (!V::intVal()->validate($type) || !in_array($type, Template::TYPE)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'type'));
            return false;
        }
        return true;
    }

    const TYPE_LENGTH = array(
        Template::TYPE['ZFX'],
        Template::TYPE['ZSX'],
        Template::TYPE['MJX'],
        Template::TYPE['JHX']
    );
    protected function length($type, $length) : bool
    {
        if (in_array($type, self::TYPE_LENGTH)) {
            if (!V::intVal()->validate($length)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'length'));
                return false;
            }
        }
        return true;
    }

    const TYPE_IS_ENUM = array(
        Template::TYPE['MJX'],
        Template::TYPE['JHX']
    );
    protected function options($type, $options) : bool
    {
        if (in_array($type, self::TYPE_IS_ENUM)) {
            if (empty($options) || !is_array($options)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'options'));
                return false;
            }
        }
        if (!in_array($type, self::TYPE_IS_ENUM)) {
            if (!empty($options)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'options'));
                return false;
            }
        }
        return true;
    }

    protected function isNecessary($isNecessary) : bool
    {
        if (!V::intVal()->validate($isNecessary) || !in_array($isNecessary, Template::IS_NECESSARY)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'isNecessary'));
            return false;
        }
        return true;
    }

    protected function isMasked($isMasked) : bool
    {
        if (!V::intVal()->validate($isMasked) || !in_array($isMasked, Template::IS_MASKED)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'isMasked'));
            return false;
        }
        return true;
    }

    protected function maskRule($isMasked, $maskRule) : bool
    {
        if ($isMasked == Template::IS_MASKED['SHI']) {
            if (empty($maskRule) || !is_array($maskRule)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'maskRule'));
                return false;
            }
        }
        if ($isMasked == Template::IS_MASKED['FOU']) {
            if (!empty($maskRule)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'maskRule'));
                return false;
            }
        }
        return true;
    }

    const REMARKS_MIN_LENGTH = 1;
    const REMARKS_MAX_LENGTH = 2000;
    public function remarks($remarks) : bool
    {
        if (!empty($remarks)) {
            if (!V::charset('UTF-8')->stringType()->length(
                self::REMARKS_MIN_LENGTH,
                self::REMARKS_MAX_LENGTH
            )->validate($remarks)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'remarks'));
                return false;
            }
        }
        return true;
    }

    public function qzjCategory($category) : bool
    {
        if (!V::intVal()->validate($category) || !in_array($category, QzjTemplate::QZJ_TEMPLATE_CATEGORY)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'category'));
            return false;
        }
        return true;
    }

    public function qzjSourceUnit($category, $sourceUnit) : bool
    {
        if ($category == QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_GB'] && !empty($sourceUnit)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'sourceUnit'));
            return false;
        }

        if (!is_numeric($sourceUnit)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'sourceUnit'));
            return false;
        }

        return true;
    }
}
