<?php
namespace Base\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Template\WidgetRule\TemplateWidgetRule;
use Base\Template\Repository\BjTemplateSdkRepository;
use Base\Template\CommandHandler\BjTemplate\BjTemplateCommandHandlerFactory;

trait BjTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : BjTemplateSdkRepository
    {
        return new BjTemplateSdkRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new BjTemplateCommandHandlerFactory());
    }
}
