<?php
namespace Base\Template\Controller;

use Marmot\Core;
use Marmot\Framework\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\Template\Model\GbTemplate;
use Base\Template\View\GbTemplateView;
use Base\Template\Command\GbTemplate\AddGbTemplateCommand;
use Base\Template\Command\GbTemplate\EditGbTemplateCommand;

class GbTemplateOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, GbTemplateControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 国标资源目录新增功能,通过POST传参
     * 对应路由 /gbTemplates
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];

        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $subjectCategory = $attributes['subjectCategory'];
        $dimension = $attributes['dimension'];
        $exchangeFrequency = $attributes['exchangeFrequency'];
        $infoClassify = $attributes['infoClassify'];
        $infoCategory = $attributes['infoCategory'];
        $description = $attributes['description'];
        $items = $attributes['items'];

        //验证
        if ($this->validateOperateScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items
        )) {
            //初始化命令
            $commandBus = $this->getCommandBus();
            $command = new AddGbTemplateCommand(
                $name,
                $identify,
                $subjectCategory,
                $dimension,
                $exchangeFrequency,
                $infoClassify,
                $infoCategory,
                $description,
                $items
            );

            //执行命令
            if ($commandBus->send($command)) {
                //获取最新数据
                $repository = $this->getRepository();
                $gbTemplate = $repository->fetchOne($command->id);
                if ($gbTemplate instanceof GbTemplate) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new GbTemplateView($gbTemplate));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 国标资源目录编辑功能,通过PATCH传参
     * 对应路由 /gbTemplates/{id:\d+}
     * @param int id 国标资源目录 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        
        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $subjectCategory = $attributes['subjectCategory'];
        $dimension = $attributes['dimension'];
        $exchangeFrequency = $attributes['exchangeFrequency'];
        $infoClassify = $attributes['infoClassify'];
        $infoCategory = $attributes['infoCategory'];
        $description = $attributes['description'];
        $items = $attributes['items'];

        //验证
        if ($this->validateOperateScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items
        )) {
            $commandBus = $this->getCommandBus();

            $command = new EditGbTemplateCommand(
                $name,
                $identify,
                $subjectCategory,
                $dimension,
                $exchangeFrequency,
                $infoClassify,
                $infoCategory,
                $description,
                $items,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $gbTemplate = $repository->fetchOne($id);
                if ($gbTemplate instanceof GbTemplate) {
                    $this->render(new GbTemplateView($gbTemplate));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateOperateScenario(
        $name,
        $identify,
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $description,
        $items
    ) : bool {
        return $this->getTemplateWidgetRule()->name($name)
        && $this->getTemplateWidgetRule()->identify($identify)
        && $this->getTemplateWidgetRule()->subjectCategory($subjectCategory)
        && $this->getTemplateWidgetRule()->dimension($dimension)
        && $this->getCommonWidgetRule()->formatNumeric($exchangeFrequency, 'exchangeFrequency')
        && $this->getCommonWidgetRule()->formatNumeric($infoClassify, 'infoClassify')
        && $this->getTemplateWidgetRule()->infoCategory($infoCategory)
        && $this->getTemplateWidgetRule()->description($description)
        && $this->getTemplateWidgetRule()->items($subjectCategory, $items);
    }
}
