<?php
namespace Base\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Template\WidgetRule\TemplateWidgetRule;
use Base\Template\Repository\QzjTemplateSdkRepository;
use Base\Template\CommandHandler\QzjTemplate\QzjTemplateCommandHandlerFactory;

trait QzjTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : QzjTemplateSdkRepository
    {
        return new QzjTemplateSdkRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new QzjTemplateCommandHandlerFactory());
    }

    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    protected function validateOperateScenario(
        $name,
        $identify,
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $description,
        $items,
        $category,
        $sourceUnit
    ) : bool {
        return $this->getTemplateWidgetRule()->name($name)
        && $this->getTemplateWidgetRule()->identify($identify)
        && $this->getTemplateWidgetRule()->subjectCategory($subjectCategory)
        && $this->getTemplateWidgetRule()->dimension($dimension)
        && $this->getCommonWidgetRule()->formatNumeric($infoClassify, 'infoClassify')
        && $this->getCommonWidgetRule()->formatNumeric($infoCategory, 'infoCategory')
        && $this->getCommonWidgetRule()->formatNumeric($exchangeFrequency, 'exchangeFrequency')
        && $this->getTemplateWidgetRule()->description($description)
        && $this->getTemplateWidgetRule()->items($subjectCategory, $items)
        && $this->getTemplateWidgetRule()->qzjCategory($category)
        && $this->getTemplateWidgetRule()->qzjSourceUnit($category, $sourceUnit);
    }
}
