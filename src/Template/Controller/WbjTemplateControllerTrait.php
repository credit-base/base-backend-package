<?php
namespace Base\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Template\WidgetRule\TemplateWidgetRule;
use Base\Template\Repository\WbjTemplateSdkRepository;
use Base\Template\CommandHandler\WbjTemplate\WbjTemplateCommandHandlerFactory;

trait WbjTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : WbjTemplateSdkRepository
    {
        return new WbjTemplateSdkRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new WbjTemplateCommandHandlerFactory());
    }
}
