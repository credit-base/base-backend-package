<?php
namespace Base\Template\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use Base\Template\Repository\GbTemplateSdkRepository;
use Base\Template\View\GbTemplateView;

class GbTemplateFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new GbTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IGbTemplateAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new GbTemplateView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'gbTemplates';
    }
}
