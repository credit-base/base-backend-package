<?php
namespace Base\Template\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\Template\View\BaseTemplateView;
use Base\Template\Repository\BaseTemplateSdkRepository;
use Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class BaseTemplateFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BaseTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IBaseTemplateAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new BaseTemplateView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'baseTemplates';
    }
}
