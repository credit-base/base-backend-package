<?php
namespace Base\Template\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\Template\Model\BaseTemplate;
use Base\Template\View\BaseTemplateView;
use Base\Template\Command\BaseTemplate\EditBaseTemplateCommand;

class BaseTemplateOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, BaseTemplateControllerTrait;

    public function add()
    {
        return false;
    }

    /**
     * 基础资源目录编辑功能,通过PATCH传参
     * 对应路由 /baseTemplates/{id:\d+}
     * @param int id 基础资源目录 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
    
        $subjectCategory = $attributes['subjectCategory'];
        $dimension = $attributes['dimension'];
        $exchangeFrequency = $attributes['exchangeFrequency'];
        $infoClassify = $attributes['infoClassify'];
        $infoCategory = $attributes['infoCategory'];
        $items = $attributes['items'];

        //验证
        if ($this->validateOperateScenario(
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $items
        )) {
            $command = new EditBaseTemplateCommand(
                $subjectCategory,
                $items,
                $dimension,
                $exchangeFrequency,
                $infoClassify,
                $infoCategory,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $baseTemplate = $this->getRepository()->fetchOne($id);
                if ($baseTemplate instanceof BaseTemplate) {
                    $this->render(new BaseTemplateView($baseTemplate));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
