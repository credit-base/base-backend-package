<?php
namespace Base\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Template\WidgetRule\TemplateWidgetRule;
use Base\Template\Repository\BaseTemplateSdkRepository;
use Base\Template\CommandHandler\BaseTemplate\BaseTemplateCommandHandlerFactory;

trait BaseTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }
    
    protected function getRepository() : BaseTemplateSdkRepository
    {
        return new BaseTemplateSdkRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new BaseTemplateCommandHandlerFactory());
    }

    protected function validateOperateScenario(
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $items
    ) : bool {
        return $this->getTemplateWidgetRule()->subjectCategory($subjectCategory)
        && $this->getTemplateWidgetRule()->dimension($dimension)
        && $this->getCommonWidgetRule()->formatNumeric($exchangeFrequency, 'exchangeFrequency')
        && $this->getCommonWidgetRule()->formatNumeric($infoClassify, 'infoClassify')
        && $this->getCommonWidgetRule()->formatNumeric($infoCategory, 'infoCategory')
        && $this->getTemplateWidgetRule()->items($subjectCategory, $items);
    }
}
