<?php
namespace Base\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Template\WidgetRule\TemplateWidgetRule;
use Base\Template\Repository\GbTemplateSdkRepository;
use Base\Template\CommandHandler\GbTemplate\GbTemplateCommandHandlerFactory;

trait GbTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : GbTemplateSdkRepository
    {
        return new GbTemplateSdkRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new GbTemplateCommandHandlerFactory());
    }
}
