<?php
namespace Base\Template\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use Base\Template\View\WbjTemplateView;
use Base\Template\Repository\WbjTemplateSdkRepository;

class WbjTemplateFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WbjTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IWbjTemplateAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new WbjTemplateView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'templates';
    }
}
