<?php
namespace Base\Template\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\Template\View\QzjTemplateView;
use Base\Template\Repository\QzjTemplateSdkRepository;
use Base\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;

class QzjTemplateFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new QzjTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IQzjTemplateAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new QzjTemplateView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'qzjTemplates';
    }
}
