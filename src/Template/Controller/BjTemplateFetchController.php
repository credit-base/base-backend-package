<?php
namespace Base\Template\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use Base\Template\View\BjTemplateView;
use Base\Template\Repository\BjTemplateSdkRepository;

class BjTemplateFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BjTemplateSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IBjTemplateAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new BjTemplateView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'bjTemplates';
    }
}
