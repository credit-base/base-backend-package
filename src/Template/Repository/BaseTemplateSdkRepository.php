<?php
namespace Base\Template\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Template\Model\BaseTemplate;
use Base\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;
use Base\Template\Adapter\BaseTemplate\BaseTemplateSdkAdapter;
use Base\Template\Adapter\BaseTemplate\BaseTemplateMockAdapter;

class BaseTemplateSdkRepository extends Repository implements IBaseTemplateAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new BaseTemplateSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IBaseTemplateAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IBaseTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IBaseTemplateAdapter
    {
        return new BaseTemplateMockAdapter();
    }

    public function fetchOne($id) : BaseTemplate
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($baseTemplate, $keys);
    }
}
