<?php
namespace Base\Template\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Template\Model\QzjTemplate;
use Base\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;
use Base\Template\Adapter\QzjTemplate\QzjTemplateSdkAdapter;
use Base\Template\Adapter\QzjTemplate\QzjTemplateMockAdapter;

class QzjTemplateSdkRepository extends Repository implements IQzjTemplateAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new QzjTemplateSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IQzjTemplateAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IQzjTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IQzjTemplateAdapter
    {
        return new QzjTemplateMockAdapter();
    }

    public function fetchOne($id) : QzjTemplate
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(QzjTemplate $qzjTemplate) : bool
    {
        return $this->getAdapter()->add($qzjTemplate);
    }

    public function edit(QzjTemplate $qzjTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($qzjTemplate, $keys);
    }
}
