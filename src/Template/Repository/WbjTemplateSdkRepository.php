<?php
namespace Base\Template\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Template\Model\WbjTemplate;
use Base\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use Base\Template\Adapter\WbjTemplate\WbjTemplateSdkAdapter;
use Base\Template\Adapter\WbjTemplate\WbjTemplateMockAdapter;

class WbjTemplateSdkRepository extends Repository implements IWbjTemplateAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new WbjTemplateSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IWbjTemplateAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IWbjTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IWbjTemplateAdapter
    {
        return new WbjTemplateMockAdapter();
    }

    public function fetchOne($id) : WbjTemplate
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(WbjTemplate $wbjTemplate) : bool
    {
        return $this->getAdapter()->add($wbjTemplate);
    }

    public function edit(WbjTemplate $wbjTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($wbjTemplate, $keys);
    }
}
