<?php
namespace Base\Template\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Template\Model\QzjTemplate;
use Base\Template\Model\NullQzjTemplate;

use BaseSdk\Template\Model\QzjTemplate as QzjTemplateSdk;
use BaseSdk\Template\Model\NullQzjTemplate as NullQzjTemplateSdk;

use Base\UserGroup\Translator\UserGroupSdkTranslator;

class QzjTemplateSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getUserGroupSdkTranslator() : ISdkTranslator
    {
        return new UserGroupSdkTranslator();
    }

    public function valueObjectToObject($qzjTemplateSdk = null, $qzjTemplate = null)
    {
        return $this->translateToObject($qzjTemplateSdk, $qzjTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($qzjTemplateSdk = null, $qzjTemplate = null) : QzjTemplate
    {
        if (!$qzjTemplateSdk instanceof QzjTemplateSdk || $qzjTemplateSdk instanceof INull) {
            return NullQzjTemplate::getInstance();
        }

        if ($qzjTemplate == null) {
            $qzjTemplate = new QzjTemplate();
        }

        $sourceUnit = $this->getUserGroupSdkTranslator()->valueObjectToObject($qzjTemplateSdk->getSourceUnit());

        $qzjTemplate->setId($qzjTemplateSdk->getId());
        $qzjTemplate->setName($qzjTemplateSdk->getName());
        $qzjTemplate->setIdentify($qzjTemplateSdk->getIdentify());
        $qzjTemplate->setSubjectCategory($qzjTemplateSdk->getSubjectCategory());
        $qzjTemplate->setDimension($qzjTemplateSdk->getDimension());
        $qzjTemplate->setExchangeFrequency($qzjTemplateSdk->getExchangeFrequency());
        $qzjTemplate->setInfoClassify($qzjTemplateSdk->getInfoClassify());
        $qzjTemplate->setInfoCategory($qzjTemplateSdk->getInfoCategory());
        $qzjTemplate->setDescription($qzjTemplateSdk->getDescription());
        $qzjTemplate->setItems($qzjTemplateSdk->getItems());
        $qzjTemplate->setCategory($qzjTemplateSdk->getCategory());
        $qzjTemplate->setCreateTime($qzjTemplateSdk->getCreateTime());
        $qzjTemplate->setStatusTime($qzjTemplateSdk->getStatusTime());
        $qzjTemplate->setUpdateTime($qzjTemplateSdk->getUpdateTime());
        $qzjTemplate->setStatus($qzjTemplateSdk->getStatus());
        $qzjTemplate->setSourceUnit($sourceUnit);

        return $qzjTemplate;
    }

    public function objectToValueObject($qzjTemplate)
    {
        if (!$qzjTemplate instanceof QzjTemplate) {
            return NullQzjTemplateSdk::getInstance();
        }

        $sourceUnitSdk = $this->getUserGroupSdkTranslator()->objectToValueObject($qzjTemplate->getSourceUnit());
        $qzjTemplateSdk = new QzjTemplateSdk(
            $qzjTemplate->getId(),
            $qzjTemplate->getName(),
            $qzjTemplate->getIdentify(),
            $qzjTemplate->getCategory(),
            $qzjTemplate->getSubjectCategory(),
            $qzjTemplate->getDimension(),
            $qzjTemplate->getExchangeFrequency(),
            $qzjTemplate->getInfoClassify(),
            $qzjTemplate->getInfoCategory(),
            $qzjTemplate->getDescription(),
            $qzjTemplate->getItems(),
            $sourceUnitSdk,
            $qzjTemplate->getStatus(),
            $qzjTemplate->getStatusTime(),
            $qzjTemplate->getCreateTime(),
            $qzjTemplate->getUpdateTime()
        );

        return $qzjTemplateSdk;
    }
}
