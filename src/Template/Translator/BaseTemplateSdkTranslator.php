<?php
namespace Base\Template\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Template\Model\BaseTemplate;
use Base\Template\Model\NullBaseTemplate;

use BaseSdk\Template\Model\BaseTemplate as BaseTemplateSdk;
use BaseSdk\Template\Model\NullBaseTemplate as NullBaseTemplateSdk;

class BaseTemplateSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($baseTemplateSdk = null, $baseTemplate = null)
    {
        return $this->translateToObject($baseTemplateSdk, $baseTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($baseTemplateSdk = null, $baseTemplate = null) : BaseTemplate
    {
        if (!$baseTemplateSdk instanceof BaseTemplateSdk || $baseTemplateSdk instanceof INull) {
            return NullBaseTemplate::getInstance();
        }

        if ($baseTemplate == null) {
            $baseTemplate = new BaseTemplate();
        }

        $baseTemplate->setId($baseTemplateSdk->getId());
        $baseTemplate->setName($baseTemplateSdk->getName());
        $baseTemplate->setIdentify($baseTemplateSdk->getIdentify());
        $baseTemplate->setSubjectCategory($baseTemplateSdk->getSubjectCategory());
        $baseTemplate->setDimension($baseTemplateSdk->getDimension());
        $baseTemplate->setExchangeFrequency($baseTemplateSdk->getExchangeFrequency());
        $baseTemplate->setInfoClassify($baseTemplateSdk->getInfoClassify());
        $baseTemplate->setInfoCategory($baseTemplateSdk->getInfoCategory());
        $baseTemplate->setDescription($baseTemplateSdk->getDescription());
        $baseTemplate->setItems($baseTemplateSdk->getItems());
        $baseTemplate->setCreateTime($baseTemplateSdk->getCreateTime());
        $baseTemplate->setStatusTime($baseTemplateSdk->getStatusTime());
        $baseTemplate->setUpdateTime($baseTemplateSdk->getUpdateTime());
        $baseTemplate->setStatus($baseTemplateSdk->getStatus());
        
        return $baseTemplate;
    }

    public function objectToValueObject($baseTemplate)
    {
        if (!$baseTemplate instanceof BaseTemplate) {
            return NullBaseTemplateSdk::getInstance();
        }

        $baseTemplateSdk = new BaseTemplateSdk(
            $baseTemplate->getId(),
            $baseTemplate->getName(),
            $baseTemplate->getIdentify(),
            $baseTemplate->getCategory(),
            $baseTemplate->getSubjectCategory(),
            $baseTemplate->getDimension(),
            $baseTemplate->getExchangeFrequency(),
            $baseTemplate->getInfoClassify(),
            $baseTemplate->getInfoCategory(),
            $baseTemplate->getDescription(),
            $baseTemplate->getItems(),
            $baseTemplate->getStatus(),
            $baseTemplate->getStatusTime(),
            $baseTemplate->getCreateTime(),
            $baseTemplate->getUpdateTime()
        );

        return $baseTemplateSdk;
    }
}
