<?php
namespace Base\Template\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Template\Model\BjTemplate;
use Base\Template\Model\NullBjTemplate;

use BaseSdk\Template\Model\BjTemplate as BjTemplateSdk;
use BaseSdk\Template\Model\NullBjTemplate as NullBjTemplateSdk;

use Base\UserGroup\Translator\UserGroupSdkTranslator;

class BjTemplateSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getUserGroupSdkTranslator() : ISdkTranslator
    {
        return new UserGroupSdkTranslator();
    }

    protected function getGbTemplateSdkTranslator() : ISdkTranslator
    {
        return new GbTemplateSdkTranslator();
    }

    public function valueObjectToObject($bjTemplateSdk = null, $bjTemplate = null)
    {
        return $this->translateToObject($bjTemplateSdk, $bjTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($bjTemplateSdk = null, $bjTemplate = null) : BjTemplate
    {
        if (!$bjTemplateSdk instanceof BjTemplateSdk || $bjTemplateSdk instanceof INull) {
            return NullBjTemplate::getInstance();
        }

        if ($bjTemplate == null) {
            $bjTemplate = new BjTemplate();
        }

        $sourceUnit = $this->getUserGroupSdkTranslator()->valueObjectToObject($bjTemplateSdk->getSourceUnit());
        $gbTemplate = $this->getGbTemplateSdkTranslator()->valueObjectToObject($bjTemplateSdk->getGbTemplate());

        $bjTemplate->setId($bjTemplateSdk->getId());
        $bjTemplate->setName($bjTemplateSdk->getName());
        $bjTemplate->setIdentify($bjTemplateSdk->getIdentify());
        $bjTemplate->setSubjectCategory($bjTemplateSdk->getSubjectCategory());
        $bjTemplate->setDimension($bjTemplateSdk->getDimension());
        $bjTemplate->setExchangeFrequency($bjTemplateSdk->getExchangeFrequency());
        $bjTemplate->setInfoClassify($bjTemplateSdk->getInfoClassify());
        $bjTemplate->setInfoCategory($bjTemplateSdk->getInfoCategory());
        $bjTemplate->setDescription($bjTemplateSdk->getDescription());
        $bjTemplate->setItems($bjTemplateSdk->getItems());
        $bjTemplate->setCreateTime($bjTemplateSdk->getCreateTime());
        $bjTemplate->setStatusTime($bjTemplateSdk->getStatusTime());
        $bjTemplate->setUpdateTime($bjTemplateSdk->getUpdateTime());
        $bjTemplate->setStatus($bjTemplateSdk->getStatus());
        $bjTemplate->setSourceUnit($sourceUnit);
        $bjTemplate->setGbTemplate($gbTemplate);

        return $bjTemplate;
    }

    public function objectToValueObject($bjTemplate)
    {
        if (!$bjTemplate instanceof BjTemplate) {
            return NullBjTemplateSdk::getInstance();
        }

        $sourceUnitSdk = $this->getUserGroupSdkTranslator()->objectToValueObject($bjTemplate->getSourceUnit());
        $gbTemplateSdk = $this->getGbTemplateSdkTranslator()->objectToValueObject($bjTemplate->getGbTemplate());
        $bjTemplateSdk = new BjTemplateSdk(
            $bjTemplate->getId(),
            $bjTemplate->getName(),
            $bjTemplate->getIdentify(),
            $bjTemplate->getCategory(),
            $bjTemplate->getSubjectCategory(),
            $bjTemplate->getDimension(),
            $bjTemplate->getExchangeFrequency(),
            $bjTemplate->getInfoClassify(),
            $bjTemplate->getInfoCategory(),
            $bjTemplate->getDescription(),
            $bjTemplate->getItems(),
            $sourceUnitSdk,
            $gbTemplateSdk,
            $bjTemplate->getStatus(),
            $bjTemplate->getStatusTime(),
            $bjTemplate->getCreateTime(),
            $bjTemplate->getUpdateTime()
        );

        return $bjTemplateSdk;
    }
}
