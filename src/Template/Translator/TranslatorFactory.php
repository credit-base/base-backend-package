<?php
namespace Base\Template\Translator;

use Base\Template\Model\Template;

use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\NullSdkTranslator;

class TranslatorFactory
{
    const MAPS = array(
        Template::CATEGORY['BJ'] =>
        'Base\Template\Translator\BjTemplateSdkTranslator',
        Template::CATEGORY['GB'] =>
        'Base\Template\Translator\GbTemplateSdkTranslator',
        Template::CATEGORY['BASE'] =>
        'Base\Template\Translator\BaseTemplateSdkTranslator',
        Template::CATEGORY['WBJ'] =>
        'Base\Template\Translator\WbjTemplateSdkTranslator',
        Template::CATEGORY['QZJ_WBJ'] =>
        'Base\Template\Translator\QzjTemplateSdkTranslator',
        Template::CATEGORY['QZJ_BJ'] =>
        'Base\Template\Translator\QzjTemplateSdkTranslator',
        Template::CATEGORY['QZJ_GB'] =>
        'Base\Template\Translator\QzjTemplateSdkTranslator',
    );

    public function getTranslator(int $category) : ISdkTranslator
    {
        $translator = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($translator) ? new $translator : NullSdkTranslator::getInstance();
    }
}
