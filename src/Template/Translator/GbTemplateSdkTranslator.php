<?php
namespace Base\Template\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Template\Model\GbTemplate;
use Base\Template\Model\NullGbTemplate;

use BaseSdk\Template\Model\GbTemplate as GbTemplateSdk;
use BaseSdk\Template\Model\NullGbTemplate as NullGbTemplateSdk;

class GbTemplateSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($gbTemplateSdk = null, $gbTemplate = null)
    {
        return $this->translateToObject($gbTemplateSdk, $gbTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($gbTemplateSdk = null, $gbTemplate = null) : GbTemplate
    {
        if (!$gbTemplateSdk instanceof GbTemplateSdk || $gbTemplateSdk instanceof INull) {
            return NullGbTemplate::getInstance();
        }

        if ($gbTemplate == null) {
            $gbTemplate = new GbTemplate();
        }

        $gbTemplate->setId($gbTemplateSdk->getId());
        $gbTemplate->setName($gbTemplateSdk->getName());
        $gbTemplate->setIdentify($gbTemplateSdk->getIdentify());
        $gbTemplate->setSubjectCategory($gbTemplateSdk->getSubjectCategory());
        $gbTemplate->setDimension($gbTemplateSdk->getDimension());
        $gbTemplate->setExchangeFrequency($gbTemplateSdk->getExchangeFrequency());
        $gbTemplate->setInfoClassify($gbTemplateSdk->getInfoClassify());
        $gbTemplate->setInfoCategory($gbTemplateSdk->getInfoCategory());
        $gbTemplate->setDescription($gbTemplateSdk->getDescription());
        $gbTemplate->setItems($gbTemplateSdk->getItems());
        $gbTemplate->setCreateTime($gbTemplateSdk->getCreateTime());
        $gbTemplate->setStatusTime($gbTemplateSdk->getStatusTime());
        $gbTemplate->setUpdateTime($gbTemplateSdk->getUpdateTime());
        $gbTemplate->setStatus($gbTemplateSdk->getStatus());

        return $gbTemplate;
    }

    public function objectToValueObject($gbTemplate)
    {
        if (!$gbTemplate instanceof GbTemplate) {
            return NullGbTemplateSdk::getInstance();
        }

        $gbTemplateSdk = new GbTemplateSdk(
            $gbTemplate->getId(),
            $gbTemplate->getName(),
            $gbTemplate->getIdentify(),
            $gbTemplate->getCategory(),
            $gbTemplate->getSubjectCategory(),
            $gbTemplate->getDimension(),
            $gbTemplate->getExchangeFrequency(),
            $gbTemplate->getInfoClassify(),
            $gbTemplate->getInfoCategory(),
            $gbTemplate->getDescription(),
            $gbTemplate->getItems(),
            $gbTemplate->getStatus(),
            $gbTemplate->getStatusTime(),
            $gbTemplate->getCreateTime(),
            $gbTemplate->getUpdateTime()
        );

        return $gbTemplateSdk;
    }
}
