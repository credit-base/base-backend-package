<?php
namespace Base\Template\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Template\Model\WbjTemplate;
use Base\Template\Model\NullWbjTemplate;

use BaseSdk\Template\Model\WbjTemplate as WbjTemplateSdk;
use BaseSdk\Template\Model\NullWbjTemplate as NullWbjTemplateSdk;

use Base\UserGroup\Translator\UserGroupSdkTranslator;

class WbjTemplateSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getUserGroupSdkTranslator() : ISdkTranslator
    {
        return new UserGroupSdkTranslator();
    }

    public function valueObjectToObject($wbjTemplateSdk = null, $wbjTemplate = null)
    {
        return $this->translateToObject($wbjTemplateSdk, $wbjTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($wbjTemplateSdk = null, $wbjTemplate = null) : WbjTemplate
    {
        if (!$wbjTemplateSdk instanceof WbjTemplateSdk || $wbjTemplateSdk instanceof INull) {
            return NullWbjTemplate::getInstance();
        }

        if ($wbjTemplate == null) {
            $wbjTemplate = new WbjTemplate();
        }

        $sourceUnit = $this->getUserGroupSdkTranslator()->valueObjectToObject($wbjTemplateSdk->getSourceUnit());

        $wbjTemplate->setId($wbjTemplateSdk->getId());
        $wbjTemplate->setName($wbjTemplateSdk->getName());
        $wbjTemplate->setIdentify($wbjTemplateSdk->getIdentify());
        $wbjTemplate->setSubjectCategory($wbjTemplateSdk->getSubjectCategory());
        $wbjTemplate->setDimension($wbjTemplateSdk->getDimension());
        $wbjTemplate->setExchangeFrequency($wbjTemplateSdk->getExchangeFrequency());
        $wbjTemplate->setInfoClassify($wbjTemplateSdk->getInfoClassify());
        $wbjTemplate->setInfoCategory($wbjTemplateSdk->getInfoCategory());
        $wbjTemplate->setDescription($wbjTemplateSdk->getDescription());
        $wbjTemplate->setItems($wbjTemplateSdk->getItems());
        $wbjTemplate->setCreateTime($wbjTemplateSdk->getCreateTime());
        $wbjTemplate->setStatusTime($wbjTemplateSdk->getStatusTime());
        $wbjTemplate->setUpdateTime($wbjTemplateSdk->getUpdateTime());
        $wbjTemplate->setStatus($wbjTemplateSdk->getStatus());
        $wbjTemplate->setSourceUnit($sourceUnit);

        return $wbjTemplate;
    }

    public function objectToValueObject($wbjTemplate)
    {
        if (!$wbjTemplate instanceof WbjTemplate) {
            return NullWbjTemplateSdk::getInstance();
        }

        $sourceUnitSdk = $this->getUserGroupSdkTranslator()->objectToValueObject($wbjTemplate->getSourceUnit());
        $wbjTemplateSdk = new WbjTemplateSdk(
            $wbjTemplate->getId(),
            $wbjTemplate->getName(),
            $wbjTemplate->getIdentify(),
            $wbjTemplate->getCategory(),
            $wbjTemplate->getSubjectCategory(),
            $wbjTemplate->getDimension(),
            $wbjTemplate->getExchangeFrequency(),
            $wbjTemplate->getInfoClassify(),
            $wbjTemplate->getInfoCategory(),
            $wbjTemplate->getDescription(),
            $wbjTemplate->getItems(),
            $sourceUnitSdk,
            $wbjTemplate->getStatus(),
            $wbjTemplate->getStatusTime(),
            $wbjTemplate->getCreateTime(),
            $wbjTemplate->getUpdateTime()
        );

        return $wbjTemplateSdk;
    }
}
