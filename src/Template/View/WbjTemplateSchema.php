<?php
namespace Base\Template\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class WbjTemplateSchema extends SchemaProvider
{
    protected $resourceType = 'templates';

    public function getId($wbjTemplate) : int
    {
        return $wbjTemplate->getId();
    }

    public function getAttributes($wbjTemplate) : array
    {
        return [
            'name' => $wbjTemplate->getName(),
            'identify' => $wbjTemplate->getIdentify(),
            'subjectCategory' => $wbjTemplate->getSubjectCategory(),
            'dimension' => $wbjTemplate->getDimension(),
            'exchangeFrequency' => $wbjTemplate->getExchangeFrequency(),
            'infoClassify' => $wbjTemplate->getInfoClassify(),
            'infoCategory' => $wbjTemplate->getInfoCategory(),
            'description' => $wbjTemplate->getDescription(),
            'category' => $wbjTemplate->getCategory(),
            'items' => $wbjTemplate->getItems(),

            'status' => $wbjTemplate->getStatus(),
            'createTime' => $wbjTemplate->getCreateTime(),
            'updateTime' => $wbjTemplate->getUpdateTime(),
            'statusTime' => $wbjTemplate->getStatusTime(),
        ];
    }

    public function getRelationships($wbjTemplate, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'sourceUnit' => [self::DATA => $wbjTemplate->getSourceUnit()]
        ];
    }
}
