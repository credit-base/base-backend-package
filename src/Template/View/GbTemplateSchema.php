<?php
namespace Base\Template\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class GbTemplateSchema extends SchemaProvider
{
    protected $resourceType = 'gbTemplates';

    public function getId($gbTemplate) : int
    {
        return $gbTemplate->getId();
    }

    public function getAttributes($gbTemplate) : array
    {
        return [
            'name' => $gbTemplate->getName(),
            'identify' => $gbTemplate->getIdentify(),
            'subjectCategory' => $gbTemplate->getSubjectCategory(),
            'dimension' => $gbTemplate->getDimension(),
            'exchangeFrequency' => $gbTemplate->getExchangeFrequency(),
            'infoClassify' => $gbTemplate->getInfoClassify(),
            'infoCategory' => $gbTemplate->getInfoCategory(),
            'description' => $gbTemplate->getDescription(),
            'category' => $gbTemplate->getCategory(),
            'items' => $gbTemplate->getItems(),

            'status' => $gbTemplate->getStatus(),
            'createTime' => $gbTemplate->getCreateTime(),
            'updateTime' => $gbTemplate->getUpdateTime(),
            'statusTime' => $gbTemplate->getStatusTime(),
        ];
    }

    public function getRelationships($gbTemplate, $isPrimary, array $includeList)
    {
        unset($gbTemplate);
        unset($isPrimary);
        unset($includeList);
        
        return [
        ];
    }
}
