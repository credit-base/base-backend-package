<?php
namespace Base\Template\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class BjTemplateSchema extends SchemaProvider
{
    protected $resourceType = 'bjTemplates';

    public function getId($bjTemplate) : int
    {
        return $bjTemplate->getId();
    }

    public function getAttributes($bjTemplate) : array
    {
        return [
            'name' => $bjTemplate->getName(),
            'identify' => $bjTemplate->getIdentify(),
            'subjectCategory' => $bjTemplate->getSubjectCategory(),
            'dimension' => $bjTemplate->getDimension(),
            'exchangeFrequency' => $bjTemplate->getExchangeFrequency(),
            'infoClassify' => $bjTemplate->getInfoClassify(),
            'infoCategory' => $bjTemplate->getInfoCategory(),
            'description' => $bjTemplate->getDescription(),
            'category' => $bjTemplate->getCategory(),
            'items' => $bjTemplate->getItems(),

            'status' => $bjTemplate->getStatus(),
            'createTime' => $bjTemplate->getCreateTime(),
            'updateTime' => $bjTemplate->getUpdateTime(),
            'statusTime' => $bjTemplate->getStatusTime(),
        ];
    }

    public function getRelationships($bjTemplate, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'sourceUnit' => [self::DATA => $bjTemplate->getSourceUnit()],
            'gbTemplate' => [self::DATA => $bjTemplate->getGbTemplate()]
        ];
    }
}
