<?php
namespace Base\User\WidgetRule;

use Respect\Validation\Validator as V;

use Marmot\Core;

class UserWidgetRule
{
    const REAL_NAME_MIN_LENGTH = 2;
    const REAL_NAME_MAX_LENGTH = 15;

    public function realName($realName) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::REAL_NAME_MIN_LENGTH,
            self::REAL_NAME_MAX_LENGTH
        )->validate($realName)) {
            Core::setLastError(USER_REAL_NAME_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    public function cellphone($cellphone) : bool
    {
        $reg = '/^[1][0-9]{10}$/';
        if (!preg_match($reg, $cellphone)) {
            Core::setLastError(USER_CELLPHONE_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    public function password($password, $pointer = '') : bool
    {
        $reg = '/^(?=.*\d)(?=.*[a-zA-Z])(?=.*[~!@#$%^*.()_+-={}:;?,])[\da-zA-Z~!@#$%^*.()_+-={}:;?,]{8,20}$/';

        if (!preg_match($reg, $password)) {
            Core::setLastError(USER_PASSWORD_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    public function cardId($cardId) : bool
    {
        $reg = "/(^\d{15}$)|(^\d{18}$)|(^\d{17}(^[0-9]|X))$/";
        if (!V::alnum()->noWhitespace()->length(15, 15)->validate($cardId)
            && !V::alnum()->noWhitespace()->length(18, 18)->validate($cardId)) {
            Core::setLastError(USER_CARDID_FORMAT_ERROR);
            return false;
        }

        if (!preg_match($reg, $cardId)) {
            Core::setLastError(USER_CARDID_FORMAT_ERROR);
            return false;
        }
      
        return true;
    }
}
