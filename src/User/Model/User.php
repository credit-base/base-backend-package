<?php
namespace Base\User\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Common\Model\IEnableAble;
use Base\Common\Model\EnableAbleTrait;

abstract class User implements IObject, IEnableAble
{
    const SALT_LENGTH = 4;
    const SALT_BASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';

    use Object, EnableAbleTrait;
    /**
     * @var int $id
     */
    protected $id;
    /**
     * @var string $userName 账户
     */
    protected $userName;
    /**
     * @var string $cellphone 手机号码
     */
    protected $cellphone;
    /**
     * @var string $password 密码
     */
    protected $password;
    /**
     * @var string $salt 密码的盐
     */
    protected $salt;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->userName = '';
        $this->cellphone = '';
        $this->password = '';
        $this->salt = '';
        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->userName);
        unset($this->cellphone);
        unset($this->password);
        unset($this->salt);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setUserName(string $userName) : void
    {
        $this->userName = $userName;
    }

    public function getUserName() : string
    {
        return $this->userName;
    }

    public function setCellphone(string $cellphone) : void
    {
        $this->cellphone = is_numeric($cellphone) ? $cellphone : '';
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setPassword(string $password) : void
    {
        $this->password = $password;
    }

    public function encryptPassword(string $password, string $salt = '') : void
    {
        //没有盐,自动生成盐
        $this->salt = empty($salt) ? $this->generateSalt() : $salt;
        $this->password = md5(md5($password).$this->salt);
    }

    private function generateSalt() : string
    {
        $salt = '';
        $max = strlen(self::SALT_BASE)-1;
        
        for ($i=0; $i<self::SALT_LENGTH; $i++) {
            $salt.=self::SALT_BASE[rand(0, $max)];
        }
        return $salt;
    }
    
    public function getPassword() : string
    {
        return $this->password;
    }

    public function setSalt(string $salt) : void
    {
        $this->salt = $salt;
    }

    public function getSalt() : string
    {
        return $this->salt;
    }

    public function changePassword(string $oldPassword, string $newPassword) : bool
    {
        return $this->verifyPassword($oldPassword) && $this->updatePassword($newPassword);
    }

    public function resetPassword(string $password) : bool
    {
        return $this->updatePassword($password);
    }

    abstract protected function verifyPassword(string $oldPassword) : bool;

    abstract protected function updatePassword(string $newPassword) : bool;
}
