<?php
namespace Base\Journal\Command\Journal;

use Base\ApplyForm\Command\ApplyForm\AddApplyFormCommand;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class AddJournalCommand extends AddApplyFormCommand
{
    use JournalCommandTrait;

    public function __construct(
        string $title,
        string $source,
        string $description,
        array $cover,
        array $attachment,
        array $authImages,
        int $year,
        int $status,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $crew,
            $id
        );
        
        $this->title = $title;
        $this->source = $source;
        $this->description = $description;
        $this->cover = $cover;
        $this->attachment = $attachment;
        $this->authImages = $authImages;
        $this->year = $year;
        $this->status = $status;
    }
}
