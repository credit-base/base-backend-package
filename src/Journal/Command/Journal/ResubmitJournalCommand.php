<?php
namespace Base\Journal\Command\Journal;

use Base\Common\Command\ResubmitCommand;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ResubmitJournalCommand extends ResubmitCommand
{
    use JournalCommandTrait;

    public $crew;

    public function __construct(
        string $title,
        string $source,
        string $description,
        array $cover,
        array $attachment,
        array $authImages,
        int $year,
        int $status,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        
        $this->title = $title;
        $this->source = $source;
        $this->description = $description;
        $this->cover = $cover;
        $this->attachment = $attachment;
        $this->authImages = $authImages;
        $this->year = $year;
        $this->status = $status;
        $this->crew = $crew;
    }
}
