<?php
namespace Base\Journal\Command\Journal;

trait JournalCommandTrait
{
    public $title;

    public $source;

    public $description;

    public $cover;

    public $attachment;

    public $authImages;

    public $year;

    public $status;
}
