<?php
namespace Base\Journal\Command\Journal;

use Base\ApplyForm\Command\ApplyForm\EnableApplyFormCommand;

class EnableJournalCommand extends EnableApplyFormCommand
{
    public $journalId;

    public function __construct(
        int $journalId,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $crew,
            $id
        );
        
        $this->journalId = $journalId;
    }
}
