<?php
namespace Base\Journal\Command\Journal;

use Base\Common\Command\DisableCommand;

class DisableJournalCommand extends DisableCommand
{
    public $crew;

    public function __construct(
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        
        $this->crew = $crew;
    }
}
