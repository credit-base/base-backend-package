<?php
namespace Base\Journal\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\Journal\WidgetRule\JournalWidgetRule;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Repository\UnAuditedJournalRepository;
use Base\Journal\CommandHandler\Journal\JournalCommandHandlerFactory;

trait JournalControllerTrait
{
    protected function getJournalWidgetRule() : JournalWidgetRule
    {
        return new JournalWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : JournalRepository
    {
        return new JournalRepository();
    }

    protected function getUnAuditedJournalRepository() : UnAuditedJournalRepository
    {
        return new UnAuditedJournalRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new JournalCommandHandlerFactory());
    }

    protected function validateCommonScenario(
        $crew
    ) {
        return $this->getCommonWidgetRule()->formatNumeric($crew, 'crewId');
    }
    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    protected function validateJournalScenario(
        $title,
        $source,
        $description,
        $cover,
        $attachment,
        $authImages,
        $year,
        $status,
        $crew
    ) {
        return $this->getCommonWidgetRule()->title($title)
            && $this->getCommonWidgetRule()->source($source)
            && $this->getCommonWidgetRule()->description($description)
            && $this->getCommonWidgetRule()->image($cover, 'cover')
            && $this->getJournalWidgetRule()->attachment($attachment)
            && (empty($authImages) ? true : $this->getJournalWidgetRule()->authImages($authImages))
            && $this->getJournalWidgetRule()->year($year)
            && $this->getCommonWidgetRule()->status($status)
            && $this->getCommonWidgetRule()->formatNumeric($crew, 'crewId');
    }
}
