<?php
namespace Base\Journal\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Journal\View\JournalView;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Adapter\Journal\IJournalAdapter;

class JournalFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new JournalRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IJournalAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new JournalView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'journals';
    }
}
