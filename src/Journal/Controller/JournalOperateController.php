<?php
namespace Base\Journal\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\View\UnAuditedJournalView;

use Base\Journal\Command\Journal\AddJournalCommand;
use Base\Journal\Command\Journal\EditJournalCommand;

class JournalOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, JournalControllerTrait;
    
    /**
     * 信用刊物新增功能,通过POST传参
     * 对应路由 /journals
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $description = $attributes['description'];
        $cover = $attributes['cover'];
        $attachment = $attributes['attachment'];
        $authImages = $attributes['authImages'];
        $status = $attributes['status'];
        $year = $attributes['year'];

        $crew = $relationships['crew']['data'][0]['id'];

        //验证
        if ($this->validateJournalScenario(
            $title,
            $source,
            $description,
            $cover,
            $attachment,
            $authImages,
            $year,
            $status,
            $crew
        )) {
            //初始化命令
            $command = new AddJournalCommand(
                $title,
                $source,
                $description,
                $cover,
                $attachment,
                $authImages,
                $year,
                $status,
                $crew
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $journal = $this->getUnAuditedJournalRepository()->fetchOne($command->id);
                if ($journal instanceof UnAuditedJournal) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new UnAuditedJournalView($journal));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 信用刊物编辑功能,通过PATCH传参
     * 对应路由 /journals/{id:\d+}
     * @param int id 信用刊物 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $source = $attributes['source'];
        $description = $attributes['description'];
        $cover = $attributes['cover'];
        $attachment = $attributes['attachment'];
        $authImages = $attributes['authImages'];
        $status = $attributes['status'];
        $year = $attributes['year'];

        $crew = $relationships['crew']['data'][0]['id'];
        
        if ($this->validateJournalScenario(
            $title,
            $source,
            $description,
            $cover,
            $attachment,
            $authImages,
            $year,
            $status,
            $crew
        )) {
            $command = new EditJournalCommand(
                $title,
                $source,
                $description,
                $cover,
                $attachment,
                $authImages,
                $year,
                $status,
                $id,
                $crew
            );

            if ($this->getCommandBus()->send($command)) {
                $journal = $this->getUnAuditedJournalRepository()->fetchOne($command->id);
                if ($journal instanceof UnAuditedJournal) {
                    $this->render(new UnAuditedJournalView($journal));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
