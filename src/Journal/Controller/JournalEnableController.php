<?php
namespace Base\Journal\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Common\Controller\Interfaces\IEnableAbleController;

use Base\Journal\Model\Journal;
use Base\Journal\View\JournalView;
use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\View\UnAuditedJournalView;
use Base\Journal\Command\Journal\EnableJournalCommand;
use Base\Journal\Command\Journal\DisableJournalCommand;

class JournalEnableController extends Controller implements IEnableAbleController
{
    use JsonApiTrait, JournalControllerTrait;
    
    /**
     * 对应路由 /journal/{id:\d+}/enable
     * 刊物类启用功能,通过PATCH传参
     * @param $id 信用刊物id
     * @param jsonApi array("data"=>array(
     *                               "type"=>"journal",
     *                               "relationships"=>array(
     *                                   "crew"=>array(
     *                                       "data"=>array(
     *                                           array("type"=>"crews","id"=>发布人id)
     *                                       )
     *                                   )
     *                               )
     *                           )
     *                   )
     * @return jsonApi
     */
    public function enable(int $id)
    {
        if (!empty($id)) {
            $data = $this->getRequest()->patch('data');
            $relationships = $data['relationships'];

            $crew = $relationships['crew']['data'][0]['id'];
            
            if ($this->validateCommonScenario($crew)) {
                $command = new EnableJournalCommand($id, $crew);

                if ($this->getCommandBus()->send($command)) {
                    $journal  = $this->getUnAuditedJournalRepository()->fetchOne($command->id);
                    if ($journal instanceof UnAuditedJournal) {
                        $this->render(new UnAuditedJournalView($journal));
                        return true;
                    }
                }
            }
        }
        
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /journal/{id:\d+}/disable
     * 刊物类禁用功能,通过PATCH传参
     * @param $id 信用刊物id
     * @param jsonApi array("data"=>array(
     *                               "type"=>"journal",
     *                               "relationships"=>array(
     *                                   "crew"=>array(
     *                                       "data"=>array(
     *                                           array("type"=>"crews","id"=>发布人id)
     *                                       )
     *                                   )
     *                               )
     *                           )
     *                   )
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $data = $this->getRequest()->patch('data');
            $relationships = $data['relationships'];

            $crew = $relationships['crew']['data'][0]['id'];

            if ($this->validateCommonScenario($crew)) {
                $command = new DisableJournalCommand($crew, $id);

                if ($this->getCommandBus()->send($command)) {
                    $journal  = $this->getRepository()->fetchOne($id);
                    if ($journal instanceof Journal) {
                        $this->render(new JournalView($journal));
                        return true;
                    }
                }
            }
        }
        
        $this->displayError();
        return false;
    }
}
