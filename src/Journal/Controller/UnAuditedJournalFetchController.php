<?php
namespace Base\Journal\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\View\UnAuditedJournalView;
use Base\Journal\Repository\UnAuditedJournalRepository;

class UnAuditedJournalFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditedJournalRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : UnAuditedJournalRepository
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new UnAuditedJournalView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'unAuditedJournals';
    }

    /**
     * 根据检索条件查询数据,通过GET传参
     * 对应路由 /unAuditedJournals
     * @return jsonApi
     */
    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();
        $filter['applyInfoCategory'] = UnAuditedJournal::APPLY_JOURNAL_CATEGORY;
        $filter['applyInfoType'] = UnAuditedJournal::APPLY_JOURNAL_TYPE;

        list($objectList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = $this->generateView($objectList);
            $view->pagination(
                $this->getResourceName(),
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
