<?php
namespace Base\Journal\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Common\Controller\Interfaces\IResubmitAbleController;

use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\View\UnAuditedJournalView;
use Base\Journal\Command\Journal\ResubmitJournalCommand;

class JournalResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, JournalControllerTrait;

    /**
     * 对应路由 /unAuditedJournal/{id:\d+}/resubmit
     * 重新提交, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function resubmit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        
        $title = $attributes['title'];
        $source = $attributes['source'];
        $description = $attributes['description'];
        $cover = $attributes['cover'];
        $attachment = $attributes['attachment'];
        $authImages = $attributes['authImages'];
        $status = $attributes['status'];
        $year = $attributes['year'];

        $crew = $relationships['crew']['data'][0]['id'];
        
        if ($this->validateJournalScenario(
            $title,
            $source,
            $description,
            $cover,
            $attachment,
            $authImages,
            $year,
            $status,
            $crew
        )) {
            $command = new ResubmitJournalCommand(
                $title,
                $source,
                $description,
                $cover,
                $attachment,
                $authImages,
                $year,
                $status,
                $crew,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $unAuditedJournal = $this->getUnAuditedJournalRepository()->fetchOne($id);
                if ($unAuditedJournal instanceof UnAuditedJournal) {
                    $this->render(new UnAuditedJournalView($unAuditedJournal));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
