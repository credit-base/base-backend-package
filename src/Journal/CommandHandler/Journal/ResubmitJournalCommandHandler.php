<?php
namespace Base\Journal\CommandHandler\Journal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Journal\Command\Journal\ResubmitJournalCommand;

class ResubmitJournalCommandHandler implements ICommandHandler
{
    use JournalCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitJournalCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedJournal = $this->fetchUnAuditedJournal($command->id);

        $unAuditedJournal = $this->executeAction($command, $unAuditedJournal);
        $applyInfo = $this->applyInfo($unAuditedJournal);
        $unAuditedJournal->setApplyInfo($applyInfo);

        return $unAuditedJournal->resubmit();
    }
}
