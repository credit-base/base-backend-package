<?php
namespace Base\Journal\CommandHandler\Journal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class JournalCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\Journal\Command\Journal\AddJournalCommand' =>
        'Base\Journal\CommandHandler\Journal\AddJournalCommandHandler',
        'Base\Journal\Command\Journal\EditJournalCommand' =>
        'Base\Journal\CommandHandler\Journal\EditJournalCommandHandler',
        'Base\Journal\Command\Journal\EnableJournalCommand' =>
        'Base\Journal\CommandHandler\Journal\EnableJournalCommandHandler',
        'Base\Journal\Command\Journal\DisableJournalCommand' =>
        'Base\Journal\CommandHandler\Journal\DisableJournalCommandHandler',
        'Base\Journal\Command\Journal\ResubmitJournalCommand' =>
        'Base\Journal\CommandHandler\Journal\ResubmitJournalCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
