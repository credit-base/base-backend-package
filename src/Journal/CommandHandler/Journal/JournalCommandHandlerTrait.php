<?php
namespace Base\Journal\CommandHandler\Journal;

use Marmot\Interfaces\ICommand;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\Journal\Model\Journal;
use Base\Journal\Model\UnAuditedJournal;
use Base\Journal\Repository\JournalRepository;
use Base\Journal\Translator\JournalDbTranslator;
use Base\Journal\Repository\UnAuditedJournalRepository;

trait JournalCommandHandlerTrait
{
    protected function getCrewRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    protected function getJournalRepository() : JournalRepository
    {
        return new JournalRepository();
    }

    protected function getJournalDbTranslator() : JournalDbTranslator
    {
        return new JournalDbTranslator();
    }

    protected function fetchJournal(int $id) : Journal
    {
        return $this->getJournalRepository()->fetchOne($id);
    }

    protected function getUnAuditedJournalRepository() : UnAuditedJournalRepository
    {
        return new UnAuditedJournalRepository();
    }

    protected function fetchUnAuditedJournal(int $id) : UnAuditedJournal
    {
        return $this->getUnAuditedJournalRepository()->fetchOne($id);
    }
    
    protected function getJournal() : Journal
    {
        return new Journal();
    }
    
    protected function getUnAuditedJournal() : UnAuditedJournal
    {
        return new UnAuditedJournal();
    }

    protected function executeAction(ICommand $command, UnAuditedJournal $unAuditedJournal) : UnAuditedJournal
    {
        $crew = $this->fetchCrew($command->crew);

        $unAuditedJournal->setApplyTitle($command->title);
        $unAuditedJournal->setApplyUserGroup($crew->getUserGroup());
        $unAuditedJournal->setRelation($crew);

        $unAuditedJournal->setTitle($command->title);
        $unAuditedJournal->setSource($command->source);
        $unAuditedJournal->setDescription($command->description);
        $unAuditedJournal->setCover($command->cover);
        $unAuditedJournal->setAttachment($command->attachment);
        $unAuditedJournal->setAuthImages($command->authImages);
        $unAuditedJournal->setYear($command->year);
        $unAuditedJournal->setPublishUserGroup($crew->getUserGroup());
        $unAuditedJournal->setCrew($crew);
        $unAuditedJournal->setStatus($command->status);

        return $unAuditedJournal;
    }

    protected function journalExecuteAction(ICommand $command, UnAuditedJournal $unAuditedJournal) : UnAuditedJournal
    {
        $journal = $this->fetchJournal($command->journalId);
        $crew = $this->fetchCrew($command->crew);

        $unAuditedJournal->setOperationType($command->operationType);
        $unAuditedJournal->setApplyTitle($journal->getTitle());
        $unAuditedJournal->setApplyUserGroup($crew->getUserGroup());
        $unAuditedJournal->setRelation($crew);

        $unAuditedJournal->setId($journal->getId());
        $unAuditedJournal->setTitle($journal->getTitle());
        $unAuditedJournal->setSource($journal->getSource());
        $unAuditedJournal->setDescription($journal->getDescription());
        $unAuditedJournal->setCover($journal->getCover());
        $unAuditedJournal->setAttachment($journal->getAttachment());
        $unAuditedJournal->setAuthImages($journal->getAuthImages());
        $unAuditedJournal->setPublishUserGroup($crew->getUserGroup());
        $unAuditedJournal->setStatus($journal->getStatus());
        $unAuditedJournal->setYear($journal->getYear());
        $unAuditedJournal->setCrew($crew);

        return $unAuditedJournal;
    }

    protected function applyInfo($unAuditedJournal) : array
    {
        $applyInfo = $this->getJournalDbTranslator()->objectToArray($unAuditedJournal);

        return $applyInfo;
    }
}
