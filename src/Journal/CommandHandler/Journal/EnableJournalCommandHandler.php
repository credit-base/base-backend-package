<?php
namespace Base\Journal\CommandHandler\Journal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Journal\Command\Journal\EnableJournalCommand;

class EnableJournalCommandHandler implements ICommandHandler
{
    use JournalCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof EnableJournalCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedJournal = $this->getUnAuditedJournal();

        $unAuditedJournal = $this->journalExecuteAction($command, $unAuditedJournal);
        $applyInfo = $this->applyInfo($unAuditedJournal);
        $unAuditedJournal->setApplyInfo($applyInfo);

        if ($unAuditedJournal->enable()) {
            $command->id = $unAuditedJournal->getApplyId();
            return true;
        }
        
        return false;
    }
}
