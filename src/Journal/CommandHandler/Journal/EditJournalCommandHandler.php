<?php
namespace Base\Journal\CommandHandler\Journal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Journal\Command\Journal\EditJournalCommand;

class EditJournalCommandHandler implements ICommandHandler
{
    use JournalCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditJournalCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedJournal = $this->getUnAuditedJournal();

        $unAuditedJournal->setOperationType($command->operationType);
        $unAuditedJournal = $this->executeAction($command, $unAuditedJournal);
        $unAuditedJournal->setId($command->journalId);
        $applyInfo = $this->applyInfo($unAuditedJournal);
        $unAuditedJournal->setApplyInfo($applyInfo);

        if ($unAuditedJournal->edit()) {
            $command->id = $unAuditedJournal->getApplyId();
            return true;
        }

        return false;
    }
}
