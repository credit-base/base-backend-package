<?php
namespace Base\Journal\CommandHandler\Journal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Journal\Command\Journal\AddJournalCommand;

class AddJournalCommandHandler implements ICommandHandler
{
    use JournalCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddJournalCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedJournal = $this->getUnAuditedJournal();

        $unAuditedJournal->setOperationType($command->operationType);
        $unAuditedJournal = $this->executeAction($command, $unAuditedJournal);
        $applyInfo = $this->applyInfo($unAuditedJournal);
        $unAuditedJournal->setApplyInfo($applyInfo);

        if ($unAuditedJournal->add()) {
            $command->id = $unAuditedJournal->getApplyId();
            return true;
        }
        
        return false;
    }
}
