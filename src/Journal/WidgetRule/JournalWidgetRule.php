<?php
namespace Base\Journal\WidgetRule;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Base\Journal\Model\Journal;

class JournalWidgetRule
{
    public function attachment($attachment) : bool
    {
        if (!V::arrayType()->notEmpty()->validate($attachment)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'attachment'));
            return false;
        }

        if (!$this->validateAttachmentExtension($attachment['identify'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'attachment'));
            return false;
        }

        return true;
    }

    private function validateAttachmentExtension($attachment) : bool
    {
        if (!V::extension('pdf')->validate($attachment)) {
            return false;
        }

        return true;
    }

    const AUTH_IMAGES_MAX_COUNT = 5;
    public function authImages($authImages) : bool
    {
        if (!V::arrayType()->validate($authImages)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'authImages'));
            return false;
        }

        if (count($authImages) > self::AUTH_IMAGES_MAX_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'authImages'));
            return false;
        }

        foreach ($authImages as $image) {
            if (!$this->validateImageExtension($image['identify'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'authImages'));
                return false;
            }
        }
        return true;
    }

    private function validateImageExtension($image) : bool
    {
        if (!V::extension('png')->validate($image)
            && !V::extension('jpg')->validate($image)
            && !V::extension('jpeg')->validate($image)) {
            return false;
        }

        return true;
    }

    const YEAR_MIN_LENGTH = 1901;
    const YEAR_MAX_LENGTH = 2155;

    public function year($year) : bool
    {
        if (!V::intVal()->between(self::YEAR_MIN_LENGTH, self::YEAR_MAX_LENGTH)->validate($year)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'year'));
            return false;
        }

        return true;
    }
}
