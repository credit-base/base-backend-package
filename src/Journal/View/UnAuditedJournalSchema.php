<?php
namespace Base\Journal\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

use Base\Journal\Model\UnAuditJournal;

class UnAuditedJournalSchema extends JournalSchema
{
    protected $resourceType = 'unAuditedJournals';

    public function getId($unAuditJournal) : int
    {
        return $unAuditJournal->getApplyId();
    }

    public function getAttributes($unAuditJournal) : array
    {
        $attributes = parent::getAttributes($unAuditJournal);
        $applyAttributes = array(
            'applyInfoCategory' => $unAuditJournal->getApplyInfoCategory()->getCategory(),
            'applyInfoType' => $unAuditJournal->getApplyInfoCategory()->getType(),
            'applyStatus'  => $unAuditJournal->getApplyStatus(),
            'rejectReason' => $unAuditJournal->getRejectReason(),
            'operationType'  => $unAuditJournal->getOperationType(),
            'journalId'  => $unAuditJournal->getId()
        );
        
        return array_merge($applyAttributes, $attributes);
    }

    public function getRelationships($unAuditJournal, $isPrimary, array $includeList)
    {
        $relationships = parent::getRelationships($unAuditJournal, $isPrimary, $includeList);
        $applyRelationships = [
            'relation' => [self::DATA => $unAuditJournal->getRelation()],
            'applyCrew' => [self::DATA => $unAuditJournal->getApplyCrew()],
            'applyUserGroup' => [self::DATA => $unAuditJournal->getApplyUserGroup()],
        ];

        return array_merge($applyRelationships, $relationships);
    }
}
