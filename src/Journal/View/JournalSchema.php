<?php
namespace Base\Journal\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class JournalSchema extends SchemaProvider
{
    protected $resourceType = 'journals';

    public function getId($journal) : int
    {
        return $journal->getId();
    }

    public function getAttributes($journal) : array
    {
        return [
            'title'  => $journal->getTitle(),
            'source'  => $journal->getSource(),
            'description' => $journal->getDescription(),
            'cover'  => $journal->getCover(),
            'attachment'  => $journal->getAttachment(),
            'authImages'  => $journal->getAuthImages(),
            'year' => $journal->getYear(),
            'status' => $journal->getStatus(),
            'createTime' => $journal->getCreateTime(),
            'updateTime' => $journal->getUpdateTime(),
            'statusTime' => $journal->getStatusTime(),
        ];
    }

    public function getRelationships($journal, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'publishUserGroup' => [self::DATA => $journal->getPublishUserGroup()],
            'crew' => [self::DATA => $journal->getCrew()]
        ];
    }
}
