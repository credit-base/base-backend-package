<?php
namespace Base\Journal\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Journal\Model\Journal;
use Base\Journal\Model\NullJournal;

class JournalDbTranslator implements ITranslator
{
    use JournalTranslatorTrait;
    
    public function arrayToObject(array $expression, $journal = null) : Journal
    {
        if (!isset($expression['journal_id'])) {
            return NullJournal::getInstance();
        }

        if ($journal == null) {
            $journal = new Journal($expression['journal_id']);
        }

        $journal->setId($expression['journal_id']);

        $this->arrayToObjectUtils($journal, $expression);

        return $journal;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($journal, array $keys = array()) : array
    {
        if (!$journal instanceof Journal) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'description',
                'cover',
                'attachment',
                'authImages',
                'year',
                'crew',
                'publishUserGroup',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['journal_id'] = $journal->getId();
        }

        $expressionInfo = $this->objectToArrayUtils($journal, $keys);
        
        return array_merge($expression, $expressionInfo);
    }
}
