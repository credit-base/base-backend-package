<?php
namespace Base\Journal\Translator;

use Base\Journal\Model\UnAuditedJournal;

use Base\ApplyForm\Translator\ApplyFormDbTranslator;

class UnAuditedJournalDbTranslator extends ApplyFormDbTranslator
{
    use JournalTranslatorTrait;

    public function arrayToObject(array $expression, $journal = null)
    {

        if ($journal == null) {
            $journal = new UnAuditedJournal();
        }

        $journal->setId($expression['apply_form_id']);
        $journal->setApplyId($expression['apply_form_id']);

        parent::arrayToObject($expression, $journal);

        return $journal;
    }

    public function arrayToObjects(array $expression, $journal = null)
    {
        if (isset($expression['journal_id'])) {
            $journal->setId($expression['journal_id']);
        }
        
        $this->arrayToObjectUtils($journal, $expression);

        return $journal;
    }
}
