<?php
namespace Base\Journal\Translator;

use Base\Journal\Model\Journal;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait JournalTranslatorTrait
{
    public function arrayToObjectUtils(Journal $journal, array $expression)
    {
        if (isset($expression['title'])) {
            $journal->setTitle($expression['title']);
        }
        if (isset($expression['source'])) {
            $journal->setSource($expression['source']);
        }
        if (isset($expression['description'])) {
            $journal->setDescription($expression['description']);
        }
        if (isset($expression['cover'])) {
            $cover = array();
            if (is_string($expression['cover'])) {
                $cover = json_decode($expression['cover'], true);
            }
            if (is_array($expression['cover'])) {
                $cover = $expression['cover'];
            }
            $journal->setCover($cover);
        }
        if (isset($expression['attachment'])) {
            $attachment = array();
            if (is_string($expression['attachment'])) {
                $attachment = json_decode($expression['attachment'], true);
            }
            if (is_array($expression['attachment'])) {
                $attachment = $expression['attachment'];
            }
            $journal->setAttachment($attachment);
        }
        if (isset($expression['auth_images'])) {
            $authImages = array();
            if (is_string($expression['auth_images'])) {
                $authImages = json_decode($expression['auth_images'], true);
            }
            if (is_array($expression['auth_images'])) {
                $authImages = $expression['auth_images'];
            }
            $journal->setAuthImages($authImages);
        }
        if (isset($expression['year'])) {
            $journal->setYear($expression['year']);
        }
        if (isset($expression['crew_id'])) {
            $journal->getCrew()->setId($expression['crew_id']);
        }
        if (isset($expression['publish_usergroup_id'])) {
            $journal->getPublishUserGroup()->setId($expression['publish_usergroup_id']);
        }
        if (isset($expression['status'])) {
            $journal->setStatus($expression['status']);
        }

        $journal->setCreateTime($expression['create_time']);
        $journal->setUpdateTime($expression['update_time']);
        $journal->setStatusTime($expression['status_time']);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArrayUtils(Journal $journal, array $keys)
    {
        $expression = array();

        if (in_array('title', $keys)) {
            $expression['title'] = $journal->getTitle();
        }
        if (in_array('source', $keys)) {
            $expression['source'] = $journal->getSource();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $journal->getDescription();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $journal->getCover();
        }
        if (in_array('attachment', $keys)) {
            $expression['attachment'] = $journal->getAttachment();
        }
        if (in_array('authImages', $keys)) {
            $expression['auth_images'] = $journal->getAuthImages();
        }
        if (in_array('year', $keys)) {
            $expression['year'] = $journal->getYear();
        }
        if (in_array('crew', $keys)) {
            $expression['crew_id'] = $journal->getCrew()->getId();
        }
        if (in_array('publishUserGroup', $keys)) {
            $expression['publish_usergroup_id'] = $journal->getPublishUserGroup()->getId();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $journal->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $journal->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $journal->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $journal->getStatusTime();
        }
        
        return $expression;
    }
}
