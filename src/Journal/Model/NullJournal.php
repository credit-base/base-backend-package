<?php
namespace Base\Journal\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;
use Base\Common\Model\NullEnableAbleTrait;

class NullJournal extends Journal implements INull
{
    use NullOperateTrait, NullEnableAbleTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
