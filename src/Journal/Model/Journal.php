<?php
namespace Base\Journal\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Journal\Repository\JournalRepository;
use Base\Journal\Adapter\Journal\IJournalAdapter;

use Base\Common\Model\IOperate;
use Base\Common\Model\IEnableAble;
use Base\Common\Model\OperateTrait;
use Base\Common\Model\EnableAbleTrait;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

class Journal implements IObject, IEnableAble, IOperate
{
    use Object, EnableAbleTrait, OperateTrait;

    protected $id;
    /**
     * @var string $title 标题
     */
    protected $title;

    /**
     * @var string $source 来源
     */
    protected $source;

    /**
     * @var string $description 简介
     */
    protected $description;

    /**
     * @var array $cover 封面
     */
    protected $cover;

    /**
     * @var array $attachment 附件
     */
    protected $attachment;

    /**
     * @var array $authImages 授权图片
     */
    protected $authImages;

    /**
     * @var int $year 年份
     */
    protected $year;

    /**
     * @var Crew $crew 发布人
     */
    protected $crew;

    protected $publishUserGroup;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->title = '';
        $this->source = '';
        $this->description = '';
        $this->cover = array();
        $this->attachment = array();
        $this->authImages = array();
        $this->year = 0;
        $this->crew = new Crew();
        $this->publishUserGroup = new UserGroup();
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new JournalRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->source);
        unset($this->description);
        unset($this->cover);
        unset($this->attachment);
        unset($this->authImages);
        unset($this->year);
        unset($this->crew);
        unset($this->publishUserGroup);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSource(string $source)
    {
        $this->source = $source;
    }

    public function getSource() : string
    {
        return $this->source;
    }
    
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setCover(array $cover)
    {
        $this->cover = $cover;
    }

    public function getCover() : array
    {
        return $this->cover;
    }

    public function setAttachment(array $attachment)
    {
        $this->attachment = $attachment;
    }

    public function getAttachment() : array
    {
        return $this->attachment;
    }

    public function setAuthImages(array $authImages)
    {
        $this->authImages = $authImages;
    }

    public function getAuthImages() : array
    {
        return $this->authImages;
    }

    public function setYear(int $year)
    {
        $this->year = $year;
    }

    public function getYear() : int
    {
        return $this->year;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setPublishUserGroup(UserGroup $publishUserGroup)
    {
        $this->publishUserGroup = $publishUserGroup;
    }

    public function getPublishUserGroup() : UserGroup
    {
        return $this->publishUserGroup;
    }

    protected function getRepository() : IJournalAdapter
    {
        return $this->repository;
    }

    protected function addAction() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crewId'));
            return false;
        }

        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crewId'));
            return false;
        }
        
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'title',
                'source',
                'description',
                'cover',
                'attachment',
                'authImages',
                'year',
                'status',
                'crew',
                'publishUserGroup',
                'updateTime'
            )
        );
    }

    public function enable() : bool
    {
        $this->setStatus(IEnableAble::STATUS['ENABLED']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'statusTime',
                    'status',
                    'updateTime',
                    'publishUserGroup',
                    'crew'
                ));
    }

    protected function updateStatus(int $status) : bool
    {
        $this->setStatus($status);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'statusTime',
                    'status',
                    'updateTime',
                    'publishUserGroup',
                    'crew'
                ));
    }
}
