<?php
namespace Base\Journal\Model;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Model\ApplyFormTrait;
use Base\ApplyForm\Model\ApplyInfoCategory;

use Base\Crew\Model\Crew;

use Base\UserGroup\Model\UserGroup;

class UnAuditedJournal extends Journal implements IApplyFormAble
{
    use ApplyFormTrait;
    
    const APPLY_JOURNAL_CATEGORY = ApplyInfoCategory::APPLY_INFO_CATEGORY['JOURNAL'];
    const APPLY_JOURNAL_TYPE = ApplyInfoCategory::APPLY_INFO_TYPE[self::APPLY_JOURNAL_CATEGORY]['JOURNAL'];

    public function __construct(int $applyId = 0)
    {
        parent::__construct();
        $this->applyId = !empty($applyId) ? $applyId : 0;
        $this->applyTitle = parent::getTitle();
        $this->relation = new Crew();
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->applyInfoCategory  = new ApplyInfoCategory(
            self::APPLY_JOURNAL_CATEGORY,
            self::APPLY_JOURNAL_TYPE
        );
        $this->applyInfo = array();
        $this->applyCrew = new Crew();
        $this->applyUserGroup = new UserGroup();
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->rejectReason = '';
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->applyId);
        unset($this->applyTitle);
        unset($this->relation);
        unset($this->operationType);
        unset($this->applyInfoCategory);
        unset($this->applyInfo);
        unset($this->applyUserGroup);
        unset($this->applyCrew);
        unset($this->applyStatus);
        unset($this->rejectReason);
    }
}
