<?php
namespace Base\Journal\Adapter\Journal;

use Base\Journal\Utils\MockFactory;
use Base\Journal\Model\UnAuditedJournal;

use Base\ApplyForm\Model\IApplyFormAble;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

class UnAuditedJournalMockAdapter implements IApplyFormAdapter
{
    public function add(IApplyFormAble $unAuditedJournal, array $keys = array()) : bool
    {
        unset($unAuditedJournal);
        unset($keys);

        return true;
    }

    public function edit(IApplyFormAble $unAuditedJournal, array $keys = array()) : bool
    {
        unset($unAuditedJournal);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : IApplyFormAble
    {
        return MockFactory::generateUnAuditedJournal($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedJournalList = array();

        foreach ($ids as $id) {
            $unAuditedJournalList[] = MockFactory::generateUnAuditedJournal($id);
        }

        return $unAuditedJournalList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
