<?php
namespace Base\Journal\Adapter\Journal;

use Base\Journal\Model\Journal;
use Base\Journal\Utils\MockFactory;

class JournalMockAdapter implements IJournalAdapter
{
    public function fetchOne($id) : Journal
    {
        return MockFactory::generateJournal($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $journalList = array();

        foreach ($ids as $id) {
            $journalList[$id] = MockFactory::generateJournal($id);
        }

        return $journalList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Journal $journal) : bool
    {
        unset($journal);
        return true;
    }

    public function edit(Journal $journal, array $keys = array()) : bool
    {
        unset($journal);
        unset($keys);
        return true;
    }
}
