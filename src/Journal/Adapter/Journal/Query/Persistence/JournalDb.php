<?php
namespace Base\Journal\Adapter\Journal\Query\Persistence;

use Marmot\Framework\Classes\Db;

class JournalDb extends Db
{
    const TABLE = 'journal';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
