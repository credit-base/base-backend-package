<?php
namespace Base\Journal\Adapter\Journal\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class JournalCache extends Cache
{
    const KEY = 'journal';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
