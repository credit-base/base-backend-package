<?php
namespace Base\Journal\Adapter\Journal\Query;

use Marmot\Framework\Query\RowCacheQuery;

class JournalRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'journal_id',
            new Persistence\JournalCache(),
            new Persistence\JournalDb()
        );
    }
}
