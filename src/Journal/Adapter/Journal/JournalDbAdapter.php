<?php
namespace Base\Journal\Adapter\Journal;

use Marmot\Interfaces\INull;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\Journal\Model\Journal;
use Base\Journal\Model\NullJournal;
use Base\Journal\Translator\JournalDbTranslator;
use Base\Journal\Adapter\Journal\Query\JournalRowCacheQuery;

use Base\Crew\Repository\CrewRepository;
use Base\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class JournalDbAdapter implements IJournalAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $crewRepository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->dbTranslator = new JournalDbTranslator();
        $this->rowCacheQuery = new JournalRowCacheQuery();
        $this->crewRepository = new CrewRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->crewRepository);
        unset($this->userGroupRepository);
    }
    
    protected function getDbTranslator() : JournalDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : JournalRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getNullObject() : INull
    {
        return NullJournal::getInstance();
    }

    public function add(Journal $journal) : bool
    {
        return $this->addAction($journal);
    }

    public function edit(Journal $journal, array $keys = array()) : bool
    {
        return $this->editAction($journal, $keys);
    }

    public function fetchOne($id) : Journal
    {
        $journal = $this->fetchOneAction($id);
        $this->fetchCrew($journal);
        $this->fetchPublishUserGroup($journal);

        return $journal;
    }

    public function fetchList(array $ids) : array
    {
        $journalList = $this->fetchListAction($ids);
        $this->fetchCrew($journalList);
        $this->fetchPublishUserGroup($journalList);

        return $journalList;
    }

    protected function fetchPublishUserGroup($journal)
    {
        return is_array($journal) ?
        $this->fetchPublishUserGroupByList($journal) :
        $this->fetchPublishUserGroupByObject($journal);
    }

    protected function fetchPublishUserGroupByObject($journal)
    {
        if (!$journal instanceof INull) {
            $userGroupId = $journal->getPublishUserGroup()->getId();
            $userGroup = $this->getUserGroupRepository()->fetchOne($userGroupId);
            $journal->setPublishUserGroup($userGroup);
        }
    }

    protected function fetchPublishUserGroupByList(array $journalList)
    {
        if (!empty($journalList)) {
            $userGroupIds = array();
            foreach ($journalList as $journal) {
                $userGroupIds[] = $journal->getPublishUserGroup()->getId();
            }

            $userGroupList = $this->getUserGroupRepository()->fetchList($userGroupIds);
            if (!empty($userGroupList)) {
                foreach ($journalList as $journal) {
                    if (isset($userGroupList[$journal->getPublishUserGroup()->getId()])) {
                        $journal->setPublishUserGroup($userGroupList[$journal->getPublishUserGroup()->getId()]);
                    }
                }
            }
        }
    }

    protected function fetchCrew($journal)
    {
        return is_array($journal) ?
        $this->fetchCrewByList($journal) :
        $this->fetchCrewByObject($journal);
    }

    protected function fetchCrewByObject($journal)
    {
        if (!$journal instanceof INull) {
            $crewId = $journal->getCrew()->getId();
            $crew = $this->getCrewRepository()->fetchOne($crewId);
            $journal->setCrew($crew);
        }
    }

    protected function fetchCrewByList(array $journalList)
    {
        if (!empty($journalList)) {
            $crewIds = array();
            foreach ($journalList as $journal) {
                $crewIds[] = $journal->getCrew()->getId();
            }

            $crewList = $this->getCrewRepository()->fetchList($crewIds);
            if (!empty($crewList)) {
                foreach ($journalList as $journal) {
                    if (isset($crewList[$journal->getCrew()->getId()])) {
                        $journal->setCrew($crewList[$journal->getCrew()->getId()]);
                    }
                }
            }
        }
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $journal = new Journal();
            if (isset($filter['publishUserGroup'])) {
                $journal->getPublishUserGroup()->setId($filter['publishUserGroup']);
                $info = $this->getDbTranslator()->objectToArray($journal, array('publishUserGroup'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['title'])) {
                $journal->setTitle($filter['title']);
                $info = $this->getDbTranslator()->objectToArray($journal, array('title'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $journal->setStatus($filter['status']);
                $info = $this->getDbTranslator()->objectToArray($journal, array('status'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['year'])) {
                $journal->setYear($filter['year']);
                $info = $this->getDbTranslator()->objectToArray($journal, array('year'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $journal = new Journal();

            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($journal, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['status'])) {
                $info = $this->getDbTranslator()->objectToArray($journal, array('status'));
                $condition .= $conjection.key($info).' '.($sort['status'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }
        
        return $condition;
    }
}
