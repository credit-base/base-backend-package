<?php
namespace Base\Journal\Adapter\Journal;

use Base\Journal\Model\Journal;

interface IJournalAdapter
{
    public function fetchOne($id) : Journal;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Journal $journal) : bool;

    public function edit(Journal $journal, array $keys = array()) : bool;
}
