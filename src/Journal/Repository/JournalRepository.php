<?php
namespace Base\Journal\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Journal\Model\Journal;
use Base\Journal\Adapter\Journal\IJournalAdapter;
use Base\Journal\Adapter\Journal\JournalDbAdapter;
use Base\Journal\Adapter\Journal\JournalMockAdapter;

class JournalRepository extends Repository implements IJournalAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new JournalDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IJournalAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IJournalAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IJournalAdapter
    {
        return new JournalMockAdapter();
    }

    public function fetchOne($id) : Journal
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Journal $journal) : bool
    {
        return $this->getAdapter()->add($journal);
    }

    public function edit(Journal $journal, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($journal, $keys);
    }
}
