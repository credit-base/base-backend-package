<?php
namespace Base\Journal\Repository;

use Base\ApplyForm\Repository\ApplyFormRepository;
use Base\ApplyForm\Adapter\ApplyForm\IApplyFormAdapter;

use Base\Journal\Adapter\Journal\UnAuditedJournalDbAdapter;
use Base\Journal\Adapter\Journal\UnAuditedJournalMockAdapter;

class UnAuditedJournalRepository extends ApplyFormRepository
{
    public function __construct()
    {
        $this->adapter = new UnAuditedJournalDbAdapter();
    }

    protected function getActualAdapter() : IApplyFormAdapter
    {
        return $this->adapter;
    }
    
    protected function getMockAdapter() : IApplyFormAdapter
    {
        return new UnAuditedJournalMockAdapter();
    }
}
