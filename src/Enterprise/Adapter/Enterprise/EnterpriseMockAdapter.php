<?php
namespace Base\Enterprise\Adapter\Enterprise;

use Base\Enterprise\Model\Enterprise;
use Base\Enterprise\Utils\MockFactory;

class EnterpriseMockAdapter implements IEnterpriseAdapter
{
    public function fetchOne($id) : Enterprise
    {
        return MockFactory::generateEnterprise($id);
    }

    public function fetchList(array $ids) : array
    {
        $enterpriseList = array();

        foreach ($ids as $id) {
            $enterpriseList[] = MockFactory::generateEnterprise($id);
        }

        return $enterpriseList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($number; $number<$size; $number++) {
            $ids[] = $number;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
