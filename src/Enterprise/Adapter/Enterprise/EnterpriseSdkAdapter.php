<?php
namespace Base\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\Enterprise\Model\Enterprise;
use Base\Enterprise\Model\NullEnterprise;
use Base\Enterprise\Translator\EnterpriseSdkTranslator;

use BaseSdk\Enterprise\Repository\EnterpriseRestfulRepository;

class EnterpriseSdkAdapter implements IEnterpriseAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    public function __construct()
    {
        $this->translator = new EnterpriseSdkTranslator();
        $this->repository = new EnterpriseRestfulRepository();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : EnterpriseRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullEnterprise::getInstance();
    }

    public function fetchOne($id) : Enterprise
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $number, $size);
    }
}
