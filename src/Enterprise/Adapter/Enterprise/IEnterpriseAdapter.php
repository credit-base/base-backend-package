<?php
namespace Base\Enterprise\Adapter\Enterprise;

use Base\Enterprise\Model\Enterprise;

interface IEnterpriseAdapter
{
    public function fetchOne($id) : Enterprise;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 0
    ) : array;
}
