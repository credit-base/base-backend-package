<?php
namespace Base\Enterprise\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\Enterprise\View\EnterpriseView;
use Base\Enterprise\Repository\EnterpriseSdkRepository;
use Base\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;

class EnterpriseFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new EnterpriseSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IEnterpriseAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new EnterpriseView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'enterprises';
    }
}
