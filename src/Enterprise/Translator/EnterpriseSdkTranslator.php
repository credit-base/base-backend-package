<?php
namespace Base\Enterprise\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\Enterprise\Model\Enterprise;
use Base\Enterprise\Model\NullEnterprise;

use BaseSdk\Enterprise\Model\Enterprise as EnterpriseSdk;
use BaseSdk\Enterprise\Model\NullEnterprise as NullEnterpriseSdk;

class EnterpriseSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($enterpriseSdk = null, $enterprise = null)
    {
        return $this->translateToObject($enterpriseSdk, $enterprise);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($enterpriseSdk = null, $enterprise = null) : Enterprise
    {
        if (!$enterpriseSdk instanceof EnterpriseSdk || $enterpriseSdk instanceof INull) {
            return NullEnterprise::getInstance();
        }

        if ($enterprise == null) {
            $enterprise = new Enterprise();
        }

        $enterprise->setId($enterpriseSdk->getId());
        $enterprise->setName($enterpriseSdk->getName());
        $enterprise->setUnifiedSocialCreditCode($enterpriseSdk->getUnifiedSocialCreditCode());
        $enterprise->setEstablishmentDate($enterpriseSdk->getEstablishmentDate());
        $enterprise->setApprovalDate($enterpriseSdk->getApprovalDate());
        $enterprise->setAddress($enterpriseSdk->getAddress());
        $enterprise->setRegistrationCapital($enterpriseSdk->getRegistrationCapital());
        $enterprise->setBusinessTermStart($enterpriseSdk->getBusinessTermStart());
        $enterprise->setBusinessTermTo($enterpriseSdk->getBusinessTermTo());
        $enterprise->setBusinessScope($enterpriseSdk->getBusinessScope());
        $enterprise->setRegistrationAuthority($enterpriseSdk->getRegistrationAuthority());
        $enterprise->setPrincipal($enterpriseSdk->getPrincipal());
        $enterprise->setPrincipalCardId($enterpriseSdk->getPrincipalCardId());
        $enterprise->setRegistrationStatus($enterpriseSdk->getRegistrationStatus());
        $enterprise->setEnterpriseTypeCode($enterpriseSdk->getEnterpriseTypeCode());
        $enterprise->setEnterpriseType($enterpriseSdk->getEnterpriseType());
        $enterprise->setIndustryCategory($enterpriseSdk->getIndustryCategory());
        $enterprise->setIndustryCode($enterpriseSdk->getIndustryCode());
        $enterprise->setAdministrativeArea($enterpriseSdk->getAdministrativeArea());
        $enterprise->setCreateTime($enterpriseSdk->getCreateTime());
        $enterprise->setStatusTime($enterpriseSdk->getStatusTime());
        $enterprise->setUpdateTime($enterpriseSdk->getUpdateTime());
        $enterprise->setStatus($enterpriseSdk->getStatus());
        
        return $enterprise;
    }

    public function objectToValueObject($enterprise)
    {
        unset($enterprise);

        return NullEnterpriseSdk::getInstance();
    }
}
