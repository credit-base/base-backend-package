<?php
namespace Base\Enterprise\Repository;

use Marmot\Framework\Classes\Repository;

use Base\Enterprise\Model\Enterprise;
use Base\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;
use Base\Enterprise\Adapter\Enterprise\EnterpriseSdkAdapter;
use Base\Enterprise\Adapter\Enterprise\EnterpriseMockAdapter;

class EnterpriseSdkRepository extends Repository implements IEnterpriseAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new EnterpriseSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IEnterpriseAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    public function getActualAdapter() : IEnterpriseAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IEnterpriseAdapter
    {
        return new EnterpriseMockAdapter();
    }

    public function fetchOne($id) : Enterprise
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $number, $size);
    }
}
