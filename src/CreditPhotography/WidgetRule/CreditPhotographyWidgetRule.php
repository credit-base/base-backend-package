<?php
namespace Base\CreditPhotography\WidgetRule;

use Marmot\Core;

use Respect\Validation\Validator as V;

class CreditPhotographyWidgetRule
{
    const IMAGES_SIZE_MAX = 9;
    const VIDEO_SIZE_MAX = 1;

    /**
     * @SuppressWarnings(PHPMD.ElseExpression)
     */
    public function attachments($attachments) : bool
    {
        if (!V::arrayType()->validate($attachments)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'attachments'));
            return false;
        } else {
            foreach ($attachments as $attachment) {
                if ($this->validateImageExtension($attachment['identify']) &&
                    count($attachments) <= self::IMAGES_SIZE_MAX) {
                    return true;
                } elseif ($this->validateVideoExtension($attachment['identify'])
                    && count($attachments) == self::VIDEO_SIZE_MAX) {
                    return true;
                } else {
                    Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'attachments'));
                    return false;
                }
            }
        }

        Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'attachments'));
        return false;
    }

    private function validateImageExtension(string $image) : bool
    {
        if (!V::extension('png')->validate($image)
            && !V::extension('jpg')->validate($image)
            && !V::extension('jpeg')->validate($image)
            && !V::extension('bmp')->validate($image)
            && !V::extension('gif')->validate($image)) {
            return false;
        }
        return true;
    }

    private function validateVideoExtension(string $video) : bool
    {
        if (!V::extension('mp4')->validate($video)) {
            return false;
        }
        return true;
    }
}
