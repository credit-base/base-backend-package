<?php
namespace Base\CreditPhotography\Command\CreditPhotography;

use Marmot\Interfaces\ICommand;

class AddCreditPhotographyCommand implements ICommand
{
    public $description;

    public $attachments;

    public $member;

    public $id;
    
    public function __construct(
        string $description,
        array $attachments,
        int $member = 0,
        int $id = 0
    ) {
        $this->description = $description;
        $this->attachments = $attachments;
        $this->member = $member;
        $this->id = $id;
    }
}
