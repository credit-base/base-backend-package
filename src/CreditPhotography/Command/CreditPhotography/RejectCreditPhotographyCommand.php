<?php
namespace Base\CreditPhotography\Command\CreditPhotography;

use Base\Common\Command\RejectCommand;

class RejectCreditPhotographyCommand extends RejectCommand
{
    public $applyCrew;

    public function __construct(
        int $applyCrew,
        string $rejectReason,
        int $id = 0
    ) {
        parent::__construct(
            $rejectReason,
            $id
        );
        $this->applyCrew = $applyCrew;
    }
}
