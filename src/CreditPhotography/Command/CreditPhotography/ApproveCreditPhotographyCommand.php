<?php
namespace Base\CreditPhotography\Command\CreditPhotography;

use Base\Common\Command\ApproveCommand;

class ApproveCreditPhotographyCommand extends ApproveCommand
{
    public $applyCrew;

    public function __construct(
        int $applyCrew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        $this->applyCrew = $applyCrew;
    }
}
