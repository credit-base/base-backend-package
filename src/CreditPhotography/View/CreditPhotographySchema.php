<?php
namespace Base\CreditPhotography\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class CreditPhotographySchema extends SchemaProvider
{
    protected $resourceType = 'creditPhotography';

    public function getId($creditPhotography) : int
    {
        return $creditPhotography->getId();
    }

    public function getAttributes($creditPhotography) : array
    {
        return [
            'description' => $creditPhotography->getDescription(),
            'attachments'  => $creditPhotography->getAttachments(),
            'applyStatus'  => $creditPhotography->getApplyStatus(),
            'rejectReason' => $creditPhotography->getRejectReason(),
            'status' => $creditPhotography->getStatus(),
            'createTime' => $creditPhotography->getCreateTime(),
            'updateTime' => $creditPhotography->getUpdateTime(),
            'statusTime' => $creditPhotography->getStatusTime(),
        ];
    }

    public function getRelationships($creditPhotography, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'member' => [self::DATA => $creditPhotography->getMember()],
            'applyCrew' => [self::DATA => $creditPhotography->getApplyCrew()]
        ];
    }
}
