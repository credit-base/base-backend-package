<?php
namespace Base\CreditPhotography\Repository;

use Marmot\Framework\Classes\Repository;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter;
use Base\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyDbAdapter;
use Base\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyMockAdapter;

class CreditPhotographyRepository extends Repository implements ICreditPhotographyAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new CreditPhotographyDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(ICreditPhotographyAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : ICreditPhotographyAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ICreditPhotographyAdapter
    {
        return new CreditPhotographyMockAdapter();
    }

    public function fetchOne($id) : CreditPhotography
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(CreditPhotography $creditPhotography) : bool
    {
        return $this->getAdapter()->add($creditPhotography);
    }

    public function edit(CreditPhotography $creditPhotography, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($creditPhotography, $keys);
    }
}
