<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography;

use Base\CreditPhotography\Model\CreditPhotography;

interface ICreditPhotographyAdapter
{
    public function fetchOne($id) : CreditPhotography;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(CreditPhotography $creditPhotography) : bool;

    public function edit(CreditPhotography $creditPhotography, array $keys = array()) : bool;
}
