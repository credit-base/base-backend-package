<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography;

use Marmot\Core;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Utils\MockFactory;

class CreditPhotographyMockAdapter implements ICreditPhotographyAdapter
{
    public function fetchOne($id) : CreditPhotography
    {
        return MockFactory::generateCreditPhotography($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $creditPhotographyList = array();

        foreach ($ids as $id) {
            $creditPhotographyList[$id] = MockFactory::generateCreditPhotography($id);
        }

        return $creditPhotographyList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(CreditPhotography $creditPhotography) : bool
    {
        unset($creditPhotography);
        return true;
    }

    public function edit(CreditPhotography $creditPhotography, array $keys = array()) : bool
    {
        unset($creditPhotography);
        unset($keys);
        return true;
    }
}
