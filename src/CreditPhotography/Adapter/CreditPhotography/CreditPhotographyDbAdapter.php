<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Model\NullCreditPhotography;
use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;
use Base\CreditPhotography\Adapter\CreditPhotography\Query\CreditPhotographyRowCacheQuery;

use Base\Crew\Repository\CrewRepository;
use Base\Member\Repository\MemberRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class CreditPhotographyDbAdapter implements ICreditPhotographyAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $crewRepository;

    private $memberRepository;

    public function __construct()
    {
        $this->dbTranslator = new CreditPhotographyDbTranslator();
        $this->rowCacheQuery = new CreditPhotographyRowCacheQuery();
        $this->crewRepository = new CrewRepository();
        $this->memberRepository = new MemberRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->crewRepository);
        unset($this->memberRepository);
    }
    
    protected function getDbTranslator() : CreditPhotographyDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : CreditPhotographyRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->memberRepository;
    }

    protected function getNullObject() : INull
    {
        return NullCreditPhotography::getInstance();
    }

    public function add(CreditPhotography $creditPhotography) : bool
    {
        return $this->addAction($creditPhotography);
    }

    public function edit(CreditPhotography $creditPhotography, array $keys = array()) : bool
    {
        return $this->editAction($creditPhotography, $keys);
    }

    public function fetchOne($id) : CreditPhotography
    {
        $creditPhotography = $this->fetchOneAction($id);
        $this->fetchApplyCrew($creditPhotography);
        $this->fetchMember($creditPhotography);

        return $creditPhotography;
    }

    public function fetchList(array $ids) : array
    {
        $creditPhotographyList = $this->fetchListAction($ids);
        $this->fetchApplyCrew($creditPhotographyList);
        $this->fetchMember($creditPhotographyList);

        return $creditPhotographyList;
    }

    protected function fetchMember($creditPhotography)
    {
        return is_array($creditPhotography) ?
        $this->fetchMemberByList($creditPhotography) :
        $this->fetchMemberByObject($creditPhotography);
    }

    protected function fetchMemberByObject($creditPhotography)
    {
        if (!$creditPhotography instanceof INull) {
            $memberId = $creditPhotography->getMember()->getId();
            $member = $this->getMemberRepository()->fetchOne($memberId);
            $creditPhotography->setMember($member);
        }
    }

    protected function fetchMemberByList(array $creditPhotographyList)
    {
        if (!empty($creditPhotographyList)) {
            $memberIds = array();
            foreach ($creditPhotographyList as $creditPhotography) {
                $memberIds[] = $creditPhotography->getMember()->getId();
            }

            $memberList = $this->getMemberRepository()->fetchList($memberIds);
            if (!empty($memberList)) {
                foreach ($creditPhotographyList as $creditPhotography) {
                    if (isset($memberList[$creditPhotography->getMember()->getId()])) {
                        $creditPhotography->setMember($memberList[$creditPhotography->getMember()->getId()]);
                    }
                }
            }
        }
    }

    protected function fetchApplyCrew($creditPhotography)
    {
        return is_array($creditPhotography) ?
        $this->fetchApplyCrewByList($creditPhotography) :
        $this->fetchApplyCrewByObject($creditPhotography);
    }

    protected function fetchApplyCrewByObject($creditPhotography)
    {
        if (!$creditPhotography instanceof INull) {
            $crewId = $creditPhotography->getApplyCrew()->getId();
            $crew = $this->getCrewRepository()->fetchOne($crewId);
            $creditPhotography->setApplyCrew($crew);
        }
    }

    protected function fetchApplyCrewByList(array $creditPhotographyList)
    {
        if (!empty($creditPhotographyList)) {
            $crewIds = array();
            foreach ($creditPhotographyList as $creditPhotography) {
                $crewIds[] = $creditPhotography->getApplyCrew()->getId();
            }

            $crewList = $this->getCrewRepository()->fetchList($crewIds);
            if (!empty($crewList)) {
                foreach ($creditPhotographyList as $creditPhotography) {
                    if (isset($crewList[$creditPhotography->getApplyCrew()->getId()])) {
                        $creditPhotography->setApplyCrew($crewList[$creditPhotography->getApplyCrew()->getId()]);
                    }
                }
            }
        }
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $creditPhotography = new CreditPhotography();
            if (isset($filter['member'])) {
                $creditPhotography->getMember()->setId($filter['member']);
                $info = $this->getDbTranslator()->objectToArray($creditPhotography, array('member'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['realName'])) {
                $condition .= $conjection.'real_name LIKE \'%'.$filter['realName'].'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $creditPhotography->setStatus($filter['status']);
                $info = $this->getDbTranslator()->objectToArray($creditPhotography, array('status'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['applyStatus'])) {
                $creditPhotography->setApplyStatus(0);
                $info = $this->getDbTranslator()->objectToArray($creditPhotography, array('applyStatus'));
                $condition .= $conjection.key($info).' IN ('.$filter['applyStatus'].')';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $creditPhotography = new CreditPhotography();

            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($creditPhotography, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['status'])) {
                $info = $this->getDbTranslator()->objectToArray($creditPhotography, array('status'));
                $condition .= $conjection.key($info).' '.($sort['status'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['applyStatus'])) {
                $info = $this->getDbTranslator()->objectToArray($creditPhotography, array('applyStatus'));
                $condition .= $conjection.key($info).' '.($sort['applyStatus'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }
        
        return $condition;
    }
}
