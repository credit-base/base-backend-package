<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class CreditPhotographyCache extends Cache
{
    const KEY = 'credit_photography';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
