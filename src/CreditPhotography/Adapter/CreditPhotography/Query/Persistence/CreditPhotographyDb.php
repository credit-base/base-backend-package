<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography\Query\Persistence;

use Marmot\Framework\Classes\Db;

class CreditPhotographyDb extends Db
{
    const TABLE = 'credit_photography';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
