<?php
namespace Base\CreditPhotography\Adapter\CreditPhotography\Query;

use Marmot\Framework\Query\RowCacheQuery;

class CreditPhotographyRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'credit_photography_id',
            new Persistence\CreditPhotographyCache(),
            new Persistence\CreditPhotographyDb()
        );
    }
}
