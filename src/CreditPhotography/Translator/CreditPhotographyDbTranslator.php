<?php
namespace Base\CreditPhotography\Translator;

use Marmot\Interfaces\ITranslator;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Model\NullCreditPhotography;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class CreditPhotographyDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $creditPhotography = null) : CreditPhotography
    {
        if (!isset($expression['credit_photography_id'])) {
            return NullCreditPhotography::getInstance();
        }

        if ($creditPhotography == null) {
            $creditPhotography = new CreditPhotography($expression['credit_photography_id']);
        }

        if (isset($expression['description'])) {
            $creditPhotography->setDescription($expression['description']);
        }
        if (isset($expression['attachments'])) {
            $attachments = array();
            if (is_string($expression['attachments'])) {
                $attachments = json_decode($expression['attachments'], true);
            }
            if (is_array($expression['attachments'])) {
                $attachments = $expression['attachments'];
            }
            $creditPhotography->setAttachments($attachments);
        }
        if (isset($expression['member_id'])) {
            $creditPhotography->getMember()->setId($expression['member_id']);
        }
        if (isset($expression['apply_crew_id'])) {
            $creditPhotography->getApplyCrew()->setId($expression['apply_crew_id']);
        }
        if (isset($expression['reject_reason'])) {
            $creditPhotography->setRejectReason($expression['reject_reason']);
        }
        if (isset($expression['apply_status'])) {
            $creditPhotography->setApplyStatus($expression['apply_status']);
        }

        $creditPhotography->setStatus($expression['status']);
        $creditPhotography->setCreateTime($expression['create_time']);
        $creditPhotography->setUpdateTime($expression['update_time']);
        $creditPhotography->setStatusTime($expression['status_time']);

        return $creditPhotography;
    }

    public function objectToArray($creditPhotography, array $keys = array()) : array
    {
        if (!$creditPhotography instanceof CreditPhotography) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'description',
                'attachments',
                'member',
                'applyCrew',
                'rejectReason',
                'applyStatus',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['credit_photography_id'] = $creditPhotography->getId();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $creditPhotography->getDescription();
        }
        if (in_array('attachments', $keys)) {
            $expression['attachments'] = $creditPhotography->getAttachments();
        }
        if (in_array('member', $keys)) {
            $expression['member_id'] = $creditPhotography->getMember()->getId();
            $expression['real_name'] = $creditPhotography->getMember()->getRealName();
        }
        if (in_array('applyCrew', $keys)) {
            $expression['apply_crew_id'] = $creditPhotography->getApplyCrew()->getId();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['reject_reason'] = $creditPhotography->getRejectReason();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['apply_status'] = $creditPhotography->getApplyStatus();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $creditPhotography->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $creditPhotography->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $creditPhotography->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $creditPhotography->getStatusTime();
        }
        
        return $expression;
    }
}
