<?php
namespace Base\CreditPhotography\Model;

use Marmot\Interfaces\INull;

use Base\Common\Model\NullOperateTrait;
use Base\Common\Model\NullApproveAbleTrait;

class NullCreditPhotography extends CreditPhotography implements INull
{
    use NullOperateTrait, NullApproveAbleTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function delete() : bool
    {
        return $this->resourceNotExist();
    }
}
