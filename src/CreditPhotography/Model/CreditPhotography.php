<?php
namespace Base\CreditPhotography\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Common\Model\IOperate;
use Base\Common\Model\IApproveAble;
use Base\Common\Model\OperateTrait;
use Base\Common\Model\ApproveAbleTrait;

use Base\Crew\Model\Crew;

use Base\Member\Model\Member;

use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter;

class CreditPhotography implements IObject, IOperate, IApproveAble
{
    use Object, OperateTrait, ApproveAbleTrait;

    const STATUS = array(
        'NORMAL' => 0, //正常
        'DELETED' => -2, //删除
    );

    protected $id;
    /**
     * @var string $description 描述
     */
    protected $description;

    /**
     * @var array $attachments 图片/视频
     */
    protected $attachments;

    /**
     * @var Member $member 前台用户
     */
    protected $member;

    /**
     * @var Crew $applyCrew 审核人
     */
    protected $applyCrew;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->description = 0;
        $this->attachments = array();
        $this->member = new Member();
        $this->applyCrew = new Crew();
        $this->applyStatus = IApproveAble::APPLY_STATUS['PENDING'];
        $this->rejectReason = '';
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new CreditPhotographyRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->description);
        unset($this->attachments);
        unset($this->member);
        unset($this->applyCrew);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    /**
     * @marmot-set-id
     */
    public function setId($id) : void
    {
        $this->id = $id;
    }
    /**
     * @marmot-get-id
     */
    public function getId()
    {
        return $this->id;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setAttachments(array $attachments) : void
    {
        $this->attachments = $attachments;
    }

    public function getAttachments() : array
    {
        return $this->attachments;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }

    protected function getRepository() : ICreditPhotographyAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        if ($this->getMember() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'memberId'));
            return false;
        }

        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return false;
    }

    protected function approveAction() : bool
    {
        return $this->updateApplyStatus(IApproveAble::APPLY_STATUS['APPROVE']);
    }

    protected function rejectAction() : bool
    {
        return $this->updateApplyStatus(IApproveAble::APPLY_STATUS['REJECT']);
    }

    protected function updateApplyStatus(int $applyStatus) : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'applyStatus'));
            return false;
        }

        $this->setApplyStatus($applyStatus);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array('updateTime','applyStatus','statusTime', 'applyCrew', 'rejectReason')
        );
    }

    public function delete() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        $this->setStatus(self::STATUS['DELETED']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array('updateTime','status','statusTime')
        );
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }
}
