<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Common\Model\IApproveAble;
use Base\Common\Command\RejectCommand;
use Base\Common\CommandHandler\RejectCommandHandler;

class RejectCreditPhotographyCommandHandler extends RejectCommandHandler
{
    use CreditPhotographyCommandHandlerTrait;

    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchCreditPhotography($id);
    }

    protected function executeAction(RejectCommand $command)
    {
        $applyCrew = $this->fetchApplyCrew($command->applyCrew);

        $this->rejectAble = $this->fetchIApplyObject($command->id);
        $this->rejectAble->setRejectReason($command->rejectReason);
        $this->rejectAble->setApplyCrew($applyCrew);
        
        return $this->rejectAble->reject();
    }
}
