<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Common\Model\IApproveAble;
use Base\Common\Command\ApproveCommand;
use Base\Common\CommandHandler\ApproveCommandHandler;

class ApproveCreditPhotographyCommandHandler extends ApproveCommandHandler
{
    use CreditPhotographyCommandHandlerTrait;

    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchCreditPhotography($id);
    }

    protected function executeAction(ApproveCommand $command)
    {
        $this->approveAble = $this->fetchIApplyObject($command->id);
        $applyCrew = $this->fetchApplyCrew($command->applyCrew);
        $this->approveAble->setApplyCrew($applyCrew);

        return $this->approveAble->approve();
    }
}
