<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;

class AddCreditPhotographyCommandHandler implements ICommandHandler
{
    use CreditPhotographyCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddCreditPhotographyCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->fetchMember($command->member);

        $creditPhotography = $this->getCreditPhotography();

        $creditPhotography->setDescription($command->description);
        $creditPhotography->setAttachments($command->attachments);
        $creditPhotography->setMember($member);

        if ($creditPhotography->add()) {
            $command->id = $creditPhotography->getId();
            return true;
        }
        
        return false;
    }
}
