<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Crew\Model\Crew;
use Base\Crew\Repository\CrewRepository;

use Base\Member\Model\Member;
use Base\Member\Repository\MemberRepository;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Translator\CreditPhotographyDbTranslator;

trait CreditPhotographyCommandHandlerTrait
{
    protected function getMemberRepository() : MemberRepository
    {
        return new MemberRepository();
    }

    protected function fetchMember(int $id) : Member
    {
        return $this->getMemberRepository()->fetchOne($id);
    }

    protected function getCrewRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    protected function fetchApplyCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    protected function getCreditPhotographyRepository() : CreditPhotographyRepository
    {
        return new CreditPhotographyRepository();
    }

    protected function getCreditPhotographyDbTranslator() : CreditPhotographyDbTranslator
    {
        return new CreditPhotographyDbTranslator();
    }

    protected function fetchCreditPhotography(int $id) : CreditPhotography
    {
        return $this->getCreditPhotographyRepository()->fetchOne($id);
    }
    
    protected function getCreditPhotography() : CreditPhotography
    {
        return new CreditPhotography();
    }
}
