<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand;

class DeleteCreditPhotographyCommandHandler implements ICommandHandler
{
    use CreditPhotographyCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof DeleteCreditPhotographyCommand)) {
            throw new \InvalidArgumentException;
        }

        $creditPhotography = $this->fetchCreditPhotography($command->id);

        return $creditPhotography->delete();
    }
}
