<?php
namespace Base\CreditPhotography\CommandHandler\CreditPhotography;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class CreditPhotographyCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand' =>
        'Base\CreditPhotography\CommandHandler\CreditPhotography\AddCreditPhotographyCommandHandler',
        'Base\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand' =>
        'Base\CreditPhotography\CommandHandler\CreditPhotography\DeleteCreditPhotographyCommandHandler',
        'Base\CreditPhotography\Command\CreditPhotography\ApproveCreditPhotographyCommand' =>
        'Base\CreditPhotography\CommandHandler\CreditPhotography\ApproveCreditPhotographyCommandHandler',
        'Base\CreditPhotography\Command\CreditPhotography\RejectCreditPhotographyCommand' =>
        'Base\CreditPhotography\CommandHandler\CreditPhotography\RejectCreditPhotographyCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
