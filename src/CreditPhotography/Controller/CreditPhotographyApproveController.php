<?php
namespace Base\CreditPhotography\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Base\Common\Controller\Interfaces\IApproveAbleController;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\View\CreditPhotographyView;
use Base\CreditPhotography\Command\CreditPhotography\RejectCreditPhotographyCommand;
use Base\CreditPhotography\Command\CreditPhotography\ApproveCreditPhotographyCommand;

class CreditPhotographyApproveController extends Controller implements IApproveAbleController
{
    use JsonApiTrait, CreditPhotographyControllerTrait;

    /**
     * 申请信息审核通过功能,通过PATCH传参
     * 对应路由 /creditPhotography/{id:\d+}/approve
     * @param int id 申请信息id
     * @return jsonApi
     */
    public function approve(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $relationships = $data['relationships'];

        $applyCrewId = $relationships['applyCrew']['data'][0]['id'];

        if ($this->validateApproveScenario($applyCrewId)) {
            $command = new ApproveCreditPhotographyCommand($applyCrewId, $id);

            if ($this->getCommandBus()->send($command)) {
                $creditPhotography = $this->getRepository()->fetchOne($command->id);
                
                if ($creditPhotography instanceof CreditPhotography) {
                    $this->render(new CreditPhotographyView($creditPhotography));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 申请信息审核驳回功能,通过PATCH传参
     * 对应路由 /creditPhotography/{id:\d+}/reject
     * @param int id 申请信息id
     * @return jsonApi
     */
    public function reject(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $rejectReason = isset($attributes['rejectReason']) ? $attributes['rejectReason'] : array();
        $applyCrew = $relationships['applyCrew']['data'][0]['id'];

        if ($this->validateRejectScenario($rejectReason, $applyCrew)) {
            $command = new RejectCreditPhotographyCommand(
                $applyCrew,
                $rejectReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $creditPhotography = $this->getRepository()->fetchOne($command->id);
                
                if ($creditPhotography instanceof CreditPhotography) {
                    $this->render(new CreditPhotographyView($creditPhotography));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
