<?php
namespace Base\CreditPhotography\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\CreditPhotography\Model\CreditPhotography;
use Base\CreditPhotography\View\CreditPhotographyView;
use Base\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;
use Base\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand;

class CreditPhotographyOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, CreditPhotographyControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }
    /**
     * 信用随手拍新增功能,通过POST传参
     * 对应路由 /creditPhotography
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $description = $attributes['description'];
        $attachments = $attributes['attachments'];

        $member = $relationships['member']['data'][0]['id'];

        //验证
        if ($this->validateAddScenario(
            $description,
            $attachments,
            $member
        )) {
            //初始化命令
            $command = new AddCreditPhotographyCommand(
                $description,
                $attachments,
                $member
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $creditPhotography = $this->getRepository()->fetchOne($command->id);
                if ($creditPhotography instanceof CreditPhotography) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new CreditPhotographyView($creditPhotography));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 信用随手拍删除功能,通过DELETE传参
     * 对应路由 /creditPhotography/{id:\d+}/delete
     * @param int id 信用随手拍 id
     * @return jsonApi
     */
    public function delete(int $id)
    {
        if (!empty($id)) {
            $command = new DeleteCreditPhotographyCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $creditPhotography  = $this->getRepository()->fetchOne($id);
                if ($creditPhotography instanceof CreditPhotography) {
                    $this->render(new CreditPhotographyView($creditPhotography));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        return false;
    }
}
