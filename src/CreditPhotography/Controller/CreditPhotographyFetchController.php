<?php
namespace Base\CreditPhotography\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use Base\CreditPhotography\View\CreditPhotographyView;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter;

class CreditPhotographyFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CreditPhotographyRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ICreditPhotographyAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new CreditPhotographyView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'creditPhotography';
    }
}
