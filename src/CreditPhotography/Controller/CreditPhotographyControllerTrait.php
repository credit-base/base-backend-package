<?php
namespace Base\CreditPhotography\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\CreditPhotography\WidgetRule\CreditPhotographyWidgetRule;
use Base\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\CreditPhotography\CommandHandler\CreditPhotography\CreditPhotographyCommandHandlerFactory;

trait CreditPhotographyControllerTrait
{
    protected function getCreditPhotographyWidgetRule() : CreditPhotographyWidgetRule
    {
        return new CreditPhotographyWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : CreditPhotographyRepository
    {
        return new CreditPhotographyRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new CreditPhotographyCommandHandlerFactory());
    }

    protected function validateAddScenario(
        $description,
        $attachments,
        $member
    ) {
        return $this->getCommonWidgetRule()->description($description)
            && $this->getCreditPhotographyWidgetRule()->attachments($attachments)
            && $this->getCommonWidgetRule()->formatNumeric($member, 'memberId');
    }

    protected function validateApproveScenario(
        $applyCrew
    ) {
        return $this->getCommonWidgetRule()->formatNumeric($applyCrew, 'applyCrewId');
    }

    protected function validateRejectScenario(
        $rejectReason,
        $applyCrew
    ) {
        return $this->getCommonWidgetRule()->description($rejectReason, 'rejectReason')
            && $this->getCommonWidgetRule()->formatNumeric($applyCrew, 'applyCrewId');
    }
}
