<?php
namespace Base\WorkOrderTask\Translator;

use Base\WorkOrderTask\Model\ParentTask;

use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\NullSdkTranslator;

class TemplateTranslatorFactory
{
    const MAPS = array(
        ParentTask::TEMPLATE_TYPE['BJ'] =>
        'Base\Template\Translator\BjTemplateSdkTranslator',
        ParentTask::TEMPLATE_TYPE['GB'] =>
        'Base\Template\Translator\GbTemplateSdkTranslator'
    );

    public function getTranslator(int $type) : ISdkTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullSdkTranslator::getInstance();
    }
}
