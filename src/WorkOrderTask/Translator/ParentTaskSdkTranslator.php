<?php
namespace Base\WorkOrderTask\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\WorkOrderTask\Model\ParentTask;
use Base\WorkOrderTask\Model\NullParentTask;

use BaseSdk\WorkOrderTask\Model\ParentTask as ParentTaskSdk;
use BaseSdk\WorkOrderTask\Model\NullParentTask as NullParentTaskSdk;

use Base\UserGroup\Translator\UserGroupSdkTranslator;

class ParentTaskSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getTemplateTranslatorFactory() : TemplateTranslatorFactory
    {
        return new TemplateTranslatorFactory();
    }

    protected function getTemplateSdkTranslator(int $type) : ISdkTranslator
    {
        return $this->getTemplateTranslatorFactory()->getTranslator($type);
    }
    
    protected function getUserGroupSdkTranslator() : ISdkTranslator
    {
        return new UserGroupSdkTranslator();
    }
    
    public function valueObjectToObject($parentTaskSdk = null, $parentTask = null)
    {
        return $this->translateToObject($parentTaskSdk, $parentTask);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($parentTaskSdk = null, $parentTask = null) : ParentTask
    {
        if (!$parentTaskSdk instanceof ParentTaskSdk || $parentTaskSdk instanceof INull) {
            return NullParentTask::getInstance();
        }

        if ($parentTask == null) {
            $parentTask = new ParentTask();
        }

        $assignObjects = array();
        $assignObjectsSdk = $parentTaskSdk->getAssignObjects();

        foreach ($assignObjectsSdk as $assignObject) {
            $assignObjects[] = $this->getUserGroupSdkTranslator()->valueObjectToObject($assignObject);
        }

        $template = $this->getTemplateSdkTranslator(
            $parentTaskSdk->getTemplateType()
        )->valueObjectToObject($parentTaskSdk->getTemplate());

        $parentTask->setId($parentTaskSdk->getId());
        $parentTask->setTitle($parentTaskSdk->getTitle());
        $parentTask->setDescription($parentTaskSdk->getDescription());
        $parentTask->setEndTime($parentTaskSdk->getEndTime());
        $parentTask->setAttachment($parentTaskSdk->getAttachment());
        $parentTask->setTemplateType($parentTaskSdk->getTemplateType());
        $parentTask->setTemplate($template);
        $parentTask->setAssignObjects($assignObjects);
        $parentTask->setFinishCount($parentTaskSdk->getFinishCount());
        $parentTask->setRatio($parentTaskSdk->getRatio());
        $parentTask->setReason($parentTaskSdk->getReason());
        $parentTask->setCreateTime($parentTaskSdk->getCreateTime());
        $parentTask->setStatusTime($parentTaskSdk->getStatusTime());
        $parentTask->setUpdateTime($parentTaskSdk->getUpdateTime());
        $parentTask->setStatus($parentTaskSdk->getStatus());

        return $parentTask;
    }

    public function objectToValueObject($parentTask)
    {
        if (!$parentTask instanceof ParentTask) {
            return NullParentTaskSdk::getInstance();
        }

        $assignObjectsSdk = array();
        $assignObjects = $parentTask->getAssignObjects();

        foreach ($assignObjects as $assignObject) {
            $assignObjectsSdk[] = $this->getUserGroupSdkTranslator()->objectToValueObject($assignObject);
        }

        $templateSdk = $this->getTemplateSdkTranslator(
            $parentTask->getTemplateType()
        )->objectToValueObject($parentTask->getTemplate());

        $parentTaskSdk = new ParentTaskSdk(
            $parentTask->getId(),
            $parentTask->getTitle(),
            $parentTask->getDescription(),
            $parentTask->getEndTime(),
            $parentTask->getAttachment(),
            $parentTask->getTemplateType(),
            $templateSdk,
            $assignObjectsSdk,
            $parentTask->getFinishCount(),
            $parentTask->getRatio(),
            $parentTask->getReason(),
            $parentTask->getStatus(),
            $parentTask->getStatusTime(),
            $parentTask->getCreateTime(),
            $parentTask->getUpdateTime()
        );

        return $parentTaskSdk;
    }
}
