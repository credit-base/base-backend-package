<?php
namespace Base\WorkOrderTask\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\Model\NullWorkOrderTask;

use BaseSdk\WorkOrderTask\Model\WorkOrderTask as WorkOrderTaskSdk;
use BaseSdk\WorkOrderTask\Model\NullWorkOrderTask as NullWorkOrderTaskSdk;

use Base\UserGroup\Translator\UserGroupSdkTranslator;

class WorkOrderTaskSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    protected function getTemplateTranslatorFactory() : TemplateTranslatorFactory
    {
        return new TemplateTranslatorFactory();
    }

    protected function getTemplateSdkTranslator(int $type) : ISdkTranslator
    {
        return $this->getTemplateTranslatorFactory()->getTranslator($type);
    }
    
    protected function getUserGroupSdkTranslator() : ISdkTranslator
    {
        return new UserGroupSdkTranslator();
    }
    
    protected function getParentTaskSdkTranslator() : ISdkTranslator
    {
        return new ParentTaskSdkTranslator();
    }
    
    public function valueObjectToObject($workOrderTaskSdk = null, $workOrderTask = null)
    {
        return $this->translateToObject($workOrderTaskSdk, $workOrderTask);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($workOrderTaskSdk = null, $workOrderTask = null) : WorkOrderTask
    {
        if (!$workOrderTaskSdk instanceof WorkOrderTaskSdk || $workOrderTaskSdk instanceof INull) {
            return NullWorkOrderTask::getInstance();
        }

        if ($workOrderTask == null) {
            $workOrderTask = new WorkOrderTask();
        }

        $parentTask = $this->getParentTaskSdkTranslator()->valueObjectToObject($workOrderTaskSdk->getParentTask());
        $assignObject = $this->getUserGroupSdkTranslator()->valueObjectToObject($workOrderTaskSdk->getAssignObject());
        $template = $this->getTemplateSdkTranslator(
            $parentTask->getTemplateType()
        )->valueObjectToObject($workOrderTaskSdk->getTemplate());

        $workOrderTask->setId($workOrderTaskSdk->getId());
        $workOrderTask->setParentTask($parentTask);
        $workOrderTask->setTemplate($template);
        $workOrderTask->setAssignObject($assignObject);
        $workOrderTask->setIsExistedTemplate($workOrderTaskSdk->getIsExistedTemplate());
        $workOrderTask->setFeedbackRecords($workOrderTaskSdk->getFeedbackRecords());
        $workOrderTask->setReason($workOrderTaskSdk->getReason());
        $workOrderTask->setCreateTime($workOrderTaskSdk->getCreateTime());
        $workOrderTask->setStatusTime($workOrderTaskSdk->getStatusTime());
        $workOrderTask->setUpdateTime($workOrderTaskSdk->getUpdateTime());
        $workOrderTask->setStatus($workOrderTaskSdk->getStatus());

        return $workOrderTask;
    }

    public function objectToValueObject($workOrderTask)
    {
        if (!$workOrderTask instanceof WorkOrderTask) {
            return NullWorkOrderTaskSdk::getInstance();
        }

        $parentTaskSdk = $this->getParentTaskSdkTranslator()->objectToValueObject($workOrderTask->getParentTask());
        $assignObjectSdk = $this->getUserGroupSdkTranslator()->objectToValueObject($workOrderTask->getAssignObject());
        $templateSdk = $this->getTemplateSdkTranslator(
            $parentTaskSdk->getTemplateType()
        )->objectToValueObject($workOrderTask->getTemplate());

        $workOrderTaskSdk = new WorkOrderTaskSdk(
            $workOrderTask->getId(),
            $parentTaskSdk,
            $templateSdk,
            $assignObjectSdk,
            $workOrderTask->getIsExistedTemplate(),
            $workOrderTask->getFeedbackRecords(),
            $workOrderTask->getReason(),
            $workOrderTask->getStatus(),
            $workOrderTask->getStatusTime(),
            $workOrderTask->getCreateTime(),
            $workOrderTask->getUpdateTime()
        );

        return $workOrderTaskSdk;
    }
}
