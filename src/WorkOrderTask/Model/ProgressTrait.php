<?php
namespace Base\WorkOrderTask\Model;

use Marmot\Core;

use Base\Template\Model\WbjTemplate;

trait ProgressTrait
{
    /**
     * @var string $reason 撤销原因或终结原因
     */
    protected $reason;
    /**
     * @var array $feedbackRecords 反馈记录
     */
    protected $feedbackRecords;

    public function setReason(string $reason) : void
    {
        $this->reason = $reason;
    }

    public function getReason() : string
    {
        return $this->reason;
    }

    public function setFeedbackRecords(array $feedbackRecords) : void
    {
        $this->feedbackRecords = $feedbackRecords;
    }

    public function getFeedbackRecords() : array
    {
        return $this->feedbackRecords;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['DQR'];
    }

    public function revoke() : bool
    {
        // 撤销【当子任务状态为待确认时，可撤销】；说明撤销原因
        if (!$this->isDqr()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        return $this->getRepository()->revoke($this, array('reason'));
    }

    public function confirm() : bool
    {
        // 确认【当子任务状态为待确认、跟进中时，可确认】
        if (!$this->isDqr() && !$this->isGjz()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        return $this->getRepository()->confirm($this);
    }

    public function end() : bool
    {
        // 终结【当子任务状态为跟进中时，可终结】；说明终结原因
        if (!$this->isGjz()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        return $this->getRepository()->end($this, array('reason'));
    }

    public function feedback() : bool
    {
        // 反馈【当子任务状态为待确认、跟进中时，可反馈】
        if (!$this->isDqr() && !$this->isGjz()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        return $this->getRepository()->feedback($this, array('feedbackRecords'));
    }

    protected function isDqr() : bool
    {
        return $this->getStatus() == self::STATUS['DQR'];
    }

    protected function isGjz() : bool
    {
        return $this->getStatus() == self::STATUS['GJZ'];
    }
}
