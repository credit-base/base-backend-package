<?php
namespace Base\WorkOrderTask\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullWorkOrderTask extends WorkOrderTask implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function revoke() : bool
    {
        return $this->resourceNotExist();
    }

    public function confirm() : bool
    {
        return $this->resourceNotExist();
    }

    public function end() : bool
    {
        return $this->resourceNotExist();
    }

    public function feedback() : bool
    {
        return $this->resourceNotExist();
    }

    public function isDqr() : bool
    {
        return $this->resourceNotExist();
    }

    public function isGjz() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
