<?php
namespace Base\WorkOrderTask\Model;

interface IProgress
{
    /**
     * @var STATUS['DQR']  待确认，默认
     * @var STATUS['YCX']  已撤销
     * @var STATUS['YQR']  已确认
     * @var STATUS['GJZ']  跟进中
     * @var STATUS['YZJ']  已终结
     */
    const STATUS = array(
        'DQR' => 0,
        'YCX' => 1,
        'YQR' => 2,
        'GJZ' => 3,
        'YZJ' => 4
    );

    public function revoke() : bool;

    public function confirm() : bool;

    public function end() : bool;

    public function feedback() : bool;
}
