<?php
namespace Base\WorkOrderTask\Command\ParentTask;

use Marmot\Interfaces\ICommand;

class RevokeParentTaskCommand implements ICommand
{
    public $id;
    public $reason;

    public function __construct(
        string $reason,
        int $id
    ) {
        $this->reason = $reason;
        $this->id = $id;
    }
}
