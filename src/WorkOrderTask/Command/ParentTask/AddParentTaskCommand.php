<?php
namespace Base\WorkOrderTask\Command\ParentTask;

use Marmot\Interfaces\ICommand;

class AddParentTaskCommand implements ICommand
{
    public $id;
    /**
     * @var string $title 任务标题
     */
    public $title;
    /**
     * @var string $description 任务描述
     */
    public $description;
    /**
     * @var string $endTime 终结时间
     */
    public $endTime;
    /**
     * @var array $attachment 依据附件
     */
    public $attachment;
    /**
     * @var int $templateType 基础目录
     */
    public $templateType;
    /**
     * @var int $template 指派目录
     */
    public $template;
    /**
     * @var array $assignObjects 指派对象
     */
    public $assignObjects;

    public function __construct(
        int $templateType,
        string $title,
        string $description,
        string $endTime,
        array $attachment,
        int $template,
        array $assignObjects,
        int $id = 0
    ) {
        $this->templateType = $templateType;
        $this->title = $title;
        $this->description = $description;
        $this->endTime = $endTime;
        $this->attachment = $attachment;
        $this->template = $template;
        $this->assignObjects = $assignObjects;
        $this->id = $id;
    }
}
