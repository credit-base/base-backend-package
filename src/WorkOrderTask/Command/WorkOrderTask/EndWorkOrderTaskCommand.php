<?php
namespace Base\WorkOrderTask\Command\WorkOrderTask;

use Marmot\Interfaces\ICommand;

class EndWorkOrderTaskCommand implements ICommand
{
    public $id;
    public $reason;

    public function __construct(
        string $reason,
        int $id
    ) {
        $this->reason = $reason;
        $this->id = $id;
    }
}
