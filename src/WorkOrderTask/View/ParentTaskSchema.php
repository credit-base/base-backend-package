<?php
namespace Base\WorkOrderTask\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

use Base\WorkOrderTask\Model\ParentTask;

class ParentTaskSchema extends SchemaProvider
{
    protected $resourceType = 'parentTasks';

    public function getId($parentTask) : int
    {
        return $parentTask->getId();
    }

    public function getAttributes($parentTask) : array
    {
        return [
            'templateType' => $parentTask->getTemplateType(),
            'title' => $parentTask->getTitle(),
            'description' => $parentTask->getDescription(),
            'endTime' => $parentTask->getEndTime(),
            'attachment' => $parentTask->getAttachment(),
            'ratio' => $parentTask->getRatio(),
            'reason' => $parentTask->getReason(),

            'status' => $parentTask->getStatus(),
            'createTime' => $parentTask->getCreateTime(),
            'updateTime' => $parentTask->getUpdateTime(),
            'statusTime' => $parentTask->getStatusTime(),
        ];
    }

    public function getRelationships($parentTask, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'template' => [self::DATA => $parentTask->getTemplate()],
            'assignObjects' => [self::DATA => $parentTask->getAssignObjects()]
        ];
    }
}
