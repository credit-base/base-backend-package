<?php
namespace Base\WorkOrderTask\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class WorkOrderTaskSchema extends SchemaProvider
{
    protected $resourceType = 'workOrderTasks';

    public function getId($workOrderTask) : int
    {
        return $workOrderTask->getId();
    }

    public function getAttributes($workOrderTask) : array
    {
        return [
            'reason' => $workOrderTask->getReason(),
            'feedbackRecords' => $workOrderTask->getFeedbackRecords(),

            'status' => $workOrderTask->getStatus(),
            'createTime' => $workOrderTask->getCreateTime(),
            'updateTime' => $workOrderTask->getUpdateTime(),
            'statusTime' => $workOrderTask->getStatusTime(),
        ];
    }

    public function getRelationships($workOrderTask, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'template' => [self::DATA => $workOrderTask->getTemplate()],
            'assignObject' => [self::DATA => $workOrderTask->getAssignObject()],
            'parentTask' => [self::DATA => $workOrderTask->getParentTask()]
        ];
    }
}
