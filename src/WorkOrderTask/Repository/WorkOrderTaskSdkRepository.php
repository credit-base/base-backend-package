<?php
namespace Base\WorkOrderTask\Repository;

use Marmot\Framework\Classes\Repository;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Base\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskSdkAdapter;
use Base\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskMockAdapter;

class WorkOrderTaskSdkRepository extends Repository implements IWorkOrderTaskAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new WorkOrderTaskSdkAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IWorkOrderTaskAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IWorkOrderTaskAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IWorkOrderTaskAdapter
    {
        return new WorkOrderTaskMockAdapter();
    }

    public function fetchOne($id) : WorkOrderTask
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function confirm(WorkOrderTask $workOrderTask) : bool
    {
        return $this->getAdapter()->confirm($workOrderTask);
    }
    
    public function end(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        return $this->getAdapter()->end($workOrderTask, $keys);
    }

    public function feedback(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        return $this->getAdapter()->feedback($workOrderTask, $keys);
    }
    
    public function revoke(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        return $this->getAdapter()->revoke($workOrderTask, $keys);
    }
}
