<?php
namespace Base\WorkOrderTask\Adapter\ParentTask;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\WorkOrderTask\Model\ParentTask;
use Base\WorkOrderTask\Model\NullParentTask;
use Base\WorkOrderTask\Translator\ParentTaskSdkTranslator;

use BaseSdk\WorkOrderTask\Repository\ParentTaskRestfulRepository;

class ParentTaskSdkAdapter implements IParentTaskAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    public function __construct()
    {
        $this->translator = new ParentTaskSdkTranslator();
        $this->repository = new ParentTaskRestfulRepository();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    public function getRestfulRepository() : ParentTaskRestfulRepository
    {
        return $this->repository;
    }

    protected function getNullObject() : INull
    {
        return NullParentTask::getInstance();
    }

    public function fetchOne($id) : ParentTask
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $number, $size);
    }

    public function add(ParentTask $parentTask) : bool
    {
        return $this->addAction($parentTask);
    }

    public function revoke(ParentTask $parentTask, array $keys = array()) : bool
    {
        $parentTaskSdk = $this->getTranslator()->objectToValueObject($parentTask, $keys);

        $result = $this->getRestfulRepository()->revoke($parentTaskSdk, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }
        return true;
    }
}
