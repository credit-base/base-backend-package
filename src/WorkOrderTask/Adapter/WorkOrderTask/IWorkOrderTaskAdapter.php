<?php
namespace Base\WorkOrderTask\Adapter\WorkOrderTask;

use Base\WorkOrderTask\Model\WorkOrderTask;

interface IWorkOrderTaskAdapter
{
    public function fetchOne($id) : WorkOrderTask;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function confirm(WorkOrderTask $workOrderTask) : bool;

    public function end(WorkOrderTask $workOrderTask, array $keys = array()) : bool;

    public function feedback(WorkOrderTask $workOrderTask, array $keys = array()) : bool;

    public function revoke(WorkOrderTask $workOrderTask, array $keys = array()) : bool;
}
