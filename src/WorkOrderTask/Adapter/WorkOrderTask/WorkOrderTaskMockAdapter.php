<?php
namespace Base\WorkOrderTask\Adapter\WorkOrderTask;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\Utils\WorkOrderTaskMockFactory;

class WorkOrderTaskMockAdapter implements IWorkOrderTaskAdapter
{
    public function fetchOne($id) : WorkOrderTask
    {
        return WorkOrderTaskMockFactory::generateWorkOrderTask($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $workOrderTaskList = array();

        foreach ($ids as $id) {
            $workOrderTaskList[$id] = WorkOrderTaskMockFactory::generateWorkOrderTask($id);
        }

        return $workOrderTaskList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function confirm(WorkOrderTask $workOrderTask) : bool
    {
        unset($workOrderTask);
        return true;
    }

    public function end(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        unset($workOrderTask);
        unset($keys);
        return true;
    }

    public function feedback(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        unset($workOrderTask);
        unset($keys);
        return true;
    }

    public function revoke(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        unset($workOrderTask);
        unset($keys);
        return true;
    }
}
