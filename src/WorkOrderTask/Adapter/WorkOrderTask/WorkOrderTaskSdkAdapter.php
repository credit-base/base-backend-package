<?php
namespace Base\WorkOrderTask\Adapter\WorkOrderTask;

use Marmot\Interfaces\INull;

use Base\Common\Adapter\SdkAdapterTrait;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Adapter\CommonMapErrorsTrait;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\Model\NullWorkOrderTask;
use Base\WorkOrderTask\Translator\WorkOrderTaskSdkTranslator;

use BaseSdk\WorkOrderTask\Repository\WorkOrderTaskRestfulRepository;

use Base\UserGroup\Repository\UserGroupRepository;

class WorkOrderTaskSdkAdapter implements IWorkOrderTaskAdapter
{
    use SdkAdapterTrait, CommonMapErrorsTrait;

    private $translator;

    private $repository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->translator = new WorkOrderTaskSdkTranslator();
        $this->repository = new WorkOrderTaskRestfulRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->repository);
        unset($this->userGroupRepository);
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function getRestfulRepository() : WorkOrderTaskRestfulRepository
    {
        return $this->repository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }
    
    protected function getTranslator() : ISdkTranslator
    {
        return $this->translator;
    }

    protected function getNullObject() : INull
    {
        return NullWorkOrderTask::getInstance();
    }

    public function fetchOne($id) : WorkOrderTask
    {
        $workOrderTask = $this->fetchOneAction($id);

        $this->fetchAssignObject($workOrderTask);

        return $workOrderTask;
    }

    public function fetchList(array $ids) : array
    {
        $workOrderTaskList = $this->fetchListAction($ids);
        $this->fetchAssignObject($workOrderTaskList);

        return $workOrderTaskList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {

        list($workOrderTaskList, $count) = $this->filterAction($filter, $sort, $number, $size);
        $this->fetchAssignObject($workOrderTaskList);
        
        return [$workOrderTaskList, $count];
    }

    public function confirm(WorkOrderTask $workOrderTask) : bool
    {
        $workOrderTaskSdk = $this->getTranslator()->objectToValueObject($workOrderTask);

        $result = $this->getRestfulRepository()->confirm($workOrderTaskSdk);

        if (!$result) {
            $this->mapErrors();
            return false;
        }
        return true;
    }

    public function end(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        $workOrderTaskSdk = $this->getTranslator()->objectToValueObject($workOrderTask, $keys);

        $result = $this->getRestfulRepository()->end($workOrderTaskSdk, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }
        return true;
    }

    public function feedback(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        $workOrderTaskSdk = $this->getTranslator()->objectToValueObject($workOrderTask, $keys);

        $result = $this->getRestfulRepository()->feedback($workOrderTaskSdk, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }
        return true;
    }

    public function revoke(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        $workOrderTaskSdk = $this->getTranslator()->objectToValueObject($workOrderTask, $keys);

        $result = $this->getRestfulRepository()->revoke($workOrderTaskSdk, $keys);

        if (!$result) {
            $this->mapErrors();
            return false;
        }
        return true;
    }

    protected function fetchAssignObject($workOrderTask)
    {
        return is_array($workOrderTask) ?
        $this->fetchAssignObjectByList($workOrderTask) :
        $this->fetchAssignObjectByObject($workOrderTask);
    }

    protected function fetchAssignObjectByObject(WorkOrderTask $workOrderTask)
    {
        $assignObjectId = $workOrderTask->getAssignObject()->getId();
        $assignObject = $this->getUserGroupRepository()->fetchOne($assignObjectId);
        $workOrderTask->setAssignObject($assignObject);
    }

    protected function fetchAssignObjectByList(array $workOrderTaskList)
    {
        $assignObjectIds = array();
        foreach ($workOrderTaskList as $workOrderTask) {
            $assignObjectIds[] = $workOrderTask->getAssignObject()->getId();
        }

        $assignObjectList = $this->getUserGroupRepository()->fetchList($assignObjectIds);
        if (!empty($assignObjectList)) {
            foreach ($workOrderTaskList as $workOrderTask) {
                $assignObjectId = $workOrderTask->getAssignObject()->getId();
                if (isset($assignObjectList[$assignObjectId])
                    && ($assignObjectList[$assignObjectId]->getId() == $assignObjectId)) {
                    $workOrderTask->setAssignObject($assignObjectList[$assignObjectId]);
                }
            }
        }
    }
}
