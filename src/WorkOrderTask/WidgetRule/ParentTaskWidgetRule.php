<?php
namespace Base\WorkOrderTask\WidgetRule;

use Respect\Validation\Validator as V;

use Marmot\Core;

use Base\WorkOrderTask\Model\ParentTask;
use Base\Common\WidgetRule\CommonWidgetRule;

class ParentTaskWidgetRule
{
    public function templateType($templateType) : bool
    {
        if (!V::intVal()->validate($templateType) || !in_array($templateType, ParentTask::TEMPLATE_TYPE)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'templateType'));
            return false;
        }
        return true;
    }

    const TITLE_MIN_LENGTH = 1;
    const TITLE_MAX_LENGTH = 100;
    public function title($title) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::TITLE_MIN_LENGTH,
            self::TITLE_MAX_LENGTH
        )->validate($title)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'title'));
            return false;
        }
        return true;
    }

    const DESCRIPTION_MIN_LENGTH = 1;
    const DESCRIPTION_MAX_LENGTH = 2000;
    public function description($description) : bool
    {
        if (!empty($description)) {
            if (!V::charset('UTF-8')->stringType()->length(
                self::DESCRIPTION_MIN_LENGTH,
                self::DESCRIPTION_MAX_LENGTH
            )->validate($description)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'description'));
                return false;
            }
        }
        return true;
    }

    public function endTime($endTime) : bool
    {
        if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $endTime)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'endTime'));
            return false;
        }
        
        list($year, $month, $day) = explode('-', $endTime);
        if (!checkdate($month, $day, $year)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'endTime'));
            return false;
        }
        return true;
    }

    public function pdf($attachment) : bool
    {
        $commonWidgetRule = new CommonWidgetRule();
        if (!empty($attachment)) {
            return $commonWidgetRule->pdf($attachment);
        }
        return true;
    }
}
