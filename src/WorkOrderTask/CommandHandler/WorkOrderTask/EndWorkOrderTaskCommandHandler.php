<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;
use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Base\WorkOrderTask\Repository\WorkOrderTaskSdkRepository;

class EndWorkOrderTaskCommandHandler implements ICommandHandler
{
    use WorkOrderTaskCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EndWorkOrderTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $workOrderTask = $this->fetchWorkOrderTask($command->id);
        $workOrderTask->setReason($command->reason);

        return $workOrderTask->end();
    }
}
