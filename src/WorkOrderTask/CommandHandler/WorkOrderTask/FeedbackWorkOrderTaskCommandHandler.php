<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;
use Marmot\Core;

use Base\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;
use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Base\WorkOrderTask\Repository\WorkOrderTaskSdkRepository;

class FeedbackWorkOrderTaskCommandHandler implements ICommandHandler
{
    use WorkOrderTaskCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof FeedbackWorkOrderTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $workOrderTask = $this->fetchWorkOrderTask($command->id);
        $workOrderTask->setFeedbackRecords($command->feedbackRecords);

        return $workOrderTask->feedback();
    }
}
