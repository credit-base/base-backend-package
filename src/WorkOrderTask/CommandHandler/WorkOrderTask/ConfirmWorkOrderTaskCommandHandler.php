<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;

class ConfirmWorkOrderTaskCommandHandler implements ICommandHandler
{
    use WorkOrderTaskCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof ConfirmWorkOrderTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $workOrderTask = $this->fetchWorkOrderTask($command->id);

        return $workOrderTask->confirm();
    }
}
