<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class WorkOrderTaskCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand' =>
        'Base\WorkOrderTask\CommandHandler\WorkOrderTask\RevokeWorkOrderTaskCommandHandler',
        'Base\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand' =>
        'Base\WorkOrderTask\CommandHandler\WorkOrderTask\ConfirmWorkOrderTaskCommandHandler',
        'Base\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand' =>
        'Base\WorkOrderTask\CommandHandler\WorkOrderTask\EndWorkOrderTaskCommandHandler',
        'Base\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand' =>
        'Base\WorkOrderTask\CommandHandler\WorkOrderTask\FeedbackWorkOrderTaskCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
