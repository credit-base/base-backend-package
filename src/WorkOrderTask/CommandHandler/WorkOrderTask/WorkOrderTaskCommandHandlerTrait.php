<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Base\WorkOrderTask\Repository\WorkOrderTaskSdkRepository;
use Base\WorkOrderTask\Model\WorkOrderTask;

trait WorkOrderTaskCommandHandlerTrait
{
    protected function getWorkOrderTaskSdkRepository() : IWorkOrderTaskAdapter
    {
        return new WorkOrderTaskSdkRepository();
    }

    protected function fetchWorkOrderTask($id) : WorkOrderTask
    {
        return $this->getWorkOrderTaskSdkRepository()->fetchOne($id);
    }
}
