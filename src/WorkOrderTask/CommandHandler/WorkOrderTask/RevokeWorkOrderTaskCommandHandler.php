<?php
namespace Base\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand;
use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Base\WorkOrderTask\Repository\WorkOrderTaskRepository;

class RevokeWorkOrderTaskCommandHandler implements ICommandHandler
{
    use WorkOrderTaskCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeWorkOrderTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $workOrderTask = $this->fetchWorkOrderTask($command->id);
        $workOrderTask->setReason($command->reason);
        
        return $workOrderTask->revoke();
    }
}
