<?php
namespace Base\WorkOrderTask\CommandHandler\ParentTask;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ParentTaskCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Base\WorkOrderTask\Command\ParentTask\AddParentTaskCommand' =>
        'Base\WorkOrderTask\CommandHandler\ParentTask\AddParentTaskCommandHandler',
        'Base\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand' =>
        'Base\WorkOrderTask\CommandHandler\ParentTask\RevokeParentTaskCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
