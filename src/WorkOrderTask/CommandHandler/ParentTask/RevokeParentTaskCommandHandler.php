<?php
namespace Base\WorkOrderTask\CommandHandler\ParentTask;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Base\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;
use Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use Base\WorkOrderTask\Repository\ParentTaskSdkRepository;

class RevokeParentTaskCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new ParentTaskSdkRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getParentTaskSdkRepository() : IParentTaskAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeParentTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getParentTaskSdkRepository();
        $parentTask = $repository->fetchOne($command->id);
        $parentTask->setReason($command->reason);
        
        return $parentTask->revoke();
    }
}
