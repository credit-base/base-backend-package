<?php
namespace Base\WorkOrderTask\CommandHandler\ParentTask;

use Base\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use Base\Template\Repository\GbTemplateSdkRepository;
use Base\Template\Model\GbTemplate;

use Base\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use Base\Template\Repository\BjTemplateSdkRepository;
use Base\Template\Model\BjTemplate;

use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use Base\UserGroup\Repository\UserGroupRepository;

trait ParentTaskCommandHandlerTrait
{
    protected function getGbTemplateSdkRepository() : IGbTemplateAdapter
    {
        return new GbTemplateSdkRepository();
    }

    protected function getBjTemplateSdkRepository() : IBjTemplateAdapter
    {
        return new BjTemplateSdkRepository();
    }
    
    protected function getUserGroupRepository() : IUserGroupAdapter
    {
        return new UserGroupRepository();
    }

    protected function fetchOneGbTemplate($id) : GbTemplate
    {
        return $this->getGbTemplateSdkRepository()->fetchOne($id);
    }

    protected function fetchOneBjTemplate($id) : BjTemplate
    {
        return $this->getBjTemplateSdkRepository()->fetchOne($id);
    }

    protected function fetchListUserGroup(array $ids) : array
    {
        return $this->getUserGroupRepository()->fetchList($ids);
    }
}
