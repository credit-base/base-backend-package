<?php
namespace Base\WorkOrderTask\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use Base\WorkOrderTask\Model\WorkOrderTask;
use Base\WorkOrderTask\View\WorkOrderTaskView;

use Base\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;
use Base\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand;
use Base\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;
use Base\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;

class WorkOrderTaskProgressController extends Controller
{
    use JsonApiTrait, WorkOrderTaskControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 撤销任务,通过PATCH传参
     * 对应路由 /workOrderTasks/{id:\d+}/revoke
     * @param int id 任务 id
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        
        $reason = $attributes['reason'];

        if ($this->validateRevokeAndEndScenario($reason)) {
            $commandBus = $this->getCommandBus();

            $command = new RevokeWorkOrderTaskCommand(
                $reason,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $workOrderTask = $repository->fetchOne($id);
                if ($workOrderTask instanceof WorkOrderTask) {
                    $this->render(new WorkOrderTaskView($workOrderTask));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 确认,通过PATCH传参
     * 对应路由 /workOrderTasks/{id:\d+}/confirm
     * @param int id 任务 id
     * @return jsonApi
     */
    public function confirm(int $id)
    {
        $commandBus = $this->getCommandBus();

        $command = new ConfirmWorkOrderTaskCommand(
            $id
        );

        if ($commandBus->send($command)) {
            $repository = $this->getRepository();
            $workOrderTask = $repository->fetchOne($id);
            if ($workOrderTask instanceof WorkOrderTask) {
                $this->render(new WorkOrderTaskView($workOrderTask));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 终结,通过PATCH传参
     * 对应路由 /workOrderTasks/{id:\d+}/end
     * @param int id 任务 id
     * @return jsonApi
     */
    public function end(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        
        $reason = $attributes['reason'];

        if ($this->validateRevokeAndEndScenario($reason)) {
            $commandBus = $this->getCommandBus();

            $command = new EndWorkOrderTaskCommand(
                $reason,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $workOrderTask = $repository->fetchOne($id);
                if ($workOrderTask instanceof WorkOrderTask) {
                    $this->render(new WorkOrderTaskView($workOrderTask));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateRevokeAndEndScenario(
        $reason
    ) : bool {
        return $this->getCommonWidgetRule()->reason($reason);
    }

    /**
     * 反馈,通过PATCH传参
     * 对应路由 /workOrderTasks/{id:\d+}/feedback
     * @param int id 任务 id
     * @return jsonApi
     */
    public function feedback(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        
        $feedbackRecords = $attributes['feedbackRecords'];

        if ($this->validateFeedbackScenario($feedbackRecords)) {
            $commandBus = $this->getCommandBus();

            $command = new FeedbackWorkOrderTaskCommand(
                $feedbackRecords,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $workOrderTask = $repository->fetchOne($id);
                if ($workOrderTask instanceof WorkOrderTask) {
                    $this->render(new WorkOrderTaskView($workOrderTask));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateFeedbackScenario(
        $feedbackRecords
    ) : bool {
        return $this->getWorkOrderTaskWidgetRule()->feedbackRecords($feedbackRecords);
    }
}
