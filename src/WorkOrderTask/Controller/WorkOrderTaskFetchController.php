<?php
namespace Base\WorkOrderTask\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Base\Common\Controller\FetchControllerTrait;
use Marmot\Framework\Common\Controller\IFetchController;

use Base\WorkOrderTask\View\WorkOrderTaskView;
use Base\WorkOrderTask\Repository\WorkOrderTaskSdkRepository;
use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class WorkOrderTaskFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WorkOrderTaskSdkRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IWorkOrderTaskAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new WorkOrderTaskView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'workOrderTasks';
    }
}
