<?php
namespace Base\WorkOrderTask\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\WorkOrderTask\WidgetRule\ParentTaskWidgetRule;
use Base\WorkOrderTask\Repository\ParentTaskSdkRepository;
use Base\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use Base\WorkOrderTask\CommandHandler\ParentTask\ParentTaskCommandHandlerFactory;

trait ParentTaskControllerTrait
{
    protected function getParentTaskWidgetRule() : ParentTaskWidgetRule
    {
        return new ParentTaskWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : IParentTaskAdapter
    {
        return new ParentTaskSdkRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new ParentTaskCommandHandlerFactory());
    }
}
