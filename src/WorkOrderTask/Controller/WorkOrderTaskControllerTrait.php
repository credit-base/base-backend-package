<?php
namespace Base\WorkOrderTask\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Common\WidgetRule\CommonWidgetRule;

use Base\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;
use Base\WorkOrderTask\Repository\WorkOrderTaskSdkRepository;
use Base\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Base\WorkOrderTask\CommandHandler\WorkOrderTask\WorkOrderTaskCommandHandlerFactory;

trait WorkOrderTaskControllerTrait
{
    protected function getWorkOrderTaskWidgetRule() : WorkOrderTaskWidgetRule
    {
        return new WorkOrderTaskWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : IWorkOrderTaskAdapter
    {
        return new WorkOrderTaskSdkRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new WorkOrderTaskCommandHandlerFactory());
    }
}
