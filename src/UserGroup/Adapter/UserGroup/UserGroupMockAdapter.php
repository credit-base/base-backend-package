<?php
namespace Base\UserGroup\Adapter\UserGroup;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Utils\MockFactory;

class UserGroupMockAdapter implements IUserGroupAdapter
{
    public function fetchOne($id) : UserGroup
    {
        return MockFactory::generateUserGroup($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $userGroupList = array();

        foreach ($ids as $id) {
            $userGroupList[$id] = MockFactory::generateUserGroup($id);
        }

        return $userGroupList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }
}
