<?php
namespace Base\UserGroup\Adapter\UserGroup\Query\Persistence;

use Marmot\Framework\Classes\Db;

class UserGroupDb extends Db
{
    const TABLE = 'user_group';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}
