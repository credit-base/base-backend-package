<?php
namespace Base\UserGroup\Adapter\UserGroup\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class UserGroupCache extends Cache
{
    const KEY = 'user_group';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}
