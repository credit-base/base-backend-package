<?php
namespace Base\UserGroup\Adapter\UserGroup\Query;

use Marmot\Framework\Query\RowCacheQuery;

class UserGroupRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'user_group_id',
            new Persistence\UserGroupCache(),
            new Persistence\UserGroupDb()
        );
    }
}
