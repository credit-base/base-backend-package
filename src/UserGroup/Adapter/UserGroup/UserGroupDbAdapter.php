<?php
namespace Base\UserGroup\Adapter\UserGroup;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Model\NullUserGroup;
use Base\UserGroup\Translator\UserGroupDbTranslator;
use Base\UserGroup\Adapter\UserGroup\Query\UserGroupRowCacheQuery;

class UserGroupDbAdapter implements IUserGroupAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new UserGroupDbTranslator();
        $this->rowCacheQuery = new UserGroupRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : UserGroupDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : UserGroupRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullUserGroup::getInstance();
    }

    public function fetchOne($id) : UserGroup
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $userGroup = new UserGroup();

            if (isset($filter['name'])) {
                $userGroup->setName($filter['name']);
                $info = $this->getDbTranslator()->objectToArray($userGroup, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new UserGroup(), array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}
