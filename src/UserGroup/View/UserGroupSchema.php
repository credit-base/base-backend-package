<?php
namespace Base\UserGroup\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class UserGroupSchema extends SchemaProvider
{
    protected $resourceType = 'userGroups';

    public function getId($userGroup) : int
    {
        return $userGroup->getId();
    }

    public function getAttributes($userGroup) : array
    {
        return [
            'name' => $userGroup->getName(),
            'shortName' => $userGroup->getShortName(),

            'status' => $userGroup->getStatus(),
            'createTime' => $userGroup->getCreateTime(),
            'updateTime' => $userGroup->getUpdateTime(),
            'statusTime' => $userGroup->getStatusTime(),
        ];
    }
}
