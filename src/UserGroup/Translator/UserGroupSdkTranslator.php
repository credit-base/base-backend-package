<?php
namespace Base\UserGroup\Translator;

use Marmot\Interfaces\INull;
use Base\Common\Translator\ISdkTranslator;
use Base\Common\Translator\SdkTranslatorTrait;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Model\NullUserGroup;

use BaseSdk\UserGroup\Model\UserGroup as UserGroupSdk;
use BaseSdk\UserGroup\Model\NullUserGroup as NullUserGroupSdk;

class UserGroupSdkTranslator implements ISdkTranslator
{
    use SdkTranslatorTrait;

    public function valueObjectToObject($userGroupSdk = null, $userGroup = null)
    {
        return $this->translateToObject($userGroupSdk, $userGroup);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject($userGroupSdk = null, $userGroup = null) : UserGroup
    {
        if (!$userGroupSdk instanceof UserGroupSdk || $userGroupSdk instanceof INull) {
            return NullUserGroup::getInstance();
        }

        if ($userGroup == null) {
            $userGroup = new UserGroup();
        }

        $userGroup->setId($userGroupSdk->getId());
        
        return $userGroup;
    }

    public function objectToValueObject($userGroup)
    {
        if (!$userGroup instanceof UserGroup) {
            return NullUserGroupSdk::getInstance();
        }

        $userGroupSdk = new UserGroupSdk(
            $userGroup->getId()
        );

        return $userGroupSdk;
    }
}
