<?php
namespace Base\UserGroup\Translator;

use Marmot\Interfaces\ITranslator;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Model\NullUserGroup;

class UserGroupDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $userGroup = null) : UserGroup
    {
        if (!isset($expression['user_group_id'])) {
            return NullUserGroup::getInstance();
        }

        if ($userGroup == null) {
            $userGroup = new UserGroup($expression['user_group_id']);
        }
        $userGroup->setName($expression['name']);
        $userGroup->setShortName($expression['short_name']);

        $userGroup->setStatus($expression['status']);
        $userGroup->setCreateTime($expression['create_time']);
        $userGroup->setUpdateTime($expression['update_time']);
        $userGroup->setStatusTime($expression['status_time']);

        return $userGroup;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($userGroup, array $keys = array()) : array
    {
        if (!$userGroup instanceof UserGroup) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'shortName',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['user_group_id'] = $userGroup->getId();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $userGroup->getName();
        }
        if (in_array('shortName', $keys)) {
            $expression['short_name'] = $userGroup->getShortName();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $userGroup->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $userGroup->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $userGroup->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $userGroup->getStatusTime();
        }

        return $expression;
    }
}
