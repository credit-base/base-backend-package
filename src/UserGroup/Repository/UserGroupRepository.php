<?php
namespace Base\UserGroup\Repository;

use Marmot\Framework\Classes\Repository;

use Base\UserGroup\Model\UserGroup;
use Base\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use Base\UserGroup\Adapter\UserGroup\UserGroupDbAdapter;
use Base\UserGroup\Adapter\UserGroup\UserGroupMockAdapter;

class UserGroupRepository extends Repository implements IUserGroupAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new UserGroupDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IUserGroupAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getActualAdapter() : IUserGroupAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IUserGroupAdapter
    {
        return new UserGroupMockAdapter();
    }

    public function fetchOne($id) : UserGroup
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
