<?php
namespace Base\UserGroup\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class UserGroup implements IObject
{
    use Object;

    const STATUS_NORMAL = 0;

    /**
     * @var int $id
     */
    protected $id;
    /**
     * @var string $name 委办局名称
     */
    protected $name;
    /**
     * @var string $shortName 委办局简称
     */
    protected $shortName;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->shortName = '';
        $this->status = STATUS_NORMAL;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->shortName);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setShortName(string $shortName) : void
    {
        $this->shortName = $shortName;
    }

    public function getShortName() : string
    {
        return $this->shortName;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
