<?php
ini_set("display_errors","on");

return [
    //database
    'database.host'     => '172.25.1.11',
    'database.port'     => 5555,
    'database.dbname'   => 'credit_ty',
    'database.user'     => 'credit_ty',
    'database.password' => 'credit_ty',
    'database.tablepre' => 'pcore_',
    'database.charset' => 'utf8mb4',
    //mongo
    'mongo.host' => '',
    'mongo.uriOptions' => [
    ],
    'mongo.driverOptions' => [
    ],
    //cache
    'cache.route.disable' => false,
    //memcached
    'memcached.service'=>[
        ['credit-backend-memcached-0.credit-backend-memcached',11211],
        ['credit-backend-memcached-1.credit-backend-memcached',11211]
    ],
    //附件上传路径
    'attachment.upload.path' => '/var/www/html/attachment/upload/',
    //附件下载路径
    'attachment.download.path' => '/var/www/html/attachment/download/',
];
