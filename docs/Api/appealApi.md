# 异议申诉接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [新增](#新增)
	* [受理](#受理)
	* [撤销](#撤销)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [互动类项目字典](../Dictionary/interaction.md "互动类项目字典")
	* [前台用户项目字典](../Dictionary/member.md "前台用户项目字典")
	* [受理委办局项目字典](../Dictionary/UserGroup.md "受理委办局项目字典")
	* [异议申诉项目字典](../Dictionary/appeal.md "异议申诉项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [互动类通用控件规范](../WidgetRule/interaction.md "互动类通用控件规范")
	* [异议申诉控件规范](../WidgetRule/appeal.md "异议申诉控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	100=>array(
		'memberId'=>前台用户不能为空
		'acceptUserGroupId'=>受理委办局不能为空
		'crewId' => 受理人不能为空
	)
	101=>array(
		'title'=>标题格式不正确
		'content'=>内容格式不正确
		'memberId'=>前台用户格式不正确
		'acceptUserGroupId'=>受理委办局格式不正确
		'name'=>姓名/企业名称格式不正确
		'identify'=>统一社会信用代码/反馈人身份证号格式不正确
		'certificates'=>上传身份证/营业执照格式不正确
		'type'=>类型格式不正确
		'contact'=>联系方式格式不正确
		'images'=>图片格式不正确
		'imagesCount'=>图片数量超出限制
		'replyContent'=>回复内容格式不正确
		'replyImages'=>回复图片格式不正确
		'replyImagesCount'=>回复图片数量超出限制
		'admissibility'=>受理情况格式不正确
		'replyCrew'=>受理人格式不正确
	),
	102=>array(
		'applyStatus'=>审核状态不能操作
		'status'=>状态不能操作
	)
    ```
	
## <a name="参数说明">参数说明</a>
     
### appeal

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| title           | string     | 是            | 个人申诉                                       | 标题            |
| content         | string     | 是            | 申诉内容                                       | 内容            |
| member          | int        | 是            | 1                                            | 前台用户         |
| acceptUserGroup | int        | 是            | 1                                            | 受理委办局        |
| acceptStatus    | int        | 是            | 0                                            | 受理状态(0 待受理, 1 受理中, 2 受理完成)|
| reply           | int        | 是            | 1                                            | 回复信息        |
| name            | string     | 是            | 张三                                          | 反馈人真实姓名/企业名称 |
| identify        | string     | 是            | 412825199009098976                           | 统一社会信用代码/反馈人身份证号 |
| certificates    | array      | 是            | array(array('name'=>'图片', 'identify'=>'图片.jpg')) | 上传身份证/营业执照          |
| type            | int        | 是            | 0                                            | 类型(1 个人, 2 企业)|
| contact         | string     | 是            | 029-87654332                                 | 联系方式            |
| images          | array      | 否            | array(array('name'=>'图片', 'identify'=>'图片.jpg')) | 图片      |
| status          | int        | 是            | 0                                            | 状态(0 正常, -2 撤销)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### reply

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| content         | string     | 是            | 回复内容                                       | 回复内容        |
| images          | array      | 否            | array(array('name'=>'图片', 'identify'=>'图片.jpg')) | 回复图片  |
| crew            | int        | 是            | 1                                            | 受理人         |
| admissibility   | int        | 是            | 0                                            | 受理情况(1 予以受理, 2 不予受理)|
| status          | int        | 是            | 0                                            | 状态(0 默认)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[members]
	    1.2 fields[userGroups]
	    1.3 fields[crews]
	    1.4 fields[replies]
	    1.5 fields[appeals]
		1.6 include=member,acceptUserGroup,reply,reply.crew
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'appeals/1?fields[appeals]=name',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/appeals/{id:\d+}

示例

	$response = $client->request('GET', 'appeals/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/appeals/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'appeals/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/appeals

	1、检索条件
	    1.1 filter[title] | 根据标题搜索
	    1.2 filter[member] | 根据前台用户搜索,前台用户id
	    1.3 filter[acceptUserGroup] | 根据受理委办局搜索,委办局id
	    1.4 filter[status] | 根据状态搜索 | 0 正常 | -2 撤销
	    1.5 filter[acceptStatus] | 根据受理状态搜索 | 0 待受理 | 1 受理中 | 2 受理完成

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-status | -status 根据状态倒序 | 状态 根据状态正序
		2.3 sort=-acceptStatus | -acceptStatus 根据受理状态倒序 | 受理状态 根据受理状态正序

示例

	$response = $client->request('GET', 'appeals?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="新增">新增</a>
	
路由

	通过POST传参
	/appeals

示例

	$data = array(
		'data' => array(
			"type"=>"appeals",
			"attributes"=>array(
				"title"=>"标题",
				"content"=>"内容",
				"name"=>"反馈人真实姓名/企业名称",
				"identify"=>"统一社会信用代码/反馈人身份证号",
				"certificates"=>array(
					array('name' => '上传身份证/营业执照', 'identify' => 'identify.jpg')
				)
				"type"=>1,
				"contact"=>"联系方式",
				"images"=>array(
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg'),
					array('name' => 'name', 'identify' => 'identify.jpg')
				)
			),
			"relationships"=>array(
				"member"=>array(
					"data"=>array(
						array("type"=>"members","id"=>前台用户id)
					)
				),
				"acceptUserGroup"=>array(
					"data"=>array(
						array("type"=>"userGroups","id"=>受理委办局id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'POST',
	                'appeals',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="受理">受理</a>

路由

	通过PATCH传参
	/appeals/{id:\d+}/accept

示例

	$data = array(
		'data' => array(
			"type"=>"appeals",
			"relationships"=>array(
				"reply"=>array(
					"data"=>array(
						array(
							"type"=>"replies",
							"attributes"=>array(
								"content"=>"回复内容",
								"images"=>array(
									array('name' => '回复图片', 'identify' => 'identify.jpg'),
									array('name' => '回复图片', 'identify' => 'identify.jpg'),
									array('name' => '回复图片', 'identify' => 'identify.jpg')
								),
								"admissibility"=>0
							),
							"relationships"=>array(
								"crew"=>array(
									"data"=>array(
										array("type"=>"crews","id"=>受理人id)
									)
								)
							),
						)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'appeals/1/accept',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="撤销">撤销</a>

路由

	通过PATCH传参
	/appeals/{id:\d+}/revoke

示例

	$response = $client->request(
	                'PATCH',
	                'appeals/1/revoke',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json']
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "appeals",
			"id": "1",
			"attributes": {
				"title": "个人申诉",
				"content": "个人申诉",
				"name": "张三",
				"identify": "412825199009098765",
				"appealType": 1,
				"contact": "联系方式",
				"images": [
					{
						"name": "name",
						"identify": "identify.jpg"
					},
					{
						"name": "name",
						"identify": "identify.jpg"
					},
					{
						"name": "name",
						"identify": "identify.jpg"
					}
				],
				"acceptStatus": 2,
				"certificates": [
					{
						"name": "上传身份证/营业执照",
						"identify": "identify.jpg"
					}
				],
				"status": 0,
				"createTime": 1635498202,
				"updateTime": 1635498810,
				"statusTime": 0
			},
			"relationships": {
				"acceptUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"member": {
					"data": {
						"type": "members",
						"id": "1"
					}
				},
				"reply": {
					"data": {
						"type": "replies",
						"id": "10"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/appeals/1"
			}
		},
		"included": [
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "members",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "18800000000",
					"email": "997809098@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 1,
					"status": 0,
					"createTime": 1621434208,
					"updateTime": 1621435225,
					"statusTime": 1621434736
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "replies",
				"id": "10",
				"attributes": {
					"content": "予以受理",
					"images": [
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635498739,
					"updateTime": 1635498739,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			}
		]
	}
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 5,
			"links": {
				"first": 1,
				"last": 3,
				"prev": null,
				"next": 2
			}
		},
		"links": {
			"first": "127.0.0.1:8089/appeals/?include=member,acceptUserGroup,reply,reply.crew&page[number]=1&page[size]=2",
			"last": "127.0.0.1:8089/appeals/?include=member,acceptUserGroup,reply,reply.crew&page[number]=3&page[size]=2",
			"prev": null,
			"next": "127.0.0.1:8089/appeals/?include=member,acceptUserGroup,reply,reply.crew&page[number]=2&page[size]=2"
		},
		"data": [
			{
				"type": "appeals",
				"id": "1",
				"attributes": {
					"title": "个人申诉",
					"content": "个人申诉",
					"name": "张三",
					"identify": "412825199009098765",
					"appealType": 1,
					"contact": "联系方式",
					"images": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"acceptStatus": 2,
					"certificates": [
						{
							"name": "上传身份证/营业执照",
							"identify": "identify.jpg"
						}
					],
					"status": 0,
					"createTime": 1635498202,
					"updateTime": 1635498810,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"member": {
						"data": {
							"type": "members",
							"id": "1"
						}
					},
					"reply": {
						"data": {
							"type": "replies",
							"id": "10"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/appeals/1"
				}
			},
			{
				"type": "appeals",
				"id": "2",
				"attributes": {
					"title": "个人申诉",
					"content": "个人申诉",
					"name": "张三",
					"identify": "412825199009098765",
					"appealType": 1,
					"contact": "联系方式",
					"images": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"acceptStatus": 1,
					"certificates": [
						{
							"name": "上传身份证/营业执照",
							"identify": "identify.jpg"
						}
					],
					"status": 0,
					"createTime": 1635498562,
					"updateTime": 1635498747,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "2"
						}
					},
					"member": {
						"data": {
							"type": "members",
							"id": "2"
						}
					},
					"reply": {
						"data": {
							"type": "replies",
							"id": "11"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/appeals/2"
				}
			}
		],
		"included": [
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "members",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "18800000000",
					"email": "997809098@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 1,
					"status": 0,
					"createTime": 1621434208,
					"updateTime": 1621435225,
					"statusTime": 1621434736
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "replies",
				"id": "10",
				"attributes": {
					"content": "予以受理",
					"images": [
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635498739,
					"updateTime": 1635498739,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "2",
				"attributes": {
					"name": "共青团萍乡市委",
					"shortName": "团市委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "members",
				"id": "2",
				"attributes": {
					"userName": "测试用户名",
					"realName": "姓名",
					"cellphone": "18800000001",
					"email": "997809099@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 0,
					"status": 0,
					"createTime": 1621434329,
					"updateTime": 1621434329,
					"statusTime": 0
				}
			},
			{
				"type": "replies",
				"id": "11",
				"attributes": {
					"content": "予以受理",
					"images": [
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635498747,
					"updateTime": 1635498747,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "2"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			}
		]
	}