# 规则接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [新增](#新增)
	* [编辑](#编辑)
	* [删除](#删除)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [规则项目字典](../Dictionary/rule.md "规则项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [委办局项目字典](../Dictionary/UserGroup.md "委办局项目字典")
	* [委办局资源目录项目字典](../Dictionary/wbjTemplate.md "委办局资源目录项目字典")
	* [本级资源目录项目字典](../Dictionary/bjTemplate.md "本级资源目录项目字典")
	* [国标资源目录项目字典](../Dictionary/gbTemplate.md "国标资源目录项目字典")

*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [规则控件规范](../WidgetRule/rule.md "规则控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
    101=>array(
        'rules'=>规则格式不正确
        'rulesName'=>规则名称不存在
        'completionRulesCount'=>补全数量不正确
        'completionRulesId' => 补全资源目录id格式不正确
        'completionRulesCategory' => 补全资源目录类型格式不正确
        'completionRulesBase'=>补全依据不正确
        'completionRulesItem'=>补全信息项不正确
        'completionRules'=>补全规则格式不正确
        'comparisonRulesCount'=>比对数量不正确
        'comparisonRulesId' => 比对资源目录ID格式不正确
        'comparisonRulesCategory' => 比对资源目录类型格式不正确
        'comparisonRulesBase'=>比对依据不正确
        'comparisonRulesItem'=>比对信息项不正确
        'comparisonRules'=>比对规则格式不正确
        'deDuplicationRules'=>去重规则格式不正确
        'deDuplicationRulesItems'=>去重信息项不正确
        'deDuplicationRulesResult' => 去重结果不正确
        'transformationRulesTransformationNotExist'=>转换资源目录信息项不存在
        'transformationRulesSourceNotExist'=>来源资源目录信息项不存在
        'transformationItemTypeNotExist'=>目标信息项类型不存在
        'typeCannotTransformation'=>类型不能转换
        'lengthCannotTransformation'=>长度不能转换
        'dimensionCannotTransformation'=>公开范围不能转换
        'isMaskedCannotTransformation'=>脱敏状态不能转换
        'maskRuleCannotTransformation'=>脱敏范围不能转换
        'zrrNotIncludeIdentify'=>补全/比对依据需要包含身份证号信息项
        'completionRulesTransformationItemNotExist'=>补全规则目标资源目录模板待补全信息项不存在
        'completionRulesCompletionTemplateNotExist'=>补全规则补全信息项来源不存在
        'completionRulesCompletionTemplateItemNotExist'=>补全规则补全信息项不存在
        'comparisonRulesTransformationItemNotExist'=>比对规则目标资源目录模板待比对信息项不存在
        'comparisonRulesComparisonTemplateNotExist'=>比对规则比对信息项来源不存在
        'comparisonRulesBaseCannotName'=>比对项为企业名称,比对依据不能选择企业名称信息项
        'comparisonRulesBaseCannotIdentify'=>比对项为统一社会信用代码/身份证号,那比对依据不能选择统一社会信用代码/身份证号信息项
        'comparisonRulesComparisonTemplateItemNotExist'=>比对规则比对信息项不存在
        'deDuplicationRulesItemsNotExit'=>去重规则去重信息项不存在
        'transformationCategory'=>目标类型格式不正确
        'sourceCategory'=>来源类型格式不正确
        'rejectReason'=>驳回原因格式不正确
        'applyCrewId'=>审核人格式不正确
        'crewId'=>发布人格式不正确
        'transformationTemplateId'=>目标资源目录格式不正确
        'sourceTemplateId'=>来源资源目录格式不正确
        'applyCrewId'=>审核人格式不正确
		'requiredItemsMapping'=>存在未转换和补全的必填项
    ),
    100=>array(
        'crewId'=>发布人为空
        'userGroupId'=>来源委办局为空
        'sourceTemplateId'=>来源资源目录为空
        'transformationTemplateId'=>目标资源目录为空
    ),
    103=>array(
        'rule'=>规则数据已经存在
    ),
    102=>array(
        'status'=>状态不能操作
        'applyStatus'=>状态不能操作
    ),
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| transformationTemplate | int | 是             | 1                                          | 目标资源目录      |
| sourceTemplate   | int       | 是             | 1                                          | 来源资源目录      |
| rules           | array      | 是             |                                             | 规则             |
| version         | int        | 是             | 1                                            | 版本号            |
| dataTotal       | int        | 是             | 1                                            | 已转换数据量       |
| crew            | int        | 是             | 1                                            | 发布人           |
| userGroup       | int        | 是             | 1                                            | 来源委办局          |
| type            | int        | 是    | 1 | 目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)       |
| transformationCategory            | int        | 是    | 1 | 目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)       |
| sourceCategory  | int        | 是   | 1  | 来源类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)  |
| status          | int        | 是            | 0                                            | 状态(0正常 -2 删除)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[userGroups]
	    1.3 fields[bjTemplates]
	    1.4 fields[gbTemplates]
	    1.5 fields[templates]
	    1.6 fields[rules]
	    1.7 fields[baseTemplates]
		1.8 include = crew,userGroup,transformationTemplate,sourceTemplate
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'rules/1?fields[rules]=version',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/rules/{id:\d+}

示例

	$response = $client->request('GET', 'rules/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/rules/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'rules/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/rules

	1、检索条件
	    1.1 filter[transformationCategory] | 根据目标库类别搜索 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)
	    1.2 filter[transformationTemplate] | 根据目标资源目录id搜索
	    1.3 filter[userGroup] | 根据来源委办局搜索
	    1.4 filter[sourceTemplate] | 根据来源资源目录id搜索
	    1.5 filter[status] | 根据状态搜索 | 0 正常 -2 删除
	    1.6 filter[sourceCategory] | 根据来源类别搜索 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)
	    1.7 filter[excludeTransformationCategory] | 排除目标类型

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序
		2.2 sort=-status | -status 根据状态倒序 | status 根据状态正序

示例

	$response = $client->request('GET', 'rules?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="新增">新增</a>
	
路由

	通过POST传参
	/rules

示例

	$data = array(
		'data' => array(
			"type"=>"rules",
			"attributes"=>array(
				"rules"=>array(
					'transformationRule' => array(
						"ZTMC" => "ZTMC",
						"XXLB" => "XXLB"
					),
					'completionRule' =>  array(
						'ZTMC' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
							array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
						),
						'TYSHXYDM' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
							array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
						),
					),
					'comparisonRule' =>  array(
						'ZTMC' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
							array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
						),
						'TYSHXYDM' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
							array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
						),
					),
					'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC","TYSHXYDM"))
				),
				"transformationCategory" => 2,
				"sourceCategory" => 10
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>1)
					)
				),
				"transformationTemplate"=>array(
					"data"=>array(
						array("type"=>"gbTemplates","id"=>1)
					)
				),
				"sourceTemplate"=>array(
					"data"=>array(
						array("type"=>"templates","id"=>1)
					)
				),
			)
		)
	);

	$response = $client->request(
					'POST',
					'rules',
					[
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
					]
				);

### <a name="编辑">编辑</a>

路由

	通过PATCH传参
	/rules/{id:\d+}

示例

	$data = array(
		'data' => array(
			"type"=>"rules",
			"attributes"=>array(
				"rules"=>array(
					'transformationRule' => array(
						"ZTMC" => "ZTMC",
						"XXLB" => "XXLB",
						"TYSHXYDM" => "TYSHXYDM"
					),
					'completionRule' =>  array(
						'ZTMC' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
							array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
						),
					),
					'comparisonRule' =>  array(
						'ZTMC' => array(
							array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
							array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
						),
					),
					'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC","TYSHXYDM","XXLB"))
				)
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>3)
					)
				),
			)
		)
	);

	$response = $client->request(
		'PATCH',
		'rules/1',
		[
			'headers'=>['Content-Type' => 'application/vnd.api+json'],
			'json' => $data
		]
	);

### <a name="删除">删除</a>

路由

	通过PATCH传参
	/rules/{id:\d+}/delete

示例

	$data = array(
		'data' => array(
			"type"=>"rules",
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'rules/1/delete',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "rules",
			"id": "1",
			"attributes": {
				"rules": {
					"transformationRule": {
						"ZTMC": "ZTMC",
						"TYSHXYDM": "TYSHXYDM",
						"CLRQ": "CLRQ",
						"HZRQ": "HZRQ",
						"JYCS": "JYCS",
						"ZCZB": "ZCZB",
						"YYQXQ": "YYQXQ",
						"YYQXZ": "YYQXZ",
						"JYFW": "JYFW",
						"DJJG": "DJJG",
						"FDDBR": "FDDBR",
						"FRZJHM": "FRZJHM",
						"DJZT": "DJZT",
						"QYLX": "QYLX",
						"QYLXDM": "QYLXDM",
						"HYML": "HYML",
						"HYDM": "HYDM",
						"SSQY": "SSQY",
						"ZTLB": "ZTLB",
						"GKFW": "GKFW",
						"GXPL": "GXPL",
						"XXFL": "XXFL",
						"XXLB": "XXLB"
					},
					"deDuplicationRule": {
						"result": "1",
						"items": []
					}
				},
				"version": 1631685765,
				"dataTotal": 0,
				"transformationCategory": 3,
				"sourceCategory": 1,
				"status": 0,
				"createTime": 1631685765,
				"updateTime": 1631686932,
				"statusTime": 1631686932
			},
			"relationships": {
				"transformationTemplate": {
					"data": {
						"type": "baseTemplates",
						"id": "1"
					}
				},
				"sourceTemplate": {
					"data": {
						"type": "bjTemplates",
						"id": "6"
					}
				},
				"userGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"crew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/rules/1"
			}
		},
		"included": [
			{
				"type": "baseTemplates",
				"id": "1",
				"attributes": {
					"name": "企业基本信息",
					"identify": "QYJBXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 4,
					"infoClassify": "5",
					"infoCategory": "1",
					"description": "企业基本信息",
					"category": 3,
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "成立日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "CLRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "核准日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "HZRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营场所",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "JYCS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "注册资本（万）",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "ZCZB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营范围",
							"type": "1",
							"length": "2000",
							"options": [],
							"remarks": "",
							"identify": "JYFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记机关",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "DJJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "FDDBR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法人身份证号",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "FRZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记状态",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "DJZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLXDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业门类",
							"type": "1",
							"length": "4",
							"options": [],
							"remarks": "",
							"identify": "HYML",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "HYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "所属区域",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "SSQY",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "主体类别",
							"type": "6",
							"length": "50",
							"options": [
								"法人及非法人组织",
								"个体工商户"
							],
							"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
							"identify": "ZTLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "公开范围",
							"type": "5",
							"length": "20",
							"options": [
								"社会公开"
							],
							"remarks": "支持单选",
							"identify": "GKFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "更新频率",
							"type": "5",
							"length": "20",
							"options": [
								"每月"
							],
							"remarks": "支持单选",
							"identify": "GXPL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息分类",
							"type": "5",
							"length": "50",
							"options": [
								"其他"
							],
							"remarks": "支持单选",
							"identify": "XXFL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1626504022,
					"updateTime": 1631585037,
					"statusTime": 0
				}
			},
			{
				"type": "bjTemplates",
				"id": "6",
				"attributes": {
					"name": "企业基本信息",
					"identify": "QYJBXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 4,
					"infoClassify": "5",
					"infoCategory": "1",
					"description": "企业基本信息",
					"category": 1,
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "成立日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "CLRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "核准日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "HZRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营场所",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "JYCS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "注册资本（万）",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "ZCZB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营范围",
							"type": "1",
							"length": "2000",
							"options": [],
							"remarks": "",
							"identify": "JYFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记机关",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "DJJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "FDDBR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法人身份证号",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "FRZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记状态",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "DJZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLXDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业门类",
							"type": "1",
							"length": "4",
							"options": [],
							"remarks": "",
							"identify": "HYML",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "HYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "所属区域",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "SSQY",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "主体类别",
							"type": "6",
							"length": "50",
							"options": [
								"法人及非法人组织",
								"个体工商户"
							],
							"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
							"identify": "ZTLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "公开范围",
							"type": "5",
							"length": "20",
							"options": [
								"社会公开"
							],
							"remarks": "支持单选",
							"identify": "GKFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "更新频率",
							"type": "5",
							"length": "20",
							"options": [
								"每月"
							],
							"remarks": "支持单选",
							"identify": "GXPL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息分类",
							"type": "5",
							"length": "50",
							"options": [
								"其他"
							],
							"remarks": "支持单选",
							"identify": "XXFL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1631264706,
					"updateTime": 1631584898,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"gbTemplate": {
						"data": {
							"type": "gbTemplates",
							"id": "4"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}

#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 1,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "rules",
				"id": "1",
				"attributes": {
					"rules": {
						"transformationRule": {
							"ZTMC": "ZTMC",
							"TYSHXYDM": "TYSHXYDM",
							"CLRQ": "CLRQ",
							"HZRQ": "HZRQ",
							"JYCS": "JYCS",
							"ZCZB": "ZCZB",
							"YYQXQ": "YYQXQ",
							"YYQXZ": "YYQXZ",
							"JYFW": "JYFW",
							"DJJG": "DJJG",
							"FDDBR": "FDDBR",
							"FRZJHM": "FRZJHM",
							"DJZT": "DJZT",
							"QYLX": "QYLX",
							"QYLXDM": "QYLXDM",
							"HYML": "HYML",
							"HYDM": "HYDM",
							"SSQY": "SSQY",
							"ZTLB": "ZTLB",
							"GKFW": "GKFW",
							"GXPL": "GXPL",
							"XXFL": "XXFL",
							"XXLB": "XXLB"
						},
						"deDuplicationRule": {
							"result": "1",
							"items": []
						}
					},
					"version": 1631685765,
					"dataTotal": 0,
					"transformationCategory": 3,
					"sourceCategory": 1,
					"status": 0,
					"createTime": 1631685765,
					"updateTime": 1631686932,
					"statusTime": 1631686932
				},
				"relationships": {
					"transformationTemplate": {
						"data": {
							"type": "baseTemplates",
							"id": "1"
						}
					},
					"sourceTemplate": {
						"data": {
							"type": "bjTemplates",
							"id": "6"
						}
					},
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/rules/1"
				}
			}
		],
		"included": [
			{
				"type": "baseTemplates",
				"id": "1",
				"attributes": {
					"name": "企业基本信息",
					"identify": "QYJBXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 4,
					"infoClassify": "5",
					"infoCategory": "1",
					"description": "企业基本信息",
					"category": 3,
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "成立日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "CLRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "核准日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "HZRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营场所",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "JYCS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "注册资本（万）",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "ZCZB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营范围",
							"type": "1",
							"length": "2000",
							"options": [],
							"remarks": "",
							"identify": "JYFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记机关",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "DJJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "FDDBR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法人身份证号",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "FRZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记状态",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "DJZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLXDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业门类",
							"type": "1",
							"length": "4",
							"options": [],
							"remarks": "",
							"identify": "HYML",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "HYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "所属区域",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "SSQY",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "主体类别",
							"type": "6",
							"length": "50",
							"options": [
								"法人及非法人组织",
								"个体工商户"
							],
							"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
							"identify": "ZTLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "公开范围",
							"type": "5",
							"length": "20",
							"options": [
								"社会公开"
							],
							"remarks": "支持单选",
							"identify": "GKFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "更新频率",
							"type": "5",
							"length": "20",
							"options": [
								"每月"
							],
							"remarks": "支持单选",
							"identify": "GXPL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息分类",
							"type": "5",
							"length": "50",
							"options": [
								"其他"
							],
							"remarks": "支持单选",
							"identify": "XXFL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1626504022,
					"updateTime": 1631585037,
					"statusTime": 0
				}
			},
			{
				"type": "bjTemplates",
				"id": "6",
				"attributes": {
					"name": "企业基本信息",
					"identify": "QYJBXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 4,
					"infoClassify": "5",
					"infoCategory": "1",
					"description": "企业基本信息",
					"category": 1,
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "成立日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "CLRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "核准日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "HZRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营场所",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "JYCS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "注册资本（万）",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "ZCZB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "YYQXZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营范围",
							"type": "1",
							"length": "2000",
							"options": [],
							"remarks": "",
							"identify": "JYFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记机关",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "DJJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "FDDBR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法人身份证号",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "FRZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记状态",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "DJZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLXDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业门类",
							"type": "1",
							"length": "4",
							"options": [],
							"remarks": "",
							"identify": "HYML",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "HYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "所属区域",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "SSQY",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "主体类别",
							"type": "6",
							"length": "50",
							"options": [
								"法人及非法人组织",
								"个体工商户"
							],
							"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
							"identify": "ZTLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "公开范围",
							"type": "5",
							"length": "20",
							"options": [
								"社会公开"
							],
							"remarks": "支持单选",
							"identify": "GKFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "更新频率",
							"type": "5",
							"length": "20",
							"options": [
								"每月"
							],
							"remarks": "支持单选",
							"identify": "GXPL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息分类",
							"type": "5",
							"length": "50",
							"options": [
								"其他"
							],
							"remarks": "支持单选",
							"identify": "XXFL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1631264706,
					"updateTime": 1631584898,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"gbTemplate": {
						"data": {
							"type": "gbTemplates",
							"id": "4"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			}
		]
	}