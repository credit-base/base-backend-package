# 员工接口文档

---

## 简介

本文档主要描述员工模块相关接口请求及响应说明。

## 目录

* [参考文档](#参考文档)
    * [项目字典](#项目字典)
    * [控件规范](#控件规范)
    * [错误规范](#错误规范)
    * [接口错误返回说明](#接口错误返回说明) 
* [参数说明](#参数说明)
* [接口示例](#接口示例)
    * [获取单条数据](#获取单条数据)
    * [获取多条数据](#获取多条数据)
    * [根据检索条件查询数据](#根据检索条件查询数据)
    * [新增](#新增)
    * [编辑](#编辑)
	* [启用](#启用)
	* [禁用](#禁用)
    * [登录](#登录)
    * [接口返回示例](#接口返回示例)
        * [单条示例](#单条示例)
        * [多条示例](#多条示例)

## <a name="参考文档">参考文档</a>

* <a name="项目字典">项目字典</a>
    * [通用字典](./docs/Dictionary/common.md "通用字典")
    * [人通用字典](./docs/Dictionary/user.md "人通用字典")
    * [员工字典](./docs/Dictionary/crew.md "员工字典")
* <a name="控件规范">控件规范</a>
    * [通用控件规范](./docs/WidgetRule/common.md "通用控件规范")
    * [人通用控件规范](./docs/WidgetRule/user.md "人通用控件规范")
    * [员工控件规范](./docs/WidgetRule/crew.md "员工控件规范")
* <a name="错误规范">错误规范</a>
    * [通用错误规范](./docs/ErrorRule/common.md "通用错误规范")
    * [人通用错误规范](./docs/ErrorRule/user.md "人通用错误规范")
    * [员工错误规范](./docs/ErrorRule/crew.md "员工错误规范")
    * 错误映射
        ```
        101=>array(
            'category'=>员工类型格式不正确
            'purview'=>权限范围格式不正确
            'userGroupId'=>所属委办局格式不正确
            'departmentId'=>所属科室格式不正确
        ),
        102=>array(
            'status'=>状态不能操作
        ),
        103=>array(
            'cellphone'=>手机号已经存在
        ),
        501=>姓名格式不正确
        502=>手机号码格式不正确
        503=>密码格式不正确
        504=>身份证号码格式不正确
        505=>密码不正确,
        1001=>该所属科室不属于该所属委办局
        ```
* <a name="接口错误返回说明">接口错误返回说明</a>
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明") 

## <a name="参数说明">参数说明</a>

<table>
  <tr>
    <th><b>英文名称</b></th>
    <th><b>类型</b></th>
    <th><b>请求参数是否必填</b></th>
    <th><b>示例</b></th>
    <th><b>描述</b></th>
  </tr> 
  <tr>
    <td>userName</td>
    <td>string</td>
    <td>是（登录时）</td>
    <td>18800000001</td>
    <td>账户（唯一）</td>
  </tr>
  <tr>
    <td>cellphone</td>
    <td>string</td>
    <td>是</td>
    <td>18800000001</td>
    <td>手机号码（唯一）</td>
  </tr>
  <tr>
    <td>realName</td>
    <td>string</td>
    <td>是</td>
    <td>赵科</td>
    <td>用户姓名</td>
  </tr>
  <tr>
    <td>password</td>
    <td>string</td>
    <td>是</td>
    <td>Admin123$</td>
    <td>密码</td>
  </tr>
  <tr>
    <td>userGroup</td>
    <td>int</td>
    <td>是</td>
    <td>1</td>
    <td>所属委办局</td>
  </tr>
  <tr>
    <td>department</td>
    <td>int</td>
    <td>否</td>
    <td>1</td>
    <td>所属科室</td>
  </tr>
  <tr>
    <td>cardId</td>
    <td>string</td>
    <td>否</td>
    <td>610424198812122655</td>
    <td>身份证号码</td>
  </tr>
  <tr>
    <td>category</td>
    <td>int</td>
    <td>是</td>
    <td>3</td>
    <td>员工类型（超级管理员 1，平台管理员 2，委办局管理员 3，操作用户 4）</td>
  </tr>
  <tr>
    <td>purview</td>
    <td>array</td>
    <td>否</td>
    <td>array('结构依据前端服务层及前端商定结果')</td>
    <td>权限范围</td>
  </tr>
</table>

## <a name="接口示例">接口示例</a>

### 获取数据支持include、fields请求参数

```
1、include请求参数
    1.1 include=userGroup
    1.2 include=department
2、fields[TYPE]请求参数
    2.1 fields[crews]
    2.2 fields[userGroups]
    2.3 fields[departments]
3、page请求参数
    3.1 page[number]=1 | 当前页
    3.2 page[size]=20 | 获取每页的数量
```

示例

```php
$response = $client->request('GET', 'crews/1?fields[crews]=userName,cellphone',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取单条数据">获取单条数据示例</a>

路由

```
通过GET传参
/crews/{id:\d+}
```

示例

```php
$response = $client->request('GET', 'crews/1',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取多条数据">获取多条数据示例</a>

路由

```
通过GET传参
/crews/{ids:\d+,[\d,]+}
```

示例

```php
$response = $client->request('GET', 'crews/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="根据检索条件查询数据">根据检索条件查询数据示例</a>

路由

```
通过GET传参
/crews

1、检索条件
    1.1 filter[userGroup] | 根据 所属委办局 搜索
    1.2 filter[department] | 根据 所属科室 搜索
    1.3 filter[cellphone] | 根据 手机号码 搜索
    1.4 filter[realName] | 根据 姓名 模糊搜索
    1.5 filter[category] | 根据 员工类型 搜索，支持多条件搜索 | 平台管理员 2 | 委办局管理员 3 | 操作用户 4
    1.6 filter[status] | 根据状态搜索 | 0 启用 | -2 禁用
2、排序
    2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序
```

示例

```php
$response = $client->request('GET', 'crews?filter[category]=3,4&sort=-updateTime',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="新增">新增示例</a>

路由

```
通过POST传参
/crews
```

示例

```php
$data = array("data"=>array(
                    "type"=>"crews",
                    "attributes"=>array(
                        "realName"=>'张科',
                        "cardId"=>'',
                        "cellphone"=>'18800000000',
                        "password"=>'Admin123$',
                        "category"=>3,
                        "purview"=>array(1,2,3),
                    ),
                    "relationships"=>array(
                        "userGroup"=>array(
                            "data"=>array(
                                array("type"=>"userGroups","id"=>1)
                            )
                        ),
                        "department"=>array(
                            "data"=>array(
                                array("type"=>"departments","id"=>1)
                            )
                        )
                    )
                )
        );
$response = $client->request(
                'POST',
                'crews',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

### <a name="编辑">编辑示例</a>

路由

```
通过PATCH传参
/crews/{id:\d+}
```

示例

```php
$data = array("data"=>array(
                    "type"=>"crews",
                    "attributes"=>array(
                        "realName"=>'张科',
                        "cardId"=>'',
                        "category"=>3,
                        "purview"=>array('结构依据前端服务层及前端商定结果')
                    ),
                    "relationships"=>array(
                        "userGroup"=>array(
                            "data"=>array(
                                array("type"=>"userGroups","id"=>1)
                            )
                        ),
                        "department"=>array(
                            "data"=>array(
                                array("type"=>"departments","id"=>1)
                            )
                        )
                    )
                )
        );
$response = $client->request(
                'PATCH',
                'crews/1',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

### <a name="登录">登录示例</a>

路由

```
通过POST传参
/crews/signIn
```

示例

```php
$data = array("data"=>array(
                    "type"=>"crews",
                    "attributes"=>array(
                        "userName"=>'18800000000',
                        "password"=>'Admin123$'
                    )
                )
        );
$response = $client->request(
                'POST',
                'crews/signIn',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

### <a name="启用">启用</a>

路由

	通过PATCH传参
	/crews/{id:\d+}/enable

示例

	$response = $client->request('PATCH', 'crews/1/enable',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="禁用">禁用</a>

路由

	通过PATCH传参
	/crews/{id:\d+}/disable

示例

	$response = $client->request('PATCH', 'crews/1/disable',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条示例">单条示例</a>

    {
        "meta": [],
        "data": {
            "type": "crews",
            "id": "1",
            "attributes": {
                "realName": "张科",
                "cardId": "412825199009094532",
                "userName": "18800000000",
                "cellphone": "18800000000",
                "category": 3,
                "purview": [1,3,5],
                "status": 0,
                "createTime": 1618284031,
                "updateTime": 1618284031,
                "statusTime": 0
            },
            "relationships": {
                "userGroup": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                },
                "department": {
                    "data": {
                        "type": "departments",
                        "id": "1"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8080/crews/1"
            }
        },
        "included": [
            {
                "type": "userGroups",
                "id": "1",
                "attributes": {
                    "name": "萍乡市发展和改革委员会",
                    "shortName": "发改委",
                    "status": 0,
                    "createTime": 1516168970,
                    "updateTime": 1516168970,
                    "statusTime": 0
                }
            },
            {
                "type": "departments",
                "id": "1",
                "attributes": {
                    "name": "财金科",
                    "status": 0,
                    "createTime": 1618283949,
                    "updateTime": 1618283949,
                    "statusTime": 0
                },
                "relationships": {
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    }
                }
            }
        ]
    }

#### <a name="多条示例">多条示例</a>

    {
        "meta": {
            "count": 2,
            "links": {
                "first": null,
                "last": null,
                "prev": null,
                "next": null
            }
        },
        "links": {
            "first": null,
            "last": null,
            "prev": null,
            "next": null
        },
        "data": [
            {
                "type": "crews",
                "id": "1",
                "attributes": {
                    "realName": "张科",
                    "cardId": "412825199009094532",
                    "userName": "18800000000",
                    "cellphone": "18800000000",
                    "category": 3,
                    "purview": [],
                    "status": 0,
                    "createTime": 1618284031,
                    "updateTime": 1618284031,
                    "statusTime": 0
                },
                "relationships": {
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    },
                    "department": {
                        "data": {
                            "type": "departments",
                            "id": "1"
                        }
                    }
                },
                "links": {
                    "self": "127.0.0.1:8080/crews/1"
                }
            },
            {
                "type": "crews",
                "id": "2",
                "attributes": {
                    "realName": "王文",
                    "cardId": "412825199009094533",
                    "userName": "18800000001",
                    "cellphone": "18800000001",
                    "category": 3,
                    "purview": [],
                    "status": 0,
                    "createTime": 1618284059,
                    "updateTime": 1618284059,
                    "statusTime": 0
                },
                "relationships": {
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    },
                    "department": {
                        "data": {
                            "type": "departments",
                            "id": "1"
                        }
                    }
                },
                "links": {
                    "self": "127.0.0.1:8080/crews/2"
                }
            }
        ],
        "included": [
            {
                "type": "userGroups",
                "id": "1",
                "attributes": {
                    "name": "萍乡市发展和改革委员会",
                    "shortName": "发改委",
                    "status": 0,
                    "createTime": 1516168970,
                    "updateTime": 1516168970,
                    "statusTime": 0
                }
            },
            {
                "type": "departments",
                "id": "1",
                "attributes": {
                    "name": "财金科",
                    "status": 0,
                    "createTime": 1618283949,
                    "updateTime": 1618283949,
                    "statusTime": 0
                },
                "relationships": {
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    }
                }
            }
        ]
    }