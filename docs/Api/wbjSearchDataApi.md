# 委办局资源目录数据搜索接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [屏蔽](#屏蔽)
	* [新增](#新增)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [资源目录数据通用项目字典](../Dictionary/searchData.md "资源目录数据通用项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [委办局项目字典](../Dictionary/UserGroup.md "委办局项目字典")
	* [委办局资源目录项目字典](../Dictionary/wbjTemplate.md "委办局资源目录项目字典")
	* [委办局资源目录数据项目字典](../Dictionary/wbjSearchData.md "委办局资源目录数据项目字典")

*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [资源目录数据通用控件规范](../WidgetRule/searchData.md "资源目录数据通用控件规范")
	* [委办局资源目录数据控件规范](../WidgetRule/wbjSearchData.md "委办局资源目录数据控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")

## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| infoClassify    | int        | 是            | 1                                            | 信息分类(1 行政许可 2 行政处罚 3 红名单 4 黑名单 5 其他) |
| infoCategory    | int        | 是            | 1                                            | 信息类别(1 基础信息 2 守信信息 3 失信信息 4 其他信息)    |
| crew            | int        | 是            | 1                                            | 发布人           |
| sourceUnit      | int        | 是            | 1                                            | 来源单位          |
| subjectCategory | int        | 是            | 1                                            | 主体类别(1 法人及非法人组织 2 自然人 3 个体工商户)        |
| dimension       | int        | 是            | 1                                            | 公开范围(1 社会公开 2 政务共享 3 授权查询)               |
| name            | string     | 是            | 张文                                          | 主体名称          |
| identify        | string     | 是            | 412825199309094321                           | 主体标识          |
| expirationDate  | int        | 是            | 1618284835                                   | 有效期限          |
| status          | int        | 是            | 0                                            | 状态(0启用 -2禁用)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |
| itemsData       | int        | 是            | 1                                            | 资源目录数据      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[userGroups]
	    1.3 fields[templates]
	    1.4 fields[wbjItemsData]
		1.5 include = crew,sourceUnit,itemsData,template
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'wbjSearchData/1?fields[wbjSearchData]=name',['haders'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/wbjSearchData/{id:\d+}

示例

	$response = $client->request('GET', 'wbjSearchData/1',['haders'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/wbjSearchData/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'wbjSearchData/1,2,3',['haders'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/wbjSearchData

	1、检索条件
	    1.1 filter[infoClassify] | 根据信息分类搜索 | 1 行政许可 2 行政处罚 3 红名单 4 黑名单 5 其他
	    1.2 filter[infoCategory] | 根据信息类别搜索 | 1 基础信息 2 守信信息 3 失信信息 4 其他信息
	    1.3 filter[subjectCategory] | 根据主体类别搜索 | 1 法人及非法人组织 2 自然人 3 个体工商户
	    1.4 filter[dimension] | 根据公开范围搜索 | 1 社会公开 2 政务共享 3 授权查询
	    1.5 filter[status] | 根据状态搜索 | 0 正常 | -2 屏蔽
	    1.6 filter[expirationDate] | 根据有效期限搜索
	    1.7 filter[sourceUnit] | 根据来源单位搜索,来源单位id
	    1.8 filter[name] | 根据主体名称搜索
	    1.9 filter[identify] | 根据主体标识搜索
	    1.10 filter[template] | 根据资源目录搜索,委办局资源目录id

	2、排序
		2.1 sort=-id | -id 根据id倒序 | id 根据id正序
		2.2 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序

示例

	$response = $client->request('GET', 'wbjSearchData?sort=-id&page[number]=1&page[size]=20',['haders'=>['Content-' => 'application/vnd.api+json']]);

### <a name="屏蔽">屏蔽</a>

路由

	通过PATCH传参
	/wbjSearchData/{id:\d+}/disable

示例

	$response = $client->request('PATCH', 'wbjSearchData/1/disable',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="新增">新增</a>

	$data = array(
		"data" => array(
			"type" => "wbjSearchData",
			"attributes" => array(
				"subjectCategory" => 1,    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
				"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
				"expirationDate" => 1618285915,    //有效期限
			),
			"relationships" => array(
				"crew" => array( // 来源单位
					"data" => array(
						array("type" => "crews", "id" => 1)
					)
				),
				"sourceUnit" => array( // 来源单位
					"data" => array(
						array("type" => "userGroups", "id" => 1)
					)
				),
				"template" => array( // 委办局资源目录
					"data" => array(
						array("type" => "wbjTemplates", "id" => 1)
					)
				),
				"itemsData" => array( //资源目录数据
					"data" => array(
						array(
							"type" => "itemsData", 
							"attributes" => array(
								"data" => array(
									"ZTMC" => "陕西传媒有限公司",
									"TYSHXYDM" => "921345679023456789"
								)
							)
						)
					)
				)
			)
		)
	);

```
$wbjSearchData = new WbjSearchData();

$wbjSearchData->setCrew(new Crew(1));
$wbjSearchData->setSourceUnit(new UserGroup(1));
$wbjSearchData->setSubjectCategory(2);
$wbjSearchData->setDimension(1);
$wbjSearchData->setExpirationDate(1618285915);

$templateRepository = new WbjTemplateRepository();
$wbjTemplate = $templateRepository->fetchOne(4);

$wbjSearchData->setTemplate($wbjTemplate);

$data = array(
    "ZTMC" => "张文",
    "ZJHM" => "412819199409098765"
);

$wbjItemsData = new WbjItemsData();
$wbjItemsData->setData($data);

$hash = md5(base64_encode(gzcompress(serialize($data))));

$wbjSearchData->setItemsData($wbjItemsData);
$wbjSearchData->setHash($hash);

$wbjSearchData->add();

```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "wbjSearchData",
			"id": "1",
			"attributes": {
				"infoClassify": 2,
				"infoCategory": 2,
				"subjectCategory": 2,
				"dimension": 1,
				"name": "张文",
				"identify": "412819199409098765",
				"expirationDate": 1618285917,
				"status": 0,
				"createTime": 1618300026,
				"updateTime": 1618300026,
				"statusTime": 0
			},
			"relationships": {
				"crew": {
					"data": {
						"type": "crews",
						"id": "1"
					}
				},
				"sourceUnit": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"itemsData": {
					"data": {
						"type": "wbjItemsData",
						"id": "1"
					}
				},
				"template": {
					"data": {
						"type": "templates",
						"id": "4"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8080/wbjSearchData/1"
			}
		},
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 3,
					"purview": [],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1618284031,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "wbjItemsData",
				"id": "1",
				"attributes": {
					"data": {
						"ZTMC": "张文",
						"ZJHM": "412819199409098765"
					}
				}
			},
			{
				"type": "templates",
				"id": "4",
				"attributes": {
					"name": "公证员信息",
					"identify": "GZYXX",
					"subjectCategory": [
						"2"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "2",
					"infoCategory": "2",
					"description": "目录描述信息",
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件号码",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "信用主体代码",
							"identify": "ZJHM",
							"isMasked": "1",
							"maskRule": [
								"3",
								"4"
							],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息",
								"守信信息",
								"失信信息",
								"其他信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "1",
							"maskRule": [
								"1",
								"2"
							],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1618284628,
					"updateTime": 1618284628,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					}
				}
			}
		]
	}
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 2,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "wbjSearchData",
				"id": "2",
				"attributes": {
					"infoClassify": 1,
					"infoCategory": 1,
					"subjectCategory": 1,
					"dimension": 2,
					"name": "陕西网络科技有限公司",
					"identify": "912333444567890098",
					"expirationDate": 1618285917,
					"status": 0,
					"createTime": 1618300065,
					"updateTime": 1618300065,
					"statusTime": 0
				},
				"relationships": {
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"itemsData": {
						"data": {
							"type": "wbjItemsData",
							"id": "2"
						}
					},
					"template": {
						"data": {
							"type": "templates",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8080/wbjSearchData/2"
				}
			},
			{
				"type": "wbjSearchData",
				"id": "1",
				"attributes": {
					"infoClassify": 2,
					"infoCategory": 2,
					"subjectCategory": 2,
					"dimension": 1,
					"name": "张文",
					"identify": "412819199409098765",
					"expirationDate": 1618285917,
					"status": 0,
					"createTime": 1618300026,
					"updateTime": 1618300026,
					"statusTime": 0
				},
				"relationships": {
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"itemsData": {
						"data": {
							"type": "wbjItemsData",
							"id": "1"
						}
					},
					"template": {
						"data": {
							"type": "templates",
							"id": "4"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8080/wbjSearchData/1"
				}
			}
		],
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 3,
					"purview": [],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1618284031,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "wbjItemsData",
				"id": "2",
				"attributes": {
					"data": {
						"ZTMC": "陕西网络科技有限公司",
						"TYSHXYDM": "912333444567890098"
					}
				}
			},
			{
				"type": "templates",
				"id": "1",
				"attributes": {
					"name": "登记信息",
					"identify": "DJXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "1",
					"infoCategory": "1",
					"description": "目录描述信息",
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "统一社会信用代码",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "信用主体代码",
							"identify": "TYSHXYDM",
							"isMasked": "1",
							"maskRule": [
								"3",
								"4"
							],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息",
								"守信信息",
								"失信信息",
								"其他信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "1",
							"maskRule": [
								"1",
								"2"
							],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1618284325,
					"updateTime": 1618284325,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "wbjItemsData",
				"id": "1",
				"attributes": {
					"data": {
						"ZTMC": "张文",
						"ZJHM": "412819199409098765"
					}
				}
			},
			{
				"type": "templates",
				"id": "4",
				"attributes": {
					"name": "公证员信息",
					"identify": "GZYXX",
					"subjectCategory": [
						"2"
					],
					"dimension": 1,
					"exchangeFrequency": 1,
					"infoClassify": "2",
					"infoCategory": "2",
					"description": "目录描述信息",
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "信用主体名称",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "证件号码",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "信用主体代码",
							"identify": "ZJHM",
							"isMasked": "1",
							"maskRule": [
								"3",
								"4"
							],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息",
								"守信信息",
								"失信信息",
								"其他信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "1",
							"maskRule": [
								"1",
								"2"
							],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1618284628,
					"updateTime": 1618284628,
					"statusTime": 0
				},
				"relationships": {
					"sourceUnit": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					}
				}
			}
		]
	}