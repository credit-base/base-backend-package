# 信用表扬审核管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [重新受理](#重新受理)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [互动类项目字典](../Dictionary/interaction.md "互动类项目字典")
	* [前台用户项目字典](../Dictionary/member.md "前台用户项目字典")
	* [受理委办局项目字典](../Dictionary/UserGroup.md "受理委办局项目字典")
	* [信用表扬项目字典](../Dictionary/praise.md "信用表扬项目字典")
	* [信用表扬审核项目字典](../Dictionary/unAuditedPraise.md "信用表扬审核项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [互动类通用控件规范](../WidgetRule/interaction.md "互动类通用控件规范")
	* [信用表扬控件规范](../WidgetRule/praise.md "信用表扬控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	101=>array(
		'replyContent'=>回复内容格式不正确
		'replyImages'=>回复图片格式不正确
		'replyImagesCount'=>回复图片数量超出限制
		'admissibility'=>受理情况格式不正确
		'replyCrew'=>受理人格式不正确
		'applyCrewId'=>审核人格式不正确
		'rejectReason'=>驳回原因格式不正确
	),
    ```
	
## <a name="参数说明">参数说明</a>
     
### unAuditedPraise

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| title           | string     | 是            | 个人表扬                                       | 标题            |
| content         | string     | 是            | 表扬内容                                       | 内容            |
| member          | int        | 是            | 1                                            | 前台用户         |
| acceptUserGroup | int        | 是            | 1                                            | 受理委办局        |
| acceptStatus    | int        | 是            | 0                                            | 受理状态(0 待受理, 1 受理中, 2 受理完成)|
| reply           | int        | 是            | 1                                            | 回复信息        |
| name            | string     | 是            | 张三                                          | 反馈人真实姓名/企业名称 |
| identify        | string     | 是            | 412825199009098976                           | 统一社会信用代码/反馈人身份证号 |
| subject         | string     | 是            | 陈锋                                         | 被表扬主体          |
| type            | int        | 是            | 0                                            | 被表扬类型(1 个人, 2 企业, 3 政府)|
| contact         | string     | 是            | 029-87654332                                 | 联系方式            |
| images          | array      | 否            | array(array('name'=>'图片', 'identify'=>'图片.jpg')) | 图片      |
| status          | int        | 是            | 0                                            | 状态(0 正常, -2 撤销, 2 公示)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |
| applyStatus     | int        | 是            | 0                                            | 审核状态(默认 0待审核 2审核通过 -2 审核驳回)|
| rejectReason    | string     | 是            | 内容不合理                                     | 驳回原因          |
| operationType   | int        | 是            | 0                                            | 操作类型(1 添加 2 编辑 3 启用 4 禁用 5 置顶 6 取消置顶 7 移动 8 受理)|
| applyInfoCategory| int        | 是            | 0                                           | 可以申请的信息分类(4 互动类 )|
| applyInfoType    | int        | 是            | 0                                           | 可以申请的最小信息分类(信用表扬 1)|
| applyCrew        | int        | 是            | Crew                                             | 审核人           |
| applyUserGroup   | int        | 是            | UserGroup                                           | 受理委办局          |
| relation         | int        | 是            | Member                                           | 修改人          |

### reply

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| content         | string     | 是            | 回复内容                                       | 回复内容        |
| images          | array      | 否            | array(array('name'=>'图片', 'identify'=>'图片.jpg')) | 回复图片  |
| crew            | int        | 是            | 1                                            | 受理人         |
| admissibility   | int        | 是            | 0                                            | 受理情况(1 予以受理, 2 不予受理)|
| status          | int        | 是            | 0                                            | 状态(0 默认)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[members]
	    1.2 fields[userGroups]
	    1.3 fields[crews]
	    1.4 fields[replies]
	    1.5 fields[unAuditedPraises]
		1.6 include = member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation

	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'unAuditedPraises/1?fields[unAuditedPraises]=name',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/unAuditedPraises/{id:\d+}

示例

	$response = $client->request('GET', 'unAuditedPraises/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/unAuditedPraises/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'unAuditedPraises/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/unAuditedPraises

	1、检索条件
	    1.1 filter[applyUserGroup] | 根据受理委办局搜索,委办局id
	    1.2 filter[applyInfoCategory] | 根据可以申请的信息分类搜索
	    1.3 filter[applyInfoType] | 可以申请的最小信息分类,此处为互动类类型
	    1.4 filter[relation] | 根据关联信息搜索,此处为前台用户id
	    1.5 filter[title] | 根据标题搜索
	    1.6 filter[applyStatus] | 根据审核状态搜索 | 0 待审核 | -2 审核驳回 | 2 审核通过
	    1.7 filter[operationType] | 根据操作类型搜索

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-applyStatus | -applyStatus 根据审核状态倒序 | 审核状态 根据审核状态正序

示例

	$response = $client->request('GET', 'unAuditedPraises?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="重新受理">重新受理</a>
	
路由

	通过PATCH传参
	/unAuditedPraises/{id:\d+}/resubmit

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedPraises",
			"relationships"=>array(
				"reply"=>array(
					"data"=>array(
						array(
							"type"=>"replies",
							"attributes"=>array(
								"content"=>"回复内容",
								"images"=>array(
									array('name' => '回复图片', 'identify' => 'identify.jpg'),
									array('name' => '回复图片', 'identify' => 'identify.jpg'),
									array('name' => '回复图片', 'identify' => 'identify.jpg')
								),
								"admissibility"=>0
							),
							"relationships"=>array(
								"crew"=>array(
									"data"=>array(
										array("type"=>"crews","id"=>受理人id)
									)
								)
							),
						)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedPraises/1/resubmit',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核通过">审核通过</a>

路由

	通过PATCH传参
	/unAuditedPraises/{id:\d+}/approve

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedPraises",
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedPraises/1/approve',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核驳回">审核驳回</a>

路由

	通过PATCH传参
	/unAuditedPraises/{id:\d+}/reject

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedPraises",
			"attributes"=>array(
				"rejectReason"=>"审核驳回原因"
			),
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>审核人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedPraises/1/reject',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "unAuditedPraises",
			"id": "110",
			"attributes": {
				"applyInfoCategory": 4,
				"applyInfoType": 1,
				"applyStatus": 2,
				"rejectReason": "",
				"operationType": 8,
				"praiseId": "1",
				"title": "表扬",
				"content": "表扬",
				"name": "张三",
				"identify": "123456789098765432",
				"praiseType": 1,
				"contact": "029-09876543",
				"images": [
					{
						"name": "name",
						"identify": "identify.jpg"
					},
					{
						"name": "name",
						"identify": "identify.jpg"
					}
				],
				"acceptStatus": 0,
				"subject": "陈锋",
				"status": 0,
				"createTime": 1635496219,
				"updateTime": 1635496245,
				"statusTime": 1635496245
			},
			"relationships": {
				"relation": {
					"data": {
						"type": "members",
						"id": "1"
					}
				},
				"applyCrew": {
					"data": {
						"type": "crews",
						"id": "2"
					}
				},
				"applyUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"acceptUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"member": {
					"data": {
						"type": "members",
						"id": "1"
					}
				},
				"reply": {
					"data": {
						"type": "replies",
						"id": "6"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/unAuditedPraises/110"
			}
		},
		"included": [
			{
				"type": "members",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "18800000000",
					"email": "997809098@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 1,
					"status": 0,
					"createTime": 1621434208,
					"updateTime": 1621435225,
					"statusTime": 1621434736
				}
			},
			{
				"type": "crews",
				"id": "2",
				"attributes": {
					"realName": "王文",
					"cardId": "412825199009094533",
					"userName": "18800000001",
					"cellphone": "18800000001",
					"category": 3,
					"purview": [],
					"status": -2,
					"createTime": 1618284059,
					"updateTime": 1619578659,
					"statusTime": 1619578659
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "replies",
				"id": "6",
				"attributes": {
					"content": "予以受理",
					"images": [
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635496219,
					"updateTime": 1635496219,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			}
		]
	}
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 4,
			"links": {
				"first": 1,
				"last": 2,
				"prev": null,
				"next": 2
			}
		},
		"links": {
			"first": "127.0.0.1:8089/unAuditedPraises/?include=member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation&page[number]=1&page[size]=2",
			"last": "127.0.0.1:8089/unAuditedPraises/?include=member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation&page[number]=2&page[size]=2",
			"prev": null,
			"next": "127.0.0.1:8089/unAuditedPraises/?include=member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation&page[number]=2&page[size]=2"
		},
		"data": [
			{
				"type": "unAuditedPraises",
				"id": "110",
				"attributes": {
					"applyInfoCategory": 4,
					"applyInfoType": 1,
					"applyStatus": 2,
					"rejectReason": "",
					"operationType": 8,
					"praiseId": "1",
					"title": "表扬",
					"content": "表扬",
					"name": "张三",
					"identify": "123456789098765432",
					"praiseType": 1,
					"contact": "029-09876543",
					"images": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"acceptStatus": 0,
					"subject": "陈锋",
					"status": 0,
					"createTime": 1635496219,
					"updateTime": 1635496245,
					"statusTime": 1635496245
				},
				"relationships": {
					"relation": {
						"data": {
							"type": "members",
							"id": "1"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "2"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"member": {
						"data": {
							"type": "members",
							"id": "1"
						}
					},
					"reply": {
						"data": {
							"type": "replies",
							"id": "6"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedPraises/110"
				}
			},
			{
				"type": "unAuditedPraises",
				"id": "111",
				"attributes": {
					"applyInfoCategory": 4,
					"applyInfoType": 1,
					"applyStatus": 0,
					"rejectReason": "审核驳回原因",
					"operationType": 8,
					"praiseId": "2",
					"title": "表扬",
					"content": "表扬",
					"name": "张三",
					"identify": "123456789098765432",
					"praiseType": 1,
					"contact": "029-09876543",
					"images": [
						{
							"name": "name",
							"identify": "identify.jpg"
						},
						{
							"name": "name",
							"identify": "identify.jpg"
						}
					],
					"acceptStatus": 0,
					"subject": "陈锋",
					"status": 0,
					"createTime": 1635496227,
					"updateTime": 1635496350,
					"statusTime": 1635496350
				},
				"relationships": {
					"relation": {
						"data": {
							"type": "members",
							"id": "2"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "2"
						}
					},
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "2"
						}
					},
					"member": {
						"data": {
							"type": "members",
							"id": "2"
						}
					},
					"reply": {
						"data": {
							"type": "replies",
							"id": "7"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedPraises/111"
				}
			}
		],
		"included": [
			{
				"type": "members",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "18800000000",
					"email": "997809098@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 1,
					"status": 0,
					"createTime": 1621434208,
					"updateTime": 1621435225,
					"statusTime": 1621434736
				}
			},
			{
				"type": "crews",
				"id": "2",
				"attributes": {
					"realName": "王文",
					"cardId": "412825199009094533",
					"userName": "18800000001",
					"cellphone": "18800000001",
					"category": 3,
					"purview": [],
					"status": -2,
					"createTime": 1618284059,
					"updateTime": 1619578659,
					"statusTime": 1619578659
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "replies",
				"id": "6",
				"attributes": {
					"content": "予以受理",
					"images": [
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635496219,
					"updateTime": 1635496219,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "members",
				"id": "2",
				"attributes": {
					"userName": "测试用户名",
					"realName": "姓名",
					"cellphone": "18800000001",
					"email": "997809099@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 0,
					"status": 0,
					"createTime": 1621434329,
					"updateTime": 1621434329,
					"statusTime": 0
				}
			},
			{
				"type": "userGroups",
				"id": "2",
				"attributes": {
					"name": "共青团萍乡市委",
					"shortName": "团市委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "replies",
				"id": "7",
				"attributes": {
					"content": "测试重新编辑",
					"images": [
						{
							"name": "测试重新编辑",
							"identify": "identify.jpg"
						},
						{
							"name": "测试重新编辑",
							"identify": "identify.jpg"
						},
						{
							"name": "测试重新编辑",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635496227,
					"updateTime": 1635496350,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "2"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			}
		]
	}