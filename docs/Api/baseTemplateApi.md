# 基础资源目录接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [编辑](#编辑)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [资源目录通用项目字典](../Dictionary/template.md "资源目录通用项目字典")
	* [基础资源目录项目字典](../Dictionary/baseTemplate.md "基础资源目录项目字典")

*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [资源目录通用控件规范](../WidgetRule/template.md "资源目录通用控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
    101=>array(
        'exchangeFrequency'=>更新频率格式不正确
        'infoClassify'=>信息分类格式不正确
        'infoCategory'=>信息类别格式不正确
        'subjectCategory=>主体类别格式不正确
        'items'=>模板信息格式不正确
        'name' => 信息项名称格式不正确
        'identify'=>信息项数据标识格式不正确
        'ZTMC' => 主体名称不存在
        'ZJHM' => 证件号码不存在
        'TYSHXYDM' => 统一社会信用代码不存在
        'type' => 信息项数据类型格式不正确
        'length' => 信息项数据长度格式不正确
        'options' => 信息项可选范围格式不正确
        'dimension' => 公开范围格式不正确
        'isNecessary' => 信息项是否必填格式不正确
        'isMasked' => 信息项是否脱敏格式不正确
        'maskRule' => 信息项脱敏规则格式不正确
        'remarks' => 信息项备注格式不正确
    )
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 行政许可                                      | 目录名称          |
| identify        | string     | 是            | XZXK                                         | 目录标识          |
| subjectCategory | array      | 是            | 1                                            | 主体类别(1 法人及非法人组织 2 自然人 3 个体工商户)        |
| dimension       | int        | 是            | 1                                            | 公开范围(1 社会公开 2 政务共享 3 授权查询)  |
| exchangeFrequency| int        | 是            | 1                                            | 更新频率(1 实时 2 每日 3 每周 4 每月 5 每季度 6 每半年 7 每一年 8 每两年 9 每三年) |
| infoClassify    | int        | 是            | 1                                            | 信息分类(1 行政许可 2 行政处罚 3 红名单 4 黑名单 5 其他) |
| infoCategory    | int        | 是            | 1                                            | 信息类别(1 基础信息 2 守信信息 3 失信信息 4 其他信息)    |
| category    | int        | 是            | 1                                            | 目录类别(1 本级 2 国标 3 基础 10 委办局 20 前置机委办局 21 前置机本级 22 前置机国标)    |
| description     | string     | 否            | 决定日期不能大于11个工作日                       | 目录描述     |
| items     | array     | 是            |                        | 模板信息     |
| status          | int        | 是            | 0                                            | 状态(0 默认)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |


### items
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 主体名称                                      | 信息项名称          |
| identify        | string     | 是            | ZTMC                                         | 数据标识          |
| type | int      | 是            | 1                                            | 数据类型(1 字符型 2 日期型 3 整数型 4 浮点型 5 枚举型 6 集合型)        |
| length       | int        | 是            | 1                                            | 数据长度  |
| options       | array        | 是            |                                           | 可选范围  |
| dimension       | int        | 是            | 1                                            | 公开范围(1 社会公开 2 政务共享 3 授权查询)  |
| isNecessary| int        | 是            | 1                                            | 是否必填(0 否 1 是) |
| isMasked| int        | 是            | 1                                            | 是否脱敏(0 否 1 是) |
| maskRule    | array        | 是            | [1,2]                                           | 脱敏规则 |
| remarks     | string     | 否            |                        |   备注   |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[baseTemplates]
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'baseTemplates/1?fields[baseTemplates]=name',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/baseTemplates/{id:\d+}

示例

	$response = $client->request('GET', 'baseTemplates/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/baseTemplates/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'baseTemplates/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/bjSearchData

	1、检索条件
		1.1 filter[name] | 根据 目录名称 搜索
		1.2 filter[identify] | 根据 目录标识 搜索
		1.3 filter[dimension] | 根据 公开范围 搜索 | 社会公开 1 | 政务共享 2 | 授权查询 3
		1.4 filter[infoClassify] | 根据 信息分类 搜索
		1.5 filter[infoCategory] | 根据 信息类别 搜索

	2、排序
		2.1 sort=-id | -id 根据id倒序 | id 根据id正序
		2.2 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序

示例

	$response = $client->request('GET', 'baseTemplates?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="编辑">编辑</a>

路由

	通过PATCH传参
	/baseTemplates/{id:\d+}

示例

	$data = array(
		"data" => array(
			"type" => "baseTemplates",
			"attributes" => array(
				"subjectCategory" => array(1, 3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
				"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
				"exchangeFrequency" => 1,    //更新频率
				"infoClassify" => 1,    //信息分类
				"infoCategory" => 1,    //信息类别
				"items" => array(
					array(
						"name" => '主体名称',    //信息项名称
						"identify" => 'ZTMC',    //数据标识
						"type" => 1,    //数据类型
						"length" => '200',    //数据长度
						"options" => array(),    //可选范围
						"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
						"isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
						"isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
						"maskRule" => array(),    //脱敏规则
						"remarks" => '信用主体名称',    //备注
					),
					array(
						"name" => '统一社会信用代码',    //信息项名称
						"identify" => 'TYSHXYDM',    //数据标识
						"type" => 1,    //数据类型
						"length" => '50',    //数据长度
						"options" => array(),    //可选范围
						"dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
						"isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
						"isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
						"maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
						"remarks" => '信用主体代码',    //备注
					),
					array(
						"name" => '信息类别',    //信息项名称
						"identify" => 'XXLB',    //数据标识
						"type" => 5,    //数据类型
						"length" => '50',    //数据长度
						"options" => array(
							"基础信息",
							"守信信息",
							"失信信息",
							"其他信息"
						),    //可选范围
						"dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
						"isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
						"isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
						"maskRule" => array(1, 2),    //脱敏规则，即左边保留1位字符，右边保留2位字符
						"remarks" => '信息性质类型，支持单选',    //备注
					)
				)
			)
		)
	);

	$response = $client->request(
		'PATCH',
		'baseTemplates/1',
		[
			'headers'=>['Content-Type' => 'application/vnd.api+json'],
			'json' => $data
		]
	);

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "baseTemplates",
			"id": "1",
			"attributes": {
				"name": "企业基本信息",
				"identify": "QYJBXX",
				"subjectCategory": [
					"1",
					"3"
				],
				"dimension": 1,
				"exchangeFrequency": 4,
				"infoClassify": "5",
				"infoCategory": "1",
				"description": "企业基本信息",
				"category": 3,
				"items": [
					{
						"name": "主体名称",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "ZTMC",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "统一社会信用代码",
						"type": "1",
						"length": "18",
						"options": [],
						"remarks": "",
						"identify": "TYSHXYDM",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "成立日期",
						"type": "2",
						"length": "8",
						"options": [],
						"remarks": "",
						"identify": "CLRQ",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "核准日期",
						"type": "2",
						"length": "8",
						"options": [],
						"remarks": "",
						"identify": "HZRQ",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "经营场所",
						"type": "1",
						"length": "255",
						"options": [],
						"remarks": "",
						"identify": "JYCS",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "注册资本（万）",
						"type": "1",
						"length": "255",
						"options": [],
						"remarks": "",
						"identify": "ZCZB",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "营业期限自",
						"type": "2",
						"length": "8",
						"options": [],
						"remarks": "",
						"identify": "JYQXQ",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "营业期限至",
						"type": "2",
						"length": "8",
						"options": [],
						"remarks": "",
						"identify": "JYQXZ",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "经营范围",
						"type": "1",
						"length": "2000",
						"options": [],
						"remarks": "",
						"identify": "JYFW",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "登记机关",
						"type": "1",
						"length": "255",
						"options": [],
						"remarks": "",
						"identify": "DJJG",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "法定代表人",
						"type": "1",
						"length": "255",
						"options": [],
						"remarks": "",
						"identify": "FDDBR",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "法人身份证号",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "FRZJHM",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "登记状态",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "DJZT",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "企业类型",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "QYLX",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "企业类型代码",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "QYLXDM",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "行业门类",
						"type": "1",
						"length": "4",
						"options": [],
						"remarks": "",
						"identify": "HYML",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "行业代码",
						"type": "1",
						"length": "200",
						"options": [],
						"remarks": "",
						"identify": "GYDM",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "所属区域",
						"type": "1",
						"length": "50",
						"options": [],
						"remarks": "",
						"identify": "SSQY",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "1",
						"isNecessary": "1"
					},
					{
						"name": "主体类别",
						"type": "6",
						"length": "50",
						"options": [
							"法人及非法人组织",
							"个体工商户"
						],
						"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
						"identify": "ZTLB",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					},
					{
						"name": "公开范围",
						"type": "5",
						"length": "20",
						"options": [
							"社会公开"
						],
						"remarks": "支持单选",
						"identify": "GKFW",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					},
					{
						"name": "更新频率",
						"type": "5",
						"length": "20",
						"options": [
							"每月"
						],
						"remarks": "支持单选",
						"identify": "GXPL",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					},
					{
						"name": "信息分类",
						"type": "5",
						"length": "50",
						"options": [
							"其他"
						],
						"remarks": "支持单选",
						"identify": "XXFL",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					},
					{
						"name": "信息类别",
						"type": "5",
						"length": "50",
						"options": [
							"基础信息"
						],
						"remarks": "信息性质类型，支持单选",
						"identify": "XXLB",
						"isMasked": "0",
						"maskRule": [],
						"dimension": "2",
						"isNecessary": "1"
					}
				],
				"status": 0,
				"createTime": 1626504022,
				"updateTime": 1629172736,
				"statusTime": 0
			},
			"links": {
				"self": "127.0.0.1:8089/baseTemplates/1"
			}
		}
	}

#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 1,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "baseTemplates",
				"id": "1",
				"attributes": {
					"name": "企业基本信息",
					"identify": "QYJBXX",
					"subjectCategory": [
						"1",
						"3"
					],
					"dimension": 1,
					"exchangeFrequency": 4,
					"infoClassify": "5",
					"infoCategory": "1",
					"description": "企业基本信息",
					"category": 3,
					"items": [
						{
							"name": "主体名称",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "ZTMC",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "统一社会信用代码",
							"type": "1",
							"length": "18",
							"options": [],
							"remarks": "",
							"identify": "TYSHXYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "成立日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "CLRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "核准日期",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "HZRQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营场所",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "JYCS",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "注册资本（万）",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "ZCZB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限自",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "JYQXQ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "营业期限至",
							"type": "2",
							"length": "8",
							"options": [],
							"remarks": "",
							"identify": "JYQXZ",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "经营范围",
							"type": "1",
							"length": "2000",
							"options": [],
							"remarks": "",
							"identify": "JYFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记机关",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "DJJG",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法定代表人",
							"type": "1",
							"length": "255",
							"options": [],
							"remarks": "",
							"identify": "FDDBR",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "法人身份证号",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "FRZJHM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "登记状态",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "DJZT",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLX",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "企业类型代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "QYLXDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业门类",
							"type": "1",
							"length": "4",
							"options": [],
							"remarks": "",
							"identify": "HYML",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "行业代码",
							"type": "1",
							"length": "200",
							"options": [],
							"remarks": "",
							"identify": "GYDM",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "所属区域",
							"type": "1",
							"length": "50",
							"options": [],
							"remarks": "",
							"identify": "SSQY",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "1",
							"isNecessary": "1"
						},
						{
							"name": "主体类别",
							"type": "6",
							"length": "50",
							"options": [
								"法人及非法人组织",
								"个体工商户"
							],
							"remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
							"identify": "ZTLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "公开范围",
							"type": "5",
							"length": "20",
							"options": [
								"社会公开"
							],
							"remarks": "支持单选",
							"identify": "GKFW",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "更新频率",
							"type": "5",
							"length": "20",
							"options": [
								"每月"
							],
							"remarks": "支持单选",
							"identify": "GXPL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息分类",
							"type": "5",
							"length": "50",
							"options": [
								"其他"
							],
							"remarks": "支持单选",
							"identify": "XXFL",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						},
						{
							"name": "信息类别",
							"type": "5",
							"length": "50",
							"options": [
								"基础信息"
							],
							"remarks": "信息性质类型，支持单选",
							"identify": "XXLB",
							"isMasked": "0",
							"maskRule": [],
							"dimension": "2",
							"isNecessary": "1"
						}
					],
					"status": 0,
					"createTime": 1626504022,
					"updateTime": 1629172736,
					"statusTime": 0
				},
				"links": {
					"self": "127.0.0.1:8089/baseTemplates/1"
				}
			}
		]
	}