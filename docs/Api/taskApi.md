# 任务接口文档

---

## 简介

本文档主要描述任务模块相关接口请求及响应说明。

## 目录

* [参考文档](#参考文档)
    * [项目字典](#项目字典)
    * [控件规范](#控件规范)
    * [错误规范](#错误规范)
    * [接口错误返回说明](#接口错误返回说明) 
* [参数说明](#参数说明)
* [接口示例](#接口示例)
    * [获取单条数据](#获取单条数据)
    * [获取多条数据](#获取多条数据)
    * [根据检索条件查询数据](#根据检索条件查询数据)
    * [登录](#登录)
    * [接口返回示例](#接口返回示例)
        * [单条示例](#单条示例)
        * [多条示例](#多条示例)

## <a name="参考文档">参考文档</a>

* <a name="项目字典">项目字典</a>
    * [通用字典](./docs/Dictionary/common.md "通用字典")
    * [任务字典](./docs/Dictionary/task.md "任务字典")
    * [员工字典](./docs/Dictionary/crew.md "员工字典")
    * [委办局字典](./docs/Dictionary/userGroup.md "委办局字典")
    * [目录字典](./docs/Dictionary/template.md "目录字典")
    * [规则字典](./docs/Dictionary/rule.md "规则字典")
* <a name="控件规范">控件规范</a>
* <a name="错误规范">错误规范</a>
* <a name="接口错误返回说明">接口错误返回说明</a>
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明") 

## <a name="参数说明">参数说明</a>

## <a name="接口示例">接口示例</a>

### 获取数据支持include、fields请求参数

```
1、include请求参数
    1.1 include=userGroup
    1.2 include=crew
2、fields[TYPE]请求参数
    2.1 fields[tasks]
    2.2 fields[userGroups]
    2.3 fields[crews]
3、page请求参数
    3.1 page[number]=1 | 当前页
    3.2 page[size]=20 | 获取每页的数量
```

示例

```php
$response = $client->request('GET', 'tasks/1?fields[crews]=userName,cellphone',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取单条数据">获取单条数据示例</a>

路由

```
通过GET传参
/tasks/{id:\d+}
```

示例

```php
$response = $client->request('GET', 'tasks/1',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取多条数据">获取多条数据示例</a>

路由

```
通过GET传参
/tasks/{ids:\d+,[\d,]+}
```

示例

```php
$response = $client->request('GET', 'tasks/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="根据检索条件查询数据">根据检索条件查询数据示例</a>

路由

```
通过GET传参
/tasks

1、检索条件
    1.1 filter[crew] | 根据 员工id 搜索
    1.2 filter[userGroup] | 根据 委办局id 搜索
    1.3 filter[pid] | 根据 父id 搜索
    1.4 filter[sourceCategory] | 根据 来源库类别 搜索
    1.5 filter[sourceTemplate] | 根据 来源目录id 搜索
    1.6 filter[targetCategory] | 根据 目标库类别 搜索
    1.7 filter[targetTemplate] | 根据 目标目录id 搜索
    1.8 filter[targetRule] | 根据 入目标库使用规则id 搜索
    1.9 filter[status] | 根据状态搜索 | 0 进行中 | 2 成功 | -2 失败 | -3 失败文件下载成功
2、排序
    2.1 sort=-createTime | -createTime 根据创建时间倒序 | createTime 根据创建时间正序
```

示例

```php
$response = $client->request('GET', 'tasks?filter[crew]=1&sort=-updateTime',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条示例">单条示例</a>

```json
{
    "meta": [],
    "data": {
        "type": "tasks",
        "id": "1",
        "attributes": {
            "pid": 0,
            "total": 4,
            "successNumber": 3,
            "failureNumber": 1,
            "sourceCategory": 10,
            "sourceTemplate": 1,
            "targetCategory": 10,
            "targetTemplate": 1,
            "targetRule": 1,
            "scheduleTask": 3416,
            "errorNumber": 0,
            "fileName": "FAILURE_1_10_HZXKXX_1_10_1_234551212.xlsx",
            "status": -3,
            "createTime": 1628066697,
            "updateTime": 1628066697,
            "statusTime": 1628066699
        },
        "relationships": {
            "crew": {
                "data": {
                    "type": "crews",
                    "id": "1"
                }
            },
            "userGroup": {
                "data": {
                    "type": "userGroups",
                    "id": "1"
                }
            }
        },
        "links": {
            "self": "127.0.0.1:8089/tasks/1"
        }
    },
    "included": [
        {
            "type": "crews",
            "id": "1",
            "attributes": {
                "realName": "张科",
                "cardId": "412825199009094532",
                "userName": "18800000000",
                "cellphone": "18800000000",
                "category": 1,
                "purview": [
                    "1",
                    "2",
                    "3"
                ],
                "status": 0,
                "createTime": 1618284031,
                "updateTime": 1619578455,
                "statusTime": 0
            },
            "relationships": {
                "userGroup": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                },
                "department": {
                    "data": {
                        "type": "departments",
                        "id": "3"
                    }
                }
            }
        },
        {
            "type": "userGroups",
            "id": "1",
            "attributes": {
                "name": "萍乡市发展和改革委员会",
                "shortName": "发改委",
                "status": 0,
                "createTime": 1516168970,
                "updateTime": 1516168970,
                "statusTime": 0
            }
        }
    ]
}
```

#### <a name="多条示例">多条示例</a>

```json
{
    "meta": {
        "count": 3,
        "links": {
            "first": null,
            "last": null,
            "prev": null,
            "next": null
        }
    },
    "links": {
        "first": null,
        "last": null,
        "prev": null,
        "next": null
    },
    "data": [
        {
            "type": "tasks",
            "id": "1",
            "attributes": {
                "pid": 0,
                "total": 4,
                "successNumber": 3,
                "failureNumber": 1,
                "sourceCategory": 10,
                "sourceTemplate": 1,
                "targetCategory": 10,
                "targetTemplate": 1,
                "targetRule": 1,
                "scheduleTask": 3416,
                "errorNumber": 0,
                "fileName": "FAILURE_1_10_HZXKXX_1_10_1_234551212.xlsx",
                "status": -3,
                "createTime": 1628066697,
                "updateTime": 1628066697,
                "statusTime": 1628066699
            },
            "relationships": {
                "crew": {
                    "data": {
                        "type": "crews",
                        "id": "1"
                    }
                },
                "userGroup": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8089/tasks/1"
            }
        },
        {
            "type": "tasks",
            "id": "2",
            "attributes": {
                "pid": 1,
                "total": 3,
                "successNumber": 2,
                "failureNumber": 1,
                "sourceCategory": 10,
                "sourceTemplate": 1,
                "targetCategory": 2,
                "targetTemplate": 1,
                "targetRule": 4,
                "scheduleTask": 3418,
                "errorNumber": 0,
                "fileName": "FAILURE_1_2_HZXKXX_1_10_1_234551212.xlsx",
                "status": -3,
                "createTime": 1628066699,
                "updateTime": 1628066699,
                "statusTime": 1628066701
            },
            "relationships": {
                "crew": {
                    "data": {
                        "type": "crews",
                        "id": "1"
                    }
                },
                "userGroup": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8089/tasks/2"
            }
        },
        {
            "type": "tasks",
            "id": "3",
            "attributes": {
                "pid": 1,
                "total": 3,
                "successNumber": 2,
                "failureNumber": 1,
                "sourceCategory": 10,
                "sourceTemplate": 1,
                "targetCategory": 1,
                "targetTemplate": 5,
                "targetRule": 6,
                "scheduleTask": 3419,
                "errorNumber": 0,
                "fileName": "FAILURE_5_1_HZXKXX_1_10_1_234551212.xlsx",
                "status": -3,
                "createTime": 1628066699,
                "updateTime": 1628066699,
                "statusTime": 1628066701
            },
            "relationships": {
                "crew": {
                    "data": {
                        "type": "crews",
                        "id": "1"
                    }
                },
                "userGroup": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8089/tasks/3"
            }
        }
    ],
    "included": [
        {
            "type": "crews",
            "id": "1",
            "attributes": {
                "realName": "张科",
                "cardId": "412825199009094532",
                "userName": "18800000000",
                "cellphone": "18800000000",
                "category": 1,
                "purview": [
                    "1",
                    "2",
                    "3"
                ],
                "status": 0,
                "createTime": 1618284031,
                "updateTime": 1619578455,
                "statusTime": 0
            },
            "relationships": {
                "userGroup": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                },
                "department": {
                    "data": {
                        "type": "departments",
                        "id": "3"
                    }
                }
            }
        },
        {
            "type": "userGroups",
            "id": "1",
            "attributes": {
                "name": "萍乡市发展和改革委员会",
                "shortName": "发改委",
                "status": 0,
                "createTime": 1516168970,
                "updateTime": 1516168970,
                "statusTime": 0
            }
        }
    ]
}
```
