# 失败数据字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### task

**任务** 

* Task

### category

**库类别** 库类别的表述.

* int
	* CATEGORY['WBJ']  委办局 10
	* CATEGORY['BJ']  本级 1
	* CATEGORY['GB']  国标 2
	* CATEGORY['QZJ_WBJ']  前置机委办局 20
	* CATEGORY['QZJ_BJ']  前置机本级 21
	* CATEGORY['QZJ_GB']  前置机国标 22

### template

**目录id** 目录id的表述，通过库类别判断是委办局资源目录、本级资源目录、国标资源目录.

* int

### itemsData

**资源目录数据** 

* ErrorItemsData

### errorType

**错误类型**

* int 错误类型的表述.

    * ERROR_TYPE['MISSING_DATA'] => 1//数据缺失
    * ERROR_TYPE['COMPARISON_FAILED'] => 2//比对失败
    * ERROR_TYPE['DUPLICATION_DATA'] => 4//数据重复
    * ERROR_TYPE['FORMAT_VALIDATION_FAILED'] => 8//数据格式错误

    <!-- * ERROR_TYPE['ABNORMAL_ERROR'] => 32//异常错误
    * ERROR_TYPE['ADD_ERROR'] => 64//添加错误 -->
    <!-- * ERROR_TYPE['TRANSFORMATION_RULE'] => 1//转换规则
    * ERROR_TYPE['COMPLETION_RULE'] => 2//补全规则
    * ERROR_TYPE['COMPARISON_RULE'] => 3//比对规则
    * ERROR_TYPE['DE_DUPLICATION_RULE'] => 4//去重规则
    * ERROR_TYPE['FORMAT_VALIDATION_RULE'] => 5//格式验证规则
    * ERROR_TYPE['ABNORMAL_ERROR'] => 20//异常错误
    * ERROR_TYPE['ADD_ERROR'] => 21//添加错误 -->

### errorReason

**错误原因**

* array 错误原因的表述.
    * array(
        'ZTMC' => array(
            ERROR_REASON[self::ERROR_TYPE['MISSING_DATA']],
            ERROR_REASON[self::ERROR_TYPE['COMPARISON_FAILED']]
        ),
        'TYSHXYDM' => array(
            ERROR_REASON[self::ERROR_TYPE['MISSING_DATA']],
            ERROR_REASON[self::ERROR_TYPE['COMPARISON_FAILED']]
        ),
    )
<!-- 
    * ERROR_REASON[self::ERROR_TYPE['TRANSFORMATION_RULE']] => "%s转换失败",
    * ERROR_REASON[self::ERROR_TYPE['COMPLETION_RULE']] => "%s补全失败",
    * ERROR_REASON[self::ERROR_TYPE['COMPARISON_RULE']] => "%s比对失败",
    * ERROR_REASON[self::ERROR_TYPE['DE_DUPLICATION_RULE']] => "%s数据重复,去重失败",
    * ERROR_REASON[self::ERROR_TYPE['FORMAT_VALIDATION_RULE']] => "%s数据格式不正确",
    * ERROR_REASON[self::ERROR_TYPE['ABNORMAL_ERROR']] => "异常错误",
    * ERROR_REASON[self::ERROR_TYPE['ADD_ERROR']] => "数据格式错误" -->

### status

**数据状态**

* int 数据状态的表述.
    * STATUS['NORMAL'] => 0//正常
    * STATUS['PROGRAM_EXCEPTION'] => 1//程序异常
    * STATUS['STORAGE_EXCEPTION'] => 2//入库异常