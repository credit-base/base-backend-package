# 工单任务字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### parentTask

**父任务**

### ratio

**父任务进度** 

* int
* 父任务进度比率=`(已完结总数/指派对象总数)*100`
	* **进行中**：任务状态为**待确认**、**跟进中**时，进度为进行中 
	* **已完结**：任务状态为**已撤销**、**已确认**、**已终结**时，进度为已完结

### templateType

**基础目录** 指基础目录类型

* int
	* GB => 1 | 国标目录
	* BJ => 2 | 本级目录
* 必填项

### template

**指派目录** 

* Template
* 必填项

### title

**任务标题** 

* string
* 1-100
* 必填项

### description

**任务描述** 

* string
* 1-2000
* 必填项

### endTime

**终结时间** 

* string
* 日期，如 `2020-01-01`
* 必填项

### attachment

**依据附件** 

* array
* pdf
* 选填项

### assignObjects

**指派对象** 用于工单任务父任务

* array
* 必填项

### assignObject

**指派对象** 用于工单任务

* UserGroup

### status

**状态** 

* int
	* DQR => 0 | 待确认
	* YCX => 1 | 已撤销
	* YQR => 2 | 已确认
	* GJZ => 3 | 跟进中
	* YZJ => 4 | 已终结

### reason

**撤销原因或终结原因** 

* string
* 1-2000
* 必填项

### feedbackRecords

**反馈记录**

* array

#### crew

**反馈人**

* Crew
* 必填项

#### crewName

**反馈人真实姓名**

* string

#### userGroup

**反馈委办局**

* UserGroup
* 必填项

#### userGroupName

**反馈委办局名称**

* string

#### isExistedTemplate

**是否已存在目录**

* int
	* FOU => 0 | 否，默认
	* SHI => 1 | 是
* 必填项

#### templateId

**目录id** 委办局目录id

* 当 **是否已存在目录** 为 **是** 时，此项必填
* 当 **是否已存在目录** 为 **否** 时，此项不填

#### items

**信息项**

* array
* 当 **是否已存在目录** 为 **是** 时，此项不填
* 当 **是否已存在目录** 为 **否** 时，此项必填

#### reason

**反馈原因**

* string
* 必填项

#### feedbackTime

**反馈时间**

* 时间戳
