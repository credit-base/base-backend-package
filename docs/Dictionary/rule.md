# 规则字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### transformationCategory

**目标资源目录类型** 

* int
	* CATEGORY[WBJ] 10 //委办局
	* CATEGORY[BJ] 1 //本级
	* CATEGORY[GB] 2 //国标
	* CATEGORY[BASE] 3 //基础
	* CATEGORY[QZJ_WBJ] 20 //前置机委办局
	* CATEGORY[QZJ_BJ] 21 //前置机本级
	* CATEGORY[QZJ_GB] 22 //前置机国标
* 必填项

### transformationTemplate

**目标资源目录**

* Template
* 必填项

### sourceCategory

**来源资源目录类型** 

* int
	* CATEGORY[WBJ] 10 //委办局
	* CATEGORY[BJ] 1 //本级
	* CATEGORY[GB] 2 //国标
	* CATEGORY[BASE] 3 //基础
	* CATEGORY[QZJ_WBJ] 20 //前置机委办局
	* CATEGORY[QZJ_BJ] 21 //前置机本级
	* CATEGORY[QZJ_GB] 22 //前置机国标
* 必填项

### sourceTemplate

**来源资源目录**

* Template
* 必填项

### rules

**规则** 此处为数组,且包含转换规则,补全规则,比对规则,去重规则

* array
* 必填项
* 例子:

	```
	array(
		'transformationRule' => array(
			"目标资源目录模板字段标识" => "来源资源目录模板字段标识",
			"ZTMC" => "ZTMC",
		),
		'completionRule' =>  array(
			"目标资源目录模板字段标识" => array(
				array("id"=>"补全目标资源目录", "category"=>"补全目标资源目录类型", "base"=>array("补全依据"), "item" => "补全模板字段标识"),
			),
			'ZTMC' => array(
				array('id'=>1, 'category'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
				array('id'=>2, 'category'=>1, 'base'=> array(1), 'item'=>'ZTMC'),
			),
			'TYSHXYDM' => array(
				array('id'=>1, 'category'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
				array('id'=>2, 'category'=>1, 'base'=> array(1), 'item'=>'TYSHXYDM'),
			),
		);
		'comparisonRule' =>  array(
			"目标资源目录模板字段标识" => array(
				array("id"=>"比对目标资源目录", "category"=>"比对目标资源目录类型", "base"=>array("比对依据"), "item" => "比对模板字段标识"),
			),
			'ZTMC' => array(
				array('id'=>1, 'category'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
				array('id'=>2, 'category'=>1, 'base'=> array(1), 'item'=>'ZTMC'),
			),
			'TYSHXYDM' => array(
				array('id'=>1, 'category'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
				array('id'=>2, 'category'=>1, 'base'=> array(1), 'item'=>'TYSHXYDM'),
			),
		);
		'deDuplicationRule' => array("result"=>去重结果(1 丢弃, 2 覆盖), "items"=>array("目标资源目录模板字段标识"));
	)
	```
* 补全依据
	* COMPLETION_BASE['NAME'] = 1 //企业名称
	* COMPLETION_BASE['IDENTIFY'] = 2 //统一社会信用代码/身份证号
* 比对依据
	* COMPARISON_BASE['NAME'] = 1 //企业名称
	* COMPARISON_BASE['IDENTIFY'] = 2 //统一社会信用代码/身份证号
* 去重结果
	* DE_DUPLICATION_RESULT['DISCARD'] = 1 //丢弃
	* DE_DUPLICATION_RESULT['COVER'] = 2 //覆盖
* 去重规则样例: array('result'=>1, items=>array('ZTMC', 'TYSHXYDM'));

### version

**版本号** 

* int
* 必填项

### dataTotal

**已转换数据量** 

* int
* 选填项

### crew

**发布人** 

* Crew
* 必填项

### userGroup

**来源委办局** 委办局资源目录的来源委办局

* UserGroup
* 必填项

### status

**状态** 

* int

	* STATUS_NORMAL | 正常 | 0
	* STATUS_DELETE | 删除 | -2

### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
