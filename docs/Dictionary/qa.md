# 信用问答项目字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [互动类通用字典](./docs/Dictionary/interaction.md "互动类通用字典")

### [标题](interaction.md)
### [内容](interaction.md)
### [前台用户](interaction.md)
### [受理委办局](interaction.md)
### [受理状态](interaction.md)
### [回复信息](interaction.md)
### [状态](interaction.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

### status

**状态** 状态的表述.

* int

	* STATUS_NORMAL  正常 0 默认
	* STATUS_PUBLISH 2 公示
	* STATUS_REVOKE -2 撤销
