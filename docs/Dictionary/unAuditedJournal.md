# 信用刊物审核字典

### 英文名称

**中文名称** 描述信息

---

### [标题](journal.md)
### [来源](journal.md)
### [简介](journal.md)
### [封面](journal.md)
### [附件](journal.md)
### [授权图片](journal.md)
### [年份](journal.md)
### [发布委办局](journal.md)
### [发布人](journal.md)
### [状态](journal.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
### [操作类型](common.md)
### [审核状态](common.md)
### [驳回原因](common.md)

### applyInfoCategory

**可以申请的信息分类** 可以申请的信息分类的表述.

* int
	* APPLY_INFO_CATEGORY['JOURNAL'] = 2 //信用刊物

### applyInfoType

**可以申请的最小信息分类** 可以申请的最小信息分类的表述(该字段为预留字段预防出现二级三级分类的情况).

* int
	* APPLY_INFO_TYPE['JOURNAL'] = array(
		信用刊物类型
	)

### applyUserGroup

**审核委办局** 审核委办局即为发布委办局

* UserGroup

### applyCrew

**审核人** 审核人的表述

* Crew

### relation

**修改人** 信用刊物修改人的表述.此处信用刊物为员工

* Crew/Member