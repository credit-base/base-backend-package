# 企业失败数据字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### task

**任务** 

* Task

### itemsData

**失败数据** 

* array

### errorType

**错误类型**

* int 错误类型的表述.
    * ERROR_TYPE['FORMAT_VALIDATION_RULE'] => 1//格式验证
    * ERROR_TYPE['FORMAT_VALIDATION_RULE'] => 2//去重验证
    * ERROR_TYPE['ABNORMAL_ERROR'] => 20//异常错误
    * ERROR_TYPE['ADD_ERROR'] => 21//添加错误

### errorReason

**错误原因**

* string 错误原因的表述.
    * ERROR_REASON[self::ERROR_TYPE['FORMAT_VALIDATION_RULE']] => "%s数据格式不正确",
    * ERROR_REASON[self::ERROR_TYPE['DE_DUPLICATION_RULE']] => "%s数据重复",
    * ERROR_REASON[self::ERROR_TYPE['ABNORMAL_ERROR']] => "异常错误",
    * ERROR_REASON[self::ERROR_TYPE['ADD_ERROR']] => "数据格式错误"
