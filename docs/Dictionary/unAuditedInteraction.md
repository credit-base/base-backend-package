# 互动类审核字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [互动类通用字典](./docs/Dictionary/interaction.md "互动类通用字典")

### [标题](interaction.md)
### [内容](interaction.md)
### [前台用户](interaction.md)
### [受理委办局](interaction.md)
### [受理状态](interaction.md)
### [回复信息](interaction.md)
### [状态](interaction.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
### [操作类型](common.md)
### [审核状态](common.md)
### [驳回原因](common.md)

### applyInfoCategory

**可以申请的信息分类** 可以申请的信息分类的表述.

* int
	* APPLY_INFO_CATEGORY['INTERACTION'] = 3 //互动类

### applyInfoType

**可以申请的最小信息分类** 可以申请的最小信息分类的表述(该字段为预留字段预防出现二级三级分类的情况).

* int
	* APPLY_INFO_TYPE['INTERACTION'] = array(
		互动类类型
	)

### applyUserGroup

**审核委办局** 审核委办局即为发布委办局

* UserGroup

### applyCrew

**审核人** 审核人的表述

* Crew

### relation

**修改人** 互动类修改人的表述.此处互动类为员工

* Crew/Member