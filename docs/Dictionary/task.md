# 资源目录数据导入任务字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### crew

**发布人** 

* Crew

### userGroup

**发布单位** 

* UserGroup

### pid

**父id** 父id的表述.

* int

### total

**总数** 总数的表述.

* int

### successNumber

**成功数** 成功数的表述.

* int

### failureNumber

**失败数** 失败数的表述.

* int

### sourceCategory

**来源库类别** 来源库类别的表述.

* int

### sourceTemplate

**来源目录id** 来源目录id的表述，通过来源库类别判断是委办局资源目录、本级资源目录、国标资源目录.

* int

### targetCategory

**目标库类别** 目标库类别的表述.

* int

### targetTemplate

**目标目录id** 目标目录id的表述，通过目标库类别判断是委办局资源目录、本级资源目录、国标资源目录.

* int

### targetRule

**入目标库使用规则id** 入目标库使用规则id的表述.

* int
* 当目标库类别为委办局时：该规则使用对应模板id
* 当目标库类别为本级或国标时：该规则使用对应清洗比对规则id

### status

**状态** 状态的表述.

* int

	* DEFAULT 0 进行中,默认
	* SUCCESS 2 成功
	* FAILURE -2 失败
	* FAILURE_FILE_DOWNLOAD -3 失败文件下载成功

### scheduleTask

**调度任务id** 调度任务id的表述.

* int

### errorNumber

**错误编号** 错误编号的表述.

* int

### fileName

**文件名** 上传文件文件名的表述.

* string
* 来源资源目录标识_来源资源目录id_来源资源目录类型_员工id_文件内容hash.xls/FAILURE_目标资源目录id_目标资源目录类型_来源资源目录标识_来源资源目录id_来源资源目录类型_员工id_文件内容hash.xls
