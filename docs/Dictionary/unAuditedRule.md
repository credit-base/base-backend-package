# 规则审核字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [规则字典](./docs/Dictionary/rule.md "规则字典")

### [目标资源目录](rule.md)
### [来源资源目录](rule.md)
### [规则](rule.md)
### [版本号](rule.md)
### [发布人](rule.md)
### [来源委办局](rule.md)
### [分类](rule.md)
### [状态](rule.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
### [操作类型](common.md)
### [驳回原因](common.md)

### publishCrew

**发布人** 发布人的表述

* Crew

### userGroup

**来源委办局** 委办局资源目录来源委办局的表述

* UserGroup

### applyCrew

**审核人** 审核人的表述

* Crew

### applyUserGroup

**审核委办局** 审核人的所属委办局

* UserGroup

### applyInfo

**申请信息** 申请信息

* array

### relationId

**关联id** 此处为规则的id

* int

### applyStatus 

**审核状态** 表述审核数据的审核状态.

* int
	* APPLY_STATUS['PENDING'] = 0 //待审核
	* APPLY_STATUS['APPROVE'] = 2 //通过
	* APPLY_STATUS['REJECT'] =-2 //驳回
	* APPLY_STATUS['REVOKE'] =-4 //撤销
