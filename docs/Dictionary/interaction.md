# 互动类处理字典


### 英文名称

**中文名称** 描述信息

---

## 互动类分类

### praise

**信用表扬** 信用表扬的表述.

### complaint

**信用投诉** 信用投诉的表述.

### appeal

**异议申诉** 异议申诉的表述.

### feedback

**问题反馈** 问题反馈的表述.

### QA

**信用问答** 信用问答的表述.

---

## 互动类--通用字典（上述五类中都存在的）

### title 

**标题** 标题的表述.

* string
* 必填项

### content

**内容** 内容的表述.

* string 
* 必填项

### member

**前台用户** 前台用户的表述

* Member 
* 必填项

### acceptUserGroup

**受理委办局** 受理委办局的表述.

* UserGroup  
* 必填项

### acceptStatus

**受理状态** 受理状态的表述.

* int
	* ACCEPT_STATUS['PENDING'] 0 待受理
	* ACCEPT_STATUS['ACCEPTING'] 1 受理中
	* ACCEPT_STATUS['COMPLETE'] 2 受理完成

### reply 

**回复信息** 回复信息的表述.

* Reply 
* 必填项

### status

**状态** 状态的表述.

* int

	* STATUS_NORMAL  正常 0 默认
	* STATUS_REVOKE -2 撤销

### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

## 回复信息(reply)

### content

**回复内容** 回复内容的表述.

* string 
* 必填项

### images

**回复图片** 回复图片的表述.

* array
* 必填填项

### crew

**受理人** 受理人的表述

* Crew
* 必填项

### admissibility

**受理情况** 受理情况的表述.

* int
	* ADMISSIBILITY['ADMISSIBLE'] 1 予以受理 默认
	* ADMISSIBILITY['INADMISSIBLE'] 2 不予受理
* 必填

### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

## 信用表扬/信用投诉特有字典

### name

**反馈人真实姓名/企业名称** 

* string 
* 必填项

### identify

**统一社会信用代码/反馈人身份证号**  

* string
* 必填项

### subject

**被表扬主体/被投诉主体**  

* 如果是信用表扬，此处的表示被表扬主体；如果是信用投诉，则表示被投诉主体
* string 
* 必填项

### type

**被表扬类型/被投诉类型** 

* 如果是信用表扬，此处的表示被表扬类型；如果是信用投诉，则表示被投诉类型
* int 
	* TYPE[PERSONAL] 1 个人
	* TYPE[ENTERPRISE] 2 企业
	* TYPE[GOVERNMENT] 3 政府
* 必填项

### contact 

**联系方式** 联系方式的表述

* string 
* 必填项

### images 

**图片** 图片的表述.

* array 
* 必填项
