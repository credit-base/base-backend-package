# 通用字典

### 英文名称

**中文名称** 描述信息

---

## 动词

### show

**查看**

### signIn

**登录**

### add

**发布/新增/注册**

### edit

**编辑**

### verify

**审核**

### pending

**待审核**

### reject

**驳回**

### approve

**通过**  

### enable

**启用**

### disable

**禁用**

### assign

**指派**

### feedback

**反馈**

### end

**终结**

### confirm

**确认**

### revoke

**撤销**

## 名词

### status

**状态**

### statusTime

**状态更新时间**

### createTime

**创建时间**

### updateTime

**更新时间**

### 审核信息

### applyStatus 

**审核状态** 表述审核数据的审核状态.

* int
	* APPLY_STATUS['PENDING'] = 0 //待审核
	* APPLY_STATUS ['APPROVE'] = 2 //通过
	* APPLY_STATUS['REJECT'] =-2 //驳回

### rejectReason

**驳回原因** 审核驳回原因的表述.

* string

### operationType

**操作类型** 审核操作的表述.

* int
	* OPERATION_TYPE['ADD'] = 1 //添加
	* OPERATION_TYPE['EDIT'] = 2 //编辑
	* OPERATION_TYPE['ENABLED'] = 3 //启用
	* OPERATION_TYPE['DISABLED'] = 4 //禁用
	* OPERATION_TYPE['TOP'] = 5 //置顶
	* OPERATION_TYPE['CANCEL_TOP'] = 6 //取消置顶
	* OPERATION_TYPE['MOVE'] = 7 //移动