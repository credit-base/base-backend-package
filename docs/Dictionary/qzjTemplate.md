# 前置机目录字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [目录字典](./docs/Dictionary/template.md "目录字典")

### sourceUnit

**来源委办局** 

* UserGroup
* 选填项

#### category

**目录类别** 

* int
	* CATEGORY['QZJ_WBJ']  前置机委办局 20
	* CATEGORY['QZJ_BJ']  前置机本级 21
	* CATEGORY['QZJ_GB']  前置机国标 22
* 必填项