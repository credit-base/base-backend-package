# 新闻分类对应关系定义

### parentCategory

**父级分类** 
	
	'NULL'=>0,              无
	'CREDIT_DYNAMICS'=>1,   信用动态  
	'DISCIPLINARY'=>2,      联合奖惩

### category

**分类**

	无:
		'ORGANIZATIONAL_STRUCTURE' => 1,  组织架构
	无:
		'POLICY_STATUTE'=>2,              政策法规
	无:
		'STANDARD_SPECIFICATION'=>3,      标准规范 
	信用动态:
		'NOTICE_ANNOUNCEMENT'=>4,         通知公告
		'WORK_DYNAMICS'=>5,               工作动态
		'CREDIT_NEWS'=>6,                 信用新闻
		'CREDIT_CONSTRUCTION'=>7,         信用建设
	无:
		'CREDIT_RESEARCH'=>8,             信用研究
	联合奖惩:
		'DISCIPLINARY_MEMORANDUM'=>9,     联合惩戒备忘录
		'DISCIPLINARY_DYNAMICS'=>10,      惩戒动态
		'DISCIPLINARY_CASE' => 11,        联合奖惩案例
	无:
		'SUPERVISION'=>12,                信用监管
	无:
	    'TRUSTWORTHINESS_DISHONESTY' => 13, 守信激励与失信惩戒
	无:
	    'INTEGRITY_BUILDING' => 14, 	诚信建设万里行
	无:   
	    'XINYI_PLUS' => 15, 				信易+

### type 

**类型**
	
	无:
		组织架构:
			'LEADING_GROUP' => 1, 	领导小组
		    'CREDIT_OFFICE' => 2, 	信用办
		    'PUBLIC_DOCUMENT' => 3, 公示文件
		    'CREDIT_WINDOW' => 4,	信用窗口
	无:
		政策法规:
		    'COUNTRY_POLICY_STATUTE' => 5, 国家级政策法规
		    'PROVINCE_POLICY_STATUTE' => 6, 省级政策法规
		    'CITY_POLICY_STATUTE' => 7,		市级政策法规
	无:
		标准规范:
		    'COUNTRY_STANDARD_SPECIFICATION' => 8,	国家级标准规范
		    'PROVINCE_STANDARD_SPECIFICATION' => 9,	省级标准规范
		    'CITY_STANDARD_SPECIFICATION' => 10,	市级标准规范
	信用动态:
		通知公告:
		    'COUNTRY_NOTICE_ANNOUNCEMENT' => 11,	国家级通知公告
		    'PROVINCE_NOTICE_ANNOUNCEMENT' => 12,	省级通知公告
		    'CITY_NOTICE_ANNOUNCEMENT' => 13,		市级通知公告
		工作动态:
		    'COUNTRY_WORK_DYNAMICS' => 14,		国家级工作动态
		    'PROVINCE_WORK_DYNAMICS' => 15,		省级工作动态
		    'CITY_WORK_DYNAMICS' => 16,			市级工作动态
		信用新闻:
		    'COUNTRY_CREDIT_NEWS' => 17,		国家级信用新闻
		    'PROVINCE_CREDIT_NEWS' => 18,		省级信用新闻
		    'CITY_CREDIT_NEWS' => 19,			市级信用新闻
		信用建设:
		    'COUNTRY_CREDIT_CONSTRUCTION' => 20,	国家级信用建设
		    'PROVINCE_CREDIT_CONSTRUCTION' => 21,	省级信用建设
		    'CITY_CREDIT_CONSTRUCTION' => 22,		市级信用建设
	无：
		信用研究:
		    'CREDIT_KNOWLEDGE' => 23,			信用知识
		    'COUNTRY_CREDIT_RESEARCH' => 24,	国家级信用研究
		    'PROVINCE_CREDIT_RESEARCH' => 25,	省级信用研究
		    'CITY_CREDIT_RESEARCH' => 26,		市级信用研究
	联合奖惩:
		联合惩戒备忘录:
		    'DISCIPLINARY_MEMORANDUM' => 27,	联合惩戒备忘录
		惩戒动态:
		    'DISCIPLINARY_DYNAMICS' => 28,		惩戒动态
		联合奖惩案例:
			'GOVERNMENT_INTEGRITY' => 32,       政务诚信联合奖惩案例
			'BUSINESS_INTEGRITY' => 33,         商务诚信联合奖惩案例
			'JUDICIAL_PUBLIC_TRUST' => 34,      司法公信联合奖惩案例
			'SOCIAL_INTEGRITY' => 35,           社会诚信联合奖惩案例
	无：
		信用监管:
		    'DEPARTMENTAL_SUPERVISION' => 29,   部门监管
		    'MEDIA_SUPERVISION' => 30,			媒体监管
		    'INDUSTRY_SUPERVISION' => 31,		行业监管
	无：
		守信激励与失信惩戒:
		    'TRUSTWORTHY_INFORMATION' => 36,   守信信息守信激励与失信惩戒
		    'SAFETY_ACCIDENT' => 37,		   安全事故守信激励与失信惩戒
		    'BAD_PHENOMENON' => 38,		       不良现象守信激励与失信惩戒
	无：
		无:
		    'PROMISE' => 39,   信用承诺
	无：
		无:
		    'CLASSIC_CASE' => 40,   典型案例
	无：
		无:
		    'SPECICAL_GOVERN' => 41,   失信专项治理
	无：
		诚信建设万里行:
		    'XX_IN_ACTION' => 42,   XX在行动
		    'TYPICAL_ACT_OF_KINDNESS' => 43,   凡人善举树典型
	无：
		信易+:
		    'XINYI_BATCH' => 44,   信易批
		    'XINYI_LOAN' => 45,    信易贷
		    'XINYI_RENT' => 46,    信易租
		    'XINYI_TRAVEL' => 47,   信易游
		    'XINYI_TOURISM' => 48,    信易行
