# 首页定制控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 首页定制

* `CREW`: 发布人控件规范.
* `CATEGORY`: 定制类型控件规范
* `CONTENT`: 定制内容控件规范
* `STATUS`: 发布状态控件规范.

### `CREW`

**发布人** 用于表述发布人

#### 规则

* int

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 发布人数据格式不正确

### `CATEGORY`

**定制类型** 控件规范,用于表述定制类型

#### 规则

* int positive
* 必填
* 定制类型在以下范围中
	* CATEGORY['HOME_PAGE'] | 首页 | 1

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`,定制类型数据格式不正确

### `CONTENT`

**定制内容** 定制内容控件规范

#### 规则

* array
* 必填
* 数组的key值必须在 array('memorialStatus','theme','headerBarLeft','headerBarRight','headerBg','logo','headerSearch','nav','centerDialog','animateWindow','leftFloatCard','frameWindow','rightToolBar','specialColumn','relatedLinks','footerBanner','organizationGroup','silhouette','partyAndGovernmentOrgans','governmentErrorCorrection','footerNav','footerTwo','footerThree')这个范围内
* 数组中必须包含array('memorialStatus','theme','headerBarLeft','headerBarRight','headerBg','logo','headerSearch','nav','rightToolBar','footerNav')
* memorialStatus: int 且范围为0(否),1(是)
* theme : 数组中必须包含 'color', 'image', 
	* color为字符串
	* image为数组,数组中包含 'name','identify',格式为jpg,jpeg,png
* headerBarLeft : 数值中包含 'name','status','url','type'
	* name: 2-10位字符串
	* status : int 0 启用, -2 禁用
	* url: 2-200位字符串,类型为0则为必填
	* type: int ,值在定制范围内
* headerBarRight : 数值中包含 'name','status','url','type'
	* name: 2-10位字符串
	* status : int 0 启用, -2 禁用
	* url: 2-200位字符串,类型为0则为必填
	* type: int ,值在定制范围内
* 头部左侧位置标识页面主题标识和头部右侧位置标识页面主题标识总字数不能超过65位
* headerBg: array,数组中包含 'name','identify',格式为png
* logo: array,数组中包含 'name','identify',格式为png
* headerSearch : 数组中包含 'creditInformation','personalCreditInformation','unifiedSocialCreditCode','legalPerson','newsTitle'
	* creditInformation: 数组中包含status,status : int 0 启用, -2 禁用
	* personalCreditInformation: 数组中包含status,status : int 0 启用, -2 禁用
	* unifiedSocialCreditCode: 数组中包含status,status : int 0 启用, -2 禁用
	* legalPerson: 数组中包含status,status : int 0 启用, -2 禁用
	* newsTitle: 数组中包含status,status : int 0 启用, -2 禁用
* nav : 数值中包含 'name','status','url','image','nav',最多为16个
	* name: 2-8为字符串
	* status : int 0 启用, -2 禁用
	* url: 5-200位字符串,必填
	* image为数组,数组中包含 'name','identify',格式为png
	* nav:int
* centerDialog : 数组中包含 'status','image','url'
	* status : int 0 启用, -2 禁用
	* image为数组,数组中包含 'name','identify',格式为png, jpg, jpeg
	* url :2-200位字符串,选填
* animateWindow : 数组中包含 'status','image','url',飘窗最多为两个
	* status : int 0 启用, -2 禁用
	* image为数组,数组中包含 'name','identify',格式为png, jpg, jpeg
	* url :2-200位字符串,选填
* leftFloatCard: 数组中包含 'status','image','url',最多为5个
	* status : int 0 启用, -2 禁用
	* image为数组,数组中包含'name','identify',格式为png
	* url :2-200位字符串,选填
* frameWindow: 数组中包含 'status','url'
	* status : int 0 启用, -2 禁用
	* url :2-200位字符串,必填
* rightToolBar: 类型分为1 图片, 2 链接, 3 文字,最多为8个
	* 类型为图片: 数组中包含 'name','category','status','images'
		* name: 2-3位字符串
		* status : int 0 启用, -2 禁用
		* images最多为两个,数组中包含 'title','image'
			* title: 格式为: 2-10位字符串
			* image为数组,数组中包含 'name','identify',格式为png, jpg, jpeg
	* 类型为链接: 数组中包含 'name','category','status','url'
		* name: 2-3位字符串
		* status : int 0 启用, -2 禁用
		* url: 链接为5-200位字符串,必填
	* 类型为文字: 数组中包含 'name','category','status','description'
		* name: 2-3位字符串
		* status : int 0 启用, -2 禁用
		* description: 200位字符串,选填
* specialColumn: 数组中包含 'status','image','url',最多为两个
	* status : int 0 启用, -2 禁用
	* image为数组,数组中包含 'name','identify',格式为png, jpg, jpeg
	* url :5-200位字符串,选填
* relatedLinks: 数组中包含 'status','name','items',最多为四个
	* status : int 0 启用, -2 禁用
	* name: 2-20为字符串
	* items,数组中包含 'name','url','status'
		* status : int 0 启用, -2 禁用
		* name: 2-20为字符串
		* url: 2-200为字符串,必填
* footerBanner: 数组中包含 'status','image','url',最多为十个
	* status : int 0 启用, -2 禁用
	* image为数组,数组中包含 'name','identify',格式为png, jpg, jpeg
	* url :5-200位字符串,选填
* organizationGroup数:组中包含 'status','image','url',最多为7个
	* status : int 0 启用, -2 禁用
	* image为数组,数组中包含 'name','identify',格式为png, jpg, jpeg
	* url :2-200位字符串,选填
* silhouette : 组中包含 'status','image','description'
	* status : int 0 启用, -2 禁用
	* image为数组,数组中包含 'name','identify',格式为png
	* description :2-200位字符串,选填
* partyAndGovernmentOrgans: 组中包含 'status','image','url'
	* status : int 0 启用, -2 禁用
	* image为数组,数组中包含 'name','identify',格式为png
	* url :5-200位字符串,选填
* governmentErrorCorrection: 组中包含 'status','image','url'
	* status : int 0 启用, -2 禁用
	* image为数组,数组中包含 'name','identify',格式为png
	* url :5-200位字符串,选填
* footerNav: 组中包含 'status','name','url'
	* status : int 0 启用, -2 禁用
	* name: 2-10为字符串
	* url :5-200位字符串,选填
* footerTwo: 数组中包含 'status','name','url','description'
	* status : int 0 启用, -2 禁用
	* name: 2-10为字符串
	* url :5-200位字符串,选填
	* description: 2-15位字符串
* footerThree: 数组中包含 'status','name','url','description','type'
	* status : int 0 启用, -2 禁用
	* name: 2-10为字符串
	* url :5-200位字符串,选填
	* description: 2-15位字符串
	* type: 在类型范围内
	* type为8: 数组中包含图片,image为数组,数组中包含'name','identify',格式为png

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`: 定制内容格式不正确

### `STATUS`

**发布状态** 控件规范,用于表述发布状态

#### 规则

* int positive
* 必填
* 发布状态在以下范围中
	* STATUS['UNPUBLISHED'| 未发布 | 0
	* STATUS['PUBLISHED'] | 发布 | 2

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`,发布状态格式不正确
