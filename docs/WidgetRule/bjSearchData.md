# 本级资源目录数据搜索控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 本级资源目录数据搜索控件规范

* `INFO_CLASSIFY`: 信息分类控件规范.
* `INFO_CATEGORY`: 信息类别控件规范.
* `CREW`: 发布人控件规范.
* `SOURCE_UNIT`: 来源单位控件规范.
* `SUBJECT_CATEGORY`: 主体类别控件规范.
* `DIMENSION`: 公开范围控件规范.
* `NAME`: 主体名称控件规范.
* `IDENTIFY`: 主体标识控件规范.
* `EXPIRATION_DATE`: 有效期限控件规范.
* `TASK`: 父任务控件规范.
* `SUBTASK`: 子任务控件规范.
* `TEMPLATE`: 资源目录控件规范.

### `INFO_CLASSIFY`
* [信息分类控件规范(INFO_CLASSIFY)](searchData.md)

### `INFO_CATEGORY`
* [信息类别控件规范(INFO_CATEGORY)](searchData.md)

### `CREW`
* [发布人控件规范(CREW)](searchData.md)

### `SOURCE_UNIT`
* [来源单位控件规范(CREW)](searchData.md)

### `SUBJECT_CATEGORY`
* [主体类别控件规范(SUBJECT_CATEGORY)](searchData.md)

### `DIMENSION`
* [公开范围控件规范(DIMENSION)](searchData.md)

### `NAME`
* [主体名称控件规范(NAME)](searchData.md)

### `IDENTIFY`
* [主体标识控件规范(IDENTIFY)](searchData.md)

### `EXPIRATION_DATE`
* [有效期限控件规范(EXPIRATION_DATE)](searchData.md)

### `TASK`
* [父任务控件规范(SUBTASK)](searchData.md)

### `SUBTASK`
* [子任务控件规范(SUBTASK)](searchData.md)

### `TEMPLATE`

**资源目录** 用于表述资源目录

#### 规则

* int

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确
