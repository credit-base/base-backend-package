# 信用随手拍控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 信用随手拍

* `DESCRIPTION`: 描述控件规范.
* `REJECT_REASON`: 驳回原因控件规范
* `ATTACHMENTS`: 附件控件规范.

### `DESCRIPTION`
* [描述控件规范(DESCRIPTION)](common.md)(必填)

### `REJECT_REASON`
* [驳回原因控件规范(DESCRIPTION)](common.md)(必填)

### `ATTACHMENTS`

**附件** 控件规范，用于上传图片/视频

#### 规则

* 图片和视频只能选择一种
* 图片格式为jpg,jpeg,png,bmp,gif,图片数量不能大于9,图片不大于2M
* 视频格式为mp4,只能上传一个视频,不大于200M
* 必填

#### 错误提示

* `ER-0100`,附件不能为空,因现在文件服务器还没部署好所以该字段先为选填,服务器部署完要改为必填
* `ER-0101`:附件格式不正确
