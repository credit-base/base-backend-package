# OA员工控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

* `REAL_NAME`: 姓名
* `CELLPHONE`: 手机号码
* `PASSWORD`: 密码
* `CARDID`: 身份证号
* `USER_GROUP`: 所属委办局
* `DEPARTMENT`: 所属科室
* `CATEGORY`: 员工类型
* `PURVIEW`: 权限范围

---

### `REAL_NAME`
* [姓名控件规范(REAL_NAME)](user.md)(必填)

### `CELLPHONE`
* [手机号控件规范(CELLPHONE)](user.md)(必填)

### `PASSWORD`
* [密码控件规范(PASSWORD)](user.md)(必填)

### `CARDID`
* [身份证号控件规范(CARDID)](user.md)(选填)

### `USER_GROUP`

**所属委办局** 所属委办局控件规范

#### 规则

* int
* 选填

#### 错误提示

* `ER-0010`: 所属委办局不存在
* `ER-0101`: 所属委办局格式不正确

### `DEPARTMENT`

**所属科室** 所属科室控件规范

#### 规则

* int
* 选填
* 所属科室的所属委办局必须与员工的所属委办局一致

#### 错误提示

* `ER-0010`: 所属科室不存在
* `ER-0101`: 所属科室格式不正确
* `ER-1001`: 该所属科室不属于该所属委办局

### `CATEGORY`

**员工类型** 员工类型控件规范

#### 规则

* 必填
* 数字，可选范围 2，3，4
	* PLATFORM_ADMINISTRATOR => 2 | 平台管理员
	* USER_GROUP_ADMINISTRATOR => 3 | 委办局管理员
	* OPERATOR => 4 | 操作用户

#### 错误提示

* `ER-0100`: 员工类型不能为空
* `ER-0101`: 员工类型格式不正确

### `PURVIEW`

**权限范围** 权限范围控件规范

#### 规则

* 必填
* 一维数组,且值为int

#### 错误提示

* `ER-0100`: 权限范围不能为空
* `ER-0101`: 权限范围格式不正确
