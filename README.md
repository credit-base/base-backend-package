# 公共业务基础项目后端服务包

### 目录

* [简介](#abstract)
* [沟通记录](#communicationRecord)
* [接口文档](#api)
* [项目字典](#dictionary)
* [控件规范](#widgetRule)
* [错误规范](#errorRule)
* [参考文档](#tutor)
* [版本记录](#version)

---

### <a name="abstract">简介</a>

用于实现 信用平台2.0 公共业务基础项目 后端服务包.

---

### <a name="communicationRecord">沟通记录</a>

根据沟通日期命名.
	
---

### <a name="api">接口文档</a>

* 接口错误返回说明 `error`
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明")
* 员工接口文档 `crew`
	* [员工接口文档](./docs/Api/crewApi.md "员工接口文档")
* 委办局接口文档 `userGroup`
	* [委办局接口文档](./docs/Api/userGroupApi.md "委办局接口文档")
* 科室接口文档 `department`
	* [科室接口文档](./docs/Api/departmentApi.md "科室接口文档")
* 国标目录接口文档 `gbTemplate`
	* [国标目录接口文档](./docs/Api/gbTemplateApi.md "国标目录接口文档")
* 本级目录接口文档 `bjTemplate`
	* [本级目录接口文档](./docs/Api/bjTemplateApi.md "本级目录接口文档")
* 委办局目录接口文档 `wbjTemplate`
	* [委办局目录接口文档](./docs/Api/wbjTemplateApi.md "委办局目录接口文档")
* 基础目录接口文档 `baseTemplate`
	* [基础目录接口文档](./docs/Api/baseTemplateApi.md "基础目录接口文档")
* 工单任务接口文档 `workOrderTask`
	* [工单任务接口文档](./docs/Api/workOrderTaskApi.md "工单任务接口文档")
* 原始资源目录数据接口文档 `ysSearchData`
	* [原始资源目录数据接口文档](./docs/Api/ysSearchDataApi.md "原始资源目录数据接口文档")
* 委办局资源目录数据接口文档 `wbjSearchData`
	* [委办局资源目录数据接口文档](./docs/Api/wbjSearchDataApi.md "委办局资源目录数据接口文档")
* 本级资源目录数据接口文档 `bjSearchData`
	* [本级资源目录数据接口文档](./docs/Api/bjSearchDataApi.md "本级资源目录数据接口文档")
* 国标资源目录数据接口文档 `gbSearchData`
	* [国标资源目录数据接口文档](./docs/Api/gbSearchDataApi.md "国标资源目录数据接口文档")
* 任务接口文档 `task`
	* [任务接口文档](./docs/Api/taskApi.md "任务接口文档")
* 失败数据接口文档 `errorData`
	* [失败数据接口文档](./docs/Api/errorDataApi.md "失败数据接口文档")
* 规则接口文档 `rule`
	* [规则接口文档](./docs/Api/ruleApi.md "规则接口文档")
* 规则审核接口文档 `unAuditedRule`
	* [规则审核接口文档](./docs/Api/unAuditedRuleApi.md "规则审核接口文档")
* 新闻接口文档 `news`
	* [新闻接口文档](./docs/Api/newsApi.md "新闻接口文档")
* 新闻审核接口文档 `unAuditedNews`
	* [新闻审核接口文档](./docs/Api/unAuditedNewsApi.md "新闻审核接口文档")
* 前台用户接口文档 `member`
	* [前台用户接口文档](./docs/Api/memberApi.md "前台用户接口文档")
* 信用刊物接口文档 `journal`
	* [信用刊物接口文档](./docs/Api/journalApi.md "信用刊物接口文档")
* 信用刊物审核接口文档 `unAuditedJournal`
	* [信用刊物审核接口文档](./docs/Api/unAuditedJournalApi.md "信用刊物审核接口文档")
* 信用随手拍接口文档 `creditPhotography`
	* [信用随手拍接口文档](./docs/Api/creditPhotographyApi.md "信用随手拍接口文档")
* 调度任务系统接口文档 `task`
	* [上传记录接口文档](./docs/Api/notifyRecordApi.md "上传记录接口文档")
* 网站定制接口文档 `websiteCustomize`
	* [网站定制接口文档](./docs/Api/websiteCustomizeApi.md "网站定制接口文档")
* 企业管理接口文档 `enterprise`
	* [企业管理接口文档](./docs/Api/enterpriseApi.md "企业管理接口文档")
* 信用投诉接口文档 `complaint`
	* [信用投诉接口文档](./docs/Api/complaintApi.md "信用投诉接口文档")
* 信用投诉审核接口文档 `unAuditedComplaint`
	* [信用投诉审核接口文档](./docs/Api/unAuditedComplaintApi.md "信用投诉审核接口文档")
* 信用表扬接口文档 `praise`
	* [信用表扬接口文档](./docs/Api/praiseApi.md "信用表扬接口文档")
* 信用表扬审核接口文档 `unAuditedPraise`
	* [信用表扬审核接口文档](./docs/Api/unAuditedPraiseApi.md "信用表扬审核接口文档")
* 异议申诉接口文档 `appeal`
	* [异议申诉接口文档](./docs/Api/appealApi.md "异议申诉接口文档")
* 异议申诉审核接口文档 `unAuditedAppeal`
	* [异议申诉审核接口文档](./docs/Api/unAuditedAppealApi.md "异议申诉审核接口文档")
* 问题反馈接口文档 `feedback`
	* [问题反馈接口文档](./docs/Api/feedbackApi.md "问题反馈接口文档")
* 问题反馈审核接口文档 `unAuditedFeedback`
	* [问题反馈审核接口文档](./docs/Api/unAuditedFeedbackApi.md "问题反馈审核接口文档")
* 信用问答接口文档 `qa`
	* [信用问答接口文档](./docs/Api/qaApi.md "信用问答接口文档")
* 信用问答审核接口文档 `unAuditedQA`
	* [信用问答审核接口文档](./docs/Api/unAuditedQAApi.md "信用问答审核接口文档")

---

### <a name="dictionary">项目字典</a>

* 通用字典 `common`
	* [通用字典](./docs/Dictionary/common.md "通用字典")
* 员工字典 `crew`
	* [员工字典](./docs/Dictionary/crew.md "员工字典")
* 委办局字典 `userGroup`
	* [委办局字典](./docs/Dictionary/userGroup.md "委办局字典")
* 科室字典 `department`
	* [科室字典](./docs/Dictionary/department.md "科室字典")
* [目录字典](./docs/Dictionary/template.md "目录字典") `template`
	* 国标目录字典 `gbTemplate`
		* [国标目录字典](./docs/Dictionary/gbTemplate.md "国标目录字典")
	* 本级目录字典 `bjTemplate`
		* [本级目录字典](./docs/Dictionary/bjTemplate.md "本级目录字典")
	* 委办局目录字典 `wbjTemplate`
		* [委办局目录字典](./docs/Dictionary/wbjTemplate.md "委办局目录字典")
* 工单任务字典 `workOrderTask`
	* [工单任务字典](./docs/Dictionary/workOrderTask.md "工单任务字典")
* [资源目录数据字典](./docs/Dictionary/searchData.md "资源目录数据字典") `searchData`
	* 原始资源目录数据字典 `ysSearchData`
		* [原始资源目录数据字典](./docs/Dictionary/ysSearchData.md "原始资源目录数据字典")
	* 委办局资源目录数据字典 `wbjSearchData`
		* [委办局资源目录数据字典](./docs/Dictionary/wbjSearchData.md "委办局资源目录数据字典")
	* 本级资源目录数据字典 `bjSearchData`
		* [本级资源目录数据字典](./docs/Dictionary/bjSearchData.md "本级资源目录数据字典")
	* 国标资源目录数据字典 `gbSearchData`
		* [国标资源目录数据字典](./docs/Dictionary/gbSearchData.md "国标资源目录数据字典")
	* 任务字典 `task`
		* [任务字典](./docs/Dictionary/task.md "任务字典")
	* 失败数据字典 `errorData`
		* [失败数据字典](./docs/Dictionary/errorData.md "失败数据字典")
* 新闻字典 `news`
	* [新闻字典](./docs/Dictionary/news.md "新闻字典")
* 新闻审核字典 `unAuditedNews`
	* [新闻审核字典](./docs/Dictionary/unAuditedNews.md "新闻审核字典")
* 前台用户字典 `member`
	* [前台用户字典](./docs/Dictionary/member.md "前台用户字典")
* 信用刊物字典 `journal`
	* [信用刊物字典](./docs/Dictionary/journal.md "信用刊物字典")
* 信用刊物审核字典 `unAuditedJournal`
	* [信用刊物审核字典](./docs/Dictionary/unAuditedJournal.md "信用刊物审核字典")
* 信用随手拍字典 `creditPhotography`
	* [信用随手拍字典](./docs/Dictionary/creditPhotography.md "信用随手拍字典")
* 调度任务系统字典 `task`
	* [调度任务系统字典](./docs/Dictionary/scheduleTask.md "调度任务系统字典")
* 规则字典 `rule`
	* [规则字典](./docs/Dictionary/rule.md "规则字典")
* 规则审核字典 `unAuditedRule`
	* [规则审核字典](./docs/Dictionary/unAuditedRule.md "规则审核字典")
* 网站地址字典 `websiteCustomize`
	* [网站地址字典](./docs/Dictionary/websiteCustomize.md "网站地址字典")
* 企业管理字典 `enterprise`
	* [企业管理字典](./docs/Dictionary/enterprise.md "企业管理字典")
* 基础资源目录管理字典 `baseTemplate`
	* [基础资源目录管理字典](./docs/Dictionary/baseTemplate.md "基础资源目录管理字典")
* 互动类通用字典 `interaction`
	* [互动类通用字典](./docs/Dictionary/interaction.md "互动类通用字典")
* 信用投诉字典 `complaint`
	* [信用投诉字典](./docs/Dictionary/complaint.md "信用投诉字典")
* 信用投诉审核字典 `unAuditedComplaint`
	* [信用投诉审核字典](./docs/Dictionary/unAuditedComplaint.md "信用投诉审核字典")
* 信用表扬字典 `praise`
	* [信用表扬字典](./docs/Dictionary/praise.md "信用表扬字典")
* 信用表扬审核字典 `unAuditedPraise`
	* [信用表扬审核字典](./docs/Dictionary/unAuditedPraise.md "信用表扬审核字典")
* 异议申诉字典 `appeal`
	* [异议申诉字典](./docs/Dictionary/appeal.md "异议申诉字典")
* 异议申诉审核字典 `unAuditedAppeal`
	* [异议申诉审核字典](./docs/Dictionary/unAuditedAppeal.md "异议申诉审核字典")
* 问题反馈字典 `feedback`
	* [问题反馈字典](./docs/Dictionary/feedback.md "问题反馈字典")
* 问题反馈审核字典 `unAuditedFeedback`
	* [问题反馈审核字典](./docs/Dictionary/unAuditedFeedback.md "问题反馈审核字典")
* 信用问答字典 `qa`
	* [信用问答字典](./docs/Dictionary/qa.md "信用问答字典")
* 信用问答审核字典 `unAuditedQA`
	* [信用问答审核字典](./docs/Dictionary/unAuditedQA.md "信用问答审核字典")

---

### <a name="widgetRule">控件规范</a>

* 通用控件规范 `common`
	* [通用控件规范](./docs/WidgetRule/common.md "通用控件规范")
* 人通用控件规范 `user`
    * [人通用控件规范](./docs/WidgetRule/user.md "人通用控件规范")
* 员工控件规范 `crew`
    * [员工控件规范](./docs/WidgetRule/crew.md "员工控件规范")
* 科室控件规范 `department`
    * [科室控件规范](./docs/WidgetRule/department.md "科室控件规范")
* [资源目录数据控件规范](./docs/WidgetRule/searchData.md "资源目录数据控件规范") `searchData`
	* 原始资源目录数据控件规范 `ysSearchData`
		* [原始资源目录数据控件规范](./docs/WidgetRule/ysSearchData.md "原始资源目录数据控件规范")
	* 委办局资源目录数据控件规范 `wbjSearchData`
		* [委办局资源目录数据控件规范](./docs/WidgetRule/wbjSearchData.md "委办局资源目录数据控件规范")
	* 本级资源目录数据控件规范 `bjSearchData`
		* [本级资源目录数据控件规范](./docs/WidgetRule/bjSearchData.md "本级资源目录数据控件规范")
	* 国标资源目录数据控件规范 `gbSearchData`
		* [国标资源目录数据控件规范](./docs/WidgetRule/gbSearchData.md "国标资源目录数据控件规范")
* 新闻控件规范 `news`
	* [新闻控件规范](./docs/WidgetRule/news.md "新闻控件规范")
* 前台用户控件规范 `members`
	* [前台用户控件规范](./docs/WidgetRule/members.md "前台用户控件规范")	
* 信用刊物控件规范 `journal`
	* [信用刊物控件规范](./docs/WidgetRule/journal.md "信用刊物控件规范")
* 信用随手拍控件规范 `creditPhotography`
	* [信用随手拍控件规范](./docs/WidgetRule/creditPhotography.md "信用随手拍控件规范")
* 规则控件规范 `rule`
	* [规则控件规范](./docs/WidgetRule/rule.md "规则控件规范")
* 网站定制控件规范 `websiteCustomize`
	* [网站定制控件规范](./docs/WidgetRule/websiteCustomize.md "网站定制控件规范")
* 信用投诉控件规范 `complaint`
	* [信用投诉控件规范](./docs/WidgetRule/complaint.md "信用投诉控件规范")
* 信用表扬控件规范 `praise`
	* [信用表扬控件规范](./docs/WidgetRule/praise.md "信用表扬控件规范")
* 异议申诉控件规范 `appeal`
	* [异议申诉控件规范](./docs/WidgetRule/appeal.md "异议申诉控件规范")
* 问题反馈控件规范 `feedback`
	* [问题反馈控件规范](./docs/WidgetRule/feedback.md "问题反馈控件规范")
* 信用问答控件规范 `qa`
	* [信用问答控件规范](./docs/WidgetRule/qa.md "信用问答控件规范")

---

### <a name="errorRule">错误规范</a>

* 通用错误规范 `common`
	* [通用错误规范](./docs/ErrorRule/common.md "通用错误规范")
* 人通用错误规范 `user`
    * [人通用错误规范](./docs/ErrorRule/user.md "人通用错误规范")
* 员工错误规范 `crew`
    * [员工错误规范](./docs/ErrorRule/crew.md "员工错误规范")

---

### <a name="tutor">参考文档</a>

* [jsonapi媒体协议](http://jsonapi.org/ "jsonapi")
* [框架](https://github.com/chloroplast1983/marmot) 

---

### <a name="version">版本记录</a>

* 说明: 基础功能为0.1版本,信用数据大闭环为0.2版本
* 基础功能: 委办局管理,科室管理,员工管理,新闻管理,门户网用户管理,信用刊物管理,信用随手拍管理
* 信用数据大闭环:目录管理子系统,规则管理子系统,数据管理子系统,前置机交换子系统

* [0.1.0](./Docs/Version/0.1.0.md "0.1.0")
	* [0.1.1](./Docs/Version/0.1.1.md "0.1.1")
	* [0.1.2](./Docs/Version/0.1.2.md "0.1.2")
	* [0.1.3](./Docs/Version/0.1.3.md "0.1.3")
	* [0.1.4](./Docs/Version/0.1.4.md "0.1.4")
	* [0.1.5](./Docs/Version/0.1.5.md "0.1.5")
* [0.2.0](./Docs/Version/0.2.0.md "0.2.0")
	* [0.2.1](./Docs/Version/0.2.1.md "0.2.1")
	* [0.2.2](./Docs/Version/0.2.2.md "0.2.2")
	* [0.2.3](./Docs/Version/0.2.3.md "0.2.3")
	* [0.2.4](./Docs/Version/0.2.4.md "0.2.4")
* [0.5.0](./Docs/Version/0.5.0.md "0.5.0")
