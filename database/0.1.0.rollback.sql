

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_user_group;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_department;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_crew;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_apply_form;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_news;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_member;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_journal;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_credit_photography;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_complaint;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_praise;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_appeal;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_feedback;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_qa;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_reply;

-- --------------------------------------------------------

