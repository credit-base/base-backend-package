

-- --------------------------------------------------------

INSERT INTO `pcore_crew` (`crew_id`, `user_name`, `cellphone`, `password`, `salt`, `real_name`, `card_id`, `category`, `user_group_id`, `department_id`, `purview`, `create_time`, `update_time`, `status`, `status_time`) VALUES
(1, '18800000000', '18800000000', 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '超级管理员', '412825199009094538', 1, 1, 1, '[]', 1516174523, 1516174523, 0, 0);

-- --------------------------------------------------------

INSERT INTO `pcore_member` (`member_id`, `user_name`, `real_name`, `cellphone`, `email`, `cardid`, `gender`, `password`, `salt`, `contact_address`, `security_question`, `security_answer`, `create_time`, `update_time`, `status`, `status_time`) VALUES
(1, '18800000000', '张科', '18800000000', '997934301@qq.com', '412825199009094538', 1, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '雁塔区', 1, '张文涛', 1516174523, 1516174523, 0, 0),
(2, '18800000001', '白玉', '18800000001', '997934302@qq.com', '412825199009094531', 2, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '长安区', 1, '白宇文', 1516174523, 1516174523, 0, 0),
(3, '18800000002', '赵伟', '18800000002', '997934303@qq.com', '412825199009094532', 1, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '长安区', 2, '我们的父辈', 1516174523, 1516174523, 0, 0),
(4, '18800000003', '钱森森', '18800000003', '997934311@qq.com', '412825199009094533', 2, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '莲湖区', 3, '白宇文', 1516174523, 1516174523, 0, 0),
(5, '18800000004', '孙子轩', '18800000004', '997934304@qq.com', '412825199009094534', 2, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '莲湖区', 4, '高新一中', 1516174523, 1516174523, 0, 0),
(6, '18800000005', '李强', '18800000005', '997934305@qq.com', '412825199009094535', 2, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '高新区', 4, '高新一中', 1516174523, 1516174523, 0, 0),
(7, '18800000006', '周驰', '18800000006', '997934306@qq.com', '412825199009094536', 2, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '高新区', 5, '黑色', 1516174523, 1516174523, 0, 0),
(8, '18800000007', '吴白', '18800000007', '997934307@qq.com', '412825199009094537', 2, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '高新区', 5, '白色', 1516174523, 1516174523, 0, 0),
(9, '18800000008', '郑倩文', '18800000008', '997934308@qq.com', '412825199009094538', 2, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '莲湖区', 5, '白色', 1516174523, 1516174523, 0, 0),
(10, '18800000009', '王倩', '18800000009', '997934309@qq.com', '412825199009094539', 2, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '莲湖区', 5, '白色', 1516174523, 1516174523, 0, 0),
(11, '18800000010', '冯倩', '18800000010', '997934310@qq.com', '412825199009094510', 2, 'b0e735ecd1370d24fd026ccabcd2d198', 'W8i3', '莲湖区', 5, '白色', 1516174523, 1516174523, 0, 0);

INSERT INTO `pcore_website_customize` (`website_customize_id`, `crew_id`, `category`, `content`, `version`, `create_time`, `update_time`, `status`, `status_time`) VALUES
(1, 1, 1, '{
  "nav": [
    {
      "nav": "1",
      "url": "/",
      "name": "首页",
      "image": {
        "name": "首页",
        "identify": "首页.png"
      },
      "status": "0"
    },
    {
      "nav": "2",
      "url": "/creditDynamics/index",
      "name": "信用动态",
      "image": [],
      "status": "0"
    },
    {
      "nav": "3",
      "url": "/news/index?category=NC0",
      "name": "政策法规",
      "image": [],
      "status": "0"
    },
    {
      "nav": "4",
      "url": "/news/index?category=NC4",
      "name": "标准规范",
      "image": [],
      "status": "0"
    },
    {
      "nav": "5",
      "url": "/creditPublicities/index",
      "name": "信息公示",
      "image": [],
      "status": "0"
    },
    {
      "nav": "6",
      "url": "/disciplinaries/index",
      "name": "联合奖惩",
      "image": [],
      "status": "0"
    },
    {
      "nav": "7",
      "url": "/news?type=MDMy",
      "name": "信用承诺",
      "image": [],
      "status": "0"
    },
    {
      "nav": "8",
      "url": "/news?type=MDQp",
      "name": "典型案例",
      "image": [],
      "status": "0"
    },
    {
      "nav": "9",
      "url": "/news/index?category=NDM",
      "name": "信用研究",
      "image": [],
      "status": "0"
    },
    {
      "nav": "10",
      "url": "/journals",
      "name": "信用刊物",
      "image": [],
      "status": "0"
    },
    {
      "nav": "11",
      "url": "/creditPhotographys",
      "name": "信用随手拍",
      "image": [],
      "status": "0"
    },
    {
      "nav": "0",
      "url": "/",
      "name": "办事服务",
      "image": [],
      "status": "0"
    },
    {
      "nav": "0",
      "url": "/",
      "name": "政府信息公开",
      "image": [],
      "status": "0"
    },
    {
      "nav": "0",
      "url": "/",
      "name": "信易+",
      "image": [],
      "status": "0"
    },
    {
      "nav": "0",
      "url": "/",
      "name": "信用地图",
      "image": [],
      "status": "0"
    },
    {
      "nav": "0",
      "url": "/",
      "name": "工程信用库",
      "image": [],
      "status": "0"
    }
  ],
  "logo": {
    "name": "信用中国（四川宜宾）",
    "identify": "https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/common/logo.png"
  },
  "theme": {
    "color": "#41245",
    "image": {
      "name": "headerBg",
      "identify": "https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/common/bg-header.png"
    }
  },
  "headerBg": {
    "name": "headerBg",
    "identify": "https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/common/bg-header.png"
  },
  "headerSearch": {
    "newsTitle": {
      "name": "站内文章",
      "status": "0"
    },
    "legalPerson": {
      "name": "法人",
      "status": "0"
    },
    "creditInformation": {
      "name": "信用信息",
      "status": "0"
    },
    "unifiedSocialCreditCode": {
      "name": "统一社会信用代码",
      "status": "0"
    },
    "personalCreditInformation": {
      "name": "个人信用信息",
      "status": "0"
    }
  },
  "headerBarLeft": [
    {
      "url": "",
      "name": "访问量统计",
      "type": "1",
      "status": "0"
    }
  ],
  "headerBarRight": [
    {
      "url": "",
      "name": "简体/繁体",
      "type": "2",
      "status": "0"
    },
    {
      "url": "",
      "name": "无障碍阅读",
      "type": "3",
      "status": "0"
    },
    {
      "url": "",
      "name": "用户中心",
      "type": "4",
      "status": "0"
    },
    {
      "url": "navigations/index",
      "name": "网站声明",
      "type": "0",
      "status": "0"
    }
  ],
  "silhouette": {
    "image": {
      "name": "",
      "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/yibin/yibin-portal-bottom_silhouette.png"
    },
    "status": "0",
    "description": "历史文化"
  },
  "footerNav": [
    {
      "url": "/about",
      "name": "帮助说明",
      "status": "0"
    },
    {
      "url": "/about",
      "name": "技术支持",
      "status": "0"
    },
    {
      "url": "/about",
      "name": "链接我站",
      "status": "0"
    },
    {
      "url": "/about",
      "name": "隐私保护",
      "status": "0"
    },
    {
      "url": "/about",
      "name": "版权声明",
      "status": "0"
    },
    {
      "url": "/about",
      "name": "关于我们",
      "status": "0"
    },
    {
      "url": "/navigations/index",
      "name": "网站地图",
      "status": "0"
    }
  ],
  "footerTwo": [
    {
      "url": "",
      "name": "主办单位：",
      "type": "0",
      "status": "0",
      "description": "宜宾市发展和改革委员会"
    },
    {
      "url": "",
      "name": "联系电话：",
      "type": "0",
      "status": "0",
      "description": "0831-2339249"
    }
  ],
  "footerThree": [
    {
      "url": "https://beian.miit.gov.cn/#/Integrated/index",
      "name": "备案号",
      "type": "7",
      "status": "0",
      "description": "蜀ICP备2021016944号-1"
    },
    {
      "url": "http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=51150402000015",
      "name": "公网安备",
      "type": "8",
      "status": "0",
      "description": "51150402000015号"
    },
    {
      "url": "",
      "name": "网站标识码",
      "type": "9",
      "status": "0",
      "description": "5115000065"
    }
  ],
  "frameWindow": {
    "url": "http://www.yibin.gov.cn/qt_297/test/test1/index.html",
    "status": "0"
  },
  "relatedLinks": [
    {
      "name": "信用体系建设共建单位",
      "items": [
        {
          "url": "http://fg.yibin.gov.cn/",
          "name": "市发展改革委",
          "status": "0"
        },
        {
          "url": "javascript:void(0)",
          "name": "人行宜宾市中支",
          "status": "0"
        },
        {
          "url": "http://fg.yibin.gov.cn/",
          "name": "市委宣传部",
          "status": "0"
        },
        {
          "url": "javascript:void(0)",
          "name": "市网信办",
          "status": "0"
        }
      ],
      "status": "0"
    },
    {
      "name": "电子政务网站",
      "items": [
        {
          "url": "http://fg.yibin.gov.cn/",
          "name": "市发展改革委",
          "status": "0"
        },
        {
          "url": "javascript:void(0)",
          "name": "人行宜宾市中支",
          "status": "0"
        },
        {
          "url": "http://fg.yibin.gov.cn/",
          "name": "市委宣传部",
          "status": "0"
        },
        {
          "url": "javascript:void(0)",
          "name": "市网信办",
          "status": "0"
        }
      ],
      "status": "0"
    }
  ],
  "footerBanner": [
    {
      "url": "http://www.xinhuanet.com/politics/fdbnlqhxzc/",
      "image": {
        "name": "",
        "identify": "https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/ossProduction/banner-fd.png"
      },
      "status": "0"
    },
    {
      "url": "http://www.sc.gov.cn/10462/sctfjkt/sctfjkt.shtml",
      "image": {
        "name": "",
        "identify": "https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/ossProduction/banner-jkt.png"
      },
      "status": "0"
    }
  ],
  "centerDialog": {
    "url": "javascript:;",
    "image": {
      "name": "",
      "identify": "http://portal.ny.qixinyun.com/images/party-100-year.png"
    },
    "status": "0"
  },
  "specialColumn": [
    {
      "url": "http://portal.yb.qixinyun.com/honestyConstructions/index",
      "image": {
        "name": "",
        "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/yibin/yibin-portal-topic-banner.jpg"
      },
      "status": "0"
    },
    {
      "url": "http://portal.yb.qixinyun.com/news?type=My8",
      "image": {
        "name": "",
        "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/yibin/xinyi-plus-banner.jpg"
      },
      "status": "0"
    }
  ],
  "animateWindow": [
    {
      "url": "javascript:;",
      "image": {
        "name": "信易贷申请入口",
        "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/nanyang/nanyang-portal-index-aside.png"
      },
      "status": "0"
    },
    {
      "url": "http://portal.px.qixinyun.com/redBlacks?scene=MA",
      "image": {
        "name": "红黑榜名单查询",
        "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/start/pxiang-portal-index-aside.png"
      },
      "status": "0"
    }
  ],
  "leftFloatCard": [
    {
      "url": "http://portal.ny.qixinyun.com/outbreakColumn/index",
      "image": {
        "name": "",
        "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/portal-yiqing-image.png"
      },
      "status": "0"
    },
    {
      "url": "http://portal.ny.qixinyun.com/creditPhotographies?scene=MA",
      "image": {
        "name": "",
        "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/portal-suishoupai-image.png"
      },
      "status": "0"
    },
    {
      "url": "https://www.celoan.cn/?city=Pingxiang",
      "image": {
        "name": "融资",
        "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/start/pingxiang/home-left-img.png"
      },
      "status": "0"
    }
  ],
  "rightToolBar": [
    {
      "name": "调查",
      "images": [],
      "status": "0",
      "category": "1"
    },
    {
      "name": "微信",
      "image": {
        "name": "微信公众号",
        "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/yibin/weixin.jpg"
      },
      "status": "0",
      "category": "1"
    },
    {
      "name": "App",
      "images": [
        {
          "title": "安卓版APP",
          "image": {
            "name": "安卓版APP",
            "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/yibin/weixin.jpg"
          }
        },
        {
          "title": "IOS版APP",
          "image": {
            "name": "IOS版APP",
            "identify": "https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/yibin/weixin.jpg"
          }
        }
      ],
      "status": "0",
      "category": "1"
    },
    {
      "url": "/",
      "name": "表扬",
      "images": [],
      "status": "0",
      "category": "2"
    },
    {
      "name": "邮箱",
      "images": [],
      "status": "0",
      "category": "3",
      "description": "&amp;lt;h3&amp;gt;邮箱地址&amp;lt;h3&amp;gt;      &amp;lt;p&amp;gt;（宜宾市发展改革委）&amp;lt;p&amp;gt;      &amp;lt;p&amp;gt;scybxytxbgs@163.com&amp;lt;p&amp;gt;"
    }
  ],
  "memorialStatus": "0",
  "organizationGroup": [
    {
      "url": "http://www.creditchina.gov.cn/",
      "image": {
        "name": "",
        "identify": "https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link1.png"
      },
      "status": "0"
    },
    {
      "url": "http://zxgk.court.gov.cn/",
      "image": {
        "name": "",
        "identify": "https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link2.png"
      },
      "status": "0"
    },
    {
      "url": "http://www.gsxt.gov.cn/index.htm",
      "image": {
        "name": "",
        "identify": "https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link3.png"
      },
      "status": "0"
    },
    {
      "url": "http://hd.chinatax.gov.cn/xxk/",
      "image": {
        "name": "",
        "identify": "https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link4.png"
      },
      "status": "0"
    },
    {
      "url": "http://www.creditsc.gov.cn/",
      "image": {
        "name": "",
        "identify": "https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link5.png"
      },
      "status": "0"
    },
    {
      "url": "http://www.yibin.gov.cn/",
      "image": {
        "name": "",
        "identify": "https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link6.png"
      },
      "status": "0"
    },
    {
      "url": "http://fg.yibin.gov.cn/",
      "image": {
        "name": "",
        "identify": "https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link7.png"
      },
      "status": "0"
    }
  ],
  "partyAndGovernmentOrgans": {
    "url": "https://bszs.conac.cn/sitename?method=show&amp;id=A259C6A7DC646119E05310291AACAE96",
    "image": {
      "name": "",
      "identify": "https://dcs.conac.cn/image/red_error.png"
    },
    "status": "0"
  },
  "governmentErrorCorrection": {
    "url": "https://bszs.conac.cn/sitename?method=show&amp;id=A259C6A7DC646119E05310291AACAE96",
    "image": {
      "name": "",
      "identify": "https://zfwzgl.www.gov.cn/exposure/images/jiucuo.png"
    },
    "status": "0"
  }
}
', 2108120000, 1516174523, 1516174523, 2, 0);


