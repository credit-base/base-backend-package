
CREATE TABLE `pcore_qzj_template` (
  `qzj_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `category` tinyint(1) NOT NULL COMMENT '目录类别，前置机委办局 20 | 前置机本级 21 | 前置机国标 22',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',
  `source_unit_id` int(10) NOT NULL COMMENT '来源委办局id',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='本级资源目录表';

ALTER TABLE `pcore_qzj_template`
  ADD PRIMARY KEY (`qzj_template_id`),
  ADD UNIQUE KEY `identify` (`identify`, `source_unit_id`, `category`);

ALTER TABLE `pcore_qzj_template`
  MODIFY `qzj_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';
