-- --------------------------------------------------------

CREATE TABLE `pcore_apply_form` (
  `apply_form_id` int(10) NOT NULL COMMENT '申请信息id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `relation_id` int(10) NOT NULL COMMENT '关联id',
  `apply_usergroup_id` int(10) NOT NULL COMMENT '审核委办局',
  `apply_crew_id` int(10) NOT NULL COMMENT '审核人',
  `operation_type` tinyint(1) NOT NULL COMMENT '操作类型',
  `apply_info` text NOT NULL COMMENT '申请信息',
  `reject_reason` text NOT NULL COMMENT '驳回原因',
  `apply_info_category` tinyint(1) NOT NULL COMMENT '可以申请的信息分类',
  `apply_info_type` int(10) NOT NULL COMMENT '申请信息的最小分类',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `apply_status` tinyint(1) NOT NULL COMMENT '状态 0,待审核, 2 通过, -2 驳回)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='申请信息表';

ALTER TABLE `pcore_apply_form`
  ADD PRIMARY KEY (`apply_form_id`);

ALTER TABLE `pcore_apply_form`
  MODIFY `apply_form_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '申请信息id';

CREATE TABLE `pcore_news` (
  `news_id` int(10) NOT NULL COMMENT '新闻id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `source` varchar(255) NOT NULL COMMENT '来源',
  `cover` json NOT NULL COMMENT '封面',
  `attachments` json NOT NULL COMMENT '附件',
  `content` longtext NOT NULL COMMENT '内容',
  `description` varchar(255) NOT NULL COMMENT '新闻简介',
  `parent_category` tinyint(1) NOT NULL COMMENT '父级分类',
  `category` int(10) NOT NULL COMMENT '分类',
  `type` int(10) NOT NULL COMMENT '新闻类型',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `publish_usergroup_id` int(10) NOT NULL COMMENT '发布单位id',
  `stick` tinyint(1) NOT NULL COMMENT '置顶(置顶 2, 默认 不置顶 0)',
  `dimension` tinyint(1) NOT NULL COMMENT '数据共享维度',
  `banner_status` tinyint(1) NOT NULL COMMENT '轮播状态(轮播 2,  默认 不轮播 0)',
  `banner_image` json NOT NULL COMMENT '轮播图图片',
  `home_page_show_status` tinyint(1) NOT NULL COMMENT '首页展示状态(展示 2,  默认 不展示 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(禁用 -2,  默认 启用 0)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='新闻表';

ALTER TABLE `pcore_news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `type` (`type`,`publish_usergroup_id`,`status`);

ALTER TABLE `pcore_news`
  MODIFY `news_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '新闻id';
