-- --------------------------------------------------------

CREATE TABLE `pcore_ys_search_data` (
  `ys_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `wbj_template_id` int(10) NOT NULL COMMENT '委办局资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `ys_items_data_id` int(10) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认启用),(STATUS_DISABLED,-2,禁用)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='原始资源目录数据搜索表';

ALTER TABLE `pcore_ys_search_data`
  ADD PRIMARY KEY (`ys_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`wbj_template_id`,`identify`),
  ADD KEY `wbj_template_id` (`wbj_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_ys_search_data`
  MODIFY `ys_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_ys_items_data` (
  `ys_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='原始资源目录数据表';

ALTER TABLE `pcore_ys_items_data`
  ADD PRIMARY KEY (`ys_items_data_id`);

ALTER TABLE `pcore_ys_items_data`
  MODIFY `ys_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_wbj_search_data` (
  `wbj_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `wbj_template_id` int(10) NOT NULL COMMENT '委办局资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `wbj_items_data_id` int(10) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认启用),(STATUS_DISABLED,-2,禁用)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='委办局资源目录数据搜索表';

ALTER TABLE `pcore_wbj_search_data`
  ADD PRIMARY KEY (`wbj_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`wbj_template_id`,`identify`),
  ADD KEY `wbj_template_id` (`wbj_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_wbj_search_data`
  MODIFY `wbj_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_wbj_items_data` (
  `wbj_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='委办局资源目录数据表';

ALTER TABLE `pcore_wbj_items_data`
  ADD PRIMARY KEY (`wbj_items_data_id`);

ALTER TABLE `pcore_wbj_items_data`
  MODIFY `wbj_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_bj_search_data` (
  `bj_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `bj_template_id` int(10) NOT NULL COMMENT '本级资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `bj_items_data_id` char(24) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认启用),(STATUS_DISABLED,-2,禁用)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='本级资源目录数据搜索表';

ALTER TABLE `pcore_bj_search_data`
  ADD PRIMARY KEY (`bj_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`bj_template_id`,`identify`),
  ADD KEY `bj_template_id` (`bj_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_bj_search_data`
  MODIFY `bj_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_bj_items_data` (
  `bj_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='本级资源目录数据表';

ALTER TABLE `pcore_bj_items_data`
  ADD PRIMARY KEY (`bj_items_data_id`);

ALTER TABLE `pcore_bj_items_data`
  MODIFY `bj_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_gb_search_data` (
  `gb_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `gb_template_id` int(10) NOT NULL COMMENT '国标资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `gb_items_data_id` int(10) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认启用),(STATUS_DISABLED,-2,禁用)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='国标资源目录数据搜索表';

ALTER TABLE `pcore_gb_search_data`
  ADD PRIMARY KEY (`gb_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`gb_template_id`,`identify`),
  ADD KEY `gb_template_id` (`gb_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_gb_search_data`
  MODIFY `gb_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_gb_items_data` (
  `gb_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='国标资源目录数据表';

ALTER TABLE `pcore_gb_items_data`
  ADD PRIMARY KEY (`gb_items_data_id`);

ALTER TABLE `pcore_gb_items_data`
  MODIFY `gb_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';
