

CREATE TABLE `pcore_apply_form_rule` (
  `apply_form_rule_id` int(10) NOT NULL COMMENT '申请规则信息id',
  `publish_crew_id` int(10) NOT NULL COMMENT '发布人',
  `usergroup_id` int(10) NOT NULL COMMENT '来源委办局',
  `apply_crew_id` int(10) NOT NULL COMMENT '审核人',
  `apply_usergroup_id` int(10) NOT NULL COMMENT '审核委办局',
  `operation_type` tinyint(1) NOT NULL COMMENT '操作类型',
  `apply_info` text NOT NULL COMMENT '申请信息',
  `reject_reason` text NOT NULL COMMENT '驳回原因',
  `transformation_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `transformation_template_id` int(10) NOT NULL COMMENT '目标资源目录id',
  `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `source_template_id` int(10) NOT NULL COMMENT '来源资源目录id',
  `relation_id` int(10) NOT NULL COMMENT '关联规则id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `apply_status` tinyint(1) NOT NULL COMMENT '审核状态 (0,待审核, 2 通过, -2 驳回, -4 撤销)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='申请信息表';

ALTER TABLE `pcore_apply_form_rule`
  ADD PRIMARY KEY (`apply_form_rule_id`);

ALTER TABLE `pcore_apply_form_rule`
  MODIFY `apply_form_rule_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '申请规则信息id';

-- --------------------------------------------------------

CREATE TABLE `pcore_rule` (
  `rule_id` int(10) NOT NULL COMMENT '主键id',
  `transformation_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `transformation_template_id` int(10) NOT NULL COMMENT '目标资源目录id(本级/国标)',
  `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `source_template_id` int(10) NOT NULL COMMENT '来源资源目录id(委办局)',
  `usergroup_id` int(10) NOT NULL COMMENT '来源委办局id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `rules` text NOT NULL COMMENT '规则',
  `version` int(10) NOT NULL COMMENT '版本号',
  `data_total` int(10) NOT NULL COMMENT '已转换数据量',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态 (0 正常, -2 删除)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='资源目录规则表';

ALTER TABLE `pcore_rule` ADD PRIMARY KEY (`rule_id`);

ALTER TABLE `pcore_rule` MODIFY `rule_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------
