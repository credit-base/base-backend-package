-- --------------------------------------------------------

CREATE TABLE `pcore_journal` (
  `journal_id` int(10) NOT NULL COMMENT '信用刊物id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `source` varchar(255) NOT NULL COMMENT '来源',
  `cover` json NOT NULL COMMENT '封面',
  `attachment` json NOT NULL COMMENT '附件',
  `auth_images` json NOT NULL COMMENT '授权图片',
  `description` varchar(255) NOT NULL COMMENT '信用刊物简介',
  `year` year(4) NOT NULL COMMENT '年份',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `publish_usergroup_id` int(10) NOT NULL COMMENT '发布单位id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(禁用 -2,  默认 启用 0)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='信用刊物表';

ALTER TABLE `pcore_journal`
  ADD PRIMARY KEY (`journal_id`);

ALTER TABLE `pcore_journal`
  MODIFY `journal_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '信用刊物id';
