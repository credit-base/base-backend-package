
CREATE TABLE `pcore_website_customize` (
  `website_customize_id` int(10) NOT NULL COMMENT '网站定制id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `category` tinyint(1) NOT NULL COMMENT '定制类型(1 首页)',
  `content` json NOT NULL COMMENT '定制内容',
  `version` int(10) NOT NULL COMMENT '版本号',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 未发布, 2 发布)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='网站定制表';

ALTER TABLE `pcore_website_customize`
  ADD PRIMARY KEY (`website_customize_id`);

ALTER TABLE `pcore_website_customize`
  MODIFY `website_customize_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '网站定制id';
