
-- 信用投诉
CREATE TABLE `pcore_complaint` (
  `complaint_id` int(10) NOT NULL COMMENT '投诉主键id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `name` varchar(255) NOT NULL COMMENT '反馈人真实姓名/企业名称',
  `identify` varchar(50) NOT NULL COMMENT '统一社会信用代码/反馈人身份证号',
  `subject` varchar(255) NOT NULL COMMENT '被投诉主体',
  `type` tinyint(1) NOT NULL COMMENT '被投诉类型(0 个人 默认, 1 企业, 2 政府)',
  `contact` varchar(50) NOT NULL COMMENT '联系方式',
  `images` json NOT NULL COMMENT '图片',
  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',
  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用投诉表';

ALTER TABLE `pcore_complaint` ADD PRIMARY KEY (`complaint_id`);

ALTER TABLE `pcore_complaint`
  MODIFY `complaint_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- 信用表扬
CREATE TABLE `pcore_praise` (
  `praise_id` int(10) NOT NULL COMMENT '表扬主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `name` varchar(255) NOT NULL COMMENT '反馈人真实姓名/企业名称',
  `identify` varchar(50) NOT NULL COMMENT '统一社会信用代码/反馈人身份证号',
  `subject` varchar(255) NOT NULL COMMENT '被表扬主体',
  `type` tinyint(1) NOT NULL COMMENT '被表扬类型(0 个人 默认, 1 企业, 2 政府)',
  `contact` varchar(50) NOT NULL COMMENT '联系方式',
  `images` json NOT NULL COMMENT '图片',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销, 2 公示)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用表扬表';

ALTER TABLE `pcore_praise` ADD PRIMARY KEY (`praise_id`);

ALTER TABLE `pcore_praise`
  MODIFY `praise_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- 异议申诉
CREATE TABLE `pcore_appeal` (
  `appeal_id` int(10) NOT NULL COMMENT '异议申诉主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `name` varchar(255) NOT NULL COMMENT '姓名/企业名称',
  `identify` varchar(50) NOT NULL COMMENT '统一社会信用代码/身份证号',
  `type` tinyint(1) NOT NULL COMMENT '类型(0 个人 默认, 1 企业)',
  `contact` varchar(50) NOT NULL COMMENT '联系方式',
  `certificates` json NOT NULL COMMENT '上传身份证/营业执照',
  `images` json NOT NULL COMMENT '图片',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='异议申诉表';

ALTER TABLE `pcore_appeal` ADD PRIMARY KEY (`appeal_id`);

ALTER TABLE `pcore_appeal`
  MODIFY `appeal_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- 问题反馈
CREATE TABLE `pcore_feedback` (
  `feedback_id` int(10) NOT NULL COMMENT '问题反馈主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='问题反馈表';

ALTER TABLE `pcore_feedback` ADD PRIMARY KEY (`feedback_id`);

ALTER TABLE `pcore_feedback`
  MODIFY `feedback_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- 信用问答
CREATE TABLE `pcore_qa` (
  `qa_id` int(10) NOT NULL COMMENT '信用问答主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销, 2 公示)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用问答表';

ALTER TABLE `pcore_qa` ADD PRIMARY KEY (`qa_id`);

ALTER TABLE `pcore_qa`
  MODIFY `qa_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- 回复
CREATE TABLE `pcore_reply` (
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',
  `content` text NOT NULL COMMENT '回复内容',
  `images` json NOT NULL COMMENT '回复图片',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `crew_id` int(10) NOT NULL COMMENT '受理人',
  `admissibility` tinyint(1) NOT NULL COMMENT '受理情况(0 予以受理 默认, 1 不予受理)',

  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(0默认)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='回复信息表';

ALTER TABLE `pcore_reply` ADD PRIMARY KEY (`reply_id`);

ALTER TABLE `pcore_reply`
  MODIFY `reply_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '回复信息id';
