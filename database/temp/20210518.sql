-- --------------------------------------------------------

CREATE TABLE `pcore_member` (
  `member_id` int(10) NOT NULL COMMENT '主键id',
  `user_name` varchar(50) NOT NULL COMMENT '用户名',
  `real_name` varchar(50) NOT NULL COMMENT '姓名',
  `cellphone` char(11) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) NOT NULL COMMENT '邮箱',
  `cardid` char(18) NOT NULL COMMENT '身份证号码',
  `gender` tinyint(1) NOT NULL COMMENT '姓名(默认 0 空, 1 男, 2 女)',
  `password` char(32) NOT NULL COMMENT '密码',
  `salt` char(4) NOT NULL COMMENT '盐杂质',
  `contact_address` varchar(255) NOT NULL COMMENT '联系地址',
  `security_question` tinyint(1) NOT NULL COMMENT '密保问题',
  `security_answer` varchar(50) NOT NULL COMMENT '密保答案',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 启用, -2 禁用)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='前台用户表';

ALTER TABLE `pcore_member`
  ADD PRIMARY KEY (`member_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `cellphone` (`cellphone`),
  ADD UNIQUE KEY `email` (`email`);

ALTER TABLE `pcore_member`
  MODIFY `member_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';
