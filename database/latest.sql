

-- --------------------------------------------------------

CREATE TABLE `pcore_user_group` (
  `user_group_id` int(10) NOT NULL COMMENT '主键id',
  `name` varchar(100) NOT NULL COMMENT '委办局名称',
  `short_name` varchar(100) NOT NULL COMMENT '委办局简称',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 预留)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='委办局表';

ALTER TABLE `pcore_user_group`
  ADD PRIMARY KEY (`user_group_id`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `pcore_user_group`
  MODIFY `user_group_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_department` (
  `department_id` int(10) NOT NULL COMMENT '主键id',
  `name` varchar(100) NOT NULL COMMENT '科室名称',
  `user_group_id` int(10) NOT NULL COMMENT '所属委办局id',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 预留)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='科室表';

ALTER TABLE `pcore_department`
  ADD PRIMARY KEY (`department_id`);

ALTER TABLE `pcore_department`
  MODIFY `department_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_crew` (
  `crew_id` int(10) NOT NULL COMMENT '主键id',
  `user_name` char(50) NOT NULL COMMENT '账户',
  `cellphone` char(11) DEFAULT NULL COMMENT '手机号码',
  `real_name` varchar(50) NOT NULL COMMENT '用户姓名',
  `password` char(32) NOT NULL COMMENT '密码',
  `salt` char(4) NOT NULL COMMENT '盐杂质',
  `card_id` char(18) NOT NULL COMMENT '身份证号码',
  `category` tinyint(1) NOT NULL COMMENT '用户类型(1 超级管理员, 2 平台管理员, 3 委办局管理员, 4 操作用户)',
  `user_group_id` int(10) NOT NULL COMMENT '所属委办局id',
  `department_id` int(10) NOT NULL COMMENT '所属科室id',
  `purview` json NOT NULL COMMENT '权限范围',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 启用, -2 禁用)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OA人员表';

ALTER TABLE `pcore_crew`
  ADD PRIMARY KEY (`crew_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `cellphone` (`cellphone`);

ALTER TABLE `pcore_crew`
  MODIFY `crew_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_gb_template` (
  `gb_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='国标资源目录表';

ALTER TABLE `pcore_gb_template`
  ADD PRIMARY KEY (`gb_template_id`),
  ADD UNIQUE KEY `identify` (`identify`);

ALTER TABLE `pcore_gb_template`
  MODIFY `gb_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_bj_template` (
  `bj_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',
  `source_unit_id` int(10) NOT NULL COMMENT '来源单位id',
  `gb_template_id` int(10) NOT NULL COMMENT '国标目录id',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='本级资源目录表';

ALTER TABLE `pcore_bj_template`
  ADD PRIMARY KEY (`bj_template_id`),
  ADD UNIQUE KEY `identify` (`identify`, `source_unit_id`);

ALTER TABLE `pcore_bj_template`
  MODIFY `bj_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_wbj_template` (
  `wbj_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',
  `source_unit_id` int(10) NOT NULL COMMENT '来源单位id',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='委办局资源目录表';

ALTER TABLE `pcore_wbj_template`
  ADD PRIMARY KEY (`wbj_template_id`),
  ADD UNIQUE KEY `identify` (`identify`, `source_unit_id`);

ALTER TABLE `pcore_wbj_template`
  MODIFY `wbj_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_parent_task` (
  `parent_task_id` int(10) NOT NULL COMMENT '主键id',

  `template_type` tinyint(1) NOT NULL COMMENT '基础目录，国标目录 1 | 本级目录 2',
  `title` varchar(200) NOT NULL COMMENT '任务标题',
  `description` text NOT NULL COMMENT '任务描述',
  `end_time` date NOT NULL COMMENT '终结时间',
  `attachment` json NOT NULL COMMENT '依据附件',
  `template_id` int(10) NOT NULL COMMENT '指派目录id',
  `template_name` varchar(100) NOT NULL COMMENT '指派目录名称',
  `assign_objects` json NOT NULL COMMENT '指派对象',
  `finish_count` int(10) NOT NULL COMMENT '已完结总数，任务状态为已撤销、已确认、已终结时，算作已完结',
  `reason` text NOT NULL COMMENT '撤销原因',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工单任务父任务表';

ALTER TABLE `pcore_parent_task`
  ADD PRIMARY KEY (`parent_task_id`);

ALTER TABLE `pcore_parent_task`
  MODIFY `parent_task_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_work_order_task` (
  `work_order_task_id` int(10) NOT NULL COMMENT '主键id',

  `title` varchar(200) NOT NULL COMMENT '任务标题',
  `end_time` date NOT NULL COMMENT '终结时间',
  `reason` text NOT NULL COMMENT '撤销原因或终结原因',
  `feedback_records` json NOT NULL COMMENT '反馈记录',

  `template_type` tinyint(1) NOT NULL COMMENT '基础目录，国标目录 1 | 本级目录 2',
  `template_id` int(10) NOT NULL COMMENT '指派目录id',
  `template_name` varchar(100) NOT NULL COMMENT '指派目录名称',
  `assign_object_id` int(10) NOT NULL COMMENT '指派对象id',
  `assign_object_name` varchar(100) NOT NULL COMMENT '指派对象名称',
  `parent_task_id` int(10) NOT NULL COMMENT '父任务id',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工单任务表';

ALTER TABLE `pcore_work_order_task`
  ADD PRIMARY KEY (`work_order_task_id`),
  ADD UNIQUE KEY `assign_object_id` (`assign_object_id`, `parent_task_id`);

ALTER TABLE `pcore_work_order_task`
  MODIFY `work_order_task_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_ys_search_data` (
  `ys_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `wbj_template_id` int(10) NOT NULL COMMENT '委办局资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `ys_items_data_id` int(10) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认正常),(STATUS_DISABLED -2 屏蔽)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='原始资源目录数据搜索表';

ALTER TABLE `pcore_ys_search_data`
  ADD PRIMARY KEY (`ys_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`wbj_template_id`,`identify`),
  ADD KEY `wbj_template_id` (`wbj_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_ys_search_data`
  MODIFY `ys_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_ys_items_data` (
  `ys_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='原始资源目录数据表';

ALTER TABLE `pcore_ys_items_data`
  ADD PRIMARY KEY (`ys_items_data_id`);

ALTER TABLE `pcore_ys_items_data`
  MODIFY `ys_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_wbj_search_data` (
  `wbj_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `wbj_template_id` int(10) NOT NULL COMMENT '委办局资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `wbj_items_data_id` int(10) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认正常),(STATUS_DISABLED -2 屏蔽)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='委办局资源目录数据搜索表';

ALTER TABLE `pcore_wbj_search_data`
  ADD PRIMARY KEY (`wbj_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`wbj_template_id`,`identify`),
  ADD KEY `wbj_template_id` (`wbj_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_wbj_search_data`
  MODIFY `wbj_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_wbj_items_data` (
  `wbj_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='委办局资源目录数据表';

ALTER TABLE `pcore_wbj_items_data`
  ADD PRIMARY KEY (`wbj_items_data_id`);

ALTER TABLE `pcore_wbj_items_data`
  MODIFY `wbj_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_bj_search_data` (
  `bj_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `bj_template_id` int(10) NOT NULL COMMENT '本级资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `bj_items_data_id` char(24) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_CONFIRM 0 待确认),(STATUS_ENABLED 2 已确认),(STATUS_DISABLED -2 屏蔽),(STATUS_DELETED -4 封存)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id',
  `front_end_processor_status` tinyint(1) NOT NULL COMMENT '前置机数据导入状态 0 未导入 2 导入',
  `description` text NOT NULL COMMENT '待确认规则描述'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='本级资源目录数据搜索表';

ALTER TABLE `pcore_bj_search_data`
  ADD PRIMARY KEY (`bj_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`bj_template_id`,`identify`),
  ADD KEY `bj_template_id` (`bj_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_bj_search_data`
  MODIFY `bj_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_bj_items_data` (
  `bj_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='本级资源目录数据表';

ALTER TABLE `pcore_bj_items_data`
  ADD PRIMARY KEY (`bj_items_data_id`);

ALTER TABLE `pcore_bj_items_data`
  MODIFY `bj_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_gb_search_data` (
  `gb_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `gb_template_id` int(10) NOT NULL COMMENT '国标资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `gb_items_data_id` int(10) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_CONFIRM 0 待确认),(STATUS_ENABLED 2 已确认),(STATUS_DISABLED -2 屏蔽),(STATUS_DELETED -4 封存)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id',
  `front_end_processor_status` tinyint(1) NOT NULL COMMENT '前置机数据导入状态 0 未导入 2 导入',
  `description` text NOT NULL COMMENT '待确认规则描述'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='国标资源目录数据搜索表';

ALTER TABLE `pcore_gb_search_data`
  ADD PRIMARY KEY (`gb_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`gb_template_id`,`identify`),
  ADD KEY `gb_template_id` (`gb_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_gb_search_data`
  MODIFY `gb_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_gb_items_data` (
  `gb_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='国标资源目录数据表';

ALTER TABLE `pcore_gb_items_data`
  ADD PRIMARY KEY (`gb_items_data_id`);

ALTER TABLE `pcore_gb_items_data`
  MODIFY `gb_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_apply_form` (
  `apply_form_id` int(10) NOT NULL COMMENT '申请信息id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `relation_id` int(10) NOT NULL COMMENT '关联id',
  `apply_usergroup_id` int(10) NOT NULL COMMENT '审核委办局',
  `apply_crew_id` int(10) NOT NULL COMMENT '审核人',
  `operation_type` tinyint(1) NOT NULL COMMENT '操作类型',
  `apply_info` text NOT NULL COMMENT '申请信息',
  `reject_reason` text NOT NULL COMMENT '驳回原因',
  `apply_info_category` tinyint(1) NOT NULL COMMENT '可以申请的信息分类',
  `apply_info_type` int(10) NOT NULL COMMENT '申请信息的最小分类',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `apply_status` tinyint(1) NOT NULL COMMENT '状态 0,待审核, 2 通过, -2 驳回)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='申请信息表';

ALTER TABLE `pcore_apply_form`
  ADD PRIMARY KEY (`apply_form_id`);

ALTER TABLE `pcore_apply_form`
  MODIFY `apply_form_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '申请信息id';

CREATE TABLE `pcore_news` (
  `news_id` int(10) NOT NULL COMMENT '新闻id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `source` varchar(255) NOT NULL COMMENT '来源',
  `cover` json NOT NULL COMMENT '封面',
  `attachments` json NOT NULL COMMENT '附件',
  `content` longtext NOT NULL COMMENT '内容',
  `description` varchar(255) NOT NULL COMMENT '新闻简介',
  `parent_category` tinyint(1) NOT NULL COMMENT '父级分类',
  `category` int(10) NOT NULL COMMENT '分类',
  `type` int(10) NOT NULL COMMENT '新闻类型',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `publish_usergroup_id` int(10) NOT NULL COMMENT '发布单位id',
  `stick` tinyint(1) NOT NULL COMMENT '置顶(置顶 2, 默认 不置顶 0)',
  `dimension` tinyint(1) NOT NULL COMMENT '数据共享维度',
  `banner_status` tinyint(1) NOT NULL COMMENT '轮播状态(轮播 2,  默认 不轮播 0)',
  `banner_image` json NOT NULL COMMENT '轮播图图片',
  `home_page_show_status` tinyint(1) NOT NULL COMMENT '首页展示状态(展示 2,  默认 不展示 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(禁用 -2,  默认 启用 0)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='新闻表';

ALTER TABLE `pcore_news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `type` (`type`,`publish_usergroup_id`,`status`);

ALTER TABLE `pcore_news`
  MODIFY `news_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '新闻id';

CREATE TABLE `pcore_member` (
  `member_id` int(10) NOT NULL COMMENT '主键id',
  `user_name` varchar(50) NOT NULL COMMENT '用户名',
  `real_name` varchar(50) NOT NULL COMMENT '姓名',
  `cellphone` char(11) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) NOT NULL COMMENT '邮箱',
  `cardid` char(18) NOT NULL COMMENT '身份证号码',
  `gender` tinyint(1) NOT NULL COMMENT '姓名(默认 0 空, 1 男, 2 女)',
  `password` char(32) NOT NULL COMMENT '密码',
  `salt` char(4) NOT NULL COMMENT '盐杂质',
  `contact_address` varchar(255) NOT NULL COMMENT '联系地址',
  `security_question` tinyint(1) NOT NULL COMMENT '密保问题',
  `security_answer` varchar(50) NOT NULL COMMENT '密保答案',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 启用, -2 禁用)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='前台用户表';

ALTER TABLE `pcore_member`
  ADD PRIMARY KEY (`member_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `cellphone` (`cellphone`),
  ADD UNIQUE KEY `email` (`email`);

ALTER TABLE `pcore_member`
  MODIFY `member_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_journal` (
  `journal_id` int(10) NOT NULL COMMENT '信用刊物id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `source` varchar(255) NOT NULL COMMENT '来源',
  `cover` json NOT NULL COMMENT '封面',
  `attachment` json NOT NULL COMMENT '附件',
  `auth_images` json NOT NULL COMMENT '授权图片',
  `description` varchar(255) NOT NULL COMMENT '信用刊物简介',
  `year` year(4) NOT NULL COMMENT '年份',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `publish_usergroup_id` int(10) NOT NULL COMMENT '发布单位id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(禁用 -2,  默认 启用 0)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='信用刊物表';

ALTER TABLE `pcore_journal`
  ADD PRIMARY KEY (`journal_id`);

ALTER TABLE `pcore_journal`
  MODIFY `journal_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '信用刊物id';

-- `real_name`为冗余字段,为了方便搜索
CREATE TABLE `pcore_credit_photography` (
  `credit_photography_id` int(10) NOT NULL COMMENT '信用随手拍主键id',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `attachments` json NOT NULL COMMENT '附件: 图片/视频',
  `member_id` int(10) NOT NULL COMMENT '用户id',
  `real_name` varchar(50) NOT NULL COMMENT '姓名',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 正常 0, 删除 -2)',
  `apply_status` tinyint(1) NOT NULL COMMENT '审核状态 0,待审核, 2 通过, -2 驳回)',
  `apply_crew_id` int(10) NOT NULL COMMENT '审核人',
  `reject_reason` text NOT NULL COMMENT '驳回原因',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='信用随手拍表';

ALTER TABLE `pcore_credit_photography`
  ADD PRIMARY KEY (`credit_photography_id`);

ALTER TABLE `pcore_credit_photography`
  MODIFY `credit_photography_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '信用随手拍主键id';

CREATE TABLE `pcore_schedule_task` (
  `task_id` int(10) NOT NULL COMMENT '主键id',
  `parent_id` int(10) NOT NULL COMMENT '父任务id',
  `identify` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1:once, 2:period',
  `data` text NOT NULL,
  `next_schedule_time` int(10) NOT NULL,
  `nice` int(10) NOT NULL,
  `pid` int(10) NOT NULL COMMENT '进程id',
  `status_time` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '-4:未定义, -3:失败, -2:异常, 0:睡眠, 2:待处理, 4:执行中, 6:成功',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `pcore_schedule_task`
  ADD PRIMARY KEY (`task_id`);

ALTER TABLE `pcore_schedule_task`
  MODIFY `task_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_apply_form_rule` (
  `apply_form_rule_id` int(10) NOT NULL COMMENT '申请规则信息id',
  `publish_crew_id` int(10) NOT NULL COMMENT '发布人',
  `usergroup_id` int(10) NOT NULL COMMENT '来源委办局',
  `apply_crew_id` int(10) NOT NULL COMMENT '审核人',
  `apply_usergroup_id` int(10) NOT NULL COMMENT '审核委办局',
  `operation_type` tinyint(1) NOT NULL COMMENT '操作类型',
  `apply_info` text NOT NULL COMMENT '申请信息',
  `reject_reason` text NOT NULL COMMENT '驳回原因',
  `transformation_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `transformation_template_id` int(10) NOT NULL COMMENT '目标资源目录id',
  `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `source_template_id` int(10) NOT NULL COMMENT '来源资源目录id',
  `relation_id` int(10) NOT NULL COMMENT '关联规则id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `apply_status` tinyint(1) NOT NULL COMMENT '审核状态 (0,待审核, 2 通过, -2 驳回, -4 撤销)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='申请信息表';

ALTER TABLE `pcore_apply_form_rule`
  ADD PRIMARY KEY (`apply_form_rule_id`);

ALTER TABLE `pcore_apply_form_rule`
  MODIFY `apply_form_rule_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '申请规则信息id';

-- --------------------------------------------------------

CREATE TABLE `pcore_rule` (
  `rule_id` int(10) NOT NULL COMMENT '主键id',
  `transformation_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `transformation_template_id` int(10) NOT NULL COMMENT '目标资源目录id(本级/国标)',
  `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `source_template_id` int(10) NOT NULL COMMENT '来源资源目录id(委办局)',
  `usergroup_id` int(10) NOT NULL COMMENT '来源委办局id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `rules` text NOT NULL COMMENT '规则',
  `version` int(10) NOT NULL COMMENT '版本号',
  `data_total` int(10) NOT NULL COMMENT '已转换数据量',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态 (0 正常, -2 删除)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='资源目录规则表';

ALTER TABLE `pcore_rule` ADD PRIMARY KEY (`rule_id`);

ALTER TABLE `pcore_rule` MODIFY `rule_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_task` (
  `task_id` int(10) NOT NULL COMMENT '主键id',
  `crew_id` int(10) NOT NULL COMMENT '员工id',
  `user_group_id` int(10) NOT NULL COMMENT '委办局id',
  `pid` int(10) NOT NULL COMMENT '父id',

  `total` int(10) NOT NULL COMMENT '总数',
  `success_number` int(10) NOT NULL COMMENT '成功数',
  `failure_number` int(10) NOT NULL COMMENT '失败数',

  `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `source_template_id` int(10) NOT NULL COMMENT '来源目录id',
  `target_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `target_template_id` int(10) NOT NULL COMMENT '目标目录id',
  `target_rule_id` int(10) NOT NULL COMMENT '入目标库使用规则id',

  `schedule_task_id` int(10) NOT NULL COMMENT '调度任务id',
  `error_number` int(10) NOT NULL COMMENT '错误编号',
  `file_name` varchar(255) NOT NULL COMMENT '上传文件文件名',

  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态 (0 进行中, -2 失败, 2 成功, -3 失败文件下载成功)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='任务表';

ALTER TABLE `pcore_task` ADD PRIMARY KEY (`task_id`);

ALTER TABLE `pcore_task` MODIFY `task_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_schedule_task_notify_record` (
  `record_id` int(10) NOT NULL COMMENT '主键id',
  `name` varchar(255) NOT NULL COMMENT '文件名',
  `hash` char(32) NOT NULL COMMENT 'hash',
  `status` tinyint(1) NOT NULL COMMENT '0: 待执行，2: 执行成功',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `status_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `pcore_schedule_task_notify_record`
  ADD PRIMARY KEY (`record_id`);

ALTER TABLE `pcore_schedule_task_notify_record`
  MODIFY `record_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

ALTER TABLE `pcore_schedule_task_notify_record` ADD UNIQUE(`hash`);

-- --------------------------------------------------------

CREATE TABLE `pcore_error_data` (
  `error_data_id` int(10) NOT NULL COMMENT '主键id',

  `task_id` int(10) NOT NULL COMMENT '任务id',
  `category` tinyint(1) NOT NULL COMMENT '库类别',
  `template_id` int(10) NOT NULL COMMENT '目录id',
  `items_data` text NOT NULL COMMENT '资源目录数据',
  `error_type` tinyint(1) NOT NULL COMMENT '错误类型',
  `error_reason` json NOT NULL COMMENT '错误原因',

  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, 1 程序异常, 2 入库异常)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='错误数据表';

ALTER TABLE `pcore_error_data`
  ADD PRIMARY KEY (`error_data_id`);

ALTER TABLE `pcore_error_data`
  MODIFY `error_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_website_customize` (
  `website_customize_id` int(10) NOT NULL COMMENT '网站定制id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `category` tinyint(1) NOT NULL COMMENT '定制类型(1 首页)',
  `content` json NOT NULL COMMENT '定制内容',
  `version` int(10) NOT NULL COMMENT '版本号',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 未发布, 2 发布)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='网站定制表';

ALTER TABLE `pcore_website_customize`
  ADD PRIMARY KEY (`website_customize_id`);

ALTER TABLE `pcore_website_customize`
  MODIFY `website_customize_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '网站定制id';

-- --------------------------------------------------------

CREATE TABLE `pcore_enterprise` (
  `enterprise_id` int(10) NOT NULL COMMENT '企业主键id',
  `name` varchar(255) NOT NULL COMMENT '企业名称/个体名称',
  `unified_social_credit_code` varchar(50) NOT NULL COMMENT '统一社会信用代码',
  `establishment_date` int(10) NOT NULL COMMENT '成立日期/注册日期',
  `approval_date` int(10) NOT NULL COMMENT '核准日期',
  `address` varchar(255) NOT NULL COMMENT '住所/经营场所',
  `registration_capital` varchar(50) NOT NULL COMMENT '注册资本（万）',
  `business_term_start` int(10) NOT NULL COMMENT '营业期限自',
  `business_term_to` int(10) NOT NULL COMMENT '营业期限至',
  `business_scope` text NOT NULL COMMENT '经营范围',
  `registration_authority` varchar(255) NOT NULL COMMENT '登记机关',
  `principal` varchar(255) NOT NULL COMMENT '法定代表人/经营者',
  `principal_card_id` varchar(20) NOT NULL COMMENT '法人身份证号',
  `registration_status` varchar(255) NOT NULL COMMENT '登记状态(1=存续(在营、开业、在册);2=吊销，未注销;3=吊 销，已注销;4=注销;5=撤 销;6=迁出;9=其他)',
  `enterprise_type_code` varchar(255) NOT NULL COMMENT '企业类型代码',
  `enterprise_type` varchar(255) NOT NULL COMMENT '企业类型',
  `data` text NOT NULL COMMENT '其他数据',
  `industry_category` char(1) NOT NULL COMMENT '行业门类',
  `industry_code` char(5) NOT NULL COMMENT '行业代码',
  `administrative_area` int(4) NOT NULL COMMENT '所属区域',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_NORMAL,0,默认),(STATUS_DELETE,-2,删除)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业表';

ALTER TABLE `pcore_enterprise`
  ADD PRIMARY KEY (`enterprise_id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `unified_social_credit_code` (`unified_social_credit_code`);

ALTER TABLE `pcore_enterprise`
  MODIFY `enterprise_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '企业主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_base_template` (
  `base_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='基础资源目录表';

ALTER TABLE `pcore_base_template`
  ADD PRIMARY KEY (`base_template_id`),
  ADD UNIQUE KEY `identify` (`identify`);

ALTER TABLE `pcore_base_template`
  MODIFY `base_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- ----------------------------------------------------------

CREATE TABLE `pcore_qzj_template` (
  `qzj_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `category` tinyint(1) NOT NULL COMMENT '目录类别，前置机委办局 20 | 前置机本级 21 | 前置机国标 22',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',
  `source_unit_id` int(10) NOT NULL COMMENT '来源委办局id',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='本级资源目录表';

ALTER TABLE `pcore_qzj_template`
  ADD PRIMARY KEY (`qzj_template_id`),
  ADD UNIQUE KEY `identify` (`identify`, `source_unit_id`, `category`);

ALTER TABLE `pcore_qzj_template`
  MODIFY `qzj_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 信用投诉
CREATE TABLE `pcore_complaint` (
  `complaint_id` int(10) NOT NULL COMMENT '投诉主键id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `name` varchar(255) NOT NULL COMMENT '反馈人真实姓名/企业名称',
  `identify` varchar(50) NOT NULL COMMENT '统一社会信用代码/反馈人身份证号',
  `subject` varchar(255) NOT NULL COMMENT '被投诉主体',
  `type` tinyint(1) NOT NULL COMMENT '被投诉类型(0 个人 默认, 1 企业, 2 政府)',
  `contact` varchar(50) NOT NULL COMMENT '联系方式',
  `images` json NOT NULL COMMENT '图片',
  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',
  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用投诉表';

ALTER TABLE `pcore_complaint` ADD PRIMARY KEY (`complaint_id`);

ALTER TABLE `pcore_complaint`
  MODIFY `complaint_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 信用表扬
CREATE TABLE `pcore_praise` (
  `praise_id` int(10) NOT NULL COMMENT '表扬主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `name` varchar(255) NOT NULL COMMENT '反馈人真实姓名/企业名称',
  `identify` varchar(50) NOT NULL COMMENT '统一社会信用代码/反馈人身份证号',
  `subject` varchar(255) NOT NULL COMMENT '被表扬主体',
  `type` tinyint(1) NOT NULL COMMENT '被表扬类型(0 个人 默认, 1 企业, 2 政府)',
  `contact` varchar(50) NOT NULL COMMENT '联系方式',
  `images` json NOT NULL COMMENT '图片',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销, 2 公示)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用表扬表';

ALTER TABLE `pcore_praise` ADD PRIMARY KEY (`praise_id`);

ALTER TABLE `pcore_praise`
  MODIFY `praise_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 异议申诉
CREATE TABLE `pcore_appeal` (
  `appeal_id` int(10) NOT NULL COMMENT '异议申诉主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `name` varchar(255) NOT NULL COMMENT '姓名/企业名称',
  `identify` varchar(50) NOT NULL COMMENT '统一社会信用代码/身份证号',
  `type` tinyint(1) NOT NULL COMMENT '类型(0 个人 默认, 1 企业)',
  `contact` varchar(50) NOT NULL COMMENT '联系方式',
  `certificates` json NOT NULL COMMENT '上传身份证/营业执照',
  `images` json NOT NULL COMMENT '图片',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='异议申诉表';

ALTER TABLE `pcore_appeal` ADD PRIMARY KEY (`appeal_id`);

ALTER TABLE `pcore_appeal`
  MODIFY `appeal_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 问题反馈
CREATE TABLE `pcore_feedback` (
  `feedback_id` int(10) NOT NULL COMMENT '问题反馈主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='问题反馈表';

ALTER TABLE `pcore_feedback` ADD PRIMARY KEY (`feedback_id`);

ALTER TABLE `pcore_feedback`
  MODIFY `feedback_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 信用问答
CREATE TABLE `pcore_qa` (
  `qa_id` int(10) NOT NULL COMMENT '信用问答主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销, 2 公示)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用问答表';

ALTER TABLE `pcore_qa` ADD PRIMARY KEY (`qa_id`);

ALTER TABLE `pcore_qa`
  MODIFY `qa_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 回复
CREATE TABLE `pcore_reply` (
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',
  `content` text NOT NULL COMMENT '回复内容',
  `images` json NOT NULL COMMENT '回复图片',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `crew_id` int(10) NOT NULL COMMENT '受理人',
  `admissibility` tinyint(1) NOT NULL COMMENT '受理情况(0 予以受理 默认, 1 不予受理)',

  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(0默认)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='回复信息表';

ALTER TABLE `pcore_reply` ADD PRIMARY KEY (`reply_id`);

ALTER TABLE `pcore_reply`
  MODIFY `reply_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '回复信息id';

-- --------------------------------------------------------