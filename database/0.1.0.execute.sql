

-- --------------------------------------------------------

CREATE TABLE `pcore_user_group` (
  `user_group_id` int(10) NOT NULL COMMENT '主键id',
  `name` varchar(100) NOT NULL COMMENT '委办局名称',
  `short_name` varchar(100) NOT NULL COMMENT '委办局简称',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 预留)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='委办局表';

ALTER TABLE `pcore_user_group`
  ADD PRIMARY KEY (`user_group_id`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `pcore_user_group`
  MODIFY `user_group_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_department` (
  `department_id` int(10) NOT NULL COMMENT '主键id',
  `name` varchar(100) NOT NULL COMMENT '科室名称',
  `user_group_id` int(10) NOT NULL COMMENT '所属委办局id',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 预留)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='科室表';

ALTER TABLE `pcore_department`
  ADD PRIMARY KEY (`department_id`);

ALTER TABLE `pcore_department`
  MODIFY `department_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_crew` (
  `crew_id` int(10) NOT NULL COMMENT '主键id',
  `user_name` char(50) NOT NULL COMMENT '账户',
  `cellphone` char(11) DEFAULT NULL COMMENT '手机号码',
  `real_name` varchar(50) NOT NULL COMMENT '用户姓名',
  `password` char(32) NOT NULL COMMENT '密码',
  `salt` char(4) NOT NULL COMMENT '盐杂质',
  `card_id` char(18) NOT NULL COMMENT '身份证号码',
  `category` tinyint(1) NOT NULL COMMENT '用户类型(1 超级管理员, 2 平台管理员, 3 委办局管理员, 4 操作用户)',
  `user_group_id` int(10) NOT NULL COMMENT '所属委办局id',
  `department_id` int(10) NOT NULL COMMENT '所属科室id',
  `purview` json NOT NULL COMMENT '权限范围',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 启用, -2 禁用)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OA人员表';

ALTER TABLE `pcore_crew`
  ADD PRIMARY KEY (`crew_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `cellphone` (`cellphone`);

ALTER TABLE `pcore_crew`
  MODIFY `crew_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_apply_form` (
  `apply_form_id` int(10) NOT NULL COMMENT '申请信息id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `relation_id` int(10) NOT NULL COMMENT '关联id',
  `apply_usergroup_id` int(10) NOT NULL COMMENT '审核委办局',
  `apply_crew_id` int(10) NOT NULL COMMENT '审核人',
  `operation_type` tinyint(1) NOT NULL COMMENT '操作类型',
  `apply_info` text NOT NULL COMMENT '申请信息',
  `reject_reason` text NOT NULL COMMENT '驳回原因',
  `apply_info_category` tinyint(1) NOT NULL COMMENT '可以申请的信息分类',
  `apply_info_type` int(10) NOT NULL COMMENT '申请信息的最小分类',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `apply_status` tinyint(1) NOT NULL COMMENT '状态 0,待审核, 2 通过, -2 驳回)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='申请信息表';

ALTER TABLE `pcore_apply_form`
  ADD PRIMARY KEY (`apply_form_id`);

ALTER TABLE `pcore_apply_form`
  MODIFY `apply_form_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '申请信息id';

-- --------------------------------------------------------

CREATE TABLE `pcore_news` (
  `news_id` int(10) NOT NULL COMMENT '新闻id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `source` varchar(255) NOT NULL COMMENT '来源',
  `cover` json NOT NULL COMMENT '封面',
  `attachments` json NOT NULL COMMENT '附件',
  `content` longtext NOT NULL COMMENT '内容',
  `description` varchar(255) NOT NULL COMMENT '新闻简介',
  `parent_category` tinyint(1) NOT NULL COMMENT '父级分类',
  `category` int(10) NOT NULL COMMENT '分类',
  `type` int(10) NOT NULL COMMENT '新闻类型',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `publish_usergroup_id` int(10) NOT NULL COMMENT '发布单位id',
  `stick` tinyint(1) NOT NULL COMMENT '置顶(置顶 2, 默认 不置顶 0)',
  `dimension` tinyint(1) NOT NULL COMMENT '数据共享维度',
  `banner_status` tinyint(1) NOT NULL COMMENT '轮播状态(轮播 2,  默认 不轮播 0)',
  `banner_image` json NOT NULL COMMENT '轮播图图片',
  `home_page_show_status` tinyint(1) NOT NULL COMMENT '首页展示状态(展示 2,  默认 不展示 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(禁用 -2,  默认 启用 0)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='新闻表';

ALTER TABLE `pcore_news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `type` (`type`,`publish_usergroup_id`,`status`);

ALTER TABLE `pcore_news`
  MODIFY `news_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '新闻id';

-- --------------------------------------------------------

CREATE TABLE `pcore_member` (
  `member_id` int(10) NOT NULL COMMENT '主键id',
  `user_name` varchar(50) NOT NULL COMMENT '用户名',
  `real_name` varchar(50) NOT NULL COMMENT '姓名',
  `cellphone` char(11) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) NOT NULL COMMENT '邮箱',
  `cardid` char(18) NOT NULL COMMENT '身份证号码',
  `gender` tinyint(1) NOT NULL COMMENT '姓名(默认 0 空, 1 男, 2 女)',
  `password` char(32) NOT NULL COMMENT '密码',
  `salt` char(4) NOT NULL COMMENT '盐杂质',
  `contact_address` varchar(255) NOT NULL COMMENT '联系地址',
  `security_question` tinyint(1) NOT NULL COMMENT '密保问题',
  `security_answer` varchar(50) NOT NULL COMMENT '密保答案',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0 启用, -2 禁用)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='前台用户表';

ALTER TABLE `pcore_member`
  ADD PRIMARY KEY (`member_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `cellphone` (`cellphone`),
  ADD UNIQUE KEY `email` (`email`);

ALTER TABLE `pcore_member`
  MODIFY `member_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_journal` (
  `journal_id` int(10) NOT NULL COMMENT '信用刊物id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `source` varchar(255) NOT NULL COMMENT '来源',
  `cover` json NOT NULL COMMENT '封面',
  `attachment` json NOT NULL COMMENT '附件',
  `auth_images` json NOT NULL COMMENT '授权图片',
  `description` varchar(255) NOT NULL COMMENT '信用刊物简介',
  `year` year(4) NOT NULL COMMENT '年份',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `publish_usergroup_id` int(10) NOT NULL COMMENT '发布单位id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(禁用 -2,  默认 启用 0)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='信用刊物表';

ALTER TABLE `pcore_journal`
  ADD PRIMARY KEY (`journal_id`);

ALTER TABLE `pcore_journal`
  MODIFY `journal_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '信用刊物id';

-- --------------------------------------------------------

-- `real_name`为冗余字段,为了方便搜索
CREATE TABLE `pcore_credit_photography` (
  `credit_photography_id` int(10) NOT NULL COMMENT '信用随手拍主键id',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `attachments` json NOT NULL COMMENT '附件: 图片/视频',
  `member_id` int(10) NOT NULL COMMENT '用户id',
  `real_name` varchar(50) NOT NULL COMMENT '姓名',
  `status` tinyint(1) NOT NULL COMMENT '状态(默认 正常 0, 删除 -2)',
  `apply_status` tinyint(1) NOT NULL COMMENT '审核状态 0,待审核, 2 通过, -2 驳回)',
  `apply_crew_id` int(10) NOT NULL COMMENT '审核人',
  `reject_reason` text NOT NULL COMMENT '驳回原因',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='信用随手拍表';

ALTER TABLE `pcore_credit_photography`
  ADD PRIMARY KEY (`credit_photography_id`);

ALTER TABLE `pcore_credit_photography`
  MODIFY `credit_photography_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '信用随手拍主键id';

-- --------------------------------------------------------

-- 信用投诉
CREATE TABLE `pcore_complaint` (
  `complaint_id` int(10) NOT NULL COMMENT '投诉主键id',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `name` varchar(255) NOT NULL COMMENT '反馈人真实姓名/企业名称',
  `identify` varchar(50) NOT NULL COMMENT '统一社会信用代码/反馈人身份证号',
  `subject` varchar(255) NOT NULL COMMENT '被投诉主体',
  `type` tinyint(1) NOT NULL COMMENT '被投诉类型(0 个人 默认, 1 企业, 2 政府)',
  `contact` varchar(50) NOT NULL COMMENT '联系方式',
  `images` json NOT NULL COMMENT '图片',
  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',
  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用投诉表';

ALTER TABLE `pcore_complaint` ADD PRIMARY KEY (`complaint_id`);

ALTER TABLE `pcore_complaint`
  MODIFY `complaint_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 信用表扬
CREATE TABLE `pcore_praise` (
  `praise_id` int(10) NOT NULL COMMENT '表扬主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `name` varchar(255) NOT NULL COMMENT '反馈人真实姓名/企业名称',
  `identify` varchar(50) NOT NULL COMMENT '统一社会信用代码/反馈人身份证号',
  `subject` varchar(255) NOT NULL COMMENT '被表扬主体',
  `type` tinyint(1) NOT NULL COMMENT '被表扬类型(0 个人 默认, 1 企业, 2 政府)',
  `contact` varchar(50) NOT NULL COMMENT '联系方式',
  `images` json NOT NULL COMMENT '图片',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销, 2 公示)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用表扬表';

ALTER TABLE `pcore_praise` ADD PRIMARY KEY (`praise_id`);

ALTER TABLE `pcore_praise`
  MODIFY `praise_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 异议申诉
CREATE TABLE `pcore_appeal` (
  `appeal_id` int(10) NOT NULL COMMENT '异议申诉主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `name` varchar(255) NOT NULL COMMENT '姓名/企业名称',
  `identify` varchar(50) NOT NULL COMMENT '统一社会信用代码/身份证号',
  `type` tinyint(1) NOT NULL COMMENT '类型(0 个人 默认, 1 企业)',
  `contact` varchar(50) NOT NULL COMMENT '联系方式',
  `certificates` json NOT NULL COMMENT '上传身份证/营业执照',
  `images` json NOT NULL COMMENT '图片',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='异议申诉表';

ALTER TABLE `pcore_appeal` ADD PRIMARY KEY (`appeal_id`);

ALTER TABLE `pcore_appeal`
  MODIFY `appeal_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 问题反馈
CREATE TABLE `pcore_feedback` (
  `feedback_id` int(10) NOT NULL COMMENT '问题反馈主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='问题反馈表';

ALTER TABLE `pcore_feedback` ADD PRIMARY KEY (`feedback_id`);

ALTER TABLE `pcore_feedback`
  MODIFY `feedback_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 信用问答
CREATE TABLE `pcore_qa` (
  `qa_id` int(10) NOT NULL COMMENT '信用问答主键id',

  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `member_id` int(10) NOT NULL COMMENT '前台用户id',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',

  `accept_status` tinyint(1) NOT NULL COMMENT '受理状态(0 待受理, 1 受理中, 2 受理完成)',
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',

  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, -2 撤销, 2 公示)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信用问答表';

ALTER TABLE `pcore_qa` ADD PRIMARY KEY (`qa_id`);

ALTER TABLE `pcore_qa`
  MODIFY `qa_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

-- 回复
CREATE TABLE `pcore_reply` (
  `reply_id` int(10) NOT NULL COMMENT '回复信息id',
  `content` text NOT NULL COMMENT '回复内容',
  `images` json NOT NULL COMMENT '回复图片',
  `accept_usergroup_id` int(10) NOT NULL COMMENT '受理委办局id',
  `crew_id` int(10) NOT NULL COMMENT '受理人',
  `admissibility` tinyint(1) NOT NULL COMMENT '受理情况(0 予以受理 默认, 1 不予受理)',

  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(0默认)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='回复信息表';

ALTER TABLE `pcore_reply` ADD PRIMARY KEY (`reply_id`);

ALTER TABLE `pcore_reply`
  MODIFY `reply_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '回复信息id';

-- --------------------------------------------------------