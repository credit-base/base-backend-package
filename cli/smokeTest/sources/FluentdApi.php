<?php

class fluentdApi implements ISmokeTest
{
    public function smokeTest() : bool
    {
        return fluentdSmokeTest(
            \Marmot\Core::$container->get('fluentd.address'),
            \Marmot\Core::$container->get('fluentd.port')
        );
    }
}
